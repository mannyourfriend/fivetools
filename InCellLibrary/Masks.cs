﻿using FIVE.InCellLibrary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace FIVE
{
    namespace Masks
    {
        //4/2021 Bay Johnson original code set
        //if rotate transform is being used, make sure major axis is making the ellipse longer in the y direction before rotation
        public class MaskListEntry
        {
            public MaskList Parent { get; set; }
            public string[] Cols { get; set; }
            public string PlateID { get; set; }
            public string FileName { get; set; }
            public string WellLabel { get; set; }
            public int FOV { get; set; }
            public string WellField { get => XDCE_Image.WellFieldKey(WellLabel, FOV); }
            public string ObjectID { get; set; }
            public float Left_pix { get; set; }
            public float Top_pix { get; set; }
            public float Width_pix { get; set; }
            public float Height_pix { get; set; }
            public PointF Pos_Pix { get => new PointF(Left_pix, Top_pix); }
            public SizeF Size_Pix { get => new SizeF(Width_pix, Height_pix); }
            public float axisAngle_deg { get; set; }
            //public bool Grayscale { get; set; }
            public float Intensity { get; set; }
            public string Intensity_Method { get; set; }
            /// <summary>
            /// These are half-width and half-height
            /// </summary>
            public int majorAxisLength_Pix { get => (int)(majorAxisLength_um / Parent.PixelWidth_um); }
            public int minorAxisLength_Pix { get => (int)(minorAxisLength_um / Parent.PixelWidth_um); }
            public float majorAxisLength_um { get; set; }
            public float minorAxisLength_um { get; set; }


            public MaskListEntry()
            {
                FOV = 0;
                WellLabel = "N0";
            }
        }

        //how these get formated into the table can also be changed later when input of funtion is decided
        public class MaskList : IEnumerable<MaskListEntry>
        {
            private List<MaskListEntry> _entries;
            public Dictionary<string, List<MaskListEntry>> WellFieldDict;
            public double PixelWidth_um { get; set; }
            public MaskList()
            {
                Init();
            }

            private void Init()
            {
                _entries = new List<MaskListEntry>();
                WellFieldDict = new Dictionary<string, List<MaskListEntry>>();
                //_FeatureNames = null;
            }

            public static char delimeter = '\t';

            private static Dictionary<MaskFeatureEnum, string> _FeatureNames;
            public static Dictionary<MaskFeatureEnum, string> FeatureNames
            {
                get
                {
                    if (_FeatureNames == null)
                    {
                        InitFeatures(); MaskIntensityType_Setup();
                    }
                    return _FeatureNames;
                }
            }

            public static void InitFeatures(int wv = 1)
            {
                Dictionary<MaskFeatureEnum, string> LU = new Dictionary<MaskFeatureEnum, string>();
                LU.Add(MaskFeatureEnum.PlateID, "PLATEID");
                LU.Add(MaskFeatureEnum.WellLabel, "WELL LABEL");
                LU.Add(MaskFeatureEnum.FOV, "FOV");
                LU.Add(MaskFeatureEnum.ObjectID, "OBJECT ID");
                LU.Add(MaskFeatureEnum.FileName, "IMAGENAME");
                LU.Add(MaskFeatureEnum.X_pix, "Nuclei Max left border wv".ToUpper() + wv); //Preferrably something in pixel coordinates
                LU.Add(MaskFeatureEnum.Y_pix, "Nuclei Max top border wv".ToUpper() + wv);
                LU.Add(MaskFeatureEnum.Width_pix, "Nuclei Max width wv".ToUpper() + wv);
                LU.Add(MaskFeatureEnum.Height_pix, "Nuclei Max height wv".ToUpper() + wv);
                LU.Add(MaskFeatureEnum.AxisAngle_Deg, "Nuclei Major Axis Angle wv".ToUpper() + wv);
                LU.Add(MaskFeatureEnum.MinorAxis_um, "Nuclei Minor Axis wv".ToUpper() + wv);
                LU.Add(MaskFeatureEnum.MajorAxis_um, "Nuclei Major Axis wv".ToUpper() + wv);
                // LU.Add(MaskFeatureEnum.Grayscale, "Greyscale".ToUpper());
                LU.Add(MaskFeatureEnum.Intensity, "Intensity".ToUpper());
                LU.Add(MaskFeatureEnum.Intensity_Method, "Intensity_Method".ToUpper());
                _FeatureNames = LU;
            }

            //---------------------------------------------------------------------------------

            public Dictionary<MaskFeatureEnum, int> FeatureIndex { get; set; }

            public static Dictionary<MaskIntensityType, string> _MaskIntensityTypeDict = null;

            public static void MaskIntensityType_Setup()
            {
                void Add(MaskIntensityType MIT)
                {
                    _MaskIntensityTypeDict.Add(MIT, Enum.GetName(typeof(MaskIntensityType), MIT).ToUpper());
                }

                _MaskIntensityTypeDict = new Dictionary<MaskIntensityType, string>();
                foreach (var val in Enum.GetValues(typeof(MaskIntensityType)))
                    Add((MaskIntensityType)val);
                //Add(MaskIntensityType.Binary);
                //Add(MaskIntensityType.Dither);
                //Add(MaskIntensityType.Binary);
                //Add(MaskIntensityType.Binary);
                //_MaskIntensityTypeDict.Add(MaskIntensityType.Dither, "BINARY");
                //_MaskIntensityTypeDict.Add(MaskIntensityType.Skeleton, "BINARY");
                //_MaskIntensityTypeDict.Add(MaskIntensityType.Grayscale, "BINARY");
            }

            public static bool CheckStringAgainst_IntensityType(string ToCheck, MaskIntensityType IntensityType)
            {
                if (_MaskIntensityTypeDict == null) return false;
                return ToCheck.Trim().ToUpper().StartsWith(_MaskIntensityTypeDict[IntensityType].Substring(0, 4));
            }

            //---------------------------------------------------------------------------------

            public MaskList(string FullPath, string PlateID = "", double pixel_width_um = 1)
            {
                Init();
                PixelWidth_um = pixel_width_um; int i;
                FileInfo FI = new FileInfo(FullPath); if (!FI.Exists) { INCELL_Folder.ErrorLog += "MaskList New Cant Find File"; return; }
                try
                {
                    string[] lines = File.ReadAllLines(FullPath);
                    INCELL_Folder.ErrorLog += lines == null ? "Empty Mask File" : "Lines.len " + lines.Length;
                    string[] Headers = lines[0].ToUpper().Split(delimeter);
                    SetupHeader_Lookup(Headers);
                    if (FeatureIndex.Count < 12) return;

                    string[] cols;
                    for (i = 1; i < lines.Length; i++)
                    {
                        cols = lines[i].Split(delimeter);
                        Add(cols, this, PlateID);
                    }
                }
                catch
                {
                    INCELL_Folder.ErrorLog += "Issue parsing mask.";
                }
                INCELL_Folder.ErrorLog += "\r\n Entries count " + _entries.Count + "\r\n";
            }

            private void SetupHeader_Lookup(string[] headers)
            {
                FeatureIndex = new Dictionary<MaskFeatureEnum, int>();
                Dictionary<string, int> HeaderDict = new Dictionary<string, int>(headers.Length);
                for (int i = 0; i < headers.Length; i++) HeaderDict.Add(headers[i], i);
                List<string> Missing = new List<string>();
                foreach (KeyValuePair<MaskFeatureEnum, string> KVP in FeatureNames)
                {
                    string Key = KVP.Value;
                    if (HeaderDict.ContainsKey(Key))
                        FeatureIndex.Add(KVP.Key, HeaderDict[Key]);
                    else
                    {
                        Key = KVP.Value.Replace("WV1", "WVH");
                        if (HeaderDict.ContainsKey(Key)) FeatureIndex.Add(KVP.Key, HeaderDict[Key]);
                        else
                            Missing.Add(KVP.Value);
                    }
                }
                INCELL_Folder.ErrorLog += Missing.Count > 0 ? ("\r\nFeatures missing: " + String.Join(",", Missing) + "\r\n") : "";
            }

            public void Add(MaskListEntry Entry)
            {
                //Add to the main list (we may not even need this one)
                _entries.Add(Entry);

                //Add to the Dictionary
                if (!WellFieldDict.ContainsKey(Entry.WellField)) WellFieldDict.Add(Entry.WellField, new List<MaskListEntry>());
                WellFieldDict[Entry.WellField].Add(Entry);
            }

            public void Add(string[] cols, MaskList Parent, string PlateID = "")
            {
                MaskListEntry Mask = new MaskListEntry();
                Mask.Parent = Parent;
                Mask.Cols = cols;

                Mask.Left_pix = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.X_pix]]);
                Mask.Top_pix = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Y_pix]]);
                Mask.Width_pix = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Width_pix]]);
                Mask.Height_pix = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Height_pix]]);
                Mask.majorAxisLength_um = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.MajorAxis_um]]);
                Mask.minorAxisLength_um = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.MinorAxis_um]]);
                Mask.axisAngle_deg = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.AxisAngle_Deg]]);
                Mask.FOV = int.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.FOV]]) - 1;
                Mask.WellLabel = cols[Parent.FeatureIndex[MaskFeatureEnum.WellLabel]];
                Mask.ObjectID = cols[Parent.FeatureIndex[MaskFeatureEnum.ObjectID]];
                Mask.FileName = cols[Parent.FeatureIndex[MaskFeatureEnum.FileName]];
                Mask.PlateID = cols[Parent.FeatureIndex[MaskFeatureEnum.PlateID]];
                if (Parent.FeatureIndex.ContainsKey(MaskFeatureEnum.Intensity_Method))
                {
                    //Mask.Grayscale = bool.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Grayscale]]);
                    Mask.Intensity = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Intensity]]);
                    Mask.Intensity_Method = (cols[Parent.FeatureIndex[MaskFeatureEnum.Intensity_Method]]).Trim().ToUpper();
                }

                //TODO TURN THIS BACK ON!
                //if (PlateID == "" || PlateID.ToUpper() == Mask.PlateID.ToUpper())

                Add(Mask);
                //else INCELL_Folder.ErrorLog += "\r\nNot Added because PlateID:" + PlateID + "\t" + Mask.PlateID;
            }



            public MaskListEntry this[int index]
            {
                get { return _entries[index]; }
            }

            public int Count { get => _entries.Count; }

            public IEnumerator<MaskListEntry> GetEnumerator()
            {
                return _entries.GetEnumerator();
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return _entries.GetEnumerator();
            }

            internal static List<XDCE_Image> XDCEImages_from_MaskList(XDCE_ImageGroup Plate, string FullPathMaskList)
            {
                MaskList ML = new MaskList(FullPathMaskList, Plate.PlateID, Plate.ParentFolder.XDCE.PixelWidth_in_um);
                INCELL_Folder.ErrorLog += "XDCEImages_from_MaskList " + ML.Count;
                List<XDCE_Image> XList = new List<XDCE_Image>();
                foreach (string Key in ML.WellFieldDict.Keys)
                {
                    XDCE_Image XI = Plate.GetImage_FromWellFOV(Key);
                    if (XI != null) XList.Add(XI);
                    else
                    {
                        INCELL_Folder.ErrorLog += "XDCEImages_from_MaskList Key UnMatched" + Key;
                    }
                }
                return XList;
            }


        }

        public enum MaskIntensityType
        {
            Binary = 0,
            Dither = 10,
            Skeleton = 15,
            Grayscale = 100
        }

        public enum MaskFeatureEnum
        {
            FileName = 0,
            PlateID = 1,
            WellLabel = 2,
            FOV = 3,
            X_pix = 4,
            Y_pix = 5,
            X_um = 6,
            Y_um = 7,
            Width_pix = 20,
            Height_pix = 21,
            AxisAngle_Deg = 8,
            MajorAxis_pix = 9,
            MinorAxis_pix = 10,
            MajorAxis_um = 11,
            MinorAxis_um = 12,
            ObjectID = 13,
            //make sure to double check this intensity
            Grayscale = 22,
            Intensity = 23,
            Intensity_Method = 24
        }
    }

    public class sCoordinateSet : IEnumerable<sCoordinate>
    {
        public List<sCoordinate> List;

        private Dictionary<Point, sCoordinate> _Dict;

        public float minX, maxX, minY, maxY;
        public int minC, maxC, minR, maxR;

        public Dictionary<Point, sCoordinate> Dict
        {
            get
            {
                //This is the rows and the columns that everything is inside
                if (_Dict == null)
                {
                    // Determine the maximum and minimum x and y values
                    minX = (int)this.Min(c => c.x);
                    maxX = (int)this.Max(c => c.x);
                    minY = (int)this.Min(c => c.y);
                    maxY = (int)this.Max(c => c.y);

                    //Make sorted set of rows and columns
                    var sX = new SortedDictionary<float, int>(); var sY = new SortedDictionary<float, int>();
                    foreach (var cor in this)
                    {
                        if (!sX.ContainsKey(cor.x)) sX.Add(cor.x, 0);
                        sX[cor.x]++;
                        if (!sY.ContainsKey(cor.y)) sY.Add(cor.y, 0);
                        sY[cor.y]++;
                    }

                    //Make a quick dictionary to store the row or column
                    var dRow = new Dictionary<float, int>();
                    var dCol = new Dictionary<float, int>();
                    int i;
                    i = 0; foreach (var kvp in sX) { dCol.Add(kvp.Key, i++); }
                    i = 0; foreach (var kvp in sY) { dRow.Add(kvp.Key, i++); }

                    minC = 0; maxC = dCol.Count;
                    minR = 0; maxR = dRow.Count;

                    //Now use those to assign row and column
                    _Dict = new Dictionary<Point, sCoordinate>();
                    foreach (var cor in this)
                    {
                        cor.row = dRow[cor.y];
                        cor.col = dCol[cor.x];
                        _Dict.Add(new Point(cor.row, cor.col), cor);
                    }
                }
                return _Dict;
            }
        }

        public sCoordinateSet()
        {
            List = new List<sCoordinate>();
        }

        public void Add(sCoordinate Cor)
        {
            _Dict = null; //Invalidates the dictionary
            List.Add(Cor);
        }

        public static sCoordinateSet CreateGrid(int Size)
        {
            var Cors = new sCoordinateSet();
            int s2 = (Size / 2); int Counter = 0;
            for (int r = 0; r < Size; r++)
            {
                for (int c = 0; c < Size; c++)
                {
                    Cors.Add(new sCoordinate()
                    {
                        FOV_20x = Counter++,
                        FOV_10x = -1,
                        //x = c - s2,
                        x = (r % 2 == 0) ? 1 + c - s2 : s2 - c,
                        y = r - s2
                    });
                }
            }
            return Cors;
        }

        private void AssignBinValues()
        {
            var coordinates = Dict;

            // Iterate through the coordinates and assign values to the "b" column
            int b = 0; bool bflip; int NewCount;
            for (int r = minR; r <= maxR; r += 2)
            {
                bflip = (int)((r - minR) / 2) % 2 == 0; // Reverse the direction of b on odd rows (but for every pair of rows)
                for (int c = bflip ? minC - 2 : maxC + 1; (bflip ? c < maxC + 2 : c >= minC - 1); c = bflip ? c + 2 : c - 2)
                {
                    NewCount = 0;
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            // Check if the current coordinate is in the set
                            var Key = new Point(r + i, c + j);
                            if (coordinates.ContainsKey(Key))
                            {
                                // Assign the current value of b to the coordinate
                                coordinates[Key].FOV_10x = b;
                                NewCount++;
                            }
                        }
                    }
                    if (NewCount > 0) b++;
                }
            }
        }


        public static void AssignBinValues2(List<sCoordinate> coordinates)
        {
            // Determine the number of rows and columns in the grid
            int rows = (int)Math.Sqrt(coordinates.Count);
            int cols = rows;

            // Iterate through the coordinates and assign values to the "b" column
            for (int i = 0; i < coordinates.Count; i++)
            {
                // Determine the row and column of the current coordinate
                int row = i / cols;
                int col = i % cols;

                // Assign a value to the "b" column based on the row and column
                if (row % 2 == 0)
                {
                    // For even rows, the "b" value increases as the column increases
                    coordinates[i].FOV_10x = row * (cols / 2) + (col / 2);
                }
                else
                {
                    // For odd rows, the "b" value decreases as the column increases
                    coordinates[i].FOV_10x = (row + 1) * (cols / 2) - (col / 2) - 1;
                }
            }
        }

        public static sCoordinateSet Load(string XMLPath)
        {
            sCoordinateSet sSet = new sCoordinateSet();
            string tAll = File.ReadAllText(XMLPath).ToUpper().Trim().Replace("\r\n", "").Replace(" ", "");
            string[] delims = new string[2] { "<POINT", "/>" };
            string[] spl = tAll.Split(delims, StringSplitOptions.RemoveEmptyEntries);
            sCoordinate sC; string[] points; int Counter = 0;
            for (int i = 0; i < spl.Length; i++)
            {
                if (!spl[i].StartsWith("X")) continue;
                points = spl[i].Split('\"');
                sC = new sCoordinate() { FOV_20x = Counter++, x = float.Parse(points[1]), y = float.Parse(points[3]) };
                sSet.Add(sC);
            }

            //Now that it is inished, we can assign the bins
            sSet.AssignBinValues();
            return sSet;
        }

        public string ToText()
        {
            char d = '\t';
            var sB = new StringBuilder();
            sB.Append("x" + d + "y" + d + "row" + d + "col" + d + "FOV 20x" + d + "FOV 10x" + "\r\n");
            foreach (var c in this)
            {
                string t = 
                    c.x.ToString() + d + c.y.ToString() + d +
                    c.row.ToString() + d + c.col.ToString() + d +
                    c.FOV_20x + d + c.FOV_10x.ToString() + "\r\n";
                sB.Append(t);
            }
            return sB.ToString();
        }

        public IEnumerator<sCoordinate> GetEnumerator()
        {
            return ((IEnumerable<sCoordinate>)List).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)List).GetEnumerator();
        }
    }

    public class sCoordinate
    {
        public int row;
        public int col;
        public float x;
        public float y;
        public int FOV_20x;
        public int FOV_10x;

        public override string ToString()
        {
            return "(" + x + "," + y + "," + FOV_10x + ")";
        }


    }

}
