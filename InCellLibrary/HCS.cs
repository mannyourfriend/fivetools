﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Drawing;
using FIVE.FOVtoRaftID;

namespace FIVE
{
    namespace InCellLibrary
    {
        public static class INCELL_Helper
        {
            public static void Post_INCARTA_Compilation()
            {
                string BaseFolder = @"\\storage1.ris.wustl.edu\wbuchser\Active\dB\Imaging\";
                BaseFolder = @"\\DESKTOP-8JQFC67\Imaging"; // @"C:\Temp\Imaging\"; 
                string ExportFolder = Path.Combine(@"C:\Temp\Imaging\", "Export013 Rafts.csv");
                INCELL_DB dB = new INCELL_DB(BaseFolder);

                //SELECT the approriate scans that you wish to compile
                int Subset = 1;
                switch (Subset)
                {
                    case 1: //163 - Triples: WT Cul4a PRIMPOL (1-4, 5-8, 9-12)
                        dB.CompileList_AFs = dB.AnalysisFolders.Where(x => (x.PlateID.Contains("163"))).ToList();
                        dB.CompileList_AFs = dB.CompileList_AFs.Where(x => (x.INCARTA_ProtocolName.Contains("Mito002"))).ToList();
                        dB.CompileList_AFs = dB.CompileList_AFs.Where(x => (x.ResultsID.Contains("Aug-20"))).ToList();
                        //dB.CompileList_AFs = dB.CompileList_AFs.GetRange(2, 3);
                        break;
                    case 2: //96 well plates with small BORG set
                        dB.CompileList_AFs = dB.AnalysisFolders.Where(x => (x.AQP == "WJB_96_20x0")).ToList();
                        dB.CompileList_AFs = dB.CompileList_AFs.Where(x => (x.INCARTA_ProtocolName.Contains("BORG"))).ToList();
                        dB.CompileList_AFs = dB.CompileList_AFs.Where(x => (!x.PlateID.Contains("146"))).ToList();
                        break;
                    case 3: //Compile Raft XDCE results
                        dB.CompileList_Folders = dB.Folders.Where(x => x.AQP.ToUpper().Contains("QUAD")).ToList();
                        XDCE.Export(dB.CompileList_Folders, ExportFolder);
                        return;
                    default:
                        dB.CompileList_AFs = dB.AnalysisFolders;
                        break;
                }

                //Setup the cell filters to get rid of cells or fields that aren't needed
                List<Cell_Filter> Filters = new List<Cell_Filter>(2)
                    { Cell_Filter.Default_FieldRestrict(80,120), Cell_Filter.Default_Object0Remove() };

                //TODO: Bring in the standard metadata format along with the files
                //Compile and Export
                INCARTA_Analysis CombinedAnalysis = new INCARTA_Analysis(dB.CompileList_AFs);
                CombinedAnalysis.ApplyFilters(Filters);
                CombinedAnalysis.Export(ExportFolder);
            }

            public static void CompileAnalyses(INCELL_DB dB, string ExportFolder, int MinCellsPerField, int MaxCellsPerField, List<INCARTA_Analysis_Folder> AFs, bool Rename_Wavelengths, System.ComponentModel.BackgroundWorker BW = null)
            {
                dB.CompileList_AFs = AFs;

                //Setup the cell filters to get rid of cells or fields that aren't needed
                List<Cell_Filter> Filters = new List<Cell_Filter>(2) { Cell_Filter.Default_FieldRestrict(MinCellsPerField, MaxCellsPerField), Cell_Filter.Default_Object0Remove() };

                //TODO: Bring in the standard metadata format along with the files
                bool FullVsPartial = false;
                string PathExport = Path.Combine(ExportFolder, AFs.First().PlateID + " compiled_" + DateTime.Now.ToString("hhmmss") + ".csv");
                if (FullVsPartial)
                {
                    //The disadvantage of this approach is that it really fills up memory
                    //Compile and Export
                    var CombinedAnalysis = new INCARTA_Analysis(dB.CompileList_AFs, BW);
                    CombinedAnalysis.ApplyFilters(Filters);
                    if (BW != null) BW.ReportProgress(0, "Exporting..");
                    CombinedAnalysis.Export(PathExport);
                }
                else
                {
                    //This approach is less memory hungry
                    INCARTA_Analysis.CompileExport(dB.CompileList_AFs, PathExport, Rename_Wavelengths, BW);
                }
            }
        }

        public class xI_RaftInfo
        {
            public FOVtoRaftID.ReturnRaft RR;
            public string RaftID { get => RR.RaftID; }
            public double FullX;
            public double FullY;
            public PointF FracXY;

            public xI_RaftInfo(FOVtoRaftID.ReturnRaft rr, double fullX, double fullY, PointF fracXY)
            {
                RR = rr;
                FullX = fullX;
                FullY = fullY;
                FracXY = fracXY;
            }
        }

        public class INCELL_MeasuredOffsets
        {
            //Metadata
            public string InstrumentName;
            public string InstrumentLocation;
            public string Objective;
            public string ScanPattern;

            //Measured settings
            public double Overlap_x_um { get; set; }
            public double Overlap_y_um { get; set; }
            public double AcrossXum_MovingMultiplier_Y { get; set; }
            public double AcrossYum_MovingMultiplier_X { get; set; }

            public static double StartingShiftX = 10000;
            public static double StartingShiftY = 10000;

            public double Transform_x(double ImageWidth_um, double PlateX_um, double PlateY_um)
            {
                return (PlateX_um * (ImageWidth_um + Overlap_x_um) / ImageWidth_um) + ((PlateY_um - StartingShiftX) * AcrossYum_MovingMultiplier_X);
            }

            public double Transform_y(double ImageHeight_um, double PlateX_um, double PlateY_um)
            {
                return (PlateY_um * (ImageHeight_um + Overlap_y_um) / ImageHeight_um) + ((PlateX_um - StartingShiftX) * AcrossXum_MovingMultiplier_Y);
            }

            public INCELL_MeasuredOffsets()
            {
                InstrumentName = "";
                InstrumentLocation = "";
                Objective = "";
                ScanPattern = "";
                Overlap_x_um = 0;
                Overlap_y_um = 0;
                AcrossYum_MovingMultiplier_X = 0;
                AcrossXum_MovingMultiplier_Y = 0;
            }

            public static INCELL_MeasuredOffsets WorseForTest()
            {
                var IMO = new INCELL_MeasuredOffsets()
                {
                    InstrumentName = "InCell 6500HS",
                    InstrumentLocation = "Room 33xx 4444 Forest Park Ave",
                    Objective = "20x 0.45NA",
                    ScanPattern = "Horizontal Serpentine",
                    Overlap_x_um = -46.3,
                    Overlap_y_um = -33.1,
                    AcrossYum_MovingMultiplier_X = 0,
                    AcrossXum_MovingMultiplier_Y = 0.02
                };
                return IMO;
            }

            public static INCELL_MeasuredOffsets Default4444()
            {
                var IMO = new INCELL_MeasuredOffsets()
                {
                    InstrumentName = "InCell 6500HS",
                    InstrumentLocation = "Room 33xx 4444 Forest Park Ave",
                    Objective = "20x 0.45NA",
                    ScanPattern = "Horizontal Serpentine",
                    Overlap_x_um = -6.3,
                    Overlap_y_um = -3.1,
                    AcrossYum_MovingMultiplier_X = 0,
                    AcrossXum_MovingMultiplier_Y = 0.01
                };
                return IMO;
            }
        }

        public class XDCE_Image
        {
            public static string ColName_AdjacentFocusAvg = "ADJACENT_FOCUS";

            public Dictionary<string, object> KVPs;

            public Dictionary<string, xI_RaftInfo> Rafts = new Dictionary<string, xI_RaftInfo>();
            public string RaftCalCode { get; set; }
            public XDCE Parent { get; set; }
            public string PlateID { get => Parent.NameString; }
            public override string ToString() { return FileName; }

            public static Dictionary<string, object> XML_Extract_KVPs(string XML)
            {
                if (XML.StartsWith("<?XML"))
                {
                    XML = XML.Replace("<?XML VERSION=\"1.0\" ENCODING=\"ISO-8859-1\" STANDALONE=\"NO\"?>", "");
                    XML += "</" + (XML.Contains("<FIELDOFFSETS") ? "FIELDOFFSETS" : "POINTLIST") + "></ACQUISITIONMODE></PLATEMAP></AUTOLEADACQUISITIONPROTOCOL></IMAGESTACK>";
                    XML = XML.Replace("&", "");
                }
                XDocument doc = XDocument.Parse(XML);
                Dictionary<string, object> KVPs = new Dictionary<string, object>();
                foreach (XElement element in doc.Descendants()/*.Where(p => p.HasElements == false)*/)
                {
                    int keyInt = 0; string keyName = element.Name.LocalName;
                    if (element.HasAttributes)
                    {
                        foreach (XAttribute attribute in element.Attributes())
                        {
                            string Key2 = keyName + "_" + attribute.Name;
                            while (KVPs.ContainsKey(Key2)) Key2 = keyName + "_" + attribute.Name + "_" + keyInt++;
                            KVPs.Add(Key2, attribute.Value);
                        }
                    }
                    if (element.Value != null)
                        if (element.Value != "")
                        {
                            while (KVPs.ContainsKey(keyName)) keyName = element.Name.LocalName + "_" + keyInt++;
                            KVPs.Add(keyName, element.Value);
                        }
                }
                return KVPs;
            }

            /// <summary>
            /// Returns the full Plate Coordinates in Microns for a pixel location on this Image expressed in fractions
            /// </summary>
            public void PlateCoordinates_FromFraction(double x, double y, out double fullX, out double fullY)
            {
                fullX = 0; fullY = 0;
                PlateCoordinates_FromPixel((int)(x * (double)Parent.ImageWidth_in_Pixels), (int)(y * (double)Parent.ImageHeight_in_Pixels), out fullX, out fullY);
            }

            public void PlateCoordinates_FromPixel(int x, int y, out double fullX, out double fullY)
            {
                fullX = PlateX_um + Parent.PixelWidth_in_um * x;
                fullY = PlateY_um + Parent.PixelHeight_in_um * y;
            }

            public PointF Fraction_FromPlateCoordinates(PointF PlateCors)
            {
                var pPixel = Pixel_FromPlateCoordinates(PlateCors);
                return new PointF(pPixel.X / Parent.ImageWidth_in_Pixels, pPixel.Y / Parent.ImageHeight_in_Pixels);
            }

            public PointF Pixel_FromPlateCoordinates(PointF PlateCors)
            {
                PointF p = new PointF(
                    (float)((PlateCors.X - PlateX_um) / Parent.PixelWidth_in_um),
                    (float)((PlateCors.Y - PlateY_um) / Parent.PixelHeight_in_um));
                return p;
            }

            public XDCE_Image()
            {

            }

            public XDCE_Image(string XML, int index, XDCE Parent)
            {
                this.Parent = Parent; _Index = index;
                KVPs = XML_Extract_KVPs(XML);
                //Debug.Print(KVPs["WELL_LABEL"].ToString() + "\t" + KVPs["PLATEPOSITION_UM_X"].ToString() + "\t" + KVPs["PLATEPOSITION_UM_Y"].ToString());
            }

            public XDCE_Image(string filePath, XDCE xDCEParent, string WellLabel, int FOV_Param, short wavelength)
            {
                KVPs = new Dictionary<string, object>();
                Parent = xDCEParent;
                _FileName = Path.GetFileName(filePath);
                _Index = FOV_Param;
                _FOV = FOV_Param; _ColNumber = 0; _RowNumber = FOV_Param;
                _wavelength = wavelength;
                _PlateX = _RowNumber * 1000; _PlateY = 0; //Just so there are coordinates
                this.WellLabel = WellLabel;
            }

            private double _x = double.NaN;
            public double x { get { if (double.IsNaN(_x)) _x = double.Parse((string)KVPs["OFFSETFROMWELLCENTER_X"]); return _x; } }

            private double _y = double.NaN;
            public double y { get { if (double.IsNaN(_y)) _y = double.Parse((string)KVPs["OFFSETFROMWELLCENTER_Y"]); return _y; } }

            private double _z = double.NaN;
            public double z { get { if (double.IsNaN(_z)) _z = double.Parse((string)KVPs["FOCUSPOSITION_Z"]); return _z; } }
            private double _z_AvgAdj = double.NaN;
            public double z_AvgAdjacent
            {
                get
                {
                    if (double.IsNaN(_z_AvgAdj))
                    {
                        if (!KVPs.ContainsKey(ColName_AdjacentFocusAvg)) this.Parent.SetupFocusDeviation();
                        _z_AvgAdj = (double)KVPs[ColName_AdjacentFocusAvg];
                    }
                    return _z_AvgAdj;
                }
            }

            private readonly static string platePosBase = "PLATEPOSITION_UM_";
            private readonly static string plposNameX = platePosBase + "X", plposNameY = platePosBase + "Y";
            /// <summary>
            /// Used to reset pre-calculated plateX and plateY when the measured offsets change
            /// </summary>
            internal void RecalcPlateCoordinates()
            {
                if (!KVPs.ContainsKey(plposNameX) || !KVPs.ContainsKey(plposNameY))
                {
                    _PlateX = -1; _PlateY = -1; return;
                }
                var rawX = double.Parse((string)KVPs[plposNameX]);
                var rawY = double.Parse((string)KVPs[plposNameY]);

                _PlateX = Parent.MeasuredOffsets.Transform_x(Width_microns, rawX, rawY);
                _PlateY = Parent.MeasuredOffsets.Transform_y(Height_microns, rawX, rawY);
            }

            private double _PlateX = double.NaN;
            public double PlateX_um { get { if (double.IsNaN(_PlateX)) RecalcPlateCoordinates(); return _PlateX; } }

            private double _PlateY = double.NaN;
            public double PlateY_um { get { if (double.IsNaN(_PlateY)) RecalcPlateCoordinates(); return _PlateY; } }
            public PointF PlateXY_um { get => new PointF((float)PlateX_um, (float)PlateY_um); }
            public PointF PlateXY_Center_um { get => new PointF((float)PlateX_um + ((float)Width_microns / 2), (float)PlateY_um + ((float)Height_microns / 2)); }

            private int _wavelength = int.MinValue;
            public int wavelength
            {
                get { if (_wavelength == int.MinValue) _wavelength = int.Parse((string)KVPs["IDENTIFIER_WAVE_INDEX"]); return _wavelength; }
            }

            public int Width_Pixels { get => Parent.ImageWidth_in_Pixels; }
            public int Height_Pixels { get => Parent.ImageWidth_in_Pixels; }
            public double Width_microns { get => Parent.ImageWidth_in_Pixels * Parent.PixelWidth_in_um; }
            public double Height_microns { get => Parent.ImageHeight_in_Pixels * Parent.PixelHeight_in_um; }

            private int _FOV = -12;
            public int FOV
            {
                get { if (_FOV == -12) _FOV = int.Parse((string)KVPs["IDENTIFIER_FIELD_INDEX"]); return _FOV; }
            }

            public string FullPath { get => Path.Combine(Parent.FolderPath, FileName); }

            private string _FileName = "";
            public string FileName
            {
                get
                {
                    if (_FileName == "") _FileName = (string)KVPs["IMAGE_FILENAME"];
                    return _FileName;
                }
            }

            public string FileName_GenImageBase { get => FileName_GenImageBaseWV(wavelength); }
            public string FileName_GenImageBaseWV(int wvUse)
            { return "" + Well_Row + "-" + Well_Column + "(F" + (FOV + 1) + " Z0 T0 W" + wvUse + ")"; } //added this 12/6/2020 to account for the gen images using different wavelengths than the one we might want to overlay from

            private int _Index;
            public int Index { get => _Index; }

            private int _RowNumber = -1;
            /// <summary>
            /// The zero-indexed row number of the well that the image is taken from (A = 0, B = 1, etc.)
            /// </summary>
            public int RowNumber
            {
                get
                {
                    if (_RowNumber == -1) _RowNumber = int.Parse((string)KVPs["ROW_NUMBER"]) - 1;
                    return _RowNumber;
                }
            }

            private int _ColNumber = -1;
            /// <summary>
            /// The zero-indexed column number of the well that the image is taken from
            /// </summary>
            public int ColNumber
            {
                get
                {
                    if (_ColNumber == -1) _ColNumber = int.Parse((string)KVPs["COLUMN_NUMBER"]) - 1;
                    return _ColNumber;
                }
            }

            public string Well_Row { get => ((char)(RowNumber + 1 + 64)).ToString(); } //Since RowNumber is 0 indexed (adjusted 12/6/2020)
            public int Well_Column { get => ColNumber + 1; } //Since ColNumber is 0 indexed (adjusted 12/6/2020)

            private const string Wellsz = "WELL_LABEL";
            public string WellLabel
            {
                get => KVPs.ContainsKey(Wellsz) ? KVPs[Wellsz].ToString() : "X - 0"; set
                {
                    if (!KVPs.ContainsKey(Wellsz)) KVPs.Add(Wellsz, value);
                    else KVPs[Wellsz] = value;
                }
            }

            public static string WellFieldKey(string WellLabel, int FOV)
            {
                return WellLabel + "." + (FOV + 1);
            }

            public static string WellFieldKey(string WellLabel, string FOV)
            {
                return WellFieldKey(WellLabel, int.Parse(FOV) - 1);
            }

            public string WellField
            {
                get => WellFieldKey(WellLabel, FOV);
            }
            public PointF CenterPixel { get => new PointF(Width_Pixels / 2, Height_Pixels / 2); }

            public string MaskFileName { get => Path.GetFileNameWithoutExtension(FileName).ToUpper() + ".TIF"; } //They are now usually 1-bit TIFFs
            public string MaskFullPath { get => Path.Combine(Parent.MaskFolder, MaskFileName); }

            public string MaskShiftedName { get => Path.GetFileNameWithoutExtension(FileName).ToUpper() + "_SHIFT.TIF"; }

            public string MaskShiftedFullPath { get => Path.Combine(Parent.MaskFolder, MaskShiftedName); }
            public bool HasMask
            {
                get
                {
                    FileInfo FI = new FileInfo(MaskFullPath);
                    return FI.Exists;
                }
            }

            internal static List<string> KVPHeaderOrder = new List<string>();

            public string Export(bool ExportHeader)
            {
                char delim = '\t';
                StringBuilder SB = new StringBuilder(); string res;
                SB.Append((ExportHeader ? "PlateID" : PlateID) + delim);

                if (KVPHeaderOrder.Count == 0) { foreach (KeyValuePair<string, object> KVP in KVPs) KVPHeaderOrder.Add(KVP.Key); }
                foreach (string Header in KVPHeaderOrder)
                {
                    res = KVPs.ContainsKey(Header) ? KVPs[Header].ToString() : "";
                    SB.Append((ExportHeader ? Header : res) + delim);
                    //SB.Append((ExportHeader ? KVP.Key : KVP.Value.ToString()) + delim);
                }
                XDCE_ImageGroup Well = Parent.Wells[WellLabel];
                SB.Append((ExportHeader ? "RaftCal" : Well.HasRaftCalibration.ToString()) + delim);
                SB.Append((ExportHeader ? "ImageCheck" : Well.HasImageCheck.ToString()) + delim);
                return SB.ToString();
            }

            internal bool ContainsPlateCoordinate(double plateX, double plateY)
            {
                //Check to see if this plate coordinate is inside the image
                if ((plateX >= this.PlateX_um) && (plateX <= (this.PlateX_um + this.Width_microns)))
                {
                    if ((plateY >= this.PlateY_um) && (plateY <= (this.PlateY_um + this.Height_microns)))
                    {
                        return true;
                    }
                }
                return false;
            }

            public PointF PlateCoordinates_um_InCenterOfRectangle(RectangleF FractionalRectangleOfThisImageInPixelCoordinates)
            {
                SizeF umPerPixelMultiplier = new SizeF(1, 1);
                return PlateCoordinates_um_InCenterOfRectangle(FractionalRectangleOfThisImageInPixelCoordinates, umPerPixelMultiplier);
            }

            public PointF PlateCoordinates_um_InCenterOfRectangle(RectangleF FractionalRectangleOfThisImageInPixelCoordinates, SizeF umPerPixelMultiplier)
            {
                RectangleF R = FractionalRectangleOfThisImageInPixelCoordinates;
                PointF Middle = new PointF(R.X + (R.Width / 2), R.Y + (R.Height / 2));
                return new PointF(
                    (float)(PlateX_um + Parent.PixelWidth_in_um * Middle.X / umPerPixelMultiplier.Width),
                    (float)(PlateY_um + Parent.PixelWidth_in_um * Middle.Y / umPerPixelMultiplier.Height));
            }

            public void RefreshRafts_Old(XDCE_ImageGroup xParent, Dictionary<string, XDCE_ImageGroup> ParentRaftsInternal, int grid = -1)
            {
                FOVtoRaftID.ReturnRaft RR; double fullX, fullY; double fracX, fracY;

                //An alternative and possible faster way is to find the coordinates on the edge, then go from their

                this.RaftCalCode = xParent.CalibrationRaftSettings.CalCode;

                //First get a nice set of Raft Points near the center of the rafts
                var RaftWithin = new Dictionary<string, Tuple<FOVtoRaftID.ReturnRaft, double, double, PointF>>();
                if (grid < 1) grid = 14;
                //grid = 14;
                double MaxDistToEdge = 0;
                for (int x = 0; x < grid; x++)
                {
                    for (int y = 0; y < grid; y++)
                    {
                        //Just so we don't add it twice for one image
                        fracX = (double)x / grid; fracY = (double)y / grid;
                        this.PlateCoordinates_FromFraction(fracX, fracY, out fullX, out fullY);
                        RR = xParent.CalibrationRaftSettings.FindRaftID_RR(fullX, fullY);
                        if (RaftWithin.ContainsKey(RR.RaftID))
                            if (RaftWithin[RR.RaftID].Item1.MinDistToEdge > RR.MinDistToEdge) continue;
                            else RaftWithin.Remove(RR.RaftID);
                        RaftWithin.Add(RR.RaftID, new Tuple<FOVtoRaftID.ReturnRaft, double, double, PointF>(RR, fullX, fullY, new PointF((float)fracX, (float)fracY)));
                        if (RR.MinDistToEdge > MaxDistToEdge) MaxDistToEdge = RR.MinDistToEdge;
                    }
                }
                foreach (var R2 in RaftWithin.Values)
                {
                    if (ParentRaftsInternal != null)
                    {
                        if (!ParentRaftsInternal.ContainsKey(R2.Item1.RaftID))
                        {
                            XDCE_ImageGroup SIG = new XDCE_ImageGroup();
                            SIG.FilePath = xParent.FilePath; SIG.Level = "Rafts";
                            SIG.NameAtLevel = R2.Item1.RaftID;
                            SIG.ParentFolder = xParent.ParentFolder;
                            ParentRaftsInternal.Add(R2.Item1.RaftID, SIG);
                        }
                        ParentRaftsInternal[R2.Item1.RaftID].Add(this);
                    }
                    var xi_RI = new xI_RaftInfo(R2.Item1, R2.Item2, R2.Item3, R2.Item4);
                    //Img.Rafts.Add(R2.Item1.RaftID,  new Tuple<double, double, float, float>(R2.Item2, R2.Item3, R2.Item4.X, R2.Item4.Y)); //Record the position of this raft in the image itself
                    if (!this.Rafts.ContainsKey(xi_RI.RaftID))
                        this.Rafts.Add(xi_RI.RaftID, xi_RI); //Updated 5/24/2022
                }
            }

            public void RefreshRafts(XDCE_ImageGroup xParent, Dictionary<string, XDCE_ImageGroup> ParentRaftsInternal)
            {   //Updates 2023/03/22 by Willie
                ReturnRaft RR;

                // Local function to get the RR object
                (ReturnRaft RR, int row, int col) GetRRAndIndices(double fracXa, double fracYa)
                {
                    this.PlateCoordinates_FromFraction(fracXa, fracYa, out double fullXa, out double fullYa);
                    var RRa = xParent.CalibrationRaftSettings.FindRaftID_RR(fullXa, fullYa);
                    return (RRa, RRa.RaftRowIdx, RRa.RaftColIdx);
                }

                // Get RR objects and row and column indices for each corner combination
                var (RR_UpperLeft, upperLeftRow, upperLeftCol) = GetRRAndIndices(0, 0);
                var (RR_UpperRight, _, upperRightCol) = GetRRAndIndices(0, 1);
                var (RR_LowerLeft, lowerLeftRow, _) = GetRRAndIndices(1, 0);

                int numRaftsBetween = Math.Abs(upperRightCol - upperLeftCol) + 1;
                int numRaftsDown = Math.Abs(lowerLeftRow - upperLeftRow) + 1;

                // Get the center coordinates of the corner rafts
                PointF upperLeftCenter = RR_UpperLeft.RaftCenterFull;
                PointF upperRightCenter = RR_UpperRight.RaftCenterFull;
                PointF lowerLeftCenter = RR_LowerLeft.RaftCenterFull;

                List<float> centerYList = new List<float>(); // Create a list of center X, Y coordinates for the row,col
                List<float> centerXList = new List<float>();
                for (int i = 0; i < numRaftsBetween; i++)
                {
                    float centerY = upperLeftCenter.Y + i * (upperRightCenter.Y - upperLeftCenter.Y) / (numRaftsBetween - 1);
                    centerYList.Add(centerY);
                }
                for (int i = 0; i < numRaftsDown; i++)
                {
                    float centerX = upperLeftCenter.X + i * (lowerLeftCenter.X - upperLeftCenter.X) / (numRaftsDown - 1);
                    centerXList.Add(centerX);
                }

                this.RaftCalCode = xParent.CalibrationRaftSettings.CalCode;
                var RaftWithin = new Dictionary<string, Tuple<FOVtoRaftID.ReturnRaft, double, double, PointF>>();
                foreach (var xCenter in centerXList)
                {
                    foreach (var yCenter in centerYList)
                    {
                        RR = xParent.CalibrationRaftSettings.FindRaftID_RR(xCenter, yCenter);
                        var P1 = this.Fraction_FromPlateCoordinates(new PointF(xCenter, yCenter));
                        //var P2 = new PointF((float)(xCenter - upperLeftCenter.X) / (lowerLeftCenter.X - upperLeftCenter.X),(float)(upperRightCenter.Y - yCenter) / (upperRightCenter.Y - upperLeftCenter.Y));
                        RaftWithin.Add(RR.RaftID, new Tuple<FOVtoRaftID.ReturnRaft, double, double, PointF>(RR, xCenter, yCenter, P1));
                    }
                }
                foreach (var R2 in RaftWithin.Values)
                {
                    if (ParentRaftsInternal != null)
                    {
                        if (!ParentRaftsInternal.ContainsKey(R2.Item1.RaftID))
                        {
                            var SIG = new XDCE_ImageGroup();
                            SIG.FilePath = xParent.FilePath; SIG.Level = "Rafts";
                            SIG.NameAtLevel = R2.Item1.RaftID;
                            SIG.ParentFolder = xParent.ParentFolder;
                            ParentRaftsInternal.Add(R2.Item1.RaftID, SIG);
                        }
                        ParentRaftsInternal[R2.Item1.RaftID].Add(this);
                    }
                    var xi_RI = new xI_RaftInfo(R2.Item1, R2.Item2, R2.Item3, R2.Item4);
                    if (!this.Rafts.ContainsKey(xi_RI.RaftID)) this.Rafts.Add(xi_RI.RaftID, xi_RI); //Updated 5/24/2022
                }
            }


        }

        public class XDCE_ImageGroup
        {
            public XDCE_ImageGroup()
            {

            }

            public string FolderPath = "";
            public string FilePath = ""; //XDCE Path
            public string Level = "";
            public string NameAtLevel = "";

            [XmlIgnore]
            public INCELL_Folder ParentFolder { get; set; }

            protected List<XDCE_Image> _Images;
            [XmlIgnore]
            public virtual List<XDCE_Image> Images { get { return _Images; } set { _Images = value; Dirty = true; } }

            public void Add(XDCE_Image Img)
            {
                if (_Images == null) _Images = new List<XDCE_Image>();
                _Images.Add(Img);
                MakeDirty();
            }

            private static Random _Rand = new Random();

            /// <summary>
            /// Picks out a random set of FOV #s from this plate's set of fields
            /// </summary>
            /// <param name="FOV_Count"></param>
            /// <returns></returns>
            public List<int> RandomFields(int FOV_Count, bool UseMaskFields = false)
            {
                XDCE_ImageGroup Fields = this;
                HashSet<int> FOVs_Picked = new HashSet<int>();
                HashSet<int> MaskFieldsHash = new HashSet<int>();
                if (UseMaskFields) MaskFieldsHash = new HashSet<int>(MaskFields.Select(x => x.FOV));
                int MaxFields = Math.Min(Fields.FOV_Max - Fields.FOV_Min, UseMaskFields ? MaskFields.Count : Fields.FOV_Max);
                while (FOVs_Picked.Count < Math.Min(FOV_Count, MaxFields))
                {
                    int FOV = _Rand.Next(Fields.FOV_Min, Fields.FOV_Max);
                    if (UseMaskFields)
                    {
                        if (!MaskFieldsHash.Contains(FOV)) continue;
                    }
                    FOVs_Picked.Add(FOV);
                }
                return FOVs_Picked.ToList();
            }

            public string PlateID { get => ParentFolder.PlateID; }

            public XDCE_ImageGroup WellPrevious(XDCE_ImageGroup Parent, string PlateID)
            {
                List<string> WellList = Parent.Wells.Keys.ToList();
                int idx = WellList.IndexOf(this.NameAtLevel);
                if (idx > 0) idx--;
                return Parent.Wells[WellList[idx]];
            }

            public XDCE_ImageGroup WellNext(XDCE_ImageGroup Parent, string PlateID)
            {
                List<string> WellList = Parent.Wells.Keys.ToList();
                int idx = WellList.IndexOf(this.NameAtLevel);
                if (idx < WellList.Count - 1) idx++;
                return Parent.Wells[WellList[idx]];
            }

            public void Refresh_Wells_Rafts(int grid = 21)
            {
                if (DirtyWells)
                {
                    _Wells = new Dictionary<string, XDCE_ImageGroup>();
                    _Rafts = new Dictionary<string, XDCE_ImageGroup>();
                    _ImageByName = new Dictionary<string, XDCE_Image>();
                    PathSearchMasks = Path.Combine(Path.GetDirectoryName(FilePath), INCELL_Folder.DefaultMaskFolderName, INCELL_Folder.DefaultMaskListName);
                    INCELL_Folder.ErrorLog = PathSearchMasks + "\r\n";
                    _MaskFields = Masks.MaskList.XDCEImages_from_MaskList(this, PathSearchMasks);
                    INCELL_Folder.ErrorLog += "\r\nMaskFields Count = " + _MaskFields.Count + "\r\n";
                    _MaskWells = _MaskFields.Select(x => x.WellLabel).Distinct().ToList();
                    if (Images == null) return;
                    foreach (XDCE_Image Img in Images)
                    {
                        if (!_Wells.ContainsKey(Img.WellLabel))
                        {
                            XDCE_ImageGroup SIG = new XDCE_ImageGroup();
                            SIG.FilePath = this.FilePath; SIG.Level = "Wells";
                            SIG.NameAtLevel = Img.WellLabel;
                            SIG.ParentFolder = ParentFolder;
                            _Wells.Add(Img.WellLabel, SIG);
                        }
                        _Wells[Img.WellLabel].Add(Img);
                        _ImageByName.Add(Path.GetFileNameWithoutExtension(Img.FileName.ToUpper()), Img);
                    }
                    DirtyWells = false; //This was set to true 2/23/2021
                }
                if (HasRaftCalibration)
                {
                    if (DirtyRafts)
                    {
                        //100x100 rafts have about 6 rafts per FOV, so that is why this is set to 21, we want multiple points so we can have one that is near the center
                        //int test = 0;
                        //grid = 5;
                        foreach (XDCE_Image Img in Images)
                        {
                            Img.RefreshRafts(this, _Rafts);
                            //if (test++ > 95) break;
                        }
                        DirtyRafts = false; //21=400, 
                    }
                }
            }

            protected Dictionary<string, XDCE_ImageGroup> _Wells;
            [XmlIgnore]
            public Dictionary<string, XDCE_ImageGroup> Wells
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _Wells;
                }
            }

            protected Dictionary<string, XDCE_ImageGroup> _Rafts;
            [XmlIgnore]
            public Dictionary<string, XDCE_ImageGroup> Rafts
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _Rafts;
                }
            }

            public int Row_Count { get => Y_to_Index.Count; }
            public int Col_Count { get => X_to_Index.Count; }

            protected int _FOV_Min;
            public int FOV_Min { get { Check_XDCEImageGroup(); return _FOV_Min; } }
            protected int _FOV_Max;
            public int FOV_Max { get { Check_XDCEImageGroup(); return _FOV_Max; } }

            protected ConcurrentDictionary<string, XDCE_ImageGroup> _WellFOV_Dict;

            protected Dictionary<string, List<XDCE_Image>> _WellFOV_to_Index;
            [XmlIgnore]
            public Dictionary<string, List<XDCE_Image>> WellFOV_to_Index { get { Check_XDCEImageGroup(); return _WellFOV_to_Index; } }

            protected Dictionary<double, int> _X_to_Index;
            protected Dictionary<double, int> _Y_to_Index;
            [XmlIgnore]
            public Dictionary<double, int> X_to_Index { get { Check_XDCEImageGroup(); return _X_to_Index; } }
            [XmlIgnore]
            public Dictionary<double, int> Y_to_Index { get { Check_XDCEImageGroup(); return _Y_to_Index; } }

            protected Dictionary<Tuple<int, int>, List<XDCE_Image>> _ImageByIndices;

            protected Dictionary<string, XDCE_Image> _ImageByName;
            [XmlIgnore]
            public Dictionary<string, XDCE_Image> ImageByName
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _ImageByName;
                }
            }

            public int Count { get { Check_XDCEImageGroup(); return Images.Count; } }

            protected void MakeDirty()
            {
                Dirty = DirtyWells = DirtyRafts = true;
            }

            protected bool Dirty = true;
            protected bool DirtyWells = true;
            protected bool DirtyRafts = true;

            public XDCE_Image GetImage(string FileName)
            {
                string Key = Path.GetFileNameWithoutExtension(FileName.ToUpper());
                if (ImageByName.ContainsKey(Key)) return ImageByName[Key];
                return null;
            }
            public XDCE_Image GetImage(int Row, int Col, short WavelenthIndex)
            {
                Check_XDCEImageGroup();
                Tuple<int, int> Key = new Tuple<int, int>(Col, Row);
                if (!_ImageByIndices.ContainsKey(Key)) return null;
                List<XDCE_Image> Imgs = _ImageByIndices[Key];
                if (WavelenthIndex < Imgs.Count) return Imgs[WavelenthIndex];
                return null;
            }

            public XDCE_Image GetImage_FromWellFOV(string Well_Field)
            {
                List<XDCE_Image> xIs = GetImages_FromWellFOV(Well_Field);
                if (xIs == null) return null;
                return xIs[0];
            }

            public List<XDCE_Image> GetImages_FromWellFOV(string Well_Field)
            {
                Check_XDCEImageGroup();
                if (!WellFOV_to_Index.ContainsKey(Well_Field)) return null;
                List<XDCE_Image> xImages = WellFOV_to_Index[Well_Field];
                return xImages;
            }

            internal List<XDCE_Image> GetAdjacent(int FOVIndex_x, int FOVIndex_y, int Offset_x, int Offset_y)
            {
                Tuple<int, int> Key = new Tuple<int, int>(FOVIndex_x + Offset_x, FOVIndex_y + Offset_y);
                if (!_ImageByIndices.ContainsKey(Key)) return new List<XDCE_Image>();
                return _ImageByIndices[Key];
            }

            protected short _Wavelength_Count;
            [XmlIgnore]
            public short Wavelength_Count
            {
                get
                {
                    Check_XDCEImageGroup();
                    return _Wavelength_Count;
                }
                set { _Wavelength_Count = value; }
            }

            public IEnumerable<string> WellFOV_Set { get => WellFOV_to_Index.Keys; }

            [XmlIgnore]
            public ConcurrentDictionary<string, XDCE_ImageGroup> WellFOV_Dict { get { Check_XDCEImageGroup(); return _WellFOV_Dict; } }

            private void _FillOutXYGrid()
            {
                //Since the different snake patterns can change how the images are loaded, we want to ensure the grid matches up with the _X/_Y to index ahead of time
                SortedList<double, int> x = new SortedList<double, int>(); SortedList<double, int> y = new SortedList<double, int>();
                foreach (XDCE_Image Img in _Images)
                {
                    if (!x.ContainsKey(Img.PlateX_um)) x.Add(Img.PlateX_um, 1);
                    if (!y.ContainsKey(Img.PlateY_um)) y.Add(Img.PlateY_um, 1);
                }
                for (int i = 0; i < x.Count; i++) x[x.Keys[i]] = i;
                for (int i = 0; i < y.Count; i++) y[y.Keys[i]] = i;
                _X_to_Index = new Dictionary<double, int>(x); _Y_to_Index = new Dictionary<double, int>(y);
            }

            private class InvertedComparer : IComparer<double>
            {
                public int Compare(double x, double y) { return y.CompareTo(x); }
            }

            protected void Check_XDCEImageGroup()
            {
                if (!Dirty) return;
                if (_Images == null) Refresh_Wells_Rafts(); //Added 8/23/2021
                if (_Images == null) return; //8/23/2021
                if (ParentFolder.Type_NoXDCE & _Images.Count == 0 && Wells.Count > 0) _Images = Wells.First().Value.Images; //This is mostly for the case where there is no XDCE
                _FillOutXYGrid(); //This ensures that the pattern of the scan and the grid are setup correctly
                _WellFOV_to_Index = new Dictionary<string, List<XDCE_Image>>();
                _WellFOV_Dict = new ConcurrentDictionary<string, XDCE_ImageGroup>(); //Ideally we will only keep this one and get rid of the Well_FOV_toindex one . . 
                _ImageByIndices = new Dictionary<Tuple<int, int>, List<XDCE_Image>>();
                _Wavelength_Count = 0; _FOV_Max = int.MinValue; _FOV_Min = int.MaxValue;

                foreach (XDCE_Image Image in _Images)
                {
                    Tuple<int, int> Key = new Tuple<int, int>(_X_to_Index[Image.PlateX_um], _Y_to_Index[Image.PlateY_um]);
                    if (!_ImageByIndices.ContainsKey(Key)) _ImageByIndices.Add(Key, new List<XDCE_Image>());
                    _ImageByIndices[Key].Add(Image);

                    string WellField = Image.WellField;
                    if (!_WellFOV_to_Index.ContainsKey(WellField)) _WellFOV_to_Index.Add(WellField, new List<XDCE_Image>());
                    _WellFOV_to_Index[WellField].Add(Image);
                    if (!_WellFOV_Dict.ContainsKey(WellField))
                    { //Ideally we will only keep this one and get rid of the Well_FOV_toindex one . . 
                        var SIG = new XDCE_ImageGroup(); SIG.FilePath = this.FilePath; SIG.Level = "Field";
                        SIG.NameAtLevel = WellField; SIG.ParentFolder = ParentFolder; _WellFOV_Dict.TryAdd(WellField, SIG);
                    }
                    _WellFOV_Dict[WellField].Add(Image);

                    if (_ImageByIndices[Key].Count > _Wavelength_Count) _Wavelength_Count = (short)_ImageByIndices[Key].Count;
                    if (Image.FOV < _FOV_Min) _FOV_Min = Image.FOV;
                    if (Image.FOV > _FOV_Max) _FOV_Max = Image.FOV;
                }
                Dirty = false; //Can't use things directly until after this is called!!
            }

            public List<XDCE_Image> GetClosestFields(double PlateX, double PlateY)
            {
                double X1, X2, Y1, Y2;
                ClosestPlateCoordinate(PlateX, true, out X1, out X2);
                ClosestPlateCoordinate(PlateY, false, out Y1, out Y2);
                List<double> Xs = new List<double>(); if (!double.IsNaN(X1)) Xs.Add(X1); if (!double.IsNaN(X2)) Xs.Add(X2);
                List<double> Ys = new List<double>(); if (!double.IsNaN(Y1)) Ys.Add(Y1); if (!double.IsNaN(Y2)) Ys.Add(Y2);

                //Now that we have the closest, we can see which of these actually CONTAIN the point of interest
                List<List<XDCE_Image>> SetOfImages = new List<List<XDCE_Image>>();
                foreach (double X in Xs)
                {
                    foreach (double Y in Ys)
                    {
                        Tuple<int, int> Key = new Tuple<int, int>(_X_to_Index[X], _Y_to_Index[Y]);
                        if (_ImageByIndices.ContainsKey(Key))
                        {
                            List<XDCE_Image> IMGS = _ImageByIndices[Key];
                            if (IMGS[0].ContainsPlateCoordinate(PlateX, PlateY))
                            {
                                SetOfImages.Add(IMGS);
                            }
                        }
                        //Since we sorted these to do the best matches first, we could return and not check the other positions . . 
                    }
                }
                //Here we should check to see if one of them has it on the edge. Keep the image that has the point closest to the middle
                if (SetOfImages.Count > 1)
                {
                    Debugger.Break();
                }
                return SetOfImages.Count == 0 ? null : SetOfImages[0];
            }

            public void ClosestPlateCoordinate(double PlateCoordinateToFind, bool SearchX, out double Closest, out double NextClosest)
            {
                Closest = NextClosest = double.NaN;

                //The values of this dictionary are sorted, so we can quickly step through them
                List<double> DToUse = SearchX ? _X_to_Index.Keys.ToList() : _Y_to_Index.Keys.ToList();
                if (DToUse[0] > PlateCoordinateToFind)
                {
                    Closest = DToUse[0];
                    return;
                }
                for (int i = 1; i < DToUse.Count; i++)
                {
                    if (DToUse[i - 1] <= PlateCoordinateToFind && DToUse[i] > PlateCoordinateToFind)
                    {
                        bool CloserIsBefore = Math.Abs(PlateCoordinateToFind - DToUse[i - 1]) < Math.Abs(PlateCoordinateToFind - DToUse[i]);
                        Closest = CloserIsBefore ? DToUse[i - 1] : DToUse[i];
                        NextClosest = CloserIsBefore ? DToUse[i] : DToUse[i - 1];
                        return;
                    }
                }
                Closest = DToUse.Last();
            }

            public List<XDCE_Image> GetFields(int FOV)
            {
                List<XDCE_Image> FieldImages = Images.Where(x => x.FOV == FOV).ToList();
                return FieldImages;
            }

            public XDCE_Image GetField(int FOV, short wavelength)
            {
                List<XDCE_Image> FieldImages = GetFields(FOV);
                if (FieldImages.Count < 1) return null;
                try
                {
                    return FieldImages.Where(x => x.wavelength == wavelength).First();
                }
                catch
                {
                    return null;
                }
            }

            private bool _CheckedForCalSettings = false; private bool _CheckForPointsList = false;

            private ImageCheck.ImageCheck_PointList _ImageCheck_PointList = null;
            [XmlIgnore]
            public ImageCheck.ImageCheck_PointList ImageCheck_PointsList
            {
                get
                {
                    if (_ImageCheck_PointList == null && _CheckForPointsList == false)
                    {
                        //Look for the newest Calibration file in the folder, start with the standard name
                        var FI = new FileInfo(FilePath);
                        _ImageCheck_PointList = ImageCheck.ImageCheck_PointList.Load_Assumed(FI.DirectoryName, this.NameAtLevel);
                        _CheckForPointsList = true;
                    }
                    return _ImageCheck_PointList;
                }
                set
                {
                    _ImageCheck_PointList = value;
                }
            }

            private FOVtoRaftID.CalibrationRafts _CalibrationRaftSettings = null;
            [XmlIgnore]
            public FOVtoRaftID.CalibrationRafts CalibrationRaftSettings
            {
                get
                {
                    if (_CalibrationRaftSettings == null && _CheckedForCalSettings == false)
                    {
                        //Look for the newest Calibration file in the folder, start with the standard name
                        FileInfo FI = new FileInfo(FilePath);
                        _CalibrationRaftSettings = FOVtoRaftID.CalibrationRafts.Load_Assumed(FI.DirectoryName, this.NameAtLevel);
                        _CheckedForCalSettings = true;
                        //if (_CalibrationRaftSettings != null) Refresh_Wells_Rafts(); //5/24/2022 - the problem with putting this here is it is really slowwwww
                    }
                    return _CalibrationRaftSettings;
                }
                set
                {
                    _CalibrationRaftSettings = value;
                    _CheckedForCalSettings = true;
                }
            }

            public bool HasRaftCalibration { get => CalibrationRaftSettings != null; }
            public bool HasImageCheck { get => ImageCheck_PointsList != null; }
            public string ImageCheckInfo
            {
                get
                {
                    if (ImageCheck_PointsList == null) return "!No Annotations";
                    else return ImageCheck_PointsList.Overview;
                }
            }
            public int FOVCount { get => FOV_Max - FOV_Min; }

            private List<XDCE_Image> _MaskFields = null;
            private List<string> _MaskWells = null;

            [XmlIgnore]
            public string PathSearchMasks;

            [XmlIgnore]
            public List<XDCE_Image> MaskFields
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _MaskFields;
                }
            }

            [XmlIgnore]
            public List<string> MaskWells
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _MaskWells;
                }
            }

            [XmlIgnore]
            public bool HasMasks { get => MaskFields.Count > 0; }
        }

        public class XDCE : XDCE_ImageGroup
        {
            #region Declares --------------------------- ----------------------------------------------------
            public bool Valid;

            private INCELL_MeasuredOffsets _MeasuredOffsets = new INCELL_MeasuredOffsets();
            [XmlIgnore]
            public INCELL_MeasuredOffsets MeasuredOffsets
            {
                get => _MeasuredOffsets;
                set
                {
                    _MeasuredOffsets = value;
                    //Possibly trigger recalculations

                    foreach (var xI in Images) xI.RecalcPlateCoordinates();
                }
            }


            [XmlIgnore]
            public override List<XDCE_Image> Images
            {
                get
                {
                    CheckLoad();
                    return base.Images;
                }
            }

            private Dictionary<string, object> _KVPs;
            [XmlIgnore]
            public Dictionary<string, object> KVPs
            {
                get
                {
                    if (_KVPs == null) LoadNow(); return _KVPs;
                }
                set { _KVPs = value; }
            }
            public string NameString;
            public bool Loaded;

            public override string ToString() { return NameString; }

            private string _Plate_Name = "[-1]";
            public string Plate_Name
            {
                get
                {
                    if (KVPs == null) return "-1";
                    if (_Plate_Name == "[-1]") _Plate_Name = (string)KVPs["PLATE_NAME"];
                    return _Plate_Name;
                }
            }
            private double _PixelHeight_in_um = double.NaN;
            [XmlIgnore]
            public double PixelHeight_in_um
            {
                get
                {
                    if (double.IsNaN(_PixelHeight_in_um))
                    {
                        if (KVPs == null) return double.NaN;
                        string res = (string)KVPs["OBJECTIVECALIBRATION_PIXEL_HEIGHT"];
                        _PixelHeight_in_um = double.Parse(res);
                    }
                    return _PixelHeight_in_um;
                }
                set { _PixelHeight_in_um = value; }
            }
            private double _PixelWidth_in_um = double.NaN;
            [XmlIgnore]
            public double PixelWidth_in_um
            {
                get
                {
                    if (double.IsNaN(_PixelWidth_in_um))
                    {
                        if (KVPs == null) return double.NaN;
                        string res = (string)KVPs["OBJECTIVECALIBRATION_PIXEL_WIDTH"];
                        _PixelWidth_in_um = double.Parse(res);
                    }
                    return _PixelWidth_in_um;
                }
                set { _PixelWidth_in_um = value; }
            }
            private int _ImageWidth_in_Pixels = -1;
            [XmlIgnore]
            public int ImageWidth_in_Pixels
            {
                get
                {
                    if (_ImageWidth_in_Pixels == -1)
                    {
                        if (KVPs == null) return -1;
                        string res = (string)KVPs["SIZE_HEIGHT"];
                        _ImageWidth_in_Pixels = int.Parse(res);
                    }
                    return _ImageWidth_in_Pixels;
                }
                set { _ImageWidth_in_Pixels = value; }
            }
            private int _ImageHeight_in_Pixels = -1;
            [XmlIgnore]
            public int ImageHeight_in_Pixels
            {
                get
                {
                    if (_ImageHeight_in_Pixels == -1)
                    {
                        if (KVPs == null) return -1;
                        string res = (string)KVPs["SIZE_HEIGHT"];
                        _ImageHeight_in_Pixels = int.Parse(res);
                    }
                    return _ImageHeight_in_Pixels;
                }
                set { _ImageHeight_in_Pixels = value; }
            }

            private double _PlateXMax_um;
            public double PlateXMax_um { get { CheckPlate_um(); return _PlateXMax_um; } }
            private double _PlateXMin_um;
            public double PlateXMin_um { get { CheckPlate_um(); return _PlateXMin_um; } }
            private double _PlateYMax_um;
            public double PlateYMax_um { get { CheckPlate_um(); return _PlateYMax_um; } }
            private double _PlateYMin_um;
            public double PlateYMin_um { get { CheckPlate_um(); return _PlateYMin_um; } }

            public string MaskFolder { get => Path.Combine(FolderPath, INCELL_Folder.DefaultMaskFolderName); }

            private bool CheckPlate_um_Dirty = true;
            private void CheckPlate_um()
            {
                if (!CheckPlate_um_Dirty) return;
                foreach (XDCE_Image Image in Images)
                {
                    double xMax = Image.PlateX_um + (PixelWidth_in_um * ImageWidth_in_Pixels);
                    double yMax = Image.PlateY_um + (PixelHeight_in_um * ImageHeight_in_Pixels);
                    if (xMax > _PlateXMax_um) _PlateXMax_um = xMax; if (Image.PlateX_um < _PlateXMin_um) _PlateXMin_um = Image.PlateX_um;
                    if (yMax > _PlateYMax_um) _PlateYMax_um = yMax; if (Image.PlateY_um < _PlateYMin_um) _PlateYMin_um = Image.PlateY_um;
                }
                CheckPlate_um_Dirty = false;
            }

            #endregion

            #region Create and Initiate ----------------------------------------------------------------------
            public XDCE(string FilePath, bool LoadNow)
            {
                Init(FilePath, LoadNow);
            }

            public XDCE(string FilePath)
            {
                Init(FilePath, false);
            }

            public XDCE()
            {
                Loaded = false;
            }

            public static string GetXDCEPath(string FolderPath)
            {
                if (FolderPath.ToUpper().EndsWith(".XDCE")) return FolderPath;
                DirectoryInfo DI = new DirectoryInfo(FolderPath);
                if (!DI.Exists) return "";
                FileInfo[] files = DI.GetFiles("*.xdce");
                if (files.Length == 0) return "";
                return files[0].FullName;
            }

            private void Init(string FilePath, bool LoadNow)
            {
                Level = "XDCE";
                Valid = false;
                Loaded = false;
                FilePath = GetXDCEPath(FilePath);
                this.FilePath = FilePath;
                this.FolderPath = Path.GetDirectoryName(this.FilePath);
                NameString = Path.GetFileNameWithoutExtension(FilePath);
                Valid = true;
                if (LoadNow) this.LoadNow();
            }

            public void CheckLoad()
            {
                if (!Loaded) LoadNow();
            }

            public void LoadNow()
            {
                base.Images = new List<XDCE_Image>();
                _PlateXMax_um = _PlateYMax_um = double.MinValue; _PlateXMin_um = _PlateYMin_um = double.MaxValue;

                if (!Valid) return;

                string tLine; StringBuilder sB; XDCE_Image xImage; int idx = 0; StringBuilder sB_Initial = new StringBuilder();
                FilePath = FilePath.ToUpper();
                FileInfo FI = new FileInfo(FilePath);
                if (!FI.Exists)
                {
                    //Try the adjusted Path
                    if (FilePath.Contains(@"I:\"))
                        FilePath = FilePath.Replace(@"I:\", @"E:\IMAGING\");
                    else
                        FilePath = FilePath.Replace(@"E:\IMAGING\", @"I:\");

                }
                using (StreamReader SR = new StreamReader(FilePath))
                {
                    while (!SR.EndOfStream)
                    {
                        tLine = SR.ReadLine().Trim().ToUpper();
                        if (sB_Initial != null) sB_Initial.Append(tLine);
                        if (tLine.Contains("POINT INDEX="))
                        {
                            if (sB_Initial != null) { _KVPs = XDCE_Image.XML_Extract_KVPs(sB_Initial.ToString()); }
                            while (!SR.EndOfStream)
                            {
                                tLine = SR.ReadLine().Trim().ToUpper();
                                if (!tLine.Contains("POINT INDEX=")) break;
                            }
                            sB_Initial = null;
                        }
                        if (!tLine.StartsWith("<IMAGE GUID")) continue;

                        sB = new StringBuilder(); sB.Append(tLine);
                        while (!SR.EndOfStream)
                        {
                            tLine = SR.ReadLine().Trim().ToUpper();
                            sB.Append(tLine);
                            if (tLine != "</IMAGE>") continue;
                            xImage = new XDCE_Image(sB.ToString(), idx++, this);
                            Add(xImage);
                            break;
                        }
                    }
                    SR.Close();
                }
                Loaded = true;
            }

            private static HashSet<string> Compare_IgnoreList = new HashSet<string>() { "IMAGESTACK_PLATEID", "UUID_VALUE", "EXCLUDE_FIELDS", "AUTOLEADACQUISITIONPROTOCOL_FINGERPRINT" };

            public string Compare(XDCE ToCompareWith)
            {
                //This is just to look through the params and see what is different
                StringBuilder sB = new StringBuilder();
                Dictionary<string, object> Other = ToCompareWith.KVPs;
                foreach (KeyValuePair<string, object> KVP in KVPs)
                {
                    if (Compare_IgnoreList.Contains(KVP.Key)) continue;
                    if (Other.ContainsKey(KVP.Key))
                    {
                        if ((string)KVP.Value != (string)Other[KVP.Key])
                        {
                            sB.Append(KVP.Key + ": " + KVP.Value + ", " + ToCompareWith.KVPs[KVP.Key] + "\r\n\r\n");
                        }
                    }
                    else
                    {
                        sB.Append("MissingIndex\r\n");
                    }
                }
                return sB.ToString();
            }

            #endregion

            #region Statics -------------------------------------------------------------------------
            public static List<Tuple<int, int>> SurroundIndices = new List<Tuple<int, int>>() {
                                                                          new Tuple<int, int>(-2, 0),
                                             new Tuple<int, int>(-1, -1), new Tuple<int, int>(-1, 0), new Tuple<int, int>(-1, 1),
                new Tuple<int, int>( 0, -2), new Tuple<int, int>( 0, -1),                             new Tuple<int, int>( 0, 1), new Tuple<int, int>( 0, 2),
                                             new Tuple<int, int>( 1, -1), new Tuple<int, int>( 1, 0), new Tuple<int, int>( 1, 1),
                                                                          new Tuple<int, int>( 2, 0)
            };

            public static List<Tuple<int, int>> SurroundIndices_Fancy = new List<Tuple<int, int>>() {
                                             new Tuple<int, int>(-2, -1), new Tuple<int, int>(-2, 0), new Tuple<int, int>(-2, 1),
                new Tuple<int, int>(-1, -2), new Tuple<int, int>(-1, -1), new Tuple<int, int>(-1, 0), new Tuple<int, int>(-1, 1), new Tuple<int, int>(-1, 2),
                new Tuple<int, int>( 0, -2), new Tuple<int, int>( 0, -1),                             new Tuple<int, int>( 0, 1), new Tuple<int, int>( 0, 2),
                                             new Tuple<int, int>( 1, -1),                             new Tuple<int, int>( 1, 1),
                                             new Tuple<int, int>( 2, -1),                             new Tuple<int, int>( 2, 1)
            };

            /// <summary>
            /// Searches for all XDCE files, and exports a table next to the original file . . Use Compile if you want to compile a single table with everything
            /// </summary>
            /// <param name="BasePath">Place to start looking for XDCE files, is recursive</param>
            internal static void MakeTableFromEach(string BasePath)
            {
                DirectoryInfo DI = new DirectoryInfo(BasePath);
                foreach (DirectoryInfo DIsub in DI.GetDirectories())
                {
                    MakeTableFromEach(DIsub.FullName);
                }
                foreach (FileInfo file in DI.GetFiles("*.xdce"))
                {
                    XDCE xTemp = new XDCE(file.FullName);
                    xTemp.Export(Path.Combine(BasePath, Path.GetFileNameWithoutExtension(file.Name) + ".txt"));
                }
            }

            /// <summary>
            /// Newer (10/2019) version of exports, directed once the XDCE's have already been created
            /// </summary>
            /// <param name="XDCEs"></param>
            /// <param name="ExportPath"></param>
            public static void Compile(List<XDCE> XDCEs, string ExportPath)
            {
                var SW = new StreamWriter(ExportPath);
                bool First = true;
                foreach (XDCE xDCE in XDCEs)
                {
                    xDCE.Export(SW, First);
                    First = false;
                }
                SW.Close();
            }

            /// <summary>
            /// Similar to MakeTableFromEach, but this compiles them into a single table - Older, Slow (<9/2019)
            /// </summary>
            /// <param name="BasePath">Place to start looking for XDCE files, does search subfolders</param>
            /// <param name="ExportPath">Where to export</param>
            public static void Compile(string BasePath, string ExportPath)
            {
                var Files = new List<FileInfo>();
                var DI = new DirectoryInfo(BasePath);
                //- - This took 8 minutes . . not cool // Too many subdirectories with lots of files >> //Files = DI.GetFiles("*.xdce", SearchOption.AllDirectories).ToList();  
                //- - This version only took 13 s !!
                foreach (var DIsub in DI.GetDirectories())
                {
                    Files.AddRange(DIsub.GetFiles("*.xdce", SearchOption.TopDirectoryOnly).ToList());
                    foreach (var DIPlate in DIsub.GetDirectories())
                    {
                        Files.AddRange(DIPlate.GetFiles("*.xdce", SearchOption.TopDirectoryOnly).ToList());
                    }
                }
                var SW = new StreamWriter(ExportPath);
                XDCE xDCE; bool First = true;
                foreach (var file in Files)
                {
                    //if (!file.Name.ToUpper().Contains("RAFT")) continue;
                    xDCE = new XDCE(file.FullName, true);
                    //if (xDCE.Images.Count > 2000) continue;
                    xDCE.Export(SW, First);
                    First = false;
                }
                SW.Close();
            }

            internal static void Export(List<INCELL_Folder> Folders, string PathExport)
            {
                StreamWriter SW = new StreamWriter(PathExport);
                bool first = true;
                foreach (INCELL_Folder folder in Folders)
                {
                    folder.XDCE.Export(SW, first);
                    first = false;
                }
                SW.Close();
            }

            internal static void Export(List<XDCE> XDCEList, string PathExport)
            {
                StreamWriter SW = new StreamWriter(PathExport);
                bool first = true;
                foreach (XDCE x in XDCEList)
                {
                    x.Export(SW, first);
                    first = false;
                }
                SW.Close();
            }

            #endregion

            internal double Get_Adjacent_ZFocus_Average(XDCE_Image Image, int XIndex, int YIndex)
            {
                double SumZFocus = 0; double CountZFocus = 0; List<XDCE_Image> tAdj;
                foreach (Tuple<int, int> tupleOffset in SurroundIndices)
                {
                    tAdj = GetAdjacent(XIndex, YIndex, tupleOffset.Item1, tupleOffset.Item2);
                    if (tAdj.Count == 0) continue;
                    CountZFocus++; SumZFocus += tAdj[0].z;
                }
                if (CountZFocus > 0) return SumZFocus / CountZFocus;
                return 0;
            }

            private bool _SetupFocusDev_Done = false;
            internal void SetupFocusDeviation()
            {
                CheckLoad();
                if (_SetupFocusDev_Done) return;
                string colName = XDCE_Image.ColName_AdjacentFocusAvg;
                List<XDCE_Image> tImages; Tuple<int, int> Key;
                double AvgZFocus;
                for (int x = 0; x < X_to_Index.Count; x++)
                {
                    for (int y = 0; y < Y_to_Index.Count; y++)
                    {
                        Key = new Tuple<int, int>(x, y);
                        if (_ImageByIndices.ContainsKey(Key))
                        {
                            tImages = _ImageByIndices[Key];
                            AvgZFocus = Get_Adjacent_ZFocus_Average(tImages[0], x, y);
                            foreach (XDCE_Image img in tImages)
                            {
                                img.KVPs.Add(colName, AvgZFocus);
                            }
                        }
                    }
                }
                _SetupFocusDev_Done = true;
            }

            public IEnumerator<XDCE_Image> GetEnumerator()
            {
                return Images.GetEnumerator();
            }

            public void Export(string PathSave)
            {
                CheckLoad();
                StreamWriter SW = new StreamWriter(PathSave);
                Export(SW, true);
                SW.Close();
            }

            public void Export(StreamWriter SW, bool IncludeHeaders)
            {
                SetupFocusDeviation();
                if (IncludeHeaders)
                {
                    //Reset these just in case
                    KVPHeaderOrder = new List<string>(); XDCE_Image.KVPHeaderOrder = new List<string>();
                    SW.WriteLine(Images.First().Export(true) + ExportPres(true));
                }
                foreach (XDCE_Image xImage in Images)
                {
                    SW.WriteLine(xImage.Export(false) + ExportPres(false));
                }
            }

            protected static List<string> KVPHeaderOrder = new List<string>();

            public string ExportPres(bool Headers)
            {
                char delim = '\t';
                StringBuilder SB = new StringBuilder();

                //These KVPs are slightly variable . . so we need to make a static that everything goes in to keep it in order! (only Keys in the first dictionary will get exported)
                if (KVPHeaderOrder.Count == 0) { foreach (KeyValuePair<string, object> KVP in KVPs) KVPHeaderOrder.Add(KVP.Key); }
                string res;
                foreach (string Header in KVPHeaderOrder)
                {
                    res = KVPs.ContainsKey(Header) ? KVPs[Header].ToString() : "";
                    SB.Append((Headers ? Header : res) + delim);
                }
                return SB.ToString();
            }
        }

        public enum FIVToolsMode
        {
            Normal = 0,
            PlateID = 1,
            PlateID_WellList = 2,
            WellCalibrate = 11,
            FOVRefine = 12,
            Listening = 33,
            Silent = 1000
        }

        public class Cell_Filter
        {
            public Cell_Filter()
            {
                DefineRows = new List<object>();
            }

            public List<object> DefineRows;
            public string Name;

            public static Cell_Filter Default_FieldRestrict(int LowCells, int HighCells)
            {
                //Count the number of Cells per Field, add this as own column
                Cell_Filter CF = new Cell_Filter();
                CF.DefineRows.Add(LowCells); CF.DefineRows.Add(HighCells);
                CF.Name = "Field Restrict";
                return CF;
            }

            public static Cell_Filter Default_ColumnsRemove(IEnumerable<string> ColumnsToRemove)
            {
                Cell_Filter CF = new Cell_Filter();
                CF.DefineRows.Add("");
                CF.Name = "Columns Remove";
                return CF;
            }

            internal void Apply(INCARTA_Analysis iNCARTA_Analysis)
            {
                if (this.Name == "Field Restrict")
                {
                    List<INCARTA_Cell> CellsToDelete = new List<INCARTA_Cell>();
                    int cellsPerfield;
                    foreach (INCARTA_Cell cell in iNCARTA_Analysis.Cells)
                    {
                        cellsPerfield = int.Parse((string)cell.DataPerHeader(INCARTA_Cell_Headers.col_CellsPerField));
                        if (cellsPerfield < (int)DefineRows[0] || cellsPerfield > (int)DefineRows[1]) CellsToDelete.Add(cell);
                    }
                    iNCARTA_Analysis.Cells = iNCARTA_Analysis.Cells.Except(CellsToDelete).ToList();
                    //foreach (INCARTA_Cell cell in CellsToDelete)
                    //{
                    //    iNCARTA_Analysis.Cells.Remove(cell);
                    //}
                }
            }

            internal static Cell_Filter Default_Object0Remove()
            {
                //ObjectID = 0  (These are something special, but are not standard cells)
                Cell_Filter CF = new Cell_Filter();
                CF.DefineRows.Add("");
                CF.Name = "Object0 Remove";
                return CF;
            }
        }

        public class INCARTA_Analysis
        {
            public INCARTA_Cell_Headers CombinedHeaders { get; }
            public List<INCARTA_Cell> Cells { get; internal set; }
            public static int CellsRemoved;
            public static int CellsPerField_Min;
            public static int CellsPerField_Max;
            public static double CellsPerField_Mean;
            public static int FOV_Count;

            public INCARTA_Analysis(IEnumerable<INCARTA_Analysis_Folder> Analyses, System.ComponentModel.BackgroundWorker BW = null)
            {
                CellsRemoved = CellsPerField_Max = FOV_Count = 0; CellsPerField_Mean = 0; CellsPerField_Min = 100000;
                CombinedHeaders = new INCARTA_Cell_Headers();
                Cells = new List<INCARTA_Cell>();
                foreach (INCARTA_Analysis_Folder folder in Analyses)
                {
                    if (BW != null) BW.ReportProgress(0, folder.FolderName);
                    Cells.AddRange(LoadFromFolder(folder, BW));
                    if (Cells.Count > 0)
                        CombinedHeaders.AddMerge(Cells.Last().Headers);
                }

                CellsPerField_Mean = FOV_Count == 0 ? 0 : Cells.Count / FOV_Count;
            }

            public INCARTA_Analysis(INCARTA_Analysis_Folder folder, BackgroundWorker BW = null)
            {
                Cells = LoadFromFolder(folder, BW);
            }

            private static List<INCARTA_Cell> LoadFromFolder(INCARTA_Analysis_Folder folder, BackgroundWorker BW = null)
            {
                //Read in the INCARTA Analysis line, store it so that we can easily re-arrange the columns
                List<INCARTA_Cell> CellList = new List<INCARTA_Cell>(); long counter = 0;
                FileInfo FI = new FileInfo(folder.Path_CellExport_Adjusted); if (!FI.Exists) return CellList;
                using (StreamReader SR = new StreamReader(FI.FullName))
                {
                    string tLine = SR.ReadLine();
                    var Headers = new INCARTA_Cell_Headers(tLine, folder);
                    INCARTA_Cell tCell;
                    //This bit of code records the Cells/Field
                    string Well_Field = ""; var CurrentFOVCells = new HashSet<INCARTA_Cell>();
                    while (!SR.EndOfStream)
                    {
                        tLine = SR.ReadLine();
                        if (BW != null) { if (counter++ % 2500 == 0) BW.ReportProgress(0, counter / 2500); }
                        tCell = new INCARTA_Cell(folder, Headers, tLine);
                        if (tCell.IsValid) //There are some lines in these files that aren't really a cell
                        {
                            Add_Cells_Coordinates_Name(tCell);
                            if (tCell.Well.HasRaftCalibration) Add_Cells_RaftID(tCell);
                            else { if (tCell.Well.HasImageCheck) Add_Cells_ImageCheck(tCell); }
                            CellList.Add(tCell);
                            if (Well_Field != tCell.WellField)
                            {
                                //Go through the previous Cell list, and add the field column
                                Add_CellsPerFieldColumn(CurrentFOVCells);
                                //Start the new set
                                Well_Field = tCell.WellField; CurrentFOVCells = new HashSet<INCARTA_Cell>();
                            }
                            CurrentFOVCells.Add(tCell);
                        }
                        else { CellsRemoved++; }
                    }
                    SR.Close();
                    Add_CellsPerFieldColumn(CurrentFOVCells);
                }
                return CellList;
            }

            private static void Add_Cells_Coordinates_Name(INCARTA_Cell tCell)
            {
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_X_Well_um, tCell.x_well_um);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_Y_Well_um, tCell.y_well_um);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_X_Plate_um, tCell.x_plate_um);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_Y_Plate_um, tCell.y_plate_um);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ImageName, tCell.RelatedImage.FileName);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ZRaw, tCell.RelatedImage.z);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ZAvgAdjacent, tCell.RelatedImage.z_AvgAdjacent);
            }

            private static void Add_Cells_RaftID(INCARTA_Cell tCell)
            {
                double x = tCell.x_plate_um;  //Convert to Plate Coordinates
                double y = tCell.y_plate_um;
                FOVtoRaftID.ReturnRaft RR = tCell.Well.CalibrationRaftSettings.FindRaftID_RR(x, y);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_CellsRaftID, RR.RaftID);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_CellsRaftRow, RR.RaftID.Substring(0, 2));
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_CellsRaftCol, RR.RaftID.Substring(2, 2));
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_CellsRaftDistBorder, RR.MinDistToEdge);
                if (tCell.Well.HasImageCheck)
                {
                    ImageCheck.ImageCheck_PointList PL = tCell.Well.ImageCheck_PointsList;

                    string Res = PL.FromDictionaryRaftID_Combined(RR.RaftID); if (Res == "") Res = "-";
                    AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ImageCheckAnnotation, Res);
                }
            }
            private static void Add_Cells_ImageCheck(INCARTA_Cell tCell)
            {
                ImageCheck.ImageCheck_PointList PL = tCell.Well.ImageCheck_PointsList;
                string Res = PL.FromDictionaryFOV_Combined(tCell.FOV); if (Res == "") Res = "-";
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ImageCheckAnnotation, Res);
            }

            internal static void AddData_AndMaybeNewColumn(INCARTA_Cell tCell, string ColumnHeaderName, object Value)
            {
                if (!tCell.Headers.Contains(ColumnHeaderName))
                    tCell.Headers.AddNewColumn(ColumnHeaderName);
                tCell.AddData(ColumnHeaderName, Value);
            }

            private static void Add_CellsPerFieldColumn(HashSet<INCARTA_Cell> currentFOVCells)
            {
                if (currentFOVCells.Count == 0) return;
                int n = currentFOVCells.Count;
                if (n < CellsPerField_Min) CellsPerField_Min = n;
                if (n > CellsPerField_Max) CellsPerField_Max = n;
                FOV_Count++;
                if (!currentFOVCells.First().Headers.Contains(INCARTA_Cell_Headers.col_CellsPerField))
                {
                    currentFOVCells.First().Headers.AddNewColumn(INCARTA_Cell_Headers.col_CellsPerField);
                }
                string szCount = n.ToString();
                foreach (INCARTA_Cell cell in currentFOVCells)
                {
                    cell.AddData(INCARTA_Cell_Headers.col_CellsPerField, szCount);
                }
            }

            public void ApplyFilters(List<Cell_Filter> Filters)
            {
                //First filter the columns
                //This won't work since we don't parse the lines until we start to export
                /*
                List<string> ColumnsToRemove = new List<string>();
                foreach (string Column in CombinedHeaders)
                {
                    if (INCARTA_Cell_Headers.ValueCount[Column] < 10) ColumnsToRemove.Add(Column);
                }
                */

                //Now apply specific filters
                foreach (Cell_Filter filter in Filters)
                {
                    filter.Apply(this);
                }
            }

            public void Export(string Path)
            {
                using (StreamWriter SW = new StreamWriter(Path))
                {
                    ExportCells(CombinedHeaders, Cells, true, false, SW);
                }
            }

            internal static void ExportCells(INCARTA_Cell_Headers CHeaders, List<INCARTA_Cell> CellsList, bool ExportHeaders, bool RenameWavelengths, StreamWriter SW)
            {
                if (ExportHeaders) SW.WriteLine(RenameWavelengths ? CHeaders.ExportLine_Rename : CHeaders.ExportLine);
                foreach (INCARTA_Cell cell in CellsList)
                {
                    SW.WriteLine(cell.ExportReOrder(CHeaders));
                }
            }

            internal static void CompileExport(List<INCARTA_Analysis_Folder> compileList_AFs, string pathExport, bool Rename_Wavelengths, BackgroundWorker bW)
            {
                StreamWriter SW = new StreamWriter(pathExport);
                string prevHeaders = "";
                foreach (INCARTA_Analysis_Folder folder in compileList_AFs)
                {
                    if (bW != null) bW.ReportProgress(0, folder.FolderName);
                    prevHeaders = CompileExport_I1(folder, prevHeaders, SW, Rename_Wavelengths, bW);
                    SW.Flush();
                }
                SW.Close();
            }

            internal static string CompileExport_I1(INCARTA_Analysis_Folder folder, string PrevHeaders, StreamWriter SW, bool Rename_Wavelengths, BackgroundWorker BW)
            {
                long counter = 0; bool First = false;
                FileInfo FI = new FileInfo(folder.Path_CellExport_Adjusted); if (!FI.Exists) return PrevHeaders;
                StreamReader SR = new StreamReader(FI.FullName);
                //This next bit figures out whether the headers match
                string tLine = SR.ReadLine();
                if (PrevHeaders == "") { First = true; }
                else
                {
                    if (PrevHeaders != tLine)
                    {
                        Debugger.Break(); //The program can deal with this if we go through all the files first just reading the headers, then going back (not yet implemented)
                        return PrevHeaders;
                    }
                }
                string Well_Field = ""; INCARTA_Cell tCell; //This bit of code records the Cells/Field
                List<INCARTA_Cell> CellList = null; HashSet<INCARTA_Cell> CurrentFOVCells = null;
                PrevHeaders = tLine;
                var Headers = new INCARTA_Cell_Headers(tLine, folder);
                bool TargetDataFolderMode = false; string[] TargetFiles = new string[0]; int TargetFileIdx = 0;

                void DoExport()
                {
                    if (Well_Field == "") return; //First time, nothing to export yet
                    Add_CellsPerFieldColumn(CurrentFOVCells); //Go through the previous Cell list, and add the field column
                    ExportCells(Headers, CellList, First, Rename_Wavelengths, SW);  //Export
                    First = false;
                }

                void NextFileTargetData() //Added 4/2022 so that we could automatically load files that didn't get fully processed into the output file. This automtaically loads them
                {
                    if (TargetDataFolderMode)
                    {
                        if (SR.EndOfStream && (TargetFileIdx < TargetFiles.Length))
                        {
                            SR.Close(); SR = new StreamReader(TargetFiles[TargetFileIdx++]);
                            SR.ReadLine();
                        }
                    }
                }

                //Here is the actual start of the reading
                TargetDataFolderMode = SR.EndOfStream;  //If the main file only has the headers, then switch to the individual files
                                                        //TargetDataFolderMode = true; //Turn this to TRUE to load from the individual files . . 
                if (TargetDataFolderMode)
                {
                    string TDataPath = Path.Combine(folder.FolderPath, "TargetData");
                    if (!Directory.Exists(TDataPath))
                    {
                        Debugger.Break(); //Couldn't find Target Data folder
                        if (BW != null) BW.ReportProgress(0, "!No data found");
                    }
                    TargetFiles = Directory.GetFiles(TDataPath, "singleTargetData*.csv");
                    NextFileTargetData();
                }
                while (!SR.EndOfStream)
                {
                    if (BW != null) { if (counter++ % 1500 == 0) BW.ReportProgress(0, counter / 1500); }
                    tCell = new INCARTA_Cell(folder, Headers, tLine = SR.ReadLine());
                    if (tCell.IsValid) //There are some lines in these files that aren't really a cell
                    {
                        if (Well_Field != tCell.WellField) //Export then Start the new set
                        {
                            DoExport();
                            Well_Field = tCell.WellField;
                            CurrentFOVCells = new HashSet<INCARTA_Cell>();
                            CellList = new List<INCARTA_Cell>();
                        }
                        Add_Cells_Coordinates_Name(tCell);
                        if (tCell.Well.HasRaftCalibration) Add_Cells_RaftID(tCell);
                        else { if (tCell.Well.HasImageCheck) Add_Cells_ImageCheck(tCell); }
                        CurrentFOVCells.Add(tCell);
                        CellList.Add(tCell);
                    }
                    else { CellsRemoved++; }
                    NextFileTargetData();
                }
                if (counter == 0 && BW != null) BW.ReportProgress(0, "Empty File");
                SR.Close();
                DoExport();


                return PrevHeaders;
            }
        }

        #region Feature Headers - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        public class INCARTA_Cell_Headers : IEnumerable<string>
        {
            public char delim = ',';

            public static string col_PlateID = "PLATEID";
            public static string col_ObjectID = "OBJECT ID";
            public static string col_CellsPerField = "CellsPerField";
            public static string col_CellsRaftID = "RaftID";
            public static string col_CellsRaftRow = "RaftRow";
            public static string col_CellsRaftCol = "RaftCol";
            public static string col_ZT_Z = "Z";
            public static string col_ZT_T = "T";
            public static string col_ZT_ResultsID = "RESULTSID";
            public static string col_CellsRaftDistBorder = "umFromBorderOfRaft";
            public static string col_ImageCheckAnnotation = "ImageCheck";
            public static string col_Well = "WELL LABEL";
            public static string col_Field = "FOV";
            public static string col_X_Well_um = "Well X um";
            public static string col_Y_Well_um = "Well Y um";
            public static string col_X_Plate_um = "Plate X um";
            public static string col_Y_Plate_um = "Plate Y um";
            public static string col_ZRaw = "Focus Z pos";
            public static string col_ZAvgAdjacent = "Focus Z Avg of Adjacent Fields";
            public static string col_ImageName = "ImageName";
            public static string col_Xcg = "Nuclei cg X wv1";
            public static string col_Ycg = "Nuclei cg Y wv1";
            public static string col_Xborder = "Nuclei Max left border wv1";
            public static string col_Yborder = "Nuclei Max top border wv1";

            public INCELL_WV_Notes WVNotes;
            private Dictionary<string, short> _Name2Index;
            private Dictionary<short, string> _Index2Name;
            public Dictionary<short, Header_Info> Index2HeaderInfo;

            public bool HasRaftHeader { get; internal set; }

            public INCARTA_Cell_Headers(string HeaderLine, INCARTA_Analysis_Folder AFolder)
            {
                HasRaftHeader = false;
                if (AFolder != null) WVNotes = AFolder.Parent.InCell_Wavelength_Notes; //This lets us decode the wv index into a real name
                init();
                string[] arr = HeaderLine.Split(delim);
                if (arr.Length < 5)
                {
                    string[] arr2 = HeaderLine.Split('\t');
                    if (arr2.Length > arr.Length)
                    {
                        delim = '\t'; arr = arr2;
                    }
                }
                foreach (string column in arr)
                {
                    if (_Name2Index.ContainsKey(column))
                    {
                        Debugger.Break(); //Add an index on the end
                    }
                    else
                    {
                        AddNewColumn(column);
                    }
                }
            }

            public INCARTA_Cell_Headers()
            {
                init();
            }

            private void init()
            {
                _Name2Index = new Dictionary<string, short>();
                _Index2Name = new Dictionary<short, string>();
                Index2HeaderInfo = new Dictionary<short, Header_Info>();
            }

            public string RepairColumnName(string column)
            {
                //This is done to all new columns
                column = column.Replace("Mitochondria", "Mito");
                column = column.Replace("MITOCHONDRIA", "Mito");
                return column;
            }

            /// <summary>
            /// Maintains the starting names
            /// </summary>
            public string ExportLine
            {
                get
                {
                    StringBuilder sB = new StringBuilder();
                    foreach (string column in _Name2Index.Keys) sB.Append(column + delim);
                    return INCARTA_Analysis_Folder.ExportLine(delim) + delim + "Val Count" + delim + sB;
                }
            }

            /// <summary>
            /// Renames wv1 => WVH, etc. If no attached WVNotes then it just exports the same as ExportLine
            /// </summary>
            public string ExportLine_Rename
            {
                get
                {
                    if (WVNotes == null) return ExportLine;

                    StringBuilder sB = new StringBuilder();
                    foreach (string column in _Name2Index.Keys) sB.Append(WVNotes.Rename(column) + delim);
                    return INCARTA_Analysis_Folder.ExportLine(delim) + delim + "Val Count" + delim + sB;
                }
            }

            public short ShortKeyTable(string HeaderColumnName)
            {
                string Key = HeaderColumnName;
                if (_Name2Index.ContainsKey(Key)) return _Name2Index[Key];

                //Added 3/16/2022
                Key = Key.ToUpper().Trim();
                if (_Name2Index.ContainsKey(Key))
                {
                    //Add this to the database so we can get it automtically next time around
                    short v = _Name2Index[Key];
                    _Name2Index.Add(HeaderColumnName, v);
                }
                return -1;
            }

            internal void AddNewColumn(string column)
            {
                column = RepairColumnName(column);
                if (column == "RAFTID") column = "RaftID";
                if (column == "RaftID") HasRaftHeader = true;
                _Name2Index.Add(column, (short)_Name2Index.Count);
                _Index2Name.Add((short)(_Name2Index.Count - 1), column);
                Index2HeaderInfo.Add((short)(_Name2Index.Count - 1), new Header_Info(column, (short)(_Name2Index.Count - 1)));
            }

            public int Count { get => _Name2Index.Count; }

            public bool Contains(string value) { return _Name2Index.ContainsKey(value); }

            public string this[short index]
            {
                get { return _Index2Name[index]; }
            }

            public IEnumerator<string> GetEnumerator()
            {
                return _Name2Index.Keys.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Name2Index.GetEnumerator();
            }

            internal void AddMerge(INCARTA_Cell_Headers headers)
            {
                foreach (string column in headers)
                {
                    if (!Contains(column))
                    {
                        AddNewColumn(column);
                    }
                }
            }

            internal static Dictionary<string, int> ValueCount { get; private set; }

            internal static void AddValueCount(string HeaderName)
            {
                if (ValueCount == null) ValueCount = new Dictionary<string, int>(1);
                if (!ValueCount.ContainsKey(HeaderName)) ValueCount.Add(HeaderName, 0);
                ValueCount[HeaderName]++;
            }

            public List<Header_Info> Cols_Measurement => Index2HeaderInfo.Where(x => x.Value.IsMeasurement).Select(x => x.Value).ToList();
            public HashSet<Header_Info> Cols_CellMetadata => new HashSet<Header_Info>(Index2HeaderInfo.Where(x => x.Value.IsCellMetadata).Select(x => x.Value));
            public List<Header_Info> Cols_Undesired => Index2HeaderInfo.Where(x => x.Value.IsUndesired).Select(x => x.Value).ToList();
            public HashSet<string> Set_Masks => new HashSet<string>(Cols_Measurement.Select(x => x.Feature_Mask).Distinct());
            public HashSet<string> Set_WVs => new HashSet<string>(Cols_Measurement.Select(x => x.Feature_Wavelength).Distinct());
            public HashSet<string> Set_Measures => new HashSet<string>(Cols_Measurement.Select(x => x.Feature_Measure).Distinct());

        }

        #endregion

        /// <summary>
        /// An InCarta "Cell" Object, DOES link out to other types, so needs to be constructed with an InCell Folder and Analysis specifically
        /// </summary>
        public class INCARTA_Cell : Simple_Cell
        {
            private XDCE_ImageGroup _Well;
            public XDCE_ImageGroup Well
            {
                get
                {
                    if (_Well == null)
                    {
                        _Well = Parent.RelatedXDCE.Wells[this.WellLabel];
                    }
                    return _Well;
                }
            }

            private XDCE_Image _RelatedImage;
            public XDCE_Image RelatedImage
            {
                get
                {
                    if (_RelatedImage == null) _RelatedImage = Parent.RelatedXDCE.GetImage_FromWellFOV(WellField);
                    return _RelatedImage;
                }
            }
            public INCARTA_Analysis_Folder Parent { get; }

            public string ExportLine
            {
                get
                {
                    Debugger.Break(); //OLD VERSION, NOT UPDATED
                    return Parent.FolderName + delim + Parent.Index + delim + Line;
                }
            }

            private void SetupPositions()
            {
                XDCE_Image rImage = RelatedImage;

                _Xcg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Xcg));
                _Ycg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Ycg));
                if (_Xcg == -1)
                {
                    //First check to see if it is measured in wv2, since that is the easiest
                    _Xcg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Xcg.Replace("wv1", "wv2")));
                    _Ycg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Ycg.Replace("wv1", "wv2")));
                }
                if (_Xcg == -1)
                {
                    //If not here, then try the x/y border, but these aren't quite the same as the cg (Center)
                    _Xcg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Xborder));
                    _Ycg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Yborder));
                    _Xcg = _Xcg * rImage.Parent.PixelWidth_in_um; _Ycg = _Ycg * rImage.Parent.PixelHeight_in_um;
                }

                _x_well_um = _Xcg + rImage.x;
                _y_well_um = -_Ycg + (rImage.Parent.PixelHeight_in_um * rImage.Parent.ImageHeight_in_Pixels) - rImage.y;

                _x_plate_um = _Xcg + rImage.PlateX_um;
                _y_plate_um = -_Ycg + (rImage.Parent.PixelHeight_in_um * rImage.Parent.ImageHeight_in_Pixels) + rImage.PlateY_um;
            }

            private double _Xcg = Pre_DefaultVal;
            public double Xcg { get { if (_Xcg == Pre_DefaultVal) SetupPositions(); return _Xcg; } }

            private double _Ycg = Pre_DefaultVal;
            public double Ycg { get { if (_Ycg == Pre_DefaultVal) SetupPositions(); return _Ycg; } }

            private double _x_well_um = Pre_DefaultVal;
            public double x_well_um { get { if (_x_well_um == Pre_DefaultVal) SetupPositions(); return _x_well_um; } }

            private double _y_well_um = Pre_DefaultVal;
            public double y_well_um { get { if (_y_well_um == Pre_DefaultVal) SetupPositions(); return _y_well_um; } }

            private double _x_plate_um = Pre_DefaultVal;
            public double x_plate_um { get { if (_x_plate_um == Pre_DefaultVal) SetupPositions(); return _x_plate_um; } }

            private double _y_plate_um = Pre_DefaultVal;
            public double y_plate_um { get { if (_y_plate_um == Pre_DefaultVal) SetupPositions(); return _y_plate_um; } }

            public INCARTA_Cell(INCARTA_Analysis_Folder Parent, INCARTA_Cell_Headers Headers, string Line, char delim_override = '0')
            {
                if (delim_override != '0') delim = delim_override;
                IsValid = true;
                this.Parent = Parent;
                this.Headers = Headers;
                this.Line = Line;
                if (DataPerHeader(INCARTA_Cell_Headers.col_ObjectID).ToString() == "0") IsValid = false;
            }

            public string ExportReOrder(INCARTA_Cell_Headers HeadersOrder)
            {
                StringBuilder sB = new StringBuilder();
                sB.Append(Parent.ExportLineThis(delim) + delim + ValuesCount + delim);
                foreach (string Column in HeadersOrder)
                {
                    if (Headers.Contains(Column))
                    {
                        sB.Append(DataPerHeader(Column));
                    }
                    sB.Append(delim);
                }
                _DataPerHeader = null;
                return sB.ToString();
            }
        }

        public class QStats
        {
            public string ColumnNameOriginal;
            public int IndexOriginal;
            public double Sum;
            public int Count;
            public int Errors;
            public int NormErrors;
            public HashSet<double> UniqueValues;

            public double Mean { get => Sum / Count; }

            public QStats(string Name, int OriginalIndex)
            {
                ColumnNameOriginal = Name;
                IndexOriginal = OriginalIndex;
                Sum = 0;
                Count = 0;
                UniqueValues = new HashSet<double>();
                Errors = 0;
            }

            public void AddValue(string ToParse)
            {
                double v1;
                if (double.TryParse(ToParse, out v1))
                {
                    Count++;
                    Sum += v1;
                    UniqueValues.Add(v1);
                }
                else
                {
                    Errors++;
                }
            }

            public double Norm(double val)
            {
                return val / Mean;
            }

            public double Norm(string ToParse)
            {
                double v1;
                if (double.TryParse(ToParse, out v1))
                {
                    return Norm(v1);
                }
                else
                {
                    NormErrors++;
                    return 0;
                }
            }
        }

        public class INCELL_DB
        {
            public List<INCELL_Folder> Folders { get; internal set; }
            public string BasePath { get; set; }
            public DateTime LastUpdated { get; set; }

            [XmlIgnore]
            public List<INCARTA_Analysis_Folder> CompileList_AFs { get; set; }
            [XmlIgnore]
            public List<INCELL_Folder> CompileList_Folders { get; set; }

            private List<INCARTA_Analysis_Folder> _AnalysisFolders;
            [XmlIgnore]
            public List<INCARTA_Analysis_Folder> AnalysisFolders
            {
                get
                {
                    if (_AnalysisFolders == null)
                    {
                        _AnalysisFolders = new List<INCARTA_Analysis_Folder>(Folders.Count);
                        foreach (INCELL_Folder folder in Folders)
                        {
                            AnalysisFolders.AddRange(folder.AnalysisFolders);
                        }
                    }
                    return _AnalysisFolders;
                }
            }

            public INCELL_DB()
            {
                BasePath = "";
                Folders = new List<INCELL_Folder>();
            }

            public INCELL_DB(string BasePath)
            {
                Init(BasePath, null);
            }

            public INCELL_DB(string BasePath, System.ComponentModel.BackgroundWorker BW)
            {
                Init(BasePath, BW);
            }

            private void Init(string BasePath, System.ComponentModel.BackgroundWorker BW)
            {
                this.BasePath = BasePath.Trim().ToUpper();
                Folders = INCELL_Folder.GetFromFolder(this.BasePath, BW);
                LastUpdated = DateTime.Now;
                Save();
            }

            public static string DefaultDBXMLName = "InCell_FIVETools.db";

            public bool Save()
            {
                //Saves it in the path of the
                return Save(Path.Combine(BasePath, DefaultDBXMLName));
            }

            public bool Save(string SavePath)
            {
                //Whenever saving, copy the
                foreach (INCELL_Folder fldr in this.Folders) fldr.BasePath_Saved = fldr.BasePath_Current;
                XmlSerializer x = new XmlSerializer(typeof(INCELL_DB));
                using (StreamWriter SW = new StreamWriter(SavePath))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                    return true;
                }
            }

            public static INCELL_DB Load_Or_Create(string BasePath, BackgroundWorker BW)
            {
                FileInfo FI = new FileInfo(Path.Combine(BasePath, DefaultDBXMLName));
                INCELL_DB dB;
                if (FI.Exists) dB = Load(FI.FullName);
                else dB = new INCELL_DB(BasePath, BW);
                foreach (INCELL_Folder fldr in dB.Folders) fldr.BasePath_Current = BasePath;
                return dB;
            }

            public static INCELL_DB Load_From_BasePath(string BasePath)
            {
                try
                {
                    FileInfo FI = new FileInfo(Path.Combine(BasePath, DefaultDBXMLName));
                    if (FI.Exists)
                    {
                        INCELL_DB dB = Load(FI.FullName);
                        foreach (INCELL_Folder fldr in dB.Folders) fldr.ReAssociate(BasePath);
                        return dB;
                    }
                }
                catch (Exception E) { RecentError = E.Message; }
                return null;
            }

            public static string RecentError = "";

            public static INCELL_DB Load(string LoadPath)
            {
                XmlSerializer x = new XmlSerializer(typeof(INCELL_DB));
                using (FileStream F = new FileStream(LoadPath, FileMode.Open))
                {
                    INCELL_DB CR = (INCELL_DB)x.Deserialize(F);
                    F.Close();
                    return CR;
                }
            }

            public void Refresh(BackgroundWorker BW, string LimitSearch = "")
            {
                //This assumes you already loaded this and we just want to check.  FOr now, we will just create a new one for testing
                Folders = INCELL_Folder.GetFromFolder(BasePath, BW, LimitSearch);
                _AnalysisFolders = null;
                int c = AnalysisFolders.Count; //This should trigger a refresh
                LastUpdated = DateTime.Now;
                Save();
            }
        }

        public class INCELL_WV_Note
        {
            public string WAVELENGTH_INDEX;
            public string WAVELENGTH_LABEL;
            public string WAVELENGTH_NAME;
            public string WAVELENGTH_IMAGING_MODE;
            public string WAVELENGTH_APERTURE_ROWS;
            public string WAVELENGTH_LASER_POWER;
            public string WAVELENGTH_OPEN_APERTURE;
            public string Threshold = "90";
            public string Staining = "N/A";
            public string Abbrev = "-";
            public string ExtraNote = "N/A";

            public INCELL_WV_Note()
            {

            }

            public override string ToString()
            {
                return QuickName;
            }

            private int _Index1 = -1;
            public int Index1
            {
                get
                {
                    if (_Index1 == -1)
                    {
                        _Index1 = int.Parse(WAVELENGTH_INDEX) + 1;
                    }
                    return _Index1;
                }
                set { _Index1 = value; }
            }

            public string QuickName
            {
                get
                {
                    if (WAVELENGTH_NAME == null) WAVELENGTH_NAME = "";
                    if (WAVELENGTH_LABEL == null) WAVELENGTH_LABEL = "";
                    return Staining == "N/A" ? WAVELENGTH_LABEL == "" ? WAVELENGTH_NAME : WAVELENGTH_LABEL : Staining;
                }
            }

            public double Threshold_Value
            {
                get
                {
                    return double.Parse(Threshold);
                }
            }
        }

        public class INCELL_WV_Notes
        {
            //And if your class inherits a list and also has its own members, only the elements of the list get serialized. The data present in your class members is not captured. (From Stack Overflow)
            //I had this as inheriting IENumerable, but it didn't serialize other members
            private List<INCELL_WV_Note> _List;
            [XmlIgnore]
            internal bool LoadedFromXML = false;

            public List<INCELL_WV_Note> List { get => _List; set { _List = value; } }

            public string AQPNote { get; set; }

            public string AdditionalNote { get; set; }

            public string XDCEFolder { get; set; }

            /// <summary>
            /// There are two versions of these, and this one is saved with the specific PlateID, whereas the other one is user specific defaults
            /// </summary>
            public wvDisplayParams DisplayParams { get; set; }

            public short NoXDCE_ChannelCount { get; set; }
            public double NoXDCE_PixelWidthMicrons { get; set; }
            public int NoXDCE_ImageWidthPixels { get; set; }
            public string NoXDCE_WellLabel { get; set; }

            public INCELL_WV_Notes()
            {
                LoadedFromXML = false;
                AQPNote = "NA";
                AdditionalNote = "NA";
                _List = new List<INCELL_WV_Note>();
            }

            public int Count { get => _List.Count; }

            public void Add(INCELL_WV_Note wvNote)
            {
                _List.Add(wvNote);
            }

            public IEnumerator<INCELL_WV_Note> GetEnumerator()
            {
                return _List.GetEnumerator();
            }

            public string DefaultXMLPath { get => DefaultXMLPathXDCE(XDCEFolder); }
            public static string DefaultXMLPathXDCE(string XDCEFolder)
            {
                return Path.Combine(XDCEFolder, "WavelengthNotes.xml");
            }

            public static INCELL_WV_Notes Load(string XMLFullPath)
            {
                //FileInfo FI = new FileInfo(XMLFullPath);

                try
                {
                    XmlSerializer x = new XmlSerializer(typeof(INCELL_WV_Notes));
                    using (FileStream F = new FileStream(XMLFullPath, FileMode.Open))
                    {
                        INCELL_WV_Notes CR = (INCELL_WV_Notes)x.Deserialize(F);
                        F.Close();
                        CR.LoadedFromXML = true;
                        return CR;
                    }
                }
                catch (Exception E)
                {
                    string tar = E.Message;
                    return null;
                }
            }

            public static INCELL_WV_Notes Load_Folder(string XDCEFolder)
            {
                return Load(DefaultXMLPathXDCE(XDCEFolder));
            }

            public bool Save(string SavePath)
            {
                XmlSerializer x = new XmlSerializer(typeof(INCELL_WV_Notes));
                using (StreamWriter SW = new StreamWriter(SavePath))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                    return true;
                }
            }

            public bool Save()
            {
                return Save(DefaultXMLPath);
            }

            public static INCELL_WV_Notes GetFromXDCE(XDCE LookHere, string FolderPath)
            {
                if (LookHere == null) return null;
                INCELL_WV_Notes IWNs = new INCELL_WV_Notes();
                INCELL_WV_Note Note = new INCELL_WV_Note(); //I had added { WAVELENGTH_INDEX = "0" } here, but that screws up the ordering of the indices (since it thinks it has already seen that index and moves on
                IWNs.Add(Note);

                IWNs.XDCEFolder = FolderPath;
                if (LookHere.KVPs.ContainsKey("USERCOMMENT"))
                    IWNs.AQPNote = LookHere.KVPs.ContainsKey("USERCOMMENT") ? LookHere.KVPs["USERCOMMENT"].ToString() : "NA";
                IWNs.AdditionalNote = "NA";

                Type t = typeof(INCELL_WV_Note);
                System.Reflection.FieldInfo[] rFields = t.GetFields();
                Dictionary<string, System.Reflection.FieldInfo> Fields = rFields.ToDictionary(x => x.Name, x => x);
                string Fieldname; string SaveVal;
                foreach (KeyValuePair<string, object> XMLVal in LookHere.KVPs)
                {
                    if (XMLVal.Key.StartsWith("WAVEL"))
                    {
                        Fieldname = XMLVal.Key;
                        if (Fieldname.Substring(Fieldname.Length - 2, 1) == "_") Fieldname = Fieldname.Substring(0, Fieldname.Length - 2);
                        if (Fieldname.Substring(Fieldname.Length - 3, 1) == "_") Fieldname = Fieldname.Substring(0, Fieldname.Length - 3);
                        if (Fields.ContainsKey(Fieldname))
                        {
                            if (Fields[Fieldname].GetValue(Note) != null)
                            {
                                Note = new INCELL_WV_Note(); IWNs.Add(Note);
                            }
                            SaveVal = XMLVal.Value.ToString();
                            Fields[Fieldname].SetValue(Note, SaveVal);
                        }
                    }
                }
                if (IWNs.List[0].WAVELENGTH_INDEX == null) IWNs.List[0].WAVELENGTH_INDEX = "0"; //This is for No XDCE I think (swapped from the beginning on 9/1/2021)
                                                                                                //If the wavelengths aren't there, why add more 10/13/2021?
                bool addextra = false;
                if (addextra)
                {
                    for (int i = IWNs.Count; i < 5; i++)
                        IWNs.Add(new INCELL_WV_Note() { WAVELENGTH_INDEX = i.ToString() });
                }
                return IWNs;
            }

            public void CheckSave()
            {
                //Basically creates the file for this and saves it if it didn't exist
                if (!LoadedFromXML) Save();
            }

            private Dictionary<string, string> _RenameHash;
            [XmlIgnore]
            public Dictionary<string, string> RenameHash
            {
                get
                {
                    if (_RenameHash == null)
                    {
                        _RenameHash = new Dictionary<string, string>();
                        //_RenameHash.Add("1", "H"); _RenameHash.Add("2", "M"); _RenameHash.Add("3", "T"); _RenameHash.Add("4", "U");
                        foreach (INCELL_WV_Note item in List)
                        {
                            _RenameHash.Add(item.Index1.ToString(), item.Abbrev);
                        }
                    }
                    return _RenameHash;
                }
            }

            public bool HasAllAbbreviations
            {
                get
                {
                    int Count = 0;
                    foreach (INCELL_WV_Note note in List)
                    {
                        if (note.Abbrev != "-" && note.Abbrev != "") Count++;
                    }
                    return Count == List.Count;
                }
            }

            public string Rename(string originalName)
            {
                if (originalName.Contains(" wv"))
                {
                    string szWV = originalName.Substring(originalName.IndexOf(" wv") + 3);
                    string szNew = originalName.Substring(0, originalName.IndexOf(" wv") + 3);
                    if (RenameHash.ContainsKey(szWV))
                    {
                        return (szNew + RenameHash[szWV]).ToUpper().Replace("  ", " ").Trim();
                    }
                    else
                    {

                    }
                }
                return originalName.Trim().Replace("  ", " ").ToUpper();
            }
        }

        public class wvDisplayParam
        {
            private static char delim = ' ';
            public override string ToString()
            {
                return wvIdx_Txt + delim + Brightness_Txt + delim + Color;
            }
            public float Brightness { get; set; }
            public string Brightness_Txt
            {
                get => Brightness.ToString();
                set
                {
                    float T;
                    if (float.TryParse(value, out T)) Brightness = T;
                    else Brightness = 1;
                }
            }
            public short wvIdx { get; set; }
            public string wvIdx_Txt
            {
                get => wvIdx.ToString();
                set
                {
                    short T;
                    if (short.TryParse(value, out T)) wvIdx = T;
                    else wvIdx = 0;
                }
            }

            private string _Color;
            public string Color
            {
                get => _Color;
                set
                {
                    if (value == null) _Color = "FFFFF";
                    if (value == "") _Color = "FFFFF";
                    _Color = value;
                    FromString(_Color, out c_R, out c_G, out c_B);
                }
            }

            public static void FromString(string HexColor, out double cR, out double cG, out double cB)
            {
                if (HexColor == null) HexColor = "FFFFF";
                HexColor = HexColor.Trim().ToUpper();
                while (HexColor.Length < 6) HexColor += "0";
                HexColor = Regex.Replace(HexColor, "[^A-F0-9]", "");
                cR = (double)Convert.ToUInt16(HexColor.Substring(0, 2), 16) / 255;
                cG = (double)Convert.ToUInt16(HexColor.Substring(2, 2), 16) / 255;
                cB = (double)Convert.ToUInt16(HexColor.Substring(4, 2), 16) / 255;
            }

            public static Color FromStringToColor(string HexColor)
            {
                double cR, cG, cB;
                FromString(HexColor, out cR, out cG, out cB);
                var C = System.Drawing.Color.FromArgb((byte)(cR * 255), (byte)(cG * 255), (byte)(cB * 255));
                return C;
            }

            [XmlIgnore]
            public double c_R;
            [XmlIgnore]
            public double c_G;
            [XmlIgnore]
            public double c_B;

            public bool Active { get; set; }

            public float Threshold { get; set; }
            public bool ObjectAnalysis { get; set; }
            public bool DI_Analysis { get; set; }
        }

        public class wvDisplayParams //: IEnumerable<wvDisplayParam> //Screws up serialization
        {
            public override string ToString()
            {
                return ColorCalcTypeStr + " " + UseClipping.ToString() + " " + ClippingExpand_positive + "," + ClippingExpand_negative + " " +
                    string.Join("|", Actives.Select(x => x.ToString()));
            }

            public List<wvDisplayParam> List
            {
                get { return _List; }
                set { _List = value; }
            }

            private List<wvDisplayParam> _List;
            public List<wvDisplayParam> Actives { get => _List.Where(x => x.Active).ToList(); }

            public List<wvDisplayParam> Tracings { get => _List.Where(x => x.ObjectAnalysis).ToList(); }

            public string ColorCalcTypeStr { get; set; }

            public bool UseClipping { get; set; }

            public int ClippingExpand_positive { get; set; }
            public int ClippingExpand_negative { get; set; }

            public bool UseSquareMode { get; set; }

            public int Count { get => _List.Count; }

            public wvDisplayParam this[int index] { get => _List[index]; }

            public wvDisplayParams()
            {
                _List = new List<wvDisplayParam>();
            }

            public IEnumerator<wvDisplayParam> GetEnumerator()
            {
                return _List.GetEnumerator();
            }

            public void Add(wvDisplayParam wvDisplayParam)
            {
                _List.Add(wvDisplayParam);
            }



            public static string DefaultPath = Path.Combine(Path.GetTempPath(), "FIVToolsCal_settings.xml");

            public static wvDisplayParams Load()
            {
                return Load(DefaultPath);
            }

            public static wvDisplayParams Load(string Path)
            {
                if (!File.Exists(Path)) goto JUMPHEREISSUE;
                try
                {
                    using (var stream = File.OpenRead(Path))
                    {
                        var serializer = new XmlSerializer(typeof(wvDisplayParams));
                        var T = (wvDisplayParams)serializer.Deserialize(stream);
                        if (T == null) goto JUMPHEREISSUE;
                        if (T.Count == 0)
                            T.Add(new wvDisplayParam() { Active = true, Brightness = 2, Color = "FFFFFF", wvIdx = 0, ObjectAnalysis = false, Threshold = 128 });
                        return T;
                    }
                }
                catch
                {
                    goto JUMPHEREISSUE;
                }
            JUMPHEREISSUE:
                var P = new wvDisplayParams();
                P.Add(new wvDisplayParam() { Active = true, Brightness = 2, Color = "FFFFFF", wvIdx = 0, ObjectAnalysis = false, Threshold = 128 });
                P.Add(new wvDisplayParam() { Active = false, Brightness = 2, Color = "FFFFFF", wvIdx = 1, ObjectAnalysis = false, Threshold = 128 });
                P.Add(new wvDisplayParam() { Active = false, Brightness = 2, Color = "FFFFFF", wvIdx = 2, ObjectAnalysis = false, Threshold = 128 });
                P.Add(new wvDisplayParam() { Active = false, Brightness = 2, Color = "FFFFFF", wvIdx = 3, ObjectAnalysis = false, Threshold = 128 });
                P.Add(new wvDisplayParam() { Active = false, Brightness = 2, Color = "FFFFFF", wvIdx = 4, ObjectAnalysis = false, Threshold = 128 });
                return P;
            }

            public void Save()
            {
                Save(DefaultPath);
            }

            public void Save(string Path)
            {
                using (var writer = new StreamWriter(Path))
                {
                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(writer, this);
                    writer.Flush();
                }
            }
        }

        public class RaftImage_Export_Settings
        {
            public bool FileName_ByAnnotation { get; set; }
            public bool FolderName_ByAnnotation { get; set; }
            public bool Include_Fiducial_Rafts { get; set; }
            public string ExportFolderBase { get; set; }

            public bool SubFolderByPlateID = true;

            public bool SubFolderBy_PlateID_Well = false;
            public bool Resize_And_MLExport { get; set; }
            public float Min_AspectRatio_Keep { get; set; }
            public int PixelList_ExportSize { get; set; }//Makes a square image that gets exported just as pixel data (grayscale, 8 bit), only applies to exported ML
            public int PixelsExpand { get; set; }
            public bool OnlyExportSquare { get; set; }
            public bool OnlyExportFullSize { get; set; }
            public float ResizeMultiplier { get; set; }
            public float CropToMaxSize { get; set; }
            public string ImageExtension { get; set; }
            public bool Export_4Rotations { get; set; }
            public int CellImageExpandPixels { get; set; }
            public double DownsampleFraction { get; set; } //If you set this to 0.1, then it will only export 1 in every 10 cell images

            public string ColumnNameForAnnotation { get; set; }

            public RaftImage_Export_Settings()
            {

            }

            public RaftImage_Export_Settings(RaftImage_Export_Settings Copy)
            {
                foreach (var item in typeof(RaftImage_Export_Settings).GetProperties())
                {
                    item.SetValue(this, item.GetValue(Copy));
                }
            }

            public static string DefaultPath = Path.Combine(Path.GetTempPath(), "FIVTools_RaftExport_Settings.xml");


            public static RaftImage_Export_Settings Load()
            {
                return Load(DefaultPath);
            }

            public static RaftImage_Export_Settings Load(string Path)
            {
                if (!File.Exists(Path)) goto JUMPHEREISSUE;
                try
                {
                    using (var stream = File.OpenRead(Path))
                    {
                        var serializer = new XmlSerializer(typeof(RaftImage_Export_Settings));
                        var RS = (RaftImage_Export_Settings)serializer.Deserialize(stream);
                        if (RS.DownsampleFraction <= 0) RS.DownsampleFraction = 1;
                        if (RS.ResizeMultiplier <= 0) RS.ResizeMultiplier = 1;
                        return RS;
                    }
                }
                catch
                {

                }
            JUMPHEREISSUE:
                RaftImage_Export_Settings P = Defaults();
                P.Save();
                return P;
            }

            public static RaftImage_Export_Settings Defaults()
            {
                RaftImage_Export_Settings P = new RaftImage_Export_Settings();
                P.ExportFolderBase = @"c:\temp\RaftExport\";
                P.ImageExtension = "bmp";
                P.FileName_ByAnnotation = false;
                P.FolderName_ByAnnotation = true;
                P.Resize_And_MLExport = false;
                P.Include_Fiducial_Rafts = false;
                P.Min_AspectRatio_Keep = 0.75F;
                P.PixelsExpand = 0;
                P.OnlyExportSquare = true;
                P.OnlyExportFullSize = true;
                P.PixelList_ExportSize = 52;
                P.ResizeMultiplier = 1; // Settings.Resize_And_MLExport ? 0.84F : 1F;
                P.CropToMaxSize = 500; // < 1 means don't do this, never expand
                P.Export_4Rotations = false;
                P.CellImageExpandPixels = 8;
                P.DownsampleFraction = 1;
                return P;
            }

            public void Save()
            {
                Save(DefaultPath);
            }

            public void Save(string Path)
            {
                using (var writer = new StreamWriter(Path))
                {
                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(writer, this);
                    writer.Flush();
                }
            }

            public void SaveCurrent(INCELL_Folder iCFolder, wvDisplayParams WVParams, string Path)
            {
                if (WVParams == null) return;
                using (var writer = new StreamWriter(Path))
                {
                    writer.WriteLine(iCFolder.PlateID);

                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(writer, this);

                    var X = WVParams;
                    serializer = new XmlSerializer(X.GetType());
                    serializer.Serialize(writer, X);

                    writer.Flush();
                }
            }
        }

        public class INCELL_Folder
        {
            public static string XDCEFileContains = ".xdce";

            public XDCE XDCE;
            public string FullPath { get; set; }
            public string AdjustedPath
            {
                get
                {
                    return AdjustPathOverall(this.BasePath_Saved, this.BasePath_Current, FullPath);
                }
            }

            public static string DefaultMaskFolderName = "Masks";
            public static string DefaultMaskListName = "MaskList.txt";
            public string MaskList_FullPath { get => Path.Combine(MaskFolder, DefaultMaskListName); }
            public string MaskFolder { get => Path.Combine(this.AdjustedPath, DefaultMaskFolderName); }
            public static string AdjustPathOverall(string PrevBase, string NewBase, string PathToAdjust)
            {
                if (PrevBase == null) PrevBase = "E:\\Imaging\\";
                if (PrevBase.Trim().ToUpper() == NewBase.Trim().ToUpper()) return PathToAdjust;
                return PathToAdjust.Trim().ToUpper().Replace(PrevBase.Trim().ToUpper(), NewBase.Trim().ToUpper());
            }

            public static string AdjustPathOverall(INCELL_Folder Folder, string PathToAdjust)
            {
                string PrevBase = Folder.BasePath_Saved; string NewBase = Folder.BasePath_Current;
                if (PrevBase.EndsWith("\\") && !NewBase.EndsWith("\\")) NewBase += "\\";
                if (!PrevBase.EndsWith("\\") && NewBase.EndsWith("\\")) PrevBase += "\\";
                return AdjustPathOverall(PrevBase, NewBase, PathToAdjust);
            }

            private ConcurrentDictionary<string, XDCE_ImageGroup> _Rafts = null;
            [XmlIgnore]
            public ConcurrentDictionary<string, XDCE_ImageGroup> Rafts
            {
                get
                {
                    if (_Rafts == null)
                    {
                        _Rafts = new ConcurrentDictionary<string, XDCE_ImageGroup>();
                        foreach (XDCE_ImageGroup tWell in XDCE.Wells.Values)
                        {
                            foreach (KeyValuePair<string, XDCE_ImageGroup> KVP in tWell.Rafts)
                            {
                                if (!_Rafts.ContainsKey(KVP.Key))
                                    _Rafts.TryAdd(KVP.Key, KVP.Value);
                            }
                        }
                    }
                    return _Rafts;
                }
            }

            public string AQP { get => AcquisitionProtocol; }
            public string AcquisitionProtocol { get; set; }

            private string _PlateID_NoScanIndex = "|-`1|";
            public string PlateID_NoScanIndex
            {
                get
                {
                    if (_PlateID_NoScanIndex == "|-`1|")
                    {
                        _PlateID_NoScanIndex = PlateID;
                        int l = PlateID.LastIndexOf("_"); int idx2;
                        string idx = PlateID.Substring(l + 1);
                        if (int.TryParse(idx, out idx2))
                        {
                            _PlateID_NoScanIndex = PlateID.Substring(0, l);
                        }
                    }
                    return _PlateID_NoScanIndex;
                }
            }

            private string _PlateIndex = "|-1|";
            public string PlateIndex
            {
                get
                {
                    if (_PlateIndex == "|-1|")
                    {
                        var re = new Regex(@"\D+\d+\D+(\d+)");
                        var M = re.Match(PlateID_NoScanIndex);
                        if (M == null) _PlateIndex = "-1";
                        else _PlateIndex = M.Groups[1].Value.Trim();
                    }
                    return _PlateIndex;
                }
            }

            [XmlIgnore]
            public bool Type_NoXDCE = false;

            [XmlIgnore]
            public Tuple<short, double, int, string> NoXDCE_Params;

            public string PlateID { get; set; }
            [XmlIgnore]
            public string BasePath_Current { get; set; }
            public string BasePath_Saved { get; set; }

            private bool _AnalysisFolder_ReAssignParents = false;
            private List<INCARTA_Analysis_Folder> _AnalysisFolders;
            public List<INCARTA_Analysis_Folder> AnalysisFolders
            {
                get
                {
                    if (!_AnalysisFolder_ReAssignParents && _AnalysisFolders != null)
                    {
                        if (_AnalysisFolders.Count > 0)
                        {
                            foreach (INCARTA_Analysis_Folder IAF in _AnalysisFolders)
                            {
                                IAF.Parent = this;
                            }
                            _AnalysisFolder_ReAssignParents = true;
                        }
                    }
                    return _AnalysisFolders;
                }
                set
                {
                    _AnalysisFolders = value;
                }
            }

            public static INCELL_Folder CreateWithNoXDCE(string Fldr, INCELL_WV_Notes WVNotes)
            {
                return CreateWithNoXDCE(Fldr, WVNotes.NoXDCE_ChannelCount, WVNotes.NoXDCE_WellLabel, WVNotes.NoXDCE_PixelWidthMicrons, WVNotes.NoXDCE_ImageWidthPixels);
            }

            /// <summary>
            /// Creates the variables even when there is no XDCE file Present
            /// </summary>
            public static INCELL_Folder CreateWithNoXDCE(string Fldr, short Channels, string WellLabel = "A - 1", double PixelWidth_um = 0.325, int ImageWidth_Pixels = 2048)
            {
                //TODO, this should be built right into the main Constructor, but if there is no XDCE file found, it defaults to this
                var IC_F = new INCELL_Folder();
                var Well = new XDCE_ImageGroup(); Well.NameAtLevel = WellLabel; var DI = new DirectoryInfo(Fldr);
                IC_F.PlateID = DI.Name; IC_F.FullPath = Fldr;
                IC_F.XDCE = new XDCE(); IC_F.XDCE.KVPs = new Dictionary<string, object>();
                IC_F.XDCE.ParentFolder = Well.ParentFolder = IC_F;
                IC_F.XDCE.FilePath = Well.FilePath = IC_F.XDCE.FolderPath = Well.FolderPath = Fldr;
                IC_F.XDCE.Wavelength_Count = Channels;
                IC_F.XDCE.PixelWidth_in_um = IC_F.XDCE.PixelHeight_in_um = PixelWidth_um;
                IC_F.XDCE.ImageWidth_in_Pixels = IC_F.XDCE.ImageWidth_in_Pixels = ImageWidth_Pixels;
                IC_F.XDCE.Wells.Add(Well.NameAtLevel, Well);
                IC_F.Type_NoXDCE = true;
                IC_F.NoXDCE_Params = new Tuple<short, double, int, string>(Channels, PixelWidth_um, ImageWidth_Pixels, WellLabel);

                XDCE_Image xI; string FileOnly; string Ext; int FileNum = 0; int FOV; short wv; Dictionary<string, int> FOVTrack = new Dictionary<string, int>();

                var Reg = new System.Text.RegularExpressions.Regex(@"\D+(\d+)\..+");
                foreach (string file in Directory.EnumerateFiles(Fldr))
                {
                    FileOnly = Path.GetFileName(file); Ext = Path.GetExtension(file).ToUpper();
                    if (FileOnly.StartsWith("._") || FileOnly == "thumbs.db") continue;
                    if (Ext == ".TXT" || Ext == ".XML") continue;
                    if (FileOnly.EndsWith("Illum.tif"))
                    {
                        string[] FParts = FileOnly.Split('_');
                        string FKey = FParts[0] + "_" + FParts[1];
                        if (!FOVTrack.ContainsKey(FKey)) FOVTrack.Add(FKey, FOVTrack.Count);
                        FOV = FOVTrack[FKey];
                        wv = (short)(FileOnly.Contains("DAPI") ? 0 : 1);
                    }
                    else
                    {
                        var M = Reg.Match(FileOnly); if (M.Success) if (M.Groups.Count == 2) { FileNum = int.Parse(M.Groups[1].Value); }
                        FOV = (int)((double)(FileNum + 1) / Channels) - 1;
                        wv = (short)((FileNum - 1) % Channels);
                    }
                    xI = new XDCE_Image(file, IC_F.XDCE, Well.NameAtLevel, FOV, wv);
                    Well.Add(xI);
                }
                return IC_F;
            }

            private DateTime _LatestEvent = DateTime.MinValue;
            public DateTime LatestEvent
            {
                get
                {
                    if (_LatestEvent == DateTime.MinValue)
                    {
                        FileInfo FI = new FileInfo(this.XDCE.FilePath);
                        _LatestEvent = FI.CreationTime;
                    }
                    return _LatestEvent;
                }
            }

            private INCELL_WV_Notes _InCell_Wavelength_Notes = null;

            [XmlIgnore]
            public INCELL_WV_Notes InCell_Wavelength_Notes
            {
                get
                {
                    if (_InCell_Wavelength_Notes == null)
                    {
                        //First try to load from a saved file, if not regenerate from the XDCE
                        _InCell_Wavelength_Notes = INCELL_WV_Notes.Load_Folder(this.FullPath);
                        if (_InCell_Wavelength_Notes == null)
                        {
                            if (XDCE != null) _InCell_Wavelength_Notes = INCELL_WV_Notes.GetFromXDCE(XDCE, FullPath);
                        }
                        else
                        {
                            _InCell_Wavelength_Notes.XDCEFolder = this.XDCE.FolderPath; //This ensures that we don't load the wrong folder if things were moved
                        }
                        if (_InCell_Wavelength_Notes.List.Last().WAVELENGTH_IMAGING_MODE == null)
                        {
                            //This is to try and get rid of empty wv that sometimes creep in . . 
                            _InCell_Wavelength_Notes.List.RemoveAt(_InCell_Wavelength_Notes.Count - 1);
                        }
                    }
                    return _InCell_Wavelength_Notes;
                }
            }

            public override string ToString()
            {
                return PlateID;
            }

            public INCELL_Folder()
            {
                FullPath = ""; PlateID = ""; AcquisitionProtocol = "";
                AnalysisFolders = new List<INCARTA_Analysis_Folder>();
            }

            public INCELL_Folder(string XDCEFolder)
            {
                var attr = File.GetAttributes(XDCEFolder);
                if (!attr.HasFlag(FileAttributes.Directory)) FullPath = Path.GetDirectoryName(XDCEFolder); else FullPath = XDCEFolder;
                //Now check whether there is an XDCE File
                if (XDCE.GetXDCEPath(XDCEFolder) == "")
                {
                    //Can't do this in here, but you can do this >
                    //CreateWithNoXDCE();
                }
                else
                {
                    XDCE = new XDCE(XDCEFolder);
                    XDCE.ParentFolder = this;
                    DirectoryInfo DI = new DirectoryInfo(FullPath);
                    DirectoryInfo DP = DI.Parent;
                    AcquisitionProtocol = DP.Name;

                    int expected_PlateIDLen = (DI.Name.Length - AcquisitionProtocol.Length) - 1;
                    PlateID = DI.Name.Replace(AcquisitionProtocol + "_", ""); //Problem with this is it may multi-replace
                    if (PlateID.Length < (expected_PlateIDLen - 1))
                    {
                        PlateID = DI.Name.Substring(AcquisitionProtocol.Length + 1);
                    }

                    DirectoryInfo DI_Analysis = new DirectoryInfo(Path.Combine(FullPath, "Results"));
                    if (DI_Analysis.Exists)
                        AnalysisFolders = INCARTA_Analysis_Folder.GetAnalysesFromFolder(DI_Analysis.FullName, this);
                    else
                        AnalysisFolders = new List<INCARTA_Analysis_Folder>();
                }
            }

            public static List<INCELL_Folder> GetFromFolder(string BasePath) { return GetFromFolder(BasePath, null); }

            public static List<INCELL_Folder> GetFromFolder(string BasePath, BackgroundWorker BW, string LimitSearch = "")
            {
                List<FileInfo> Files = new List<FileInfo>();
                List<INCELL_Folder> folders = new List<INCELL_Folder>();
                INCELL_Folder tFolder; FileInfo FI;
                DirectoryInfo DI = new DirectoryInfo(BasePath);
                //- - This version only took 11 s !!
                foreach (DirectoryInfo DIsub in DI.GetDirectories())
                {
                    foreach (DirectoryInfo DIPlate in DIsub.GetDirectories())
                    {
                        //if (DIPlate.Name.Contains("701"))  { }
                        if (LimitSearch != "") { if (!DIPlate.Name.Contains(LimitSearch)) continue; }
                        if (BW == null) Debug.Print(Path.GetFileNameWithoutExtension(DIPlate.Name)); else BW.ReportProgress(0, DIPlate.Name);
                        //First guess on the XDCE name, if it doesn't exist then search
                        FI = new FileInfo(Path.Combine(DIPlate.FullName, DIPlate.Name + ".xdce"));
                        if (FI.Exists)
                        {
                            Files.Add(FI);
                        }
                        else
                        {
                            //Debug.Print(DIPlate.Name);
                            //Skip these for now . . 
                            Files.AddRange(DIPlate.GetFiles("*.xdce", SearchOption.TopDirectoryOnly).ToList());
                        }
                    }
                }
                if (BW == null) Debug.Print("\r\n - - Loading - - \r\n"); else BW.ReportProgress(0, "\r\n - - Loading - - \r\n");
                foreach (FileInfo file in Files)
                {
                    //if (file.Name.Contains("701")) { }
                    if (LimitSearch != "") { if (!file.Name.Contains(LimitSearch)) continue; }
                    if (BW == null) Debug.Print(file.Name); else BW.ReportProgress(0, "^" + Path.GetFileNameWithoutExtension(file.Name));
                    tFolder = new INCELL_Folder(file.FullName);
                    tFolder.BasePath_Current = BasePath;
                    folders.Add(tFolder);
                }
                if (BW == null) Debug.Print("\r\n - - Saving ..  - - \r\n"); else BW.ReportProgress(0, "\r\n - - Saving .. - - \r\n");
                return folders;
            }

            internal void RemoveAF(INCARTA_Analysis_Folder AF)
            {
                AnalysisFolders.Remove(AF);
            }

            internal void ReAssociate(string BasePath)
            {
                BasePath_Current = BasePath;
                XDCE.ParentFolder = this;
            }

            /// <summary>
            /// The point of this is to regenerate (quickly) and see what new images are present, usually for the NoXDCE version
            /// </summary>
            public void RefreshImages()
            {
                throw new NotImplementedException();
            }

            public bool HasMasks
            {
                get
                {
                    DirectoryInfo DI = new DirectoryInfo(MaskFolder);
                    return DI.Exists;
                }
            }

            private string _FIViD = "";
            public string FIViD
            {
                get
                {
                    if (_FIViD == "")
                    {
                        if (PlateID.Length < 6) return PlateID;
                        Regex R = new Regex(@"(FI\D\d\d\d)\D");
                        Match M = R.Match(PlateID);
                        if (M.Success) _FIViD = M.Groups[1].Value; else _FIViD = PlateID.Substring(6);
                    }
                    return _FIViD;
                }
                set { _FIViD = value; }
            }

            public static string ErrorLog { get; set; }
        }

        public class INCARTA_Analysis_Folder
        {
            public static string CellAnalysisFileContains = "_Single_Target_Data.csv";
            public static List<string> FilesInFolder_INCARTAAnalysis = new List<string>(1) { CellAnalysisFileContains };
            public static List<INCARTA_Analysis_Folder> GetAnalysesFromFolder(string BasePath, INCELL_Folder ParentFolder)
            {
                DirectoryInfo DI = new DirectoryInfo(BasePath);
                INCARTA_Analysis_Folder IAF;
                List<INCARTA_Analysis_Folder> Analyses = new List<INCARTA_Analysis_Folder>();
                List<INCARTA_Analysis_Folder> TList;
                if (ContainsAnalysisFiles(DI))
                {
                    IAF = GetAnalysisFromFolder(DI.FullName, ParentFolder);
                    Analyses.Add(IAF); return Analyses;
                }
                foreach (DirectoryInfo DISub in DI.GetDirectories())
                {
                    if (DISub.Name == "thumbs" || DISub.Name == "temp") continue;
                    TList = GetAnalysesFromFolder(DISub.FullName, ParentFolder);
                    Analyses.AddRange(TList);
                }
                return Analyses;
            }
            public static INCARTA_Analysis_Folder GetAnalysisFromFolder(string fullName, INCELL_Folder ParentFolder)
            {
                INCARTA_Analysis_Folder IAF = new INCARTA_Analysis_Folder(fullName, ParentFolder);
                return IAF;
            }
            public static bool ContainsAnalysisFiles(DirectoryInfo DI)
            {
                foreach (string item in FilesInFolder_INCARTAAnalysis)
                {
                    FileInfo[] files = DI.GetFiles("*" + FilesInFolder_INCARTAAnalysis[0], SearchOption.TopDirectoryOnly);
                    if (files.Length > 0) return true;
                }
                return false;
            }

            //----------------------------------------------------------------------------------------

            public string FolderPath { get; set; }
            public string FolderPath_GenImages { get => Path.Combine(FolderPath, "GeneratedImages"); }
            public string FolderName { get; set; }
            public string Path_CellExport { get; set; }
            public string Path_CellExport_Adjusted
            {
                get
                {
                    return INCELL_Folder.AdjustPathOverall(Parent, Path_CellExport);
                }
            }
            public string AQP { get => AcquisitionProtocol; }
            public string AcquisitionProtocol { get; set; }
            public string PlateID { get; set; }
            public string INCARTA_ProtocolName { get; set; }
            public string ResultsID { get; set; }
            [XmlIgnore]
            public XDCE RelatedXDCE { get => Parent.XDCE; }
            [XmlIgnore]
            public INCELL_Folder Parent { get; set; }

            public int Index { get; set; }
            private static int IndexCounter = 0;

            public override string ToString()
            {
                return PlateID;
            }

            public INCARTA_Analysis_Folder(string FolderPath, INCELL_Folder ParentFolder)
            {
                Parent = ParentFolder;
                this.FolderPath = FolderPath;
                Init();
            }

            public INCARTA_Analysis_Folder()
            {
                Parent = null;
                this.FolderPath = "";
                Init();
            }

            private void Init()
            {
                Index = IndexCounter++;
                if (FolderPath != "")
                {
                    DirectoryInfo DI = new DirectoryInfo(FolderPath);
                    //Determine the Cell Analysis File (CSV)
                    foreach (FileInfo file in DI.GetFiles())
                    {
                        if (file.Name.Contains(CellAnalysisFileContains))
                        {
                            Path_CellExport = file.FullName;
                            break;
                        }
                    }
                    if (Path_CellExport == null || Path_CellExport == "")
                    {
                    }
                    //Figure out the name
                    DirectoryInfo DP = DI.Parent;
                    DP = DP.Parent;
                    FolderName = DP.Name;
                    AcquisitionProtocol = DP.Parent.Name;
                    PlateID = FolderName.Replace(AcquisitionProtocol + "_", "");
                    int l1 = DI.Name.LastIndexOf("_");
                    if (l1 < 2)
                    {
                        INCARTA_ProtocolName = "_Unknown";
                        ResultsID = DI.Name;
                    }
                    else
                    {
                        INCARTA_ProtocolName = DI.Name.Substring(0, l1);
                        ResultsID = DI.Name.Substring(l1 + 1);
                    }
                }
            }

            internal static string ExportLine(char delim)
            {
                return "AQP" + delim + "PlateID" + delim + "INCARTA_ProtocolName" + delim + "ResultsID" + delim + "Index";
            }

            internal string ExportLineThis(char delim)
            {
                return AQP + delim + PlateID + delim + INCARTA_ProtocolName + delim + ResultsID + delim + Index;
            }

            public void Delete()
            {
                DirectoryInfo DI = new DirectoryInfo(this.FolderPath);
                //Deletes this whole analysis folder and removes from Parent
                Parent.RemoveAF(this);
                DI.Delete(true);
            }

            /// <summary>
            /// This isn't implemented yet
            /// </summary>
            public void ExportTracedImages()
            {
                foreach (XDCE_Image image in RelatedXDCE.Images)
                {
                    string f = image.FileName_GenImageBase;
                    string h = this.FolderPath_GenImages;

                    //lbl-cell0-B-2(F1 Z0 T0 W2)
                    //lbl-cell0-B-3(F48 Z0 T0 W2)_cell_zoi
                    //lbl-nuc0-B-2(F4 Z0 T0 W1)
                    //seg-cell0-B-3(F8 Z0 T0 W2)
                    //seg-nuc0-B-2(F24 Z0 T0 W1)

                }
            }

            [XmlIgnore]
            public string[] GenImages_AppendList
            {
                get => GenImages_Dict.Keys.ToArray();
            }

            [XmlIgnore]
            public Dictionary<string, string[]> GenImages_Dict;

            public void Refresh_GenList()
            {
                GenImages_Dict = new Dictionary<string, string[]>();

                DirectoryInfo DI = new DirectoryInfo(FolderPath_GenImages);
                List<string> tList = new List<string>();

                Regex Re = new System.Text.RegularExpressions.Regex(@"(\w+-\w+\d-).+\(\w\d+ \w\d+ \w\d+ W(\d+)\)(.*)");
                MatchCollection MC; string t; string key; string[] arr;
                foreach (FileInfo fi in DI.GetFiles())
                {
                    t = Path.GetFileNameWithoutExtension(fi.Name);
                    MC = Re.Matches(t); if (MC.Count != 1) continue;
                    //if (!MC[0].Groups[1].Value.StartsWith("seg")) continue; //this exclude the labels, which are encoded into the tifs
                    arr = new string[3] { MC[0].Groups[1].Value, MC[0].Groups[2].Value, MC[0].Groups[3].Value };
                    key = String.Join("|", arr);
                    if (!GenImages_Dict.ContainsKey(key)) GenImages_Dict.Add(key, arr);
                }

                //>The segs with no other _XXX are usually the best ones to display
                //                _wcells == bin3
                //_skel = _imskel(very thin, skeletons), _imBF similar, but less scaled
                //_prob > saled does have data. .can see neurites

            }
        }
    }

    namespace ImageCheck
    {
        public class ImageCheck_PointList
        {
            public string Name { get; set; }
            public string Well { get; set; }
            public DateTime Created { get; set; }
            public List<ImageCheck_Point> Points { get; set; }

            public string Errors = "";

            public int Count { get => Points.Count; }
            public void Add(ImageCheck_Point Point)
            {
                Points.Add(Point);
                AddToDictionaries(Point);
                Dirty_Saved = true;
            }

            public void RemoveLast()
            {
                Dirty_Saved = true;
                ImageCheck_Point Pt = Points.Last();
                Points.Remove(Pt);
                _Dict = new Dictionary<string, List<ImageCheck_Point>>();
                _DictFOV = new Dictionary<int, List<ImageCheck_Point>>();
                _DictRaftID = new Dictionary<string, List<ImageCheck_Point>>();
                RefreshDictionaries();
            }

            public void RemovePoints(List<ImageCheck_Point> pointsToRemove)
            {
                Dirty_Saved = true;
                Points = Points.Except(pointsToRemove).ToList();
                _Dict = new Dictionary<string, List<ImageCheck_Point>>();
                _DictFOV = new Dictionary<int, List<ImageCheck_Point>>();
                _DictRaftID = new Dictionary<string, List<ImageCheck_Point>>();
                RefreshDictionaries();
            }

            private void AddToDictionaries(ImageCheck_Point Point)
            {
                string key1 = KeyMaker(Point);
                if (!_Dict.ContainsKey(key1)) _Dict.Add(key1, new List<ImageCheck_Point>());
                _Dict[key1].Add(Point);
                if (Point.RaftID != null)
                {
                    if (!_DictRaftID.ContainsKey(Point.RaftID)) _DictRaftID.Add(Point.RaftID, new List<ImageCheck_Point>());
                    _DictRaftID[Point.RaftID].Add(Point);
                }
                if (!_DictFOV.ContainsKey(Point.FOV)) _DictFOV.Add(Point.FOV, new List<ImageCheck_Point>());
                _DictFOV[Point.FOV].Add(Point);
            }

            internal void RefreshDictionaries()
            {
                //Also fix up the colors
                foreach (ImageCheck_Point Point in Points)
                {
                    foreach (ImageCheck_Annotation ICA in Point.Annotations)
                    {
                        if (ICA.Color.IsEmpty && ICA.Name == "General" && (string)ICA.Value == "Bad") ICA.Color = System.Drawing.Color.Red;
                        if (ICA.Color.IsEmpty && ICA.Name == "General" && (string)ICA.Value == "Good") ICA.Color = System.Drawing.Color.LightGreen;
                    }
                    AddToDictionaries(Point);
                }
            }

            public ImageCheck_Point this[int index] { get => Points[index]; }

            public ImageCheck_PointList()
            {
                Points = new List<ImageCheck_Point>();
            }

            [XmlIgnore]
            public bool Dirty_Saved = false;

            public bool Save(string SavePath)
            {
                Created = DateTime.Now;
                XmlSerializer x = new XmlSerializer(typeof(ImageCheck_PointList));
                using (StreamWriter SW = new StreamWriter(SavePath))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                    Dirty_Saved = false;
                    return true;
                }
            }

            public bool SaveDefault(string FolderPath)
            {
                string FullPath = Path.Combine(FolderPath, DefaultName);
                return Save(FullPath);
            }

            public string DefaultName { get => DefaultNameConstruct(Well); }
            public static string DefaultNameConstruct(string WellLabel = "") { return (DefaultSaveName + " " + WellLabel).Trim() + "." + DefaultExtension; }
            public static string DefaultSaveName = "ImageCheckList";
            public static string DefaultExtension = "XML";

            private Dictionary<string, List<ImageCheck_Point>> _Dict = new Dictionary<string, List<ImageCheck_Point>>();
            private Dictionary<string, List<ImageCheck_Point>> _DictRaftID = new Dictionary<string, List<ImageCheck_Point>>();
            private Dictionary<int, List<ImageCheck_Point>> _DictFOV = new Dictionary<int, List<ImageCheck_Point>>();
            public List<int> FOVs { get => _DictFOV.Keys.ToList(); }
            public IEnumerable<string> WellFields { get => _Dict.Keys; }

            public string Overview
            {
                get
                {
                    var AnnoCount = new Dictionary<string, int>();
                    foreach (var ICPoint in _Dict.Values)
                    {
                        foreach (var i2 in ICPoint)
                        {
                            foreach (var Anno in i2.Annotations)
                            {
                                if (!AnnoCount.ContainsKey(Anno.Value.ToString())) AnnoCount.Add(Anno.Value.ToString(), 0);
                                AnnoCount[Anno.Value.ToString()]++;
                            }
                        }
                    }
                    var SB = new StringBuilder();
                    foreach (var KVP in AnnoCount)
                        SB.Append((SB.Length < 1 ? "" : ", ") + KVP.Key + ":" + KVP.Value);
                    return SB.ToString();
                }
            }

            public static string KeyMaker(string well_Row, int well_Col, int fOV, int Wavelength) { return well_Row + "_" + well_Col + "-" + fOV + ":0" /* + Wavelength*/; }
            public static string KeyMaker(ImageCheck_Point P) { return P.Well_Row + "_" + P.Well_Col + "-" + P.FOV + ":0" /* + P.WaveLength */; }

            public List<ImageCheck_Point> FromDictionary(string well_Row, int well_Col, int fOV, int Wavelength)
            {
                List<ImageCheck_Point> RetList = new List<ImageCheck_Point>();
                string Key = KeyMaker(well_Row, well_Col, fOV, Wavelength);
                if (_Dict.ContainsKey(Key)) RetList = _Dict[Key];
                return RetList;
            }

            public IEnumerable<string> RaftIDs => _DictRaftID.Keys;

            public List<ImageCheck_Point> FromDictionaryRaftID(string raftID)
            {
                var RetList = new List<ImageCheck_Point>();
                if (_DictRaftID.ContainsKey(raftID)) RetList = _DictRaftID[raftID];
                return RetList;
            }

            public List<ImageCheck_Point> FromDictionarySpecific(string wellLabel, string fOV, string raftID)
            {
                var RetList = new List<ImageCheck_Point>();
                if (_DictRaftID.ContainsKey(raftID)) RetList = _DictRaftID[raftID];
                if (wellLabel == "" && fOV == "") return RetList;

                RetList.RemoveAll(x => x.Well_Label != wellLabel || x.FOV.ToString() != fOV);
                return RetList;
            }

            public string FromDictionaryRaftID_Combined(string raftID)
            {
                List<ImageCheck_Point> RetList = FromDictionaryRaftID(raftID);
                if (RetList.Count == 0) return "";
                HashSet<string> Total = new HashSet<string>();
                foreach (var item in RetList) foreach (var item2 in item.Annotations) Total.Add(item2.Value.ToString()); //I tried to do this with LINQ but had a little trouble
                string T = string.Join(" ", Total);
                if (T.Length > 23)
                {

                }
                return T;
            }

            public List<ImageCheck_Point> FromDictionaryFOV(int FOV)
            {
                List<ImageCheck_Point> RetList = new List<ImageCheck_Point>();
                if (_DictFOV.ContainsKey(FOV)) RetList = _DictFOV[FOV];
                return RetList;
            }

            public string FromDictionaryFOV_Combined(int FOV)
            {
                List<ImageCheck_Point> RetList = FromDictionaryFOV(FOV);
                if (RetList.Count == 0) return "";
                string T = RetList.Select(x => x.Result()).Distinct().Aggregate((v1, v2) => v1 + " " + v2);
                //string T = RetList.Select(x => x.Result()).Aggregate((v1, v2) => v1 + " " + v2);
                return T;
            }

            public List<ImageCheck_Point> FromDictionaryWellField(string well_Field)
            {
                List<ImageCheck_Point> RetList = new List<ImageCheck_Point>();
                RetList = _Dict[well_Field];
                return RetList;
            }

            internal static ImageCheck_PointList Load_Assumed(string FolderThatXDCEisIn, string WellLabel = "")
            {
                string PathAssumed = Path.Combine(FolderThatXDCEisIn, DefaultNameConstruct(WellLabel));
                FileInfo FI = new FileInfo(PathAssumed);
                if (FI.Exists) return Load(PathAssumed);
                return null;
            }

            public static ImageCheck_PointList Load(string LoadPath)
            {
                XmlSerializer x = new XmlSerializer(typeof(ImageCheck_PointList));
                using (FileStream F = new FileStream(LoadPath, FileMode.Open))
                {
                    try
                    {
                        ImageCheck_PointList CR = (ImageCheck_PointList)x.Deserialize(F);
                        F.Close();
                        CR.RefreshDictionaries();
                        return CR;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }

            public IEnumerator<List<ImageCheck_Point>> GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

            public static bool FromRaft(InCellLibrary.INCELL_Folder IC_Folder, RaftInfoS RaftInfo, ImageCheck_PointList ICPL, bool ExportMultiFieldsForSameRaft_ID = true)
            {
                string rID = RaftInfo.RaftID; HashSet<string> FOV_UniqueT; InCellLibrary.XDCE_Image Img;
                if (rID.Contains("/") || rID.Contains(".") || rID.Contains("-") || rID.Contains(",")) return false;
                if (IC_Folder.Rafts.ContainsKey(rID))
                {
                    InCellLibrary.XDCE_ImageGroup SIG = IC_Folder.Rafts[rID];
                    if ((RaftInfo.FOV != "") && (RaftInfo.WellLabel != ""))
                    {
                        //If the FOV and Well are specified, then do it directly
                        Img = SIG.GetImage_FromWellFOV(RaftInfo.WellField);
                        if (Img == null) { ICPL.Errors += RaftInfo.FOV + "_" + rID + "\t"; }
                        else
                        {
                            var ICP = ImageCheck_Point.FromImage(Img, rID);
                            ICP.Annotations.Add(new ImageCheck_Annotation() { Value = RaftInfo.ClassLabel });
                            ICPL.Add(ICP);
                        }
                        return true;
                    }
                    else
                    {

                    }
                    //If they aren't specified, then look through them
                    FOV_UniqueT = new HashSet<string>(SIG.Images.Select(x => x.WellField));
                    foreach (string FOV in FOV_UniqueT)
                    {
                        Img = SIG.GetImage_FromWellFOV(FOV);
                        ICPL.Add(ImageCheck_Point.FromImage(Img, rID));
                        if (!ExportMultiFieldsForSameRaft_ID) continue;
                        //Most of the multi are actually just a sliver, and will get thrown out if we don't do it like this
                    }
                }
                return true;
            }

            public static ImageCheck_PointList FromRafts(InCellLibrary.INCELL_Folder IC_Folder, HashSet<RaftInfoS> Rafts)
            {
                // *** Restructure this so that each entry doesn't need to have a field/well as is the case here
                var T = IC_Folder.Rafts; //Not sure if this is necessary, but it seems to help, otherwise the Rafts won't be found

                ImageCheck_PointList ICPL = new ImageCheck_PointList();
                bool ExportMultiFieldsForSameRaft_ID = true;
                //if (RaftIDs == null) RaftIDs = IC_Folder.Rafts.Keys;
                foreach (var rIS in Rafts)
                {
                    FromRaft(IC_Folder, rIS, ICPL, ExportMultiFieldsForSameRaft_ID);
                }
                if (ICPL.Errors != "")
                {

                }
                return ICPL; //Check error list
            }

            public static ImageCheck_PointList FromRafts(InCellLibrary.INCELL_Folder IC_Folder, IEnumerable<string> RaftIDs = null)
            {
                ImageCheck_PointList ICPL = new ImageCheck_PointList(); InCellLibrary.XDCE_Image Img; List<string> Removed = new List<string>();
                HashSet<string> FOV_UniqueT; bool ExportMultiFieldsForSameRaft_ID = true;
                if (RaftIDs == null) RaftIDs = IC_Folder.Rafts.Keys;
                foreach (string rID in RaftIDs)
                {
                    if (rID.Contains("/") || rID.Contains(".") || rID.Contains("-") || rID.Contains(",")) continue;
                    if (IC_Folder.Rafts.ContainsKey(rID))
                    {
                        InCellLibrary.XDCE_ImageGroup SIG = IC_Folder.Rafts[rID];
                        FOV_UniqueT = new HashSet<string>(SIG.Images.Select(x => x.WellField));

                        foreach (string FOV in FOV_UniqueT)
                        {
                            Img = SIG.GetImage_FromWellFOV(FOV);
                            ICPL.Add(ImageCheck_Point.FromImage(Img, rID));
                            if (!ExportMultiFieldsForSameRaft_ID) continue;
                            //Most of the multi are actually just a sliver, and will get thrown out if we don't do it like this
                        }
                        Removed.Add(rID);
                    }
                }
                return ICPL;
            }

            public static ImageCheck_PointList FromWellFieldCells(InCellLibrary.INCELL_Folder IC_Folder, Dictionary<string, HashSet<CellInfoS>> WellField_Cells)
            {
                ImageCheck_PointList ICPL = new ImageCheck_PointList(); InCellLibrary.XDCE_Image Img;
                foreach (var KVP in WellField_Cells)
                {
                    if (IC_Folder.XDCE.WellFOV_Dict.ContainsKey(KVP.Key))
                    {
                        InCellLibrary.XDCE_ImageGroup SIG = IC_Folder.XDCE.WellFOV_Dict[KVP.Key];
                        Img = SIG.Images[0];
                        foreach (var Cell in KVP.Value) ICPL.Add(ImageCheck_Point.FromImageCell(Img, Cell));
                    }
                }
                return ICPL;
            }

        }

        public class ImageCheck_Point
        {
            public double Plate_Xum { get; set; }
            public double Plate_Yum { get; set; }
            public float PixelX_Fraction { get; set; }
            public float PixelY_Fraction { get; set; }
            public int WaveLength { get; set; }
            public string Well_Row { get; set; }
            public string Well_Col { get; set; }

            [XmlIgnore]
            public string Well_Label { get => Well_Row + " - " + Well_Col; }
            public int FOV { get; set; }
            public string RaftID { get; set; }
            public List<ImageCheck_Annotation> Annotations { get; set; }

            [XmlIgnore]
            public bool IsCell = false;

            [XmlIgnore]
            public CellInfoS CellInfo = null;

            public Rectangle CellCoordinates(InCellLibrary.XDCE_Image XI, int Expand)
            {
                int L = (int)Math.Max(0, CellInfo.R.Left - Expand);
                int T = (int)Math.Max(0, CellInfo.R.Top - Expand);
                int R = (int)Math.Min(XI.Width_Pixels, CellInfo.R.Right + Expand);
                int B = (int)Math.Min(XI.Height_Pixels, CellInfo.R.Bottom + Expand);
                return new Rectangle(L, T, R - L, B - T);
            }

            public override string ToString()
            {
                return Well_Row + Well_Col;
            }

            public int Count { get => Annotations.Count; }
            public bool IsBad
            {
                get
                {
                    if (Annotations.Count < 1) return false;
                    return Annotations.Select(x => x.Value).Contains(ImageCheck_Annotation.Bad.Value);   //Annotations.Contains(ImageCheck_Annotation.Bad);
                }
            }

            public string Result()
            {
                string ret = Annotations.Select(x => x.Value).Aggregate((v1, v2) => string.Format("{0} {1}", v1, v2)).ToString();
                return ret;
            }

            public void Add(ImageCheck_Annotation imageCheck_Annotation)
            {
                Annotations.Add(imageCheck_Annotation);
            }

            public static ImageCheck_Point FromImage(InCellLibrary.XDCE_Image xDCE_Image, string RaftID)
            {
                var ICP = new ImageCheck_Point();
                ICP.RaftID = RaftID;
                ICP.FOV = xDCE_Image.FOV;
                ICP.Well_Row = xDCE_Image.Well_Row;
                ICP.Well_Col = xDCE_Image.Well_Column.ToString();
                if (xDCE_Image.Rafts.ContainsKey(RaftID))
                {
                    //Old style
                    //var Points = (Tuple<double, double, float, float>)xDCE_Image.KVPs[RaftID]; //Refresh_Wells_Rafts() is what makes the points in the first place
                    //ICP.Plate_Xum = Points.Item1; ICP.Plate_Yum = Points.Item2;
                    //ICP.PixelX_Fraction = Points.Item3; ICP.PixelY_Fraction = Points.Item4;

                    var xiRI = xDCE_Image.Rafts[RaftID]; //New style 5/24/2022
                    ICP.Plate_Xum = xiRI.FullX; ICP.Plate_Yum = xiRI.FullY;
                    ICP.PixelX_Fraction = xiRI.FracXY.X; ICP.PixelY_Fraction = xiRI.FracXY.Y;
                }
                return ICP;
            }

            public static ImageCheck_Point FromImageCell(InCellLibrary.XDCE_Image xDCE_Image, CellInfoS cell)
            {
                ImageCheck_Point ICP = new ImageCheck_Point();
                ICP.FOV = xDCE_Image.FOV;
                ICP.Well_Row = xDCE_Image.Well_Row;
                ICP.Well_Col = xDCE_Image.Well_Column.ToString();

                ICP.IsCell = true;
                ICP.CellInfo = cell;

                return ICP;
            }

            public ImageCheck_Point()
            {
                Annotations = new List<ImageCheck_Annotation>();
            }


        }

        public class ImageCheck_Annotation_Schema : IEnumerable<ImageCheck_Annotation>
        {
            //And if your class inherits a list and also has its own members, only the elements of the list get serialized. The data present in your class members is not captured. (From Stack Overflow)
            //I had this as inheriting IENumerable, but it didn't serialize other members

            private List<ImageCheck_Annotation> _List;
            private Dictionary<char, ImageCheck_Annotation> _DictMod;
            private Dictionary<String, ImageCheck_Annotation> _DictValue;

            public string Name { get; set; }
            public static ImageCheck_Annotation_Schema ActiveSchema = Neuronal_Schema();

            public static ImageCheck_Annotation_Schema Neuronal_Schema()
            {
                var ICAS = new ImageCheck_Annotation_Schema();

                ICAS.Add("Neurons", "Unique 0", Color.LightGray, '0', 1);
                ICAS.Add("Neurons", "Poor focus", Color.DarkRed, '1', 1);
                ICAS.Add("Neurons", "No Neurons or dead", Color.Purple, '2', 1);
                ICAS.Add("Neurons", "Overgrown", Color.DarkOliveGreen, '3', 1);
                ICAS.Add("Neurons", "Unsure", Color.Yellow, '4', 1);
                ICAS.Add("Neurons", "Straight neurites or not adhered", System.Drawing.Color.Orange, '5', 1);
                ICAS.Add("Neurons", "Degenerated axons", System.Drawing.Color.Pink, '6', 1);
                ICAS.Add("Neurons", "Retraction bulbs", System.Drawing.Color.LightBlue, '7', 1);
                ICAS.Add("Neurons", "Intact axons", System.Drawing.Color.LightGreen, '8', 1);
                ICAS.Add("Neurons", "Short axons", Color.White, '9', 1);

                return ICAS;
            }

            public ImageCheck_Annotation_Schema()
            {
                _List = new List<ImageCheck_Annotation>();
                _DictMod = new Dictionary<char, ImageCheck_Annotation>();
                _DictValue = new Dictionary<string, ImageCheck_Annotation>();
            }

            public ImageCheck_Annotation FromValue(string Value)
            {
                if (_DictValue.ContainsKey(Value)) return _DictValue[Value];
                else return null;
            }

            public ImageCheck_Annotation FromModifier(char mod_key)
            {
                if (_DictMod.ContainsKey(mod_key))
                    return _DictMod[mod_key];
                else
                    return _List.First();
                //return new ImageCheck_Annotation() { Name = "New", Value = mod_key, Color = System.Drawing.Color.Magenta };
            }

            public void Add(string Name, object Value, char DefaultKey = ':', object DefaultButton = null)
            {
                var Annotate = new ImageCheck_Annotation() { Name = Name, Value = Value, Default_Key = DefaultKey, Default_Button = DefaultButton, Color = System.Drawing.Color.White };
                Add(Annotate);
            }

            public void Add(string Name, object Value, Color Color, char DefaultKey = ':', object DefaultButton = null)
            {
                var Annotate = new ImageCheck_Annotation() { Name = Name, Value = Value, Default_Key = DefaultKey, Default_Button = DefaultButton, Color = Color };
                Add(Annotate);
            }

            public void Add(ImageCheck_Annotation Annotation)
            {
                _List.Add(Annotation);
                _DictMod.Add(Annotation.Default_Key, Annotation);
                _DictValue.Add(Annotation.Value.ToString(), Annotation);
            }

            public string GetAsText(string delim = "\t")
            {
                return string.Join("\r\n", _List.Select(x => x.Name + delim + x.Default_Key + delim + x.Value));
            }

            public static ImageCheck_Annotation_Schema Load(string Path)
            {
                try
                {
                    var x = new XmlSerializer(typeof(ImageCheck_Annotation_Schema));
                    using (var F = new FileStream(Path, FileMode.Open))
                    {
                        var CR = (ImageCheck_Annotation_Schema)x.Deserialize(F);
                        F.Close();
                        return CR;
                    }
                }
                catch
                {
                    return Neuronal_Schema();
                }
            }

            public void Save(string Path)
            {
                var x = new XmlSerializer(typeof(ImageCheck_Annotation_Schema));
                using (var SW = new StreamWriter(Path))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                }
            }

            public IEnumerator<ImageCheck_Annotation> GetEnumerator()
            {
                return _List.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _List.GetEnumerator();
            }
        }

        public class ImageCheck_Annotation
        {
            /// <summary>
            /// Like the type or subset of annotation
            /// </summary>
            public string Name { get; set; }
            public object Value { get; set; }
            public char Default_Key { get; set; }
            public object Default_Button { get; set; }
            public float Score { get; set; }
            public string Note { get; set; }
            public bool Ignore { get; set; }

            public int[] FColor { get; set; }

            [XmlIgnore]
            public System.Drawing.Color Color
            {
                get
                {
                    if (FColor == null) return Color.Empty;
                    //if (FColor == null) FColor = new int[3] { 255, 255, 255 };
                    return Color.FromArgb(FColor[0], FColor[1], FColor[2]);
                }
                set
                {
                    FColor = new int[3] { value.R, value.G, value.B };
                }
            }

            public override string ToString()
            {
                return Value + (float.IsNaN(Score) ? "" : " (" + Score.ToString("0.00") + ")") + (Name == "" ? "" : " : " + Name);
            }

            public ImageCheck_Annotation()
            {
                Ignore = false;
                Score = float.NaN;
                Note = "";
                Name = "";
                Value = "";
                Color = Color.MintCream;
            }

            /// <summary>
            /// Default Key
            /// </summary>
            /// <returns></returns>
            public string Report()
            {
                return Default_Key + " = " + Name + " / " + Value;
            }

            public static ImageCheck_Annotation Bad = new ImageCheck_Annotation() { Name = "General", Value = "Bad", Color = System.Drawing.Color.Red };
            public static ImageCheck_Annotation Good = new ImageCheck_Annotation() { Name = "General", Value = "Good", Color = System.Drawing.Color.LightGreen };
        }

    }

    /// <summary>
    /// Basically just for exporting the cell coordinates
    /// </summary>
    public class CellInfoS : RaftInfoS
    {
        public string ObjectID;
        public RectangleF R;

        public CellInfoS(string cPlate, string cWell, string cFOV, string cObjectID, string L, string T, string W, string H)
        {
            PlateID = cPlate;
            WellLabel = cWell;
            FOV = cFOV;
            RaftID = "";
            ObjectID = cObjectID;
            if (L == "") return;
            float fL = float.Parse(L);
            float fT = float.Parse(T);
            float fW = float.Parse(W);
            float fH = float.Parse(H);
            R = new RectangleF(fL, fT, fW, fH);
        }

        public CellInfoS NearestNeighbor = null;
        public double NNDist = double.NaN;

        public long ConnectID = -1;
        public double ConnectDist = double.NaN;

        public override string ToString()
        {
            return CenterPoint.ToString();
        }

        private PointF _CenterPoint = PointF.Empty;
        public PointF CenterPoint
        {
            get
            {
                if (_CenterPoint == PointF.Empty)
                {
                    _CenterPoint = new PointF(R.X + R.Width / 2, R.Y + R.Height / 2);
                }
                return _CenterPoint;
            }
            set { _CenterPoint = value; }
        }

        /// <summary>
        /// Simple method to Parse out PlateID and Rafts
        /// </summary>
        /// <param name="Path">file to load</param>
        /// <returns>Returns them in a hierarchy</returns>
        public static Dictionary<string, Dictionary<string, HashSet<CellInfoS>>> Plate_WellField_CellInfo(string Path, string ColumnNameForAnnotation = "")
        {
            char delim = ',';
            var cols = new List<string> { "PLATEID", "WELL LABEL", "FOV", "OBJECT ID", "NUCLEI MAX LEFT BORDER WV1", "NUCLEI MAX TOP BORDER WV1", "NUCLEI MAX WIDTH WV1", "NUCLEI MAX HEIGHT WV1" };
            if (ColumnNameForAnnotation != "") cols.Add(ColumnNameForAnnotation.ToUpper());
            int[] iCols = new int[cols.Count]; string[] arr; var cHeaders = new Dictionary<string, int>(); var cHeaderList = new List<string>();
            var Output = new Dictionary<string, Dictionary<string, HashSet<CellInfoS>>>(); HashSet<CellInfoS> HS;

            string[] lines = File.ReadAllLines(Path);

            { //Setup Headers
                cHeaderList = lines[0].Split(delim).ToList();
                for (int i = 0; i < cHeaderList.Count; i++) cHeaders.Add(cHeaderList[i].ToUpper(), i);
                for (int i = 0; i < cols.Count; i++)
                {
                    string ColSearch = cols[i];
                    if (!cHeaders.ContainsKey(ColSearch))
                    {
                        if (ColSearch.Substring(ColSearch.Length - 1) == "1") //TODO: Make a centralized function for this
                        {
                            ColSearch = ColSearch.Substring(0, ColSearch.Length - 1) + "H"; //Convert to renamed version
                        }
                        if (ColSearch == "PLATEID")
                        {
                            ColSearch = "PLATE ID";
                        }
                        if (!cHeaders.ContainsKey(ColSearch))
                        {
                            if (ColSearch == ColumnNameForAnnotation.ToUpper())
                            {
                                //If we can't find the annotation column name, we can remove it from the list
                                cols.Remove(ColumnNameForAnnotation.ToUpper());
                                ColumnNameForAnnotation = "";
                                continue;
                            }
                            else
                            {
                                ParseError = "Need a column called: " + ColSearch;
                                return null;
                            }
                        }
                    }
                    iCols[i] = cHeaders[ColSearch];
                }
            } //Setup Headers

            string cPlateID, cWell, cFOV, cObj, cL, cT, cW, cH;
            for (int i = 1; i < lines.Length; i++)
            {
                arr = lines[i].Split(delim);
                cPlateID = arr[iCols[0]]; cWell = arr[iCols[1]]; cFOV = arr[iCols[2]]; cObj = arr[iCols[3]];
                cL = arr[iCols[4]]; cT = arr[iCols[5]]; cW = arr[iCols[6]]; cH = arr[iCols[7]];

                if (cL == "") continue;
                var CI = new CellInfoS(cPlateID, cWell, cFOV, cObj, cL, cT, cW, cH);
                if (ColumnNameForAnnotation != "") CI.ClassLabel = arr[iCols[8]];
                if (!Output.ContainsKey(cPlateID)) Output.Add(cPlateID, new Dictionary<string, HashSet<CellInfoS>>());
                if (!Output[cPlateID].ContainsKey(CI.WellField)) Output[cPlateID].Add(CI.WellField, new HashSet<CellInfoS>());
                HS = Output[cPlateID][CI.WellField];
                HS.Add(CI);
            }
            return Output;
        }

        public double DistanceTo(CellInfoS cellB)
        {
            return Math.Sqrt(Math.Pow(this.R.X - cellB.R.X, 2) + Math.Pow(this.R.Y - cellB.R.Y, 2));
        }

        public string ExportNN(bool Headers = false)
        {
            if (Headers)
                return "PlateID" + "\t" + "WELL LABEL" + "\t" + "FOV" + "\t" + "OBJECT ID" + "\t" + "NN ID" + "\t" + "NN Dist";
            else
                return PlateID + "\t" + WellLabel + "\t" + FOV + "\t" + ObjectID + "\t" + NearestNeighbor.ObjectID + "\t" + NNDist;
        }
    }

    public class RaftInfoS
    {
        public string PlateID;
        public string RaftID;
        public string ClassLabel = "";
        public float Score = float.NaN;
        public string ModelName = "";
        public string Note = "";
        public string WellLabel = "";
        public string FOV = "";
        public Dictionary<string, int> Headers; //these refer to each other
        public List<string> HeaderList;
        public string[] Line;
        public string WellField => InCellLibrary.XDCE_Image.WellFieldKey(WellLabel, FOV);

        public override string ToString()
        {
            return RaftID;
        }

        public string Export(int SubPlate, bool OnlyHeaders = false, bool RinseWell = false, string AppendStatus = "")
        {
            var sB = new StringBuilder();
            for (int i = 0; i < HeaderList.Count; i++)
            {
                sB.Append(OnlyHeaders ? HeaderList[i] : (HeaderList[i].ToUpper() == "PICKSUBPLATE" ? SubPlate.ToString() : (HeaderList[i].ToUpper() == "RAFTID" && RinseWell ? "B5B5" : Line[i])));
                sB.Append("\t");
            }
            sB.Append(OnlyHeaders ? "Status1" : AppendStatus);
            sB.Append("\r\n");
            return sB.ToString();
        }

        public static string ParseError = "";

        /// <summary>
        /// Simple method to Parse out PlateID and Rafts
        /// </summary>
        /// <param name="Path">file to load</param>
        /// <returns>Returns them in a hierarchy</returns>
        public static Dictionary<string, HashSet<RaftInfoS>> Parse_PlateID_RaftInfo(string Path)
        {
            var cols = new string[4] { "PLATEID", "WELL LABEL", "FOV", "RAFTID" };
            int[] iCols = new int[cols.Length]; string[] arr; var cHeaders = new Dictionary<string, int>(); var cHeaderList = new List<string>();
            var Output = new Dictionary<string, HashSet<RaftInfoS>>(); HashSet<RaftInfoS> HS;
            int ClassLabelCol = -1;
            int AnnoCol = -1;

            string[] lines = File.ReadAllLines(Path);

            { //Setup Headers
                cHeaderList = lines[0].Split('\t').ToList();
                for (int i = 0; i < cHeaderList.Count; i++) cHeaders.Add(cHeaderList[i].ToUpper(), i);
                for (int i = 0; i < cols.Length; i++)
                {
                    if (!cHeaders.ContainsKey(cols[i]))
                    {
                        ParseError = "Need to include a column with the name: " + cols[i];
                        return null;
                    }
                    iCols[i] = cHeaders[cols[i]];
                }
                string ClassLbl = "MLCLASSLAYOUT";
                if (cHeaders.ContainsKey(ClassLbl)) ClassLabelCol = cHeaders[ClassLbl];
                string Anno = "PREDICTEDCLASS";
                if (cHeaders.ContainsKey(Anno)) AnnoCol = cHeaders[Anno];
            } //Setup Headers

            string cPlateID, cWell, cFOV, cRaft;
            for (int i = 1; i < lines.Length; i++)
            {
                arr = lines[i].Split('\t');
                cPlateID = arr[iCols[0]];
                cWell = arr[iCols[1]];
                cFOV = arr[iCols[2]];
                cRaft = arr[iCols[3]];

                if (!Output.ContainsKey(cPlateID)) Output.Add(cPlateID, new HashSet<RaftInfoS>());
                HS = Output[cPlateID];
                var RI = new RaftInfoS() { HeaderList = cHeaderList, Headers = cHeaders, WellLabel = cWell, FOV = cFOV, RaftID = cRaft, PlateID = cPlateID, Line = arr };
                if (ClassLabelCol >= 0) RI.ClassLabel = arr[ClassLabelCol];
                if (AnnoCol >= 0) RI.Note = arr[AnnoCol];
                HS.Add(RI);
            }
            ParseError = "";
            return Output;

            { //Old Style
              //if (T == null) return null;
              //var NewImport = new Dictionary<string, HashSet<RaftInfoS>>();
              //foreach (var PlateSet in T)
              //{
              //    var HS = new HashSet<RaftInfoS>();
              //    NewImport.Add(PlateSet.Key, HS);
              //    foreach (var Well in PlateSet.Value)
              //    {
              //        foreach (var FOV in Well.Value)
              //        {
              //            foreach (var Raft in FOV.Value)
              //            {
              //                HS.Add(new RaftInfoS() { WellLabel = Well.Key, FOV = FOV.Key, RaftID = Raft });
              //            }
              //        }
              //    }
              //}
              //return NewImport;
            } //Old style
        }

        /// <summary>
        /// Simple method to Parse out PlateID, Well, FOV, and RaftID
        /// </summary>
        /// <param name="Path">file to load</param>
        /// <returns>Returns them in a hierarchy</returns>
        public static Dictionary<string, Dictionary<string, Dictionary<string, HashSet<string>>>> Parse_PlateID_Well_FOV_RaftID(string Path)
        {
            //Written 12/2021, technically well label and FOV are optional, would be nice to implement that
            string[] lines = File.ReadAllLines(Path);
            var cols = new string[4] { "PLATEID", "WELL LABEL", "FOV", "RAFTID" };
            int[] iCols = new int[cols.Length];
            List<string> arr;

            arr = lines[0].ToUpper().Split('\t').ToList();
            for (int i = 0; i < cols.Length; i++)
            {
                if (!arr.Contains(cols[i]))
                {
                    ParseError = "Need to include a column with the name: " + cols[i];
                    return null;
                }
                iCols[i] = arr.IndexOf(cols[i]);
            }
            var PlateIDs_Well_FOV_RaftID = new Dictionary<string, Dictionary<string, Dictionary<string, HashSet<string>>>>();
            for (int i = 1; i < lines.Length; i++)
            {
                arr = lines[i].Split('\t').ToList();
                string PlateID = arr[iCols[0]];
                string Well = arr[iCols[1]];
                string FOV = arr[iCols[2]];
                string RaftID = arr[iCols[3]];
                if (!PlateIDs_Well_FOV_RaftID.ContainsKey(PlateID)) PlateIDs_Well_FOV_RaftID.Add(PlateID, new Dictionary<string, Dictionary<string, HashSet<string>>>());
                if (!PlateIDs_Well_FOV_RaftID[PlateID].ContainsKey(Well)) PlateIDs_Well_FOV_RaftID[PlateID].Add(Well, new Dictionary<string, HashSet<string>>());
                if (!PlateIDs_Well_FOV_RaftID[PlateID][Well].ContainsKey(FOV)) PlateIDs_Well_FOV_RaftID[PlateID][Well].Add(FOV, new HashSet<string>());
                PlateIDs_Well_FOV_RaftID[PlateID][Well][FOV].Add(RaftID);
            }
            ParseError = "";
            return PlateIDs_Well_FOV_RaftID;
        }

        public static Dictionary<string, HashSet<RaftInfoS>> FromAnnotations(InCellLibrary.INCELL_Folder folderToPass, int MaxToReturn = int.MaxValue)
        {
            var Output = new Dictionary<string, HashSet<RaftInfoS>>(); HashSet<RaftInfoS> HS;

            string cPlateID;
            int TestCount = 0;
            foreach (var Well in folderToPass.XDCE.Wells.Values)
            {
                var ICPL = Well.ImageCheck_PointsList;
                foreach (var WellField in ICPL)
                {
                    foreach (var ICP in WellField)
                    {
                        cPlateID = folderToPass.PlateID;
                        if (!Output.ContainsKey(cPlateID)) Output.Add(cPlateID, new HashSet<RaftInfoS>());
                        HS = Output[cPlateID];
                        var Anno = ICP.Annotations[0];
                        var RI = new RaftInfoS()
                        {
                            HeaderList = null,
                            Headers = null,
                            Line = null,
                            WellLabel = ICP.Well_Label,
                            FOV = (ICP.FOV + 1).ToString(),
                            RaftID = ICP.RaftID,
                            ClassLabel = Anno.Value.ToString(),
                            ModelName = Anno.Name,
                            Score = Anno.Score,
                            PlateID = cPlateID,
                            Note = Anno.Note
                        };
                        HS.Add(RI);
                        if (TestCount++ > MaxToReturn) return Output;
                    }
                }
            }
            ParseError = "";
            return Output;
        }
    }

    public static class Name_Link
    {
        public static void RenameTest()
        {

        }
    }

    namespace FOVtoRaftID
    {
        //------------------------------------------------------------------------------------------------------------
        //Data Objects
        //------------------------------------------------------------------------------------------------------------



        /// <summary>
        /// Calibration information
        /// </summary>
        public class CalibrationRafts
        {
            public CalibrationRafts()
            {
                //To Serialize we need an empty constructor
                Dirty = false;
            }

            // constructor, order does not matter!
            public CalibrationRafts(KnownRaft raftOne, KnownRaft raftTwo, KnownRaft raftThree, KnownRaft raftFour)
            {
                Init(raftOne, raftTwo, raftThree, raftFour);
            }

            private void Init(KnownRaft raftOne, KnownRaft raftTwo, KnownRaft raftThree, KnownRaft raftFour)
            {
                // fudge factor for if rafts make a perfectly vertical line (which is bad in cartesia)
                double fudgeFactor = 0.000001;

                CalibrationVersion = "4-Point 01"; //So we can distinguish

                // sort by x coordinate
                KnownRaft[] xOrder = new KnownRaft[4];
                xOrder[0] = raftOne;
                xOrder[1] = raftTwo;
                xOrder[2] = raftThree;
                xOrder[3] = raftFour;
                string sBT = "";
                for (int i = 0; i < xOrder.Length; i++)
                {
                    xOrder[i].Parent = this; //Know who my parent is 5/24/2022

                    // this code reorders them so that they are sorted by x - Willie should convert to a sorted structure
                    if (true)
                    {
                        for (int j = i + 1; j < xOrder.Length; j++)
                        {
                            if (xOrder[j].X < xOrder[i].X)
                            {
                                KnownRaft temp = xOrder[j]; xOrder[j] = xOrder[i]; xOrder[i] = temp;
                                sBT += "ReOr ";
                            }
                        }
                    }
                }

                // assign known rafts in the correct orientation
                if (xOrder[0].Y < xOrder[1].Y)
                {
                    sBT += "01a ";
                    this.UpperLeft = xOrder[0];
                    this.LowerLeft = xOrder[1];
                }
                else
                {
                    sBT += "01b ";
                    this.UpperLeft = xOrder[1];
                    this.LowerLeft = xOrder[0];
                }

                if (xOrder[2].Y < xOrder[3].Y)
                {
                    sBT += "23a ";
                    this.UpperRight = xOrder[2];
                    this.LowerRight = xOrder[3];
                }
                else
                {
                    sBT += "23b ";
                    this.UpperRight = xOrder[3];
                    this.LowerRight = xOrder[2];
                }

                if (this.UpperLeft.X == this.LowerLeft.X)
                {
                    sBT += "LeftFF ";
                    this.UpperLeft.X += fudgeFactor;
                }

                if (this.UpperRight.X == this.LowerRight.X)
                {
                    sBT += "RightFF ";
                    this.UpperRight.X += fudgeFactor;
                }
                Debug.Print(sBT);
            }

            public CalibrationRafts(List<KnownRaft> knownRafts)
            {
                Init(knownRafts[0], knownRafts[1], knownRafts[2], knownRafts[3]);
            }

            public KnownRaft LowerLeft { get; set; }
            public KnownRaft UpperLeft { get; set; }
            public KnownRaft UpperRight { get; set; }
            public KnownRaft LowerRight { get; set; }
            public DateTime DateCalibrated { get; set; }
            [XmlIgnore]
            public bool Dirty;
            public string Well { get; set; }

            private Dictionary<string, KnownRaft> _KnownRaftSet = null;
            [XmlIgnore]
            public Dictionary<string, KnownRaft> KnownRaftSet
            {
                get
                {
                    if (_KnownRaftSet == null)
                    {
                        _KnownRaftSet = new Dictionary<string, KnownRaft>();
                        foreach (var R in KnownRafts) _KnownRaftSet.Add(R.RaftID, R);
                    }
                    return _KnownRaftSet;
                }
            }

            private List<KnownRaft> _KnownRafts = null;
            [XmlIgnore]
            public List<KnownRaft> KnownRafts
            {
                get
                {
                    if (_KnownRafts == null)
                    {
                        if (LowerLeft != null && LowerRight != null) _KnownRafts = new List<KnownRaft>(4) { LowerLeft, UpperLeft, UpperRight, LowerRight };
                        else _KnownRafts = new List<KnownRaft>(4);
                    }
                    return _KnownRafts;
                }
            }

            /// <summary>
            /// Check whether any of the RAFT IDs in the provided list are Calibration Rafts
            /// </summary>
            /// <param name="RaftIDs">List of RAFT IDs</param>
            /// <returns>The Known Raft Objects for those rafts in the list </returns>
            public IEnumerable<KnownRaft> KnownRaftsContained(IEnumerable<string> RaftIDs)
            {
                List<KnownRaft> tList = new List<KnownRaft>();
                foreach (var RaftID in RaftIDs)
                {
                    if (KnownRaftSet.ContainsKey(RaftID))
                    {
                        tList.Add(KnownRaftSet[RaftID]);
                        KnownRaftSet[RaftID].Parent = this; //Not sure why it sometimes loses this . . 
                    }
                }
                return tList;
            }

            public ReturnRaft FindRaftID_RR(InCellLibrary.XDCE_Image xI, string RaftID)
            {
                var Point = ImageCheck.ImageCheck_Point.FromImage(xI, RaftID);
                return FindRaftID_RR(Point.Plate_Xum, Point.Plate_Yum);
            }

            public static bool RecordDebug = false;

            public ReturnRaft FindRaftID_RR(double fullX, double fullY)
            {
                RefreshPreCalcs(); //Don't need to do this everytime, but for now it is still faster than the previous method

                var u = new UnknownRaft(fullX, fullY);

                int row = FindRow(fullX, fullY);
                int col = FindCol(fullX, fullY);

                string raftID = GetRaftID(row, col);
                var rr = new ReturnRaft(u);

                rr.XOnRaft = FindXOnRaft(fullX, fullY);
                rr.YOnRaft = FindYOnRaft(fullX, fullY);

                rr.MinDistToEdge = FindMinDistToEdge(fullX, fullY);// Min Dist to Edge
                rr.RaftCorners = FindRaftCorners(fullX, fullY, FindXDist(fullX, fullY), FindYDist(fullX, fullY), rr.XOnRaft, rr.YOnRaft);// raft corners
                rr.RaftID = raftID;

                return rr;
            }

            internal double leftVertDist, rightVertDist, topHorDist, bottomHorDist;
            internal Line left, right, top, bottom;
            internal double offset = 0.5;

            public static int GetRow_FromRaftID(string RaftID) { return (Convert.ToInt32(RaftID[0]) - 65) * 10 + (Convert.ToInt32(RaftID[1]) - 48); }
            public static int GetCol_FromRaftID(string RaftID) { return (Convert.ToInt32(RaftID[2]) - 65) * 10 + (Convert.ToInt32(RaftID[3]) - 48); }
            private void GetSubdivisions(out int leftVertSubdivisions, out int rightVertSubdivisions, out int topHorSubdivisions, out int bottomHorSubdivisions)
            {
                //Remember the letters are tilted from our perspective, which is why things seem odd
                int llRowNum = GetRow_FromRaftID(LowerLeft.RaftID);
                int ulRowNum = GetRow_FromRaftID(UpperLeft.RaftID);
                int urRowNum = GetRow_FromRaftID(UpperRight.RaftID);
                int lrRowNum = GetRow_FromRaftID(LowerRight.RaftID);

                int llColNum = GetCol_FromRaftID(LowerLeft.RaftID);
                int ulColNum = GetCol_FromRaftID(UpperLeft.RaftID);
                int urColNum = GetCol_FromRaftID(UpperRight.RaftID);
                int lrColNum = GetCol_FromRaftID(LowerRight.RaftID);

                if (ulRowNum < urRowNum) // is Left
                {
                    leftVertSubdivisions = ulColNum - llColNum;
                    rightVertSubdivisions = urColNum - lrColNum;
                    topHorSubdivisions = urRowNum - ulRowNum;
                    bottomHorSubdivisions = lrRowNum - llRowNum;
                }
                else if (ulRowNum > urRowNum) // is Right
                {
                    leftVertSubdivisions = llColNum - ulColNum;
                    rightVertSubdivisions = lrColNum - urColNum;
                    topHorSubdivisions = ulRowNum - urRowNum;
                    bottomHorSubdivisions = llRowNum - lrRowNum;
                }
                else
                {
                    if (ulRowNum < llRowNum) // is Up
                    {
                        leftVertSubdivisions = llRowNum - ulRowNum;
                        rightVertSubdivisions = lrRowNum - urRowNum;
                        topHorSubdivisions = urColNum - ulColNum;
                        bottomHorSubdivisions = lrColNum - llColNum;
                    }
                    else if (ulRowNum > llRowNum) // is Down
                    {
                        leftVertSubdivisions = ulRowNum - llRowNum;
                        rightVertSubdivisions = urRowNum - lrRowNum;
                        topHorSubdivisions = ulColNum - urColNum;
                        bottomHorSubdivisions = llColNum - lrColNum;
                    }
                    else //Is Equal
                    {

                        leftVertSubdivisions = ulRowNum - llRowNum;
                        rightVertSubdivisions = urRowNum - lrRowNum;
                        topHorSubdivisions = ulColNum - urColNum;
                        bottomHorSubdivisions = llColNum - lrColNum;
                    }
                }
            }
            private void GetHorVertDistances(out double leftVertDist, out double rightVertDist, out double topHorDist, out double bottomHorDist)
            {
                //// make horizontal subdivisions according to orientation
                int leftVertSubdivisions, rightVertSubdivisions, topHorSubdivisions, bottomHorSubdivisions;
                GetSubdivisions(out leftVertSubdivisions, out rightVertSubdivisions, out topHorSubdivisions, out bottomHorSubdivisions);

                // y distance between subdivisions
                leftVertDist = (LowerLeft.Y - UpperLeft.Y) / (leftVertSubdivisions);
                rightVertDist = (LowerRight.Y - UpperRight.Y) / (rightVertSubdivisions);

                // x distance between subdivisions
                topHorDist = (UpperRight.X - UpperLeft.X) / (topHorSubdivisions);
                bottomHorDist = (LowerRight.X - LowerLeft.X) / (bottomHorSubdivisions);
            }

            public void RefreshPreCalcs()
            {
                GetHorVertDistances(out leftVertDist, out rightVertDist, out topHorDist, out bottomHorDist);

                left = new Line((LowerLeft.X - offset * bottomHorDist, LowerLeft.Y - offset * leftVertDist),
                                (UpperLeft.X - offset * topHorDist, UpperLeft.Y - offset * leftVertDist));
                right = new Line((LowerRight.X - offset * bottomHorDist, LowerRight.Y - offset * rightVertDist),
                                (UpperRight.X - offset * topHorDist, UpperRight.Y - offset * rightVertDist));
                top = new Line((UpperLeft.X - offset * topHorDist, UpperLeft.Y - offset * leftVertDist),
                              (UpperRight.X - offset * topHorDist, UpperRight.Y - offset * rightVertDist));
                bottom = new Line((LowerLeft.X - offset * bottomHorDist, LowerLeft.Y - offset * leftVertDist),
                                 (LowerRight.X - offset * bottomHorDist, LowerRight.Y - offset * rightVertDist));
            }

            public int FindRow(double x, double y)
            {
                // figure out if we need to go up or down
                double leftRowY = UpperLeft.Y - offset * leftVertDist;
                double rightRowY = UpperRight.Y - offset * rightVertDist;
                Line currentRowLine = new Line((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));

                bool up = !currentRowLine.isBelow(x, y);
                //if (RecordDebug) Debug.Print("Row: " + (leftRowY - rightRowY).ToString("0.0") + " " + up + " " + currentRowLine.m.ToString("0.0"));  //When the slope gets negative . . 

                int currentRow = 0;
                if (!up)
                {
                    while (currentRowLine.isBelow(x, y))
                    {
                        if (currentRow++ > 500) break;    // go to next line
                        leftRowY += leftVertDist;
                        rightRowY += rightVertDist;
                        currentRowLine.setPoints((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));
                    }
                    return currentRow - 1; // return the row
                }
                else
                {
                    while (!currentRowLine.isBelow(x, y))
                    {
                        if (currentRow-- < -500) break; // go to next line
                        leftRowY -= leftVertDist;
                        rightRowY -= rightVertDist;
                        currentRowLine.setPoints((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));
                    }
                    return currentRow; // return the row
                }
            }

            public int FindCol(double x, double y)
            {
                // figure out if we need to go left or right
                double topColX = UpperLeft.X - (offset * topHorDist);
                double bottomColX = LowerLeft.X - (offset * bottomHorDist);
                Line currentColLine = new Line((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));

                bool right = currentColLine.isRight(x, y);
                char d = '\t'; string dNote = right.ToString() + d + topHorDist + d + bottomHorDist + d + topColX.ToString("0.0") + d + bottomColX.ToString("0.0") + d + top.getY(topColX) + d + bottom.getY(bottomColX) + d + currentColLine.m.ToString("0.0") + d + currentColLine.b.ToString("0.0") + d + x + d + y;

                int currentCol = 0;
                if (right)
                {
                    while (currentColLine.isRight(x, y))
                    {
                        if (currentCol++ > 500) break;
                        // go to next line
                        topColX += topHorDist;
                        bottomColX += bottomHorDist;
                        currentColLine.setPoints((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));
                    }
                    currentCol--;
                }
                else
                {
                    //double tx = x + (bottomHorDist/2);
                    while (!currentColLine.isRight(x, y))
                    {
                        if (currentCol-- < -500) break;
                        // go to next line
                        //tx += bottomHorDist;
                        topColX -= topHorDist;
                        bottomColX -= bottomHorDist;
                        currentColLine.setPoints((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));
                    }
                }
                if (RecordDebug) Debug.Print(currentCol.ToString() + d + dNote);
                return currentCol;
            }

            public double FindXOnRaft(double x, double y)
            {
                // figure out if we need to go left or right
                double topColX = UpperLeft.X - offset * topHorDist;
                double bottomColX = LowerLeft.X - offset * bottomHorDist;
                Line currentColLine = new Line((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));

                bool right = currentColLine.isRight(x, y);
                int currentCol = 0;
                if (right)
                {
                    double topY;
                    double bottomY;
                    while (currentColLine.isRight(x, y))
                    {
                        if (currentCol++ > 500) break;
                        // go to next line
                        topColX += topHorDist;
                        topY = top.getY(topColX);
                        bottomColX += bottomHorDist;
                        bottomY = bottom.getY(bottomColX);
                        currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));
                    }
                    topColX -= topHorDist;
                    topY = top.getY(topColX);
                    bottomColX -= bottomHorDist;
                    bottomY = bottom.getY(bottomColX);
                    currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));

                    // return the x difference
                    return x - currentColLine.getX(y);
                }
                else
                {
                    while (!currentColLine.isRight(x, y))
                    {
                        if (currentCol-- < -500) break;
                        // go to next line
                        topColX -= topHorDist;
                        double topY = top.getY(topColX);
                        bottomColX -= bottomHorDist;
                        double bottomY = bottom.getY(bottomColX);
                        currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));
                    }
                    // return the row
                    return x - currentColLine.getX(y);
                }
            }

            public double FindYOnRaft(double x, double y)
            {
                // figure out if we need to go up or down
                double leftRowY = UpperLeft.Y - offset * leftVertDist;
                double rightRowY = UpperRight.Y - offset * rightVertDist;
                Line currentRowLine = new Line((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));

                bool up = !currentRowLine.isBelow(x, y);
                int currentRow = 0;
                if (!up)
                {
                    double leftX;
                    double rightX;
                    while (currentRowLine.isBelow(x, y))
                    {
                        if (currentRow++ > 500) break;
                        // go to next line
                        leftRowY = leftRowY + leftVertDist;
                        rightRowY = rightRowY + rightVertDist;
                        leftX = left.getX(leftRowY);
                        rightX = right.getX(rightRowY);
                        currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    }
                    leftRowY = leftRowY - leftVertDist;
                    leftX = left.getX(leftRowY);
                    rightRowY = rightRowY - rightVertDist;
                    rightX = right.getX(rightRowY);
                    currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    // return the row
                    return y - currentRowLine.getY(x);
                }
                else
                {
                    while (!currentRowLine.isBelow(x, y))
                    {
                        if (currentRow-- < -500) break;
                        // go to next line
                        leftRowY = leftRowY - leftVertDist;
                        double leftX = left.getX(leftRowY);
                        rightRowY = rightRowY - rightVertDist;
                        double rightX = right.getX(rightRowY);
                        currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    }
                    return y - currentRowLine.getY(x);
                }
            }

            public double FindXDist(double x, double y)
            {
                // figure out if we need to go left or right
                int currentCol = 0;
                double topColX = UpperLeft.X - offset * topHorDist;
                double bottomColX = LowerLeft.X - offset * bottomHorDist;
                Line currentColLine = new Line((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));

                bool right = currentColLine.isRight(x, y);

                if (right)
                {
                    while (currentColLine.isRight(x, y))
                    {
                        if (currentCol++ > 500) break;
                        // go to next line
                        topColX += topHorDist;
                        bottomColX += bottomHorDist;
                        double topY = top.getY(topColX);
                        double bottomY = bottom.getY(bottomColX);
                        currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));
                    }
                    // return the row
                    topColX = topColX - topHorDist;
                    double topY2 = top.getY(topColX);
                    bottomColX = bottomColX - bottomHorDist;
                    double bottomY2 = bottom.getY(bottomColX);
                    Line prevColLine = new Line((topColX, topY2), (bottomColX, bottomY2));

                    return currentColLine.getX(y) - prevColLine.getX(y);
                }
                else
                {
                    while (!currentColLine.isRight(x, y))
                    {
                        if (currentCol-- < -500) break;
                        // go to next line
                        topColX = topColX - topHorDist;
                        double topY = top.getY(topColX);
                        bottomColX = bottomColX - bottomHorDist;
                        double bottomY = bottom.getY(bottomColX);
                        currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));
                    }

                    // return the row
                    topColX = topColX + topHorDist;
                    double topY2 = top.getY(topColX);
                    bottomColX = bottomColX + bottomHorDist;
                    double bottomY2 = bottom.getY(bottomColX);
                    Line nextColLine = new Line((topColX, topY2), (bottomColX, bottomY2));

                    return nextColLine.getX(y) - currentColLine.getX(y);
                }
            }

            public double FindYDist(double x, double y)
            {
                // figure out if we need to go up or down
                double leftRowY = UpperLeft.Y - offset * leftVertDist;
                double rightRowY = UpperRight.Y - offset * rightVertDist;
                Line currentRowLine = new Line((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));

                bool up = !currentRowLine.isBelow(x, y);
                int currentRow = 0;
                if (!up)
                {
                    while (currentRowLine.isBelow(x, y))
                    {
                        if (currentRow++ > 500) break;
                        // go to next line
                        leftRowY = leftRowY + leftVertDist;
                        double leftX = left.getX(leftRowY);
                        rightRowY = rightRowY + rightVertDist;
                        double rightX = right.getX(rightRowY);
                        currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    }
                    // return the row
                    leftRowY = leftRowY - leftVertDist;
                    double leftX2 = left.getX(leftRowY);
                    rightRowY = rightRowY - rightVertDist;
                    double rightX2 = right.getX(rightRowY);
                    Line prevRowLine = new Line((leftX2, leftRowY), (rightX2, rightRowY));
                    return currentRowLine.getY(x) - prevRowLine.getY(x);
                }
                else
                {
                    while (!currentRowLine.isBelow(x, y))
                    {
                        if (currentRow-- < -500) break;
                        // go to next line
                        leftRowY = leftRowY - leftVertDist;
                        double leftX = left.getX(leftRowY);
                        rightRowY = rightRowY - rightVertDist;
                        double rightX = right.getX(rightRowY);
                        currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    }

                    leftRowY = leftRowY + leftVertDist;
                    double leftX2 = left.getX(leftRowY);
                    rightRowY = rightRowY + rightVertDist;
                    double rightX2 = right.getX(rightRowY);
                    Line nextRowLine = new Line((leftX2, leftRowY), (rightX2, rightRowY));
                    return nextRowLine.getY(x) - currentRowLine.getY(x);
                }
            }

            public double FindMinDistToEdge(double x, double y)
            {
                double XOnRaft = FindXOnRaft(x, y);
                double YOnRaft = FindYOnRaft(x, y);
                double XDist = FindXDist(x, y);
                double YDist = FindYDist(x, y);
                double[] DistToEdgeArray = { //dist from top, right, bottom, and left edges in array indeces 0,1,2,3
                    Math.Abs(YOnRaft),
                    Math.Abs(XDist - XOnRaft),
                    Math.Abs(YDist - YOnRaft),
                    Math.Abs(XOnRaft)
                };
                double MinDistToEdge = DistToEdgeArray[0];
                //double MinDistToEdge = XOnRaft;
                for (int i = 1; i <= DistToEdgeArray.Length - 1; i++)
                {
                    if (DistToEdgeArray[i] < MinDistToEdge) MinDistToEdge = DistToEdgeArray[i];
                }

                return MinDistToEdge;
            }

            public static KnownRaft ExtrapolateFourthRaft(KnownRaft ll, KnownRaft ul, KnownRaft ur)
            {
                // uncomment this if you want to use the cal rafts stored in this object
                //string llRow = LowerLeft.RaftID;
                //string ulRow = UpperLeft.RaftID;
                //string urRow = UpperRight.RaftID;

                int ulRowNum = GetRow_FromRaftID(ul.RaftID);
                int urRowNum = GetRow_FromRaftID(ur.RaftID);

                // find orientation & find new RaftID
                string RaftID;
                if ((ulRowNum < urRowNum) | (ulRowNum > urRowNum)) // is left or right
                {
                    RaftID = ur.RaftID[0].ToString() + ur.RaftID[1].ToString() + ll.RaftID[2].ToString() + ll.RaftID[3].ToString();
                }
                else
                {
                    // is up or down
                    RaftID = ll.RaftID[0].ToString() + ll.RaftID[1].ToString() + ur.RaftID[2].ToString() + ur.RaftID[3].ToString();
                }

                // do maths to figure out X and Y
                double Ax = ll.X; double Ay = ll.Y;
                double Bx = ul.X; double By = ul.Y;
                double Cx = ur.X; double Cy = ur.Y;

                double Dx = Ax + Cx - Bx;
                double Dy = Ay + Cy - By;

                // return known raft
                KnownRaft lr = new KnownRaft(RaftID, Dx, Dy);
                return lr;
            }

            public static List<Tuple<double, double>> FindRaftCorners(double x, double y, double XDist, double YDist, double XOnRaft, double YonRaft)
            {
                double raftUpperLeftX = x - XOnRaft;
                double raftUpperLeftY = y - YonRaft;
                List<Tuple<double, double>> raftCornersMicrons = new List<Tuple<double, double>>
                    {
                        new Tuple<double, double>(raftUpperLeftX, raftUpperLeftY),
                        new Tuple<double, double>(raftUpperLeftX + XDist, raftUpperLeftY),
                        new Tuple<double, double>(raftUpperLeftX + XDist, raftUpperLeftY + YDist),
                        new Tuple<double, double>(raftUpperLeftX, raftUpperLeftY + YDist)
                    };
                return raftCornersMicrons;
            }

            public string GetRaftID(int row, int col)
            {
                int newRow;
                int newCol;


                int llRowNum = GetRow_FromRaftID(LowerLeft.RaftID);
                int ulRowNum = GetRow_FromRaftID(UpperLeft.RaftID);
                int urRowNum = GetRow_FromRaftID(UpperRight.RaftID);
                int lrRowNum = GetRow_FromRaftID(LowerRight.RaftID);

                if (ulRowNum < urRowNum) // is Left
                {
                    newRow = col + (System.Convert.ToInt32(this.UpperLeft.RaftID[0]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[1]) - 48);
                    newCol = (System.Convert.ToInt32(this.UpperLeft.RaftID[2]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[3]) - 48) - row;
                }
                else if (ulRowNum > urRowNum) // is Right
                {
                    newRow = (System.Convert.ToInt32(this.UpperLeft.RaftID[0]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[1]) - 48) - col;
                    newCol = (System.Convert.ToInt32(this.UpperLeft.RaftID[2]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[3]) - 48) + row;
                }
                else
                {
                    if (ulRowNum < llRowNum) // is Up
                    {
                        newRow = row + (System.Convert.ToInt32(this.UpperLeft.RaftID[0]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[1]) - 48);
                        newCol = col + (System.Convert.ToInt32(this.UpperLeft.RaftID[2]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[3]) - 48);
                    }
                    else // is Down
                    {
                        newRow = (System.Convert.ToInt32(this.UpperLeft.RaftID[0]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[1]) - 48) - row;
                        newCol = (System.Convert.ToInt32(this.UpperLeft.RaftID[2]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[3]) - 48) - col;
                    }
                }

                string rowString = ((char)((int)(newRow / 10) + 65)).ToString() + ((char)(newRow % 10 + 48)).ToString();
                string colString = ((char)((int)(newCol / 10) + 65)).ToString() + ((char)(newCol % 10 + 48)).ToString();

                return rowString + colString;
            }

            public bool Save(string SavePath)
            {
                DateCalibrated = DateTime.Now;
                XmlSerializer x = new XmlSerializer(typeof(CalibrationRafts));
                using (StreamWriter SW = new StreamWriter(SavePath))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                    Dirty = false;
                    return true;
                }
            }

            public bool SaveDefault(string FolderPath)
            {
                string p = Path.Combine(FolderPath, CalibrationFileName);
                return Save(p);
            }

            public string CalibrationFileName { get => CalibrationNameConstruct(Well); }

            public static string CalibrationNameConstruct(string WellLabel = "") { return (CalibrationNamePart + " " + WellLabel).Trim() + "." + CalibrationExtension; }
            public static string CalibrationNamePart = "RaftCalibration";
            public static string CalibrationExtension = "XML";

            /// <summary>
            /// Give this the folder that the XDCE file is in, and it will guess on the file name here.  This is fast, but if it returns null, there may be a calibration of a different name or in a different place
            /// </summary>
            public static CalibrationRafts Load_Assumed(string FolderThatXDCEisIn, string WellLabel = "")
            {
                string PathAssumed = Path.Combine(FolderThatXDCEisIn, CalibrationNameConstruct(WellLabel));
                FileInfo FI = new FileInfo(PathAssumed);
                if (FI.Exists) return Load(PathAssumed);
                return null;
            }

            /// <summary>
            /// Must give the fully qualified path, will not look for a calibration file (use Load_Assumed for that)
            /// </summary>
            public static CalibrationRafts Load(string LoadPath)
            {
                XmlSerializer x = new XmlSerializer(typeof(CalibrationRafts));
                using (FileStream F = new FileStream(LoadPath, FileMode.Open))
                {
                    CalibrationRafts CR = (CalibrationRafts)x.Deserialize(F);
                    F.Close();
                    if (CR.DateCalibrated < new DateTime(2019, 8, 15)) CR.DateCalibrated = new DateTime(2019, 8, 15);

                    if (CR.LowerRight == null && CR.UpperLeft != null)
                    {
                        CR.LowerRight = CalibrationRafts.ExtrapolateFourthRaft(CR.LowerLeft, CR.UpperLeft, CR.UpperRight);
                        CR.CalibrationVersion = "3-Point 01";
                    }

                    foreach (var KR in CR.KnownRafts) KR.Parent = CR; //Reassociate parent 5/24/2022

                    return CR;
                }
            }

            public string CalibrationVersion { get; set; }
            public bool Calibrated { get => KnownRafts.Count == 4; }
            private Random _Rnd = new Random();
            private string _CalCode = "";
            [XmlIgnore]
            public string CalCode
            {
                get
                {
                    if (_CalCode == "")
                    {
                        _CalCode = _Rnd.Next(100000, 999999).ToString();
                    }
                    return _CalCode;
                }
            }

            /// <summary>
            /// This allows the rafts to be refreshed, usually when shifting
            /// </summary>
            public void CalCodeShift()
            {
                _CalCode += "s";
                _CalCode = _CalCode.Replace("sssss", "t");
                _CalCode = _CalCode.Replace("ttttt", "u");
                _CalCode = _CalCode.Replace("uuuuu", "v");
            }

            public static bool OnSameVertAxis(string raftOne, string raftTwo)
            {
                return (raftOne[0] == raftTwo[0] && raftOne[1] == raftTwo[1]);
            }

            public static bool OnSameHorAxis(string raftOne, string raftTwo)
            {
                return (raftOne[2] == raftTwo[2] && raftOne[3] == raftTwo[3]);
            }
        }

        /// <summary>
        /// Known Raft Position, i.e. from the calibration itself
        /// </summary>
        public class KnownRaft
        {
            public KnownRaft()
            {
                // need this to serialize
            }

            public KnownRaft(string RaftID, double x, double y)
            {
                this.RaftID = RaftID;
                this.X = x;
                this.Y = y;
            }

            [XmlIgnore]
            public CalibrationRafts Parent { get; set; }

            public string RaftID { get; set; }
            public double X { get; set; }
            public double Y { get; set; }

            private ReturnRaft _ReturnRaft;

            [XmlIgnore]
            public ReturnRaft ReturnRaft
            {
                get
                {
                    if (_ReturnRaft == null && Parent != null)
                    {
                        _ReturnRaft = Parent.FindRaftID_RR(X, Y);
                    }
                    return _ReturnRaft;
                }
                set { _ReturnRaft = value; }
            }

            [XmlIgnore]
            public object DragPoint { get; set; }
        }

        /// <summary>
        /// Unknown Raft Position
        /// </summary>
        public class UnknownRaft
        {
            public UnknownRaft(double x, double y)
            {
                this.X = x;
                this.Y = y;
            }

            public double X { get; set; }
            public double Y { get; set; }
        }

        /// <summary>
        /// Primary way we work with a raft that came from some part of a calibrated image
        /// </summary>
        public class ReturnRaft
        {
            public ReturnRaft(UnknownRaft u)
            {
                this.U = u;
            }
            public UnknownRaft U { get; set; }
            public string RaftID { get; set; }

            public bool IsFiducial { get => IsRaftAnyFiducial(RaftID); }

            public double XOnRaft { get; set; }
            public double YOnRaft { get; set; }
            public double MinDistToEdge { get; set; }

            //In Microns, convert back to pixels
            public List<Tuple<double, double>> RaftCorners { get; set; }

            public PointF RaftCenterFull => new PointF((float)(RaftCorners[0].Item1 + RaftCorners[2].Item1) / 2, (float)(RaftCorners[0].Item2 + RaftCorners[2].Item2) / 2);

            public int RaftRowIdx => CalibrationRafts.GetRow_FromRaftID(RaftID);

            public int RaftColIdx => CalibrationRafts.GetCol_FromRaftID(RaftID);

            public static bool IsRaftAnyFiducial(string RaftID)
            {
                return IsRaftFiducialLabels(RaftID) || IsRaftFiducialDot(RaftID);
            }

            public static bool IsRaftFiducialLabels(string RaftID)
            {
                return (RaftID[1] == '0' & RaftID[3] == '0');
            }

            public static bool IsRaftFiducialDot(string RaftID)
            {
                return (RaftID[1] == '5' & RaftID[3] == '0') || (RaftID[1] == '0' & RaftID[3] == '5') || (RaftID[1] == '5' & RaftID[3] == '5');
            }

            public static PointF PointCenterOnImage(SizeF pB, InCellLibrary.XDCE_Image xI, ReturnRaft rR)
            {
                var Ps = PointsFromCornersOnImage(pB, xI, rR);
                if (Ps.Count == 0) return PointF.Empty;

                var P = new PointF((Ps[0].X + Ps[2].X) / 2, (Ps[0].Y + Ps[2].Y) / 2);
                return P;
            }

            public static List<PointF> PointsFromCornersOnImage(SizeF pB, InCellLibrary.XDCE_Image xI, ReturnRaft rR)
            {
                if (rR == null) return new List<PointF>();
                List<PointF> Points = new List<PointF>(rR.RaftCorners.Count);
                foreach (Tuple<double, double> pt in rR.RaftCorners) Points.Add(CornerToPoint(pB, xI, pt));
                Points.Add(Points[0]);
                return Points;
            }

            public static RectangleF RectFromCorners(InCellLibrary.XDCE_Image xI, ReturnRaft RR, SizeF Bitmap_size)
            {
                List<PointF> Points = PointsFromCornersOnImage(Bitmap_size, xI, RR);
                float X = Math.Max(0, Points[0].X);
                float Y = Math.Max(0, Points[0].Y);
                float Width = Math.Min(Bitmap_size.Width, Points[2].X) - X;
                float Height = Math.Min(Bitmap_size.Height, Points[2].Y) - Y;
                return new RectangleF(X, Y, Width, Height);
            }

            public static PointF CornerToPoint(SizeF pB, InCellLibrary.XDCE_Image xI, Tuple<double, double> pt)
            {
                float x = (float)((pt.Item1 - xI.PlateX_um) / xI.Parent.PixelWidth_in_um);
                float y = (float)((pt.Item2 - xI.PlateY_um) / xI.Parent.PixelHeight_in_um);
                x = pB.Width * x / xI.Parent.ImageWidth_in_Pixels;
                y = pB.Height * y / xI.Parent.ImageHeight_in_Pixels;
                return new PointF(x, y);
            }

            public static PointF Point_from_ImageCheck(ImageCheck.ImageCheck_Point Point, SizeF size)
            {
                float X = Point.PixelX_Fraction * size.Width;
                float Y = Point.PixelY_Fraction * size.Height;
                PointF pRet = new PointF(X, Y);
                return pRet;
            }

            public override string ToString()
            {
                return RaftID;
            }
        }

        /// <summary>
        /// New class 4/2022 by Vinay for 4-point method of calibration
        /// </summary>
        public class Line
        {
            // constructor
            public Line((double, double) pointOne, (double, double) pointTwo)
            {
                // point two is to the right of point one
                if (pointTwo.Item1 > pointOne.Item1)
                {
                    this.pointOne = pointOne;
                    this.pointTwo = pointTwo;
                }
                else
                {
                    this.pointTwo = pointOne;
                    this.pointOne = pointTwo;
                }

                this.m = (this.pointTwo.Item2 - this.pointOne.Item2) / (this.pointTwo.Item1 - this.pointOne.Item1);
                this.b = this.pointOne.Item2 - (this.m * this.pointOne.Item1);
                //this.b = this.pointOne.Item2 - (this.m * pointOne.Item1); //WB Found this bug 9/2022
            }

            // mutator
            public void setPoints((double, double) pointOne, (double, double) pointTwo)
            {
                // point two is to the right of point one
                if (pointTwo.Item1 > pointOne.Item1)
                {
                    this.pointOne = pointOne;
                    this.pointTwo = pointTwo;
                }
                else
                {
                    this.pointTwo = pointOne;
                    this.pointOne = pointTwo;
                }
                this.m = (this.pointTwo.Item2 - this.pointOne.Item2) / (this.pointTwo.Item1 - this.pointOne.Item1);
                this.b = this.pointOne.Item2 - this.m * this.pointOne.Item1;
                //this.b = this.pointOne.Item2 - this.m * pointOne.Item1; //WB Found this bug 9/2022
            }

            // get x
            public double getX(double y)
            {
                return (y - this.b) / this.m;
            }

            // get y
            public double getY(double x)
            {
                return this.m * x + this.b;
            }

            // returns true if line is below a point
            public bool isBelow(double x, double y)
            {
                double bPrime = y - this.m * x;
                return (bPrime > this.b); // line below point
            }

            // is right
            public bool isRight(double x, double y)
            {
                double lineX = this.getX(y);
                return lineX <= x;
            }

            public (double, double) pointOne { get; set; }
            public (double, double) pointTwo { get; set; }
            public double m { get; set; }
            public double b { get; set; }
        }

    }
}