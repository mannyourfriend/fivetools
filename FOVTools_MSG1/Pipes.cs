﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Text;
using System.Threading;
using System.Security.Principal;

namespace FIVE_MSG
{
    //https://docs.microsoft.com/en-us/dotnet/standard/io/how-to-use-anonymous-pipes-for-local-interprocess-communication
    public class PipeClass
    {
        internal static string PipeName = "FIVToolsPipe01";
        internal static string ServerMessage = "FIVTOOLS-Server-000000100";
        private static NamedPipeServerStream pipeServer;
        private static StreamString ss;

        //https://docs.microsoft.com/en-us/dotnet/standard/io/how-to-use-named-pipes-for-network-interprocess-communication

        public static string Server_Start_WaitRead()
        {
            pipeServer = new NamedPipeServerStream(PipeName, PipeDirection.InOut);
            pipeServer.WaitForConnection();
            try
            {
                ss = new StreamString(pipeServer);

                // Verify our identity to the connected client using a string the client expects
                ss.WriteString(ServerMessage);
                // Get the message from the client
                string resultMSG = ss.ReadString();
                return resultMSG;
            }
            // Catch the IOException that is raised if the pipe is broken
            // or disconnected.
            catch (IOException e)
            {
                return "||" + e.Message;
            }
        }

        public static void Server_ReadyClose()
        {
            // Send back a message when it is time to close
            if (pipeServer == null) return;

            ss.WriteString("Done.");
            pipeServer.Close();
        }

        public static void Client_SendMessage(string Message)
        {
            try
            {
                string reply;
                NamedPipeClientStream pipeClient = new NamedPipeClientStream(".", PipeName, PipeDirection.InOut, PipeOptions.None, TokenImpersonationLevel.Impersonation);
                pipeClient.Connect();
                Console.WriteLine("Connected to Client");

                StreamString ss = new StreamString(pipeClient);
                reply = ss.ReadString();
                if (reply != ServerMessage)
                {
                    Console.WriteLine("Not the expected client");
                    pipeClient.Close();
                    return;
                }
                ss.WriteString(Message);
                Console.WriteLine("Message has been sent. Awaiting Finish reply");

                reply = ss.ReadString();
                Console.WriteLine(reply);
                pipeClient.Close();
            }
            catch (Exception E)
            {
                Console.WriteLine(E.Message);
            }
        }

    }
    public class StreamString
    {
        private Stream ioStream;
        private UnicodeEncoding streamEncoding;

        public StreamString(Stream ioStream)
        {
            this.ioStream = ioStream;
            streamEncoding = new UnicodeEncoding();
        }

        public string ReadString()
        {
            int len = 0;

            len = ioStream.ReadByte() * 256;
            len += ioStream.ReadByte();
            byte[] inBuffer = new byte[len];
            ioStream.Read(inBuffer, 0, len);

            return streamEncoding.GetString(inBuffer);
        }

        public int WriteString(string outString)
        {
            byte[] outBuffer = streamEncoding.GetBytes(outString);
            int len = outBuffer.Length;
            if (len > UInt16.MaxValue)
            {
                len = (int)UInt16.MaxValue;
            }
            ioStream.WriteByte((byte)(len / 256));
            ioStream.WriteByte((byte)(len & 255));
            ioStream.Write(outBuffer, 0, len);
            ioStream.Flush();

            return outBuffer.Length + 2;
        }
    }

    // Contains the method executed in the context of the impersonated user
    public class ReadFileToStream
    {
        private string fn;
        private StreamString ss;

        public ReadFileToStream(StreamString str, string filename)
        {
            fn = filename;
            ss = str;
        }

        public void Start()
        {
            string contents = File.ReadAllText(fn);
            ss.WriteString(contents);
        }
    }

    public class NonWorking_AnonPipes
    {
        public static string DefaultPath_TempName { get; private set; }

        public static string ANON_StartServer_Read() //Server is on the receiving end
        {
            using (AnonymousPipeServerStream pipeServer = new AnonymousPipeServerStream(PipeDirection.In, HandleInheritability.Inheritable))
            {
                string status = pipeServer.TransmissionMode.ToString();

                // Pass the client process a handle to the server.
                File.WriteAllText(DefaultPath_TempName, pipeServer.GetClientHandleAsString());
                Thread.Sleep(5000);
                //pipeServer.DisposeLocalCopyOfClientHandle();
                try
                {
                    using (StreamReader sr = new StreamReader(pipeServer))
                    {
                        string temp;
                        do // Wait for 'sync message' from the server.
                        {
                            do
                            {
                                temp = sr.ReadLine();
                                if (temp == null) { Thread.Sleep(25); }
                            } while (temp == null);
                        }
                        while (!temp.StartsWith("SYNC"));

                        // Read the server data and echo to the console.
                        while ((temp = sr.ReadLine()) != null)
                        {
                            return temp;
                        }
                    }

                }
                catch (IOException e) // Catch the IOException that is raised if the pipe is broken or disconnected.
                {
                    return e.Message;
                }
            }
            return "";
        }

        public static void ANON_Client_SendMessage(string Message)
        {
            try
            {
                string Handle = File.ReadAllText(DefaultPath_TempName);
                Console.Write("Handle Acquired " + Handle);
                using (PipeStream pipeClient = new AnonymousPipeClientStream(PipeDirection.Out, Handle))
                {
                    Console.Write("Got Inside");
                    string status = pipeClient.TransmissionMode.ToString();

                    // Read user input and send that to the client process.
                    using (StreamWriter sw = new StreamWriter(pipeClient))
                    {
                        sw.AutoFlush = true;
                        // Send a 'sync message' and wait for client to receive it.
                        sw.WriteLine("SYNC");
                        pipeClient.WaitForPipeDrain();
                        // Send the console input to the client process.
                        sw.WriteLine(Message);
                    }

                }
            }
            catch (Exception E)
            {
                Console.WriteLine(E.Message);
            }
        }

    }

}