﻿using System;

namespace FIVE_MSG
{
    class Program
    {
        public static string FIVToolsExe = @"R:\dB\Software\FIVE_Tools\Bin\FIVE_Tools_Main.exe";

        //We can't verify who created this file . . can be turned off by making a local location trusted
        //https://community.spiceworks.com/topic/2107019-we-can-t-verify-who-created-this-file

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Please enter an argument to trigger a particular FIVTools functionality.  Press enter to exit.");
                Console.ReadLine();
            }
            else
            {
                string Message = string.Join(" ", args);
                if (args[0] == "20")
                {
                    //If we get windows permissions problems we could try to turn this off . . anyway, we can just do this directly from the Leica
                    Console.WriteLine("Starting FIVTools . . ");
                    System.Diagnostics.ProcessStartInfo SI = new System.Diagnostics.ProcessStartInfo(FIVToolsExe);
                    SI.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                    SI.Arguments = Message;
                    System.Diagnostics.Process.Start(SI);
                    System.Threading.Thread.Sleep(25);
                }
                else
                {
                    //Assumes FIVTools is already open
                    Console.WriteLine("Piping message to FIVTools . . (if it is not open, open then you may have to run again)");
                    PipeClass.Client_SendMessage(Message);
                    //System.Threading.Thread.Sleep(5);
                    
                }
            }
        }
    }
}
