﻿namespace _5SpoolManager
{
    partial class SpoolManagerSettingsForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.Folder_Fast = new System.Windows.Forms.TextBox();
            this.Folder_Mid = new System.Windows.Forms.TextBox();
            this.Folder_Archive = new System.Windows.Forms.TextBox();
            this.FractionFull_Fast = new System.Windows.Forms.TextBox();
            this.FractionFull_Mid = new System.Windows.Forms.TextBox();
            this.HourToRun = new System.Windows.Forms.TextBox();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSearchExp_Fast = new System.Windows.Forms.LinkLabel();
            this.lblSearchExp_Mid = new System.Windows.Forms.LinkLabel();
            this.lblSearchExp_Archive = new System.Windows.Forms.LinkLabel();
            this.btn_StartSchedule = new System.Windows.Forms.Button();
            this.btnOpenLogs = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txBx_FolderPlaceHold = new System.Windows.Forms.TextBox();
            this.btnPlaceHold = new System.Windows.Forms.Button();
            this.txBx_HoldDate = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(8, 172);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(85, 35);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(99, 172);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(85, 35);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // Folder_Fast
            // 
            this.Folder_Fast.Location = new System.Drawing.Point(8, 38);
            this.Folder_Fast.Name = "Folder_Fast";
            this.Folder_Fast.Size = new System.Drawing.Size(239, 23);
            this.Folder_Fast.TabIndex = 2;
            // 
            // Folder_Mid
            // 
            this.Folder_Mid.Location = new System.Drawing.Point(8, 88);
            this.Folder_Mid.Name = "Folder_Mid";
            this.Folder_Mid.Size = new System.Drawing.Size(239, 23);
            this.Folder_Mid.TabIndex = 3;
            // 
            // Folder_Archive
            // 
            this.Folder_Archive.Location = new System.Drawing.Point(8, 139);
            this.Folder_Archive.Name = "Folder_Archive";
            this.Folder_Archive.Size = new System.Drawing.Size(239, 23);
            this.Folder_Archive.TabIndex = 4;
            // 
            // FractionFull_Fast
            // 
            this.FractionFull_Fast.Location = new System.Drawing.Point(257, 38);
            this.FractionFull_Fast.Name = "FractionFull_Fast";
            this.FractionFull_Fast.Size = new System.Drawing.Size(100, 23);
            this.FractionFull_Fast.TabIndex = 5;
            // 
            // FractionFull_Mid
            // 
            this.FractionFull_Mid.Location = new System.Drawing.Point(257, 88);
            this.FractionFull_Mid.Name = "FractionFull_Mid";
            this.FractionFull_Mid.Size = new System.Drawing.Size(100, 23);
            this.FractionFull_Mid.TabIndex = 6;
            // 
            // HourToRun
            // 
            this.HourToRun.Location = new System.Drawing.Point(257, 139);
            this.HourToRun.Name = "HourToRun";
            this.HourToRun.Size = new System.Drawing.Size(100, 23);
            this.HourToRun.TabIndex = 7;
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(373, 12);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(629, 320);
            this.txBx_Update.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "Folder_Fast";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "Folder_Mid";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "Folder_Archive";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(257, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "FractionFull_Fast";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(257, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 15);
            this.label5.TabIndex = 13;
            this.label5.Text = "FractionFull_Mid";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(257, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 15);
            this.label6.TabIndex = 14;
            this.label6.Text = "HourToRun (24 H)";
            // 
            // lblSearchExp_Fast
            // 
            this.lblSearchExp_Fast.AutoSize = true;
            this.lblSearchExp_Fast.Location = new System.Drawing.Point(162, 20);
            this.lblSearchExp_Fast.Name = "lblSearchExp_Fast";
            this.lblSearchExp_Fast.Size = new System.Drawing.Size(69, 15);
            this.lblSearchExp_Fast.TabIndex = 15;
            this.lblSearchExp_Fast.TabStop = true;
            this.lblSearchExp_Fast.Text = "Search Exps";
            // 
            // lblSearchExp_Mid
            // 
            this.lblSearchExp_Mid.AutoSize = true;
            this.lblSearchExp_Mid.Location = new System.Drawing.Point(162, 70);
            this.lblSearchExp_Mid.Name = "lblSearchExp_Mid";
            this.lblSearchExp_Mid.Size = new System.Drawing.Size(69, 15);
            this.lblSearchExp_Mid.TabIndex = 16;
            this.lblSearchExp_Mid.TabStop = true;
            this.lblSearchExp_Mid.Text = "Search Exps";
            // 
            // lblSearchExp_Archive
            // 
            this.lblSearchExp_Archive.AutoSize = true;
            this.lblSearchExp_Archive.Location = new System.Drawing.Point(162, 121);
            this.lblSearchExp_Archive.Name = "lblSearchExp_Archive";
            this.lblSearchExp_Archive.Size = new System.Drawing.Size(69, 15);
            this.lblSearchExp_Archive.TabIndex = 17;
            this.lblSearchExp_Archive.TabStop = true;
            this.lblSearchExp_Archive.Text = "Search Exps";
            // 
            // btn_StartSchedule
            // 
            this.btn_StartSchedule.Location = new System.Drawing.Point(8, 213);
            this.btn_StartSchedule.Name = "btn_StartSchedule";
            this.btn_StartSchedule.Size = new System.Drawing.Size(119, 35);
            this.btn_StartSchedule.TabIndex = 18;
            this.btn_StartSchedule.Text = "Start Schedule";
            this.btn_StartSchedule.UseVisualStyleBackColor = true;
            this.btn_StartSchedule.Click += new System.EventHandler(this.btn_StartSchedule_Click);
            // 
            // btnOpenLogs
            // 
            this.btnOpenLogs.Location = new System.Drawing.Point(272, 172);
            this.btnOpenLogs.Name = "btnOpenLogs";
            this.btnOpenLogs.Size = new System.Drawing.Size(85, 35);
            this.btnOpenLogs.TabIndex = 19;
            this.btnOpenLogs.Text = "Open Logs";
            this.btnOpenLogs.UseVisualStyleBackColor = true;
            this.btnOpenLogs.Click += new System.EventHandler(this.btnOpenLogs_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 290);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 15);
            this.label7.TabIndex = 21;
            this.label7.Text = "Folder Path for Placing Hold";
            // 
            // txBx_FolderPlaceHold
            // 
            this.txBx_FolderPlaceHold.Location = new System.Drawing.Point(8, 308);
            this.txBx_FolderPlaceHold.Name = "txBx_FolderPlaceHold";
            this.txBx_FolderPlaceHold.Size = new System.Drawing.Size(271, 23);
            this.txBx_FolderPlaceHold.TabIndex = 20;
            this.txBx_FolderPlaceHold.TextChanged += new System.EventHandler(this.txBx_FolderPlaceHold_TextChanged);
            // 
            // btnPlaceHold
            // 
            this.btnPlaceHold.Enabled = false;
            this.btnPlaceHold.Location = new System.Drawing.Point(179, 270);
            this.btnPlaceHold.Name = "btnPlaceHold";
            this.btnPlaceHold.Size = new System.Drawing.Size(100, 35);
            this.btnPlaceHold.TabIndex = 22;
            this.btnPlaceHold.Text = "Place Hold";
            this.btnPlaceHold.UseVisualStyleBackColor = true;
            this.btnPlaceHold.Click += new System.EventHandler(this.btnPlaceHold_Click);
            // 
            // txBx_HoldDate
            // 
            this.txBx_HoldDate.Location = new System.Drawing.Point(285, 308);
            this.txBx_HoldDate.Name = "txBx_HoldDate";
            this.txBx_HoldDate.Size = new System.Drawing.Size(72, 23);
            this.txBx_HoldDate.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(285, 290);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 15);
            this.label8.TabIndex = 24;
            this.label8.Text = "Hold Date";
            // 
            // SpoolManagerSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 344);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txBx_HoldDate);
            this.Controls.Add(this.btnPlaceHold);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txBx_FolderPlaceHold);
            this.Controls.Add(this.btnOpenLogs);
            this.Controls.Add(this.btn_StartSchedule);
            this.Controls.Add(this.lblSearchExp_Archive);
            this.Controls.Add(this.lblSearchExp_Mid);
            this.Controls.Add(this.lblSearchExp_Fast);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.HourToRun);
            this.Controls.Add(this.FractionFull_Mid);
            this.Controls.Add(this.FractionFull_Fast);
            this.Controls.Add(this.Folder_Archive);
            this.Controls.Add(this.Folder_Mid);
            this.Controls.Add(this.Folder_Fast);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Name = "SpoolManagerSettingsForm";
            this.Text = "FIVE Spool Manager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button btnStart;
        private Button btnStop;
        private TextBox Folder_Fast;
        private TextBox Folder_Mid;
        private TextBox Folder_Archive;
        private TextBox FractionFull_Fast;
        private TextBox FractionFull_Mid;
        private TextBox HourToRun;
        private TextBox txBx_Update;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private LinkLabel lblSearchExp_Fast;
        private LinkLabel lblSearchExp_Mid;
        private LinkLabel lblSearchExp_Archive;
        private Button btn_StartSchedule;
        private Button btnOpenLogs;
        private Label label7;
        private TextBox txBx_FolderPlaceHold;
        private Button btnPlaceHold;
        private TextBox txBx_HoldDate;
        private Label label8;
    }
}