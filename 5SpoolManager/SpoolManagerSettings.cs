using System.Security.Cryptography;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Xml;
using System.Diagnostics;

namespace _5SpoolManager
{
    public partial class SpoolManagerSettingsForm : Form
    {
        public Store MainStore;
        public BackgroundWorker BWMain;

        public SpoolManagerSettingsForm()
        {
            InitializeComponent();
            MainStore = Store.Load();
            UpdateStore();

            BWMain = new BackgroundWorker();
            BWMain.WorkerReportsProgress = true;
            BWMain.WorkerSupportsCancellation = true;
            BWMain.DoWork += BW_DoWork;
            BWMain.ProgressChanged += BW_ProgressChanged;
            BWMain.RunWorkerCompleted += BW_RunWorkerCompleted;

            lblSearchExp_Archive.Click += SearchExp_Click;
            lblSearchExp_Fast.Click += SearchExp_Click;
            lblSearchExp_Mid.Click += SearchExp_Click;

            txBx_HoldDate.Text = DateTime.Today.AddMonths(3).ToShortDateString();

            this.FormClosing += SpoolManagerSettingsForm_FormClosing;
        }

        private void SpoolManagerSettingsForm_FormClosing(object? sender, FormClosingEventArgs e)
        {
            UpdateStore(false);
        }

        private void BW_RunWorkerCompleted(object? sender, RunWorkerCompletedEventArgs e)
        {
            txBx_Update.Text = "Done!!" + "\r\n" + txBx_Update.Text;
            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }

        private void BW_ProgressChanged(object? sender, ProgressChangedEventArgs e)
        {
            if (DateTime.Now.Second % 2 == 0) return; //Only allows updates once every other second
            if (txBx_Update.Text.Length > 20000) txBx_Update.Text = txBx_Update.Text.Substring(0, 10000);
            txBx_Update.Text = e.UserState.ToString() + "\r\n" + txBx_Update.Text;
        }

        private void BW_DoWork(object? sender, DoWorkEventArgs e)
        {
            //Mid first so that we can make room for the fast if needed
            Start_Main_Routine(MainStore.Folder_Mid, MainStore.Folder_Archive, MainStore.FractionFull_Mid, "", BWMain);
            if (BWH.Msg(BWMain, "Switching to second set")) return;
            Start_Main_Routine(MainStore.Folder_Fast, MainStore.Folder_Archive, MainStore.FractionFull_Fast, MainStore.Folder_Mid, BWMain);
        }

        public void UpdateStore(bool LoadIntoForm = true)
        {
            var Props = typeof(Store).GetProperties().ToDictionary(x => x.Name, y => y);
            foreach (Control C in this.Controls)
            {
                if (Props.ContainsKey(C.Name))
                {
                    var TB = (TextBox)C;
                    var P = Props[C.Name];
                    if (LoadIntoForm)
                    {
                        TB.Text = P.GetValue(MainStore).ToString();
                    }
                    else
                    {
                        if (P.PropertyType.Name == "Double") P.SetValue(MainStore, double.Parse(TB.Text));
                        else P.SetValue(MainStore, TB.Text);
                    }
                }
            }
            if (!LoadIntoForm) MainStore.Save();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            txBx_Update.Text = "Starting . . ";
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            UpdateStore(false); //save to the Main Store

            BWMain.RunWorkerAsync();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            BWMain.CancelAsync();
            if (_timer != null) _timer.Dispose();
        }

        private void SearchExp_Click(object? sender, EventArgs e)
        {
            UpdateStore(false); //save to the Main Store
            var lbl = (LinkLabel)sender;
            switch (lbl.Name)
            {
                case "lblSearchExp_Fast":
                    txBx_Update.Text = SearchExp(MainStore.Folder_Fast);
                    break;
                case "lblSearchExp_Mid":
                    txBx_Update.Text = SearchExp(MainStore.Folder_Mid);
                    break;
                case "lblSearchExp_Archive":
                    txBx_Update.Text = SearchExp(MainStore.Folder_Archive);
                    break;
                default:
                    break;
            }
        }

        public string SearchExp(string path)
        {
            var OldestList = DriveTools.ListOldestFoldersFirst(path);
            var NameList = OldestList.Select(x =>
                Path.GetFileName(x).Substring(Path.GetFileName(Path.GetDirectoryName(x)).Length)).ToList();
            NameList = NameList.Select(x => x.StartsWith("_") ? x.Substring(1) : x).ToList();
            int l1; string t;
            var Dict = new Dictionary<string, int>();
            foreach (var Name in NameList)
            {
                l1 = Name.LastIndexOf("_");
                if (l1 < 0) t = Name;
                else t = Name.Substring(0, l1);
                if (!Dict.ContainsKey(t)) Dict.Add(t, 0);
                Dict[t]++;
            }
            return string.Join("\r\n", Dict.Select(x => x.Key + "\t" + x.Value));
        }

        public void Play()
        {
            var List1 = DriveTools.ListOldestFoldersFirst("K:\\", 1);

            var List2 = DriveTools.ListOldestFoldersFirst("K:\\", 2);

            var FFree = DriveTools.FractionUsed("K:\\");
            var CFree = DriveTools.FractionUsed("c:\\temp\\");

            var FF = DriveTools.GetFolderSize_Mb(MainStore.Folder_Fast);
            var FM = DriveTools.GetFolderSize_Mb(MainStore.Folder_Mid);
        }

        public void DeleteSpecific(string LocSource, string LocDest)
        {
            var OldestList = DriveTools.ListOldestFoldersFirst(LocSource);
            var HS = new HashSet<string>(); var DeleteList = new List<string>();
            HS.Add("FIV523"); HS.Add("FIV491"); HS.Add("FIV493"); HS.Add("FIV424"); HS.Add("FIV431"); HS.Add("FIV433"); HS.Add("FIV471"); HS.Add("FIV479"); HS.Add("FIV598"); HS.Add("FIV604"); HS.Add("FIV412"); HS.Add("FIV449"); HS.Add("FIV487"); HS.Add("FIV511"); HS.Add("FIV489"); HS.Add("FIV490"); HS.Add("FIV497"); HS.Add("FIV528"); HS.Add("FIV534"); HS.Add("FIV540"); HS.Add("FIV541"); HS.Add("FIV547"); HS.Add("FIV548"); HS.Add("FIV550"); HS.Add("FIV576"); HS.Add("FIV577"); HS.Add("FIV586"); HS.Add("FIV588"); HS.Add("FIV619"); HS.Add("FIV655"); HS.Add("FIV656"); HS.Add("FIV666"); HS.Add("FIV692"); HS.Add("FIV695"); HS.Add("FIV719"); HS.Add("FIV735");
            foreach (var Fldr in OldestList)
            {
                foreach (var Exp in HS)
                {
                    if (Fldr.Contains(Exp))
                    {
                        DeleteList.Add(Fldr); break;
                    }
                }
            }

            string Not = "";
            foreach (var ToMove in DeleteList)
            {
                DriveTools.CheckBackup(ToMove, DriveTools.ConvertLocation(ToMove, LocDest, LocSource), true);
                if (DriveTools.CheckBackupFolderExists(ToMove, DriveTools.ConvertLocation(ToMove, LocDest, LocSource)))
                {
                    DriveTools.Delete(ToMove);
                }
                else
                {
                    Not += ToMove + "\r\n";
                }
            }
        }

        private static System.Timers.Timer _timer = new System.Timers.Timer();

        private void btn_StartSchedule_Click(object sender, EventArgs e)
        {
            txBx_Update.Text = "Running Schedule . . ";
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            UpdateStore(false); //save to the Main Store

            var timeToRun = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, (int)MainStore.HourToRun, 0, 0);
            if (timeToRun < DateTime.Now) timeToRun = timeToRun.AddDays(1);

            var timeToWait = (int)(timeToRun - DateTime.Now).TotalMilliseconds;

            _timer = new System.Timers.Timer(timeToWait);
            _timer.AutoReset = true;
            _timer.Elapsed += _timer_Elapsed;

            _timer.Start();
        }

        private void _timer_Elapsed(object? sender, System.Timers.ElapsedEventArgs e)
        {
            BWMain.RunWorkerAsync();
            _timer.Interval = 24 * 60 * 60 * 1000; //Every 24 hours after this
        }

        public void Start_Main_Routine(string LocSource, string LocDest, double FractionFull_Threshold, string MoveIfFullLocation = "", BackgroundWorker? BW = null)
        {
            //DeleteSpecific(LocSource, LocDest);
            DriveTools.DebugMode = false;
            DriveTools.CheckType = "FileSizes";

            //Get a list of the folders at the right level from the source
            BWH.Msg(BW, "Getting the list of folders from the source " + LocSource +" ..");
            var OldestList = DriveTools.ListOldestFoldersFirst(LocSource);

            //Next see if the space threshold has been crossed
            string update; string indent = "   ";
            double FractionUsed = DriveTools.FractionUsed(LocSource);
            if (FractionUsed < 0) { BWH.Msg(BW, "Try a mapped drive letter instead . . ?");   return; }

            while (FractionUsed > FractionFull_Threshold)
            {
                string ToMove = OldestList[0].ToUpper();
                //This is another way to "hold" certain items
                if (ToMove.Contains("QUAD") || ToMove.Contains("MASM7") || ToMove.Contains("SINGLE") || ToMove.Contains("HAP1")) { OldestList.RemoveAt(0); continue; }
                if (BWH.Msg(BW, LocSource + " is over size limit (" + FractionFull_Threshold.ToString("0.0%") + "), currently " + FractionUsed.ToString("0.0%"))) return;

                DateTime HoldTill = DriveTools.CheckForHold(ToMove);
                if (HoldTill > DateTime.Today)
                {
                    if (BWH.Msg(BW, indent + "This folder has a hold")) return;
                    continue;
                }

                //Either move or delete the oldest folders first
                DriveTools.CheckBackup(ToMove, DriveTools.ConvertLocation(ToMove, LocDest, LocSource), true, BW);

                if (BWH.Msg(BW, indent + "Delete or Move . . ")) return;
                if (MoveIfFullLocation == "")
                    update = DriveTools.Delete(ToMove);
                else
                    update = DriveTools.Move(ToMove, DriveTools.ConvertLocation(ToMove, MoveIfFullLocation, LocSource), BW);
                OldestList.RemoveAt(0);
                if (BWH.Msg(BW, indent + update)) return;
                FractionUsed = DriveTools.FractionUsed(LocSource);
            }

            //Delete any folders that are just empty
            if (BWH.Msg(BW, "Checking for Empty Folders . . .")) return;
            DriveTools.DeleteEmptyFolders(LocSource);

            //Backup the rest new or changed folders on Source to Dest
            if (BWH.Msg(BW, "Backing up other folders . . .")) return;
            DriveTools.CheckBackupSet(OldestList, LocSource, LocDest, BW);

            if (BWH.Msg(BW, "Done with this source: " + LocSource)) return;
        }

        private void txBx_FolderPlaceHold_TextChanged(object sender, EventArgs e)
        {
            btnPlaceHold.Enabled = txBx_FolderPlaceHold.Text != "";
        }

        private void btnOpenLogs_Click(object sender, EventArgs e)
        {
            OpenInNotepadPlus(BWH.LogPath);
        }

        public void OpenInNotepadPlus(string FileToOpen)
        {
            string NotepadPath = @"C:\Program Files\Notepad++\notepad++.exe";
            if (!File.Exists(NotepadPath)) NotepadPath = @"C:\Program Files (x86)\Notepad++\notepad++.exe";
            if (!File.Exists(NotepadPath)) NotepadPath = @"notepad.exe";
            try
            {
                var Pr = Process.Start(NotepadPath, FileToOpen);
            }
            catch { }
            return;
        }

        private void btnPlaceHold_Click(object sender, EventArgs e)
        {
            var FHC = new FolderHoldClass();
            FHC.HoldTill = DateTime.Parse(txBx_HoldDate.Text);
            FHC.Save(txBx_FolderPlaceHold.Text);
            txBx_Update.Text = "Hold Placed";
        }
    }

    public static class BWH
    {
        /// <summary>
        /// Returns whether cancellation is pending
        /// </summary>
        /// <param name="BW"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static bool Msg(BackgroundWorker? BW, string Message)
        {
            File.AppendAllText(LogPath, DateTime.Now + "\r\n" + Message.Substring(0,Math.Min(Message.Length,100)));
            if (BW == null) return false;
            BW.ReportProgress(0, Message);
            return BW.CancellationPending;
        }

        public static string LogPath =>Path.Combine(Store.DefaultPath, Store.LogName);
    }

    public class Store
    {
        public string Folder_Fast { get; set; }
        public string Folder_Mid { get; set; }
        public string Folder_Archive { get; set; }
        public double FractionFull_Fast { get; set; }
        public double FractionFull_Mid { get; set; }

        /// <summary>
        /// Enter the hour of the day in local time when you want this to run (in 24 hour time). So enter 22.5 to run at 10:30 pm
        /// </summary>
        public double HourToRun { get; set; }

        public Store()
        {
            Folder_Fast = ""; Folder_Mid = ""; Folder_Archive = "";
            FractionFull_Fast = 0.7;
            FractionFull_Mid = 0.5;
            HourToRun = 22;
        }

        public static Store Defaults()
        {
            var S = new Store();
            S.Folder_Fast = "K:\\";
            S.Folder_Mid = "M:\\";
            S.Folder_Archive = "X:\\dB\\Imaging\\";
            return S;
        }

        public static Store DefaultsTestMoves()
        {
            var S = new Store();
            string rootPath = @"E:\Temp\Spool";
            S.Folder_Fast = Path.Combine(rootPath, "Fast");
            S.Folder_Mid = Path.Combine(rootPath, "Mid");
            S.Folder_Archive = Path.Combine(rootPath, "Archive");
            return S;
        }

        public static string DefaultName = "FIVE_SpoolManagerSettings.xml";
        public static string DefaultPath = Path.GetTempPath();
        public static string LogName = "FIVE_SpoolManagerLog.txt";

        public static Store Load()
        {
            return Load(Path.Combine(DefaultPath, DefaultName));
        }

        public static Store Load(string FileName)
        {
            if (!File.Exists(FileName)) return Defaults();
            try
            {
                using (var stream = File.OpenRead(FileName))
                {
                    var serializer = new XmlSerializer(typeof(Store));
                    var SVP = (Store)serializer.Deserialize(stream);
                    return SVP;
                }
            }
            catch
            {
                return Defaults();
            }
        }

        public void Save()
        {
            Save(Path.Combine(DefaultPath, DefaultName));
        }

        public void Save(string FileName)
        {
            //this.SaveTime = DateTime.Now;
            var DI = new DirectoryInfo(Path.GetDirectoryName(FileName));
            if (!DI.Exists) DI.Create();
            using (var writer = new StreamWriter(FileName))
            {
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(writer, this);
                writer.Flush();
            }
        }
    }


    public class FileSystemMonitor
    {
        private System.Threading.Timer _timer;

        public FileSystemMonitor()
        {
            var timeToRun = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 2, 0, 0);
            if (timeToRun < DateTime.Now)
                timeToRun = timeToRun.AddDays(1);

            var timeToWait = (int)(timeToRun - DateTime.Now).TotalMilliseconds;

            _timer = new System.Threading.Timer(CheckChanges, null, timeToWait, Timeout.Infinite);
        }

        private void CheckChanges(object? state)
        {
            string[] fastFiles = Directory.GetFiles("Fast", "*", SearchOption.AllDirectories);
            foreach (string file in fastFiles)
            {
                string destination = Path.Combine("Archive", file.Substring("Fast".Length));
                if (!File.Exists(destination))
                {
                    File.Copy(file, destination);
                }
            }

            string[] fastFolders = Directory.GetDirectories("Fast", "*", SearchOption.AllDirectories);
            foreach (string folder in fastFolders)
            {
                string destination = Path.Combine("Archive", folder.Substring("Fast".Length));
                if (!Directory.Exists(destination))
                {
                    Directory.CreateDirectory(destination);
                }
            }
            // check for new files and folders in "Mid" location
            //...
            //...
            // schedule the next execution
            _timer.Change((int)TimeSpan.FromDays(1).TotalMilliseconds, Timeout.Infinite);
        }


        private void CheckChanges2(object state)
        {
            // check for new files and folders in "Fast" location
            string[] fastFiles = Directory.GetFiles("Fast", "*", SearchOption.AllDirectories);
            foreach (string file in fastFiles)
            {
                string destination = Path.Combine("Archive", file.Substring("Fast".Length));
                try
                {
                    if (!File.Exists(destination))
                    {
                        File.Copy(file, destination);
                    }
                }
                catch (IOException ioe)
                {
                    // handle the scenario where the destination file already exists
                    // or if there's an issue with permissions
                    // you can log the error or display a message to the user
                    // for example:
                    Console.WriteLine("Error occurred while copying file " + file + ": " + ioe.Message);
                }
            }

            string[] fastFolders = Directory.GetDirectories("Fast", "*", SearchOption.AllDirectories);
            foreach (string folder in fastFolders)
            {
                string destination = Path.Combine("Archive", folder.Substring("Fast".Length));
                try
                {
                    if (!Directory.Exists(destination))
                    {
                        Directory.CreateDirectory(destination);
                    }
                }
                catch (IOException ioe)
                {
                    // handle the scenario where the destination folder already exists
                    // or if there's an issue with permissions
                    // you can log the error or display a message to the user
                    // for example:
                    Console.WriteLine("Error occurred while creating directory " + folder + ": " + ioe.Message);
                }
            }
            // check for new files and folders in "Mid" location
            //...
            //...
            // schedule the next execution
            _timer.Change((int)TimeSpan.FromDays(1).TotalMilliseconds, Timeout.Infinite);
        }

    }

    // ---------------------------------------------------------------


    public class DriveTools
    {

        public static bool DebugMode = true;
        public static string CheckType = "FileNumber";

        public static string Delete(string Location)
        {
            string ret = "Delete\t" + Location + "\t";
            try
            {
                if (!DebugMode) Directory.Delete(Location, true);
                ret += "\tSuccess";
            }
            catch (Exception ioE) { ret += "\tFailed " + ioE.Message; }
            return ret;
        }

        public static string Move_Old(string Location1, string Location2)
        {
            //TODO: Recognize if the base location is the same and actually move, otherwise do a copy/delete

            string ret = "Move\t" + Location1 + "\t" + Location2;
            try
            {
                if (!DebugMode) Directory.Move(Location1, Location2);
                ret += "\tSuccess";
            }
            catch (Exception ioE) { ret += "\tFailed " + ioE.Message; }
            return ret;
        }

        public static string Move(string Location1, string Location2, BackgroundWorker? BW = null)
        {
            string ret;
            try
            {
                if (Path.GetPathRoot(Location1) == Path.GetPathRoot(Location2))
                {
                    // Base directory is the same, so perform a move
                    ret = "Move\t" + Location1 + "\t" + Location2;
                    if (!DebugMode) Directory.Move(Location1, Location2);
                    ret += "\tSuccess";
                }
                else
                {
                    // Base directory is different, so perform a copy/delete
                    ret = "Copy/Delete\t" + Location1 + "\t" + Location2;

                    // Copy directory and its contents
                    bool cont = CopyFolder(Location1, Location2, BW);

                    if (cont)
                    {
                        Directory.Delete(Location1, true); // Delete original directory and its contents
                        ret += "\tSuccess";
                    }
                    else ret += "\tProblem during copy, nothing deleted";
                }
            }
            catch (Exception ioE) { ret = "Failed\t" + ioE.Message; }
            return ret;
        }

        //False means there was an error or that copy didn't finish
        public static bool CopyFolder(string Location1, string Location2, BackgroundWorker? BW = null)
        {
            var fastFolders = new List<string>();
            fastFolders.Add(Location1);
            fastFolders.AddRange(Directory.GetDirectories(Location1, "*", SearchOption.AllDirectories).ToList());
            string indent = "       ";

            if (BWH.Msg(BW, indent + "Copying folders")) return false;

            foreach (string folder in fastFolders)
            {
                string destination = ConvertLocation(folder, Location2, Location1);
                try
                {
                    if (!Directory.Exists(destination)) Directory.CreateDirectory(destination);
                }
                catch (IOException ioe)
                {
                    if (BWH.Msg(BW, indent + "Error occurred while creating directory " + folder + ": " + ioe.Message)) return false;
                }
            }
            if (BWH.Msg(BW, indent + "Finding files")) return false;

            string[] fastFiles = Directory.GetFiles(Location1, "*", SearchOption.AllDirectories);
            if (BWH.Msg(BW, indent + "Copying " + fastFiles.Length.ToString() + " files")) return false;
            foreach (string file in fastFiles)
            {
                string destination = ConvertLocation(file, Location2, Location1);
                try
                {
                    if (!File.Exists(destination))
                    {
                        if (BWH.Msg(BW, indent + "copying: " + file)) return false;
                        File.Copy(file, destination);
                    }
                }
                catch (IOException ioe)
                {
                    if (BWH.Msg(BW, indent + "Error occurred while copying file " + file + ": " + ioe.Message)) return false;
                }
            }

            return true; //Only if everything finished correctly
        }


        public static string ConvertLocation(string FolderToConvert, string DestinationRoot, string SourceRoot)
        {
            string sRel = Path.GetRelativePath(SourceRoot, FolderToConvert);
            if (sRel == ".") sRel = "";
            return Path.Combine(DestinationRoot, sRel);
        }

        public static double FractionUsed(string Drive)
        {
            try
            {
                var drive = new DriveInfo(Drive);
                double freeSpace = (double)drive.AvailableFreeSpace / drive.TotalSize;
                return 1 - freeSpace;
            } catch
            {
                return -1;
            }
        }

        /// <summary>
        /// If it finds any folders older than 2 days that are empty, it will remove them
        /// </summary>
        public static void DeleteEmptyFolders(string Location1)
        {
            //var dirs = Directory.GetDirectories(Location1, "*", SearchOption.AllDirectories);
            var dirs = Directory.GetDirectories(Location1, "*");
            foreach (var dir in dirs)
            {
                var subdir = Directory.GetDirectories(dir, "*");
                if (subdir.Length > 0) continue;
                if (Path.GetFileName(dir).StartsWith("$")) continue;
                var files = Directory.GetFiles(dir);
                if (files.Length == 1)
                { //First remove any nonsense files
                    string fName = Path.GetFileName(files[0]).ToUpper();
                    if (fName == "INCELL_FIVETOOLS.DB" || fName == "THUMBS.DB")
                    {
                        File.Delete(files[0]);
                        files = new string[0];
                    }
                }
                if (files.Length == 0)
                {
                    Directory.Delete(dir);
                }
            }
        }

        /// <summary>
        /// Gives the list of folders sorted from oldest to newest
        /// </summary>
        /// <param name="RootFolder">Folder to start the search</param>
        /// <param name="Level">1 means look for the subfolders of the root folder, 2 means look for the subfolders inside of those</param>
        public static List<string> ListOldestFoldersFirst(string RootFolder, int Level = 2)
        {
            var sorted = new SortedList<DateTime, string>();

            ListOldestFoldersFirst_Recurse(RootFolder, Level - 1, sorted);

            return sorted.Values.ToList();
        }

        private static void ListOldestFoldersFirst_Recurse(string LookFolder, int Level, SortedList<DateTime, string> ListToAdd)
        {
            string[] directories = Directory.GetDirectories(LookFolder);
            DateTime Key; DirectoryInfo current;
            foreach (string dir in directories)
            {
                if (Level == 0)
                {
                    current = new DirectoryInfo(dir);
                    Key = current.LastWriteTime;
                    while (ListToAdd.ContainsKey(Key))
                    {
                        Key = Key.AddMilliseconds(1);
                    }
                    ListToAdd.Add(Key, current.FullName);
                }
                else
                {
                    ListOldestFoldersFirst_Recurse(dir, Level - 1, ListToAdd);
                }
            }
        }

        public static void CheckBackupSet(List<string> FolderList, string SourceLocation, string DestinationLocation, BackgroundWorker? BW = null)
        {
            foreach (var folder in FolderList)
            {
                CheckBackup(folder, DriveTools.ConvertLocation(folder, DestinationLocation, SourceLocation), true, BW);
            }
        }

        public static bool CheckBackupFolderExists(string SourcePath, string BackupPath)
        {
            return Directory.Exists(BackupPath);
        }
        public static string CheckBackup(string SourcePath, string BackupPath, bool CopyIfNeeded = true, BackgroundWorker? BW = null)
        {
            bool StatusBackedUp = false;
            string indent = "    ";
            string ret = "CheckBackup";
            if (BWH.Msg(BW, SourcePath)) return "Cancelled";
            if (Directory.Exists(BackupPath))
            {
                if (CheckType == "FileNumber")
                {
                    if (BWH.Msg(BW, indent + "Exists in Archive, File Counting..")) return "Cancelled";
                    var FS1 = GetFileNum(SourcePath);
                    var FS2 = GetFileNum(BackupPath);
                    if (FS1 == FS2) StatusBackedUp = true;
                }
                else if (CheckType == "FileSizes")
                {
                    if (BWH.Msg(BW, indent + "Exists in Archive, Checking Sizes")) return "Cancelled";
                    var FS1 = GetFolderSize_Mb(SourcePath);
                    var FS2 = GetFolderSize_Mb(BackupPath);
                    if (FS1 != FS2)
                    {
                        //Lets see how different they are
                        var Diff = Math.Abs(FS2 - FS1);
                        ret += "Sizes differ by " + Diff.ToString("0.0") + "MB, ";
                        if (Diff < 2) StatusBackedUp = true;
                    }
                    else
                    {
                        StatusBackedUp = true;
                    }
                }
            }
            if (!StatusBackedUp && CopyIfNeeded)
            {
                if (BWH.Msg(BW, indent + "Copying . . ")) return "Cancelled";
                CopyFolder(SourcePath, BackupPath, BW);
                ret += "Newly Copied";
            }
            if (BWH.Msg(BW, indent + ret)) return "Cancelled";
            return ret;
        }

        public static void BackupFileWatcher(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Created)
            {
                string destination = Path.Combine("Archive", e.FullPath);
                if (!File.Exists(destination))
                {
                    File.Copy(e.FullPath, destination);
                }
            }
        }

        public static string GetChecksum(string path)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(path))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                }
            }
        }

        public static int GetFileNum(string path)
        {
            return new DirectoryInfo(path).EnumerateFiles("*", SearchOption.AllDirectories).Count();
        }

        public static double GetFolderSize_Mb(string path)
        {
            double Size = new DirectoryInfo(path).EnumerateFiles("*", SearchOption.AllDirectories).Sum(file => file.Length);
            return Size / 1024 / 1024;
        }

        /// <summary>
        /// Checks whether a "hold" has been placed on this folder. This will prevent it from getting moved or deleted. Use PlaceHold() to place a hold on the file
        /// </summary>
        public static DateTime CheckForHold(string FolderPath)
        {
            //Automatically returns yesterday if there is no hold file
            var FH = FolderHoldClass.Load(FolderPath);
            return FH.HoldTill;
        }

        public static void PlaceHold(string FolderPath, DateTime HoldTill)
        {
            var FH = new FolderHoldClass() { HoldTill = HoldTill };
            FH.Save(FolderPath);
        }

        //Always gives the whole drive, not just the folder
        //[DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //static extern bool GetDiskFreeSpaceEx(string lpDirectoryName,
        //    out ulong lpFreeBytesAvailable,
        //    out ulong lpTotalNumberOfBytes,
        //    out ulong lpTotalNumberOfFreeBytes);

        //public static long GetFolderSize_Ex(string folder)
        //{
        //    if (!GetDiskFreeSpaceEx(folder, out _, out ulong totalSize, out _))
        //    {
        //        throw new System.ComponentModel.Win32Exception();
        //    }
        //    return (long)totalSize;
        //}
    }

    public class FolderHoldClass
    {
        public DateTime HoldTill { get; set; }
        public string? Note { get; set; }

        public static string FileName = "sm5HOLD.xml";

        public void Save(string FolderPath)
        {
            var serializer = new XmlSerializer(typeof(FolderHoldClass));
            using (var writer = new StreamWriter(Path.Combine(FolderPath, FileName)))
            {
                serializer.Serialize(writer, this);
            }
        }

        public static FolderHoldClass Load(string FolderPath)
        {
            string fileName = Path.Combine(FolderPath, FileName);
            if (!File.Exists(fileName)) return new FolderHoldClass() { HoldTill = DateTime.Today.AddDays(-1) };
            var ser = new XmlSerializer(typeof(FolderHoldClass));
            FolderHoldClass fhc;
            using (XmlReader reader = XmlReader.Create(fileName))
            {
                fhc = (FolderHoldClass)ser.Deserialize(reader);
                return fhc;
            }
        }
    }

    // ---------------------------------------------------------------


}

