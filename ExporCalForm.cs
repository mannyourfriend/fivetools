using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using FIVE.ImageCheck;
using FIVE.FOVtoRaftID;
using FIVE.InCellLibrary;
using FIVE_IMG;
using System.Diagnostics;

namespace FIVE.RaftCal
{
    public partial class ExporCalForm : Form
    {
        FormRaftCal ParentCalForm;
        RaftImage_Export_Settings Settings;

        #region FormWide - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        public ExporCalForm(FormRaftCal Parent = null)
        {
            ParentCalForm = Parent;
            InitializeComponent();
            SaveLoad_Export_Settings(true);
            Enable_Save_Settings(true);
            SaveLoad_Annotation_Settings(true);
        }

        private void ExporCalForm_Load(object sender, EventArgs e)
        {

        }

        private void ExporCalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        #endregion

        #region Export Settings - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        //This was a function in the old version that got the default path copied
        private void LoadLoc()
        {
            RaftImage_Export_Settings.Load();
            Clipboard.SetText(RaftImage_Export_Settings.DefaultPath);
            MessageBox.Show("Settings for this export and also list-based export are saved here: " + RaftImage_Export_Settings.DefaultPath + "\r\n\r\n (and copied to the clipboard)  You can edit that file first (XML), then click on an export button again (it checks for the file every time you click). Those settings will become the new defaults. Delete the file to 'reset' back to factory defaults.");
        }

        public void SaveLoad_Export_Settings(bool LoadIntoForm = true)
        {
            if (Settings == null) Settings = RaftImage_Export_Settings.Load();
            Type RIES = Settings.GetType();
            PropertyInfo[] Fields = RIES.GetProperties();
            Control C;
            foreach (PropertyInfo field in Fields)
            {
                C = Controls.Find(field.Name, true).First();
                if (field.PropertyType.Name.Contains("String"))
                {
                    TextBox CB = (TextBox)C;
                    if (LoadIntoForm) CB.Text = (string)field.GetValue(Settings);
                    else field.SetValue(Settings, CB.Text);
                }
                else
                if (field.PropertyType.Name.Contains("Boolean"))
                {
                    CheckBox CB = (CheckBox)C;
                    if (LoadIntoForm) CB.Checked = (bool)field.GetValue(Settings);
                    else field.SetValue(Settings, CB.Checked);
                }
                else
                {
                    TextBox CB = (TextBox)C;
                    if (LoadIntoForm)
                        CB.Text = field.GetValue(Settings).ToString();
                    else
                    {
                        if (field.PropertyType.Name.Contains("Single"))
                        {
                            field.SetValue(Settings, float.Parse(CB.Text));
                        }
                        else
                        {
                            field.SetValue(Settings, int.Parse(CB.Text));
                        }
                    }
                }
            }
        }
        public String GetImageExtensionFromWhiteList(String ImageExtension)
        {

            //first return the bmp default if there's nothing there
            //so there's a lot going on in this function, first we need to make sure only ASCII character's were used, the best way to do that is just to check in between 127 and 0 inclusive
            //next to dramatically limit the size of the hashset we make go toLower, and then we trim any empty space that might be on any side of the input string. After that we want to take out
            //any periods that the user must have put it on purpose or accidently and make sure that we are the one's putting in the period (even if it looks to them like they think they are)
            String defaul = ".bmp";
            if (String.IsNullOrEmpty(ImageExtension) || ImageExtension.Trim() == ".")
            {
                return defaul;
            }
            ImageExtension = ImageExtension.ToLower();
            ImageExtension = ImageExtension.Trim();
            //the true image format could be hidden by multiple periods or even several images could be saved this way I mean it's not much but it's something
            //much more likely it prevents our users from using double periods
            ImageExtension = "." + ImageExtension;
            ImageExtension = ImageExtension.Replace(".", "");
            Boolean isASCII = ImageExtension.All(t => t <= SByte.MaxValue || t >= Byte.MinValue);
            if (!isASCII)
            {
                return defaul;
            }
            //next instead of trying to write strange regex, or attempt classification logic or call an api  i've made a list of all of the images that the web allows without question formats that the three main browsers (safari, chrome, edge) support by default
            //from there I got pretty much all of the raw types that we would commonly see in the wild including some of the old kodak type formats. Especially DNG, PDF ,RAF and a few others. You'll notice  that it starts with formats you recognize and goes further
            //and further away from likely images we would handle. I finally got to a point where things were obscure enough that you would need to be using a very strange camera and such a camera would have some kind of conversion so once I was happy with what I had.
            HashSet<String> WhiteList = new HashSet<String> { "jpeg", "jfif", "jpeg2000", "jpg", "tiff", "tif", "gif", "bmp", "png", "webp", "raw", "dng", "raf", "eps", "exif", "ppm", "pgm", "xbm",
                                        "pbm", "pnm", "heif", "avif", "bpg", "webpg", "ai", "indd", "psd", "svg", "pdf", "3fr", "arw", "pjp", "pjpeg", "av1", "apng", "mng",
                                        "ari", "bay", "braw", "crw", "cr2", "cr3", "cap", "crx", "tiff/ep", "x3f", "iiq", "fff", "dcr", "drf", "erf", "eip", "k25", "kdc",
                                        "mef", "mos", "mdc", "mrw", "nef", "pxn", "r3d", "ptx", "pef", "rw2", "rwz", "sr2", "srf", "srw", "ico", "cur", "orf", "obm"};
            if (WhiteList.Contains(ImageExtension))
            {
                return ImageExtension;
            }
            else
            {
                return defaul;
            }
        }
        private void btn_SaveSettings_Click(object sender, EventArgs e)
        {
            SaveLoad_Export_Settings(false);
            Enable_Save_Settings(true);
        }

        private void btn_DefaultSettings_Click(object sender, EventArgs e)
        {
            Settings = RaftImage_Export_Settings.Defaults();
            SaveLoad_Export_Settings(true);
        }

        public string Default_ExportSettingsFolder = @"R:\dB\Software\FIVE_Tools\Settings\";

        private void btn_SaveAsSettings_Click(object sender, EventArgs e)
        {
            btn_SaveSettings_Click(sender, e); //Do this first

            var SFD = new SaveFileDialog();
            SFD.InitialDirectory = Default_ExportSettingsFolder;
            SFD.Filter = "xml file|*.xml";
            SFD.Title = "Enter a location to Save the settings";
            var DR = SFD.ShowDialog();
            string extension = GetImageExtensionFromWhiteList(ImageExtension.Text);
            if (DR == DialogResult.OK)
            {
                Settings.Save();
                HashSet<String> OraclesBlackList = new HashSet<String>() {  ".jspx", "*\\jspx", "*jspx", "^jspx",
                                                                                    ".jsp", "*\\jsp", "*jsp", "^jsp",
                                                                                    ".exe", "*\\exe", "*exe", "^exe",
                                                                                    ".bat",  "*\\bat", "\\bat", "*bat",
                                                                                    ".app", "*\\app", "\\bat", "*app"};
                string testFile = SFD.FileName + extension;
                if (OraclesBlackList.Contains(testFile))
                {
                    return;
                }
                else
                {
                    txBx_Update.Text = "Saved Settings to \r\n" + SFD.FileName + extension;
                    ImageExtension.Text = extension;
                }
            }
        }

        private void btn_LoadSettings_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.InitialDirectory = Default_ExportSettingsFolder;
            OFD.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            OFD.Title = "Please select the settings XML file to Load.";
            var DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                Settings = RaftImage_Export_Settings.Load(OFD.FileName); //Makes it Active
                SaveLoad_Export_Settings(true);
            }
        }

        private void Form_Settings_Changed(object sender, EventArgs e)
        {
            Enable_Save_Settings(false);
        }

        public void Enable_Save_Settings(bool TurnOnActions)
        {
            btn_SaveSettings.Enabled = !TurnOnActions;
            btn_ExportAllRaftImages.Enabled = btn_ExportFromList.Enabled = btn_ExportRaftImages.Enabled = TurnOnActions;
        }

        #endregion

        #region Export Methods - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        private void ExportFromList()
        {
            if (ParentCalForm.CurrentWell.CalibrationRaftSettings == null)
            {
                MessageBox.Show("Currently only possible for calibrated rafts.");
                return;
            }

            //Ask user for a file that has RaftIDs
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "This will look for ANY and ALL Raft IDs in the file, make sure that only ones related to this plate are in the selected file.";
            ofd.Filter = "txt files (*.txt)|*.txt|csv files (*.csv)|*.csv|All files (*.*)|*.*";
            ofd.FilterIndex = 1;
            ofd.RestoreDirectory = true;
            DialogResult DR = ofd.ShowDialog();
            if (DR == DialogResult.Cancel) return;
            string textContents = File.ReadAllText(ofd.FileName);
            System.Text.RegularExpressions.Regex Regexp = new System.Text.RegularExpressions.Regex("[A-Z]\\d[A-Z]\\d");
            System.Text.RegularExpressions.MatchCollection MC = Regexp.Matches(textContents);

            //Get the unique list of IDs
            HashSet<string> HS = new HashSet<string>();
            foreach (System.Text.RegularExpressions.Match match in MC) HS.Add(match.Value);

            //Package up the IDs so they can be exported (link them to images and points)
            ImageCheck_PointList ICPL = ImageCheck_PointList.FromRafts(ParentCalForm.IC_Folder, HS);

            //Now call the export on this
            Trigger_Export(Settings, ICPL);

            //Go back and get remaining RaftIDs
            txBx_Update.Text += ". . " + (HS.Count - ICPL.Count) + " remaining rafts which weren't exported.";
        }

        private void btn_ExportRaftImages_Click(object sender, EventArgs e)
        {
            //Use the loaded list of points to go through with the current image settings and crop out the rafts that are marked.  We can name them with prefix or suffix that indicates which marking they got
            Trigger_Export(Settings, ParentCalForm.ImageChk_List);
        }

        private void btn_ExportAllRaftImages_Click(object sender, EventArgs e)
        {
            //Export all
            Trigger_Export(Settings, new ImageCheck_PointList());
        }

        private void btn_ExportFromList_Click(object sender, EventArgs e)
        {
            //Export from List
            ExportFromList();
        }

        private void Trigger_Export(RaftImage_Export_Settings SettingsToUse, ImageCheck_PointList PLToUse)
        {
            string PName = Path.Combine(SettingsToUse.ExportFolderBase, ParentCalForm.IC_Folder.PlateID);
            DirectoryInfo DI = new DirectoryInfo(PName);
            if (DI.Exists)
            {
                DialogResult DR = MessageBox.Show("That subfolder appears to already exist. Do you want to continue (may overwrite previous exports)?", "Export Folder Check", MessageBoxButtons.OKCancel);
                if (DR == DialogResult.Cancel) return;
            }

            var Args = new Tuple<RaftImage_Export_Settings, ImageCheck_PointList, INCELL_Folder, wvDisplayParams>(
                                 SettingsToUse, PLToUse, ParentCalForm.IC_Folder, ParentCalForm.CurrentWVParams);
            bgMain.RunWorkerAsync(Args);
        }

        public void ExportImages(RaftImage_Export_Settings Settings, ImageCheck_PointList PointList, INCELL_Folder ICFolder, wvDisplayParams WVParams, BackgroundWorker BW)
        {
            XDCE_ImageGroup Welli; string FName; // = ICFolder.XDCE.Wells.First().Value;

            string Plate = ICFolder.PlateID;
            DirectoryInfo DI = new DirectoryInfo(Settings.ExportFolderBase); if (!DI.Exists) DI.Create();
            DI = new DirectoryInfo(Path.Combine(Settings.ExportFolderBase, Plate)); if (!DI.Exists) DI.Create();
            string ExportFolder = DI.FullName;
            //StreamWriter SW = new StreamWriter(Path.Combine(ExportFolder, Plate) + ".txt");
            //SW.WriteLine("Annotation" + "\t" + "Plate" + "\t" + "Well" + "\t" + "RaftID" + "\t" + "");
            //SaveXMLSettings(); //TODO make an XML file with the settings
            if (BW.CancellationPending) return;

            HashSet<string> RaftsExported = new HashSet<string>(); //Ensures that we don't export a raft more than once
            ReturnRaft RR; RectangleF r; Bitmap nb; Bitmap resized; float SquareRatio; float SmallerDim;
            XDCE_Image xI; Bitmap bmap = null; Bitmap bmapMult = null; string Annotation; byte[] TE1; string ExportSubFolder = ExportFolder;
            int PointsExported = 0; int PointsTotal = 0; int NonSquare = 0; int counter = 0; bool IgnoreBadRafts;
            ImageCheck_PointList PointListTemp;

            if (PointList.FOVs.Count == 0)
            {
                foreach (var well in ICFolder.XDCE.Wells)
                {
                    if (!well.Value.HasRaftCalibration)
                    {
                        BW.ReportProgress(0, "!!! Warning, Well " + well.Value.NameAtLevel + " isn't calibrated");
                        return;
                    }
                }
                BW.ReportProgress(0, "Exporting ALL RAFTS . . ");
                PointListTemp = ImageCheck_PointList.FromRafts(ICFolder);
                IgnoreBadRafts = true;
            }
            else
            {
                BW.ReportProgress(0, "Exporting only ANNOTATED RAFTS . . ");
                PointListTemp = PointList;
                IgnoreBadRafts = false;
            }

            HashSet<string> SkipWells = new HashSet<string>(); //Skips problem wells
            //Now go through and actually export
            if (PointListTemp.Count == 0) { BW.ReportProgress(0, "!!!! Nothing to export, could be because Wells are not Calibrated. !!"); return; }
            Settings.SaveCurrent(ICFolder, WVParams, Path.Combine(ExportFolder, "ExportSettings.xml")); //Write a file out that has the settings in it
            foreach (string Well_Field in PointListTemp.WellFields)
            {
                List<ImageCheck_Point> points = PointListTemp.FromDictionaryWellField(Well_Field);
                Welli = ICFolder.XDCE.Wells[points[0].Well_Label];
                if (SkipWells.Contains(Welli.NameAtLevel)) { continue; }
                if (!Welli.HasRaftCalibration) { BW.ReportProgress(0, "!!! Warning, Well " + Welli.NameAtLevel + " isn't calibrated"); SkipWells.Add(Welli.NameAtLevel); continue; }
                xI = Welli.GetField(points[0].FOV, 0); // Instead of 0, used to be Wavelength
                bmap = ParentCalForm.CombinedBMAP(Welli, xI); //Debug.Print(Welli.NameAtLevel + " " + xI.FOV);
                if (++counter % 5 == 0) BW.ReportProgress(1, counter); if (BW.CancellationPending) { BW.ReportProgress(0, "User Cancelled"); return; }
                foreach (ImageCheck_Point point in points)
                {
                    PointsTotal++;
                    RR = Welli.CalibrationRaftSettings.FindRaftID_RR(point.Plate_Xum, point.Plate_Yum);
                    if (!Settings.Include_Fiducial_Rafts && RR.IsFiducial) continue;
                    if (RaftsExported.Contains(RR.RaftID)) continue; //Seems necessary, but I had taken it out for some reason. . 
                    r = ReturnRaft.RectFromCorners(xI, RR, bmap.Size);

                    SmallerDim = Math.Min(r.Width, r.Height); SquareRatio = SmallerDim / Math.Max(r.Width, r.Height); //if (PixelsExpand > 0) r = new RectangleF(r.Left - PixelsExpand, r.Top + PixelsExpand, r.Width + PixelsExpand, r.Height + PixelsExpand); //Implementation of Pixels expand . . but isn't quite right
                    if (Settings.OnlyExportSquare)
                    {
                        if (SquareRatio < Settings.Min_AspectRatio_Keep) { NonSquare++; continue; } //Skip the export if this isn't mostly square

                        // 7/2021 this was the code to previously crop based on this multiplier, but now we are using it to stretch
                        //float shift = SmallerDim * 0.5F * (1 - Settings.ResizeMultiplier);
                        //float newSize = SmallerDim * Settings.ResizeMultiplier;
                        //r = new RectangleF(r.Left + shift, r.Top + shift, newSize, newSize); //Crop the image slightly

                        //This new style we actually crop out the region of interest first, resize it, then do the final crop (trimming)
                        r = new RectangleF(r.Left, r.Top, SmallerDim, SmallerDim);
                        try
                        {
                            bmapMult = bmap.Clone(r, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                        }
                        catch { continue; }
                        if (Settings.ResizeMultiplier != 1)
                        {
                            int newSize = (int)(SmallerDim * Settings.ResizeMultiplier);
                            bmapMult = new Bitmap(bmapMult, new Size(newSize, newSize));
                        }
                    } //NotSquare is not implemented
                    //Trying to restrict the final size
                    float CropToMax = Math.Min(Settings.CropToMaxSize, Math.Min(bmapMult.Size.Height, bmapMult.Size.Width));
                    if (Settings.CropToMaxSize >= 1 && CropToMax <= bmapMult.Width)
                    {
                        // This is the code before 7/2021 when we already have the cropped out piece that we need that is resized
                        //float CropToMax = Math.Min(Settings.CropToMaxSize, Math.Min(r.Size.Height, r.Size.Width));
                        //SizeF FinalSize = new SizeF(CropToMax, CropToMax); PointF Shift2 = new PointF((r.Size.Width - FinalSize.Width) / 2, (r.Size.Height - FinalSize.Height) / 2);
                        //r = new RectangleF(new PointF(r.Left + Shift2.X, r.Top + Shift2.Y), FinalSize); //r = new RectangleF(Math.Min(bmap.Width, Math.Max(0, r.Left)), Math.Min(bmap.Height, Math.Max(0, r.Top)), Math.Min(bmap.Width, Math.Max(0, r.Width)), Math.Min(bmap.Height, Math.Max(0, r.Height))); //Old one but the math isn't right
                        SizeF FinalSize = new SizeF(CropToMax, CropToMax);
                        PointF Shift2 = new PointF((bmapMult.Size.Width - FinalSize.Width) / 2, (bmapMult.Size.Height - FinalSize.Height) / 2);
                        r = new RectangleF(new PointF(Shift2.X, Shift2.Y), FinalSize);
                    }
                    else
                    { r = new RectangleF(new PointF(0, 0), (SizeF)bmapMult.Size); }
                    if (Settings.CropToMaxSize >= 1 && Settings.OnlyExportFullSize && r.Size.Width < Settings.CropToMaxSize) { continue; } //record how many this is catching?
                    if (float.IsNaN(r.Width) || float.IsNaN(r.Height) || SmallerDim < 0)
                    {
                        if (!IgnoreBadRafts) BW.ReportProgress(1, point.RaftID); //Problem with the calibration, couldn't figure out the rafts . . 
                    }
                    else
                    {
                        try
                        {
                            //For Jack's program, we need to export as RGB, not ARGB
                            //nb = bmap.Clone(r, System.Drawing.Imaging.PixelFormat.Format24bppRgb); //Common to throw an error if the rectangle is weirdly sized
                            nb = bmapMult.Clone(r, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                        }
                        catch { continue; }
                        Annotation = PointList.FromDictionaryRaftID_Combined(point.RaftID).Replace("/", "_");
                        if (Annotation == "") Annotation = "A";  //if (Annotation.StartsWith("No")) continue; //Use this to only export one type of annotation
                        FName = (Settings.FileName_ByAnnotation ? Annotation + "-" : "") + Plate + "." + point.Well_Label + "." + point.RaftID + "." + (!RaftsExported.Contains(point.RaftID) ? "0" : "1"); //The .0 will always have that one, but may have a second one that is .1 or .2, etc
                        FName = FName.Replace("  ", " ").Trim();
                        if (Settings.Resize_And_MLExport) //This is for direct pixel data export
                        {
                            resized = new System.Drawing.Bitmap(nb, new Size(Settings.PixelList_ExportSize, Settings.PixelList_ExportSize));
                            TE1 = FormRaftCal.BitmapToByteArray(resized);
                            //SW.WriteLine(Annotation + "\t" + Plate + "\t" + point.Well_Label + "\t" + point.RaftID + "\t" + String.Join("\t", TE1)); resized.Save(Path.Combine(ExportFolder, FName) + "_s.bmp"); //To export the actual resized image
                        }
                        if (Settings.FolderName_ByAnnotation)
                        {
                            ExportSubFolder = Path.Combine(ExportFolder, Annotation);
                            DirectoryInfo DISF = new DirectoryInfo(ExportSubFolder); if (!DISF.Exists) DISF.Create();
                        }
                        nb.Save(Path.Combine(ExportSubFolder, FName) + "." + Settings.ImageExtension);
                        if (Settings.Export_4Rotations)
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                nb.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                nb.Save(Path.Combine(ExportSubFolder, FName) + "-" + i + "." + Settings.ImageExtension);
                            }
                        }
                        RaftsExported.Add(point.RaftID); PointsExported++;
                    }
                }
                bmap.Dispose();
            }
            //SW.Close();
            BW.ReportProgress(0, "Exported " + RaftsExported.Count + " raft Images to " + ExportFolder + " from " + PointsExported + " points. " + NonSquare + " rafts excluded since not square enough.");
        }

        #endregion

        #region RunExport - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            bgMain.CancelAsync();
        }

        private void bgMain_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = (Tuple<RaftImage_Export_Settings, ImageCheck_PointList, INCELL_Folder, wvDisplayParams>)e.Argument;
            ExportImages(Args.Item1, Args.Item2, Args.Item3, Args.Item4, bgMain);
        }

        private void bgMain_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 0) txBx_Update.Text = e.UserState + "\r\n" + txBx_Update.Text;
            else txBx_Update.Text = e.UserState + ", " + txBx_Update.Text;
        }

        private void bgMain_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        #endregion

        #region Random - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        private void btn_PlateMetadata(object sender, EventArgs e)
        {
            //Saves a file with all the annotations for this whole plate
            Clipboard.SetText(ImageCheck_GetTable(ParentCalForm.ImageChk_List));
            txBx_Update.Text = "Copied RaftIDs to Clipboard.";
        }
        public static string ImageCheck_GetTable(ImageCheck_PointList PointList)
        {
            System.Text.StringBuilder SB = new System.Text.StringBuilder();
            char delim = '\t';
            foreach (ImageCheck_Point Point in PointList.Points)
            {
                SB.Append(Point.Well_Label + delim + Point.FOV + delim + Point.RaftID + delim + delim + string.Join("|", Point.Annotations.Select(x => x.Value)) + "\r\n");
            }
            return SB.ToString();
        }

        #endregion

        #region Annotations - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        public ImageCheck_Annotation_Schema ActiveSchema { get => ImageCheck_Annotation_Schema.ActiveSchema; set { ImageCheck_Annotation_Schema.ActiveSchema = value; } }

        public string Default_AnnotationSchemaFolder = @"R:\dB\Software\FIVE_Tools\Schemas\";

        private void SaveLoad_Annotation_Settings(bool v)
        {
            txBx_AnnotationsInfo.Text = ActiveSchema.GetAsText();
        }

        public void LoadSchema(string Path)
        {
            ActiveSchema = ImageCheck_Annotation_Schema.Load(Path);
            txBx_AnnotationsInfo.Text = ActiveSchema.GetAsText();
            txBx_AnnotationsCurrentSet.Text = Path;
        }

        private void btn_Annotations_Load_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.InitialDirectory = Default_AnnotationSchemaFolder;
            OFD.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            OFD.Title = "Please select the Annotation Schema XML file to Load.";
            var DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                LoadSchema(OFD.FileName); //Makes it Active
            }
        }

        private void btn_Annotations_New_Click(object sender, EventArgs e)
        {
            txBx_Update.Text = "Not Implemented";
        }

        private void btn_Annotations_SaveAs_Click(object sender, EventArgs e)
        {
            var SFD = new SaveFileDialog();
            SFD.InitialDirectory = Default_AnnotationSchemaFolder;
            SFD.Filter = "xml file|*.xml";
            SFD.Title = "Enter a location to Save this annotation Schema";
            var DR = SFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                ActiveSchema.Save(SFD.FileName);
                txBx_AnnotationsCurrentSet.Text = SFD.FileName;
            }
        }

        private void btn_Annotations_Open_Click(object sender, EventArgs e)
        {
            //Opens the current one for editing
            bool cont = true; FileInfo FI = null;
            if (txBx_AnnotationsCurrentSet.Text == "") cont = false;
            if (cont) { FI = new FileInfo(txBx_AnnotationsCurrentSet.Text); if (!FI.Exists) cont = false; }
            if (!cont)
            {
                txBx_Update.Text = "Please load an annotation set first (or Save As)";
                return;
            }
            Process Pr = new Process();
            try
            {
                Pr = Process.Start("notepad++.exe", FI.FullName);
            }
            catch
            {
                Pr = Process.Start("notepad.exe", FI.FullName);
            }
        }

        private void btn_Import_Annotations_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.InitialDirectory = Path.Combine(@"R:\FIVE\EXP", ParentCalForm.IC_Folder.FIViD);
            OFD.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            OFD.Title = "Please select at TXT file with Columns = PlateID, RaftID, Name, Value, Well Label, FOV";
            var DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                List<string[]> Table1 = LoadAnnotationImportTable(OFD.FileName);
                if (Table1 == null)
                {
                    txBx_Update.Text = "Problem importing the table, check the headings (& use tab delimited text).\r\n" + OFD.Title;
                    return;
                }
                txBx_Update.Text = AppendAnnotationsFromFile(Table1);
                ParentCalForm.UpdateAll();
            }
        }

        private List<string[]> LoadAnnotationImportTable(string fileName)
        {
            var table1 = new List<string[]>(); string[] tLine;
            using (var SR = new StreamReader(fileName))
            {
                tLine = SR.ReadLine().Split('\t');
                if (tLine.Length < 4) return null;
                while (!SR.EndOfStream)
                {
                    tLine = SR.ReadLine().Split('\t');
                    table1.Add(tLine);
                }
                SR.Close();
            }
            return table1;
        }

        private string AppendAnnotationsFromFile(List<string[]> table1)
        {
            //We are assuming the following column order, the headers have already been removed
            int iPlate = 0;
            int iRaftID = 1;
            int iAName = 2;
            int iAVal = 3;
            int iWell = 4;
            int iFOV = 5;
            string response;
            Debug.Print(ParentCalForm.ImageChk_List.Count.ToString());
            var UpdateTrack = new Dictionary<string, int>();
            var ICPL = ImageCheck_PointList.FromRafts(ParentCalForm.IC_Folder, table1.Select(x => x[1]));
            foreach (string[] Row in table1)
            {
                response = ParentCalForm.AppendImageCheckPoint(Row[iPlate], Row.Length > iWell ? Row[iWell] : "", Row.Length > iFOV ? Row[iFOV] : "", Row[iRaftID], Row[iAName], Row[iAVal], ICPL);
                if (!UpdateTrack.ContainsKey(response)) UpdateTrack.Add(response, 0);
                UpdateTrack[response]++;
            }
            return string.Join("\r\n", UpdateTrack.Select(x => x.Key + "\t" + x.Value));
        }


        #endregion

        #region Crop Around Objects - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        private void btn_ExportCropRegions_Objects_Click(object sender, EventArgs e)
        {
            //for now just do this through segmentation

        }
        #endregion

        private void btn_ExportStitchFOV(object sender, EventArgs e)
        {
            //Josh M's new function for Purva 12/16/2021
            RaftImage_Export_Settings RIES = RaftImage_Export_Settings.Load();
            foreach (var well in ParentCalForm.IC_Folder.XDCE.Wells.Values)
            {
                ParentCalForm.WellLabel = well.NameAtLevel;
                //Set Currentwell to this well
                for (int FOV = well.FOV_Min; FOV < well.FOV_Max; FOV += 4)
                {
                    //set current field to this field
                    ParentCalForm.CurrentFOV = FOV;
                    ParentCalForm.UpdateAll();
                    string Exported = ParentCalForm.ExportStitchCurrentImageSet(
                        RIES.ExportFolderBase, ParentCalForm.IC_Folder.PlateID,
                        Path.GetFileNameWithoutExtension(ParentCalForm.txBx_ImageName.Text), RIES.ImageExtension, RIES.ResizeMultiplier, (int)RIES.CropToMaxSize);

                    txBx_Update.Text = Exported + "\r\n" + txBx_Update.Text;
                    Application.DoEvents();
                }
            }
            txBx_Update.Text = "Done exporting";
        }
    }
}

public static class ExportHelper
{
    public delegate int MYTestDel(int x, int y);

    public static void AccessImages_Parallel(RaftImage_Export_Settings Settings, wvDisplayParams WVParams, INCELL_WV_Notes WVNotes, ImageCheck_PointList PointList, INCELL_Folder ICFolder, MYTestDel Del, BackgroundWorker BW)
    {
        //Raft List, Annotated Rafts, All Rafts, Objects, Fields

        //Assumes you already setup Pointlist with ALL Rafts, or a designation that you want all cells to be analyzed here
        foreach (var Well_Field in PointList.WellFields)
        {
            AccessImages_Internal(Well_Field, Settings, WVParams, WVNotes, PointList, ICFolder, Del, BW);
        }
    }

    public static void AccessImages_Internal(string Well_Field, RaftImage_Export_Settings Settings, wvDisplayParams WVParams, INCELL_WV_Notes WVNotes, ImageCheck_PointList PointList, INCELL_Folder ICFolder, MYTestDel Del, BackgroundWorker BW)
    {

    }
}

public class RaftImage_Export_Settings
{
    public bool FileName_ByAnnotation { get; set; }
    public bool FolderName_ByAnnotation { get; set; }
    public bool Include_Fiducial_Rafts { get; set; }
    public string ExportFolderBase { get; set; }
    public bool Resize_And_MLExport { get; set; }
    public float Min_AspectRatio_Keep { get; set; }
    public int PixelList_ExportSize { get; set; }//Makes a square image that gets exported just as pixel data (grayscale, 8 bit), only applies to exported ML
    public int PixelsExpand { get; set; }
    public bool OnlyExportSquare { get; set; }
    public bool OnlyExportFullSize { get; set; }
    public float ResizeMultiplier { get; set; }
    public float CropToMaxSize { get; set; }
    public string ImageExtension { get; set; }
    public bool Export_4Rotations { get; set; }
    public RaftImage_Export_Settings()
    {

    }

    public static string DefaultPath = Path.Combine(Path.GetTempPath(), "FIVTools_RaftExport_Settings.xml");

    public static RaftImage_Export_Settings Load()
    {
        return Load(DefaultPath);
    }

    public static RaftImage_Export_Settings Load(string Path)
    {
        if (!File.Exists(Path)) goto JUMPHEREISSUE;
        try
        {
            using (var stream = File.OpenRead(Path))
            {
                var serializer = new XmlSerializer(typeof(RaftImage_Export_Settings));
                return (RaftImage_Export_Settings)serializer.Deserialize(stream);
            }
        }
        catch
        {

        }
    JUMPHEREISSUE:
        RaftImage_Export_Settings P = Defaults();
        P.Save();
        return P;
    }

    public static RaftImage_Export_Settings Defaults()
    {
        RaftImage_Export_Settings P = new RaftImage_Export_Settings();
        P.ExportFolderBase = @"c:\temp\RaftExport\";
        P.ImageExtension = "bmp";
        P.FileName_ByAnnotation = false;
        P.FolderName_ByAnnotation = true;
        P.Resize_And_MLExport = false;
        P.Include_Fiducial_Rafts = false;
        P.Min_AspectRatio_Keep = 0.75F;
        P.PixelsExpand = 0;
        P.OnlyExportSquare = true;
        P.OnlyExportFullSize = true;
        P.PixelList_ExportSize = 52;
        P.ResizeMultiplier = 1; // Settings.Resize_And_MLExport ? 0.84F : 1F;
        P.CropToMaxSize = 500; // < 1 means don't do this, never expand
        P.Export_4Rotations = false;
        return P;
    }

    public void Save()
    {
        Save(DefaultPath);
    }

    public void Save(string Path)
    {
        using (var writer = new StreamWriter(Path))
        {
            var serializer = new XmlSerializer(this.GetType());
            serializer.Serialize(writer, this);
            writer.Flush();
        }
    }

    internal void SaveCurrent(INCELL_Folder iCFolder, wvDisplayParams WVParams, string Path)
    {
        using (var writer = new StreamWriter(Path))
        {
            writer.WriteLine(iCFolder.PlateID);

            var serializer = new XmlSerializer(this.GetType());
            serializer.Serialize(writer, this);

            var X = WVParams;
            serializer = new XmlSerializer(X.GetType());
            serializer.Serialize(writer, X);

            writer.Flush();
        }
    }
}
