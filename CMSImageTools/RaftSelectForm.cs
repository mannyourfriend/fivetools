﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMSImageTools
{
    public partial class RaftSelectForm : Form
    {
        public List<string> Rafts;

        public RaftSelectForm()
        {
            InitializeComponent();
        }

        public void SetRafts(IEnumerable<string> Rafts) {
            if (Rafts == null) return;
            raftBox.Text = String.Join(Environment.NewLine, Rafts);
            return;
        }

        private void ClickCancel(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ClickOK(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            string[] splitChars = { Environment.NewLine, ",", "\t", " "};
            this.Rafts = raftBox.Text.Trim().ToUpper().Split(splitChars, StringSplitOptions.RemoveEmptyEntries).ToList();
            this.Close();
        }
    }
}
