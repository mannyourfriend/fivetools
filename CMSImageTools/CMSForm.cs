﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMSImageTools
{
    public partial class CMSForm : Form
    {
        private enum StitchOpers { StitchNorm, CustomStitch, RaftImages};
        private List<Button> StitchButtons;
        private List<BackgroundWorker> Workers;
        public string RootFolderName;
        public List<string> Projects;
        public Dictionary<string, List<string>> ProjectScans;
        public bool Loaded = false;
        public List<string> Rafts;

        public CMSForm()
        {
            InitializeComponent();
            StitchButtons = new List<Button> { btnAllRafts, btnStitcher, btnStitcherCustom };
            Workers = new List<BackgroundWorker> { workerSQLite, workerStitcher };
        }

        private void LoadDB (object sender, EventArgs e)
        {
            if (inputCMSRootFolder.Text == RootFolderName && Loaded)
            {
                UpdateForm("Already Loaded");
                return;
            }
            string dbfile = Path.Combine(inputCMSRootFolder.Text, ".cmsdb");
            if (!File.Exists(dbfile))
            {
                LoadCMSData();
                return;
            }
            (Projects, ProjectScans) = FormTools.LoadDB(dbfile);
            projectBox.Items.Clear();
            projectBox.Items.AddRange(Projects.Cast<object>().ToArray());
            Loaded = true;
            RootFolderName = inputCMSRootFolder.Text;
            UpdateForm($"Loaded from {Path.GetFileName(dbfile)}");
        }

        private void RefreshDB(object sender, EventArgs e)
        {
            LoadCMSData();
            UpdateForm("Refreshed!");
        }

        public static Regex ScanNameMatch = new Regex(@"^\d{6}-\d{4}$");
        private void LoadCMSData()
        {
            DirectoryInfo rootFolder = new DirectoryInfo(inputCMSRootFolder.Text);
            if (!rootFolder.Exists)
            {
                UpdateForm("Invalid folder");
                return;
            }
            RootFolderName = inputCMSRootFolder.Text;
            List<DirectoryInfo> folders = rootFolder.GetDirectories().Where(d => d.GetFiles().Any(f => f.Name.EndsWith(".sqlite"))).ToList();
            Projects = folders.Select(f => f.Name).ToList();
            ProjectScans = new Dictionary<string, List<string>>();
            foreach(var folder in folders)
            {
                ProjectScans.Add(folder.Name, folder.GetDirectories()?.Select(d => d.Name).Where(d => ScanNameMatch.IsMatch(d))?.ToList());
            }
            FormTools.SaveDB(RootFolderName, Projects, ProjectScans);
            projectBox.Items.Clear();
            projectBox.Items.AddRange(Projects.Cast<object>().ToArray());
            Loaded = true;
        }

        private void UpdateForm(string v, bool clear = false)
        {
            if (clear) updateBox.ResetText();
            updateBox.Text += v + "\r\n";
        }

        private void GetScans(object sender, EventArgs e)
        {
            btnPickedRafts.Enabled = !workerSQLite.IsBusy;
            foreach (var btn in StitchButtons) btn.Enabled = false;
            scanBox.Items.Clear();
            scanBox.Items.AddRange(ProjectScans[projectBox.SelectedItem.ToString()].Cast<object>().ToArray());
        }

        private void projectBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string project = projectBox.SelectedItem?.ToString();
            if (project != null) Process.Start(Path.Combine(RootFolderName, project));
        }

        private void SelectScan(object sender, EventArgs e)
        {
            bool selected = scanBox.SelectedItems.Count > 0 && !workerStitcher.IsBusy;
            if (selected)
            {
                foreach (var btn in StitchButtons) btn.Enabled = true;
            }
        }

        private void scanBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string project = projectBox.SelectedItem?.ToString();
            string scan = scanBox.SelectedItem?.ToString();
            if (project != null && scan != null) Process.Start(Path.Combine(RootFolderName, project, scan));
        }

        private void clickStitcher(object sender, EventArgs e)
        {
            foreach (var btn in StitchButtons) btn.Enabled = false;
            string folderPath = Path.Combine(RootFolderName, projectBox.SelectedItem.ToString(), scanBox.SelectedItem.ToString());
            string outPath = OutFolder.Text.Trim().Length == 0 ? null : OutFolder.Text;
            var arguments = new List<object>() {StitchOpers.StitchNorm, folderPath, outPath, ExtensionImage.Text};
            workerStitcher.RunWorkerAsync(arguments);
        }

        
        private void clickStitcherCustom(object sender, EventArgs e)
        {
            if (Rafts.Count == 0) {
                UpdateForm("No rafts were given");
                return;
            }
            foreach (var raft in Rafts)
            {
                if (!Tools.RaftMatch.IsMatch(raft))
                {
                    UpdateForm($"{raft} is not a valid raft id");
                    return;
                }
            }
            foreach (var btn in StitchButtons) btn.Enabled = false;
            string folderPath = Path.Combine(RootFolderName, projectBox.SelectedItem.ToString(), scanBox.SelectedItem.ToString());
            string outPath = OutFolder.Text.Trim().Length == 0 ? null : OutFolder.Text;
            var arguments = new List<object>() { StitchOpers.CustomStitch, folderPath, outPath, ExtensionImage.Text,  Rafts };
            workerStitcher.RunWorkerAsync(arguments);
        }

        private void workerStitcher_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            var arguments = e.Argument as List<object>;
            StitchOpers operation = (StitchOpers)arguments[0];
            string folder = (string)arguments[1];
            string outPath = (string)arguments[2];
            string ext = (string)arguments[3];
            CMSStitcher stitcher;
            if (operation == StitchOpers.CustomStitch)
            {
                var RW = (List<string>)arguments[4];
                stitcher = new CMSStitcher(folder, outPath, raftsWanted: RW, croppedBuffer: 1, bw: worker);
            }
            else if (operation == StitchOpers.StitchNorm)
            {
                stitcher = new CMSStitcher(folder, outPath, bw: worker);
            }
            else if (operation == StitchOpers.RaftImages)
            {
                stitcher = new CMSStitcher(folder, outPath, useAll: true, bw: worker);
            }
            else
            {
                throw new ArgumentException("No stitch operation provided");
            }
            stitcher.GenerateAllMaps();
            if (operation == StitchOpers.RaftImages)
            {
                stitcher.SeparateRaftImages(true,ext);
            }
            else
            {
                stitcher.Stitch();
            }
            if (worker.CancellationPending) e.Cancel = true;
        }

        private void workerStitcher_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            bool selected = scanBox.SelectedItems.Count > 0;
            foreach (var btn in StitchButtons) btn.Enabled = selected;
            if (e.Error != null) UpdateForm(e.Error.Message);
            else if (!e.Cancelled) UpdateForm("Finised Stitching!");
        }

        private void clickSQLite(object sender, EventArgs e)
        {
            btnPickedRafts.Enabled = false;
            string sqlitePath = (new DirectoryInfo(Path.Combine(RootFolderName, projectBox.SelectedItem.ToString()))).GetFiles().Select(f => f.FullName).Where(f => f.EndsWith(".sqlite")).ToArray()[0];
            string outPath = OutFolder.Text.Trim().Length == 0 ? (new FileInfo(sqlitePath).DirectoryName) : OutFolder.Text;
            var arguments = new List<object>() { sqlitePath, outPath };
            workerSQLite.RunWorkerAsync(arguments);
        }

        private void workerSQLite_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            var arguments = e.Argument as List<object>;
            string SQLitePath = (string)arguments[0];
            string outPath = (string)arguments[1];
            CMSSQLite.GetImages(SQLitePath, worker);
            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                CMSSQLite.SplitImages(Path.Combine((new FileInfo(SQLitePath)).DirectoryName, CMSSQLite.SaveFolder));
            }
        }

        private void workerSQLite_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            btnPickedRafts.Enabled = true;
            if (e.Error != null) UpdateForm(e.Error.Message);
        }

        private void clickLoadRafts(object sender, EventArgs e)
        {
            using (var form = new RaftSelectForm())
            {
                form.SetRafts(this.Rafts);
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Rafts = form.Rafts;
                }
            }
        }

        private void clickAllRafts(object sender, EventArgs e)
        {
            foreach (var btn in StitchButtons) btn.Enabled = false;
            string folderPath = Path.Combine(RootFolderName, projectBox.SelectedItem.ToString(), scanBox.SelectedItem.ToString());
            string outPath = OutFolder.Text.Trim().Length == 0 ? null : OutFolder.Text;
            var arguments = new List<object>() { StitchOpers.RaftImages, folderPath, outPath, ExtensionImage.Text };
            workerStitcher.RunWorkerAsync(arguments);
        }

        private void clickStop(object sender, EventArgs e)
        {
            btnStop.Enabled = false;
            System.Threading.Thread.Sleep(300);
            foreach (var worker in Workers) worker.CancelAsync();
            btnStop.Enabled = true;
            UpdateForm("Stop! In the name of love...");
        }
    }
}
