﻿namespace FIVE_Tools_Main
{
    partial class NGS_LibraryAligner_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NGS_LibraryAligner_Form));
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.txBx_DownloadLink = new System.Windows.Forms.TextBox();
            this.txBx_DestFolder = new System.Windows.Forms.TextBox();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.btn_Start = new System.Windows.Forms.Button();
            this.dataGridViewM = new System.Windows.Forms.DataGridView();
            this.bgWrk_LA = new System.ComponentModel.BackgroundWorker();
            this.btn_LoadDefaults = new System.Windows.Forms.Button();
            this.btn_LoadFromFile = new System.Windows.Forms.Button();
            this.btn_SaveParams = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_Download_UnZip = new System.Windows.Forms.Button();
            this.bgWrk_Download = new System.ComponentModel.BackgroundWorker();
            this.btn_LoadRef = new System.Windows.Forms.Button();
            this.btn_LoadFASTQ = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_CheckRep = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.chkBx_MergePools = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txBx_MOI = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txBx_TiterIUoML = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txBx_CellFactor = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewM)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(456, 449);
            this.btn_Cancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(88, 27);
            this.btn_Cancel.TabIndex = 6;
            this.btn_Cancel.Text = "Cancel";
            this.toolTip1.SetToolTip(this.btn_Cancel, "Cancel processing (wait a few minutes after clicking)");
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // txBx_DownloadLink
            // 
            this.txBx_DownloadLink.Location = new System.Drawing.Point(621, 14);
            this.txBx_DownloadLink.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_DownloadLink.Name = "txBx_DownloadLink";
            this.txBx_DownloadLink.Size = new System.Drawing.Size(204, 23);
            this.txBx_DownloadLink.TabIndex = 1;
            this.toolTip1.SetToolTip(this.txBx_DownloadLink, "Paste the link to the Apache file list to automatically download the R1 and R2 fi" +
        "les");
            // 
            // txBx_DestFolder
            // 
            this.txBx_DestFolder.Location = new System.Drawing.Point(873, 14);
            this.txBx_DestFolder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_DestFolder.Name = "txBx_DestFolder";
            this.txBx_DestFolder.Size = new System.Drawing.Size(165, 23);
            this.txBx_DestFolder.TabIndex = 2;
            this.toolTip1.SetToolTip(this.txBx_DestFolder, "Specifies the location to download the files");
            this.txBx_DestFolder.TextChanged += new System.EventHandler(this.txBx_DestFolder_TextChanged);
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(581, 44);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(572, 444);
            this.txBx_Update.TabIndex = 3;
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(456, 416);
            this.btn_Start.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(88, 27);
            this.btn_Start.TabIndex = 5;
            this.btn_Start.Text = "Start";
            this.toolTip1.SetToolTip(this.btn_Start, "Start processing FastQ files");
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // dataGridViewM
            // 
            this.dataGridViewM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewM.Location = new System.Drawing.Point(14, 14);
            this.dataGridViewM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridViewM.Name = "dataGridViewM";
            this.dataGridViewM.Size = new System.Drawing.Size(422, 530);
            this.dataGridViewM.TabIndex = 5;
            // 
            // bgWrk_LA
            // 
            this.bgWrk_LA.WorkerReportsProgress = true;
            this.bgWrk_LA.WorkerSupportsCancellation = true;
            this.bgWrk_LA.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk_LA_DoWork);
            this.bgWrk_LA.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWrk_LA_ProgressChanged);
            this.bgWrk_LA.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk_LA_RunWorkerCompleted);
            // 
            // btn_LoadDefaults
            // 
            this.btn_LoadDefaults.Location = new System.Drawing.Point(454, 112);
            this.btn_LoadDefaults.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_LoadDefaults.Name = "btn_LoadDefaults";
            this.btn_LoadDefaults.Size = new System.Drawing.Size(88, 27);
            this.btn_LoadDefaults.TabIndex = 6;
            this.btn_LoadDefaults.Text = "Defaults";
            this.toolTip1.SetToolTip(this.btn_LoadDefaults, "Loads the default settings. This is a good place to start.");
            this.btn_LoadDefaults.UseVisualStyleBackColor = true;
            this.btn_LoadDefaults.Click += new System.EventHandler(this.btn_LoadDefaults_Click);
            // 
            // btn_LoadFromFile
            // 
            this.btn_LoadFromFile.Location = new System.Drawing.Point(454, 145);
            this.btn_LoadFromFile.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_LoadFromFile.Name = "btn_LoadFromFile";
            this.btn_LoadFromFile.Size = new System.Drawing.Size(88, 27);
            this.btn_LoadFromFile.TabIndex = 7;
            this.btn_LoadFromFile.Text = "Load ..";
            this.toolTip1.SetToolTip(this.btn_LoadFromFile, "Loads previousy saved settings.");
            this.btn_LoadFromFile.UseVisualStyleBackColor = true;
            this.btn_LoadFromFile.Click += new System.EventHandler(this.btn_LoadFromFile_Click);
            // 
            // btn_SaveParams
            // 
            this.btn_SaveParams.Location = new System.Drawing.Point(454, 179);
            this.btn_SaveParams.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_SaveParams.Name = "btn_SaveParams";
            this.btn_SaveParams.Size = new System.Drawing.Size(88, 27);
            this.btn_SaveParams.TabIndex = 8;
            this.btn_SaveParams.Text = "Save";
            this.toolTip1.SetToolTip(this.btn_SaveParams, "Save the current settings.");
            this.btn_SaveParams.UseVisualStyleBackColor = true;
            this.btn_SaveParams.Click += new System.EventHandler(this.btn_SaveParams_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(459, 93);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "XML Settings:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(555, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "HTCF Link";
            this.toolTip1.SetToolTip(this.label2, "Paste the link to the Apache file list to automatically download the R1 and R2 fi" +
        "les");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(838, 18);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "Dest";
            this.toolTip1.SetToolTip(this.label3, "Specifies the location to download the files");
            // 
            // btn_Download_UnZip
            // 
            this.btn_Download_UnZip.Location = new System.Drawing.Point(1045, 12);
            this.btn_Download_UnZip.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Download_UnZip.Name = "btn_Download_UnZip";
            this.btn_Download_UnZip.Size = new System.Drawing.Size(108, 27);
            this.btn_Download_UnZip.TabIndex = 12;
            this.btn_Download_UnZip.Text = "Download UZ";
            this.toolTip1.SetToolTip(this.btn_Download_UnZip, "Downloads files from the provided link, then attempts to UnZip them. 7-Zip must b" +
        "e installed for the latter to work.");
            this.btn_Download_UnZip.UseVisualStyleBackColor = true;
            this.btn_Download_UnZip.Click += new System.EventHandler(this.btn_Download_UnZip_Click);
            // 
            // bgWrk_Download
            // 
            this.bgWrk_Download.WorkerReportsProgress = true;
            this.bgWrk_Download.WorkerSupportsCancellation = true;
            this.bgWrk_Download.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk_Download_DoWork);
            this.bgWrk_Download.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWrk_Download_ProgressChanged);
            this.bgWrk_Download.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk_Download_RunWorkerCompleted);
            // 
            // btn_LoadRef
            // 
            this.btn_LoadRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_LoadRef.Location = new System.Drawing.Point(455, 23);
            this.btn_LoadRef.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_LoadRef.Name = "btn_LoadRef";
            this.btn_LoadRef.Size = new System.Drawing.Size(86, 23);
            this.btn_LoadRef.TabIndex = 3;
            this.btn_LoadRef.Text = "browse ref";
            this.toolTip1.SetToolTip(this.btn_LoadRef, resources.GetString("btn_LoadRef.ToolTip"));
            this.btn_LoadRef.UseVisualStyleBackColor = true;
            this.btn_LoadRef.Click += new System.EventHandler(this.btn_LoadRef_Click);
            // 
            // btn_LoadFASTQ
            // 
            this.btn_LoadFASTQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_LoadFASTQ.Location = new System.Drawing.Point(455, 49);
            this.btn_LoadFASTQ.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_LoadFASTQ.Name = "btn_LoadFASTQ";
            this.btn_LoadFASTQ.Size = new System.Drawing.Size(86, 23);
            this.btn_LoadFASTQ.TabIndex = 4;
            this.btn_LoadFASTQ.Text = "browse fa";
            this.toolTip1.SetToolTip(this.btn_LoadFASTQ, "Select any FastQ file and LA will run on all FastQ files in this folder");
            this.btn_LoadFASTQ.UseVisualStyleBackColor = true;
            this.btn_LoadFASTQ.Click += new System.EventHandler(this.btn_LoadFASTQ_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(448, 212);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MaximumSize = new System.Drawing.Size(117, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 156);
            this.label4.TabIndex = 15;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // btn_CheckRep
            // 
            this.btn_CheckRep.Location = new System.Drawing.Point(568, 6);
            this.btn_CheckRep.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_CheckRep.Name = "btn_CheckRep";
            this.btn_CheckRep.Size = new System.Drawing.Size(126, 36);
            this.btn_CheckRep.TabIndex = 24;
            this.btn_CheckRep.Text = "Check Rep";
            this.toolTip1.SetToolTip(this.btn_CheckRep, "After running LA, use this to export standard plots to check the representation");
            this.btn_CheckRep.UseVisualStyleBackColor = true;
            this.btn_CheckRep.Click += new System.EventHandler(this.btn_CheckRep_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(9, 8);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.MaximumSize = new System.Drawing.Size(105, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 32);
            this.label5.TabIndex = 17;
            this.label5.Text = "Post Count Analysis";
            // 
            // chkBx_MergePools
            // 
            this.chkBx_MergePools.AutoSize = true;
            this.chkBx_MergePools.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.chkBx_MergePools.Location = new System.Drawing.Point(495, 16);
            this.chkBx_MergePools.Name = "chkBx_MergePools";
            this.chkBx_MergePools.Size = new System.Drawing.Size(61, 16);
            this.chkBx_MergePools.TabIndex = 23;
            this.chkBx_MergePools.Text = "Merge P";
            this.toolTip1.SetToolTip(this.chkBx_MergePools, "Merge the Subpools and libraries together");
            this.chkBx_MergePools.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(245, 4);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.MaximumSize = new System.Drawing.Size(105, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "MOI";
            this.toolTip1.SetToolTip(this.label6, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4," +
        " 1\".");
            // 
            // txBx_MOI
            // 
            this.txBx_MOI.Location = new System.Drawing.Point(245, 20);
            this.txBx_MOI.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_MOI.Name = "txBx_MOI";
            this.txBx_MOI.Size = new System.Drawing.Size(65, 23);
            this.txBx_MOI.TabIndex = 20;
            this.txBx_MOI.Text = "0.4";
            this.toolTip1.SetToolTip(this.txBx_MOI, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4," +
        " 1\".");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(404, 4);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.MaximumSize = new System.Drawing.Size(105, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Titer (IU/ml)";
            this.toolTip1.SetToolTip(this.label8, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4," +
        " 1\".");
            // 
            // txBx_TiterIUoML
            // 
            this.txBx_TiterIUoML.Location = new System.Drawing.Point(403, 20);
            this.txBx_TiterIUoML.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_TiterIUoML.Name = "txBx_TiterIUoML";
            this.txBx_TiterIUoML.Size = new System.Drawing.Size(83, 23);
            this.txBx_TiterIUoML.TabIndex = 22;
            this.txBx_TiterIUoML.Text = "4.5E7";
            this.toolTip1.SetToolTip(this.txBx_TiterIUoML, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4," +
        " 1\".");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(316, 4);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.MaximumSize = new System.Drawing.Size(105, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "mF (Cell Factor)";
            this.toolTip1.SetToolTip(this.label9, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4," +
        " 1\".");
            // 
            // txBx_CellFactor
            // 
            this.txBx_CellFactor.Location = new System.Drawing.Point(315, 20);
            this.txBx_CellFactor.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_CellFactor.Name = "txBx_CellFactor";
            this.txBx_CellFactor.Size = new System.Drawing.Size(83, 23);
            this.txBx_CellFactor.TabIndex = 21;
            this.txBx_CellFactor.Text = "2";
            this.toolTip1.SetToolTip(this.txBx_CellFactor, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4," +
        " 1\".");
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txBx_CellFactor);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txBx_TiterIUoML);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txBx_MOI);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.btn_CheckRep);
            this.panel1.Controls.Add(this.chkBx_MergePools);
            this.panel1.Location = new System.Drawing.Point(444, 494);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(709, 50);
            this.panel1.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(460, 398);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 15);
            this.label7.TabIndex = 20;
            this.label7.Text = "Get Counts:";
            // 
            // NGS_LibraryAligner_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 549);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_LoadFASTQ);
            this.Controls.Add(this.btn_LoadRef);
            this.Controls.Add(this.btn_Download_UnZip);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_SaveParams);
            this.Controls.Add(this.btn_LoadFromFile);
            this.Controls.Add(this.btn_LoadDefaults);
            this.Controls.Add(this.dataGridViewM);
            this.Controls.Add(this.btn_Start);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.txBx_DestFolder);
            this.Controls.Add(this.txBx_DownloadLink);
            this.Controls.Add(this.btn_Cancel);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "NGS_LibraryAligner_Form";
            this.Text = "Library Aligner";
            this.Load += new System.EventHandler(this.NGS_LibraryAligner_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewM)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.TextBox txBx_DownloadLink;
        private System.Windows.Forms.TextBox txBx_DestFolder;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.DataGridView dataGridViewM;
        private System.ComponentModel.BackgroundWorker bgWrk_LA;
        private System.Windows.Forms.Button btn_LoadDefaults;
        private System.Windows.Forms.Button btn_LoadFromFile;
        private System.Windows.Forms.Button btn_SaveParams;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_Download_UnZip;
        private System.ComponentModel.BackgroundWorker bgWrk_Download;
        private System.Windows.Forms.Button btn_LoadRef;
        private System.Windows.Forms.Button btn_LoadFASTQ;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_CheckRep;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox chkBx_MergePools;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txBx_MOI;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txBx_CellFactor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txBx_TiterIUoML;
    }
}