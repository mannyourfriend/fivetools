﻿using FIVE.FOVtoRaftID;
using FIVE_Tools_Main.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE_Tools_Main.Forms
{
    public partial class Form_Layouts : Form
    {
        FL_FilledPlates FP;
        FL_Table FL;

        public Form_Layouts()
        {
            InitializeComponent();
            FL_Table FL = MakeFakeTable();
            Data_to_Grid(FL);
        }

        public FL_Settings SaveSettings()
        {
            FL_Settings S = new FL_Settings();
            S.ExpName = txBx_ExpName.Text;
            S.DestPlates = int.Parse(txBx_DestinationPlateCount.Text);
            S.DestStartPlate = int.Parse(txBx_DestinationPlateStart.Text);
            S.Row_First = int.Parse(txBx_RowFirst.Text);
            S.Row_Last = int.Parse(txBx_RowLast.Text);
            S.Col_First = int.Parse(txBx_ColFirst.Text);
            S.Col_Last = int.Parse(txBx_ColLast.Text);
            S.RandEmpties = int.Parse(txBx_RanEmpty.Text);
            S.Randomize_Ordered = radioButton_Randomize.Checked;
            S.WellsPerAspirate = int.Parse(txBx_WellsPerAspirate.Text);
            S.DispenseVolume = float.Parse(txBx_DispenseVolume.Text);
            S.DispenseVolRand = float.Parse(txBx_DispenseVolRand.Text);
            S.ActiveTips = int.Parse(txBx_ActiveTips.Text);
            S.AbPlateUse = chkBx_AddAntibodyPlate.Checked;
            S.AbPlate_Reps = int.Parse(txBx_AbPlate_Reps.Text);
            S.AbPlate_StartCol = int.Parse(txBx_AbPlate_StartCol.Text);
            S.CellsPerWell = int.Parse(txBx_CellsPerWell.Text);

            S.FF = double.Parse(txBx_FF.Text);
            S.MinVolSource_uL = double.Parse(txBx_MinSourceVolumeUL.Text);
            return S;
        }

        public FL_Table Grid_to_Data(DataGridView DGV)
        {
            FL_Table FL = new FL_Table();
            foreach (DataGridViewRow row in DGV.Rows)
            {
                FL_Entry FLE = new FL_Entry();
                if (row.Cells[0].Value == null) continue; //Skip empty ones
                for (int i = 0; i < DGV.Columns.Count; i++)
                    FLE.SetValue(DGV.Columns[i].Name, row.Cells[i].Value);
                FL.Add(FLE);
            }
            return FL;
        }

        public void Data_to_Grid(FL_Table FL)
        {
            //Setup Columns
            foreach (var col in FL_Table.Columns)
                dataGridViewM.Columns.Add(col, col);

            //Setup Rows
            foreach (var item in FL)
            {
                string[] St = new string[FL_Table.Columns.Length];
                for (int i = 0; i < FL_Table.Columns.Length; i++) St[i] = item.GetValue(FL_Table.Columns[i]);
                dataGridViewM.Rows.Add(St);
            }
        }

        public void ClearGrid()
        {
            dataGridViewM.Columns.Clear();
            dataGridViewM.Rows.Clear();
        }

        private static FL_Table MakeFakeTable()
        {
            FL_Table FLt = new FL_Table();
            FLt.Add(new FL_Entry() { Name = "G1", Well = "A1", RelativeRepresentation = 1 });
            FLt.Add(new FL_Entry() { Name = "G2", Well = "A2", RelativeRepresentation = 1 });
            FLt.Add(new FL_Entry() { Name = "Mix50", Well = "A3", RelativeRepresentation = 1 });
            FLt.Add(new FL_Entry() { Name = "_Fake", Well = "A4", RelativeRepresentation = 1 });
            return FLt;
        }

        private void label_Paste_Names_Click(object sender, EventArgs e)
        {
            //Check the clipboard and paste stuff in . . 
            string clip = Clipboard.GetText();
            string[] arr = clip.Split('\r');
            var FLt = new FL_Table();
            int Row = 1; int Col = 0;
            PH_Well Well;
            for (int i = 0; i < arr.Length; i++)
            {
                if (++Col > 4) { Row++; Col = 1; }
                Well = new PH_Well(Row, Col);
                FLt.Add(new FL_Entry() { Name = arr[i].Trim(), Well = Well.Well, RelativeRepresentation = 1 });
            }
            ClearGrid();
            Data_to_Grid(FLt);
        }

        private void btn_FillPlates_Click(object sender, EventArgs e)
        {
            FL_Settings St = SaveSettings();
            FL = Grid_to_Data(dataGridViewM);

            FP = FL.FillPlates(St);

            //Draw the examples
            FL_Entry.ColorsUsed = new HashSet<Color>(); //Resets the colors
            pictureBox_Legend.Image = FP.MakeLegend(pictureBox_Legend.Width, pictureBox_Legend.Height);
            pictureBox_Plate1.Image = FP.MakePlateImage(0, pictureBox_Plate1.Width, pictureBox_Plate1.Height);
            pictureBox_Plate2.Image = null;
            pictureBox_Plate3.Image = null;
            if (St.DestPlates > 1)
                pictureBox_Plate2.Image = FP.MakePlateImage(1, pictureBox_Plate2.Width, pictureBox_Plate2.Height);
            if (St.DestPlates > 2)
                pictureBox_Plate3.Image = FP.MakePlateImage(2, pictureBox_Plate3.Width, pictureBox_Plate3.Height);

            txBx_Update.Text = FL.Stats;
        }

        private void btn_ExportThis_Click(object sender, EventArgs e)
        {
            if (FP == null) return;
            //Make Folder
            var DI = new DirectoryInfo(txBx_ExportFolder.Text);
            string ExpName = txBx_ExpName.Text.Trim().ToUpper();
            if (DI.Name.ToUpper() != ExpName) if (DI.Name.ToUpper() != "4 MAPPING") DI = new DirectoryInfo(Path.Combine(DI.FullName, ExpName));
            string PlateMapName = Path.Combine(DI.FullName, FP.Settings.ExpName + "_PlateMap.txt");
            if (!DI.Exists) DI.Create();
            else if (File.Exists(PlateMapName))
            {  //Check if there are files here already
                var Ret = MessageBox.Show("Already found a layout, overwrite it?", "Layout Tool for Plating", MessageBoxButtons.YesNo);
                if (Ret == DialogResult.No) return;
            }

            //Export Images
            ExportPlateImages(DI, FP);
            //Export Plate Map
            File.WriteAllText(PlateMapName, FP.PlateMap());
            //Export Plating Setup
            File.WriteAllText(Path.Combine(DI.FullName, FP.Settings.ExpName + "_PlatingSetup.txt"), FP.PlatingSetup());
            //Export Biomek Script
            File.WriteAllText(Path.Combine(DI.FullName, FP.Settings.ExpName + "_script.vb"), FL.Script(FP.Settings));
            txBx_Update.Text = "Exported.";

            var startInfo = new ProcessStartInfo { Arguments = DI.FullName, FileName = "explorer.exe" };
            Process.Start(startInfo);
        }

        private void ExportPlateImages(DirectoryInfo DI, FL_FilledPlates FPa)
        {
            var Imgs = new List<Image>();
            Imgs.Add(pictureBox_Plate1.Image);
            if (pictureBox_Plate2.Image != null) Imgs.Add(pictureBox_Plate2.Image);
            if (pictureBox_Plate3.Image != null) Imgs.Add(pictureBox_Plate3.Image);

            for (int i = 0; i < Imgs.Count; i++)
                Imgs[i].Save(Path.Combine(DI.FullName, FP.Settings.FullPlateName(i) + ".jpg"));

            //Now draw the master sheet

            var BMP = new Bitmap(800, 1000);
            int PlateRows = 3; int PlateCols = 4;
            var WellBrush = Brushes.Bisque;
            var tPen = Pens.Black;
            var tFont = new Font("Arial", 22);
            var wFont = new Font("Arial", 14);
            float x, y, plateWidth, wellWidth, plateHeight;
            float platelx, plately;
            FL_Entry FLE;

            platelx = BMP.Width / 16;
            plately = BMP.Height / 10;
            plateWidth = BMP.Width * 0.57f;
            plateHeight = plateWidth * PlateRows / PlateCols;
            wellWidth = (plateWidth / PlateCols);

            using (var g = Graphics.FromImage(BMP))
            {
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.Clear(Color.White);
                g.DrawRectangle(tPen, platelx, plately, plateWidth, plateHeight);
                for (int r = 0; r < PlateRows; r++)
                {
                    y = plately + (r * wellWidth);
                    for (int c = 0; c < PlateCols; c++)
                    {
                        string tWell = new PH_Well(r + 1, c + 1).Well;
                        var tObj = FPa.Table.List_Full.Where(x => x.Well == tWell);
                        if (tObj.Count() < 1) FLE = null; else FLE = (FL_Entry)tObj.First();
                        x = platelx + (c * wellWidth);
                        if (FLE != null)
                        {
                            g.FillEllipse(new SolidBrush(LightenFromBrush(FLE.Brush, 3)), x + 3, y + 3, wellWidth - 6, wellWidth - 6);
                            g.FillEllipse(FLE.Brush, x + wellWidth / 2.5f, y + 4, wellWidth / 5, wellWidth / 5);
                        }
                        g.DrawEllipse(Pens.Black, x + 3, y + 3, wellWidth - 6, wellWidth - 6);
                        if (FLE != null)
                            g.DrawString(FLE.Name, tFont, Brushes.Black, x + wellWidth / 15, y + wellWidth / 3.6f);
                        g.DrawString(tWell, wFont, Brushes.Red, x + wellWidth / 2.7f, y + wellWidth / 1.5f);
                    }
                }
                var sB = new StringBuilder(); string d = "  ";
                var Settings = FPa.Settings;
                sB.Append("Date:" + d + Settings.TimeStampRand.ToString("d") + d + d + "uL/Well:" + d + Settings.DispenseVolume + d + d + "Min uL:" + d + Settings.MinVolSource_uL + "\r\n");
                sB.Append("Exp:" + d + Settings.ExpName + d + d + "FF:" + d + Settings.FF + d + "" + d + "Max uL:" + d + Settings.MaxVolSource_uL + "\r\n");
                sB.Append("Plates:" + d + Settings.DestPlates + d + "Signature:" + Settings.Signature + "\r\n");
                y = plately * 1.5f + plateHeight;
                g.DrawString(sB.ToString(), wFont, Brushes.Navy, platelx, y);

                y += 120;
                float destWidth = ((BMP.Width - platelx * 2) / Imgs.Count) - platelx;
                float destHeight = (float)Imgs[0].Height * destWidth / Imgs[0].Width;
                for (int i = 0; i < Imgs.Count; i++)
                {
                    x = platelx + (i * (destWidth + platelx));
                    g.DrawString((FPa.Settings.DestStartPlate + i).ToString(), wFont, Brushes.DarkGreen, x, y - 20);
                    g.DrawImage(Imgs[i], new RectangleF(x, y, destWidth, destHeight));
                }
            }
            BMP.Save(Path.Combine(DI.FullName, FP.Settings.ExpName + "_Source.jpg"));
        }

        public static Color LightenFromBrush(Brush BToLighten, float Mult)
        {
            int r = (int)(((SolidBrush)BToLighten).Color.R * Mult);
            int g = (int)(((SolidBrush)BToLighten).Color.G * Mult);
            int b = (int)(((SolidBrush)BToLighten).Color.B * Mult);
            r = Math.Min(235, r);
            g = Math.Min(235, g);
            b = Math.Min(235, b);
            Color C = Color.FromArgb(r, g, b);
            return C;
        }

        private void txBx_ExpName_Leave(object sender, EventArgs e)
        {
            if (txBx_ExpName.Text.StartsWith("FIV"))
            {
                if (txBx_ExpName.Text == "FIV999") return;
                txBx_ExportFolder.Text = @"R:\FIVE\Exp\" + txBx_ExpName.Text + @"\4 Mapping\";
            }
        }

        private void radioButton_Randomize_CheckedChanged(object sender, EventArgs e)
        {
            CheckEnableds();
        }

        private void radioButton_Ordered_CheckedChanged(object sender, EventArgs e)
        {
            CheckEnableds();
        }

        private void CheckEnableds()
        {
            //This was before I fixed that feature . .
            //txBx_ActiveTips.Enabled = !radioButton_Ordered.Checked;
            //if (radioButton_Ordered.Checked) txBx_ActiveTips.Text = "1";
        }

        private void Form_Layouts_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void lbl_96Mote_Click(object sender, EventArgs e)
        {
            txBx_ColFirst.Text = txBx_RowFirst.Text = "2";
            txBx_ColLast.Text = "11";
            txBx_RowLast.Text = "7";
        }

        private void lbl_96Full_Click(object sender, EventArgs e)
        {
            txBx_ColFirst.Text = txBx_RowFirst.Text = "1";
            txBx_ColLast.Text = "12";
            txBx_RowLast.Text = "8";
        }

        private void lbl_384Full_Click(object sender, EventArgs e)
        {
            txBx_ColFirst.Text = txBx_RowFirst.Text = "1";
            txBx_ColLast.Text = "24";
            txBx_RowLast.Text = "16";
        }

        private void chkBx_AddAntibodyPlate_CheckedChanged(object sender, EventArgs e)
        {
            txBx_AbPlate_Reps.Enabled = txBx_AbPlate_StartCol.Enabled = chkBx_AddAntibodyPlate.Checked;
        }
    }

    public class FL_Settings
    {
        public string ExpName;
        public DateTime TimeStampRand;
        public string Signature;
        public int Row_First;
        public int Row_Last;
        public int Col_First;
        public int Col_Last;
        public int DestStartPlate;
        public int DestPlates;
        public int RandEmpties;
        public bool Randomize_Ordered;
        public int WellsPerAspirate;
        public float DispenseVolume;
        public float DispenseVolRand;
        public int ActiveTips;
        public double FF;
        public double MinVolSource_uL;
        public double MaxVolSource_uL = 6250;
        public bool AbPlateUse;
        public int AbPlate_Reps;
        public int AbPlate_StartCol;
        public int CellsPerWell;
        public bool ConvertScriptSource_12to96 = true;

        public int Row_Count => 1 + Row_Last - Row_First;
        public int Col_Count => 1 + Col_Last - Col_First;

        public int Wells_Per_Plate => Row_Count * Col_Count;
        public int Wells_Total => Wells_Per_Plate * DestPlates;

        public string FullPlateName(int index0) { return ExpName + "P" + (DestStartPlate + index0); }
    }

    public class FL_FilledPlates
    {
        public Dictionary<Tuple<int, int, int>, FL_Dest> FillHash;
        public FL_Settings Settings;
        public FL_Table Table;

        public int TotalSteps => FillHash.Count;

        public FL_FilledPlates(FL_Settings settings = null, FL_Table table = null)
        {
            Settings = settings;
            Table = table;
            FillHash = new Dictionary<Tuple<int, int, int>, FL_Dest>();
        }

        public string PlatingSetup()
        {
            StringBuilder sB = new StringBuilder();
            char d = '\t';
            string aFF = "$E$2";
            string aVolPWell = "$E$1";
            string aMinVol = "$H$1";
            string aMaxVol = "$H$2";
            sB.Append("Date:" + d + Settings.TimeStampRand.ToString("d") + d + d + "uL/Well:" + d + Settings.DispenseVolume + d + d + "Min uL:" + d + Settings.MinVolSource_uL + "\r\n");
            sB.Append("Exp:" + d + Settings.ExpName + d + d + "FF:" + d + Settings.FF + d + "" + d + "Max uL:" + d + Settings.MaxVolSource_uL + "\r\n");
            sB.Append("Plates:" + d + Settings.DestPlates + d + "\r\n");
            sB.Append(Settings.Signature + d + "" + d + "" + d + "Change Me" + d + d + d + "Enter Count Below" + "\r\n");
            //sB.Append("Condition" + d + "Source Well" + d + "Dest Wells/Plate" + d + "Cells/Well" + d + "Cells Total" + d + "uL Total" + d + "Cells/uL" + d + "uL Media" + d + "uL Cells" + "\r\n"); //Old version with plate multiplier
            sB.Append("Condition" + d + "Source Well" + d + "Dest Wells" + d + "Cells/Well" + d + "Cells Total" + d + "uL Total" + d + "Cells/uL" + d + "uL Media" + d + "uL Cells" + d + "Note" + "\r\n");
            int r = 6;
            foreach (var src in Settings.AbPlateUse ? Table.List_Full : Table.List)
            {
                sB.Append(src.Name + d + src.Well + d + src.DestList.Count + d + (r == 6 ? Settings.CellsPerWell : "=D6") + d + //Cells per well starter
                    ("=" + ("F" + r) + "*" + ("D" + r) + "/" + aVolPWell) + d + //Cells Total
                                                                                //("=Max(" + aMinVol + "," + aFF + "*" + aVolPWell + "*" + ("C" + r)) + ")" + d + //uL Total //Alternative, but probably not correct
                    ("=" + aMinVol + " + (" + aFF + "*" + aVolPWell + "*" + ("C" + r)) + ")" + d + //uL Total
                    "300" + d + ("=F" + r + "-I" + r) + d + //cells/ul and uL Media
                    ("=E" + r + "/G" + r) + d +  //cells to add
                    "=if(" + ("F" + r) + ">" + aMaxVol + ",\"**Over Volume**\",\"\")" + "\r\n"); //Note
                r++;
            }

            return sB.ToString();
        }

        public string PlateMap()
        {
            var sB = new StringBuilder();
            char delim = '\t';
            sB.Append("### " + Settings.ExpName + " " + Settings.TimeStampRand + "  " + Settings.Signature + " ###\r\n");
            sB.Append("Plate" + delim + "PlateIdx" + delim + "Well" + delim + "Well Proper" + delim + "WELL LABEL" + delim + "Well Idx" + delim + "Row" + delim + "Column" + delim + "Name" + delim + "SourceWell" + delim + "Replicate" + delim + "Tip" + delim + "DispenseIdx" + "\r\n");
            string Row; int Col, DispIdx; string Well, WellProper, WellLabel, Plate, PlateIdx;
            var PlateIdxCount = new Dictionary<string, int>();
            var Replicates = new Dictionary<string, int>();
            foreach (var KVP in FillHash)
            {
                PlateIdx = (KVP.Key.Item1 + 1).ToString();
                Plate = Settings.FullPlateName(KVP.Key.Item1);
                Row = Convert.ToChar(KVP.Key.Item2 + 64).ToString();
                Col = KVP.Key.Item3;
                Well = Row + Col;
                WellProper = Row + Col.ToString("00");
                WellLabel = Row + " - " + Col;
                if (!PlateIdxCount.ContainsKey(PlateIdx)) PlateIdxCount.Add(PlateIdx, 0);
                PlateIdxCount[PlateIdx]++;
                if (!Replicates.ContainsKey(KVP.Value.Source.Name)) Replicates.Add(KVP.Value.Source.Name, 0);
                Replicates[KVP.Value.Source.Name]++;
                //=MOD(FLOOR((K3-1)/2,1),2)
                DispIdx = (int)(Math.Floor((double)(Replicates[KVP.Value.Source.Name] - 1) / 2) % Settings.WellsPerAspirate);
                sB.Append(Plate + delim + PlateIdx + delim + Well + delim + WellProper + delim +
                    WellLabel + delim + PlateIdxCount[PlateIdx] + delim +
                    Row + delim + Col + delim + KVP.Value.Source.Name + delim + KVP.Value.Source.Well + delim +
                    Replicates[KVP.Value.Source.Name] + delim + KVP.Value.Tip + delim + DispIdx + "\r\n");
            }
            return sB.ToString();
        }

        public Bitmap MakePlateImage(int PlateIndex, int Width, int Height)
        {
            float xf, yf;
            int x, y, w;
            var BMP = new Bitmap(Width, Height);
            w = (int)(Math.Min((float)Width / Settings.Col_Count, (float)Height / Settings.Row_Count) * 0.98);
            using (var g = Graphics.FromImage(BMP))
            {
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                g.Clear(Color.White);
                g.DrawRectangle(Pens.DarkGray, 0, 0, Width, Height);
                for (int xi = Settings.Col_First; xi <= Settings.Col_Last; xi++)
                {
                    for (int yi = Settings.Row_First; yi <= Settings.Row_Last; yi++)
                    {
                        xf = (float)(xi - Settings.Col_First) / Settings.Col_Count; yf = (float)(yi - Settings.Row_First) / Settings.Row_Count;
                        x = (int)(Width * xf); y = (int)(Height * yf);
                        Tuple<int, int, int> Key = new Tuple<int, int, int>(PlateIndex, yi, xi);
                        if (FillHash.ContainsKey(Key))
                            g.FillEllipse(FillHash[Key].Source.Brush, new Rectangle(x, y, w, w));
                    }
                }
            }
            return BMP;
        }

        internal Bitmap MakeLegend(int width, int height)
        {
            var BMP = new Bitmap(width, height);
            int w = (int)(((float)height / Table.Count) * 0.98);
            int count = 0;
            Font drawFont = new Font("Arial", 9, FontStyle.Bold);
            using (Graphics g = Graphics.FromImage(BMP))
            {
                g.FillRectangle(Brushes.White, 0, 0, width, height);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                foreach (var FLE in Table)
                {
                    g.DrawString(FLE.Name, drawFont, FLE.Brush, new Point(10, count * w));
                    count++;
                }
            }
            return BMP;
        }
    }

    public class FL_Table : IEnumerable<FL_Entry>
    {
        public List<FL_Entry> List;      //Mains go here
        public List<FL_Entry> List_Full; //All including antibody plate go here
        public static string[] Columns = typeof(FL_Entry).GetFields().Select(x => x.Name).ToArray();

        public FL_Table()
        {
            List = new List<FL_Entry>(); List_Full = new List<FL_Entry>();
        }

        public int Count => List.Count;
        public void Add(FL_Entry NewEntry)
        {
            List_Full.Add(NewEntry);
            if (NewEntry.Main) List.Add(NewEntry);
        }

        public FL_FilledPlates FillPlates(FL_Settings Settings)
        {
            var FP = new FL_FilledPlates(Settings, this);
            Tuple<int, int, int> Key; float Vol; int plate, Row, Col;
            Settings.Signature = char.ConvertFromUtf32(_Rnd.Next(65, 90)) + _Rnd.Next(10000, 99999);
            Settings.TimeStampRand = DateTime.Now;
            //First Fill Main Plates
            for (plate = 0; plate < Settings.DestPlates; plate++)
            {
                Row = Settings.Row_First - 1;
                Col = Settings.Col_First;
                //Decide how many repeats of each
                SetInstances(Settings.Randomize_Ordered ? (Settings.Wells_Per_Plate - Settings.RandEmpties) : Settings.Wells_Per_Plate, Settings.ActiveTips);
                //Randomize into plates
                foreach (var FLE in this)
                {
                    for (int i = 0; i < (FLE.Instances / Settings.ActiveTips); i++)
                    {
                        int iJump = 0, rJump = 0; bool retry;
                        do
                        {
                            retry = false;
                            if (Settings.Randomize_Ordered)
                            { //Randomization
                                do
                                {
                                    Row = _Rnd.Next(Settings.Row_First, Settings.Row_Last + 1); rJump++; if (rJump > 30) break;
                                } while (FLE.Imbalance_Row(plate, Row, Settings));
                                Col = _Rnd.Next(0, 1 + Settings.Col_Last - Settings.Col_First); //This is to deal with active tips and different start positions
                                Col -= (Col % Settings.ActiveTips);
                                Col += Settings.Col_First;
                                //retry = FLE.Imbalance_Row(plate, Row, Settings) || FLE.Imbalance_Col(plate, Col, Settings);
                            }
                            else
                            { //Ordered
                                Row++;
                                if (Row > Settings.Row_Last)
                                {
                                    Row = Settings.Row_First;
                                    Col += Settings.ActiveTips;
                                }
                            }
                            Key = new Tuple<int, int, int>(plate, Row, Col);
                            if (iJump++ > 3000) break;
                            retry = FP.FillHash.ContainsKey(Key);
                        } while (retry);
                        Vol = SetVolume(Settings); //Volume has to be the same for all the active tips, but doesn't have to be the same for multi-dispense
                        for (int tip = 0; tip < Settings.ActiveTips; tip++)
                        {
                            Key = new Tuple<int, int, int>(plate, Row, Col + tip);
                            if (!FP.FillHash.ContainsKey(Key)) //Checking again because of the jump-out
                                FP.FillHash.Add(Key, FLE.AddDest(Key, tip, Vol));
                        }
                    }
                }
            }
            //Now Fill the antibody plate if exists
            if (Settings.AbPlateUse)
            {
                Settings.DestPlates++;
                Row = Settings.Row_First - 1;
                Col = Settings.AbPlate_StartCol;
                foreach (var FLE in this.List_Full)
                {
                    for (int i = 0; i < (Settings.AbPlate_Reps / Settings.ActiveTips); i++)
                    {
                        int iJump = 0;
                        do
                        {
                            //Ordered
                            Row++;
                            if (Row > Settings.Row_Last)
                            {
                                Row = Settings.Row_First; Col += Settings.ActiveTips;
                            }
                            Key = new Tuple<int, int, int>(plate, Row, Col);
                            if (iJump++ > 1000) break;
                        } while (FP.FillHash.ContainsKey(Key));
                        Vol = SetVolume(Settings); //Volume has to be the same for all the active tips, but doesn't have to be the same for multi-dispense
                        for (int tip = 0; tip < Settings.ActiveTips; tip++)
                        {
                            Key = new Tuple<int, int, int>(plate, Row, Col + tip);
                            if (!FP.FillHash.ContainsKey(Key)) //Checking again because of the jump-out
                                FP.FillHash.Add(Key, FLE.AddDest(Key, tip, Vol));
                        }
                    }
                }
            }
            return FP;
        }

        /// <summary>
        /// Adds or subtracts the random volume
        /// </summary>
        private static float SetVolume(FL_Settings Settings)
        {
            if (_Rnd.Next(0, 2) == 0)
                return Settings.DispenseVolume + ((float)_Rnd.NextDouble() * Settings.DispenseVolRand);
            else
                return Settings.DispenseVolume - ((float)_Rnd.NextDouble() * Settings.DispenseVolRand);
        }

        private static Random _Rnd = new Random();

        public void SetInstances(int TotalWells, int ActiveTips)
        {
            TotalWells = (int)(ActiveTips * Math.Floor((double)TotalWells / ActiveTips));
            //Figure out how many instances are needed
            float Sum = RelativeRepresentation_Sum;
            foreach (var FLE in List)
                FLE.Instances = (int)(ActiveTips * Math.Round((double)(TotalWells * FLE.RelativeRepresentation) / (Sum * ActiveTips), 0));

            //Now adjust randomly if it doesn't add up perfectly
            int idx;
            while (InstancesTotalCount != TotalWells)
            {
                idx = _Rnd.Next(0, Count - 1);
                if (InstancesTotalCount > TotalWells)
                    List[idx].Instances -= ActiveTips; //Take Away
                else
                    List[idx].Instances += ActiveTips; //Add
            }
        }

        public float RelativeRepresentation_Sum => List.Sum(x => x.RelativeRepresentation);
        public int InstancesTotalCount => List.Sum(x => x.Instances);

        public string Stats
        {
            get
            {
                var sB = new StringBuilder();
                sB.Append(InstancesTotalCount.ToString() + " Total\r\n");
                foreach (var item in List)
                {
                    sB.Append(item.Name + ": " + item.Instances.ToString() + "  (plt,row,col)\r\n");
                    sB.Append(" (" + item.DestList.Select(x => x.Plate).Average().ToString("0.00") + ", ");
                    sB.Append("" + item.DestList.Select(x => x.Row).Average().ToString("0.0") + ", ");
                    sB.Append("" + item.DestList.Select(x => x.Col).Average().ToString("0.0") + ")\r\n");
                }
                return sB.ToString();
            }
        }

        public string Script(FL_Settings settings)
        {
            int i = 1, j, nDisPerAsp; bool TipLoad, TipUnload = false; float vAsp, vDisp = settings.DispenseVolume; string sourceRow; int sourceCol;
            int tipStartCol = 13 - settings.ActiveTips, tipStartRow = 8;
            int tipEndCol = tipStartCol, tipEndRow = tipStartRow;

            StringBuilder sB = new StringBuilder();
            sB.Append(
                "'" + settings.ExpName + "\r\n" +
                "'Sig: " + settings.Signature + "\r\n" +
                "'Timestamp: " + settings.TimeStampRand + "\r\n\r\n" +
                "option explicit\r\n\r\n" +
                "dim TipUnload\r\n" +
                "dim TipLoad\r\n" +
                "dim SourceRow\r\n" +
                "dim SourceCol\r\n" +
                "dim Plate\r\n" +
                "dim Row\r\n" +
                "dim Col\r\n" +
                "dim VolAspirate\r\n" +
                "dim VolDispense\r\n" +
                "dim NumDispensesPerAspirate\r\n" +
                "dim tipStartCol\r\n" +
                "dim tipStartRow\r\n" +
                "dim tipEndCol\r\n" +
                "dim tipEndRow\r\n" +
                "dim breakOut\r\n" +
                "breakOut = False\r\n" +
                //"dim numDispense\r\n" +
                "\r\n" +
                "Select case loopv\r\n");
            foreach (var source in settings.AbPlateUse ? this.List_Full : this.List)
            {
                TipLoad = true; j = -1;
                nDisPerAsp = Math.Min(2, settings.WellsPerAspirate); //max of 2 right now because of the tips
                foreach (var FDest in source.DestList)
                {
                    if (FDest.Tip > 0) continue; //Don't script out the other tips
                    j++;
                    if (j > source.DestList.Count - settings.WellsPerAspirate && j % settings.WellsPerAspirate == 0)
                        nDisPerAsp = source.DestList.Count - j; //if there is an odd number at the end, then set this for the remainder of these (will get reset for the next)
                    vAsp = (vDisp + 0) * nDisPerAsp; //Budget in a small amount extra breaks on the other side, so we should keep it at 0
                    PlateHelper.Convert12To96(source.SourceRow, source.SourceCol, settings.ConvertScriptSource_12to96, out sourceRow, out sourceCol);

                    sB.Append("  case " + i++ + "\r\n");
                    sB.Append($"     SourceRow = \"{sourceRow}\" : SourceCol = {sourceCol} : Plate = {FDest.Plate} : Row = {FDest.Row} : Col = {FDest.Col} : TipLoad = {TipLoad} : TipUnload = {TipUnload} \r\n");
                    sB.Append($"     VolAspirate = {vAsp} : VolDispense = {vDisp} : NumDispensesPerAspirate = {nDisPerAsp} \r\n");
                    sB.Append($"     tipStartCol = {tipStartCol} : tipStartRow = {tipStartRow} : tipEndCol = {tipEndCol} : tipEndRow = {tipEndRow} \r\n");
                    if (TipUnload == true) { tipEndCol = tipStartCol; tipEndRow = tipStartRow; }
                    TipLoad = TipUnload = false;
                }
                TipUnload = true;
                tipStartCol -= settings.ActiveTips;
                if (tipStartCol < 1) { tipStartCol = 13 - settings.ActiveTips; tipStartRow--; }
            }
            sB.Append(
            "  case Else\r\n" +
            "     breakOut = True\r\n" +
            "End select\r\n\r\n" +
            "Extend \"TipLoad\",TipLoad\r\n" +
            "Extend \"TipUnload\",TipUnload\r\n" +
            "Extend \"SourceRow\",SourceRow\r\n" +
            "Extend \"SourceCol\",SourceCol\r\n" +
            "Extend \"Plate\",Plate\r\n" +
            "Extend \"Row\",Row\r\n" +
            "Extend \"Col\",Col\r\n" +
            "Extend \"VolAspirate\",VolAspirate\r\n" +
            "Extend \"VolDispense\",VolDispense\r\n" +
            "Extend \"NumDispensesPerAspirate\",NumDispensesPerAspirate\r\n" +
            "Extend \"breakOut\",breakOut\r\n" +
            "Extend \"tipStartCol\",tipStartCol\r\n" +
            "Extend \"tipStartRow\",tipStartRow\r\n" +
            "Extend \"tipEndCol\",tipEndCol\r\n" +
            "Extend \"tipEndRow\",tipEndRow\r\n");
            return sB.ToString();
        }

        public IEnumerator<FL_Entry> GetEnumerator()
        {
            return List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return List.GetEnumerator();
        }
    }

    public class FL_Dest
    {
        public Tuple<int, int, int> B;
        public int Plate, Row, Col;
        public int Tip;
        public float Volume;
        public FL_Entry Source;

        public FL_Dest(Tuple<int, int, int> plateRowCol, int tip, float Vol, FL_Entry source)
        {
            Plate = plateRowCol.Item1;
            Row = plateRowCol.Item2;
            Col = plateRowCol.Item3;
            B = plateRowCol;
            Tip = tip;
            Source = source;
            Volume = Vol;
        }
    }

    public class FL_Entry
    {
        public string Name;
        public string Well;
        internal bool Main = true;

        public override string ToString() { return Name; }

        public string SourceRow => Well.Substring(0, 1);
        public string SourceCol => Well.Substring(1);

        public float RelativeRepresentation;
        internal int Instances;
        internal List<FL_Dest> DestList = new List<FL_Dest>();
        private static Random _Rnd = new Random();
        private Brush _Brush;


        internal static HashSet<Color> ColorsUsed = new HashSet<Color>();

        public static float Closest_Color_Dist(Color ToCheck)
        {
            if (ColorsUsed.Count < 1) return float.MaxValue;
            var S = new SortedDictionary<double, int>();
            foreach (var C in ColorsUsed)
            {
                double r = Math.Pow(C.R - ToCheck.R, 2);
                double g = Math.Pow(C.G - ToCheck.G, 2);
                double b = Math.Pow(C.B - ToCheck.B, 2);
                double d = Math.Sqrt(r + g + b);
                while (S.ContainsKey(d)) d += 0.001;
                S.Add(d, 0);
            }
            return (float)S.Keys.First();
        }

        internal Brush Brush
        {
            get
            {
                if (_Brush == null)
                {
                    //This tries to make the colors somewhat different
                    Color c; byte MaxColor = 232; //If we make it 256, it will get too light and hard to see
                    int i = 0;
                    do
                    {
                        c = Color.FromArgb(_Rnd.Next(MaxColor), _Rnd.Next(MaxColor), _Rnd.Next(MaxColor));
                        if (i++ > 100) break;
                    } while (Closest_Color_Dist(c) < 150);
                    ColorsUsed.Add(c);
                    _Brush = new SolidBrush(c);
                }
                return _Brush;
            }
        }

        public string GetValue(string ColumnName)
        {
            switch (ColumnName)
            {
                case "Name": return Name;
                case "Well": return Well;
                case "RelativeRepresentation": return RelativeRepresentation.ToString();
                default: return "";
            }
        }

        public void SetValue(string ColumnName, object Value)
        {
            switch (ColumnName)
            {
                case "Name":
                    Name = (string)Value;
                    if (Name.StartsWith("_")) { Name = Name.Substring(1); Main = false; } //This is set aside only for the antibody plate
                    break;
                case "Well": Well = (string)Value; break;
                case "RelativeRepresentation":
                    RelativeRepresentation = float.Parse((string)Value);
                    break;
                default: break;
            }
        }

        internal FL_Dest AddDest(Tuple<int, int, int> PlateRowCol, int Tip, float Volume)
        {
            var tDest = new FL_Dest(PlateRowCol, Tip, Volume, this);
            DestList.Add(tDest);
            return tDest;
        }

        public bool Imbalance_Row(int plate, int Row, FL_Settings flset)
        {
            if (DestList.Count < 2) return false;
            int midpoint = 1 + (flset.Row_Last - flset.Row_First) / 2;
            int low = DestList.Where(x => x.Row <= midpoint && x.Plate == plate).Count();
            if (Row < midpoint) low++;
            int hi = 1 + DestList.Where(x => x.Plate == plate).Count() - low;
            //return false;
            return Math.Abs(low - hi) > flset.ActiveTips;
        }

        public bool Imbalance_Col(int Col, FL_Settings flset)
        {
            if (DestList.Count < 2) return false;
            int midpoint = 1 + (flset.Col_Last - flset.Col_First) / 2;
            int low = DestList.Where(x => x.Col <= midpoint).Count();
            if (Col < midpoint) low++;
            int hi = (DestList.Count + 1) - low;
            return false;
            //return Math.Abs(low - hi) > flset.ActiveTips;
        }
    }

    public class PH_Well
    {
        public string sRow => char.ConvertFromUtf32(iRow + 64);
        public int iRow;
        public int iCol;

        public string WellLabel => sRow + " - " + iCol;
        public string Well => sRow + iCol;
        public string WellProper => sRow + iCol.ToString("00");

        public override string ToString()
        {
            return Well;
        }

        public PH_Well()
        {
            iRow = 1; iCol = 1;
        }

        public PH_Well(string Well)
        {
            char sRow = Well[0];
            iRow = 1 + sRow - 'A';
            iCol = int.Parse(Well.Substring(1, Well.Length - 1));
        }

        public PH_Well(int Row1Indexed, int Col1Indexed)
        {
            iRow = Row1Indexed;
            iCol = Col1Indexed;
        }

        /// <summary>
        /// Converts 95 well coordinates into 384 (upper left quad)
        /// </summary>
        internal PH_Well To384()
        {
            PH_Well w384 = new PH_Well();
            w384.iCol = 1 + (this.iCol - 1) / 2;
            w384.iRow = 1 + (this.iRow - 1) / 2;
            return w384;
        }
    }

    public static class PlateHelper
    {
        private static Dictionary<string, PH_Well> _f12_t384 = null;
        private static Dictionary<string, PH_Well> _f12_t96 = null;

        private static PH_Well c(string WellName)
        {
            return new PH_Well(WellName);
        }

        private static void Init()
        {
            if (_f12_t384 == null)
            {
                var d = new Dictionary<string, PH_Well>();
                d.Add("A1", c("C3"));
                d.Add("A2", c("C9"));
                d.Add("A3", c("C15"));
                d.Add("A4", c("C21"));
                d.Add("B1", c("H3"));
                d.Add("B2", c("H9"));
                d.Add("B3", c("H15"));
                d.Add("B4", c("H21"));
                d.Add("C1", c("M3"));
                d.Add("C2", c("M9"));
                d.Add("C3", c("M15"));
                d.Add("C4", c("M21"));
                _f12_t384 = d;

                d = new Dictionary<string, PH_Well>();
                d.Add("A1", c("B2"));
                d.Add("A2", c("B5"));
                d.Add("A3", c("B7"));
                d.Add("A4", c("B10"));
                d.Add("B1", c("D2"));
                d.Add("B2", c("D5"));
                d.Add("B3", c("D7"));
                d.Add("B4", c("D10"));
                d.Add("C1", c("G2"));
                d.Add("C2", c("G5"));
                d.Add("C3", c("G7"));
                d.Add("C4", c("G10"));
                _f12_t96 = d;
            }
        }

        public static PH_Well From96_To384_PHW(string Well96)
        {
            var cW = new PH_Well(Well96);
            return cW.To384();
        }

        public static PH_Well From12_To384_PHW(string Well)
        {
            Init();
            return _f12_t384[Well];
        }

        public static PH_Well From12_To96_PHW(string Well12)
        {
            Init();
            return _f12_t96[Well12];
        }

        public static void Convert12To96(string sourceRow12, string sourceCol12, bool convertScriptSource_12to96, out string sourceRow96, out int sourceCol96)
        {
            if (convertScriptSource_12to96)
            {
                //var cW12 = new PH_Well(sourceRow12 + sourceCol12);
                var cW96 = From12_To96_PHW(sourceRow12 + sourceCol12);
                sourceRow96 = cW96.sRow;
                sourceCol96 = cW96.iCol;
            }
            else
            {
                sourceRow96 = sourceRow12;
                sourceCol96 = int.Parse(sourceCol12);
            }
        }
    }

}
