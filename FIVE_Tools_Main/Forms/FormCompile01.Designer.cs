﻿
namespace FIVE_Tools_Main
{
    partial class FormCompile01
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txBx_ExpIDs = new System.Windows.Forms.TextBox();
            this.btn_SearchFiles = new System.Windows.Forms.Button();
            this.listBox_Files = new System.Windows.Forms.ListBox();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.txBx_ExpPath = new System.Windows.Forms.TextBox();
            this.listBox_Columns = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_CompareColumns = new System.Windows.Forms.Button();
            this.btn_SaveColList = new System.Windows.Forms.Button();
            this.btn_ReduceColumns = new System.Windows.Forms.Button();
            this.txBx_Filter = new System.Windows.Forms.TextBox();
            this.txBx_Append = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txBx_ExpIDs
            // 
            this.txBx_ExpIDs.Location = new System.Drawing.Point(166, 58);
            this.txBx_ExpIDs.Multiline = true;
            this.txBx_ExpIDs.Name = "txBx_ExpIDs";
            this.txBx_ExpIDs.Size = new System.Drawing.Size(361, 43);
            this.txBx_ExpIDs.TabIndex = 0;
            // 
            // btn_SearchFiles
            // 
            this.btn_SearchFiles.Location = new System.Drawing.Point(359, 29);
            this.btn_SearchFiles.Name = "btn_SearchFiles";
            this.btn_SearchFiles.Size = new System.Drawing.Size(75, 23);
            this.btn_SearchFiles.TabIndex = 1;
            this.btn_SearchFiles.Text = "Search Files";
            this.btn_SearchFiles.UseVisualStyleBackColor = true;
            this.btn_SearchFiles.Click += new System.EventHandler(this.btn_SearchFiles_Click);
            // 
            // listBox_Files
            // 
            this.listBox_Files.FormattingEnabled = true;
            this.listBox_Files.Location = new System.Drawing.Point(166, 123);
            this.listBox_Files.Name = "listBox_Files";
            this.listBox_Files.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox_Files.Size = new System.Drawing.Size(361, 316);
            this.listBox_Files.TabIndex = 31;
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(11, 217);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.ReadOnly = true;
            this.txBx_Update.Size = new System.Drawing.Size(150, 222);
            this.txBx_Update.TabIndex = 30;
            // 
            // txBx_ExpPath
            // 
            this.txBx_ExpPath.Location = new System.Drawing.Point(11, 61);
            this.txBx_ExpPath.Name = "txBx_ExpPath";
            this.txBx_ExpPath.Size = new System.Drawing.Size(150, 20);
            this.txBx_ExpPath.TabIndex = 32;
            this.txBx_ExpPath.Text = "R:\\FIVE\\EXP\\";
            // 
            // listBox_Columns
            // 
            this.listBox_Columns.FormattingEnabled = true;
            this.listBox_Columns.Location = new System.Drawing.Point(533, 58);
            this.listBox_Columns.Name = "listBox_Columns";
            this.listBox_Columns.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox_Columns.Size = new System.Drawing.Size(259, 381);
            this.listBox_Columns.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Search Folder";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Messages:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(163, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 36;
            this.label3.Text = "Enter all EXP IDs";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(530, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 37;
            this.label4.Text = "Columns";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(163, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 38;
            this.label5.Text = "Select Files to Examine";
            // 
            // btn_CompareColumns
            // 
            this.btn_CompareColumns.Location = new System.Drawing.Point(440, 29);
            this.btn_CompareColumns.Name = "btn_CompareColumns";
            this.btn_CompareColumns.Size = new System.Drawing.Size(87, 23);
            this.btn_CompareColumns.TabIndex = 39;
            this.btn_CompareColumns.Text = "Compare Cols";
            this.btn_CompareColumns.UseVisualStyleBackColor = true;
            this.btn_CompareColumns.Click += new System.EventHandler(this.btn_CompareColumns_Click);
            // 
            // btn_SaveColList
            // 
            this.btn_SaveColList.Location = new System.Drawing.Point(706, 29);
            this.btn_SaveColList.Name = "btn_SaveColList";
            this.btn_SaveColList.Size = new System.Drawing.Size(84, 23);
            this.btn_SaveColList.TabIndex = 40;
            this.btn_SaveColList.Text = "Save Col List";
            this.btn_SaveColList.UseVisualStyleBackColor = true;
            this.btn_SaveColList.Click += new System.EventHandler(this.btn_SaveColList_Click);
            // 
            // btn_ReduceColumns
            // 
            this.btn_ReduceColumns.Location = new System.Drawing.Point(601, 29);
            this.btn_ReduceColumns.Name = "btn_ReduceColumns";
            this.btn_ReduceColumns.Size = new System.Drawing.Size(99, 23);
            this.btn_ReduceColumns.TabIndex = 41;
            this.btn_ReduceColumns.Text = "Reduce Columns";
            this.btn_ReduceColumns.UseVisualStyleBackColor = true;
            this.btn_ReduceColumns.Click += new System.EventHandler(this.btn_ReduceColumns_Click);
            // 
            // txBx_Filter
            // 
            this.txBx_Filter.Location = new System.Drawing.Point(10, 108);
            this.txBx_Filter.Name = "txBx_Filter";
            this.txBx_Filter.Size = new System.Drawing.Size(150, 20);
            this.txBx_Filter.TabIndex = 42;
            this.txBx_Filter.Text = "*norm*";
            // 
            // txBx_Append
            // 
            this.txBx_Append.Location = new System.Drawing.Point(10, 156);
            this.txBx_Append.Name = "txBx_Append";
            this.txBx_Append.Size = new System.Drawing.Size(150, 20);
            this.txBx_Append.TabIndex = 43;
            this.txBx_Append.Text = "_sm";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 44;
            this.label6.Text = "Filter";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "Append";
            // 
            // FormCompile01
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 446);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txBx_Append);
            this.Controls.Add(this.txBx_Filter);
            this.Controls.Add(this.btn_ReduceColumns);
            this.Controls.Add(this.btn_SaveColList);
            this.Controls.Add(this.btn_CompareColumns);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox_Columns);
            this.Controls.Add(this.txBx_ExpPath);
            this.Controls.Add(this.listBox_Files);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.btn_SearchFiles);
            this.Controls.Add(this.txBx_ExpIDs);
            this.Name = "FormCompile01";
            this.Text = "Compile and Select Columns";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txBx_ExpIDs;
        private System.Windows.Forms.Button btn_SearchFiles;
        private System.Windows.Forms.ListBox listBox_Files;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.TextBox txBx_ExpPath;
        private System.Windows.Forms.ListBox listBox_Columns;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_CompareColumns;
        private System.Windows.Forms.Button btn_SaveColList;
        private System.Windows.Forms.Button btn_ReduceColumns;
        private System.Windows.Forms.TextBox txBx_Filter;
        private System.Windows.Forms.TextBox txBx_Append;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}