﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using FIVE.InCellLibrary;
using FIVE.RaftCal;

namespace FIVE_Tools_Main.Forms
{

    //PreCheckAnnotation_Settings - - use this to store annotations

    public partial class Form_PreCheckRaft : Form
    {
        public FormRaftCal CalCheck;
        public Dictionary<string, HashSet<FIVE.RaftInfoS>> RaftListOrig;
        public List<Tuple<INCELL_Folder, FIVE.RaftInfoS>> RaftGroup;
        public List<(string cls, int count)> ClassesOrdered;
        public bool PickListVersion = true;
        public List<string> menuClassItems;

        public int Table_ColMax = 7; //These we can change
        public int sizeWidth => (int)((flowLayout.Width - 30) / Table_ColMax) - 4;
        public float RowHeight => sizeWidth + 4;
        public string Error;
        public bool Drawing;
        public string Path_AlreadyPicked;
        public string Path_PickListFolder;
        public string Path_ImageMainFolder;
        public PictureBox LastClicked_PictureBox;
        public PBStorage LastClicked_PBS;
        public string Path_ImageSubFolder => Path.Combine(Path_ImageMainFolder, "PreChecks");
        public static int StdSizeDimension = 128;
        public List<string> RaftList;

        private static Bitmap _BM_Error;
        private static List<Bitmap> _BM_Annotations;

        public string SourceListName = "";

        public static Bitmap BM_Error
        {
            get
            {
                if (_BM_Error == null)
                {
                    _BM_Error = new Bitmap(StdSizeDimension, StdSizeDimension, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                    using (Graphics g = Graphics.FromImage(_BM_Error))
                    {
                        g.Clear(Color.DarkGray);
                    }
                }
                return _BM_Error;
            }
        }

        public static int BM_Annotation_Max
        {
            get
            {
                //- - - This way makes sense when you only have a few, but probably don't want to cycle through this many
                //if (_BM_Annotations == null) _ = BM_Annotation(1);
                //return _BM_Annotations.Count;

                return 2;
            }
        }

        public static Bitmap BM_Annotation(int index)
        {
            if (_BM_Annotations == null)
            {
                _BM_Annotations = new List<Bitmap>();
                _BM_Annotations.Add(ColoredBitmap(Color.DarkRed));   //1
                _BM_Annotations.Add(ColoredBitmap(Color.DarkBlue));  //2
                _BM_Annotations.Add(ColoredBitmap(Color.Purple));    //3 
                _BM_Annotations.Add(ColoredBitmap(Color.DarkGreen)); //4
                _BM_Annotations.Add(ColoredBitmap(Color.Gray));      //5
                _BM_Annotations.Add(ColoredBitmap(Color.DarkKhaki)); //6
                _BM_Annotations.Add(ColoredBitmap(Color.Green));     //7
                _BM_Annotations.Add(ColoredBitmap(Color.IndianRed)); //8
                _BM_Annotations.Add(ColoredBitmap(Color.Pink));      //9
            }
            return _BM_Annotations[index - 1];
        }

        private static Bitmap ColoredBitmap(Color color)
        {
            Bitmap tB = new Bitmap(StdSizeDimension, StdSizeDimension, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            using (Graphics g = Graphics.FromImage(tB))
            {
                g.Clear(color);
            }
            return tB;
        }

        public Form_PreCheckRaft(FormRaftCal previousCalCheck, Dictionary<string, HashSet<FIVE.RaftInfoS>> raftList, INCELL_DB dB_Main, string SourceListName = "")
        {
            InitializeComponent();
            LoadSave();

            RaftListOrig = raftList;
            CalCheck = previousCalCheck;
            this.SourceListName = SourceListName;

            { //Figure out Paths
                string PlateID = raftList.First().Key;
                string FIVID = PlateID.Substring(0, 6);
                txBx_ExportName.Text = PlateID.Substring(0, 8) + " P";
                Path_PickListFolder = @"R:\FIVE\EXP\" + FIVID + @"\3 PickRelease\";
                Path_ImageMainFolder = @"R:\FIVE\EXP\" + FIVID + @"\6 Images\";
                if (!Directory.Exists(Path_PickListFolder))
                {
                    Error = "Couldn't find PickList Path . ."; return;
                }
                Path_AlreadyPicked = Path.Combine(Path_PickListFolder, FIVID + " AlreadyPicked.txt");
                if (!File.Exists(Path_AlreadyPicked))
                {
                    Error = "Couldn't find Already Picked";
                }
            }

            RaftGroup = new List<Tuple<INCELL_Folder, FIVE.RaftInfoS>>();
            RaftList = new List<string>();

            foreach (var KVP in RaftListOrig)
            {
                if (KVP.Key == "") continue; //Some can be empty if there are empty rows on the bottom
                var PreFolder = dB_Main.Folders.Where(x => x.PlateID == KVP.Key).ToArray();
                if (PreFolder.Length == 0) Error = KVP.Key + " couldn't be found in this dB. Try changing the spool location.";
                var IC_Folder = PreFolder[0];
                foreach (var item in KVP.Value)
                {
                    RaftGroup.Add(new Tuple<INCELL_Folder, FIVE.RaftInfoS>(IC_Folder, item));
                    RaftList.Add(item.RaftID);
                }
            }
            ClassesOrdered = RaftGroup.GroupBy(rg => rg.Item2.ClassLabel).Select(group => (cls: group.Key, count: group.Count())).OrderByDescending(x => x.count).ToList();
            PickListVersion = float.IsNaN(RaftGroup[0].Item2.Score);

            if (PickListVersion)
            {
                //Use standard pick classs
                menuClassItems = new List<string> { "Good", "Too many cells", "Too close to edge", "Focus Poor", "Calibration Error", "Dead Cell", "Blank", "Dividing", "Annotation Error", "Other" };
            } else
            {
                //Sort in a different order
                RaftGroup = RaftGroup.OrderBy(TP => TP.Item2.Note).ToList();
                RaftList = RaftGroup.Select(x => x.Item2.RaftID).ToList();
                menuClassItems = ClassesOrdered.Select(x => x.cls).ToList();
            }
            SetupContextMenus();
        }

        public PreCheckSettingsStore SettingStore = new PreCheckSettingsStore();
        public bool CheckEvents = true;

        public void LoadSave(bool Save = false)
        {
            if (Save)
            {
                SettingStore.AddRinseWell = chkBx_AddRinseWell.Checked;
                SettingStore.Randomize = chkBx_Randomize.Checked;
                SettingStore.AppendAlreadyPicked = chkBx_AppendAlreadyPicked.Checked;
                int t;
                if (!int.TryParse(txBx_WellsCount.Text, out t)) t = 95;
                SettingStore.WellsCount = t;
            }
            else
            {
                CheckEvents = false;
                chkBx_AddRinseWell.Checked = SettingStore.AddRinseWell;
                chkBx_Randomize.Checked = SettingStore.Randomize;
                chkBx_AppendAlreadyPicked.Checked = SettingStore.AppendAlreadyPicked;
                txBx_WellsCount.Text = SettingStore.WellsCount.ToString();
                CheckEvents = true;
            }
        }

        public void DrawTable()
        {
            //flowLayoutPanel1.Controls.Add(button);
            txBx_Update.Text = "Loading . ."; Application.DoEvents();
            Drawing = true;
            int Col = Table_ColMax; int Counter = 0;
            Size PBSize = new Size(sizeWidth, sizeWidth);
            PictureBox PB;
            var Settings = new RaftImage_Export_Settings(CalCheck.ExportCalForm.Settings);
            Settings.OnlyExportFullSize = false; Settings.Min_AspectRatio_Keep = 0.4F;
            if (RaftGroup == null)
            {
                txBx_Update.Text += "\r\nNo Rafts, check file or check folder system";
                return;
            }

            foreach (var TP in RaftGroup)
            {
                PB = new PictureBox(); PB.Size = PBSize; PB.SizeMode = PictureBoxSizeMode.StretchImage;
                PB.Image = ExportCalForm.GetImage(TP.Item2, TP.Item1, Settings, CalCheck.CurrentWVParams, CalCheck /*, Counter.ToString()*/);
                var tt = new ToolTip(); tt.SetToolTip(PB, TP.Item2.Note + "\r\n\r\n" + TP.Item2.ClassLabel);
                var PBS = new PBStorage(TP, PB.Image, menuClassItems); //Save the information about this
                PB.Tag = PBS;
                PB.Click += PB_Click;
                flowLayout.Controls.Add(PB);
                txBx_Update.Text = "Showing " + Counter++;
                Application.DoEvents();
            }
            Drawing = false;
            Update_Breaks_List();
            txBx_Update.Text = "Finished!";
        }

        //Sometimes the break needs to be turned off . . 
        //Don't call update breaks i it is already offf

        public void Update_Breaks_List()
        {
            int ActualCounter = 0; PBStorage PB; int PerPlateCounter = 1; int SubPlate = 0;
            StringBuilder sB = new StringBuilder(); sB.Append("Index\tRaftID\tNote\tPlateID\r\n");
            bool NeedsBreak;
            for (int i = 0; i < flowLayout.Controls.Count; i++)
            {
                NeedsBreak = false;
                PB = (PBStorage)flowLayout.Controls[i].Tag;
                PB.SubPlate = SubPlate;
                PB.IndexWithin = PerPlateCounter;
                sB.Append((PB.Active ? PerPlateCounter.ToString() : "") + "\t" + PB.Raft.RaftID + "\t" + PB.Raft.PlateID + "\t" + PB.Annotation + "\r\n");
                if (PB.Active)
                {
                    ActualCounter++; PerPlateCounter++;
                    NeedsBreak = ActualCounter % SettingStore.WellsCount == 0;
                    flowLayout.SetFlowBreak(flowLayout.Controls[i], NeedsBreak);
                    if (NeedsBreak)
                    {
                        PerPlateCounter = 1; SubPlate++;
                        sB.Append("- - - - - - - \r\n");
                        Application.DoEvents();
                    }
                }
                if (!NeedsBreak) flowLayout.SetFlowBreak(flowLayout.Controls[i], false);
            }
            txBx_List.Text = sB.ToString();
        }

        private void PB_Click(object sender, EventArgs e)
        {
            //if (Drawing) return;
            var ME = (MouseEventArgs)e;
            LastClicked_PictureBox = (PictureBox)sender;
            LastClicked_PBS = (PBStorage)LastClicked_PictureBox.Tag;

            if (ME.Button == MouseButtons.Right)
            {
                //PBStore.Annotation_Current += 2;
                contextMenu01.Items[0].Text = LastClicked_PBS.Raft.RaftID + " " + LastClicked_PBS.Annotation;
                contextMenu01.Show(Cursor.Position);
                contextMenu01.Visible = true;
                return;
            }
            int newAnn = LastClicked_PBS.Annotation_Current + 1;
            if (newAnn >= BM_Annotation_Max) newAnn = 0;
            AssignAnnotation(newAnn);
        }

        private void AssignAnnotation(int newAnnotation, string Message = "")
        {
            LastClicked_PBS.Annotation_Previous = LastClicked_PBS.Annotation_Current;
            LastClicked_PBS.Annotation_Current = newAnnotation;
            txBx_Update.Text = Message == "" ? LastClicked_PBS.Annotation : Message;

            if (LastClicked_PBS.Annotation_Current == 0)
            {
                LastClicked_PictureBox.Image = LastClicked_PBS.image;
                LastClicked_PictureBox.Size = new Size(LastClicked_PictureBox.Height, LastClicked_PictureBox.Height);
            }
            else
            {
                LastClicked_PictureBox.Image = BM_Annotation(LastClicked_PBS.Annotation_Current);
                LastClicked_PictureBox.Size = new Size(18, LastClicked_PictureBox.Height);
            }
            //toolTip1.SetToolTip(PB, PBStore.Annotation);
            Application.DoEvents();
            if ((LastClicked_PBS.Active && LastClicked_PBS.Annotation_Previous != 0) || (!LastClicked_PBS.Active && LastClicked_PBS.Annotation_Previous == 0))
                Update_Breaks_List();
        }

        private void btn_First_Click(object sender, EventArgs e)
        {
            FinishClick(1);
        }

        private void btn_All_Click(object sender, EventArgs e)
        {
            FinishClick(int.MaxValue);
        }

        public static Random _Rand = new Random();

        public void FinishClick(int PlatesToSave)
        {
            LoadSave(true);
            txBx_Update.Text = "Exporting Pick List (not to CMS) . . "; Application.DoEvents();
            //Do some pre checks
            string newFile = Path.Combine(Path_PickListFolder, Path.GetFileNameWithoutExtension(txBx_ExportName.Text) + ".txt");
            if (File.Exists(newFile))
            {
                txBx_Update.Text = "File Already Exists, please use a different name.";
                return;
            }
            string szFirstSubPlate = txBx_ExportName.Text.Substring(txBx_ExportName.Text.LastIndexOf("P") + 1);
            int FirstSubPlate;
            if (!int.TryParse(szFirstSubPlate, out FirstSubPlate))
            {
                txBx_Update.Text = "Couldn't find SubPlate in Export Name (" + txBx_ExportName.Text + ")";
                return;
            }
            if (!Directory.Exists(Path_ImageSubFolder)) Directory.CreateDirectory(Path_ImageSubFolder);

            //Now get the data
            PBStorage PBS; int previousPlate = -1; double PlateMult = 10000;
            var Tracker = new SortedDictionary<double, string>(); double Key; var InActive = new List<string>();
            for (int i = 0; i < flowLayout.Controls.Count; i++)
            {
                PBS = (PBStorage)flowLayout.Controls[i].Tag;
                PBS.image.Save(Path.Combine(Path_ImageSubFolder, PBS.Raft.PlateID + "." + PBS.Raft.WellLabel + "." + PBS.Raft.FOV + "." + PBS.Raft.RaftID + ".jpg"));
                if (!PBS.Active) { InActive.Add(PBS.Export(FirstSubPlate)); continue; }
                if (PBS.SubPlate != previousPlate)
                {
                    if (PlatesToSave <= PBS.SubPlate) break;
                    if (previousPlate == -1) Tracker.Add(-1, PBS.Export(0, true)); //Header
                    if (SettingStore.AddRinseWell) Tracker.Add(PlateMult * PBS.SubPlate, PBS.Export(FirstSubPlate, false, true));
                    previousPlate = PBS.SubPlate;
                }
                if (SettingStore.Randomize) do Key = PBS.SubPlate * PlateMult + 10 + (_Rand.NextDouble() * 100); while (Tracker.ContainsKey(Key));
                else Key = PBS.SubPlate * PlateMult + 10 + PBS.IndexWithin;
                Tracker.Add(Key, PBS.Export(FirstSubPlate));
            }

            //Save the PickList file
            txBx_Update.Text = "Saving Pick List (not to CMS) . . "; Application.DoEvents();
            var sB = new StringBuilder();
            foreach (string Line in Tracker.Values) sB.Append(Line);
            string sz = sB.ToString();
            File.WriteAllText(newFile, sz);

            // Now update Already Picked
            txBx_Update.Text += "\r\nAppending AlreadyPicked . . "; Application.DoEvents();
            if (SettingStore.AppendAlreadyPicked)
            {
                sB = new StringBuilder(); bool First = true;
                foreach (string Line in Tracker.Values) { if (First) First = false; else sB.Append(Line); }
                foreach (string Line in InActive) sB.Append(Line);
                try
                {
                    File.AppendAllText(Path_AlreadyPicked, sB.ToString());
                }
                catch
                {
                    txBx_Update.Text = "Exported the Picklist but \r\n *** Could not write to the AlreadyPicked file, probably because it is open somewhere else. ***";
                    return;
                }
            }

            txBx_Update.Text += "\r\nDone saving pick list (not to CMS). "; Application.DoEvents();
        }

        private void Form_PreCheckRaft_Shown(object sender, EventArgs e)
        {
            DrawTable();
        }

        private void Form_PreCheckRaft_Load(object sender, EventArgs e)
        {

        }

        private void txBx_WellsCount_TextChanged(object sender, EventArgs e)
        {
            if (!CheckEvents) return;
            if (txBx_WellsCount.Text == "" || txBx_WellsCount.Text == "1" || txBx_WellsCount.Text == "0" || txBx_WellsCount.Text == "2") return;
            LoadSave(true);
            Update_Breaks_List();
        }

        private void chkBx_CheckedChanged(object sender, EventArgs e)
        {
            if (!CheckEvents) return;
            var chkBx = (CheckBox)sender;
            LoadSave(true);
            if (chkBx.Text == chkBx_AddRinseWell.Text)
            {
                Update_Breaks_List();
            }
        }

        private void btn_Poster_Click(object sender, EventArgs e)
        {
            txBx_Update.Text = "Creating Poster . . ";
            int cols = 12; int rows = 8; int r = 0; int c = 0; int i;
            Bitmap bm = new Bitmap(sizeWidth * cols, sizeWidth * rows);
            using (Graphics g = Graphics.FromImage(bm))
            {
                for (i = 0; i < flowLayout.Controls.Count; i++)
                {
                    g.DrawImage(((PictureBox)flowLayout.Controls[i]).Image, new Rectangle(c, r, sizeWidth, sizeWidth));
                    if (i > (cols * rows)) break;
                    if (i == 80)
                    {

                    }
                    r += sizeWidth;
                    if (r >= (rows * sizeWidth))
                    {
                        c += sizeWidth; r = 0;
                    }
                }
            }
            if (!Directory.Exists(Path_ImageSubFolder)) Directory.CreateDirectory(Path_ImageSubFolder);
            bm.Save(Path.Combine(Path_ImageSubFolder, SourceListName + "_Poster.jpg"));
            txBx_Update.Text = "Finished Poster.";
        }

        private void SetupContextMenus()
        {
            ToolStripMenuItem mi;
            mi = new ToolStripMenuItem("- - info - - ");
            contextMenu01.Items.Add(mi);
            contextMenu01.Items.Add("-");

            for (int i = 0; i < menuClassItems.Count; i++)
            {
                mi = new ToolStripMenuItem("&" + i + " " + menuClassItems[i]);
                mi.Click += Mi_Click;
                contextMenu01.Items.Add(mi);
            }
        }

        private void Mi_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = (ToolStripMenuItem)sender;
            string Note = mi.Text;
            int newAnnotation = int.Parse(Note.Substring(1, 1));
            AssignAnnotation(newAnnotation, Note.Substring(3));
        }
    }

    public class PreCheckSettingsStore
    {
        public bool AddRinseWell;
        public bool Randomize;
        public bool AppendAlreadyPicked;
        public int WellsCount;

        public PreCheckSettingsStore()
        {
            AddRinseWell = Randomize = AppendAlreadyPicked = true;
            WellsCount = 95;
        }
    }

    public class PBStorage
    {
        public Image image;
        public int Annotation_Current = 0;
        public int Annotation_Previous = 0;
        public INCELL_Folder Folder;
        public FIVE.RaftInfoS Raft;
        public int SubPlate = 0;
        public int IndexWithin = 0;
        public List<string> AnnotationList;

        public bool Active => Annotation_Current == 0;

        public string Annotation
        {
            get
            {
                //adjusted 3/2023
                return AnnotationList[Annotation_Current];

                //Old way before 2/2023
                //switch (Annotation_Current)
                //{
                //    case 1: return "TooMany";
                //    case 2: return "Edge";
                //    case 3: return "Focus";
                //    case 4: return "Calibration";
                //    case 5: return "Dead";
                //    case 6: return "Blank";
                //    case 7: return "Dividing";
                //    case 8: return "Unused 8";
                //    case 9: return "Other";
                //    default: return "Picked";
                //}
            }
        }

        public PBStorage(Tuple<INCELL_Folder, FIVE.RaftInfoS> Folder_RaftInfoTuple, Image image1, List<string> SetAnnotationList)
        {
            image = image1;
            Folder = Folder_RaftInfoTuple.Item1;
            Raft = Folder_RaftInfoTuple.Item2;
            AnnotationList = SetAnnotationList;
        }

        public string Export(int FirstSubPlate, bool OnlyHeaders = false, bool RinseWell = false)
        {
            return Raft.Export(FirstSubPlate + SubPlate, OnlyHeaders, RinseWell, Annotation);
        }
    }

    public class PreCheckAnnotation_Settings
    {
        public static string DefaultPath = Path.Combine(Path.GetTempPath(), typeof(PreCheckAnnotation_Settings).Name + ".xml");

        public static PreCheckAnnotation_Settings Load()
        {
            return Load(DefaultPath);
        }

        public static PreCheckAnnotation_Settings Load(string Path)
        {
            if (!File.Exists(Path)) goto JUMPHEREISSUE;
            try
            {
                using (var stream = File.OpenRead(Path))
                {
                    var serializer = new XmlSerializer(typeof(PreCheckAnnotation_Settings));
                    return (PreCheckAnnotation_Settings)serializer.Deserialize(stream);
                }
            }
            catch
            {

            }
        JUMPHEREISSUE:
            PreCheckAnnotation_Settings P = Defaults();
            P.Save();
            return P;
        }

        public static PreCheckAnnotation_Settings Defaults()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            Save(DefaultPath);
        }

        public void Save(string Path)
        {
            using (var writer = new StreamWriter(Path))
            {
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(writer, this);
                writer.Flush();
            }
        }
    }

}
