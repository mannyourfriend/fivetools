﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE_Tools_Main
{

    public enum FormRegResult
    {
        Continue = 0, Exit = 1, Retry = 2
    }

    public partial class FormRegistration : Form
    {

        public FormRegistration()
        {
            InitializeComponent();
        }

        public FormRegResult continueCondition = FormRegResult.Exit;
        public string ResultMessage = "";
        public List<FIVE_IMG.SharedResource> SR_List;
        public int CurrentSR;
        public string RegisterWellResult;
        public FIVE_IMG.SVP_Registration_Params RegParams;

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                PointF tP;
                try
                {
                    tP = new PointF(float.Parse(txBx_Offset_X.Text), float.Parse(txBx_Offset_Y.Text));
                }
                catch { txBx_Update.Text = "Check that well offset points are numbers."; return; }
                FIVE_IMG.InCell_To_Leica.SR.Offset_WellRefined = tP;
            } else if (radioButton2.Checked)
            {
                FIVE_IMG.InCell_To_Leica.SR.Offset_WellRefined = SR.IAR.Offset;
            } else if (radioButton3.Checked)
            {
                PointF tP;
                try
                {
                    tP = new PointF(float.Parse(txBx_Cu_X.Text), float.Parse(txBx_Cu_Y.Text));
                }
                catch { txBx_Update.Text = "Check that custom points are numbers."; return; }
                FIVE_IMG.InCell_To_Leica.SR.Offset_WellRefined = tP;
            }

            //Also update the RegParams that are inputted here
            float LastFOVMixRatio;
            if (!float.TryParse(txBx_LastFOVMixRatio.Text, out LastFOVMixRatio)) { txBx_Update.Text = "Check that the LastFOVMixRatio is between 0 and 1"; return; }
            RegParams.LastFOVOffset_over_WellOffset_MixRatio = LastFOVMixRatio;
            float x, y; bool bx, by;
            bx = float.TryParse(txBx_MaxDev_X.Text, out x);
            by = float.TryParse(txBx_MaxDev_Y.Text, out y);
            if (!(bx && by)) { txBx_Update.Text = "Check that the Max Dev X and Y are numeric."; return; }
            RegParams.FOVOffset_MaxDeviationFromWell = new PointF(x,y);

            continueCondition = FormRegResult.Continue;
            this.Hide();
        }

        private void btn_SR_Backward_Click(object sender, EventArgs e)
        {
            CurrentSR--; if (CurrentSR < 0)
                CurrentSR = SR_List.Count - 1;
            UpdateReport();
        }

        private void btn_SR_Forward_Click(object sender, EventArgs e)
        {
            CurrentSR++; if (CurrentSR >= SR_List.Count) CurrentSR = 0;
            UpdateReport();
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            continueCondition = FormRegResult.Exit;
            this.Hide();
        }

        private void FormRegistration_Load(object sender, EventArgs e)
        {
            RegisterWellResult = FIVE_IMG.InCell_To_Leica.Leica003b_RegisterWell();
            SR_List = FIVE_IMG.InCell_To_Leica.SRQ.ToList();

            if (SR_List.Count == 0)
            {
                txBx_Update.Text = "Couldn't find an overlay, probably means that there was an issue with the intensity of the Leica image being too low or too little contrast.";
                return;
            }
            else
            {
                txBx_Update.Text = RegisterWellResult;
            }

            CurrentSR = 0;

            CalculateDisplayWellOffset();
            lbl_MaxRangeReport.Text = "Max Range :" +
                FIVE_IMG.InCell_To_Leica.SR.WellBased_MaxRange_LastSet.X.ToString("0.0") + ", " +
                FIVE_IMG.InCell_To_Leica.SR.WellBased_MaxRange_LastSet.Y.ToString("0.0");  //Range of how far off the wells are
            if (FIVE_IMG.InCell_To_Leica.SR.Offset_WellRefined.X != 0) radioButton1.Checked = true; else radioButton3.Checked = true;

            txBx_Cu_X.Text = FIVE_IMG.InCell_To_Leica.SR.DefaultInitOffset.X.ToString("0");
            txBx_Cu_Y.Text = FIVE_IMG.InCell_To_Leica.SR.DefaultInitOffset.Y.ToString("0");
            lbl_PlateWell.Text = SR.PlateID + "." + SR.CurrentWell;

            txBx_LastFOVMixRatio.Text = RegParams.LastFOVOffset_over_WellOffset_MixRatio.ToString();
            txBx_MaxDev_X.Text = RegParams.FOVOffset_MaxDeviationFromWell.X.ToString();
            txBx_MaxDev_Y.Text = RegParams.FOVOffset_MaxDeviationFromWell.Y.ToString();

            Application.DoEvents();
            this.Focus();
            this.BringToFront();
            this.TopMost = true;
            Application.DoEvents();

            UpdateReport();
        }

        private void CalculateDisplayWellOffset()
        {
            //Based on the includes . . 
            var Includes = SR_List.Where(x => x.IAR.RR.Include);
            txBx_Offset_X.Text = Includes.Average(x => x.IAR.Offset.X).ToString("0.00");
            txBx_Offset_Y.Text = Includes.Average(x => x.IAR.Offset.Y).ToString("0.00");
            //txBx_Offset_X.Text = FIVE_IMG.InCell_To_Leica.SR.Offset_WellRefined.X.ToString(); //Whole well offset
            //txBx_Offset_Y.Text = FIVE_IMG.InCell_To_Leica.SR.Offset_WellRefined.Y.ToString();
        }

        public FIVE_IMG.SharedResource SR { get => SR_List[CurrentSR]; }

        public void UpdateReport()
        {
            try
            {
                pictureBox_Main.Image = new Bitmap(SR.OverlayPath);

                label_ID.Text = SR.IAR.ExportName;
                label_Info.Text = "Score F " + SR.IAR.RR.Score_Final.ToString("0.0");
                lbl_Info2.Text = "Score R2 " + SR.IAR.RR.FinalR2.ToString("0.0");
                chkBx_Include.Checked = SR.IAR.RR.Include;

                txBx_FO_X.Text = SR.IAR.Offset.X.ToString();
                txBx_FO_Y.Text = SR.IAR.Offset.Y.ToString();
            }
            catch
            {
                pictureBox_Main.Image = null;
                txBx_Update.Text = "Some error occured, try another image.";
            }
        }

        private void chkBx_Include_CheckedChanged(object sender, EventArgs e)
        {
            SR.IAR.RR.Include = chkBx_Include.Checked;
            CalculateDisplayWellOffset();
        }
    }
}
