﻿
namespace FIVE_Tools_Main.Forms
{
    partial class Form_Layouts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Layouts));
            this.label1 = new System.Windows.Forms.Label();
            this.txBx_ExpName = new System.Windows.Forms.TextBox();
            this.txBx_DestinationPlateCount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txBx_ExportFolder = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridViewM = new System.Windows.Forms.DataGridView();
            this.btn_FillPlates = new System.Windows.Forms.Button();
            this.pictureBox_Plate1 = new System.Windows.Forms.PictureBox();
            this.btn_ExportThis = new System.Windows.Forms.Button();
            this.txBx_RowFirst = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txBx_ColFirst = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txBx_ColLast = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txBx_RowLast = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox_Legend = new System.Windows.Forms.PictureBox();
            this.pictureBox_Plate2 = new System.Windows.Forms.PictureBox();
            this.txBx_RanEmpty = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox_Plate3 = new System.Windows.Forms.PictureBox();
            this.txBx_WellsPerAspirate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.radioButton_Randomize = new System.Windows.Forms.RadioButton();
            this.radioButton_Ordered = new System.Windows.Forms.RadioButton();
            this.txBx_FF = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txBx_ActiveTips = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txBx_DispenseVolume = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lbl_96Mote = new System.Windows.Forms.Label();
            this.lbl_96Full = new System.Windows.Forms.Label();
            this.lbl_384Full = new System.Windows.Forms.Label();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.txBx_DispenseVolRand = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.chkBx_AddAntibodyPlate = new System.Windows.Forms.CheckBox();
            this.txBx_AbPlate_Reps = new System.Windows.Forms.TextBox();
            this.txBx_AbPlate_StartCol = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label_Paste_Names = new System.Windows.Forms.Label();
            this.txBx_DestinationPlateStart = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txBx_MinSourceVolumeUL = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txBx_CellsPerWell = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Plate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Legend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Plate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Plate3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Exp Name";
            // 
            // txBx_ExpName
            // 
            this.txBx_ExpName.Location = new System.Drawing.Point(15, 26);
            this.txBx_ExpName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ExpName.Name = "txBx_ExpName";
            this.txBx_ExpName.Size = new System.Drawing.Size(65, 23);
            this.txBx_ExpName.TabIndex = 1;
            this.txBx_ExpName.Text = "FIV999";
            this.txBx_ExpName.Leave += new System.EventHandler(this.txBx_ExpName_Leave);
            // 
            // txBx_DestinationPlateCount
            // 
            this.txBx_DestinationPlateCount.Location = new System.Drawing.Point(162, 26);
            this.txBx_DestinationPlateCount.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_DestinationPlateCount.Name = "txBx_DestinationPlateCount";
            this.txBx_DestinationPlateCount.Size = new System.Drawing.Size(43, 23);
            this.txBx_DestinationPlateCount.TabIndex = 3;
            this.txBx_DestinationPlateCount.Text = "2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(162, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Count";
            this.toolTip1.SetToolTip(this.label2, "# of Plates to produce. Usually 2 or 3 replicate plates. The Biomek currently has" +
        " a max of 4. This does NOT count the Antibody Plate (see below).");
            // 
            // txBx_ExportFolder
            // 
            this.txBx_ExportFolder.Location = new System.Drawing.Point(15, 102);
            this.txBx_ExportFolder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ExportFolder.Name = "txBx_ExportFolder";
            this.txBx_ExportFolder.Size = new System.Drawing.Size(366, 23);
            this.txBx_ExportFolder.TabIndex = 5;
            this.txBx_ExportFolder.Text = "c:\\temp\\";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 83);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Export Folder";
            // 
            // dataGridViewM
            // 
            this.dataGridViewM.AllowDrop = true;
            this.dataGridViewM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewM.Location = new System.Drawing.Point(400, 26);
            this.dataGridViewM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridViewM.Name = "dataGridViewM";
            this.dataGridViewM.Size = new System.Drawing.Size(534, 607);
            this.dataGridViewM.TabIndex = 6;
            // 
            // btn_FillPlates
            // 
            this.btn_FillPlates.Location = new System.Drawing.Point(17, 222);
            this.btn_FillPlates.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_FillPlates.Name = "btn_FillPlates";
            this.btn_FillPlates.Size = new System.Drawing.Size(88, 27);
            this.btn_FillPlates.TabIndex = 7;
            this.btn_FillPlates.Text = "Fill Plates";
            this.toolTip1.SetToolTip(this.btn_FillPlates, "Randomize the sources into the destination wells.  You can press this multiple ti" +
        "mes until you like the arrangement.");
            this.btn_FillPlates.UseVisualStyleBackColor = true;
            this.btn_FillPlates.Click += new System.EventHandler(this.btn_FillPlates_Click);
            // 
            // pictureBox_Plate1
            // 
            this.pictureBox_Plate1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Plate1.Location = new System.Drawing.Point(13, 258);
            this.pictureBox_Plate1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox_Plate1.Name = "pictureBox_Plate1";
            this.pictureBox_Plate1.Size = new System.Drawing.Size(208, 129);
            this.pictureBox_Plate1.TabIndex = 8;
            this.pictureBox_Plate1.TabStop = false;
            // 
            // btn_ExportThis
            // 
            this.btn_ExportThis.Location = new System.Drawing.Point(110, 222);
            this.btn_ExportThis.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_ExportThis.Name = "btn_ExportThis";
            this.btn_ExportThis.Size = new System.Drawing.Size(160, 27);
            this.btn_ExportThis.TabIndex = 9;
            this.btn_ExportThis.Text = "Export This Arrangement";
            this.toolTip1.SetToolTip(this.btn_ExportThis, "Saves out the arrangement by creating a plate map, plating setup, and biomek scri" +
        "pt.");
            this.btn_ExportThis.UseVisualStyleBackColor = true;
            this.btn_ExportThis.Click += new System.EventHandler(this.btn_ExportThis_Click);
            // 
            // txBx_RowFirst
            // 
            this.txBx_RowFirst.ForeColor = System.Drawing.Color.DarkRed;
            this.txBx_RowFirst.Location = new System.Drawing.Point(239, 26);
            this.txBx_RowFirst.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_RowFirst.Name = "txBx_RowFirst";
            this.txBx_RowFirst.Size = new System.Drawing.Size(65, 23);
            this.txBx_RowFirst.TabIndex = 12;
            this.txBx_RowFirst.Text = "1";
            this.toolTip1.SetToolTip(this.txBx_RowFirst, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used" +
        ".");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(236, 7);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "First Row";
            this.toolTip1.SetToolTip(this.label4, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used" +
        ".");
            // 
            // txBx_ColFirst
            // 
            this.txBx_ColFirst.ForeColor = System.Drawing.Color.ForestGreen;
            this.txBx_ColFirst.Location = new System.Drawing.Point(316, 26);
            this.txBx_ColFirst.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ColFirst.Name = "txBx_ColFirst";
            this.txBx_ColFirst.Size = new System.Drawing.Size(65, 23);
            this.txBx_ColFirst.TabIndex = 14;
            this.txBx_ColFirst.Text = "1";
            this.toolTip1.SetToolTip(this.txBx_ColFirst, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used" +
        ".");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.ForestGreen;
            this.label5.Location = new System.Drawing.Point(318, 7);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 15);
            this.label5.TabIndex = 13;
            this.label5.Text = "First Column";
            this.toolTip1.SetToolTip(this.label5, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used" +
        ".");
            // 
            // txBx_ColLast
            // 
            this.txBx_ColLast.ForeColor = System.Drawing.Color.ForestGreen;
            this.txBx_ColLast.Location = new System.Drawing.Point(316, 71);
            this.txBx_ColLast.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ColLast.Name = "txBx_ColLast";
            this.txBx_ColLast.Size = new System.Drawing.Size(65, 23);
            this.txBx_ColLast.TabIndex = 18;
            this.txBx_ColLast.Text = "12";
            this.toolTip1.SetToolTip(this.txBx_ColLast, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used" +
        ".");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.ForestGreen;
            this.label6.Location = new System.Drawing.Point(318, 52);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 15);
            this.label6.TabIndex = 17;
            this.label6.Text = "Last Column";
            this.toolTip1.SetToolTip(this.label6, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used" +
        ".");
            // 
            // txBx_RowLast
            // 
            this.txBx_RowLast.ForeColor = System.Drawing.Color.DarkRed;
            this.txBx_RowLast.Location = new System.Drawing.Point(239, 71);
            this.txBx_RowLast.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_RowLast.Name = "txBx_RowLast";
            this.txBx_RowLast.Size = new System.Drawing.Size(65, 23);
            this.txBx_RowLast.TabIndex = 16;
            this.txBx_RowLast.Text = "8";
            this.toolTip1.SetToolTip(this.txBx_RowLast, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used" +
        ".");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(236, 52);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 15);
            this.label7.TabIndex = 15;
            this.label7.Text = "Last Row";
            this.toolTip1.SetToolTip(this.label7, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used" +
        ".");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(741, 7);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 15);
            this.label8.TabIndex = 19;
            this.label8.Text = "Templates:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(811, 7);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 15);
            this.label9.TabIndex = 20;
            this.label9.Text = "FIVE Val 01";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(885, 7);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 15);
            this.label10.TabIndex = 21;
            this.label10.Text = "595 01";
            // 
            // pictureBox_Legend
            // 
            this.pictureBox_Legend.Location = new System.Drawing.Point(232, 270);
            this.pictureBox_Legend.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox_Legend.Name = "pictureBox_Legend";
            this.pictureBox_Legend.Size = new System.Drawing.Size(156, 179);
            this.pictureBox_Legend.TabIndex = 22;
            this.pictureBox_Legend.TabStop = false;
            // 
            // pictureBox_Plate2
            // 
            this.pictureBox_Plate2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Plate2.Location = new System.Drawing.Point(13, 393);
            this.pictureBox_Plate2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox_Plate2.Name = "pictureBox_Plate2";
            this.pictureBox_Plate2.Size = new System.Drawing.Size(208, 124);
            this.pictureBox_Plate2.TabIndex = 23;
            this.pictureBox_Plate2.TabStop = false;
            // 
            // txBx_RanEmpty
            // 
            this.txBx_RanEmpty.Location = new System.Drawing.Point(155, 71);
            this.txBx_RanEmpty.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_RanEmpty.Name = "txBx_RanEmpty";
            this.txBx_RanEmpty.Size = new System.Drawing.Size(65, 23);
            this.txBx_RanEmpty.TabIndex = 25;
            this.txBx_RanEmpty.Text = "1";
            this.toolTip1.SetToolTip(this.txBx_RanEmpty, "Random Empties is used to \"fingerprint\" the plates so that their identity and ori" +
        "entation can be checked. Recommended to be set at 1.");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(152, 52);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 15);
            this.label11.TabIndex = 24;
            this.label11.Text = "Rand Empty";
            this.toolTip1.SetToolTip(this.label11, "Random Empties is used to \"fingerprint\" the plates so that their identity and ori" +
        "entation can be checked. Recommended to be set at 1.");
            // 
            // pictureBox_Plate3
            // 
            this.pictureBox_Plate3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Plate3.Location = new System.Drawing.Point(13, 523);
            this.pictureBox_Plate3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox_Plate3.Name = "pictureBox_Plate3";
            this.pictureBox_Plate3.Size = new System.Drawing.Size(208, 129);
            this.pictureBox_Plate3.TabIndex = 26;
            this.pictureBox_Plate3.TabStop = false;
            // 
            // txBx_WellsPerAspirate
            // 
            this.txBx_WellsPerAspirate.Location = new System.Drawing.Point(169, 149);
            this.txBx_WellsPerAspirate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_WellsPerAspirate.Name = "txBx_WellsPerAspirate";
            this.txBx_WellsPerAspirate.Size = new System.Drawing.Size(65, 23);
            this.txBx_WellsPerAspirate.TabIndex = 28;
            this.txBx_WellsPerAspirate.Text = "2";
            this.toolTip1.SetToolTip(this.txBx_WellsPerAspirate, resources.GetString("txBx_WellsPerAspirate.ToolTip"));
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(162, 132);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 15);
            this.label12.TabIndex = 27;
            this.label12.Text = "Wells/Aspirate";
            this.toolTip1.SetToolTip(this.label12, resources.GetString("label12.ToolTip"));
            // 
            // radioButton_Randomize
            // 
            this.radioButton_Randomize.AutoSize = true;
            this.radioButton_Randomize.Checked = true;
            this.radioButton_Randomize.Location = new System.Drawing.Point(30, 134);
            this.radioButton_Randomize.Name = "radioButton_Randomize";
            this.radioButton_Randomize.Size = new System.Drawing.Size(84, 19);
            this.radioButton_Randomize.TabIndex = 29;
            this.radioButton_Randomize.TabStop = true;
            this.radioButton_Randomize.Text = "Randomize";
            this.radioButton_Randomize.UseVisualStyleBackColor = true;
            this.radioButton_Randomize.CheckedChanged += new System.EventHandler(this.radioButton_Randomize_CheckedChanged);
            // 
            // radioButton_Ordered
            // 
            this.radioButton_Ordered.AutoSize = true;
            this.radioButton_Ordered.Location = new System.Drawing.Point(30, 159);
            this.radioButton_Ordered.Name = "radioButton_Ordered";
            this.radioButton_Ordered.Size = new System.Drawing.Size(68, 19);
            this.radioButton_Ordered.TabIndex = 30;
            this.radioButton_Ordered.Text = "Ordered";
            this.radioButton_Ordered.UseVisualStyleBackColor = true;
            this.radioButton_Ordered.CheckedChanged += new System.EventHandler(this.radioButton_Ordered_CheckedChanged);
            // 
            // txBx_FF
            // 
            this.txBx_FF.Location = new System.Drawing.Point(255, 149);
            this.txBx_FF.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_FF.Name = "txBx_FF";
            this.txBx_FF.Size = new System.Drawing.Size(38, 23);
            this.txBx_FF.TabIndex = 32;
            this.txBx_FF.Text = "1.1";
            this.toolTip1.SetToolTip(this.txBx_FF, "Multiplier to determine how much extra cell mixture should go into each well");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(253, 132);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 15);
            this.label13.TabIndex = 31;
            this.label13.Text = "FFactor";
            this.toolTip1.SetToolTip(this.label13, "Multiplier to determine how much extra cell mixture should go into each well");
            // 
            // txBx_ActiveTips
            // 
            this.txBx_ActiveTips.Location = new System.Drawing.Point(124, 149);
            this.txBx_ActiveTips.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ActiveTips.Name = "txBx_ActiveTips";
            this.txBx_ActiveTips.Size = new System.Drawing.Size(37, 23);
            this.txBx_ActiveTips.TabIndex = 34;
            this.txBx_ActiveTips.Text = "2";
            this.toolTip1.SetToolTip(this.txBx_ActiveTips, "# of Tips the Biomek will use. 1 gives the most random arrangement and flexibilit" +
        "y, but 2 is twice as fast. ");
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(120, 132);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 15);
            this.label14.TabIndex = 33;
            this.label14.Text = "Tips";
            this.toolTip1.SetToolTip(this.label14, "# of Tips the Biomek will use. 1 gives the most random arrangement and flexibilit" +
        "y, but 2 is twice as fast. ");
            // 
            // txBx_DispenseVolume
            // 
            this.txBx_DispenseVolume.Location = new System.Drawing.Point(212, 194);
            this.txBx_DispenseVolume.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_DispenseVolume.Name = "txBx_DispenseVolume";
            this.txBx_DispenseVolume.Size = new System.Drawing.Size(65, 23);
            this.txBx_DispenseVolume.TabIndex = 36;
            this.txBx_DispenseVolume.Text = "100";
            this.toolTip1.SetToolTip(this.txBx_DispenseVolume, "How much volume to dispense per well.");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(199, 177);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 15);
            this.label15.TabIndex = 35;
            this.label15.Text = "Dispense Vol (ul)";
            this.toolTip1.SetToolTip(this.label15, "How much volume to dispense per well.");
            // 
            // lbl_96Mote
            // 
            this.lbl_96Mote.AutoSize = true;
            this.lbl_96Mote.ForeColor = System.Drawing.Color.Blue;
            this.lbl_96Mote.Location = new System.Drawing.Point(403, 7);
            this.lbl_96Mote.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_96Mote.Name = "lbl_96Mote";
            this.lbl_96Mote.Size = new System.Drawing.Size(50, 15);
            this.lbl_96Mote.TabIndex = 37;
            this.lbl_96Mote.Text = "96 Mote";
            this.lbl_96Mote.Click += new System.EventHandler(this.lbl_96Mote_Click);
            // 
            // lbl_96Full
            // 
            this.lbl_96Full.AutoSize = true;
            this.lbl_96Full.ForeColor = System.Drawing.Color.Blue;
            this.lbl_96Full.Location = new System.Drawing.Point(462, 7);
            this.lbl_96Full.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_96Full.Name = "lbl_96Full";
            this.lbl_96Full.Size = new System.Drawing.Size(41, 15);
            this.lbl_96Full.TabIndex = 38;
            this.lbl_96Full.Text = "96 Full";
            this.lbl_96Full.Click += new System.EventHandler(this.lbl_96Full_Click);
            // 
            // lbl_384Full
            // 
            this.lbl_384Full.AutoSize = true;
            this.lbl_384Full.ForeColor = System.Drawing.Color.Blue;
            this.lbl_384Full.Location = new System.Drawing.Point(516, 7);
            this.lbl_384Full.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_384Full.Name = "lbl_384Full";
            this.lbl_384Full.Size = new System.Drawing.Size(47, 15);
            this.lbl_384Full.TabIndex = 39;
            this.lbl_384Full.Text = "384 Full";
            this.lbl_384Full.Click += new System.EventHandler(this.lbl_384Full_Click);
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(232, 455);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(156, 197);
            this.txBx_Update.TabIndex = 40;
            // 
            // txBx_DispenseVolRand
            // 
            this.txBx_DispenseVolRand.Location = new System.Drawing.Point(301, 194);
            this.txBx_DispenseVolRand.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_DispenseVolRand.Name = "txBx_DispenseVolRand";
            this.txBx_DispenseVolRand.Size = new System.Drawing.Size(65, 23);
            this.txBx_DispenseVolRand.TabIndex = 42;
            this.txBx_DispenseVolRand.Text = "0";
            this.toolTip1.SetToolTip(this.txBx_DispenseVolRand, "Makes the volume of dispense variable. Set to 0 to turn off.");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(297, 177);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 15);
            this.label16.TabIndex = 41;
            this.label16.Text = "+/- Rand uL";
            this.toolTip1.SetToolTip(this.label16, "Makes the volume of dispense variable. Set to 0 to turn off.");
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(400, 636);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(297, 15);
            this.label17.TabIndex = 43;
            this.label17.Text = "Start Name with _ to only use that in the final (ab) plate";
            // 
            // chkBx_AddAntibodyPlate
            // 
            this.chkBx_AddAntibodyPlate.AutoSize = true;
            this.chkBx_AddAntibodyPlate.Location = new System.Drawing.Point(40, 196);
            this.chkBx_AddAntibodyPlate.Name = "chkBx_AddAntibodyPlate";
            this.chkBx_AddAntibodyPlate.Size = new System.Drawing.Size(70, 19);
            this.chkBx_AddAntibodyPlate.TabIndex = 44;
            this.chkBx_AddAntibodyPlate.Text = "Ab Plate";
            this.toolTip1.SetToolTip(this.chkBx_AddAntibodyPlate, resources.GetString("chkBx_AddAntibodyPlate.ToolTip"));
            this.chkBx_AddAntibodyPlate.UseVisualStyleBackColor = true;
            this.chkBx_AddAntibodyPlate.CheckedChanged += new System.EventHandler(this.chkBx_AddAntibodyPlate_CheckedChanged);
            // 
            // txBx_AbPlate_Reps
            // 
            this.txBx_AbPlate_Reps.Enabled = false;
            this.txBx_AbPlate_Reps.Location = new System.Drawing.Point(117, 194);
            this.txBx_AbPlate_Reps.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_AbPlate_Reps.Name = "txBx_AbPlate_Reps";
            this.txBx_AbPlate_Reps.Size = new System.Drawing.Size(27, 23);
            this.txBx_AbPlate_Reps.TabIndex = 45;
            this.txBx_AbPlate_Reps.Text = "4";
            this.toolTip1.SetToolTip(this.txBx_AbPlate_Reps, resources.GetString("txBx_AbPlate_Reps.ToolTip"));
            // 
            // txBx_AbPlate_StartCol
            // 
            this.txBx_AbPlate_StartCol.Enabled = false;
            this.txBx_AbPlate_StartCol.Location = new System.Drawing.Point(152, 194);
            this.txBx_AbPlate_StartCol.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_AbPlate_StartCol.Name = "txBx_AbPlate_StartCol";
            this.txBx_AbPlate_StartCol.Size = new System.Drawing.Size(27, 23);
            this.txBx_AbPlate_StartCol.TabIndex = 46;
            this.txBx_AbPlate_StartCol.Text = "2";
            this.toolTip1.SetToolTip(this.txBx_AbPlate_StartCol, resources.GetString("txBx_AbPlate_StartCol.ToolTip"));
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(101, 176);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(88, 15);
            this.label18.TabIndex = 47;
            this.label18.Text = "Ab Reps / 1 Col";
            this.toolTip1.SetToolTip(this.label18, resources.GetString("label18.ToolTip"));
            // 
            // label_Paste_Names
            // 
            this.label_Paste_Names.AutoSize = true;
            this.label_Paste_Names.ForeColor = System.Drawing.Color.BlueViolet;
            this.label_Paste_Names.Location = new System.Drawing.Point(635, 7);
            this.label_Paste_Names.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Paste_Names.Name = "label_Paste_Names";
            this.label_Paste_Names.Size = new System.Drawing.Size(79, 15);
            this.label_Paste_Names.TabIndex = 48;
            this.label_Paste_Names.Text = "PASTE Names";
            this.label_Paste_Names.Click += new System.EventHandler(this.label_Paste_Names_Click);
            // 
            // txBx_DestinationPlateStart
            // 
            this.txBx_DestinationPlateStart.Location = new System.Drawing.Point(110, 26);
            this.txBx_DestinationPlateStart.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_DestinationPlateStart.Name = "txBx_DestinationPlateStart";
            this.txBx_DestinationPlateStart.Size = new System.Drawing.Size(37, 23);
            this.txBx_DestinationPlateStart.TabIndex = 49;
            this.txBx_DestinationPlateStart.Text = "1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(101, 7);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 15);
            this.label19.TabIndex = 50;
            this.label19.Text = "Plate Start";
            this.toolTip1.SetToolTip(this.label19, "This is the starting sub-plate number. If you enter FIV999 as Exp, and 1 as start" +
        "ing plate, the first plate made will be called FIV999P1.");
            // 
            // txBx_MinSourceVolumeUL
            // 
            this.txBx_MinSourceVolumeUL.Location = new System.Drawing.Point(301, 149);
            this.txBx_MinSourceVolumeUL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_MinSourceVolumeUL.Name = "txBx_MinSourceVolumeUL";
            this.txBx_MinSourceVolumeUL.Size = new System.Drawing.Size(67, 23);
            this.txBx_MinSourceVolumeUL.TabIndex = 52;
            this.txBx_MinSourceVolumeUL.Text = "1200";
            this.toolTip1.SetToolTip(this.txBx_MinSourceVolumeUL, "Minimum Volume in the source plate. This will automatically increase the amount s" +
        "o that the tips won\'t run dry.");
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(301, 132);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 15);
            this.label20.TabIndex = 51;
            this.label20.Text = "Min Src uL";
            this.toolTip1.SetToolTip(this.label20, "Minimum Volume in the source plate. This will automatically increase the amount s" +
        "o that the tips won\'t run dry.");
            // 
            // txBx_CellsPerWell
            // 
            this.txBx_CellsPerWell.Location = new System.Drawing.Point(301, 237);
            this.txBx_CellsPerWell.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_CellsPerWell.Name = "txBx_CellsPerWell";
            this.txBx_CellsPerWell.Size = new System.Drawing.Size(65, 23);
            this.txBx_CellsPerWell.TabIndex = 54;
            this.txBx_CellsPerWell.Text = "5000";
            this.toolTip1.SetToolTip(this.txBx_CellsPerWell, "Changes the # cells per well in the spreadsheet, but it can be changed more once " +
        "the spreadsheet is opened.");
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(297, 220);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 15);
            this.label21.TabIndex = 53;
            this.label21.Text = "Cells per Well";
            this.toolTip1.SetToolTip(this.label21, "Makes the volume of dispense variable. Set to 0 to turn off.");
            // 
            // Form_Layouts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 660);
            this.Controls.Add(this.txBx_CellsPerWell);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txBx_MinSourceVolumeUL);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txBx_DestinationPlateStart);
            this.Controls.Add(this.label_Paste_Names);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txBx_AbPlate_StartCol);
            this.Controls.Add(this.txBx_AbPlate_Reps);
            this.Controls.Add(this.chkBx_AddAntibodyPlate);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txBx_DispenseVolRand);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.lbl_384Full);
            this.Controls.Add(this.lbl_96Full);
            this.Controls.Add(this.lbl_96Mote);
            this.Controls.Add(this.txBx_DispenseVolume);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txBx_ActiveTips);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txBx_FF);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.radioButton_Ordered);
            this.Controls.Add(this.radioButton_Randomize);
            this.Controls.Add(this.txBx_WellsPerAspirate);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pictureBox_Plate3);
            this.Controls.Add(this.txBx_RanEmpty);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pictureBox_Plate2);
            this.Controls.Add(this.pictureBox_Legend);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txBx_ColLast);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txBx_RowLast);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txBx_ColFirst);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txBx_RowFirst);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_ExportThis);
            this.Controls.Add(this.pictureBox_Plate1);
            this.Controls.Add(this.btn_FillPlates);
            this.Controls.Add(this.dataGridViewM);
            this.Controls.Add(this.txBx_ExportFolder);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txBx_DestinationPlateCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txBx_ExpName);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form_Layouts";
            this.Text = "FIV Layouts";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Layouts_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Plate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Legend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Plate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Plate3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txBx_ExpName;
        private System.Windows.Forms.TextBox txBx_DestinationPlateCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txBx_ExportFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridViewM;
        private System.Windows.Forms.Button btn_FillPlates;
        private System.Windows.Forms.PictureBox pictureBox_Plate1;
        private System.Windows.Forms.Button btn_ExportThis;
        private System.Windows.Forms.TextBox txBx_RowFirst;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txBx_ColFirst;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txBx_ColLast;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txBx_RowLast;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox_Legend;
        private System.Windows.Forms.PictureBox pictureBox_Plate2;
        private System.Windows.Forms.TextBox txBx_RanEmpty;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox_Plate3;
        private System.Windows.Forms.TextBox txBx_WellsPerAspirate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton radioButton_Randomize;
        private System.Windows.Forms.RadioButton radioButton_Ordered;
        private System.Windows.Forms.TextBox txBx_FF;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txBx_ActiveTips;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txBx_DispenseVolume;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbl_96Mote;
        private System.Windows.Forms.Label lbl_96Full;
        private System.Windows.Forms.Label lbl_384Full;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.TextBox txBx_DispenseVolRand;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkBx_AddAntibodyPlate;
        private System.Windows.Forms.TextBox txBx_AbPlate_Reps;
        private System.Windows.Forms.TextBox txBx_AbPlate_StartCol;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label_Paste_Names;
        private System.Windows.Forms.TextBox txBx_DestinationPlateStart;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox txBx_MinSourceVolumeUL;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txBx_CellsPerWell;
        private System.Windows.Forms.Label label21;
    }
}