﻿namespace FIVE_Tools_Main
{
    partial class Form_CombineModels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_AzureModelCompile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txBx_Folder = new System.Windows.Forms.TextBox();
            this.txBx_Name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.bgWrk01 = new System.ComponentModel.BackgroundWorker();
            this.btn_TF_Compile01 = new System.Windows.Forms.Button();
            this.txbx_Folder_2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_AzureModelCompile
            // 
            this.btn_AzureModelCompile.Location = new System.Drawing.Point(209, 135);
            this.btn_AzureModelCompile.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_AzureModelCompile.Name = "btn_AzureModelCompile";
            this.btn_AzureModelCompile.Size = new System.Drawing.Size(204, 27);
            this.btn_AzureModelCompile.TabIndex = 0;
            this.btn_AzureModelCompile.Text = "Azure Model Compile";
            this.btn_AzureModelCompile.UseVisualStyleBackColor = true;
            this.btn_AzureModelCompile.Click += new System.EventHandler(this.btn_AzureModelCompile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Folder with the Files to Compile";
            // 
            // txBx_Folder
            // 
            this.txBx_Folder.Location = new System.Drawing.Point(14, 39);
            this.txBx_Folder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Folder.Name = "txBx_Folder";
            this.txBx_Folder.Size = new System.Drawing.Size(814, 23);
            this.txBx_Folder.TabIndex = 2;
            this.txBx_Folder.TextChanged += new System.EventHandler(this.txBx_Folder_TextChanged);
            // 
            // txBx_Name
            // 
            this.txBx_Name.Location = new System.Drawing.Point(14, 113);
            this.txBx_Name.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Name.Name = "txBx_Name";
            this.txBx_Name.Size = new System.Drawing.Size(151, 23);
            this.txBx_Name.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 95);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Name";
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(446, 97);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(382, 127);
            this.txBx_Update.TabIndex = 5;
            // 
            // bgWrk01
            // 
            this.bgWrk01.WorkerReportsProgress = true;
            this.bgWrk01.WorkerSupportsCancellation = true;
            this.bgWrk01.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk01_DoWork);
            this.bgWrk01.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWrk01_ProgressChanged);
            this.bgWrk01.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk01_RunWorkerCompleted);
            // 
            // btn_TF_Compile01
            // 
            this.btn_TF_Compile01.Location = new System.Drawing.Point(209, 97);
            this.btn_TF_Compile01.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_TF_Compile01.Name = "btn_TF_Compile01";
            this.btn_TF_Compile01.Size = new System.Drawing.Size(204, 27);
            this.btn_TF_Compile01.TabIndex = 6;
            this.btn_TF_Compile01.Text = "TF Inferences Compile";
            this.btn_TF_Compile01.UseVisualStyleBackColor = true;
            this.btn_TF_Compile01.Click += new System.EventHandler(this.btn_TF_Compile01_Click);
            // 
            // txbx_Folder_2
            // 
            this.txbx_Folder_2.Location = new System.Drawing.Point(14, 68);
            this.txbx_Folder_2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txbx_Folder_2.Name = "txbx_Folder_2";
            this.txbx_Folder_2.Size = new System.Drawing.Size(814, 23);
            this.txbx_Folder_2.TabIndex = 7;
            // 
            // Form_CombineModels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 239);
            this.Controls.Add(this.txbx_Folder_2);
            this.Controls.Add(this.btn_TF_Compile01);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txBx_Name);
            this.Controls.Add(this.txBx_Folder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_AzureModelCompile);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form_CombineModels";
            this.Text = "Compilers";
            this.Shown += new System.EventHandler(this.Form_CombineModels_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_AzureModelCompile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txBx_Folder;
        private System.Windows.Forms.TextBox txBx_Name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.ComponentModel.BackgroundWorker bgWrk01;
        private System.Windows.Forms.Button btn_TF_Compile01;
        private System.Windows.Forms.TextBox txbx_Folder_2;
    }
}