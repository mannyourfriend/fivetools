﻿using FIVE_IMG;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE
{
    public partial class FormFSdB : Form
    {
        FIVE_IMG.Store_Validation_Parameters NVP;

        public FormFSdB(FIVE_IMG.Store_Validation_Parameters nVP = null)
        {
            NVP = nVP;
            InitializeComponent();
            LoadSave(false);
        }

        private void LoadSave(bool Save = false)
        {
            if (!Save)
            {
                fs_dbname.Text = NVP.fs_dbname;
                fs_Port.Text = NVP.fs_Port;
                fs_Username.Text = NVP.fs_Username;
                fs_Password.Text = NVP.fs_Password;

                fs_Filetoload.Text = NVP.fs_Filetoload;
            }
            else
            {
                //Just saves it to the NVP, but doesn't force this to save
                NVP.fs_dbname = fs_dbname.Text;
                NVP.fs_Port = fs_Port.Text;
                NVP.fs_Username = fs_Username.Text;
                NVP.fs_Password = fs_Password.Text;

                NVP.fs_Filetoload = fs_Filetoload.Text;
            }
        }

        private void btn_Browse_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.InitialDirectory = @"R:\FIVE\EXP";
            OFD.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            OFD.Title = "Please select a rename/normalized file to import";
            //OFD.RestoreDirectory = true;
            var DR = OFD.ShowDialog();
            if (DR == DialogResult.OK) fs_Filetoload.Text = OFD.FileName;
        }

        private void btn_Import_Click(object sender, EventArgs e)
        {
            LoadSave(true);
            bgWorkerImport.RunWorkerAsync(NVP);
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            bgWorkerImport.CancelAsync();
        }

        private void bgWorkerImport_DoWork(object sender, DoWorkEventArgs e)
        {
            var iNVP = (FIVE_IMG.Store_Validation_Parameters)e.Argument;
            var fDB = FeatureDBLib.FeaturedB.OpenConnection(iNVP.fs_dbname, iNVP.fs_Port, iNVP.fs_Username, iNVP.fs_Password);
            //string rnNormPath = @"R:\FIVE\EXP\FIV627\2 MLModels\FIV627 CellsUse_rnnorm.txt";
            fDB.InsertFromFile_InCarta(iNVP.fs_Filetoload, bgWorkerImport);
        }

        private void bgWorkerImport_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState.ToString() + "\r\n" + txBx_Update.Text;
        }

        private void bgWorkerImport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void btn_DeleteTables_Click(object sender, EventArgs e)
        {
            string DelCmd = "*DELETE*";
            if (fs_Filetoload.Text == DelCmd)
            {
                fs_Filetoload.Text = "";

                var iNVP = NVP;
                var fDB = FeatureDBLib.FeaturedB.OpenConnection(iNVP.fs_dbname, iNVP.fs_Port, iNVP.fs_Username, iNVP.fs_Password);
                fDB.dB.Open();
                fDB.DeleteTables();
            }
            else
            {
                var Res = MessageBox.Show("Are you sure? If so type " + DelCmd + " in the FilePATH window above and then click this again.");
                if (Res == DialogResult.OK) fs_Filetoload.Text = "Type " + DelCmd + " here.";
            }
        }
    }
}
