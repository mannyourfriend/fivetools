﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace FIVE_Tools_Main
{
    public partial class FormCompile01 : Form
    {
        public FormCompile01()
        {
            InitializeComponent();
        }

        public static Dictionary<string, char> ExtensionLookUp = new Dictionary<string, char>() { { ".TXT", '\t' }, { ".TSV", '\t' }, { ".CSV", ',' } };
        public static string[] Split_CommaSpace = new string[] { ",", " " };
        public static string[] Exclude = new string[7] { "._", "999", "REPORT", "REF_LONG", "INDEXMAP", "COMPILED", "_NGSMAP_" };

        private void btn_SearchFiles_Click(object sender, EventArgs e)
        {
            txBx_Update.Text = "Searching . . ";
            string[] Folders = Directory.GetDirectories(txBx_ExpPath.Text, "*", SearchOption.TopDirectoryOnly);
            string[] ExpIDs = txBx_ExpIDs.Text.Trim().ToUpper().Split(Split_CommaSpace, StringSplitOptions.RemoveEmptyEntries);
            string Filter = txBx_Filter.Text.Trim(); Filter = Filter == "" ? "*.*" : Filter;

            listBox_Files.Items.Clear();
            foreach (string ExpID in ExpIDs)
            {
                var FoldersSub = Folders.Where(x => x.Contains(ExpID));
                foreach (var Folder in FoldersSub)
                {
                    //listBox_Files.Items.Add("- - - - - " + Folder.Substring(Folder.Length - 6, 6));
                    txBx_Update.Text += "\r\n" + Folder.Substring(Folder.Length - 6, 6);
                    Application.DoEvents();
                    foreach (var File in Directory.GetFiles(Folder, Filter, SearchOption.AllDirectories))
                    {
                        string ext = Path.GetExtension(File).ToUpper();
                        if (ExtensionLookUp.ContainsKey(ext))
                        {
                            bool cont = true;
                            foreach (var item in Exclude) if (File.ToUpper().Contains(item)) { cont = false; break; }
                            if (cont) listBox_Files.Items.Add(File);
                        }
                    }
                }
            }
            txBx_Update.Text = "";
        }

        private void btn_CompareColumns_Click(object sender, EventArgs e)
        {
            if (listBox_Files.SelectedItems.Count < 1) return;
            var ColumnCounts = new Dictionary<string, int>();
            List<string> TempColumnList;
            foreach (var item in listBox_Files.SelectedItems)
            {
                TempColumnList = GetColumnHeaders(item.ToString());
                foreach (var Col in TempColumnList)
                {
                    if (!ColumnCounts.ContainsKey(Col)) ColumnCounts.Add(Col, 0);
                    ColumnCounts[Col]++;
                }
            }

            var Sorted = new SortedDictionary<int, List<string>>();
            foreach (var KVP in ColumnCounts)
            {
                if (!Sorted.ContainsKey(-KVP.Value)) Sorted.Add(-KVP.Value, new List<string>());
                Sorted[-KVP.Value].Add(KVP.Key);
            }

            listBox_Columns.Items.Clear();
            foreach (var KVP in Sorted)
            {
                foreach (var Col in KVP.Value)
                {
                    listBox_Columns.Items.Add(-KVP.Key + " " + Col);
                }
            }
        }

        public List<string> GetColumnHeaders(string FilePath)
        {
            string Ext = Path.GetExtension(FilePath).ToUpper();
            if (!ExtensionLookUp.ContainsKey(Ext)) return new List<string>();
            char delim = ExtensionLookUp[Ext];
            using (StreamReader SR = new StreamReader(FilePath))
            {
                string Line = SR.ReadLine();
                SR.Close();
                return Line.Split(delim).Select(x => x.ToUpper().Trim()).ToList();
            }
        }

        public List<string> ColumnsToKeep()
        {
            List<string> ColsKeep = new List<string>();
            foreach (var item in listBox_Columns.SelectedItems)
            {
                string T = item.ToString();
                T = T.Substring(T.IndexOf(" ") + 1);
                ColsKeep.Add(T);
            }
            return ColsKeep;
        }

        private void btn_SaveColList_Click(object sender, EventArgs e)
        {
            SaveFileDialog SFD = new SaveFileDialog();
            DialogResult DR = SFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                File.WriteAllLines(SFD.FileName, ColumnsToKeep());
                txBx_Update.Text = "Saved the columns in a simple lists here:\r\n" + SFD.FileName;
            }
        }

        private void btn_ReduceColumns_Click(object sender, EventArgs e)
        {
            HashSet<string> Columns;
            if (listBox_Columns.SelectedItems.Count == 0)
            {
                OpenFileDialog OFD = new OpenFileDialog();
                OFD.Title = "Select the column List to use";
                if (OFD.ShowDialog() == DialogResult.OK)
                {
                    string[] List = File.ReadAllLines(OFD.FileName);
                    Columns = new HashSet<string>(List);
                }
                else return;
            }
            else Columns = new HashSet<string>(ColumnsToKeep()); //Get it from selected list
            //Go through and Reduce Columns on the ones selected
            foreach (var file in listBox_Files.SelectedItems)
            {
                RemoveColumns_Internal(file.ToString(), Columns, ExtensionLookUp[Path.GetExtension(file.ToString()).ToUpper()]);
            }
        }

        public void RemoveColumns()
        {
            Debugger.Break();
            HashSet<string> S = new HashSet<string>(); //These are the columns we want to keep
            List<string> pth = new List<string>(); char delim = ',';
            string today = "6";
            if (today == "1")
            {
                pth.Add(@"R:\FIVE\EXP\FIV463\1 Data\FIV463_E484K_compiled_124741.csv");
                S.Add("PlateID");
                S.Add("ResultsID");
                //S.Add("OBJECT ID");
                S.Add("Row");
                S.Add("Column");
                //S.Add("FOV");
                S.Add("WELL LABEL");
                S.Add("Nuclei Area wv1");
                S.Add("Nuclei Form Factor wv1");
                S.Add("Nuclei Elongation wv1");
                S.Add("Nuclei Intensity wv1");
                S.Add("Nuclei Total Intensity wv1");
                S.Add("Nuclei Intensity CV wv1");
                S.Add("Nuclei Intensity-Bckg wv1");
                S.Add("Nuclei Max Intensity wv1");
                S.Add("Nuclei Spacing (SOI) wv1");
                S.Add("Nuclei Neighbor Count (SOI)  wv1");
                S.Add("Nuclei Intensity wv2");
                S.Add("Nuclei Intensity-Bckg wv2");
                S.Add("Nuclei Max Intensity wv2");
                S.Add("Cells Area wv1");
                S.Add("Cells Intensity (Cell) wv2");
                S.Add("Cells Total Intensity (Cell)  wv2");
                S.Add("Cells Intensity-Bckg (Cell)  wv2");
                S.Add("Cells Max Intensity wv2");
                S.Add("Plate ID");
                S.Add("Well X um");
                S.Add("Well Y um");
                //S.Add("CellsPerField");
            }
            if (today == "5")
            {
                delim = '\t';
                pth.Add(@"R:\FIVE\EXP\FIV540\2 MLModels\FIV540_CellsKeep_A7_rnnorm.txt");
                {
                    S.Add("PLATEID");
                    S.Add("RESULTSID");
                    S.Add("OBJECT ID");
                    S.Add("ROW");
                    S.Add("COLUMN");
                    S.Add("FOV");
                    S.Add("WELL LABEL");
                    S.Add("PLATE ID");
                    S.Add("WELL X UM");
                    S.Add("WELL Y UM");
                    S.Add("PLATE X UM");
                    S.Add("PLATE Y UM");
                    S.Add("IMAGENAME");
                    S.Add("CELLSPERFIELD");
                    S.Add("MLPLATEINDEX");
                    S.Add("NUCLEI AREA WVH");
                    S.Add("NUCLEI FORM FACTOR WVH");
                    S.Add("NUCLEI ELONGATION WVH");
                    S.Add("NUCLEI COMPACTNESS  WVH");
                    S.Add("NUCLEI DISPLACEMENT WVH");
                    S.Add("NUCLEI INTENSITY WVH");
                    S.Add("NUCLEI TOTAL INTENSITY WVH");
                    S.Add("NUCLEI INTENSITY CV WVH");
                    S.Add("NUCLEI LIGHT FLUX  WVH");
                    S.Add("NUCLEI INTENSITY SD WVH");
                    S.Add("NUCLEI MAX INTENSITY WVH");
                    S.Add("NUCLEI SKEWNESS WVH");
                    S.Add("NUCLEI KURTOSIS WVH");
                    S.Add("NUCLEI ENERGY WVH");
                    S.Add("NUCLEI ENTROPY WVH");
                    S.Add("NUCLEI GREY LEVEL NON UNIFORMITY WVH");
                    S.Add("NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVH");
                    S.Add("NUCLEI LOW GREY LEVEL RUN EMPHASIS WVH");
                    S.Add("NUCLEI RUN LENGTH NON UNIFORMITY WVH");
                    S.Add("NUCLEI INTENSITY WVT");
                    S.Add("NUCLEI TOTAL INTENSITY WVT");
                    S.Add("NUCLEI INTENSITY CV WVT");
                    S.Add("NUCLEI LIGHT FLUX  WVT");
                    S.Add("NUCLEI INTENSITY SD WVT");
                    S.Add("NUCLEI MAX INTENSITY WVT");
                    S.Add("NUCLEI SKEWNESS WVT");
                    S.Add("NUCLEI KURTOSIS WVT");
                    S.Add("NUCLEI ENERGY WVT");
                    S.Add("NUCLEI ENTROPY WVT");
                    S.Add("NUCLEI GREY LEVEL NON UNIFORMITY WVT");
                    S.Add("NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVT");
                    S.Add("NUCLEI LOW GREY LEVEL RUN EMPHASIS WVT");
                    S.Add("NUCLEI RUN LENGTH NON UNIFORMITY WVT");
                    S.Add("NUCLEI INTENSITY WVM");
                    S.Add("NUCLEI TOTAL INTENSITY WVM");
                    S.Add("NUCLEI INTENSITY CV WVM");
                    S.Add("NUCLEI LIGHT FLUX  WVM");
                    S.Add("NUCLEI INTENSITY SD WVM");
                    S.Add("NUCLEI MAX INTENSITY WVM");
                    S.Add("NUCLEI SKEWNESS WVM");
                    S.Add("NUCLEI KURTOSIS WVM");
                    S.Add("NUCLEI ENERGY WVM");
                    S.Add("NUCLEI ENTROPY WVM");
                    S.Add("NUCLEI GREY LEVEL NON UNIFORMITY WVM");
                    S.Add("NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVM");
                    S.Add("NUCLEI LOW GREY LEVEL RUN EMPHASIS WVM");
                    S.Add("NUCLEI RUN LENGTH NON UNIFORMITY WVM");
                    S.Add("CELLS INTENSITY (CELL) WVT");
                    S.Add("CELLS INTENSITY (CYTO) WVT");
                    S.Add("CELLS TOTAL INTENSITY (CELL)  WVT");
                    S.Add("CELLS TOTAL INTENSITY (CYTO)  WVT");
                    S.Add("CELLS INTENSITY CV (CELL)  WVT");
                    S.Add("CELLS INTENSITY CV (CYTO)  WVT");
                    S.Add("CELLS INTENSITY SPREADING  WVT");
                    S.Add("CELLS LIGHT FLUX  WVT");
                    S.Add("CELLS NUC/CYTO INTENSITY  WVT");
                    S.Add("CELLS MAX INTENSITY WVT");
                    S.Add("CELLS SKEWNESS WVT");
                    S.Add("CELLS KURTOSIS WVT");
                    S.Add("CELLS ENERGY WVT");
                    S.Add("CELLS ENTROPY WVT");
                    S.Add("CELLS GREY LEVEL NON UNIFORMITY WVT");
                    S.Add("CELLS HIGH GREY LEVEL RUN EMPHASIS WVT");
                    S.Add("CELLS LOW GREY LEVEL RUN EMPHASIS WVT");
                    S.Add("CELLS RUN LENGTH NON UNIFORMITY WVT");
                    S.Add("CELLS INTENSITY (CELL) WVM");
                    S.Add("CELLS INTENSITY (CYTO) WVM");
                    S.Add("CELLS TOTAL INTENSITY (CELL)  WVM");
                    S.Add("CELLS TOTAL INTENSITY (CYTO)  WVM");
                    S.Add("CELLS INTENSITY CV (CELL)  WVM");
                    S.Add("CELLS INTENSITY CV (CYTO)  WVM");
                    S.Add("CELLS INTENSITY SPREADING  WVM");
                    S.Add("CELLS LIGHT FLUX  WVM");
                    S.Add("CELLS NUC/CYTO INTENSITY  WVM");
                    S.Add("CELLS MAX INTENSITY WVM");
                    S.Add("CELLS SKEWNESS WVM");
                    S.Add("CELLS KURTOSIS WVM");
                    S.Add("CELLS ENERGY WVM");
                    S.Add("CELLS ENTROPY WVM");
                    S.Add("CELLS GREY LEVEL NON UNIFORMITY WVM");
                    S.Add("CELLS HIGH GREY LEVEL RUN EMPHASIS WVM");
                    S.Add("CELLS LOW GREY LEVEL RUN EMPHASIS WVM");
                    S.Add("CELLS RUN LENGTH NON UNIFORMITY WVM");
                    S.Add("CONDITION");
                    S.Add("MITOTRACKER ORG PER CELL WVM");
                    S.Add("MITOTRACKER TOTAL AREA WVM");
                }
            }
            if (today == "6")
            {
                delim = '\t';
                pth.Add(@"R:\FIVE\EXP\FIV521\2 MLModels\FIV521CellUse01_rnnorm.txt");
                pth.Add(@"R:\FIVE\EXP\FIV532\2 MLModels\FIV532CellUse01_rnnorm.txt");
                pth.Add(@"R:\FIVE\EXP\FIV537\2 MLModels\FIV537_CellsUse_V02_rnnorm.txt");
                pth.Add(@"R:\FIVE\EXP\FIV547\2 MLModels\FIV547_CellsUse v05_rnnorm.txt");
                pth.Add(@"R:\FIVE\EXP\FIV551\2 MLModels\FIV551_CellsUse_rnnorm.txt");
                {
                    S.Add("PLATEID");
                    S.Add("PLATE ID");
                    S.Add("RESULTSID");
                    S.Add("OBJECT ID");
                    S.Add("ROW");
                    S.Add("COLUMN");
                    S.Add("FOV");
                    S.Add("WELL LABEL");
                    S.Add("RAFTID");
                    S.Add("PLATE ID");
                    S.Add("WELL X UM");
                    S.Add("WELL Y UM");
                    S.Add("PLATE X UM");
                    S.Add("PLATE Y UM");
                    S.Add("IMAGENAME");
                    S.Add("CELLSPERFIELD");
                    S.Add("MLPLATEINDEX");
                    S.Add("MLCLASSLAYOUT");
                    S.Add("NUCLEI AREA WVH");
                    S.Add("NUCLEI FORM FACTOR WVH");
                    S.Add("NUCLEI ELONGATION WVH");
                    S.Add("NUCLEI COMPACTNESS  WVH");
                    S.Add("NUCLEI DISPLACEMENT WVH");
                    S.Add("NUCLEI INTENSITY WVH");
                    S.Add("NUCLEI TOTAL INTENSITY WVH");
                    S.Add("NUCLEI INTENSITY CV WVH");
                    S.Add("NUCLEI LIGHT FLUX  WVH");
                    S.Add("NUCLEI INTENSITY SD WVH");
                    S.Add("NUCLEI MAX INTENSITY WVH");
                    S.Add("NUCLEI SKEWNESS WVH");
                    S.Add("NUCLEI KURTOSIS WVH");
                    S.Add("NUCLEI ENERGY WVH");
                    S.Add("NUCLEI ENTROPY WVH");
                    S.Add("NUCLEI GREY LEVEL NON UNIFORMITY WVH");
                    S.Add("NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVH");
                    S.Add("NUCLEI LOW GREY LEVEL RUN EMPHASIS WVH");
                    S.Add("NUCLEI RUN LENGTH NON UNIFORMITY WVH");
                    S.Add("NUCLEI INTENSITY WVT");
                    S.Add("NUCLEI TOTAL INTENSITY WVT");
                    S.Add("NUCLEI INTENSITY CV WVT");
                    S.Add("NUCLEI LIGHT FLUX  WVT");
                    S.Add("NUCLEI INTENSITY SD WVT");
                    S.Add("NUCLEI MAX INTENSITY WVT");
                    S.Add("NUCLEI SKEWNESS WVT");
                    S.Add("NUCLEI KURTOSIS WVT");
                    S.Add("NUCLEI ENERGY WVT");
                    S.Add("NUCLEI ENTROPY WVT");
                    S.Add("NUCLEI GREY LEVEL NON UNIFORMITY WVT");
                    S.Add("NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVT");
                    S.Add("NUCLEI LOW GREY LEVEL RUN EMPHASIS WVT");
                    S.Add("NUCLEI RUN LENGTH NON UNIFORMITY WVT");
                    S.Add("CELLS INTENSITY (CELL) WVT");
                    S.Add("CELLS INTENSITY (CYTO) WVT");
                    S.Add("CELLS TOTAL INTENSITY (CELL)  WVT");
                    S.Add("CELLS TOTAL INTENSITY (CYTO)  WVT");
                    S.Add("CELLS INTENSITY CV (CELL)  WVT");
                    S.Add("CELLS INTENSITY CV (CYTO)  WVT");
                    S.Add("CELLS INTENSITY SPREADING  WVT");
                    S.Add("CELLS LIGHT FLUX  WVT");
                    S.Add("CELLS NUC/CYTO INTENSITY  WVT");
                    S.Add("CELLS MAX INTENSITY WVT");
                    S.Add("CELLS SKEWNESS WVT");
                    S.Add("CELLS KURTOSIS WVT");
                    S.Add("CELLS ENERGY WVT");
                    S.Add("CELLS ENTROPY WVT");
                    S.Add("CELLS GREY LEVEL NON UNIFORMITY WVT");
                    S.Add("CELLS HIGH GREY LEVEL RUN EMPHASIS WVT");
                    S.Add("CELLS LOW GREY LEVEL RUN EMPHASIS WVT");
                    S.Add("CELLS RUN LENGTH NON UNIFORMITY WVT");
                    S.Add("MITOTRACKER ORG PER CELL WVM");
                    S.Add("MITOTRACKER TOTAL AREA WVM");
                }
            }
            foreach (string FileToFix in pth)
            {
                if (!File.Exists(FileToFix)) Debugger.Break();
                RemoveColumns_Internal(FileToFix, S, delim);
            }
        }

        public void RemoveColumns_Internal(string FilePathToFix, HashSet<string> ColsToKeep, char delim)
        {
            txBx_Update.Text += Path.GetFileNameWithoutExtension(FilePathToFix) + " starting . . \r\n"; Application.DoEvents();
            StreamReader SR = new StreamReader(FilePathToFix);
            string hdr = SR.ReadLine();
            string[] headers = hdr.Split(delim);

            List<int> PositionsKeep = new List<int>();
            for (int i = 0; i < headers.Length; i++)
            {
                if (ColsToKeep.Contains(headers[i])) PositionsKeep.Add(i);
            }
            //Close it so it works on the headers
            SR.Close();
            StreamWriter SW = new StreamWriter(Path.Combine(Path.GetDirectoryName(FilePathToFix), Path.GetFileNameWithoutExtension(FilePathToFix) + " sm." + (delim == '\t' ? "txt" : "csv")));
            SR = new StreamReader(FilePathToFix);
            string t; StringBuilder SB; string[] tArr;
            while (!SR.EndOfStream)
            {
                t = SR.ReadLine(); tArr = t.Split(delim);
                SB = new StringBuilder();
                for (int i = 0; i < PositionsKeep.Count; i++)
                {
                    if (PositionsKeep[i] < tArr.Length) SB.Append(tArr[PositionsKeep[i]] + delim);
                }
                SW.WriteLine(SB);
            }
            SR.Close();
            SW.Close();
            txBx_Update.Text += "Columns Reduced . . \r\n"; Application.DoEvents();
        }
    }
}
