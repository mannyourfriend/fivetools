﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            InCellLibrary.FIVToolsMode StartingMode = InCellLibrary.FIVToolsMode.Normal;
            if (args.Length > 0)
            {
                if (int.TryParse(args[0], out int d))
                {
                    StartingMode = (InCellLibrary.FIVToolsMode)d;
                }
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain(StartingMode));
        }
    }
}
