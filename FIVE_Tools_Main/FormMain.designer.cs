﻿namespace FIVE
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.btn_LoadDB = new System.Windows.Forms.Button();
            this.bgWrk_CheckFolders = new System.ComponentModel.BackgroundWorker();
            this.txBx_HCS_Image_DestFolder = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_CreateXDCE = new System.Windows.Forms.Label();
            this.comboBox_Folder = new System.Windows.Forms.ComboBox();
            this.lbl_Notifications = new System.Windows.Forms.Label();
            this.labl_dbUpdated = new System.Windows.Forms.Label();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txBx_HCS_Image_PlateName = new System.Windows.Forms.TextBox();
            this.label_DestinationFolder = new System.Windows.Forms.Label();
            this.label_RaftImageFolder = new System.Windows.Forms.Label();
            this.chkBx_OnlyAnalyses = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btn_HCS_Analyses = new System.Windows.Forms.Button();
            this.btn_RaftCal = new System.Windows.Forms.Button();
            this.btn_CompareScans = new System.Windows.Forms.Button();
            this.btn_OpenFolder = new System.Windows.Forms.Button();
            this.btn_MultiExportXDCE = new System.Windows.Forms.Button();
            this.btnNGSAUC = new System.Windows.Forms.Button();
            this.btn_AlleleFragments = new System.Windows.Forms.Button();
            this.btnLibraryAligner = new System.Windows.Forms.Button();
            this.lbl_Combine_Az_Scores = new System.Windows.Forms.Label();
            this.lbl_XMLSettings = new System.Windows.Forms.Label();
            this.lbl_PerceptiLabs_CSV = new System.Windows.Forms.Label();
            this.btn_Bootstrap = new System.Windows.Forms.Button();
            this.btn_CMSTools = new System.Windows.Forms.Button();
            this.btn_MetadataEntry = new System.Windows.Forms.Button();
            this.btn_XCDE_Stitch = new System.Windows.Forms.Button();
            this.btn_DeleteAnalyses = new System.Windows.Forms.Button();
            this.btn_ExportTraced = new System.Windows.Forms.Button();
            this.lbl_Test_01 = new System.Windows.Forms.Label();
            this.lbl_Test_02 = new System.Windows.Forms.Label();
            this.btn_Layouts = new System.Windows.Forms.Button();
            this.label_FSdB = new System.Windows.Forms.Label();
            this.btn_NearestNeighbors = new System.Windows.Forms.Button();
            this.btn_MultiRaftExportImages = new System.Windows.Forms.Button();
            this.btn_Align2Scans = new System.Windows.Forms.Button();
            this.btn_CodeOnly = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txBx_Restrict_List = new System.Windows.Forms.TextBox();
            this.listBox_Scans = new System.Windows.Forms.ListBox();
            this.contextMenuStrip_Scans = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip_Multi = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip_CodeOnly = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.listBox_Results = new System.Windows.Forms.ListBox();
            this.bgWrk_Stitcher = new System.ComponentModel.BackgroundWorker();
            this.txBx_CellsPerField_Max = new System.Windows.Forms.TextBox();
            this.txBx_CellsPerField_Min = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkBx_Rafts = new System.Windows.Forms.CheckBox();
            this.chkBx_Today = new System.Windows.Forms.CheckBox();
            this.bgWrk_CompileCells = new System.ComponentModel.BackgroundWorker();
            this.bgWrk_ExportMasks = new System.ComponentModel.BackgroundWorker();
            this.bgWrk_RenameNormalize = new System.ComponentModel.BackgroundWorker();
            this.bgWrk_RaftMultiExport = new System.ComponentModel.BackgroundWorker();
            this.bgWork_NearestNeighbor = new System.ComponentModel.BackgroundWorker();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Enabled = false;
            this.btn_Cancel.Location = new System.Drawing.Point(290, 128);
            this.btn_Cancel.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(117, 29);
            this.btn_Cancel.TabIndex = 14;
            this.btn_Cancel.Text = "Stop / &Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(139, 178);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.ReadOnly = true;
            this.txBx_Update.Size = new System.Drawing.Size(293, 302);
            this.txBx_Update.TabIndex = 12;
            // 
            // btn_LoadDB
            // 
            this.btn_LoadDB.Location = new System.Drawing.Point(248, 95);
            this.btn_LoadDB.Margin = new System.Windows.Forms.Padding(2);
            this.btn_LoadDB.Name = "btn_LoadDB";
            this.btn_LoadDB.Size = new System.Drawing.Size(77, 29);
            this.btn_LoadDB.TabIndex = 21;
            this.btn_LoadDB.Text = "Load dB";
            this.toolTip1.SetToolTip(this.btn_LoadDB, "Load Existing Database");
            this.btn_LoadDB.UseVisualStyleBackColor = true;
            this.btn_LoadDB.Click += new System.EventHandler(this.btn_LoadFolders);
            // 
            // bgWrk_CheckFolders
            // 
            this.bgWrk_CheckFolders.WorkerReportsProgress = true;
            this.bgWrk_CheckFolders.WorkerSupportsCancellation = true;
            this.bgWrk_CheckFolders.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk_CheckFolders_DoWork);
            this.bgWrk_CheckFolders.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWrk_CheckFolders_ProgressChanged);
            this.bgWrk_CheckFolders.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk_CheckFolders_RunWorkerCompleted);
            // 
            // txBx_HCS_Image_DestFolder
            // 
            this.txBx_HCS_Image_DestFolder.Location = new System.Drawing.Point(10, 67);
            this.txBx_HCS_Image_DestFolder.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_HCS_Image_DestFolder.Name = "txBx_HCS_Image_DestFolder";
            this.txBx_HCS_Image_DestFolder.Size = new System.Drawing.Size(396, 23);
            this.txBx_HCS_Image_DestFolder.TabIndex = 22;
            this.txBx_HCS_Image_DestFolder.Text = "c:\\temp\\";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbl_CreateXDCE);
            this.panel2.Controls.Add(this.comboBox_Folder);
            this.panel2.Controls.Add(this.lbl_Notifications);
            this.panel2.Controls.Add(this.labl_dbUpdated);
            this.panel2.Controls.Add(this.btn_Refresh);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txBx_HCS_Image_PlateName);
            this.panel2.Controls.Add(this.btn_Cancel);
            this.panel2.Controls.Add(this.label_DestinationFolder);
            this.panel2.Controls.Add(this.label_RaftImageFolder);
            this.panel2.Controls.Add(this.txBx_HCS_Image_DestFolder);
            this.panel2.Controls.Add(this.btn_LoadDB);
            this.panel2.Location = new System.Drawing.Point(9, 9);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(423, 162);
            this.panel2.TabIndex = 23;
            // 
            // lbl_CreateXDCE
            // 
            this.lbl_CreateXDCE.AutoSize = true;
            this.lbl_CreateXDCE.ForeColor = System.Drawing.Color.Blue;
            this.lbl_CreateXDCE.Location = new System.Drawing.Point(304, 5);
            this.lbl_CreateXDCE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_CreateXDCE.Name = "lbl_CreateXDCE";
            this.lbl_CreateXDCE.Size = new System.Drawing.Size(96, 15);
            this.lbl_CreateXDCE.TabIndex = 47;
            this.lbl_CreateXDCE.Text = "Open Any Folder";
            this.lbl_CreateXDCE.Click += new System.EventHandler(this.lbl_CreateXDCE_Click);
            // 
            // comboBox_Folder
            // 
            this.comboBox_Folder.FormattingEnabled = true;
            this.comboBox_Folder.Location = new System.Drawing.Point(10, 23);
            this.comboBox_Folder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBox_Folder.Name = "comboBox_Folder";
            this.comboBox_Folder.Size = new System.Drawing.Size(396, 23);
            this.comboBox_Folder.TabIndex = 46;
            this.comboBox_Folder.SelectedIndexChanged += new System.EventHandler(this.comboBox_Folder_SelectedIndexChanged);
            this.comboBox_Folder.SelectionChangeCommitted += new System.EventHandler(this.comboBox_Folder_SelectionChangeCommitted);
            this.comboBox_Folder.TextChanged += new System.EventHandler(this.txBx_HCS_Image_SourceFolder_TextChanged);
            // 
            // lbl_Notifications
            // 
            this.lbl_Notifications.AutoSize = true;
            this.lbl_Notifications.ForeColor = System.Drawing.Color.Blue;
            this.lbl_Notifications.Location = new System.Drawing.Point(210, 135);
            this.lbl_Notifications.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Notifications.Name = "lbl_Notifications";
            this.lbl_Notifications.Size = new System.Drawing.Size(75, 15);
            this.lbl_Notifications.TabIndex = 45;
            this.lbl_Notifications.Text = "Notifications";
            this.lbl_Notifications.Click += new System.EventHandler(this.lbl_Notifications_Click);
            // 
            // labl_dbUpdated
            // 
            this.labl_dbUpdated.AutoSize = true;
            this.labl_dbUpdated.ForeColor = System.Drawing.Color.ForestGreen;
            this.labl_dbUpdated.Location = new System.Drawing.Point(8, 135);
            this.labl_dbUpdated.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labl_dbUpdated.Name = "labl_dbUpdated";
            this.labl_dbUpdated.Size = new System.Drawing.Size(69, 15);
            this.labl_dbUpdated.TabIndex = 31;
            this.labl_dbUpdated.Text = "dB Updated";
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Location = new System.Drawing.Point(330, 95);
            this.btn_Refresh.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(77, 29);
            this.btn_Refresh.TabIndex = 28;
            this.btn_Refresh.Text = "Refresh*";
            this.toolTip1.SetToolTip(this.btn_Refresh, "Seek out new files (Refresh).\r\nRight click to only refresh with the text in Plate" +
        "ID contains (Faster, targeted).");
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Refresh_MouseUp);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 102);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 15);
            this.label7.TabIndex = 26;
            this.label7.Text = "Plate Name";
            // 
            // txBx_HCS_Image_PlateName
            // 
            this.txBx_HCS_Image_PlateName.Location = new System.Drawing.Point(91, 97);
            this.txBx_HCS_Image_PlateName.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_HCS_Image_PlateName.Name = "txBx_HCS_Image_PlateName";
            this.txBx_HCS_Image_PlateName.Size = new System.Drawing.Size(152, 23);
            this.txBx_HCS_Image_PlateName.TabIndex = 25;
            this.txBx_HCS_Image_PlateName.Text = "NOT USED";
            // 
            // label_DestinationFolder
            // 
            this.label_DestinationFolder.AutoSize = true;
            this.label_DestinationFolder.Location = new System.Drawing.Point(15, 48);
            this.label_DestinationFolder.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_DestinationFolder.Name = "label_DestinationFolder";
            this.label_DestinationFolder.Size = new System.Drawing.Size(103, 15);
            this.label_DestinationFolder.TabIndex = 24;
            this.label_DestinationFolder.Text = "Destination Folder";
            // 
            // label_RaftImageFolder
            // 
            this.label_RaftImageFolder.AutoSize = true;
            this.label_RaftImageFolder.Location = new System.Drawing.Point(15, 5);
            this.label_RaftImageFolder.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_RaftImageFolder.Name = "label_RaftImageFolder";
            this.label_RaftImageFolder.Size = new System.Drawing.Size(131, 15);
            this.label_RaftImageFolder.TabIndex = 14;
            this.label_RaftImageFolder.Text = "InCell Images dB (Root)";
            // 
            // chkBx_OnlyAnalyses
            // 
            this.chkBx_OnlyAnalyses.AutoSize = true;
            this.chkBx_OnlyAnalyses.Location = new System.Drawing.Point(668, 10);
            this.chkBx_OnlyAnalyses.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_OnlyAnalyses.Name = "chkBx_OnlyAnalyses";
            this.chkBx_OnlyAnalyses.Size = new System.Drawing.Size(72, 19);
            this.chkBx_OnlyAnalyses.TabIndex = 27;
            this.chkBx_OnlyAnalyses.Text = "Analyses";
            this.chkBx_OnlyAnalyses.UseVisualStyleBackColor = true;
            this.chkBx_OnlyAnalyses.CheckedChanged += new System.EventHandler(this.chkBx_OnlyAnalyses_CheckedChanged);
            // 
            // btn_HCS_Analyses
            // 
            this.btn_HCS_Analyses.Enabled = false;
            this.btn_HCS_Analyses.Location = new System.Drawing.Point(1013, 455);
            this.btn_HCS_Analyses.Margin = new System.Windows.Forms.Padding(1);
            this.btn_HCS_Analyses.Name = "btn_HCS_Analyses";
            this.btn_HCS_Analyses.Size = new System.Drawing.Size(156, 27);
            this.btn_HCS_Analyses.TabIndex = 25;
            this.btn_HCS_Analyses.Text = "INCARTA Compilation";
            this.toolTip1.SetToolTip(this.btn_HCS_Analyses, "Primary way to get data out and into Spotfire");
            this.btn_HCS_Analyses.UseVisualStyleBackColor = true;
            this.btn_HCS_Analyses.Click += new System.EventHandler(this.btn_HCS_Analyses_Click);
            // 
            // btn_RaftCal
            // 
            this.btn_RaftCal.Enabled = false;
            this.btn_RaftCal.Location = new System.Drawing.Point(447, 455);
            this.btn_RaftCal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_RaftCal.Name = "btn_RaftCal";
            this.btn_RaftCal.Size = new System.Drawing.Size(68, 27);
            this.btn_RaftCal.TabIndex = 27;
            this.btn_RaftCal.Text = "Cal Chk";
            this.toolTip1.SetToolTip(this.btn_RaftCal, "Calibrate, Focus Check");
            this.btn_RaftCal.Click += new System.EventHandler(this.button_LoadRaftCalTest_Click);
            // 
            // btn_CompareScans
            // 
            this.btn_CompareScans.Enabled = false;
            this.btn_CompareScans.Location = new System.Drawing.Point(572, 455);
            this.btn_CompareScans.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_CompareScans.Name = "btn_CompareScans";
            this.btn_CompareScans.Size = new System.Drawing.Size(51, 27);
            this.btn_CompareScans.TabIndex = 37;
            this.btn_CompareScans.Text = "Comp";
            this.toolTip1.SetToolTip(this.btn_CompareScans, "Compare 2 XDCE and only show differences");
            this.btn_CompareScans.Click += new System.EventHandler(this.btn_CompareScans_Click);
            // 
            // btn_OpenFolder
            // 
            this.btn_OpenFolder.Enabled = false;
            this.btn_OpenFolder.Location = new System.Drawing.Point(519, 455);
            this.btn_OpenFolder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_OpenFolder.Name = "btn_OpenFolder";
            this.btn_OpenFolder.Size = new System.Drawing.Size(50, 27);
            this.btn_OpenFolder.TabIndex = 38;
            this.btn_OpenFolder.Text = "Open";
            this.toolTip1.SetToolTip(this.btn_OpenFolder, "Open Containing Folder");
            this.btn_OpenFolder.Click += new System.EventHandler(this.btn_OpenFolder_Click);
            // 
            // btn_MultiExportXDCE
            // 
            this.btn_MultiExportXDCE.Enabled = false;
            this.btn_MultiExportXDCE.Location = new System.Drawing.Point(729, 455);
            this.btn_MultiExportXDCE.Margin = new System.Windows.Forms.Padding(1);
            this.btn_MultiExportXDCE.Name = "btn_MultiExportXDCE";
            this.btn_MultiExportXDCE.Size = new System.Drawing.Size(65, 27);
            this.btn_MultiExportXDCE.TabIndex = 42;
            this.btn_MultiExportXDCE.Text = "mu XD";
            this.toolTip1.SetToolTip(this.btn_MultiExportXDCE, "Export multiple XDCE (Image-based dataset)");
            this.btn_MultiExportXDCE.UseVisualStyleBackColor = true;
            this.btn_MultiExportXDCE.Click += new System.EventHandler(this.MultiExportXDCE);
            // 
            // btnNGSAUC
            // 
            this.btnNGSAUC.Location = new System.Drawing.Point(934, 10);
            this.btnNGSAUC.Margin = new System.Windows.Forms.Padding(2);
            this.btnNGSAUC.Name = "btnNGSAUC";
            this.btnNGSAUC.Size = new System.Drawing.Size(82, 31);
            this.btnNGSAUC.TabIndex = 32;
            this.btnNGSAUC.Text = "NGS AUC";
            this.toolTip1.SetToolTip(this.btnNGSAUC, "AUC Calculations for UnMix");
            this.btnNGSAUC.UseVisualStyleBackColor = true;
            this.btnNGSAUC.Click += new System.EventHandler(this.button_NGSAssist_Click);
            // 
            // btn_AlleleFragments
            // 
            this.btn_AlleleFragments.Location = new System.Drawing.Point(756, 10);
            this.btn_AlleleFragments.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_AlleleFragments.Name = "btn_AlleleFragments";
            this.btn_AlleleFragments.Size = new System.Drawing.Size(113, 31);
            this.btn_AlleleFragments.TabIndex = 32;
            this.btn_AlleleFragments.Text = "Allele Fragments";
            this.toolTip1.SetToolTip(this.btn_AlleleFragments, "Figure out Allele fragments present");
            this.btn_AlleleFragments.Click += new System.EventHandler(this.btn_AlleleFragments_Click);
            // 
            // btnLibraryAligner
            // 
            this.btnLibraryAligner.Location = new System.Drawing.Point(875, 10);
            this.btnLibraryAligner.Margin = new System.Windows.Forms.Padding(2);
            this.btnLibraryAligner.Name = "btnLibraryAligner";
            this.btnLibraryAligner.Size = new System.Drawing.Size(54, 31);
            this.btnLibraryAligner.TabIndex = 43;
            this.btnLibraryAligner.Text = "LA";
            this.toolTip1.SetToolTip(this.btnLibraryAligner, "Library Aligner");
            this.btnLibraryAligner.UseVisualStyleBackColor = true;
            this.btnLibraryAligner.Click += new System.EventHandler(this.btnLibraryAligner_Click);
            // 
            // lbl_Combine_Az_Scores
            // 
            this.lbl_Combine_Az_Scores.AutoSize = true;
            this.lbl_Combine_Az_Scores.ForeColor = System.Drawing.Color.Blue;
            this.lbl_Combine_Az_Scores.Location = new System.Drawing.Point(872, 47);
            this.lbl_Combine_Az_Scores.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Combine_Az_Scores.Name = "lbl_Combine_Az_Scores";
            this.lbl_Combine_Az_Scores.Size = new System.Drawing.Size(61, 15);
            this.lbl_Combine_Az_Scores.TabIndex = 44;
            this.lbl_Combine_Az_Scores.Text = "Compilers";
            this.toolTip1.SetToolTip(this.lbl_Combine_Az_Scores, "Model Compilers (Azure focused)");
            this.lbl_Combine_Az_Scores.Click += new System.EventHandler(this.lbl_Combine_Az_Scores_Click);
            // 
            // lbl_XMLSettings
            // 
            this.lbl_XMLSettings.AutoSize = true;
            this.lbl_XMLSettings.ForeColor = System.Drawing.Color.Blue;
            this.lbl_XMLSettings.Location = new System.Drawing.Point(13, 178);
            this.lbl_XMLSettings.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_XMLSettings.Name = "lbl_XMLSettings";
            this.lbl_XMLSettings.Size = new System.Drawing.Size(76, 15);
            this.lbl_XMLSettings.TabIndex = 47;
            this.lbl_XMLSettings.Text = "XML Settings";
            this.toolTip1.SetToolTip(this.lbl_XMLSettings, "After the settings open, Close FIVTools. Then save these and restart FIV Tools (i" +
        "f you make the changes first, they will be overwritten when FIVTools closes)");
            this.lbl_XMLSettings.Click += new System.EventHandler(this.lbl_XMLSettings_Click);
            // 
            // lbl_PerceptiLabs_CSV
            // 
            this.lbl_PerceptiLabs_CSV.AutoSize = true;
            this.lbl_PerceptiLabs_CSV.ForeColor = System.Drawing.Color.Blue;
            this.lbl_PerceptiLabs_CSV.Location = new System.Drawing.Point(941, 47);
            this.lbl_PerceptiLabs_CSV.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_PerceptiLabs_CSV.Name = "lbl_PerceptiLabs_CSV";
            this.lbl_PerceptiLabs_CSV.Size = new System.Drawing.Size(59, 15);
            this.lbl_PerceptiLabs_CSV.TabIndex = 48;
            this.lbl_PerceptiLabs_CSV.Text = "TF Deploy";
            this.toolTip1.SetToolTip(this.lbl_PerceptiLabs_CSV, "Perceptilabs CSV Generator, Tensor Flow Deploy");
            this.lbl_PerceptiLabs_CSV.Click += new System.EventHandler(this.lbl_PerceptiLabs_CSV_Click);
            // 
            // btn_Bootstrap
            // 
            this.btn_Bootstrap.Location = new System.Drawing.Point(16, 72);
            this.btn_Bootstrap.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Bootstrap.Name = "btn_Bootstrap";
            this.btn_Bootstrap.Size = new System.Drawing.Size(88, 30);
            this.btn_Bootstrap.TabIndex = 47;
            this.btn_Bootstrap.Text = "p Values ..";
            this.toolTip1.SetToolTip(this.btn_Bootstrap, "Does Bootstrap on a data set to get p-values");
            this.btn_Bootstrap.UseVisualStyleBackColor = true;
            this.btn_Bootstrap.Click += new System.EventHandler(this.btn_Bootstrap_Click);
            // 
            // btn_CMSTools
            // 
            this.btn_CMSTools.Location = new System.Drawing.Point(16, 105);
            this.btn_CMSTools.Margin = new System.Windows.Forms.Padding(2);
            this.btn_CMSTools.Name = "btn_CMSTools";
            this.btn_CMSTools.Size = new System.Drawing.Size(88, 30);
            this.btn_CMSTools.TabIndex = 31;
            this.btn_CMSTools.Text = "CMS Tools ..";
            this.toolTip1.SetToolTip(this.btn_CMSTools, "Alex Yenkin\'s tools to process the images that are taken from the CMS Air systems" +
        " themselves (Custom Stitch, Marker Stitch, etc)");
            this.btn_CMSTools.UseVisualStyleBackColor = true;
            this.btn_CMSTools.Click += new System.EventHandler(this.btn_CMSTools_click);
            // 
            // btn_MetadataEntry
            // 
            this.btn_MetadataEntry.Location = new System.Drawing.Point(16, 39);
            this.btn_MetadataEntry.Margin = new System.Windows.Forms.Padding(1);
            this.btn_MetadataEntry.Name = "btn_MetadataEntry";
            this.btn_MetadataEntry.Size = new System.Drawing.Size(88, 30);
            this.btn_MetadataEntry.TabIndex = 30;
            this.btn_MetadataEntry.Text = "Metadata ..";
            this.toolTip1.SetToolTip(this.btn_MetadataEntry, "Opens the Metadata / Data Lineage Tool");
            this.btn_MetadataEntry.UseVisualStyleBackColor = true;
            this.btn_MetadataEntry.Click += new System.EventHandler(this.btn_MetadataEntry_Click);
            // 
            // btn_XCDE_Stitch
            // 
            this.btn_XCDE_Stitch.Enabled = false;
            this.btn_XCDE_Stitch.Location = new System.Drawing.Point(677, 455);
            this.btn_XCDE_Stitch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_XCDE_Stitch.Name = "btn_XCDE_Stitch";
            this.btn_XCDE_Stitch.Size = new System.Drawing.Size(50, 27);
            this.btn_XCDE_Stitch.TabIndex = 31;
            this.btn_XCDE_Stitch.Text = "Stitch";
            this.toolTip1.SetToolTip(this.btn_XCDE_Stitch, "Stitch all the images of a plate together (gives a gigantic montage)");
            this.btn_XCDE_Stitch.UseVisualStyleBackColor = true;
            this.btn_XCDE_Stitch.Click += new System.EventHandler(this.StitchImages_click);
            // 
            // btn_DeleteAnalyses
            // 
            this.btn_DeleteAnalyses.Enabled = false;
            this.btn_DeleteAnalyses.Location = new System.Drawing.Point(919, 455);
            this.btn_DeleteAnalyses.Margin = new System.Windows.Forms.Padding(1);
            this.btn_DeleteAnalyses.Name = "btn_DeleteAnalyses";
            this.btn_DeleteAnalyses.Size = new System.Drawing.Size(91, 27);
            this.btn_DeleteAnalyses.TabIndex = 32;
            this.btn_DeleteAnalyses.Text = "Del Analyses";
            this.toolTip1.SetToolTip(this.btn_DeleteAnalyses, "Delete an analysis above");
            this.btn_DeleteAnalyses.UseVisualStyleBackColor = true;
            this.btn_DeleteAnalyses.Click += new System.EventHandler(this.btn_DeleteAnalyses_Click);
            // 
            // btn_ExportTraced
            // 
            this.btn_ExportTraced.Enabled = false;
            this.btn_ExportTraced.Location = new System.Drawing.Point(855, 455);
            this.btn_ExportTraced.Margin = new System.Windows.Forms.Padding(1);
            this.btn_ExportTraced.Name = "btn_ExportTraced";
            this.btn_ExportTraced.Size = new System.Drawing.Size(62, 27);
            this.btn_ExportTraced.TabIndex = 41;
            this.btn_ExportTraced.Text = "Traced";
            this.toolTip1.SetToolTip(this.btn_ExportTraced, "Export the traced version of images (to then pull into Spotfire)");
            this.btn_ExportTraced.UseVisualStyleBackColor = true;
            this.btn_ExportTraced.Click += new System.EventHandler(this.ExportTracedImages);
            // 
            // lbl_Test_01
            // 
            this.lbl_Test_01.AutoSize = true;
            this.lbl_Test_01.ForeColor = System.Drawing.Color.Blue;
            this.lbl_Test_01.Location = new System.Drawing.Point(797, 463);
            this.lbl_Test_01.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Test_01.Name = "lbl_Test_01";
            this.lbl_Test_01.Size = new System.Drawing.Size(21, 15);
            this.lbl_Test_01.TabIndex = 46;
            this.lbl_Test_01.Text = "A1";
            this.toolTip1.SetToolTip(this.lbl_Test_01, "For InCell-Leica Registration Testing");
            this.lbl_Test_01.Click += new System.EventHandler(this.lbl_Test_01_Click);
            // 
            // lbl_Test_02
            // 
            this.lbl_Test_02.AutoSize = true;
            this.lbl_Test_02.ForeColor = System.Drawing.Color.Blue;
            this.lbl_Test_02.Location = new System.Drawing.Point(825, 463);
            this.lbl_Test_02.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Test_02.Name = "lbl_Test_02";
            this.lbl_Test_02.Size = new System.Drawing.Size(21, 15);
            this.lbl_Test_02.TabIndex = 47;
            this.lbl_Test_02.Text = "A2";
            this.toolTip1.SetToolTip(this.lbl_Test_02, "For InCell-Leica Registration Testing");
            this.lbl_Test_02.Click += new System.EventHandler(this.lbl_Test_02_Click);
            // 
            // btn_Layouts
            // 
            this.btn_Layouts.Location = new System.Drawing.Point(16, 6);
            this.btn_Layouts.Margin = new System.Windows.Forms.Padding(1);
            this.btn_Layouts.Name = "btn_Layouts";
            this.btn_Layouts.Size = new System.Drawing.Size(88, 30);
            this.btn_Layouts.TabIndex = 49;
            this.btn_Layouts.Text = "Layouts ..";
            this.toolTip1.SetToolTip(this.btn_Layouts, "Setup Plate Layouts and Biomek Scripts");
            this.btn_Layouts.UseVisualStyleBackColor = true;
            this.btn_Layouts.Click += new System.EventHandler(this.btn_Layouts_Click);
            // 
            // label_FSdB
            // 
            this.label_FSdB.AutoSize = true;
            this.label_FSdB.ForeColor = System.Drawing.Color.Blue;
            this.label_FSdB.Location = new System.Drawing.Point(772, 47);
            this.label_FSdB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_FSdB.Name = "label_FSdB";
            this.label_FSdB.Size = new System.Drawing.Size(36, 15);
            this.label_FSdB.TabIndex = 49;
            this.label_FSdB.Text = "FS dB";
            this.toolTip1.SetToolTip(this.label_FSdB, "Model Compilers (Azure focused)");
            this.label_FSdB.Click += new System.EventHandler(this.label_FSdB_Click);
            // 
            // btn_NearestNeighbors
            // 
            this.btn_NearestNeighbors.Location = new System.Drawing.Point(62, 167);
            this.btn_NearestNeighbors.Margin = new System.Windows.Forms.Padding(2);
            this.btn_NearestNeighbors.Name = "btn_NearestNeighbors";
            this.btn_NearestNeighbors.Size = new System.Drawing.Size(54, 30);
            this.btn_NearestNeighbors.TabIndex = 50;
            this.btn_NearestNeighbors.Text = "NearN";
            this.toolTip1.SetToolTip(this.btn_NearestNeighbors, "Adds Nearest Neighbors information to Cell Based Data Table");
            this.btn_NearestNeighbors.UseVisualStyleBackColor = true;
            this.btn_NearestNeighbors.Click += new System.EventHandler(this.btn_NearestNeighbors_Click);
            // 
            // btn_MultiRaftExportImages
            // 
            this.btn_MultiRaftExportImages.Enabled = false;
            this.btn_MultiRaftExportImages.Location = new System.Drawing.Point(624, 455);
            this.btn_MultiRaftExportImages.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_MultiRaftExportImages.Name = "btn_MultiRaftExportImages";
            this.btn_MultiRaftExportImages.Size = new System.Drawing.Size(51, 27);
            this.btn_MultiRaftExportImages.TabIndex = 50;
            this.btn_MultiRaftExportImages.Text = "mRaft";
            this.toolTip1.SetToolTip(this.btn_MultiRaftExportImages, "Equivalent to opening CalCheck, then saying Export Annotated Rafts, but for multi" +
        "ple scans");
            this.btn_MultiRaftExportImages.Click += new System.EventHandler(this.btn_MultiRaftExportImages_Click);
            // 
            // btn_Align2Scans
            // 
            this.btn_Align2Scans.Location = new System.Drawing.Point(4, 167);
            this.btn_Align2Scans.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Align2Scans.Name = "btn_Align2Scans";
            this.btn_Align2Scans.Size = new System.Drawing.Size(54, 30);
            this.btn_Align2Scans.TabIndex = 51;
            this.btn_Align2Scans.Text = "Align 2";
            this.toolTip1.SetToolTip(this.btn_Align2Scans, "Looks at 2 compiled files and tries to align cells from the 2 scans per FOV. It e" +
        "xports a table that has the original cell identity an a new \"ConnectID\" that is " +
        "the same if two cells are aligned.");
            this.btn_Align2Scans.UseVisualStyleBackColor = true;
            this.btn_Align2Scans.Click += new System.EventHandler(this.btn_Align2Scans_Click);
            // 
            // btn_CodeOnly
            // 
            this.btn_CodeOnly.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_CodeOnly.Location = new System.Drawing.Point(15, 240);
            this.btn_CodeOnly.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_CodeOnly.Name = "btn_CodeOnly";
            this.btn_CodeOnly.Size = new System.Drawing.Size(88, 30);
            this.btn_CodeOnly.TabIndex = 0;
            this.btn_CodeOnly.Text = "Code Only";
            this.btn_CodeOnly.UseVisualStyleBackColor = false;
            this.btn_CodeOnly.Click += new System.EventHandler(this.btn_CodeOnly_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btn_Align2Scans);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_NearestNeighbors);
            this.panel1.Controls.Add(this.btn_Layouts);
            this.panel1.Controls.Add(this.btn_Bootstrap);
            this.panel1.Controls.Add(this.btn_CMSTools);
            this.panel1.Controls.Add(this.btn_MetadataEntry);
            this.panel1.Controls.Add(this.btn_CodeOnly);
            this.panel1.Location = new System.Drawing.Point(9, 198);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(123, 281);
            this.panel1.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 149);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 48;
            this.label1.Text = "Post Compile";
            // 
            // txBx_Restrict_List
            // 
            this.txBx_Restrict_List.Location = new System.Drawing.Point(554, 42);
            this.txBx_Restrict_List.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_Restrict_List.Name = "txBx_Restrict_List";
            this.txBx_Restrict_List.Size = new System.Drawing.Size(194, 23);
            this.txBx_Restrict_List.TabIndex = 28;
            this.txBx_Restrict_List.TextChanged += new System.EventHandler(this.txBx_Restrict_List_TextChanged);
            this.txBx_Restrict_List.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txBx_Restrict_List_KeyUp);
            // 
            // listBox_Scans
            // 
            this.listBox_Scans.FormattingEnabled = true;
            this.listBox_Scans.ItemHeight = 15;
            this.listBox_Scans.Location = new System.Drawing.Point(447, 70);
            this.listBox_Scans.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.listBox_Scans.Name = "listBox_Scans";
            this.listBox_Scans.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox_Scans.Size = new System.Drawing.Size(302, 379);
            this.listBox_Scans.TabIndex = 29;
            this.listBox_Scans.SelectedIndexChanged += new System.EventHandler(this.listBox_Scans_SelectedIndexChanged);
            this.listBox_Scans.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox_Scans_MouseDoubleClick);
            this.listBox_Scans.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_Scans_MouseDown);
            // 
            // contextMenuStrip_Scans
            // 
            this.contextMenuStrip_Scans.Name = "contextMenuStrip_Scans";
            this.contextMenuStrip_Scans.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip_Multi
            // 
            this.contextMenuStrip_Multi.Name = "contextMenuStrip_Multi";
            this.contextMenuStrip_Multi.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip_CodeOnly
            // 
            this.contextMenuStrip_CodeOnly.Name = "contextMenuStrip_CodeOnly";
            this.contextMenuStrip_CodeOnly.Size = new System.Drawing.Size(61, 4);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(446, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 15);
            this.label2.TabIndex = 29;
            this.label2.Text = "PlateID Contains:";
            // 
            // listBox_Results
            // 
            this.listBox_Results.FormattingEnabled = true;
            this.listBox_Results.ItemHeight = 15;
            this.listBox_Results.Location = new System.Drawing.Point(756, 70);
            this.listBox_Results.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.listBox_Results.Name = "listBox_Results";
            this.listBox_Results.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox_Results.Size = new System.Drawing.Size(412, 379);
            this.listBox_Results.TabIndex = 30;
            this.listBox_Results.SelectedIndexChanged += new System.EventHandler(this.listBox_Results_SelectedIndexChanged);
            // 
            // bgWrk_Stitcher
            // 
            this.bgWrk_Stitcher.WorkerReportsProgress = true;
            this.bgWrk_Stitcher.WorkerSupportsCancellation = true;
            this.bgWrk_Stitcher.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk_Stitcher_DoWork);
            this.bgWrk_Stitcher.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk_Stitcher_RunWorkerCompleted);
            // 
            // txBx_CellsPerField_Max
            // 
            this.txBx_CellsPerField_Max.Location = new System.Drawing.Point(1112, 42);
            this.txBx_CellsPerField_Max.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_CellsPerField_Max.Name = "txBx_CellsPerField_Max";
            this.txBx_CellsPerField_Max.Size = new System.Drawing.Size(55, 23);
            this.txBx_CellsPerField_Max.TabIndex = 33;
            this.txBx_CellsPerField_Max.Text = "1000";
            // 
            // txBx_CellsPerField_Min
            // 
            this.txBx_CellsPerField_Min.Location = new System.Drawing.Point(1113, 9);
            this.txBx_CellsPerField_Min.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_CellsPerField_Min.Name = "txBx_CellsPerField_Min";
            this.txBx_CellsPerField_Min.Size = new System.Drawing.Size(55, 23);
            this.txBx_CellsPerField_Min.TabIndex = 34;
            this.txBx_CellsPerField_Min.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1020, 13);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 35;
            this.label3.Text = "Min cells/field";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1020, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 15);
            this.label4.TabIndex = 36;
            this.label4.Text = "Max cells/field";
            // 
            // chkBx_Rafts
            // 
            this.chkBx_Rafts.AutoSize = true;
            this.chkBx_Rafts.Location = new System.Drawing.Point(559, 10);
            this.chkBx_Rafts.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_Rafts.Name = "chkBx_Rafts";
            this.chkBx_Rafts.Size = new System.Drawing.Size(52, 19);
            this.chkBx_Rafts.TabIndex = 39;
            this.chkBx_Rafts.Text = "Rafts";
            this.chkBx_Rafts.UseVisualStyleBackColor = true;
            this.chkBx_Rafts.CheckedChanged += new System.EventHandler(this.chkBx_OnlyAnalyses_CheckedChanged);
            // 
            // chkBx_Today
            // 
            this.chkBx_Today.AutoSize = true;
            this.chkBx_Today.Location = new System.Drawing.Point(449, 10);
            this.chkBx_Today.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_Today.Name = "chkBx_Today";
            this.chkBx_Today.Size = new System.Drawing.Size(57, 19);
            this.chkBx_Today.TabIndex = 40;
            this.chkBx_Today.Text = "Today";
            this.chkBx_Today.UseVisualStyleBackColor = true;
            this.chkBx_Today.CheckedChanged += new System.EventHandler(this.chkBx_OnlyAnalyses_CheckedChanged);
            // 
            // bgWrk_CompileCells
            // 
            this.bgWrk_CompileCells.WorkerReportsProgress = true;
            this.bgWrk_CompileCells.WorkerSupportsCancellation = true;
            this.bgWrk_CompileCells.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk_CompileCells_DoWork);
            this.bgWrk_CompileCells.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWrk_CompileCells_ProgressChanged);
            this.bgWrk_CompileCells.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk_CompileCells_RunWorkerCompleted);
            // 
            // bgWrk_ExportMasks
            // 
            this.bgWrk_ExportMasks.WorkerReportsProgress = true;
            this.bgWrk_ExportMasks.WorkerSupportsCancellation = true;
            this.bgWrk_ExportMasks.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk_ExportMasks_DoWork);
            this.bgWrk_ExportMasks.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWrk_ExportMasks_ProgressChanged);
            this.bgWrk_ExportMasks.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk_ExportMasks_RunWorkerCompleted);
            // 
            // bgWrk_RenameNormalize
            // 
            this.bgWrk_RenameNormalize.WorkerReportsProgress = true;
            this.bgWrk_RenameNormalize.WorkerSupportsCancellation = true;
            this.bgWrk_RenameNormalize.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk_RenameNormalize_DoWork);
            this.bgWrk_RenameNormalize.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWrk_RenameNormalize_ProgressChanged);
            this.bgWrk_RenameNormalize.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk_RenameNormalize_RunWorkerCompleted);
            // 
            // bgWrk_RaftMultiExport
            // 
            this.bgWrk_RaftMultiExport.WorkerReportsProgress = true;
            this.bgWrk_RaftMultiExport.WorkerSupportsCancellation = true;
            this.bgWrk_RaftMultiExport.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk_RaftMultiExport_DoWork);
            this.bgWrk_RaftMultiExport.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWrk_RaftMultiExport_ProgressChanged);
            this.bgWrk_RaftMultiExport.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk_RaftMultiExport_RunWorkerCompleted);
            // 
            // bgWork_NearestNeighbor
            // 
            this.bgWork_NearestNeighbor.WorkerReportsProgress = true;
            this.bgWork_NearestNeighbor.WorkerSupportsCancellation = true;
            this.bgWork_NearestNeighbor.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWork_NearestNeighbor_DoWork);
            this.bgWork_NearestNeighbor.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWork_NearestNeighbor_ProgressChanged);
            this.bgWork_NearestNeighbor.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWork_NearestNeighbor_RunWorkerCompleted);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1181, 488);
            this.Controls.Add(this.btn_MultiRaftExportImages);
            this.Controls.Add(this.label_FSdB);
            this.Controls.Add(this.lbl_PerceptiLabs_CSV);
            this.Controls.Add(this.lbl_XMLSettings);
            this.Controls.Add(this.lbl_Test_02);
            this.Controls.Add(this.lbl_Test_01);
            this.Controls.Add(this.lbl_Combine_Az_Scores);
            this.Controls.Add(this.btnLibraryAligner);
            this.Controls.Add(this.btn_AlleleFragments);
            this.Controls.Add(this.btnNGSAUC);
            this.Controls.Add(this.btn_MultiExportXDCE);
            this.Controls.Add(this.btn_ExportTraced);
            this.Controls.Add(this.chkBx_Today);
            this.Controls.Add(this.chkBx_Rafts);
            this.Controls.Add(this.btn_OpenFolder);
            this.Controls.Add(this.btn_CompareScans);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txBx_CellsPerField_Min);
            this.Controls.Add(this.txBx_CellsPerField_Max);
            this.Controls.Add(this.btn_DeleteAnalyses);
            this.Controls.Add(this.btn_XCDE_Stitch);
            this.Controls.Add(this.listBox_Results);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox_Scans);
            this.Controls.Add(this.chkBx_OnlyAnalyses);
            this.Controls.Add(this.txBx_Restrict_List);
            this.Controls.Add(this.btn_RaftCal);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_HCS_Analyses);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.txBx_Update);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormMain";
            this.Text = "FIVE Tools";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_LoadDB;
        private System.ComponentModel.BackgroundWorker bgWrk_CheckFolders;
        private System.Windows.Forms.TextBox txBx_HCS_Image_DestFolder;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label_DestinationFolder;
        private System.Windows.Forms.Label label_RaftImageFolder;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txBx_HCS_Image_PlateName;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox chkBx_OnlyAnalyses;
        private System.Windows.Forms.Button btn_CodeOnly;
        private System.Windows.Forms.Button btn_HCS_Analyses;
        private System.Windows.Forms.Button btn_RaftCal;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txBx_Restrict_List;
        private System.Windows.Forms.Button btn_Refresh;
        private System.Windows.Forms.ListBox listBox_Scans;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_MetadataEntry;
        private System.Windows.Forms.ListBox listBox_Results;
        private System.Windows.Forms.Label labl_dbUpdated;
        private System.Windows.Forms.Button btn_XCDE_Stitch;
        private System.Windows.Forms.Button btn_CMSTools;
        private System.ComponentModel.BackgroundWorker bgWrk_Stitcher;
        private System.Windows.Forms.Button btn_DeleteAnalyses;
        private System.Windows.Forms.TextBox txBx_CellsPerField_Max;
        private System.Windows.Forms.TextBox txBx_CellsPerField_Min;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_CompareScans;
        private System.Windows.Forms.Button btn_OpenFolder;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip_Scans;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip_Multi;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip_CodeOnly;
        private System.Windows.Forms.CheckBox chkBx_Rafts;
        private System.Windows.Forms.CheckBox chkBx_Today;
        private System.Windows.Forms.Button btn_ExportTraced;
        private System.Windows.Forms.Button btn_MultiExportXDCE;
        private System.ComponentModel.BackgroundWorker bgWrk_CompileCells;
        private System.Windows.Forms.Button btnNGSAUC;
        private System.Windows.Forms.Button btn_AlleleFragments;
        private System.Windows.Forms.Button btnLibraryAligner;
        private System.Windows.Forms.Label lbl_Combine_Az_Scores;
        private System.Windows.Forms.Label lbl_Notifications;
        private System.Windows.Forms.Label lbl_Test_01;
        private System.Windows.Forms.Label lbl_Test_02;
        private System.ComponentModel.BackgroundWorker bgWrk_ExportMasks;
        private System.ComponentModel.BackgroundWorker bgWrk_RenameNormalize;
        private System.Windows.Forms.ComboBox comboBox_Folder;
        private System.Windows.Forms.Label lbl_XMLSettings;
        private System.Windows.Forms.Label lbl_CreateXDCE;
        private System.Windows.Forms.Button btn_Bootstrap;
        private System.Windows.Forms.Label lbl_PerceptiLabs_CSV;
        private System.Windows.Forms.Button btn_Layouts;
        private System.ComponentModel.BackgroundWorker bgWrk_RaftMultiExport;
        private System.Windows.Forms.Label label_FSdB;
        private System.Windows.Forms.Button btn_NearestNeighbors;
        private System.ComponentModel.BackgroundWorker bgWork_NearestNeighbor;
        private System.Windows.Forms.Button btn_MultiRaftExportImages;
        private System.Windows.Forms.Button btn_Align2Scans;
        private System.Windows.Forms.Label label1;
    }
}

