﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;


//Original program made by Jack in GO LANG, C# adaptation made by Bay 6/23/2021
namespace FIVE
{
    public class GenerateMasksClass
    {
        public static string SubPath_Exp = "R:\\FIVE\\EXP\\";
        public static string SubPath_CMS = "R:\\dB\\CMS_Machine\\";
        public static string TemplateName = "Load From File Template2.xlsx";
        public static string TemplatePath { get => Path.Combine(SubPath_CMS, TemplateName); }

        public bool TemplateLoaded = false;
        private Excel.Application excelApp;
        private Excel.Workbook template;
        private Excel.Worksheet worksheet;

        public string[] pickListArray;
        public char Separator = '\t';
        public string cmsPickDir;
        public Dictionary<string, int> dict = new Dictionary<string, int>();
        public string plateID;
        public string currentTimeString;
        public StreamWriter SW;
        public string FIVNumber;
        string folderPath;

        public List<string> dictValues = new List<string>();


        public GenerateMasksClass()
        {
            try
            {
                excelApp = new Excel.Application();
                template = excelApp.Workbooks.Open(TemplatePath);
                worksheet = (Excel.Worksheet)template.Worksheets.get_Item(1);
                TemplateLoaded = true;
            }
            catch
            {
                TemplateLoaded = false;
            }
            System.DateTime currentTime = DateTime.Now;
            currentTimeString = currentTime.ToString().Replace("/", "").Replace(":", "").Replace(" ", "");
            //the following are different values that we want to see if the dictionary.
            //right now, there are more than we really need, so if we want to remove anything from the dictionary, remove it and change the code where it is used
            dictValues.Add("raftid");
            dictValues.Add("mlplateindex");
            dictValues.Add("mlclasslayout");
            dictValues.Add("rank");
            dictValues.Add("modelatpick");
            dictValues.Add("picksubplate");
            dictValues.Add("cells per raft");
        }

        public string CSVFileCheck(string FileName, string FIVin)
        {
            FIVNumber = FIVin;
            string risExp = Path.Combine(SubPath_Exp, FIVNumber); // "3 PickRelease\\"
            //string pickPath = Path.Combine(risExp, FileName + ".txt"); string ngsPath = Path.Combine(@"\\storage1.ris.wustl.edu\wbuchser\Active\FIVE\EXP\", FIVNumber);
            //I have no idea why, but I could not make cmsPickDir get the correct string using Path.Combin, so I gave up and just did string addition
            //string cmsPickDir = Path.Combine(SubPath_CMS, "\\PickLists\\", FIVNumber);
            cmsPickDir = SubPath_CMS + "PickLists\\" + FIVNumber;

            if (!Directory.Exists(cmsPickDir)) Directory.CreateDirectory(cmsPickDir);

            pickListArray = File.ReadAllLines(FileName);
            string[] headers = pickListArray[0].Split(Separator);
            headers = headers.Select(x => x.Trim().ToLower()).ToArray(); ////for (int i = 0; i < headers.Length; i++) headers[i] = headers[i].Trim().ToLower(); //Old Style
            for (int i = 0; i < headers.Length; i++) dict.Add(headers[i], i);
            bool dictComplete = true;
            string dictCheck = CheckDict();
            if (!(dictCheck == "")) dictComplete = false;

            //there was a for loop outlined here in Jack's code. From what I could tell, it was pointless so I did not finish implementing it and comented out the for loop. 
            //if things start to go wrong BETWEEN excel files, this may be a place to look and/or ask jack for questions
            //for (int i = 0; i < length; i++)

            //can add a funtion comparing unique and current rafts to check for duplicates as was in jack's code, however I don't think this is neccsisary.
            pickListArray.Append<string>("-1");
            CreateExcel(cmsPickDir);

            string[] firstLine = pickListArray[1].Split(Separator);
            plateID = firstLine[dict["mlplateindex"]].Trim();
            string DestPlate = "A" + plateID + "P" + firstLine[dict["picksubplate"]].Trim();
            string arraySubDir = Path.Combine(cmsPickDir, @"\A" + plateID);

            Directory.CreateDirectory(arraySubDir);

            string ngsPlateMapFolder = Path.Combine(risExp, @"4 Mapping\Prep Maps\");
            if (!Directory.Exists(ngsPlateMapFolder)) Directory.CreateDirectory(ngsPlateMapFolder);
            //string ngsPlateMap = Path.Combine(ngsPlateMapFolder, FIVNumber + "_" + "NGSMap_Array" + plateID[0] + "_" + currentTimeString + ".txt"); //Old before 8/30/2021
            string ngsPlateMap; int Extra = 1;
            do
            {
                ngsPlateMap = Path.Combine(ngsPlateMapFolder, FIVNumber + DestPlate +" NGSMap " + (Extra++).ToString("000") + ".txt");
            } 
            while (File.Exists(ngsPlateMap));
            try
            {
                SW = File.CreateText(ngsPlateMap);
            }
            catch
            {   //stream writer doesn't work, maybe because the File.Create is keeping the ngsPlateMap open
                template.Close();
                return "prep map could not be made";
            }

            SW.WriteLine("FIVNumber" + "\t" + "BlabLongID" + "\t" + "ArrayNumber" + "\t" + "PickPlate" + "\t" + "mlClassLayout" + "\t" + "RaftID" + "\t" + "PredictedClass" + "\t" + "Score" + "\t" + "CellsPerRaft" + "\t" + "ArrayID" + "\t" + "Well" + "\t" + "ModelAtPick"); //may need to make a fail state for above, was in Jack's code

            //jack's comment below
            //For Scope because we dont know excel file type. This is easier than using pointers
            if (dictComplete)
                return "success";
            else
                return "the following values are not found in the template provided: " + dictCheck + ". If you wish to continue, missing values will be replaced by the value in the first cell per line.";
        }

        public string CreateCSVFiles() { 
            //Excel.Workbook cmsPlateFile = template;

            int cordCount = 0;
            string currentPlate;
            string prev = "-100";
            int Row = 3;
            string[] lines;
            
            //main loop of the function. Iterates through picklist array
            //original name by jack is MasterOfRaftality
            for (int j = 1; j < pickListArray.Length + 1; j++)
            {
                folderPath = cmsPickDir + "\\A" + plateID + "\\";
                string filePath = (cmsPickDir + "\\A" + plateID + "\\CMSPlateMap__A" + plateID + "_P" + prev + "_" + currentTimeString + ".xlsx");

                //this is the state for the last loop of picklistarray
                if (j == pickListArray.Length)
                {
                    //currentPlate = "This Isnt Real";
                    //lines = pickListArray[j].Split(Separator);
                    //string filePath = (cmsPickDir + "\\A" + plateID + "\\CMSPlateMap__A" + plateID + "_P" + prev + "_" + currentTimeString + ".xlsx");

                    if (!ExcelSaveAs(filePath))
                    {
                        SW.Close();
                        template.Close(false);
                        return "cannot save excel file due to incorrect path";
                    }
                    break;
                }

                lines = pickListArray[j].Split(Separator);
                currentPlate = lines[dict["picksubplate"]];

                if (j >= 0)
                {
                    
                    if (currentPlate != prev)
                    {
                        Row = 3;
                        
                        //this case is for when the current excel gets full, then it coninues but saves an excel file before moving to the next pick plate
                        if (prev != "-100")
                        {
                            //string filePath = (cmsPickDir + "\\A" + plateID + "\\CMSPlateMap__A" + plateID + "_P" + prev + "_" + currentTimeString + ".xlsx");
                            if (!ExcelSaveAs(filePath))
                            {
                                SW.Close(); template.Close();
                                return "cannot save excel file due to incorrect path";
                            }
                        }
                    } else
                    {
                        if (Row >= 99) //Means we have more than 96 things mapped to a plate, return and report this
                        {
                            SW.Close();
                            template.Close(false);
                            return "Too many rafts in each plate, please check the pick list.";
                        }
                    }

                    string s = lines[dict["raftid"]].Trim();
                    string arrayRow = s.Remove(2, s.Length - 2).Trim();
                    string arrayColumn = s.Remove(0, 2).Trim();

                    worksheet.Cells[Row, 2] = arrayRow;
                    worksheet.Cells[Row, 3] = arrayColumn;
                    string WellRow = ((Excel.Range)worksheet.Cells[Row, 4]).Value2.ToString();
                    string WellCol = ((Excel.Range)worksheet.Cells[Row, 5]).Value2.ToString();
                    //string well = arrayRow + arrayColumn;
                    //string ID = FIVNumber + "A" + lines[dict["mlPlateIndex"]] + "P" + currentPlate;
                    cordCount++; Row++;
                    prev = currentPlate;
                    SW.WriteLine(FIVNumber + "\t" + FIVNumber + "A" + plateID + "P" + prev + "\t" + lines[dict["mlplateindex"]] + "\t" + currentPlate + "\t" + lines[dict["mlclasslayout"]] + "\t" + lines[dict["raftid"]] + "\t" + lines[dict["mlclasslayout"]] + "\t" + lines[dict["rank"]] + "\t" + lines[dict["cells per raft"]] + "\t" + lines[dict["mlplateindex"]] + "\t" + WellRow + WellCol + "\t" + lines[dict["modelatpick"]]);

                }
            }
            //this was in jack's code, however not needed as long as  global excel variables are used
            //Directory.Remove("Z://PickLists/" + FIVNumber + "FIVGO_CMSPlateMap.xlsx");

            //bracket from unsused for loop
            //}
            SW.Flush(); SW.Close();
            template.Close();
            
            //excelApp.
            return "success";
        }


        //creates a new excel file from the template one and blanks out the columns that will be changed
        //original name by jack is HeavenAndExcel
        private string CreateExcel(string cmsFolder)
        {

            for (int i = 3; i < 99; i++)
            {

                worksheet.Cells[i, 2] = "";
                worksheet.Cells[i, 3] = "";
            }

            //string path = Path.Combine(cmsFolder, "FIVGO_CMSPlateMap.xlsx");

            return cmsFolder;
        }


        //save as function for excel. brought into it's own function because the save as is so weird
        //if you want to change filepath, go ahead, but do not touch the things after unless you are willing to face the consequences

        private bool ExcelSaveAs(string filePath)
        {
            Directory.CreateDirectory(folderPath);
            try
            {
                template.SaveAs(filePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private string CheckDict()
        {
            string s = "";
            for (int i = 0; i < dictValues.Count; i++)
            {
                if (!dict.ContainsKey(dictValues[i]))
                {
                    s += dictValues[i] + " ";
                    dict.Add(dictValues[i], 0);
                }
            }

            s.Trim();
            return s;

        }



    }




}