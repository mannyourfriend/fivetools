
namespace FIVE.RaftCal
{
    partial class ExporCalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ExportFolderBase = new System.Windows.Forms.TextBox();
            this.FileName_ByAnnotation = new System.Windows.Forms.CheckBox();
            this.btn_ExportAllRaftImages = new System.Windows.Forms.Button();
            this.btn_ExportFromList = new System.Windows.Forms.Button();
            this.btn_ExportRaftImages = new System.Windows.Forms.Button();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.FolderName_ByAnnotation = new System.Windows.Forms.CheckBox();
            this.btn_RaftIDsAnnotated = new System.Windows.Forms.Button();
            this.ImageExtension = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Resize_And_MLExport = new System.Windows.Forms.CheckBox();
            this.Export_4Rotations = new System.Windows.Forms.CheckBox();
            this.OnlyExportSquare = new System.Windows.Forms.CheckBox();
            this.PixelList_ExportSize = new System.Windows.Forms.TextBox();
            this.ResizeMultiplier = new System.Windows.Forms.TextBox();
            this.Min_AspectRatio_Keep = new System.Windows.Forms.TextBox();
            this.PixelsExpand = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_SaveSettings = new System.Windows.Forms.Button();
            this.panelExportSettings = new System.Windows.Forms.Panel();
            this.OnlyExportFullSize = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CropToMaxSize = new System.Windows.Forms.TextBox();
            this.Include_Fiducial_Rafts = new System.Windows.Forms.CheckBox();
            this.btn_LoadSettings = new System.Windows.Forms.Button();
            this.btn_SaveAsSettings = new System.Windows.Forms.Button();
            this.btn_DefaultSettings = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.bgMain = new System.ComponentModel.BackgroundWorker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Annotations_Load = new System.Windows.Forms.Button();
            this.btn_Annotations_New = new System.Windows.Forms.Button();
            this.btn_Annotations_SaveAs = new System.Windows.Forms.Button();
            this.txBx_AnnotationsCurrentSet = new System.Windows.Forms.TextBox();
            this.txBx_AnnotationsInfo = new System.Windows.Forms.TextBox();
            this.panel_Annotations = new System.Windows.Forms.Panel();
            this.btn_Annotations_Open = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btn_Import_Annotations = new System.Windows.Forms.Button();
            this.btn_ExportCropReg_Objects = new System.Windows.Forms.Button();
            this.btn_ExpStitchFOV = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panelExportSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel_Annotations.SuspendLayout();
            this.SuspendLayout();
            // 
            // ExportFolderBase
            // 
            this.ExportFolderBase.Location = new System.Drawing.Point(13, 25);
            this.ExportFolderBase.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ExportFolderBase.Name = "ExportFolderBase";
            this.ExportFolderBase.Size = new System.Drawing.Size(541, 22);
            this.ExportFolderBase.TabIndex = 0;
            this.ExportFolderBase.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // FileName_ByAnnotation
            // 
            this.FileName_ByAnnotation.AutoSize = true;
            this.FileName_ByAnnotation.Location = new System.Drawing.Point(315, 89);
            this.FileName_ByAnnotation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FileName_ByAnnotation.Name = "FileName_ByAnnotation";
            this.FileName_ByAnnotation.Size = new System.Drawing.Size(181, 21);
            this.FileName_ByAnnotation.TabIndex = 1;
            this.FileName_ByAnnotation.Text = "FileName_ByAnnotation";
            this.FileName_ByAnnotation.UseVisualStyleBackColor = true;
            this.FileName_ByAnnotation.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // btn_ExportAllRaftImages
            // 
            this.btn_ExportAllRaftImages.Location = new System.Drawing.Point(421, 442);
            this.btn_ExportAllRaftImages.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_ExportAllRaftImages.Name = "btn_ExportAllRaftImages";
            this.btn_ExportAllRaftImages.Size = new System.Drawing.Size(183, 30);
            this.btn_ExportAllRaftImages.TabIndex = 95;
            this.btn_ExportAllRaftImages.Text = "Export All Raft Images";
            this.btn_ExportAllRaftImages.UseVisualStyleBackColor = true;
            this.btn_ExportAllRaftImages.Click += new System.EventHandler(this.btn_ExportAllRaftImages_Click);
            // 
            // btn_ExportFromList
            // 
            this.btn_ExportFromList.Location = new System.Drawing.Point(421, 405);
            this.btn_ExportFromList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_ExportFromList.Name = "btn_ExportFromList";
            this.btn_ExportFromList.Size = new System.Drawing.Size(183, 30);
            this.btn_ExportFromList.TabIndex = 94;
            this.btn_ExportFromList.Text = "Export From List . . ";
            this.btn_ExportFromList.UseVisualStyleBackColor = true;
            this.btn_ExportFromList.Click += new System.EventHandler(this.btn_ExportFromList_Click);
            // 
            // btn_ExportRaftImages
            // 
            this.btn_ExportRaftImages.Location = new System.Drawing.Point(421, 516);
            this.btn_ExportRaftImages.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_ExportRaftImages.Name = "btn_ExportRaftImages";
            this.btn_ExportRaftImages.Size = new System.Drawing.Size(183, 30);
            this.btn_ExportRaftImages.TabIndex = 93;
            this.btn_ExportRaftImages.Text = "Export Annotated Rafts";
            this.btn_ExportRaftImages.UseVisualStyleBackColor = true;
            this.btn_ExportRaftImages.Click += new System.EventHandler(this.btn_ExportRaftImages_Click);
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(9, 405);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(403, 341);
            this.txBx_Update.TabIndex = 96;
            // 
            // FolderName_ByAnnotation
            // 
            this.FolderName_ByAnnotation.AutoSize = true;
            this.FolderName_ByAnnotation.Location = new System.Drawing.Point(315, 63);
            this.FolderName_ByAnnotation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FolderName_ByAnnotation.Name = "FolderName_ByAnnotation";
            this.FolderName_ByAnnotation.Size = new System.Drawing.Size(199, 21);
            this.FolderName_ByAnnotation.TabIndex = 97;
            this.FolderName_ByAnnotation.Text = "FolderName_ByAnnotation";
            this.FolderName_ByAnnotation.UseVisualStyleBackColor = true;
            this.FolderName_ByAnnotation.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // btn_RaftIDsAnnotated
            // 
            this.btn_RaftIDsAnnotated.Location = new System.Drawing.Point(421, 715);
            this.btn_RaftIDsAnnotated.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_RaftIDsAnnotated.Name = "btn_RaftIDsAnnotated";
            this.btn_RaftIDsAnnotated.Size = new System.Drawing.Size(183, 30);
            this.btn_RaftIDsAnnotated.TabIndex = 98;
            this.btn_RaftIDsAnnotated.Text = "Annotations to Clipboard";
            this.btn_RaftIDsAnnotated.UseVisualStyleBackColor = true;
            this.btn_RaftIDsAnnotated.Click += new System.EventHandler(this.btn_PlateMetadata);
            // 
            // ImageExtension
            // 
            this.ImageExtension.Location = new System.Drawing.Point(13, 75);
            this.ImageExtension.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ImageExtension.Name = "ImageExtension";
            this.ImageExtension.Size = new System.Drawing.Size(193, 22);
            this.ImageExtension.TabIndex = 99;
            this.ImageExtension.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 17);
            this.label1.TabIndex = 100;
            this.label1.Text = "Export Folder Base";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 55);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 101;
            this.label2.Text = "Image Type";
            // 
            // Resize_And_MLExport
            // 
            this.Resize_And_MLExport.AutoSize = true;
            this.Resize_And_MLExport.Enabled = false;
            this.Resize_And_MLExport.Location = new System.Drawing.Point(315, 114);
            this.Resize_And_MLExport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Resize_And_MLExport.Name = "Resize_And_MLExport";
            this.Resize_And_MLExport.Size = new System.Drawing.Size(173, 21);
            this.Resize_And_MLExport.TabIndex = 103;
            this.Resize_And_MLExport.Text = "Resize_And_MLExport";
            this.Resize_And_MLExport.UseVisualStyleBackColor = true;
            this.Resize_And_MLExport.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // Export_4Rotations
            // 
            this.Export_4Rotations.AutoSize = true;
            this.Export_4Rotations.Location = new System.Drawing.Point(315, 192);
            this.Export_4Rotations.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Export_4Rotations.Name = "Export_4Rotations";
            this.Export_4Rotations.Size = new System.Drawing.Size(146, 21);
            this.Export_4Rotations.TabIndex = 102;
            this.Export_4Rotations.Text = "Export_4Rotations";
            this.Export_4Rotations.UseVisualStyleBackColor = true;
            this.Export_4Rotations.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // OnlyExportSquare
            // 
            this.OnlyExportSquare.AutoSize = true;
            this.OnlyExportSquare.Enabled = false;
            this.OnlyExportSquare.Location = new System.Drawing.Point(315, 140);
            this.OnlyExportSquare.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OnlyExportSquare.Name = "OnlyExportSquare";
            this.OnlyExportSquare.Size = new System.Drawing.Size(145, 21);
            this.OnlyExportSquare.TabIndex = 104;
            this.OnlyExportSquare.Text = "OnlyExportSquare";
            this.OnlyExportSquare.UseVisualStyleBackColor = true;
            this.OnlyExportSquare.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // PixelList_ExportSize
            // 
            this.PixelList_ExportSize.Enabled = false;
            this.PixelList_ExportSize.Location = new System.Drawing.Point(13, 112);
            this.PixelList_ExportSize.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PixelList_ExportSize.Name = "PixelList_ExportSize";
            this.PixelList_ExportSize.Size = new System.Drawing.Size(100, 22);
            this.PixelList_ExportSize.TabIndex = 105;
            this.PixelList_ExportSize.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // ResizeMultiplier
            // 
            this.ResizeMultiplier.Location = new System.Drawing.Point(13, 201);
            this.ResizeMultiplier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ResizeMultiplier.Name = "ResizeMultiplier";
            this.ResizeMultiplier.Size = new System.Drawing.Size(100, 22);
            this.ResizeMultiplier.TabIndex = 106;
            this.ResizeMultiplier.Text = "11";
            this.ResizeMultiplier.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // Min_AspectRatio_Keep
            // 
            this.Min_AspectRatio_Keep.Location = new System.Drawing.Point(13, 171);
            this.Min_AspectRatio_Keep.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Min_AspectRatio_Keep.Name = "Min_AspectRatio_Keep";
            this.Min_AspectRatio_Keep.Size = new System.Drawing.Size(100, 22);
            this.Min_AspectRatio_Keep.TabIndex = 107;
            this.Min_AspectRatio_Keep.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // PixelsExpand
            // 
            this.PixelsExpand.Enabled = false;
            this.PixelsExpand.Location = new System.Drawing.Point(13, 142);
            this.PixelsExpand.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PixelsExpand.Name = "PixelsExpand";
            this.PixelsExpand.Size = new System.Drawing.Size(100, 22);
            this.PixelsExpand.TabIndex = 108;
            this.PixelsExpand.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(123, 116);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 17);
            this.label3.TabIndex = 109;
            this.label3.Text = "PixelList_ExportSize";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(123, 145);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 17);
            this.label4.TabIndex = 110;
            this.label4.Text = "PixelsExpand";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(123, 175);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(155, 17);
            this.label5.TabIndex = 111;
            this.label5.Text = "Min_AspectRatio_Keep";
            this.toolTip1.SetToolTip(this.label5, "If the aspect ratio is worse than this number (lower), it is not exported. 1 = pe" +
        "rfect square, 0 = straight line");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(123, 204);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 17);
            this.label6.TabIndex = 112;
            this.label6.Text = "Resize Multiplier";
            this.toolTip1.SetToolTip(this.label6, "If you enter 0.5, the new image will be half the size of the original. If you ent" +
        "er 2, it is like zooming in if you use the cropping below. Resize is first, crop" +
        " is second");
            // 
            // btn_SaveSettings
            // 
            this.btn_SaveSettings.Location = new System.Drawing.Point(13, 265);
            this.btn_SaveSettings.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_SaveSettings.Name = "btn_SaveSettings";
            this.btn_SaveSettings.Size = new System.Drawing.Size(144, 30);
            this.btn_SaveSettings.TabIndex = 113;
            this.btn_SaveSettings.Text = "Save Settings";
            this.toolTip1.SetToolTip(this.btn_SaveSettings, "Locks in the setting typed above so they can be applied to this export");
            this.btn_SaveSettings.UseVisualStyleBackColor = true;
            this.btn_SaveSettings.Click += new System.EventHandler(this.btn_SaveSettings_Click);
            // 
            // panelExportSettings
            // 
            this.panelExportSettings.Controls.Add(this.OnlyExportFullSize);
            this.panelExportSettings.Controls.Add(this.label7);
            this.panelExportSettings.Controls.Add(this.CropToMaxSize);
            this.panelExportSettings.Controls.Add(this.Include_Fiducial_Rafts);
            this.panelExportSettings.Controls.Add(this.btn_LoadSettings);
            this.panelExportSettings.Controls.Add(this.btn_SaveAsSettings);
            this.panelExportSettings.Controls.Add(this.btn_DefaultSettings);
            this.panelExportSettings.Controls.Add(this.PixelList_ExportSize);
            this.panelExportSettings.Controls.Add(this.btn_SaveSettings);
            this.panelExportSettings.Controls.Add(this.ResizeMultiplier);
            this.panelExportSettings.Controls.Add(this.OnlyExportSquare);
            this.panelExportSettings.Controls.Add(this.label6);
            this.panelExportSettings.Controls.Add(this.Resize_And_MLExport);
            this.panelExportSettings.Controls.Add(this.Min_AspectRatio_Keep);
            this.panelExportSettings.Controls.Add(this.Export_4Rotations);
            this.panelExportSettings.Controls.Add(this.label5);
            this.panelExportSettings.Controls.Add(this.FolderName_ByAnnotation);
            this.panelExportSettings.Controls.Add(this.label2);
            this.panelExportSettings.Controls.Add(this.PixelsExpand);
            this.panelExportSettings.Controls.Add(this.label1);
            this.panelExportSettings.Controls.Add(this.label4);
            this.panelExportSettings.Controls.Add(this.ImageExtension);
            this.panelExportSettings.Controls.Add(this.FileName_ByAnnotation);
            this.panelExportSettings.Controls.Add(this.label3);
            this.panelExportSettings.Controls.Add(this.ExportFolderBase);
            this.panelExportSettings.Location = new System.Drawing.Point(9, 92);
            this.panelExportSettings.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelExportSettings.Name = "panelExportSettings";
            this.panelExportSettings.Size = new System.Drawing.Size(595, 304);
            this.panelExportSettings.TabIndex = 114;
            // 
            // OnlyExportFullSize
            // 
            this.OnlyExportFullSize.AutoSize = true;
            this.OnlyExportFullSize.Location = new System.Drawing.Point(315, 166);
            this.OnlyExportFullSize.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OnlyExportFullSize.Name = "OnlyExportFullSize";
            this.OnlyExportFullSize.Size = new System.Drawing.Size(148, 21);
            this.OnlyExportFullSize.TabIndex = 120;
            this.OnlyExportFullSize.Text = "OnlyExportFullSize";
            this.OnlyExportFullSize.UseVisualStyleBackColor = true;
            this.OnlyExportFullSize.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(124, 234);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 17);
            this.label7.TabIndex = 119;
            this.label7.Text = "CropToMaxSize";
            this.toolTip1.SetToolTip(this.label7, "Enter -1 to disable, will crop the image (only smaller) so the dimensions are thi" +
        "s number in pixels");
            // 
            // CropToMaxSize
            // 
            this.CropToMaxSize.Location = new System.Drawing.Point(13, 230);
            this.CropToMaxSize.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CropToMaxSize.Name = "CropToMaxSize";
            this.CropToMaxSize.Size = new System.Drawing.Size(100, 22);
            this.CropToMaxSize.TabIndex = 118;
            this.CropToMaxSize.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // Include_Fiducial_Rafts
            // 
            this.Include_Fiducial_Rafts.AutoSize = true;
            this.Include_Fiducial_Rafts.Location = new System.Drawing.Point(315, 218);
            this.Include_Fiducial_Rafts.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Include_Fiducial_Rafts.Name = "Include_Fiducial_Rafts";
            this.Include_Fiducial_Rafts.Size = new System.Drawing.Size(172, 21);
            this.Include_Fiducial_Rafts.TabIndex = 117;
            this.Include_Fiducial_Rafts.Text = "Include_Fiducial_Rafts";
            this.Include_Fiducial_Rafts.UseVisualStyleBackColor = true;
            this.Include_Fiducial_Rafts.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // btn_LoadSettings
            // 
            this.btn_LoadSettings.Location = new System.Drawing.Point(476, 265);
            this.btn_LoadSettings.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_LoadSettings.Name = "btn_LoadSettings";
            this.btn_LoadSettings.Size = new System.Drawing.Size(93, 30);
            this.btn_LoadSettings.TabIndex = 116;
            this.btn_LoadSettings.Text = "Load . .";
            this.btn_LoadSettings.UseVisualStyleBackColor = true;
            this.btn_LoadSettings.Click += new System.EventHandler(this.btn_LoadSettings_Click);
            // 
            // btn_SaveAsSettings
            // 
            this.btn_SaveAsSettings.Location = new System.Drawing.Point(375, 265);
            this.btn_SaveAsSettings.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_SaveAsSettings.Name = "btn_SaveAsSettings";
            this.btn_SaveAsSettings.Size = new System.Drawing.Size(93, 30);
            this.btn_SaveAsSettings.TabIndex = 115;
            this.btn_SaveAsSettings.Text = "Save As . .";
            this.btn_SaveAsSettings.UseVisualStyleBackColor = true;
            this.btn_SaveAsSettings.Click += new System.EventHandler(this.btn_SaveAsSettings_Click);
            // 
            // btn_DefaultSettings
            // 
            this.btn_DefaultSettings.Location = new System.Drawing.Point(285, 265);
            this.btn_DefaultSettings.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_DefaultSettings.Name = "btn_DefaultSettings";
            this.btn_DefaultSettings.Size = new System.Drawing.Size(81, 30);
            this.btn_DefaultSettings.TabIndex = 114;
            this.btn_DefaultSettings.Text = "Defaults";
            this.btn_DefaultSettings.UseVisualStyleBackColor = true;
            this.btn_DefaultSettings.Click += new System.EventHandler(this.btn_DefaultSettings_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(421, 650);
            this.btn_Cancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(108, 30);
            this.btn_Cancel.TabIndex = 115;
            this.btn_Cancel.Text = "Stop / Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // bgMain
            // 
            this.bgMain.WorkerReportsProgress = true;
            this.bgMain.WorkerSupportsCancellation = true;
            this.bgMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgMain_DoWork);
            this.bgMain.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgMain_ProgressChanged);
            this.bgMain.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgMain_RunWorkerCompleted);
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(9, 7);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(595, 78);
            this.panel2.TabIndex = 116;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(4, 4);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(535, 193);
            this.dataGridView1.TabIndex = 0;
            // 
            // btn_Annotations_Load
            // 
            this.btn_Annotations_Load.Location = new System.Drawing.Point(105, 204);
            this.btn_Annotations_Load.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_Annotations_Load.Name = "btn_Annotations_Load";
            this.btn_Annotations_Load.Size = new System.Drawing.Size(93, 30);
            this.btn_Annotations_Load.TabIndex = 119;
            this.btn_Annotations_Load.Text = "Load . .";
            this.btn_Annotations_Load.UseVisualStyleBackColor = true;
            this.btn_Annotations_Load.Click += new System.EventHandler(this.btn_Annotations_Load_Click);
            // 
            // btn_Annotations_New
            // 
            this.btn_Annotations_New.Enabled = false;
            this.btn_Annotations_New.Location = new System.Drawing.Point(4, 204);
            this.btn_Annotations_New.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_Annotations_New.Name = "btn_Annotations_New";
            this.btn_Annotations_New.Size = new System.Drawing.Size(93, 30);
            this.btn_Annotations_New.TabIndex = 118;
            this.btn_Annotations_New.Text = "New";
            this.btn_Annotations_New.UseVisualStyleBackColor = true;
            this.btn_Annotations_New.Click += new System.EventHandler(this.btn_Annotations_New_Click);
            // 
            // btn_Annotations_SaveAs
            // 
            this.btn_Annotations_SaveAs.Location = new System.Drawing.Point(207, 204);
            this.btn_Annotations_SaveAs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_Annotations_SaveAs.Name = "btn_Annotations_SaveAs";
            this.btn_Annotations_SaveAs.Size = new System.Drawing.Size(93, 30);
            this.btn_Annotations_SaveAs.TabIndex = 120;
            this.btn_Annotations_SaveAs.Text = "Save As . .";
            this.btn_Annotations_SaveAs.UseVisualStyleBackColor = true;
            this.btn_Annotations_SaveAs.Click += new System.EventHandler(this.btn_Annotations_SaveAs_Click);
            // 
            // txBx_AnnotationsCurrentSet
            // 
            this.txBx_AnnotationsCurrentSet.Location = new System.Drawing.Point(4, 241);
            this.txBx_AnnotationsCurrentSet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txBx_AnnotationsCurrentSet.Name = "txBx_AnnotationsCurrentSet";
            this.txBx_AnnotationsCurrentSet.ReadOnly = true;
            this.txBx_AnnotationsCurrentSet.Size = new System.Drawing.Size(533, 22);
            this.txBx_AnnotationsCurrentSet.TabIndex = 121;
            // 
            // txBx_AnnotationsInfo
            // 
            this.txBx_AnnotationsInfo.Location = new System.Drawing.Point(4, 273);
            this.txBx_AnnotationsInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txBx_AnnotationsInfo.Multiline = true;
            this.txBx_AnnotationsInfo.Name = "txBx_AnnotationsInfo";
            this.txBx_AnnotationsInfo.Size = new System.Drawing.Size(533, 360);
            this.txBx_AnnotationsInfo.TabIndex = 122;
            // 
            // panel_Annotations
            // 
            this.panel_Annotations.Controls.Add(this.btn_Annotations_Open);
            this.panel_Annotations.Controls.Add(this.dataGridView1);
            this.panel_Annotations.Controls.Add(this.btn_Annotations_New);
            this.panel_Annotations.Controls.Add(this.txBx_AnnotationsInfo);
            this.panel_Annotations.Controls.Add(this.btn_Annotations_Load);
            this.panel_Annotations.Controls.Add(this.txBx_AnnotationsCurrentSet);
            this.panel_Annotations.Controls.Add(this.btn_Annotations_SaveAs);
            this.panel_Annotations.Location = new System.Drawing.Point(612, 7);
            this.panel_Annotations.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel_Annotations.Name = "panel_Annotations";
            this.panel_Annotations.Size = new System.Drawing.Size(547, 640);
            this.panel_Annotations.TabIndex = 118;
            // 
            // btn_Annotations_Open
            // 
            this.btn_Annotations_Open.Location = new System.Drawing.Point(308, 204);
            this.btn_Annotations_Open.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_Annotations_Open.Name = "btn_Annotations_Open";
            this.btn_Annotations_Open.Size = new System.Drawing.Size(100, 30);
            this.btn_Annotations_Open.TabIndex = 123;
            this.btn_Annotations_Open.Text = "Open/Edit";
            this.btn_Annotations_Open.UseVisualStyleBackColor = true;
            this.btn_Annotations_Open.Click += new System.EventHandler(this.btn_Annotations_Open_Click);
            // 
            // btn_Import_Annotations
            // 
            this.btn_Import_Annotations.Location = new System.Drawing.Point(421, 682);
            this.btn_Import_Annotations.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_Import_Annotations.Name = "btn_Import_Annotations";
            this.btn_Import_Annotations.Size = new System.Drawing.Size(144, 30);
            this.btn_Import_Annotations.TabIndex = 119;
            this.btn_Import_Annotations.Text = "Import Annotations";
            this.toolTip1.SetToolTip(this.btn_Import_Annotations, "This will append the annotations you select from a file into this PlateID. Select" +
        " a .txt file that has 6 columns = PlateID, RaftID, Name, Value, Well Label, FOV");
            this.btn_Import_Annotations.UseVisualStyleBackColor = true;
            this.btn_Import_Annotations.Click += new System.EventHandler(this.btn_Import_Annotations_Click);
            // 
            // btn_ExportCropReg_Objects
            // 
            this.btn_ExportCropReg_Objects.Location = new System.Drawing.Point(421, 479);
            this.btn_ExportCropReg_Objects.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_ExportCropReg_Objects.Name = "btn_ExportCropReg_Objects";
            this.btn_ExportCropReg_Objects.Size = new System.Drawing.Size(183, 30);
            this.btn_ExportCropReg_Objects.TabIndex = 120;
            this.btn_ExportCropReg_Objects.Text = "Export All Seg Images";
            this.toolTip1.SetToolTip(this.btn_ExportCropReg_Objects, "Uses the Segmentation Settings and \"OBJ\" selection to crop out around a single nu" +
        "clei or object");
            this.btn_ExportCropReg_Objects.UseVisualStyleBackColor = true;
            this.btn_ExportCropReg_Objects.Click += new System.EventHandler(this.btn_ExportCropRegions_Objects_Click);
            // 
            // btn_ExpStitchFOV
            // 
            this.btn_ExpStitchFOV.Location = new System.Drawing.Point(483, 553);
            this.btn_ExpStitchFOV.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_ExpStitchFOV.Name = "btn_ExpStitchFOV";
            this.btn_ExpStitchFOV.Size = new System.Drawing.Size(121, 30);
            this.btn_ExpStitchFOV.TabIndex = 121;
            this.btn_ExpStitchFOV.Text = "Exp Stitch Fov";
            this.btn_ExpStitchFOV.UseVisualStyleBackColor = true;
            this.btn_ExpStitchFOV.Click += new System.EventHandler(this.btn_ExportStitchFOV);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(421, 558);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(52, 22);
            this.textBox1.TabIndex = 121;
            this.textBox1.Text = "4";
            // 
            // ExporCalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 762);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_ExpStitchFOV);
            this.Controls.Add(this.btn_ExportCropReg_Objects);
            this.Controls.Add(this.btn_Import_Annotations);
            this.Controls.Add(this.panel_Annotations);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.panelExportSettings);
            this.Controls.Add(this.btn_RaftIDsAnnotated);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.btn_ExportAllRaftImages);
            this.Controls.Add(this.btn_ExportFromList);
            this.Controls.Add(this.btn_ExportRaftImages);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ExporCalForm";
            this.Text = "ExporCalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExporCalForm_FormClosing);
            this.Load += new System.EventHandler(this.ExporCalForm_Load);
            this.panelExportSettings.ResumeLayout(false);
            this.panelExportSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel_Annotations.ResumeLayout(false);
            this.panel_Annotations.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ExportFolderBase;
        private System.Windows.Forms.CheckBox FileName_ByAnnotation;
        private System.Windows.Forms.Button btn_ExportAllRaftImages;
        private System.Windows.Forms.Button btn_ExportFromList;
        private System.Windows.Forms.Button btn_ExportRaftImages;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.CheckBox FolderName_ByAnnotation;
        private System.Windows.Forms.Button btn_RaftIDsAnnotated;
        public  System.Windows.Forms.TextBox ImageExtension;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox Resize_And_MLExport;
        private System.Windows.Forms.CheckBox Export_4Rotations;
        private System.Windows.Forms.CheckBox OnlyExportSquare;
        private System.Windows.Forms.TextBox PixelList_ExportSize;
        private System.Windows.Forms.TextBox ResizeMultiplier;
        private System.Windows.Forms.TextBox Min_AspectRatio_Keep;
        private System.Windows.Forms.TextBox PixelsExpand;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_SaveSettings;
        private System.Windows.Forms.Panel panelExportSettings;
        private System.Windows.Forms.Button btn_Cancel;
        private System.ComponentModel.BackgroundWorker bgMain;
        private System.Windows.Forms.Button btn_LoadSettings;
        private System.Windows.Forms.Button btn_SaveAsSettings;
        private System.Windows.Forms.Button btn_DefaultSettings;
        private System.Windows.Forms.CheckBox Include_Fiducial_Rafts;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Annotations_New;
        private System.Windows.Forms.Button btn_Annotations_Load;
        private System.Windows.Forms.Button btn_Annotations_SaveAs;
        private System.Windows.Forms.TextBox txBx_AnnotationsCurrentSet;
        private System.Windows.Forms.TextBox txBx_AnnotationsInfo;
        private System.Windows.Forms.Panel panel_Annotations;
        private System.Windows.Forms.Button btn_Annotations_Open;
        private System.Windows.Forms.TextBox CropToMaxSize;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox OnlyExportFullSize;
        private System.Windows.Forms.Button btn_Import_Annotations;
        private System.Windows.Forms.Button btn_ExportCropReg_Objects;
        private System.Windows.Forms.Button btn_ExpStitchFOV;
        private System.Windows.Forms.TextBox textBox1;
    }
}