﻿using NpgsqlTypes;
using static WebApplication9.TypeValueMaps.Maps;
using static WebApplication9.TypeValueMaps;
using Npgsql;

namespace WebApplication9
{

    public struct MeasurementRow : IComparable<MeasurementRow>
    {
        public MeasurementRow()
        {
            ColumnName = "";
            ParameterName = "";
            ClassType = typeof(string);

            dbType = NpgsqlDbType.Bit;
            IsNull = DBNull.Value;
            Feature = 0;
            WavelengthKey = 0;
            MaskKey = 0;
            OtherValue = 0;
            DoubleValue = double.NaN;
            
        }

        public String? TableName = "Measurement_Table";
        public String ColumnName { get; set; }
        public Type ClassType { get; set; }
        public NpgsqlDbType dbType { get; set; }
        public DBNull? IsNull { get; set; }
        public int Feature { get; set; }
        public int FileId { get => getMaxKey("datalevel", "FileData", "Id", new NpgsqlConnection(Maps.GrabConnectionString())); }
        public int WavelengthKey { get; set; }
        public int MaskKey { get; set; }
        public Object? OtherValue { get; set; }
        public Double DoubleValue { get; set; }
        public String ParameterName { get; set; }

        int IComparable<MeasurementRow>.CompareTo(MeasurementRow That)
        {
            if (this.ClassType == typeof(double) && this.DoubleValue < That.DoubleValue) return -1;
            if (this.ClassType == typeof(double) && this.DoubleValue > That.DoubleValue) return 1;
            if (this.ClassType == typeof(double) && this.DoubleValue == That.DoubleValue) return 0;
            if (this.ClassType != typeof(double) && That.ClassType == typeof(double)) return -1;
            if (this.ClassType == typeof(double) && That.ClassType != typeof(double)) return 1;
            if (this.ClassType == typeof(double) && That.ClassType == typeof(double)) return 0;
            else
                return 0;
        }

        public static MeasurementRow GenerateMeasurementRow(String columName, String value)
        {
            Dictionary<Type, NpgsqlDbType> TypeToDb = Maps.ClassTypeToSQLType();
            Dictionary<String, int> FeatureIdMap = Maps.FeatureIdMap();
            Dictionary<String, int> MaskIdMap = Maps.MaskIdMap();
            Dictionary<String, int> WaveLengthIdMap = Maps.WavelengthIdMap();
            MeasurementRow row = new MeasurementRow();
            String[] Splitter = columName.Split(" ");
            if (columName.Contains("AntiHealth"))
            {
                row.WavelengthKey = WaveLengthIdMap.GetValueOrDefault("noWavelength");
                String Mask = Splitter.First().ToString();
                String TheFeature = Splitter.Last().ToString();
                row.MaskKey = MaskIdMap.GetValueOrDefault(columName);
                int feat;
                FeatureIdMap.TryGetValue(TheFeature, out feat);
                row.Feature = feat;
            }
            else
            {
                String Mask = Splitter.First().ToString();
                String Wavelength = Splitter.Last().ToString();
                row.MaskKey = MaskIdMap.GetValueOrDefault(Mask);
                row.WavelengthKey = WaveLengthIdMap.GetValueOrDefault(Wavelength);
                String StrFeature = String.Join(" ", Splitter.Where(t => t != Mask && t != Wavelength).ToList());
                int aFeat;
                FeatureIdMap.TryGetValue(StrFeature, out aFeat);
                row.Feature = aFeat;
            }
            row.TableName = "Measurement_Table";
            row.ColumnName = columName.ToUpper();

            try
            {
                Double DoubleVal;
                Boolean IsDouble = Double.TryParse(value.ToString(), out DoubleVal);
                row.DoubleValue = DoubleVal;
                row.ClassType = typeof(double);
                row.dbType = TypeToDb.GetValueOrDefault(row.ClassType);
                row.ParameterName = "@" + columName.ToUpper() + ",";
            }
            catch 
            {
                Object o = SpreadSheetTools.ParseOrDefault(value);
                if (o != null)
                {
                    row.OtherValue = o;
                    row.ClassType = o.GetType();
                    row.dbType = TypeToDb.GetValueOrDefault(row.ClassType);
                    row.ParameterName = "@" + columName.ToUpper();
                    row.OtherValue = o;
                }
                else
                {
                    row.OtherValue = null;
                    row.ClassType = typeof(Nullable);
                    row.IsNull = DBNull.Value;
                }
            }
            return row;
        }
    }

    public struct NonMeasurementRow : IComparable<NonMeasurementRow>
    {
        public String? Table_Name { get; set; }
        public Dictionary<String, int> InitialKeys { get; set; }
        public int Order { get; set; }
        public Type? classType { get; set; }
        public NpgsqlDbType? dbType { get; set; }
        public Object? value { get; set; }
        public String ParameterName { get; set; }

        public ValueTuple<(String Column, int Order)> ColumnOrder { get; set; }

        int IComparable<NonMeasurementRow>.CompareTo(NonMeasurementRow that)
        {
            if (this.Order < that.Order) return -1;
            if (this.Order > that.Order) return 1;
            if (this.Order == that.Order) return 0;


            if (this.value == null && that.value != null) return -1;
            if (this.value == null && that.value != null) return 1;
            if (this.value == null && that.value == null) return 0;

            if (Table_Name == null) return 0;
            if (Table_Name.ToUpper() == "SCAN_TABLE") return 5;
            if (Table_Name.ToUpper() == "WELL_TABLE") return 4;
            if (Table_Name.ToUpper() == "FOV_TABLE") return 3;
            if (Table_Name.ToUpper() == "CELL_TABLE") return 2;
            if (Table_Name.ToUpper() == "RAFT_TABLE") return 1;
            if (Table_Name.ToUpper() == "UNKNOWN_TABLE") return -1;


            else
                return 0;
        }

        public static NonMeasurementRow GenerateNonMeasurementRow(String columnName, String value)
        {
            Dictionary<String, int> ScanDict = ScanTableDictionary();
            Dictionary<String, int> WellDict = WellDictionary();
            Dictionary<String, int> FOVDict = FOVDictionary();
            Dictionary<String, int> RaftDict = RaftDictionary();
            Dictionary<String, int> CellDict = CellDictionary();
            Dictionary<Type, NpgsqlDbType> TypeToDb = ClassTypeToSQLType();
            Dictionary<String, Type> NonMeasurementTypeMap = Maps.NonMeasurementTypeMap();

            NonMeasurementRow row = new NonMeasurementRow();
            Object o = SpreadSheetTools.ParseOrDefault(value);
            if (o != null)
            {
                row.value = o;
            }
            else
            {
                row.value = DBNull.Value;
            }
            if (NonMeasurementTypeMap.ContainsKey(columnName))
            {
                Type? T = NonMeasurementTypeMap.GetValueOrDefault(columnName);
                if (T != null)
                {
                    row.classType = T;
                    row.dbType = TypeToDb.GetValueOrDefault(row.classType);
                }
                else
                {
                    row.classType = null;
                    row.dbType = null;
                }
                if (RaftDict.ContainsKey(columnName))
                {
                    row.Table_Name = "Raft_Table";
                    row.Order = RaftDict.GetValueOrDefault(columnName);
                    row.ColumnOrder = new ValueTuple<(string Column, int Order)>((columnName, row.Order));
                    row.ParameterName = "@" + columnName + ",";

                }
                else if (WellDict.ContainsKey(columnName))
                {
                    row.Table_Name = "Well_Table";
                    row.Order = WellDict.GetValueOrDefault(columnName);
                    row.ColumnOrder = new ValueTuple<(string Column, int Order)>((columnName, row.Order));
                    row.ParameterName = "@" + columnName + ",";
                }
                else if (CellDict.ContainsKey(columnName))
                {
                    row.Table_Name = "Cell_Table";
                    row.Order = CellDict.GetValueOrDefault(columnName);
                    row.ColumnOrder = new ValueTuple<(string Column, int Order)>((columnName, row.Order));
                    row.ParameterName = "@" + columnName + ",";

                }
                else if (ScanDict.ContainsKey(columnName))
                {
                    row.Table_Name = "Scan_Table";
                    row.Order = ScanDict.GetValueOrDefault(columnName);
                    row.ColumnOrder = new ValueTuple<(string Column, int Order)>((columnName, row.Order));
                    row.ParameterName = "@" + columnName + ",";
                }
                else if (FOVDict.ContainsKey(columnName))
                {
                    row.Table_Name = "FOV_Table";
                    row.Order = FOVDict.GetValueOrDefault(columnName);
                    row.ColumnOrder = new ValueTuple<(string Column, int Order)>((columnName, row.Order));
                    row.ParameterName = "@" + columnName + ",";

                }
            }
            return row;
        }
    }

    public struct RowsForInsertion
    {

      
        public static int getFileKey(String TableName, NpgsqlConnection conn)
        {

            return Maps.getMaxKey("datalevel", "FileData", "Id", conn);
        }
        public static int getForeignKey(String TableName, NpgsqlConnection conn)
        {

            return Maps.getMaxKey("ExperimentData", TableName, "Id", conn);
        }

        
    }

}
