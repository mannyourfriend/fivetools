﻿/*
using Loyc.Math;
using Microsoft.Extensions.Caching.Memory;
using System.Reflection;
using Loyc.Binary;
using System.Text;
using static WebApplication9.SpreadSheet.WillieExperimentData.NonMeasurementData;
using static WebApplication9.SpreadSheet.WillieExperimentData.NonMeasurementData.Scan;
using static WebApplication9.SpreadSheet.WillieExperimentData.NonMeasurementData.Scan.Well;
using static WebApplication9.SpreadSheet.WillieExperimentData.NonMeasurementData.Scan.Well.FOV;
using static WebApplication9.SpreadSheet.WillieExperimentData.NonMeasurementData.Scan.Well.FOV.Raft;
using Loyc;
using System.ComponentModel;
using System.Drawing;
using System;
using System.Numerics;
*/

/*
namespace Temp
{
 

    public class SpreadSheet

    {
     


        public SpreadSheet(String fileName)
        {
            (List<string> headers, List<List<string>> records)? DataTuple = GetDataFromFile(fileName);
            List<String> headers = DataTuple.Value.headers; List<List<String>> AllRecords = DataTuple.Value.records;
            foreach(List<string> line in AllRecords)
            {
                WillieExperimentData.HandleDataRow(line, headers);
            }
        
        }

        public static Boolean? ViolatesStrangeDataFlag(List<Object> ColumnData, String header, string value)
        {
            //could put an asterisk on a column with data that either should be null or shouldn't be there
            return null;
        }
        public static String? FilePath { get; set; }

        public static String? FileName { get; set; }

        public static String? FileExtension { get; set; }

        public static String? CreationTime { get; set; }

        public static Int64 FileSizeInBytes { get; set; }

        public static string? Encoding { get; set; }

        public static string? LastAccessed { get; set; }

        public static Int32 RowNum { get; set; }

        public static Int32 RecordNum { get; set; }

        public static Int32 ColumnNum { get; set; }

        public static String? Delimiter { get; set; }

        public static bool SetDelimiter(String? theDelimiter)
        {
            if (string.IsNullOrEmpty(theDelimiter))
            {
                return false;
            }
            else
            {
                SpreadSheet.Delimiter = theDelimiter;
                return true;
            }

        }
        public void SetDelimiter(List<char?> theDelimiter)
        {
            if (theDelimiter == null)
            {
                SetDelimiter(string.Empty);
            }
            StringBuilder sb = new StringBuilder();
            foreach (char c in theDelimiter)
            {
                theDelimiter.Add(c);
                return;
            }
            SetDelimiter(sb.ToString());
            SetDelimiter(theDelimiter);
        }
        public void SetDelimiter(Guid? theDelimiter)
        {
            if (theDelimiter == null)
            {
                SetDelimiter(string.Empty);
                return;
            }
            SetDelimiter(theDelimiter?.ToString("D"));
        }

        public void SetDelimiter(char theDelimiter)
        {
            if (theDelimiter == null)
            {
                SetDelimiter(string.Empty);
                return;
            }
            SetDelimiter(theDelimiter.ToString());
        }
        public static (List<String> headers, List<List<String>> records)? GetDataFromFile(String fileName)
        {
            try
            {
                char seperatorChar = new char();
                using (TextReader areader = File.OpenText(fileName))
                {
                    char a = detectdelimiter.DetectDelimiter(areader, areader.ReadLine().Count());
                    seperatorChar = a;
                    areader.Close();
                }

                List<String> headers = new List<String>();
                List<List<String>> records = new List<List<String>>();
                bool first = false;
                foreach (string line in File.ReadAllLines(fileName))
                {
                    if (!first)
                    {
                        headers = line?.Split(seperatorChar)?.ToList() ?? new List<string>();
                    }
                    else
                    {
                        records.Add(line?.Split(seperatorChar)?.ToList() ?? new List<string>());
                    }
                    first = true;
                }
                FileInfo Info = new FileInfo(fileName);
                SpreadSheet.FileSizeInBytes = Info.Length;
                SpreadSheet.FileExtension  = Info.Extension;
                SpreadSheet.LastAccessed = Info.LastWriteTime.ToString();
                SpreadSheet.FileName = Info.Name;
                SpreadSheet.FilePath = Info.FullName;
                SpreadSheet.CreationTime = Info.CreationTime.ToString();
                SpreadSheet.ColumnNum =  headers.Count;
                SpreadSheet.RecordNum =  records.Count;
                SpreadSheet.RowNum = (SpreadSheet.RecordNum)/(SpreadSheet.ColumnNum);
                SpreadSheet.SetDelimiter(seperatorChar.ToString());


                if (headers.Count == 0 || records.Count == 0)
                {
                    throw new Exception("No records");
                }
                return (headers, records);
            }
            catch (Exception ex)
            {
                //todo Exception database table, for now it's fine to just eat them and test things out in the early stages.
                //for now we can debug it locally
                System.Diagnostics.Trace.WriteLine(ex.InnerException?.Message);
                System.Diagnostics.Trace.WriteLine(ex?.Data);
                System.Diagnostics.Trace.WriteLine(ex?.Message);
                System.Diagnostics.Trace.WriteLine(ex.StackTrace);
                System.Diagnostics.Trace.WriteLine(fileName);
                System.Diagnostics.Trace.WriteLine(fileName);
                throw ex ?? new Exception("File Failed to open or has no records Debug locally");
            }
        }
        public class WillieExperimentData
        {
            public static void HandleDataRow(List<String> rowOfData, List<String> headerRow)
            {
                foreach (String row in rowOfData)
                {
                    //measurement data must have a wave, a wave is surrounded by spaces and 
                    //consists of three characters, always beginning with the character w
                    //if it has a wavelength we know it's measurementData.
                    //we also have a list of nonmeasurement headers.
                    //data that doesn't fall into either of these categories if starts with "ML"
                    //I guess i'll classify it as nonmeasurement for now
                    MemoryCacheOptions options = new MemoryCacheOptions();
                    options.SizeLimit = 1000000;
                    MemoryCache memoryCache = new MemoryCache(options);
                    List<string> AllFields;
                    if (!memoryCache.TryGetValue("TypeFields", out AllFields))
                    {
                        List<Type> list = new List<Type>() { typeof(Cell), typeof(Raft), typeof(FOV), typeof(Well), typeof(Scan), typeof(Cell) };
                        List<List<String>>? Fields = new List<List<string>>();
                        foreach (Type t in list)
                        {
                            Fields.Add(t.GetProperties().Select(p => p.Name).ToList());
                        }

                        List<string> fiel = Fields.SelectMany(x => x).ToList();
                    }
                    else
                    {
                        AllFields = (List<string>)memoryCache.Get("TypeFields");
                    }

      

                    
                    if (row != null)
                    {
                        for (int i = 0; i < headerRow.Count; i++)
                        {
                            List<String> item = headerRow[i].Split(" ").ToList();
                            for (int j = 0; j < item.Count; j++)
                            {
                                if (item[j].ToUpper().StartsWith("ML"))
                                {
                                    NonMeasurementData.HandleNonMeasurementData(rowOfData[i], headerRow[i], AllFields);
                                    continue;
                                }
                                else if (AllFields.Contains(item[j].ToUpper()) || item.ToList().Count < 2)
                                {

                                    NonMeasurementData.HandleNonMeasurementData(rowOfData[i], headerRow[i], AllFields);
                                    continue;
                                }
                                else
                                {
                                    List<string> breakupItem = item.ToList();
                                    int size = breakupItem.Count;
                                    String WaveLength = breakupItem[size - 1];
                                    if (WaveLength.ToUpper().Contains("W") && WaveLength.Trim().Count() == 3)
                                    {
                                        MeasurementData.HandleMeasurementData(rowOfData[i], headerRow[i]);
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            public class MeasurementData
            {
                public static void HandleMeasurementData(String field, string header)
                {
                    Fullname = field;
                    List<string> breakUpName = Fullname.Split(" ").ToList();
                    int size = breakUpName.Count;
                    Mask = breakUpName[0];
                    WaveLength = breakUpName[size - 1];
                    foreach (String name in breakUpName)
                    {
                        if (name == Mask || name == WaveLength)
                            continue;
                        Name += name;
                    }

                    Type? T = field.GetType();
                    if (T== null)
                    {
                        T = String.Empty.GetType();
                        value = "Null";
                    }
                    PropertyType = T;
                    headerName = header;

                }

                public static String? headerName { get; set; }
                public static String? Name { get; set; }
                public static String? Mask { get; set; }
                public static string? WaveLength { get; set; }
                public static string? Fullname { get; set; }
                public static Type? PropertyType { get; set; }
                public static Object? value { get; set; }
            }
            public class NonMeasurementData
            {
                public static void HandleNonMeasurementData(String? field, string header, List<String> hardCoded)
                {
                    headerName = header;
                    Type? T = field.GetType();
                    if (T == null)
                    {
                        value = "Null";
                    }
                    else
                    {
                        value = Convert.ChangeType(field, T);
                    }
                    switch (header.ToUpper())
                    {
                        case "PLATEID":
                            Scan.PlateId = field;
                            break;
                        case "PLATE_ID":
                            Scan.Plate_Id= field;
                            break;
                        case "PLATE-ID":
                            Scan.PlateId = field;
                            break;
                        case "INCARTA_PROTOCOLNAME":
                            Scan.INCARTA_ProtolName = field;
                            break;
                        case "INCARTA PROTOCOLNAME":
                            Scan.INCARTA_ProtolName = field;
                            break;
                        case "AQP":
                            Scan.AQP = field;
                            break;
                        case "SCANID":
                            Scan.Well.ScanId = int.Parse(field);
                            break;
                        case "ROW":
                            Scan.Well.Row = field;
                            break;
                        case "COLUMN":
                            Scan.Well.Column = int.Parse(field);
                            break;
                        case "WELL_LABEL":
                            Scan.Well.Well_Label = field;
                            break;
                        case "MLTRAININGLAYOUT":
                            Scan.Well.mlTrainingLayout = field;
                            break;
                        case "MLCLASSLAYOUT":
                            Scan.Well.mlClassLayout = field;
                            break;
                        case "MLPLATEINDEX":
                            Scan.Well.mlPlateIndex = int.Parse(field);
                            break;
                        case "MLMIX":
                            Scan.Well.mlMix = field;
                            break;
                        case "FOV":
                            Scan.Well.FOV.Fov = int.Parse(field);
                            break;
                        case "WELLID":
                            Scan.Well.FOV.WellId = int.Parse(field);
                            break;
                        case "IMAGENAME":
                            Scan.Well.FOV.ImageName = field;
                            break;
                        case "FOCUZ_Z_POSITION":
                            Scan.Well.FOV.Focus_Z_Pos = float.Parse(field);
                            break;
                        case "FOCUZ Z POSITION":
                            Scan.Well.FOV.Focus_Z_Pos = float.Parse(field);
                            break;
                        case "FOCUS Z POS":
                            Scan.Well.FOV.Focus_Z_Pos = float.Parse(field);
                            break;
                        case "FOCUS_Z_POS":
                            Scan.Well.FOV.Focus_Z_Pos = float.Parse(field);
                            break;
                        case "FOCUZ_Z_AVERAGE_OF_FIELDS":
                            Scan.Well.FOV.Focus_Z_Avg_of_Adjacent_Fields = float.Parse(field);
                            break;
                        case "FOCUZ Z AVERAGE_OF FIELDS":
                            Scan.Well.FOV.Focus_Z_Avg_of_Adjacent_Fields = float.Parse(field);
                            break;
                        case "OBJECTID":
                            Scan.Well.FOV.Raft.Cell.ObjectId = int.Parse(field);
                            break;
                        case "WELL_Y_UM":
                            Scan.Well.FOV.Raft.Cell.Well_y_um = float.Parse(field);
                            break;
                        case "WELL Y UM":
                            Scan.Well.FOV.Raft.Cell.Well_y_um = float.Parse(field);
                            break;
                        case "WELL_X_UM":
                            Scan.Well.FOV.Raft.Cell.Well_X_um = float.Parse(field);
                            break;
                        case "WELL X UM":
                            Scan.Well.FOV.Raft.Cell.Well_X_um = float.Parse(field);
                            break;
                        case "PLATE_X_UM":
                            Scan.Well.FOV.Raft.Cell.Plate_X_um = float.Parse(field);
                            break;
                        case "PLATE X UM":
                            Scan.Well.FOV.Raft.Cell.Plate_X_um = float.Parse(field);
                            break;
                        case "PLATE_Y_UM":
                            Scan.Well.FOV.Raft.Cell.Plate_Y_um = float.Parse(field);
                            break;
                        case "PLATE Y UM":
                            Scan.Well.FOV.Raft.Cell.Plate_Y_um = float.Parse(field);
                            break;
                        case "ACQUISITION TIME STAMP":
                            Scan.Well.FOV.Acquisition_Time_Stamp = float.Parse(field);
                            break;
                        case "ACQUISITION_TIME_STAMP":
                            Scan.Well.FOV.Acquisition_Time_Stamp = float.Parse(field);
                            break;
                        case "CELLSPERFIELD":
                            Scan.Well.FOV.Raft.CellsPerField = int.Parse(field);
                            break;
                        case "RAFTID":
                            Scan.Well.FOV.Raft.RaftId = field;
                            break;
                        case "RAFTROW":
                            Scan.Well.FOV.Raft.RaftRow= field;
                            break;
                        case "RAFTCOLUMN":
                            Scan.Well.FOV.Raft.RaftCol= field;
                            break;
                        case "UMFROMBORDEROFRAFT":
                            Scan.Well.FOV.Raft.Cell.UmFromBorderOfRaft = float.Parse(field);
                            break;
                        case "Plate_Well":
                            Scan.Well.FOV.Raft.plate_well = field;
                            break;
                        case "Plate Well":
                            Scan.Well.FOV.Raft.plate_well = field;
                            break;
                        case "Plate-Well":
                            Scan.Well.FOV.Raft.plate_well = field;
                            break;
                        default:
                            break;
                    }
                }



                public static String? headerName { get; set; }
                public static Object? value { get; set; }
                public static Type? PropertyTypeT { get; set; }
                public class Scan
                {
                    public static String? PlateId { get; set; }

                    public static String? Plate_Id { get; set; }

                    public static String? AQP { get; set; }

                    public static String? INCARTA_ProtolName { get; set; }

                    public class Well
                    {
                        public static int ScanId { get; set; }

                        public static String? Row { get; set; }

                        public static int Column { get; set; }

                        public static String? Well_Label { get; set; }

                        public static String? mlTrainingLayout { get; set; }

                        public static String? mlClassLayout { get; set; }

                        public static int mlPlateIndex { get; set; }

                        public static String? mlMix { get; set; }

                        public class FOV
                        {
                            public static int WellId { get; set; }

                            public static int Fov { get; set; }

                            public static String? ImageName { get; set; }

                            public static float? Acquisition_Time_Stamp { get; set; }

                            public static float? Focus_Z_Pos { get; set; }

                            public static float? Focus_Z_Avg_of_Adjacent_Fields { get; set; }

                            public class Raft
                            {
                                public static String? plate_well { get; set; }
                               
                                public static String? RaftId { get; set; }

                                public static String? RaftRow { get; set; }

                                public static String? RaftCol { get; set; }

                                public static int CellsPerField { get; set; }


                                public static string ImageCheck = (FOV.ImageName != null && FOV.ImageName.Any()) ? "True" : "False";

                                public class Cell
                                {
                                    public static int ObjectId { get; set; }

                                    public static float Well_X_um { get; set; }

                                    public static float Well_y_um { get; set; }

                                    public static float Plate_X_um { get; set; }

                                    public static float Plate_Y_um { get; set; }

                                    public static float UmFromBorderOfRaft { get; set; }

                                }

                            }
                        }
                    }
                }
                }

            }
        }

    internal record struct NewStruct(object Item1, object Item2)
    {
        public static implicit operator (object, object)(NewStruct value)
        {
            return (value.Item1, value.Item2);
        }

        public static implicit operator NewStruct((object, object) value)
        {
            return new NewStruct(value.Item1, value.Item2);
        }

        public static implicit operator TextWriter(NewStruct v)
        {
            throw new NotImplementedException();
        }
    }
}

*/



