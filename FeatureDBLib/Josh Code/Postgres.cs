﻿using Npgsql;
using NpgsqlTypes;

namespace WebApplication9
{
    public class Postgres
    {
        public static Boolean MakeQuery(List<Object> parameters)
        {

            var jsonParam = new NpgsqlParameter(Newtonsoft.Json.JsonConvert.SerializeObject(parameters), NpgsqlDbType.Json);

            try
            {
                String ConnnectionString = "Server=localhost;Port=5432;Database=postgres;UserId=postgres;Password=N3onblue;TrustServerCertificate=true;";
                NpgsqlConnection conn = new Npgsql.NpgsqlConnection(ConnnectionString);
                conn.Open();
                using (NpgsqlCommand? command = new NpgsqlCommand(@"INSERT INTO ""DataLevel"".""SpreadSheetData""  (""ColumnValues"") VALUES (@jsonparam)", conn))
                {
                    int i = command.ExecuteNonQuery();
                    if (i == -1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
        }
    
        public static Boolean RunTransaction(List<Object> parameters)
        {
            String ConnnectionString = "Server=localhost;Port=5432;Database=postgres;UserId=postgres;Password=N3onblue;TrustServerCertificate=true;";
            NpgsqlConnection conn = new Npgsql.NpgsqlConnection(ConnnectionString);
            conn.Open();
            NpgsqlCommand dbcmd = conn.CreateCommand();
            var jsonParam = new NpgsqlParameter(Newtonsoft.Json.JsonConvert.SerializeObject(parameters), NpgsqlDbType.Json);

            try
            {
                NpgsqlCommand d = conn.CreateCommand();
                dbcmd.Parameters.AddWithValue("@jsonparam", NpgsqlDbType.Json, jsonParam);
                dbcmd.CommandText = @"INSERT INTO ""DataLevel"".""SpreadSheetData""  (""ColumnValues"") VALUES (@jsonparam)"; 
                using (var transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable))
                {
                    using (var command = new NpgsqlCommand(dbcmd.CommandText, conn, transaction))
                    {
                        command.CommandTimeout = 120;
                        try
                        {
                            int i = command.ExecuteNonQuery();
                            transaction.Commit();
                            if (i == -1)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        catch (NpgsqlException ex)
                        {
                            //todo Exception database table, for now it's fine to just eat them and test things out in the early stages.
                            //for now we can debug it locally
                            System.Diagnostics.Trace.WriteLine(ex.InnerException?.Message);
                            System.Diagnostics.Trace.WriteLine(ex?.Data);
                            System.Diagnostics.Trace.WriteLine(ex?.Message);
                            System.Diagnostics.Trace.WriteLine(ex?.BatchCommand);
                            System.Diagnostics.Trace.WriteLine(ex?.ErrorCode);
                            System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                            System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                            System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                            System.Diagnostics.Trace.WriteLine(ex?.StackTrace);
                            throw ex ?? new Exception("File Failed to open or has no records Debug locally");
                            throw;
                        }
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                Console.Write(ex);
                if (ex.Data == null)
                {
                    System.Diagnostics.Trace.WriteLine(ex.InnerException?.Message);
                    System.Diagnostics.Trace.WriteLine(ex?.Data);
                    System.Diagnostics.Trace.WriteLine(ex?.Message);
                    System.Diagnostics.Trace.WriteLine(ex?.BatchCommand);
                    System.Diagnostics.Trace.WriteLine(ex?.ErrorCode);
                    System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                    System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                    System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                    System.Diagnostics.Trace.WriteLine(ex?.StackTrace);
                    throw;
                }
                else
                {
                    return false;
                    //var token = new CancellationToken();
                }
            }
        }
    }
}
