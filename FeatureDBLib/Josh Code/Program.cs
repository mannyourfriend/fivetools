using Npgsql;
using System.Data;
using WebApplication9;
using NpgsqlTypes;
using System.Collections.ObjectModel;

public static class MainClass
{

    public static void AAATest()
    {

        //// Configure the HTTP request pipeline.
        //if (!app.Environment.IsDevelopment())
        //{
        //    app.UseExceptionHandler("/Error");
        //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        //    app.UseHsts();
        //}
        //app.UseHttpsRedirection();
        //app.UseStaticFiles();
        //app.UseRouting();
        //app.UseAuthorization();
        //app.MapRazorPages();


        //var theRows = new RowsForInsertion();


        string fileName = $@"R:\FIVE\EXP\FIV593\2 MLModels\{Globals.file}{Globals.extension}";

        Lookup<string, int>? isNonMeasurementLookup = TypeValueMaps.Maps.GetLookup();

        string[][]? TheFile = File.ReadAllLines(fileName).Select(t => t.Split('\t').ToArray()).ToArray();
        ILookup<IEnumerable<string>, List<string>>? FileLookup = TheFile.Select(t => t).Select(t => t.Take(TheFile.Length)).ToLookup(t => t.Select(t => t), f => f.ToList());

        Dictionary<int, String> keepHeaders = new Dictionary<int, String>();
        ICollection<string> GrabCounts = new Collection<string>();

        List<MeasurementRow> MeasurementRows = new List<MeasurementRow>();
        List<NonMeasurementRow> NonMeasurementRows = new List<NonMeasurementRow>();
        HashSet<String> UndesiredData = new HashSet<String>() { "Column1", "MLPREDICTIONPROB", "FILTERED", "MLCLASSPREDICTED", "VAL COUNT" };
        HashSet<int> MeasurementSet = new HashSet<int>();
        HashSet<int> NonMeasurementSet = new HashSet<int>();
        Boolean Headers = true;
        foreach (IGrouping<IEnumerable<string>, List<string>>? Row in FileLookup)
        {
            IEnumerable<string>? item = Row.Key;
            Enumerable.TryGetNonEnumeratedCount(item, out int Count);
            for (int j = 0; j < Count; j++)
            {
                if (Headers)
                {
                    if (j == 0 && Headers == false)
                    {
                        Headers = true;
                    }
                    String Header = Row.Key.ElementAt(j);
                    if (isNonMeasurementLookup.Contains(Header))
                    {
                        NonMeasurementSet.Add(j);
                        keepHeaders.Add(j, Header);
                    }
                    else if (UndesiredData.Contains(Header))
                    {
                        UndesiredData.Add(Header);
                        keepHeaders.Add(j, Header);
                    }
                    else if (SpreadSheetTools.IsMeasurementData(Header))
                    {
                        MeasurementSet.Add(j);
                        keepHeaders.Add(j, Header);
                    }
                    else
                    {
                        UndesiredData.Add(Header);
                        keepHeaders.Add(j, Header);
                    }
                }
            }
            Headers = false;
            IEnumerable<string>? anItem = Row.Key;
            Enumerable.TryGetNonEnumeratedCount(anItem, out int count);
            for (int j = 0; j < count; j++)
            {
                String TheHeader = Row.Key.ElementAt(j);
                if (NonMeasurementSet.Contains(j))
                {
                    NonMeasurementRows.Add(NonMeasurementRow.GenerateNonMeasurementRow(keepHeaders[j], TheHeader));
                }
                else if (MeasurementSet.Contains(j))
                {
                    MeasurementRows.Add(MeasurementRow.GenerateMeasurementRow(keepHeaders[j], TheHeader));
                }
                else if (UndesiredData.Contains(TheHeader))
                {
                    continue;
                }
                else
                {
                    GrabCounts.Add(TheHeader);
                }
            }
        }


        List<MeasurementRow> Rows = MeasurementRows;
        List<NonMeasurementRow> ScanRows = NonMeasurementRows.Where(f => f.Table_Name?.ToUpper() == "SCAN_TABLE").Select(t => t).ToList();
        List<NonMeasurementRow> WellRows = NonMeasurementRows.Where(f => f.Table_Name?.ToUpper() == "WELL_TABLE").Select(t => t).ToList();
        List<NonMeasurementRow> FovRows = NonMeasurementRows.Where(f => f.Table_Name?.ToUpper() == "FOV_TABLE").Select(t => t).ToList();
        List<NonMeasurementRow> CellRows = NonMeasurementRows.Where(f => f.Table_Name?.ToUpper() == "CELL_TABLE").Select(t => t).ToList();
        List<NonMeasurementRow> RaftRows = NonMeasurementRows.Where(f => f.Table_Name?.ToUpper() == "RAFT_TABLE").Select(t => t).ToList();
        String ConnectionString = TypeValueMaps.Maps.GrabConnectionString();
        NpgsqlConnection conn = new NpgsqlConnection(ConnectionString);
        NpgsqlConnection conn2 = new NpgsqlConnection(ConnectionString);
        NpgsqlConnection conn3 = new NpgsqlConnection(ConnectionString);
        conn.Open();
        conn2.Open();
        conn3.Open();
        int FileId = 0; // getFileKey("FileData", conn);
        int ScanForeignKey = 0; // getFileKey("FileData", conn);
        int WellForeignKey = 0; // getForeignKey("Scan_Table", conn);
        int FovForeignKey = 0; // getForeignKey("Well_Table", conn);
        int CellForeignKey = 0; //getForeignKey("FOV_Table", conn);
        int RaftOneForeignKey = 0; // getForeignKey("FOV_Table", conn);
        List<int> RaftIds = new List<int>() { RaftOneForeignKey };


        while (MeasurementRows.Any())
        {
            InsertToMeasurementTable(MeasurementRows, conn2, FileId);
        }
        while (ScanRows.Any())
        {
            IEnumerable<NonMeasurementRow> SRows = ScanRows.GroupBy(t => t.Order).Select(t => t.FirstOrDefault()).Take(7).OrderBy(t => t.Order);
            InsertToScanTable(SRows, conn3, FileId);
        }

        while (WellRows.Any())
        {
            IEnumerable<NonMeasurementRow> WRows = WellRows.GroupBy(t => t.Order).Select(t => t.FirstOrDefault()).Take(12).OrderBy(t => t.Order);
            InsertToWellTable(WRows, conn2, ScanForeignKey);
        }

        while (FovRows.Any())
        {
            IEnumerable<NonMeasurementRow> FRows = FovRows.GroupBy(t => t.Order).Select(t => t.FirstOrDefault()).Take(7).OrderBy(t => t.Order);
            InsertToFovTable(FRows, conn3, WellForeignKey);
        }

        while (CellRows.Any())
        {
            IEnumerable<NonMeasurementRow> CRows = CellRows.GroupBy(t => t.Order).Select(t => t.FirstOrDefault()).Take(10).OrderBy(t => t.Order);
            InsertToCellTable(CRows, conn2, FovForeignKey);
        }
        while (RaftRows.Any())
        {
            IEnumerable<NonMeasurementRow> RRows = RaftRows.GroupBy(t => t.Order).Select(t => t.FirstOrDefault()).Take(10).OrderBy(t => t.Order);
            InsertToRaftTable(RRows, conn3, RaftIds);
        }
        while (GrabCounts.Any())
        {
            foreach (String grab in GrabCounts)
            {
                InsertNewColumn(conn, "Unknown", grab, fileName);
            }
        }
    }


    public static int InsertToScanTable(IEnumerable<NonMeasurementRow> ScanData, NpgsqlConnection conn, int foreignKey)
    {


        String InsertSqlIntoScanTable = $@"INSERT INTO ""ExperimentData"".""Scan_Table"" (""PlateId"", ""PlateSectionId"", ""AQP"", ""INCARATA_ProtocolName"", ""ResultsId"", ""MLPlateIndex"", ""Index"", ""FileId"")
                        VALUES(";
        using var cmd = new NpgsqlCommand(InsertSqlIntoScanTable, conn);
        try
        {
            foreach (NonMeasurementRow scanData in ScanData)
            {
                cmd.Parameters.AddWithValue(scanData.ParameterName, scanData.dbType.Value, scanData.value);
                cmd.CommandText += scanData.ParameterName;
                cmd.Parameters.Add("@FileId", NpgsqlTypes.NpgsqlDbType.Integer, foreignKey);

            }
            cmd.CommandText += "@FileId)";
            foreignKey = foreignKey + 1;
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            return 1;
        }
        catch //(NpgsqlException ex)
        {
            return -1;
        }
    }

    public static int InsertToWellTable(IEnumerable<NonMeasurementRow> wellList, NpgsqlConnection conn, int ScanForeignKey)
    {
        int iterator = 0;
        String WellTableInsert = $@"INSERT INTO ""ExperimentData"".""Well_Table""
        (""ScanTableId"", ""WELL LABEL"", ""row"", ""column"" ""MLTrainingLayout"", ""MLClassLayout"", ""MLPlateIndex"", ""MLMix"", ""PlateWell"", ""MLReplicates"", ""MLClassLayout Replicates"", ""MLCrossvalFold"")
            VALUES (@ScanTableId,";

        using var cmd = new NpgsqlCommand(WellTableInsert, conn);

        Boolean first = true;
        try
        {
            foreach (NonMeasurementRow WellItem in wellList)
            {
                if (first)
                {
                    cmd.Parameters.AddWithValue("@ScanTableId", NpgsqlTypes.NpgsqlDbType.Integer, ScanForeignKey);
                    first = false;
                }
                cmd.Parameters.AddWithValue(WellItem.ParameterName, WellItem.dbType.Value, WellItem.value);
                cmd.CommandText += WellItem.ParameterName;



                int Index = WellTableInsert.LastIndexOf(",");
                cmd.CommandText.Remove(Index, 1);
                cmd.CommandText += "@FileId)";
                ScanForeignKey = ScanForeignKey + 1;
                first = true;
                cmd.Prepare();
                cmd.ExecuteNonQuery();
            }
        }
        catch //(NpgsqlException ex)
        {
            return -1;
        }
        return 1;
    }

    public static int InsertToFileTable(String fileName, NpgsqlConnection conn)
    {

        String FileSqlStatment = @"INSERT INTO ""datalevel"".""FileData""(""FileIdName"") Values(@FileIdName) RETURNING ""Id""";
        try
        {
            String File = Path.GetFileName(fileName);
            using var cmd = new NpgsqlCommand(FileSqlStatment, conn);
            cmd.Parameters.AddWithValue("@FileIdName", NpgsqlDbType.Text, File);
            int fileForeignKey = (int)cmd.ExecuteScalar();
            return fileForeignKey;
        }
        catch (NpgsqlException ex)
        {
            if ("FileNameUnique" == ex.Data["ConstraintName"].ToString() || ex.Data["ConstraintName"].ToString() == "FileIdUnique")
            {
                return 1;
            }
            else
            {
                throw ex;
            }
        }
    }

    public static int InsertNewColumn(NpgsqlConnection conn, string Column, string Value, string filename)
    {
        try
        {


            String val = Value;
            object o = SpreadSheetTools.ParseOrDefault(Value);
            Type type = o.GetType();
            String MeasurementSqlStatement = $@"INSERT INTO ""NonMeasurementTableName"".""NonMeasurementSchemaName"" 
                            VALUES (@FileIdName, @Column_Name, @Column_Type, @Text_Value)";
            String fileName = Path.GetFileName(filename);
            using var command = new NpgsqlCommand(MeasurementSqlStatement, conn);
            command.Parameters.AddWithValue("@FileIdName", NpgsqlDbType.Text, fileName);
            command.Parameters.AddWithValue("@Column_Name", NpgsqlDbType.Text, Column);
            command.Parameters.AddWithValue("@Column_Type", NpgsqlDbType.Text, type.Name);
            command.Parameters.AddWithValue("@Text_Value", NpgsqlDbType.Text, Value);
            command.Prepare();
            command.ExecuteNonQuery();
            return 1;

        }
        catch (NpgsqlException ex)
        {
            return -1;
        }

        return 1;
    }

    public static int InsertToFovTable(IEnumerable<NonMeasurementRow> FovList, NpgsqlConnection conn, int WellForeignKey)
    {
        String FovTableinsert = @"INSERT INTO ""ExperimentData"".""FOV_Table""
                        (""WellTableId"", ""Fov"" ""ImageName"", ""Focus z Pos"", ""Focus Z Pos Avg of Adjacent Fields"", ""Acquisition Time Stamp"" ""CELLSPERFIELD"") 
    VALUES(@WellTableId";

        int iterator = 0;
        using var cmd = new NpgsqlCommand(FovTableinsert, conn);
        try
        {
            foreach (NonMeasurementRow row in FovList)
            {
                cmd.Parameters.AddWithValue("@WellTableId", NpgsqlDbType.Integer, WellForeignKey);
                cmd.Parameters.AddWithValue(row.ParameterName, row.dbType.Value, row.value);
                cmd.CommandText += row.ParameterName;
            }

            int Index = FovTableinsert.LastIndexOf(",");
            cmd.CommandText.Remove(Index, 1);
            cmd.CommandText += ")";
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            WellForeignKey = WellForeignKey + iterator;
            return 1;
        }
        catch (NpgsqlException ex)
        {
            return -1;
        }
    }

    public static int InsertToCellTable(IEnumerable<NonMeasurementRow> CellRows, NpgsqlConnection conn, int CellForeignKey)
    {

        String CellDataInsert = @"INSERT INTO ""ExperimentData"".""Cell_Table""(""FOVTableId"", ""ObjectId"", ""Well_X_um"", ""Well_Y_um"",""Plate_X_um"", ""Plate_Y_um"", ""MLCELLSUSE"", ""INPUTROWINDEX"", ""MLTHRESHOLD"", ""MLC1CELLULAR"", ""MITOORGINTEN"",  ""FileId"")
	                 VALUES (@FOVTableId";

        using var cmd = new NpgsqlCommand(CellDataInsert, conn);
        try
        {
            foreach (NonMeasurementRow row in CellRows)
            {
                cmd.Parameters.AddWithValue(row.ParameterName, row.dbType.Value, row.value);
                cmd.CommandText += row.ParameterName;

                int index = CellDataInsert.LastIndexOf(",");

                cmd.CommandText.Remove(index, 1);
                cmd.CommandText += ")";
                cmd.Prepare();
                cmd.ExecuteNonQuery();
                CellForeignKey = CellForeignKey + 1;
            }
            return 1;
        }
        catch (NpgsqlException ex)
        {
            return -1;
        }
    }

    static int InsertToRaftTable(IEnumerable<NonMeasurementRow> RaftDataInsert, NpgsqlConnection conn, List<int> FovKeys)
    {
        String RaftSqlData =
        @"INSERT INTO ""ExperimentData"".""Raft_Table""(""FovKeys"", ""raftId"", ""raftrow"", ""raftcol"", ""umfromborderofraft"", ""imagecheck"", ""mlquad"", ""mlfiduciaryraft"", ""mlimagecheck"", ""mlc0allowedregion"", ""mlvalidrafts"", ""mlcellsperraft"", ""mlc2allowedperraft"")              
                    VALUES(@FovKeys";
        using var cmd = new NpgsqlCommand(RaftSqlData, conn);
        try
        {
            foreach (NonMeasurementRow row in RaftDataInsert)
            {
                cmd.Parameters.AddWithValue(row.ParameterName, row.dbType.Value, row.value);
                cmd.CommandText += row.ParameterName;
            }
            int FinalIndex = RaftSqlData.LastIndexOf(",");

            cmd.CommandText.Remove(FinalIndex, 1);
            cmd.CommandText += ")";
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            List<int> NewKeys = new List<int>();
            foreach (int key in FovKeys)
            {
                FovKeys.Add(key + 1); ;
            }
            return 1;
        }
        catch (NpgsqlException ex)
        {
            return -1;
        }
    }

    public static int InsertToMeasurementTable(List<MeasurementRow> measurementRow, NpgsqlConnection conn, int FileId)
    {
        String MeasurementSqlStatement = $@"INSERT INTO
                    ""MeasurementData"".""Measurement_Table""(
                    ""FileId"",
                    ""FeatureId"", 
                    ""MaskId"", 
                    ""WaveLengthId"", 
                    ""MeasurementValues"")                                         
                    Values(@FileId, @FeatureId, @MaskId, @WavelengthId, @MeasurementValues)";


        using var cmd = new NpgsqlCommand(MeasurementSqlStatement, conn);
        foreach (MeasurementRow row in measurementRow)
        {
            try
            {
                cmd.Parameters.AddWithValue("@FileId", NpgsqlDbType.Integer, FileId);
                cmd.Parameters.AddWithValue("@FeatureId", NpgsqlDbType.Integer, row.Feature);
                cmd.Parameters.AddWithValue("@MaskId", NpgsqlDbType.Integer, row.MaskKey);
                cmd.Parameters.AddWithValue("@WavelengthId", NpgsqlDbType.Integer, row.WavelengthKey);
                cmd.Parameters.AddWithValue("@MeasurementValues", NpgsqlDbType.Double, row.DoubleValue);
                cmd.Prepare();
                cmd.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {

            }

        }
        return -1;
    }



}