﻿
namespace FIVE.RaftCal
{
    partial class MultiInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txBx1 = new System.Windows.Forms.TextBox();
            this.txBx2 = new System.Windows.Forms.TextBox();
            this.txBx3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txBx4 = new System.Windows.Forms.TextBox();
            this.txBx5 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txBx1
            // 
            this.txBx1.Location = new System.Drawing.Point(24, 39);
            this.txBx1.Name = "txBx1";
            this.txBx1.Size = new System.Drawing.Size(344, 20);
            this.txBx1.TabIndex = 0;
            // 
            // txBx2
            // 
            this.txBx2.Location = new System.Drawing.Point(24, 87);
            this.txBx2.Name = "txBx2";
            this.txBx2.Size = new System.Drawing.Size(145, 20);
            this.txBx2.TabIndex = 1;
            // 
            // txBx3
            // 
            this.txBx3.Location = new System.Drawing.Point(24, 136);
            this.txBx3.Name = "txBx3";
            this.txBx3.Size = new System.Drawing.Size(145, 20);
            this.txBx3.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "label3";
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(24, 184);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.TabIndex = 6;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(293, 184);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(226, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(226, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "label5";
            // 
            // txBx4
            // 
            this.txBx4.Location = new System.Drawing.Point(223, 87);
            this.txBx4.Name = "txBx4";
            this.txBx4.Size = new System.Drawing.Size(145, 20);
            this.txBx4.TabIndex = 9;
            // 
            // txBx5
            // 
            this.txBx5.Location = new System.Drawing.Point(223, 136);
            this.txBx5.Name = "txBx5";
            this.txBx5.Size = new System.Drawing.Size(145, 20);
            this.txBx5.TabIndex = 8;
            // 
            // MultiInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 229);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txBx4);
            this.Controls.Add(this.txBx5);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txBx3);
            this.Controls.Add(this.txBx2);
            this.Controls.Add(this.txBx1);
            this.Name = "MultiInputForm";
            this.Text = "MultiInputForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txBx1;
        public System.Windows.Forms.TextBox txBx2;
        public System.Windows.Forms.TextBox txBx3;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Button button_Cancel;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txBx4;
        public System.Windows.Forms.TextBox txBx5;
    }
}