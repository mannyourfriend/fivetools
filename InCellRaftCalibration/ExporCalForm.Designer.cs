﻿
namespace FIVE.RaftCal
{
    partial class ExportCalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ExportFolderBase = new System.Windows.Forms.TextBox();
            this.FileName_ByAnnotation = new System.Windows.Forms.CheckBox();
            this.btn_ExportAllRaftImages = new System.Windows.Forms.Button();
            this.btn_ExportFromList = new System.Windows.Forms.Button();
            this.btn_ExportRaftImages = new System.Windows.Forms.Button();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.FolderName_ByAnnotation = new System.Windows.Forms.CheckBox();
            this.btn_RaftIDsAnnotated = new System.Windows.Forms.Button();
            this.ImageExtension = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Resize_And_MLExport = new System.Windows.Forms.CheckBox();
            this.Export_4Rotations = new System.Windows.Forms.CheckBox();
            this.OnlyExportSquare = new System.Windows.Forms.CheckBox();
            this.PixelList_ExportSize = new System.Windows.Forms.TextBox();
            this.ResizeMultiplier = new System.Windows.Forms.TextBox();
            this.Min_AspectRatio_Keep = new System.Windows.Forms.TextBox();
            this.PixelsExpand = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_SaveSettings = new System.Windows.Forms.Button();
            this.panelExportSettings = new System.Windows.Forms.Panel();
            this.ColumnNameForAnnotation = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.DownsampleFraction = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CellImageExpandPixels = new System.Windows.Forms.TextBox();
            this.OnlyExportFullSize = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CropToMaxSize = new System.Windows.Forms.TextBox();
            this.Include_Fiducial_Rafts = new System.Windows.Forms.CheckBox();
            this.btn_LoadSettings = new System.Windows.Forms.Button();
            this.btn_SaveAsSettings = new System.Windows.Forms.Button();
            this.btn_DefaultSettings = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.bgMain = new System.ComponentModel.BackgroundWorker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Annotations_Load = new System.Windows.Forms.Button();
            this.btn_Annotations_New = new System.Windows.Forms.Button();
            this.btn_Annotations_SaveAs = new System.Windows.Forms.Button();
            this.txBx_AnnotationsCurrentSet = new System.Windows.Forms.TextBox();
            this.txBx_AnnotationsInfo = new System.Windows.Forms.TextBox();
            this.panel_Annotations = new System.Windows.Forms.Panel();
            this.btn_Annotations_Open = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btn_Import_Annotations = new System.Windows.Forms.Button();
            this.btn_ExportCropReg_Objects = new System.Windows.Forms.Button();
            this.btn_ExpStitchFOV = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.FractionUnAnnotated = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panelExportSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel_Annotations.SuspendLayout();
            this.SuspendLayout();
            // 
            // ExportFolderBase
            // 
            this.ExportFolderBase.Location = new System.Drawing.Point(12, 23);
            this.ExportFolderBase.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ExportFolderBase.Name = "ExportFolderBase";
            this.ExportFolderBase.Size = new System.Drawing.Size(474, 23);
            this.ExportFolderBase.TabIndex = 0;
            this.ExportFolderBase.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // FileName_ByAnnotation
            // 
            this.FileName_ByAnnotation.AutoSize = true;
            this.FileName_ByAnnotation.Location = new System.Drawing.Point(250, 81);
            this.FileName_ByAnnotation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.FileName_ByAnnotation.Name = "FileName_ByAnnotation";
            this.FileName_ByAnnotation.Size = new System.Drawing.Size(154, 19);
            this.FileName_ByAnnotation.TabIndex = 1;
            this.FileName_ByAnnotation.Text = "FileName_ByAnnotation";
            this.FileName_ByAnnotation.UseVisualStyleBackColor = true;
            this.FileName_ByAnnotation.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // btn_ExportAllRaftImages
            // 
            this.btn_ExportAllRaftImages.Location = new System.Drawing.Point(369, 430);
            this.btn_ExportAllRaftImages.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_ExportAllRaftImages.Name = "btn_ExportAllRaftImages";
            this.btn_ExportAllRaftImages.Size = new System.Drawing.Size(160, 28);
            this.btn_ExportAllRaftImages.TabIndex = 95;
            this.btn_ExportAllRaftImages.Text = "Export All Raft Images";
            this.btn_ExportAllRaftImages.UseVisualStyleBackColor = true;
            this.btn_ExportAllRaftImages.Click += new System.EventHandler(this.btn_ExportAllRaftImages_Click);
            // 
            // btn_ExportFromList
            // 
            this.btn_ExportFromList.Location = new System.Drawing.Point(369, 396);
            this.btn_ExportFromList.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_ExportFromList.Name = "btn_ExportFromList";
            this.btn_ExportFromList.Size = new System.Drawing.Size(160, 28);
            this.btn_ExportFromList.TabIndex = 94;
            this.btn_ExportFromList.Text = "Export From List . . ";
            this.btn_ExportFromList.UseVisualStyleBackColor = true;
            this.btn_ExportFromList.Click += new System.EventHandler(this.btn_ExportFromList_Click);
            // 
            // btn_ExportRaftImages
            // 
            this.btn_ExportRaftImages.Location = new System.Drawing.Point(369, 500);
            this.btn_ExportRaftImages.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_ExportRaftImages.Name = "btn_ExportRaftImages";
            this.btn_ExportRaftImages.Size = new System.Drawing.Size(160, 28);
            this.btn_ExportRaftImages.TabIndex = 93;
            this.btn_ExportRaftImages.Text = "Export Annotated Rafts";
            this.btn_ExportRaftImages.UseVisualStyleBackColor = true;
            this.btn_ExportRaftImages.Click += new System.EventHandler(this.btn_ExportRaftImages_Click);
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(8, 396);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(353, 320);
            this.txBx_Update.TabIndex = 96;
            // 
            // FolderName_ByAnnotation
            // 
            this.FolderName_ByAnnotation.AutoSize = true;
            this.FolderName_ByAnnotation.Location = new System.Drawing.Point(250, 57);
            this.FolderName_ByAnnotation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.FolderName_ByAnnotation.Name = "FolderName_ByAnnotation";
            this.FolderName_ByAnnotation.Size = new System.Drawing.Size(169, 19);
            this.FolderName_ByAnnotation.TabIndex = 97;
            this.FolderName_ByAnnotation.Text = "FolderName_ByAnnotation";
            this.FolderName_ByAnnotation.UseVisualStyleBackColor = true;
            this.FolderName_ByAnnotation.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // btn_RaftIDsAnnotated
            // 
            this.btn_RaftIDsAnnotated.Location = new System.Drawing.Point(369, 687);
            this.btn_RaftIDsAnnotated.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_RaftIDsAnnotated.Name = "btn_RaftIDsAnnotated";
            this.btn_RaftIDsAnnotated.Size = new System.Drawing.Size(160, 28);
            this.btn_RaftIDsAnnotated.TabIndex = 98;
            this.btn_RaftIDsAnnotated.Text = "Annotations to Clipboard";
            this.btn_RaftIDsAnnotated.UseVisualStyleBackColor = true;
            this.btn_RaftIDsAnnotated.Click += new System.EventHandler(this.btn_PlateMetadata);
            // 
            // ImageExtension
            // 
            this.ImageExtension.Location = new System.Drawing.Point(12, 70);
            this.ImageExtension.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ImageExtension.Name = "ImageExtension";
            this.ImageExtension.Size = new System.Drawing.Size(170, 23);
            this.ImageExtension.TabIndex = 99;
            this.ImageExtension.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 15);
            this.label1.TabIndex = 100;
            this.label1.Text = "Export Folder Base";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 101;
            this.label2.Text = "Image Type";
            // 
            // Resize_And_MLExport
            // 
            this.Resize_And_MLExport.AutoSize = true;
            this.Resize_And_MLExport.Enabled = false;
            this.Resize_And_MLExport.Location = new System.Drawing.Point(250, 118);
            this.Resize_And_MLExport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Resize_And_MLExport.Name = "Resize_And_MLExport";
            this.Resize_And_MLExport.Size = new System.Drawing.Size(141, 19);
            this.Resize_And_MLExport.TabIndex = 103;
            this.Resize_And_MLExport.Text = "Resize_And_MLExport";
            this.Resize_And_MLExport.UseVisualStyleBackColor = true;
            this.Resize_And_MLExport.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // Export_4Rotations
            // 
            this.Export_4Rotations.AutoSize = true;
            this.Export_4Rotations.Location = new System.Drawing.Point(250, 191);
            this.Export_4Rotations.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Export_4Rotations.Name = "Export_4Rotations";
            this.Export_4Rotations.Size = new System.Drawing.Size(121, 19);
            this.Export_4Rotations.TabIndex = 102;
            this.Export_4Rotations.Text = "Export_4Rotations";
            this.Export_4Rotations.UseVisualStyleBackColor = true;
            this.Export_4Rotations.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // OnlyExportSquare
            // 
            this.OnlyExportSquare.AutoSize = true;
            this.OnlyExportSquare.Enabled = false;
            this.OnlyExportSquare.Location = new System.Drawing.Point(250, 143);
            this.OnlyExportSquare.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.OnlyExportSquare.Name = "OnlyExportSquare";
            this.OnlyExportSquare.Size = new System.Drawing.Size(121, 19);
            this.OnlyExportSquare.TabIndex = 104;
            this.OnlyExportSquare.Text = "OnlyExportSquare";
            this.OnlyExportSquare.UseVisualStyleBackColor = true;
            this.OnlyExportSquare.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // PixelList_ExportSize
            // 
            this.PixelList_ExportSize.Enabled = false;
            this.PixelList_ExportSize.Location = new System.Drawing.Point(12, 103);
            this.PixelList_ExportSize.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.PixelList_ExportSize.Name = "PixelList_ExportSize";
            this.PixelList_ExportSize.Size = new System.Drawing.Size(88, 23);
            this.PixelList_ExportSize.TabIndex = 105;
            this.PixelList_ExportSize.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // ResizeMultiplier
            // 
            this.ResizeMultiplier.Location = new System.Drawing.Point(12, 186);
            this.ResizeMultiplier.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ResizeMultiplier.Name = "ResizeMultiplier";
            this.ResizeMultiplier.Size = new System.Drawing.Size(88, 23);
            this.ResizeMultiplier.TabIndex = 106;
            this.ResizeMultiplier.Text = "11";
            this.ResizeMultiplier.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // Min_AspectRatio_Keep
            // 
            this.Min_AspectRatio_Keep.Location = new System.Drawing.Point(12, 158);
            this.Min_AspectRatio_Keep.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Min_AspectRatio_Keep.Name = "Min_AspectRatio_Keep";
            this.Min_AspectRatio_Keep.Size = new System.Drawing.Size(88, 23);
            this.Min_AspectRatio_Keep.TabIndex = 107;
            this.Min_AspectRatio_Keep.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // PixelsExpand
            // 
            this.PixelsExpand.Enabled = false;
            this.PixelsExpand.Location = new System.Drawing.Point(12, 130);
            this.PixelsExpand.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.PixelsExpand.Name = "PixelsExpand";
            this.PixelsExpand.Size = new System.Drawing.Size(88, 23);
            this.PixelsExpand.TabIndex = 108;
            this.PixelsExpand.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(107, 106);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 15);
            this.label3.TabIndex = 109;
            this.label3.Text = "PixelList_ExportSize";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(107, 134);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 15);
            this.label4.TabIndex = 110;
            this.label4.Text = "PixelsExpand";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(107, 162);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 15);
            this.label5.TabIndex = 111;
            this.label5.Text = "Min_AspectRatio_Keep";
            this.toolTip1.SetToolTip(this.label5, "If the aspect ratio is worse than this number (lower), it is not exported. 1 = pe" +
        "rfect square, 0 = straight line");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(107, 189);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 15);
            this.label6.TabIndex = 112;
            this.label6.Text = "Resize Multiplier";
            this.toolTip1.SetToolTip(this.label6, "If you enter 0.5, the new image will be half the size of the original. If you ent" +
        "er 2, it is like zooming in if you use the cropping below. Resize is first, crop" +
        " is second");
            // 
            // btn_SaveSettings
            // 
            this.btn_SaveSettings.Location = new System.Drawing.Point(12, 270);
            this.btn_SaveSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_SaveSettings.Name = "btn_SaveSettings";
            this.btn_SaveSettings.Size = new System.Drawing.Size(126, 28);
            this.btn_SaveSettings.TabIndex = 113;
            this.btn_SaveSettings.Text = "Save Settings";
            this.toolTip1.SetToolTip(this.btn_SaveSettings, "Locks in the setting typed above so they can be applied to this export");
            this.btn_SaveSettings.UseVisualStyleBackColor = true;
            this.btn_SaveSettings.Click += new System.EventHandler(this.btn_SaveSettings_Click);
            // 
            // panelExportSettings
            // 
            this.panelExportSettings.Controls.Add(this.label10);
            this.panelExportSettings.Controls.Add(this.FractionUnAnnotated);
            this.panelExportSettings.Controls.Add(this.ColumnNameForAnnotation);
            this.panelExportSettings.Controls.Add(this.label9);
            this.panelExportSettings.Controls.Add(this.DownsampleFraction);
            this.panelExportSettings.Controls.Add(this.label8);
            this.panelExportSettings.Controls.Add(this.CellImageExpandPixels);
            this.panelExportSettings.Controls.Add(this.OnlyExportFullSize);
            this.panelExportSettings.Controls.Add(this.label7);
            this.panelExportSettings.Controls.Add(this.CropToMaxSize);
            this.panelExportSettings.Controls.Add(this.Include_Fiducial_Rafts);
            this.panelExportSettings.Controls.Add(this.btn_LoadSettings);
            this.panelExportSettings.Controls.Add(this.btn_SaveAsSettings);
            this.panelExportSettings.Controls.Add(this.btn_DefaultSettings);
            this.panelExportSettings.Controls.Add(this.PixelList_ExportSize);
            this.panelExportSettings.Controls.Add(this.btn_SaveSettings);
            this.panelExportSettings.Controls.Add(this.ResizeMultiplier);
            this.panelExportSettings.Controls.Add(this.OnlyExportSquare);
            this.panelExportSettings.Controls.Add(this.label6);
            this.panelExportSettings.Controls.Add(this.Resize_And_MLExport);
            this.panelExportSettings.Controls.Add(this.Min_AspectRatio_Keep);
            this.panelExportSettings.Controls.Add(this.Export_4Rotations);
            this.panelExportSettings.Controls.Add(this.label5);
            this.panelExportSettings.Controls.Add(this.FolderName_ByAnnotation);
            this.panelExportSettings.Controls.Add(this.label2);
            this.panelExportSettings.Controls.Add(this.PixelsExpand);
            this.panelExportSettings.Controls.Add(this.label1);
            this.panelExportSettings.Controls.Add(this.label4);
            this.panelExportSettings.Controls.Add(this.ImageExtension);
            this.panelExportSettings.Controls.Add(this.FileName_ByAnnotation);
            this.panelExportSettings.Controls.Add(this.label3);
            this.panelExportSettings.Controls.Add(this.ExportFolderBase);
            this.panelExportSettings.Location = new System.Drawing.Point(8, 87);
            this.panelExportSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelExportSettings.Name = "panelExportSettings";
            this.panelExportSettings.Size = new System.Drawing.Size(520, 302);
            this.panelExportSettings.TabIndex = 114;
            // 
            // ColumnNameForAnnotation
            // 
            this.ColumnNameForAnnotation.Location = new System.Drawing.Point(416, 68);
            this.ColumnNameForAnnotation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ColumnNameForAnnotation.Name = "ColumnNameForAnnotation";
            this.ColumnNameForAnnotation.Size = new System.Drawing.Size(99, 23);
            this.ColumnNameForAnnotation.TabIndex = 125;
            this.ColumnNameForAnnotation.TextChanged += new System.EventHandler(this.ColumnNameForAnnotation_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(361, 246);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(142, 15);
            this.label9.TabIndex = 124;
            this.label9.Text = "Cell Downsampling (1 all)";
            this.toolTip1.SetToolTip(this.label9, "Enter -1 to disable, will crop the image (only smaller) so the dimensions are thi" +
        "s number in pixels");
            // 
            // DownsampleFraction
            // 
            this.DownsampleFraction.Location = new System.Drawing.Point(265, 242);
            this.DownsampleFraction.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DownsampleFraction.Name = "DownsampleFraction";
            this.DownsampleFraction.Size = new System.Drawing.Size(88, 23);
            this.DownsampleFraction.TabIndex = 123;
            this.DownsampleFraction.TextChanged += new System.EventHandler(this.DownsampleFraction_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(108, 245);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 15);
            this.label8.TabIndex = 122;
            this.label8.Text = "CellImage Expand Pixels";
            this.toolTip1.SetToolTip(this.label8, "Enter -1 to disable, will crop the image (only smaller) so the dimensions are thi" +
        "s number in pixels");
            // 
            // CellImageExpandPixels
            // 
            this.CellImageExpandPixels.Location = new System.Drawing.Point(12, 241);
            this.CellImageExpandPixels.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.CellImageExpandPixels.Name = "CellImageExpandPixels";
            this.CellImageExpandPixels.Size = new System.Drawing.Size(88, 23);
            this.CellImageExpandPixels.TabIndex = 121;
            this.CellImageExpandPixels.TextChanged += new System.EventHandler(this.CellImageExpandPixels_TextChanged);
            // 
            // OnlyExportFullSize
            // 
            this.OnlyExportFullSize.AutoSize = true;
            this.OnlyExportFullSize.Location = new System.Drawing.Point(250, 167);
            this.OnlyExportFullSize.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.OnlyExportFullSize.Name = "OnlyExportFullSize";
            this.OnlyExportFullSize.Size = new System.Drawing.Size(124, 19);
            this.OnlyExportFullSize.TabIndex = 120;
            this.OnlyExportFullSize.Text = "OnlyExportFullSize";
            this.OnlyExportFullSize.UseVisualStyleBackColor = true;
            this.OnlyExportFullSize.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(108, 217);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 15);
            this.label7.TabIndex = 119;
            this.label7.Text = "CropToMaxSize";
            this.toolTip1.SetToolTip(this.label7, "Enter -1 to disable, will crop the image (only smaller) so the dimensions are thi" +
        "s number in pixels");
            // 
            // CropToMaxSize
            // 
            this.CropToMaxSize.Location = new System.Drawing.Point(12, 213);
            this.CropToMaxSize.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.CropToMaxSize.Name = "CropToMaxSize";
            this.CropToMaxSize.Size = new System.Drawing.Size(88, 23);
            this.CropToMaxSize.TabIndex = 118;
            this.CropToMaxSize.TextChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // Include_Fiducial_Rafts
            // 
            this.Include_Fiducial_Rafts.AutoSize = true;
            this.Include_Fiducial_Rafts.Location = new System.Drawing.Point(250, 215);
            this.Include_Fiducial_Rafts.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Include_Fiducial_Rafts.Name = "Include_Fiducial_Rafts";
            this.Include_Fiducial_Rafts.Size = new System.Drawing.Size(142, 19);
            this.Include_Fiducial_Rafts.TabIndex = 117;
            this.Include_Fiducial_Rafts.Text = "Include_Fiducial_Rafts";
            this.Include_Fiducial_Rafts.UseVisualStyleBackColor = true;
            this.Include_Fiducial_Rafts.CheckedChanged += new System.EventHandler(this.Form_Settings_Changed);
            // 
            // btn_LoadSettings
            // 
            this.btn_LoadSettings.Location = new System.Drawing.Point(416, 270);
            this.btn_LoadSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_LoadSettings.Name = "btn_LoadSettings";
            this.btn_LoadSettings.Size = new System.Drawing.Size(82, 28);
            this.btn_LoadSettings.TabIndex = 116;
            this.btn_LoadSettings.Text = "Load . .";
            this.btn_LoadSettings.UseVisualStyleBackColor = true;
            this.btn_LoadSettings.Click += new System.EventHandler(this.btn_LoadSettings_Click);
            // 
            // btn_SaveAsSettings
            // 
            this.btn_SaveAsSettings.Location = new System.Drawing.Point(328, 270);
            this.btn_SaveAsSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_SaveAsSettings.Name = "btn_SaveAsSettings";
            this.btn_SaveAsSettings.Size = new System.Drawing.Size(82, 28);
            this.btn_SaveAsSettings.TabIndex = 115;
            this.btn_SaveAsSettings.Text = "Save As . .";
            this.btn_SaveAsSettings.UseVisualStyleBackColor = true;
            this.btn_SaveAsSettings.Click += new System.EventHandler(this.btn_SaveAsSettings_Click);
            // 
            // btn_DefaultSettings
            // 
            this.btn_DefaultSettings.Location = new System.Drawing.Point(250, 270);
            this.btn_DefaultSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_DefaultSettings.Name = "btn_DefaultSettings";
            this.btn_DefaultSettings.Size = new System.Drawing.Size(71, 28);
            this.btn_DefaultSettings.TabIndex = 114;
            this.btn_DefaultSettings.Text = "Defaults";
            this.btn_DefaultSettings.UseVisualStyleBackColor = true;
            this.btn_DefaultSettings.Click += new System.EventHandler(this.btn_DefaultSettings_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(369, 625);
            this.btn_Cancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(94, 28);
            this.btn_Cancel.TabIndex = 115;
            this.btn_Cancel.Text = "Stop / Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // bgMain
            // 
            this.bgMain.WorkerReportsProgress = true;
            this.bgMain.WorkerSupportsCancellation = true;
            this.bgMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgMain_DoWork);
            this.bgMain.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgMain_ProgressChanged);
            this.bgMain.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgMain_RunWorkerCompleted);
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(8, 7);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(520, 73);
            this.panel2.TabIndex = 116;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(4, 3);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(468, 181);
            this.dataGridView1.TabIndex = 0;
            // 
            // btn_Annotations_Load
            // 
            this.btn_Annotations_Load.Location = new System.Drawing.Point(92, 192);
            this.btn_Annotations_Load.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Annotations_Load.Name = "btn_Annotations_Load";
            this.btn_Annotations_Load.Size = new System.Drawing.Size(82, 28);
            this.btn_Annotations_Load.TabIndex = 119;
            this.btn_Annotations_Load.Text = "Load . .";
            this.btn_Annotations_Load.UseVisualStyleBackColor = true;
            this.btn_Annotations_Load.Click += new System.EventHandler(this.btn_Annotations_Load_Click);
            // 
            // btn_Annotations_New
            // 
            this.btn_Annotations_New.Enabled = false;
            this.btn_Annotations_New.Location = new System.Drawing.Point(4, 192);
            this.btn_Annotations_New.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Annotations_New.Name = "btn_Annotations_New";
            this.btn_Annotations_New.Size = new System.Drawing.Size(82, 28);
            this.btn_Annotations_New.TabIndex = 118;
            this.btn_Annotations_New.Text = "New";
            this.btn_Annotations_New.UseVisualStyleBackColor = true;
            this.btn_Annotations_New.Click += new System.EventHandler(this.btn_Annotations_New_Click);
            // 
            // btn_Annotations_SaveAs
            // 
            this.btn_Annotations_SaveAs.Location = new System.Drawing.Point(181, 192);
            this.btn_Annotations_SaveAs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Annotations_SaveAs.Name = "btn_Annotations_SaveAs";
            this.btn_Annotations_SaveAs.Size = new System.Drawing.Size(82, 28);
            this.btn_Annotations_SaveAs.TabIndex = 120;
            this.btn_Annotations_SaveAs.Text = "Save As . .";
            this.btn_Annotations_SaveAs.UseVisualStyleBackColor = true;
            this.btn_Annotations_SaveAs.Click += new System.EventHandler(this.btn_Annotations_SaveAs_Click);
            // 
            // txBx_AnnotationsCurrentSet
            // 
            this.txBx_AnnotationsCurrentSet.Location = new System.Drawing.Point(4, 226);
            this.txBx_AnnotationsCurrentSet.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_AnnotationsCurrentSet.Name = "txBx_AnnotationsCurrentSet";
            this.txBx_AnnotationsCurrentSet.ReadOnly = true;
            this.txBx_AnnotationsCurrentSet.Size = new System.Drawing.Size(467, 23);
            this.txBx_AnnotationsCurrentSet.TabIndex = 121;
            // 
            // txBx_AnnotationsInfo
            // 
            this.txBx_AnnotationsInfo.Location = new System.Drawing.Point(4, 256);
            this.txBx_AnnotationsInfo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_AnnotationsInfo.Multiline = true;
            this.txBx_AnnotationsInfo.Name = "txBx_AnnotationsInfo";
            this.txBx_AnnotationsInfo.Size = new System.Drawing.Size(467, 337);
            this.txBx_AnnotationsInfo.TabIndex = 122;
            // 
            // panel_Annotations
            // 
            this.panel_Annotations.Controls.Add(this.btn_Annotations_Open);
            this.panel_Annotations.Controls.Add(this.dataGridView1);
            this.panel_Annotations.Controls.Add(this.btn_Annotations_New);
            this.panel_Annotations.Controls.Add(this.txBx_AnnotationsInfo);
            this.panel_Annotations.Controls.Add(this.btn_Annotations_Load);
            this.panel_Annotations.Controls.Add(this.txBx_AnnotationsCurrentSet);
            this.panel_Annotations.Controls.Add(this.btn_Annotations_SaveAs);
            this.panel_Annotations.Location = new System.Drawing.Point(536, 7);
            this.panel_Annotations.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel_Annotations.Name = "panel_Annotations";
            this.panel_Annotations.Size = new System.Drawing.Size(478, 600);
            this.panel_Annotations.TabIndex = 118;
            // 
            // btn_Annotations_Open
            // 
            this.btn_Annotations_Open.Location = new System.Drawing.Point(270, 192);
            this.btn_Annotations_Open.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Annotations_Open.Name = "btn_Annotations_Open";
            this.btn_Annotations_Open.Size = new System.Drawing.Size(88, 28);
            this.btn_Annotations_Open.TabIndex = 123;
            this.btn_Annotations_Open.Text = "Open/Edit";
            this.btn_Annotations_Open.UseVisualStyleBackColor = true;
            this.btn_Annotations_Open.Click += new System.EventHandler(this.btn_Annotations_Open_Click);
            // 
            // btn_Import_Annotations
            // 
            this.btn_Import_Annotations.Location = new System.Drawing.Point(369, 655);
            this.btn_Import_Annotations.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Import_Annotations.Name = "btn_Import_Annotations";
            this.btn_Import_Annotations.Size = new System.Drawing.Size(126, 28);
            this.btn_Import_Annotations.TabIndex = 119;
            this.btn_Import_Annotations.Text = "Import Annotations";
            this.toolTip1.SetToolTip(this.btn_Import_Annotations, "This will append the annotations you select from a file into this PlateID. Select" +
        " a .txt file that has 6 columns = PlateID, RaftID, Name, Value, Well Label, FOV");
            this.btn_Import_Annotations.UseVisualStyleBackColor = true;
            this.btn_Import_Annotations.Click += new System.EventHandler(this.btn_Import_Annotations_Click);
            // 
            // btn_ExportCropReg_Objects
            // 
            this.btn_ExportCropReg_Objects.Location = new System.Drawing.Point(369, 465);
            this.btn_ExportCropReg_Objects.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_ExportCropReg_Objects.Name = "btn_ExportCropReg_Objects";
            this.btn_ExportCropReg_Objects.Size = new System.Drawing.Size(160, 28);
            this.btn_ExportCropReg_Objects.TabIndex = 120;
            this.btn_ExportCropReg_Objects.Text = "Export All Seg Images";
            this.toolTip1.SetToolTip(this.btn_ExportCropReg_Objects, "Uses the Segmentation Settings and \"OBJ\" selection to crop out around a single nu" +
        "clei or object");
            this.btn_ExportCropReg_Objects.UseVisualStyleBackColor = true;
            this.btn_ExportCropReg_Objects.Click += new System.EventHandler(this.btn_ExportCropRegions_Objects_Click);
            // 
            // btn_ExpStitchFOV
            // 
            this.btn_ExpStitchFOV.Location = new System.Drawing.Point(422, 534);
            this.btn_ExpStitchFOV.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_ExpStitchFOV.Name = "btn_ExpStitchFOV";
            this.btn_ExpStitchFOV.Size = new System.Drawing.Size(106, 28);
            this.btn_ExpStitchFOV.TabIndex = 121;
            this.btn_ExpStitchFOV.Text = "Exp Stitch Fov";
            this.btn_ExpStitchFOV.UseVisualStyleBackColor = true;
            this.btn_ExpStitchFOV.Click += new System.EventHandler(this.btn_ExportStitchFOV);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(369, 539);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(46, 23);
            this.textBox1.TabIndex = 121;
            this.textBox1.Text = "4";
            // 
            // FractionUnAnnotated
            // 
            this.FractionUnAnnotated.Location = new System.Drawing.Point(426, 176);
            this.FractionUnAnnotated.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.FractionUnAnnotated.Name = "FractionUnAnnotated";
            this.FractionUnAnnotated.Size = new System.Drawing.Size(88, 23);
            this.FractionUnAnnotated.TabIndex = 126;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(428, 143);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 30);
            this.label10.TabIndex = 127;
            this.label10.Text = "Fraction\r\nUnAnnotated";
            this.toolTip1.SetToolTip(this.label10, "Enter -1 to disable, will crop the image (only smaller) so the dimensions are thi" +
        "s number in pixels");
            // 
            // ExportCalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 764);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_ExpStitchFOV);
            this.Controls.Add(this.btn_ExportCropReg_Objects);
            this.Controls.Add(this.btn_Import_Annotations);
            this.Controls.Add(this.panel_Annotations);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.panelExportSettings);
            this.Controls.Add(this.btn_RaftIDsAnnotated);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.btn_ExportAllRaftImages);
            this.Controls.Add(this.btn_ExportFromList);
            this.Controls.Add(this.btn_ExportRaftImages);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "ExportCalForm";
            this.Text = "ExporCalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExporCalForm_FormClosing);
            this.Load += new System.EventHandler(this.ExporCalForm_Load);
            this.panelExportSettings.ResumeLayout(false);
            this.panelExportSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel_Annotations.ResumeLayout(false);
            this.panel_Annotations.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ExportFolderBase;
        private System.Windows.Forms.CheckBox FileName_ByAnnotation;
        private System.Windows.Forms.Button btn_ExportAllRaftImages;
        private System.Windows.Forms.Button btn_ExportFromList;
        private System.Windows.Forms.Button btn_ExportRaftImages;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.CheckBox FolderName_ByAnnotation;
        private System.Windows.Forms.Button btn_RaftIDsAnnotated;
        private System.Windows.Forms.TextBox ImageExtension;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox Resize_And_MLExport;
        private System.Windows.Forms.CheckBox Export_4Rotations;
        private System.Windows.Forms.CheckBox OnlyExportSquare;
        private System.Windows.Forms.TextBox PixelList_ExportSize;
        private System.Windows.Forms.TextBox ResizeMultiplier;
        private System.Windows.Forms.TextBox Min_AspectRatio_Keep;
        private System.Windows.Forms.TextBox PixelsExpand;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_SaveSettings;
        private System.Windows.Forms.Panel panelExportSettings;
        private System.Windows.Forms.Button btn_Cancel;
        private System.ComponentModel.BackgroundWorker bgMain;
        private System.Windows.Forms.Button btn_LoadSettings;
        private System.Windows.Forms.Button btn_SaveAsSettings;
        private System.Windows.Forms.Button btn_DefaultSettings;
        private System.Windows.Forms.CheckBox Include_Fiducial_Rafts;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Annotations_New;
        private System.Windows.Forms.Button btn_Annotations_Load;
        private System.Windows.Forms.Button btn_Annotations_SaveAs;
        private System.Windows.Forms.TextBox txBx_AnnotationsCurrentSet;
        private System.Windows.Forms.TextBox txBx_AnnotationsInfo;
        private System.Windows.Forms.Panel panel_Annotations;
        private System.Windows.Forms.Button btn_Annotations_Open;
        private System.Windows.Forms.TextBox CropToMaxSize;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox OnlyExportFullSize;
        private System.Windows.Forms.Button btn_Import_Annotations;
        private System.Windows.Forms.Button btn_ExportCropReg_Objects;
        private System.Windows.Forms.Button btn_ExpStitchFOV;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox CellImageExpandPixels;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox DownsampleFraction;
        private System.Windows.Forms.TextBox ColumnNameForAnnotation;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox FractionUnAnnotated;
    }
}