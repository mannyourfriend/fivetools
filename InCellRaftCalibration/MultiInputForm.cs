﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE.RaftCal
{
    public partial class MultiInputForm : Form
    {
        public MultiInputForm()
        {
            InitializeComponent();
        }

        public static DialogResult ShowMulti(string Title, out Tuple<string, string, string, string, string> Results, string Caption1, string Default1, string Caption2 = "", string Default2 = "", string Caption3 = "", string Default3 = "", string Caption4 = "", string Default4 = "", string Caption5 = "", string Default5 = "")
        {
            var th = new MultiInputForm();
            th.Text = Title;
            th.label1.Text = Caption1; th.label2.Text = Caption2; th.label3.Text = Caption3; th.label4.Text = Caption4; th.label5.Text = Caption5;
            th.txBx1.Text = Default1; th.txBx2.Text = Default2; th.txBx3.Text = Default3; th.txBx4.Text = Default4; th.txBx5.Text = Default5;

            th.label3.Visible = th.txBx3.Visible = (Caption3 != "");
            th.label4.Visible = th.txBx4.Visible = (Caption4 != "");
            th.label5.Visible = th.txBx5.Visible = (Caption5 != "");

            DialogResult Dr = th.ShowDialog();
            Results = new Tuple<string, string, string, string, string>(th.txBx1.Text, th.txBx2.Text, th.txBx3.Text, th.txBx4.Text, th.txBx5.Text);
            return Dr;
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
