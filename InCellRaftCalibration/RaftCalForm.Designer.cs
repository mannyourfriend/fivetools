﻿namespace FIVE.RaftCal
{
    partial class FormRaftCal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRaftCal));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txBx_FolderPath = new System.Windows.Forms.TextBox();
            this.btn_Update = new System.Windows.Forms.Button();
            this.txBx_ImageName = new System.Windows.Forms.TextBox();
            this.txBx_FOV = new System.Windows.Forms.TextBox();
            this.txBx_BrightnessMultiplier1 = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txBx_Row = new System.Windows.Forms.TextBox();
            this.txBx_Col = new System.Windows.Forms.TextBox();
            this.btn_Up = new System.Windows.Forms.Button();
            this.btn_Down = new System.Windows.Forms.Button();
            this.btn_Left = new System.Windows.Forms.Button();
            this.btn_Right = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txBx_Wavelength1 = new System.Windows.Forms.TextBox();
            this.btn_U5 = new System.Windows.Forms.Button();
            this.btn_D5 = new System.Windows.Forms.Button();
            this.btn_L5 = new System.Windows.Forms.Button();
            this.btn_R5 = new System.Windows.Forms.Button();
            this.labl_ImageName = new System.Windows.Forms.Label();
            this.txBx_SavedList = new System.Windows.Forms.TextBox();
            this.txBx_RaftID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txBx_LocationInfo = new System.Windows.Forms.TextBox();
            this.label_Notify = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnFieldSnake = new System.Windows.Forms.Button();
            this.btn_RaftNext = new System.Windows.Forms.Button();
            this.btn_RaftPrev = new System.Windows.Forms.Button();
            this.btnFieldNext = new System.Windows.Forms.Button();
            this.btnFieldBack = new System.Windows.Forms.Button();
            this.btn_BL = new System.Windows.Forms.Button();
            this.btn_UR = new System.Windows.Forms.Button();
            this.btn_BR = new System.Windows.Forms.Button();
            this.btn_UL = new System.Windows.Forms.Button();
            this.btn_SaveCalibration = new System.Windows.Forms.Button();
            this.label_Warning = new System.Windows.Forms.Label();
            this.radioButton_TestCal = new System.Windows.Forms.RadioButton();
            this.radioButton_RecordFocus = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_SaveCheck = new System.Windows.Forms.Button();
            this.comboBox_Well = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.lblImageCheck = new System.Windows.Forms.Label();
            this.btnResetImageCheck = new System.Windows.Forms.Button();
            this.btnResetSaveCal = new System.Windows.Forms.Button();
            this.chkBx_SquareMode = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btn_Size512 = new System.Windows.Forms.Button();
            this.panelControls = new System.Windows.Forms.Panel();
            this.txBx_ClipExpand_Neg = new System.Windows.Forms.TextBox();
            this.txBx_ClipExpand_Pos = new System.Windows.Forms.TextBox();
            this.chk_Bx_ObjClipsVis = new System.Windows.Forms.CheckBox();
            this.txBx_Color_Overlay = new System.Windows.Forms.TextBox();
            this.comboBox_ColorMerge = new System.Windows.Forms.ComboBox();
            this.chkBx_ShowCalGrid = new System.Windows.Forms.CheckBox();
            this.btnJumpNextCalPoint = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.chkBx_DI_5 = new System.Windows.Forms.CheckBox();
            this.chkBx_DI_4 = new System.Windows.Forms.CheckBox();
            this.chkBx_DI_3 = new System.Windows.Forms.CheckBox();
            this.chkBx_DI_2 = new System.Windows.Forms.CheckBox();
            this.chkBx_DI_1 = new System.Windows.Forms.CheckBox();
            this.btnRunSegmentation = new System.Windows.Forms.Button();
            this.txBx_Wv_Thresh5 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Thresh4 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Thresh3 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Thresh2 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Thresh1 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.btn_SaveWavelengthInfo = new System.Windows.Forms.Button();
            this.txBx_WavelengthExtended = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txBx_Wv_Abbrev5 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Name5 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Abbrev4 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Name4 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Abbrev3 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Name3 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Abbrev2 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Name2 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Abbrev1 = new System.Windows.Forms.TextBox();
            this.txBx_Wv_Name1 = new System.Windows.Forms.TextBox();
            this.chkBx_ObjDetect5 = new System.Windows.Forms.CheckBox();
            this.chkBx_Onwv5 = new System.Windows.Forms.CheckBox();
            this.txBx_Color5 = new System.Windows.Forms.TextBox();
            this.txBx_BrightnessMultiplier5 = new System.Windows.Forms.TextBox();
            this.txBx_Wavelength5 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chkBx_ObjDetect4 = new System.Windows.Forms.CheckBox();
            this.chkBx_ObjDetect3 = new System.Windows.Forms.CheckBox();
            this.chkBx_ObjDetect2 = new System.Windows.Forms.CheckBox();
            this.chkBx_ObjDetect1 = new System.Windows.Forms.CheckBox();
            this.lbl_xyi = new System.Windows.Forms.Label();
            this.lbl_SegTest = new System.Windows.Forms.Label();
            this.pictureBox_Zoom = new System.Windows.Forms.PictureBox();
            this.btn_ExportCurrentImageView = new System.Windows.Forms.Button();
            this.txBx_FOV_LR = new System.Windows.Forms.TextBox();
            this.txBx_FOV_UR = new System.Windows.Forms.TextBox();
            this.txBx_FOV_LL = new System.Windows.Forms.TextBox();
            this.Leica_Label = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chkBx_Onwv4 = new System.Windows.Forms.CheckBox();
            this.chkBx_Onwv3 = new System.Windows.Forms.CheckBox();
            this.chkBx_Onwv2 = new System.Windows.Forms.CheckBox();
            this.txBx_Color4 = new System.Windows.Forms.TextBox();
            this.txBx_Color3 = new System.Windows.Forms.TextBox();
            this.txBx_Color2 = new System.Windows.Forms.TextBox();
            this.txBx_Color1 = new System.Windows.Forms.TextBox();
            this.chkBx_Onwv1 = new System.Windows.Forms.CheckBox();
            this.txBx_BrightnessMultiplier4 = new System.Windows.Forms.TextBox();
            this.txBx_Wavelength4 = new System.Windows.Forms.TextBox();
            this.txBx_BrightnessMultiplier3 = new System.Windows.Forms.TextBox();
            this.txBx_Wavelength3 = new System.Windows.Forms.TextBox();
            this.txBx_BrightnessMultiplier2 = new System.Windows.Forms.TextBox();
            this.txBx_Wavelength2 = new System.Windows.Forms.TextBox();
            this.btn_ExportFromList = new System.Windows.Forms.Button();
            this.btnUndoImageCheck = new System.Windows.Forms.Button();
            this.comboBox_Overlay = new System.Windows.Forms.ComboBox();
            this.chkBx_ShowOverlay = new System.Windows.Forms.CheckBox();
            this.btnSize300 = new System.Windows.Forms.Button();
            this.btnSize700 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btn_Finish = new System.Windows.Forms.Button();
            this.bgWorker_ImageLoad = new System.ComponentModel.BackgroundWorker();
            this.bgWorker_Analysis1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Zoom)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox1.Location = new System.Drawing.Point(7, 7);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(350, 346);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // txBx_FolderPath
            // 
            this.txBx_FolderPath.Location = new System.Drawing.Point(2, 31);
            this.txBx_FolderPath.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_FolderPath.Name = "txBx_FolderPath";
            this.txBx_FolderPath.ReadOnly = true;
            this.txBx_FolderPath.Size = new System.Drawing.Size(446, 23);
            this.txBx_FolderPath.TabIndex = 1;
            this.txBx_FolderPath.DoubleClick += new System.EventHandler(this.txBx_FolderPath_DoubleClick);
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(352, 228);
            this.btn_Update.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(95, 33);
            this.btn_Update.TabIndex = 2;
            this.btn_Update.Text = "Update";
            this.toolTip1.SetToolTip(this.btn_Update, "Reload the images with current settings");
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.Update_Display);
            // 
            // txBx_ImageName
            // 
            this.txBx_ImageName.Enabled = false;
            this.txBx_ImageName.Location = new System.Drawing.Point(89, 740);
            this.txBx_ImageName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ImageName.Name = "txBx_ImageName";
            this.txBx_ImageName.Size = new System.Drawing.Size(240, 23);
            this.txBx_ImageName.TabIndex = 3;
            // 
            // txBx_FOV
            // 
            this.txBx_FOV.Enabled = false;
            this.txBx_FOV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txBx_FOV.Location = new System.Drawing.Point(9, 710);
            this.txBx_FOV.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_FOV.Name = "txBx_FOV";
            this.txBx_FOV.Size = new System.Drawing.Size(33, 20);
            this.txBx_FOV.TabIndex = 4;
            // 
            // txBx_BrightnessMultiplier1
            // 
            this.txBx_BrightnessMultiplier1.Location = new System.Drawing.Point(240, 78);
            this.txBx_BrightnessMultiplier1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_BrightnessMultiplier1.Name = "txBx_BrightnessMultiplier1";
            this.txBx_BrightnessMultiplier1.Size = new System.Drawing.Size(46, 23);
            this.txBx_BrightnessMultiplier1.TabIndex = 5;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox2.Location = new System.Drawing.Point(364, 7);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(350, 346);
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox3.Location = new System.Drawing.Point(7, 360);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(350, 346);
            this.pictureBox3.TabIndex = 9;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox4.Location = new System.Drawing.Point(364, 360);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(350, 346);
            this.pictureBox4.TabIndex = 8;
            this.pictureBox4.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 691);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "FOV";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(245, 60);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 15);
            this.label2.TabIndex = 11;
            this.label2.Text = "Bright";
            this.toolTip1.SetToolTip(this.label2, "Multiplier, can be a decimal");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 15);
            this.label3.TabIndex = 12;
            this.label3.Text = "InCell Folder";
            // 
            // txBx_Row
            // 
            this.txBx_Row.Location = new System.Drawing.Point(156, 280);
            this.txBx_Row.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Row.Name = "txBx_Row";
            this.txBx_Row.Size = new System.Drawing.Size(59, 23);
            this.txBx_Row.TabIndex = 13;
            // 
            // txBx_Col
            // 
            this.txBx_Col.Location = new System.Drawing.Point(222, 280);
            this.txBx_Col.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Col.Name = "txBx_Col";
            this.txBx_Col.Size = new System.Drawing.Size(59, 23);
            this.txBx_Col.TabIndex = 14;
            // 
            // btn_Up
            // 
            this.btn_Up.Location = new System.Drawing.Point(69, 30);
            this.btn_Up.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Up.Name = "btn_Up";
            this.btn_Up.Size = new System.Drawing.Size(56, 23);
            this.btn_Up.TabIndex = 15;
            this.btn_Up.Text = "Up";
            this.btn_Up.UseVisualStyleBackColor = true;
            this.btn_Up.Click += new System.EventHandler(this.btn_Up_Click);
            // 
            // btn_Down
            // 
            this.btn_Down.Location = new System.Drawing.Point(69, 85);
            this.btn_Down.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Down.Name = "btn_Down";
            this.btn_Down.Size = new System.Drawing.Size(56, 23);
            this.btn_Down.TabIndex = 16;
            this.btn_Down.Text = "Down";
            this.btn_Down.UseVisualStyleBackColor = true;
            this.btn_Down.Click += new System.EventHandler(this.btn_Down_Click);
            // 
            // btn_Left
            // 
            this.btn_Left.Location = new System.Drawing.Point(48, 58);
            this.btn_Left.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Left.Name = "btn_Left";
            this.btn_Left.Size = new System.Drawing.Size(46, 23);
            this.btn_Left.TabIndex = 17;
            this.btn_Left.Text = "Left";
            this.btn_Left.UseVisualStyleBackColor = true;
            this.btn_Left.Click += new System.EventHandler(this.btn_Left_Click);
            // 
            // btn_Right
            // 
            this.btn_Right.Location = new System.Drawing.Point(94, 58);
            this.btn_Right.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Right.Name = "btn_Right";
            this.btn_Right.Size = new System.Drawing.Size(56, 23);
            this.btn_Right.TabIndex = 18;
            this.btn_Right.Text = "Right";
            this.btn_Right.UseVisualStyleBackColor = true;
            this.btn_Right.Click += new System.EventHandler(this.btn_Right_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(156, 262);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 15);
            this.label4.TabIndex = 19;
            this.label4.Text = "Row";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(224, 262);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 15);
            this.label5.TabIndex = 20;
            this.label5.Text = "Column";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(209, 60);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 15);
            this.label6.TabIndex = 22;
            this.label6.Text = "wv";
            this.toolTip1.SetToolTip(this.label6, "This is 0-indexed");
            // 
            // txBx_Wavelength1
            // 
            this.txBx_Wavelength1.Location = new System.Drawing.Point(206, 78);
            this.txBx_Wavelength1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wavelength1.Name = "txBx_Wavelength1";
            this.txBx_Wavelength1.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wavelength1.TabIndex = 21;
            // 
            // btn_U5
            // 
            this.btn_U5.Location = new System.Drawing.Point(78, 2);
            this.btn_U5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_U5.Name = "btn_U5";
            this.btn_U5.Size = new System.Drawing.Size(37, 23);
            this.btn_U5.TabIndex = 23;
            this.btn_U5.Text = "U5";
            this.btn_U5.UseVisualStyleBackColor = true;
            this.btn_U5.Click += new System.EventHandler(this.btn_U5_Click);
            // 
            // btn_D5
            // 
            this.btn_D5.Location = new System.Drawing.Point(78, 112);
            this.btn_D5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_D5.Name = "btn_D5";
            this.btn_D5.Size = new System.Drawing.Size(37, 23);
            this.btn_D5.TabIndex = 24;
            this.btn_D5.Text = "D5";
            this.btn_D5.UseVisualStyleBackColor = true;
            this.btn_D5.Click += new System.EventHandler(this.btn_D5_Click);
            // 
            // btn_L5
            // 
            this.btn_L5.Location = new System.Drawing.Point(4, 58);
            this.btn_L5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_L5.Name = "btn_L5";
            this.btn_L5.Size = new System.Drawing.Size(37, 23);
            this.btn_L5.TabIndex = 25;
            this.btn_L5.Text = "L5";
            this.btn_L5.UseVisualStyleBackColor = true;
            this.btn_L5.Click += new System.EventHandler(this.btn_L5_Click);
            // 
            // btn_R5
            // 
            this.btn_R5.Location = new System.Drawing.Point(156, 58);
            this.btn_R5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_R5.Name = "btn_R5";
            this.btn_R5.Size = new System.Drawing.Size(37, 23);
            this.btn_R5.TabIndex = 26;
            this.btn_R5.Text = "R5";
            this.btn_R5.UseVisualStyleBackColor = true;
            this.btn_R5.Click += new System.EventHandler(this.btn_R5_Click);
            // 
            // labl_ImageName
            // 
            this.labl_ImageName.AutoSize = true;
            this.labl_ImageName.Location = new System.Drawing.Point(91, 721);
            this.labl_ImageName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labl_ImageName.Name = "labl_ImageName";
            this.labl_ImageName.Size = new System.Drawing.Size(75, 15);
            this.labl_ImageName.TabIndex = 27;
            this.labl_ImageName.Text = "Image Name";
            // 
            // txBx_SavedList
            // 
            this.txBx_SavedList.Location = new System.Drawing.Point(105, 518);
            this.txBx_SavedList.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_SavedList.Multiline = true;
            this.txBx_SavedList.Name = "txBx_SavedList";
            this.txBx_SavedList.Size = new System.Drawing.Size(340, 55);
            this.txBx_SavedList.TabIndex = 28;
            this.txBx_SavedList.DoubleClick += new System.EventHandler(this.txBx_SavedList_DoubleClick);
            // 
            // txBx_RaftID
            // 
            this.txBx_RaftID.Location = new System.Drawing.Point(4, 518);
            this.txBx_RaftID.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_RaftID.Name = "txBx_RaftID";
            this.txBx_RaftID.Size = new System.Drawing.Size(80, 23);
            this.txBx_RaftID.TabIndex = 29;
            this.toolTip1.SetToolTip(this.txBx_RaftID, resources.GetString("txBx_RaftID.ToolTip"));
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(6, 543);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 24);
            this.label7.TabIndex = 30;
            this.label7.Text = "Type the RaftID,\r\nthen Click to Save.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(329, 721);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 15);
            this.label9.TabIndex = 27;
            this.label9.Text = "Plate Location (um)";
            // 
            // txBx_LocationInfo
            // 
            this.txBx_LocationInfo.Enabled = false;
            this.txBx_LocationInfo.Location = new System.Drawing.Point(336, 740);
            this.txBx_LocationInfo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_LocationInfo.Name = "txBx_LocationInfo";
            this.txBx_LocationInfo.Size = new System.Drawing.Size(112, 23);
            this.txBx_LocationInfo.TabIndex = 32;
            // 
            // label_Notify
            // 
            this.label_Notify.AutoSize = true;
            this.label_Notify.Location = new System.Drawing.Point(243, 9);
            this.label_Notify.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Notify.Name = "label_Notify";
            this.label_Notify.Size = new System.Drawing.Size(42, 15);
            this.label_Notify.TabIndex = 33;
            this.label_Notify.Text = "( - - - )";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnFieldSnake);
            this.panel1.Controls.Add(this.btn_RaftNext);
            this.panel1.Controls.Add(this.btn_RaftPrev);
            this.panel1.Controls.Add(this.btnFieldNext);
            this.panel1.Controls.Add(this.btnFieldBack);
            this.panel1.Controls.Add(this.btn_BL);
            this.panel1.Controls.Add(this.btn_UR);
            this.panel1.Controls.Add(this.btn_BR);
            this.panel1.Controls.Add(this.btn_UL);
            this.panel1.Controls.Add(this.btn_Right);
            this.panel1.Controls.Add(this.btn_Up);
            this.panel1.Controls.Add(this.btn_Down);
            this.panel1.Controls.Add(this.btn_Left);
            this.panel1.Controls.Add(this.btn_U5);
            this.panel1.Controls.Add(this.btn_D5);
            this.panel1.Controls.Add(this.btn_L5);
            this.panel1.Controls.Add(this.btn_R5);
            this.panel1.Location = new System.Drawing.Point(246, 377);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(197, 137);
            this.panel1.TabIndex = 34;
            // 
            // btnFieldSnake
            // 
            this.btnFieldSnake.Location = new System.Drawing.Point(122, 112);
            this.btnFieldSnake.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnFieldSnake.Name = "btnFieldSnake";
            this.btnFieldSnake.Size = new System.Drawing.Size(26, 23);
            this.btnFieldSnake.TabIndex = 35;
            this.btnFieldSnake.Text = "~";
            this.btnFieldSnake.UseVisualStyleBackColor = true;
            this.btnFieldSnake.Click += new System.EventHandler(this.btnFieldSnake_Click);
            // 
            // btn_RaftNext
            // 
            this.btn_RaftNext.Location = new System.Drawing.Point(132, 30);
            this.btn_RaftNext.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_RaftNext.Name = "btn_RaftNext";
            this.btn_RaftNext.Size = new System.Drawing.Size(37, 23);
            this.btn_RaftNext.TabIndex = 34;
            this.btn_RaftNext.Text = ">";
            this.btn_RaftNext.UseVisualStyleBackColor = true;
            this.btn_RaftNext.Click += new System.EventHandler(this.btn_RaftNext_Click);
            // 
            // btn_RaftPrev
            // 
            this.btn_RaftPrev.Location = new System.Drawing.Point(24, 30);
            this.btn_RaftPrev.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_RaftPrev.Name = "btn_RaftPrev";
            this.btn_RaftPrev.Size = new System.Drawing.Size(37, 23);
            this.btn_RaftPrev.TabIndex = 33;
            this.btn_RaftPrev.Text = "<";
            this.btn_RaftPrev.UseVisualStyleBackColor = true;
            this.btn_RaftPrev.Click += new System.EventHandler(this.btn_RaftPrev_Click);
            // 
            // btnFieldNext
            // 
            this.btnFieldNext.Location = new System.Drawing.Point(132, 85);
            this.btnFieldNext.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnFieldNext.Name = "btnFieldNext";
            this.btnFieldNext.Size = new System.Drawing.Size(37, 23);
            this.btnFieldNext.TabIndex = 32;
            this.btnFieldNext.Text = ">";
            this.btnFieldNext.UseVisualStyleBackColor = true;
            this.btnFieldNext.Click += new System.EventHandler(this.btnFieldNext_Click);
            // 
            // btnFieldBack
            // 
            this.btnFieldBack.Location = new System.Drawing.Point(24, 85);
            this.btnFieldBack.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnFieldBack.Name = "btnFieldBack";
            this.btnFieldBack.Size = new System.Drawing.Size(37, 23);
            this.btnFieldBack.TabIndex = 31;
            this.btnFieldBack.Text = "<";
            this.btnFieldBack.UseVisualStyleBackColor = true;
            this.btnFieldBack.Click += new System.EventHandler(this.btnFieldBack_Click);
            // 
            // btn_BL
            // 
            this.btn_BL.Location = new System.Drawing.Point(4, 112);
            this.btn_BL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_BL.Name = "btn_BL";
            this.btn_BL.Size = new System.Drawing.Size(37, 23);
            this.btn_BL.TabIndex = 30;
            this.btn_BL.Text = "BL";
            this.btn_BL.UseVisualStyleBackColor = true;
            this.btn_BL.Click += new System.EventHandler(this.btn_BL_Click);
            // 
            // btn_UR
            // 
            this.btn_UR.Location = new System.Drawing.Point(156, 2);
            this.btn_UR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_UR.Name = "btn_UR";
            this.btn_UR.Size = new System.Drawing.Size(37, 23);
            this.btn_UR.TabIndex = 29;
            this.btn_UR.Text = "UR";
            this.btn_UR.UseVisualStyleBackColor = true;
            this.btn_UR.Click += new System.EventHandler(this.btn_UR_Click);
            // 
            // btn_BR
            // 
            this.btn_BR.Location = new System.Drawing.Point(156, 112);
            this.btn_BR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_BR.Name = "btn_BR";
            this.btn_BR.Size = new System.Drawing.Size(37, 23);
            this.btn_BR.TabIndex = 28;
            this.btn_BR.Text = "BR";
            this.btn_BR.UseVisualStyleBackColor = true;
            this.btn_BR.Click += new System.EventHandler(this.btn_BR_Click);
            // 
            // btn_UL
            // 
            this.btn_UL.Location = new System.Drawing.Point(4, 2);
            this.btn_UL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_UL.Name = "btn_UL";
            this.btn_UL.Size = new System.Drawing.Size(37, 23);
            this.btn_UL.TabIndex = 27;
            this.btn_UL.Text = "UL";
            this.btn_UL.UseVisualStyleBackColor = true;
            this.btn_UL.Click += new System.EventHandler(this.btn_UL_Click);
            // 
            // btn_SaveCalibration
            // 
            this.btn_SaveCalibration.Enabled = false;
            this.btn_SaveCalibration.Location = new System.Drawing.Point(134, 642);
            this.btn_SaveCalibration.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_SaveCalibration.Name = "btn_SaveCalibration";
            this.btn_SaveCalibration.Size = new System.Drawing.Size(88, 52);
            this.btn_SaveCalibration.TabIndex = 35;
            this.btn_SaveCalibration.Text = "Save Calibration";
            this.btn_SaveCalibration.UseVisualStyleBackColor = true;
            this.btn_SaveCalibration.Click += new System.EventHandler(this.btn_SaveCalibration_Click);
            // 
            // label_Warning
            // 
            this.label_Warning.AutoSize = true;
            this.label_Warning.ForeColor = System.Drawing.Color.Green;
            this.label_Warning.Location = new System.Drawing.Point(20, 597);
            this.label_Warning.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Warning.Name = "label_Warning";
            this.label_Warning.Size = new System.Drawing.Size(96, 15);
            this.label_Warning.TabIndex = 36;
            this.label_Warning.Text = "Reset Calibration";
            // 
            // radioButton_TestCal
            // 
            this.radioButton_TestCal.AutoSize = true;
            this.radioButton_TestCal.Enabled = false;
            this.radioButton_TestCal.Location = new System.Drawing.Point(19, 642);
            this.radioButton_TestCal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.radioButton_TestCal.Name = "radioButton_TestCal";
            this.radioButton_TestCal.Size = new System.Drawing.Size(104, 19);
            this.radioButton_TestCal.TabIndex = 37;
            this.radioButton_TestCal.Text = "See Calibration";
            this.radioButton_TestCal.UseVisualStyleBackColor = true;
            this.radioButton_TestCal.CheckedChanged += new System.EventHandler(this.radioButton_TestCal_CheckedChanged);
            // 
            // radioButton_RecordFocus
            // 
            this.radioButton_RecordFocus.AutoSize = true;
            this.radioButton_RecordFocus.Checked = true;
            this.radioButton_RecordFocus.Enabled = false;
            this.radioButton_RecordFocus.Location = new System.Drawing.Point(318, 618);
            this.radioButton_RecordFocus.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.radioButton_RecordFocus.Name = "radioButton_RecordFocus";
            this.radioButton_RecordFocus.Size = new System.Drawing.Size(74, 19);
            this.radioButton_RecordFocus.TabIndex = 38;
            this.radioButton_RecordFocus.TabStop = true;
            this.radioButton_RecordFocus.Text = "Annotate";
            this.toolTip1.SetToolTip(this.radioButton_RecordFocus, "Hover over the images, then press a number key to set an annotation. Then Click. " +
        "Use the \"Exports\" option above to edit the schema");
            this.radioButton_RecordFocus.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(253, 575);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(167, 15);
            this.label11.TabIndex = 39;
            this.label11.Text = "Left Click = Good, Right = Bad";
            // 
            // btn_SaveCheck
            // 
            this.btn_SaveCheck.Location = new System.Drawing.Point(314, 642);
            this.btn_SaveCheck.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_SaveCheck.Name = "btn_SaveCheck";
            this.btn_SaveCheck.Size = new System.Drawing.Size(132, 52);
            this.btn_SaveCheck.TabIndex = 40;
            this.btn_SaveCheck.Text = "Save Image Check";
            this.btn_SaveCheck.UseVisualStyleBackColor = true;
            this.btn_SaveCheck.Click += new System.EventHandler(this.btn_SaveCheck_Click);
            // 
            // comboBox_Well
            // 
            this.comboBox_Well.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Well.FormattingEnabled = true;
            this.comboBox_Well.Location = new System.Drawing.Point(6, 280);
            this.comboBox_Well.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBox_Well.Name = "comboBox_Well";
            this.comboBox_Well.Size = new System.Drawing.Size(140, 23);
            this.comboBox_Well.TabIndex = 41;
            this.comboBox_Well.SelectedIndexChanged += new System.EventHandler(this.comboBox_Well_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(229, 582);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(12, 150);
            this.panel2.TabIndex = 42;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 575);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 15);
            this.label12.TabIndex = 43;
            this.label12.Text = "Raft Calibration Only:";
            // 
            // lblImageCheck
            // 
            this.lblImageCheck.AutoSize = true;
            this.lblImageCheck.ForeColor = System.Drawing.Color.Green;
            this.lblImageCheck.Location = new System.Drawing.Point(252, 597);
            this.lblImageCheck.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblImageCheck.Name = "lblImageCheck";
            this.lblImageCheck.Size = new System.Drawing.Size(107, 15);
            this.lblImageCheck.TabIndex = 44;
            this.lblImageCheck.Text = "Reset Image Check";
            this.lblImageCheck.Click += new System.EventHandler(this.lblImageCheck_Click);
            // 
            // btnResetImageCheck
            // 
            this.btnResetImageCheck.Location = new System.Drawing.Point(257, 668);
            this.btnResetImageCheck.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnResetImageCheck.Name = "btnResetImageCheck";
            this.btnResetImageCheck.Size = new System.Drawing.Size(50, 25);
            this.btnResetImageCheck.TabIndex = 45;
            this.btnResetImageCheck.Text = "reset";
            this.btnResetImageCheck.UseVisualStyleBackColor = true;
            this.btnResetImageCheck.Click += new System.EventHandler(this.btnResetImageCheck_Click);
            // 
            // btnResetSaveCal
            // 
            this.btnResetSaveCal.Location = new System.Drawing.Point(79, 663);
            this.btnResetSaveCal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnResetSaveCal.Name = "btnResetSaveCal";
            this.btnResetSaveCal.Size = new System.Drawing.Size(50, 25);
            this.btnResetSaveCal.TabIndex = 46;
            this.btnResetSaveCal.Text = "reset";
            this.btnResetSaveCal.UseVisualStyleBackColor = true;
            this.btnResetSaveCal.Click += new System.EventHandler(this.btnResetCalibration);
            // 
            // chkBx_SquareMode
            // 
            this.chkBx_SquareMode.AutoSize = true;
            this.chkBx_SquareMode.Checked = true;
            this.chkBx_SquareMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBx_SquareMode.Location = new System.Drawing.Point(287, 294);
            this.chkBx_SquareMode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_SquareMode.Name = "chkBx_SquareMode";
            this.chkBx_SquareMode.Size = new System.Drawing.Size(96, 19);
            this.chkBx_SquareMode.TabIndex = 47;
            this.chkBx_SquareMode.Text = "Square Mode";
            this.toolTip1.SetToolTip(this.chkBx_SquareMode, "Square Mode - Assumes that the layout of the fields is in a uniform (usually rect" +
        "angular) pattern");
            this.chkBx_SquareMode.UseVisualStyleBackColor = true;
            this.chkBx_SquareMode.CheckedChanged += new System.EventHandler(this.chkBx_RaftToggle_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 262);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 15);
            this.label13.TabIndex = 48;
            this.label13.Text = "Well:";
            // 
            // btn_Size512
            // 
            this.btn_Size512.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_Size512.Location = new System.Drawing.Point(138, 5);
            this.btn_Size512.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Size512.Name = "btn_Size512";
            this.btn_Size512.Size = new System.Drawing.Size(50, 25);
            this.btn_Size512.TabIndex = 49;
            this.btn_Size512.Text = "512";
            this.toolTip1.SetToolTip(this.btn_Size512, "Single Image Size 512x512");
            this.btn_Size512.UseVisualStyleBackColor = true;
            this.btn_Size512.Click += new System.EventHandler(this.btn_FullSize_Click);
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.txBx_ClipExpand_Neg);
            this.panelControls.Controls.Add(this.txBx_ClipExpand_Pos);
            this.panelControls.Controls.Add(this.chk_Bx_ObjClipsVis);
            this.panelControls.Controls.Add(this.txBx_Color_Overlay);
            this.panelControls.Controls.Add(this.comboBox_ColorMerge);
            this.panelControls.Controls.Add(this.chkBx_ShowCalGrid);
            this.panelControls.Controls.Add(this.btnJumpNextCalPoint);
            this.panelControls.Controls.Add(this.label19);
            this.panelControls.Controls.Add(this.label18);
            this.panelControls.Controls.Add(this.chkBx_DI_5);
            this.panelControls.Controls.Add(this.chkBx_DI_4);
            this.panelControls.Controls.Add(this.chkBx_DI_3);
            this.panelControls.Controls.Add(this.chkBx_DI_2);
            this.panelControls.Controls.Add(this.chkBx_DI_1);
            this.panelControls.Controls.Add(this.btnRunSegmentation);
            this.panelControls.Controls.Add(this.txBx_Wv_Thresh5);
            this.panelControls.Controls.Add(this.txBx_Wv_Thresh4);
            this.panelControls.Controls.Add(this.txBx_Wv_Thresh3);
            this.panelControls.Controls.Add(this.txBx_Wv_Thresh2);
            this.panelControls.Controls.Add(this.txBx_Wv_Thresh1);
            this.panelControls.Controls.Add(this.label17);
            this.panelControls.Controls.Add(this.btn_SaveWavelengthInfo);
            this.panelControls.Controls.Add(this.txBx_WavelengthExtended);
            this.panelControls.Controls.Add(this.label16);
            this.panelControls.Controls.Add(this.label15);
            this.panelControls.Controls.Add(this.txBx_Wv_Abbrev5);
            this.panelControls.Controls.Add(this.txBx_Wv_Name5);
            this.panelControls.Controls.Add(this.txBx_Wv_Abbrev4);
            this.panelControls.Controls.Add(this.txBx_Wv_Name4);
            this.panelControls.Controls.Add(this.txBx_Wv_Abbrev3);
            this.panelControls.Controls.Add(this.txBx_Wv_Name3);
            this.panelControls.Controls.Add(this.txBx_Wv_Abbrev2);
            this.panelControls.Controls.Add(this.txBx_Wv_Name2);
            this.panelControls.Controls.Add(this.txBx_Wv_Abbrev1);
            this.panelControls.Controls.Add(this.txBx_Wv_Name1);
            this.panelControls.Controls.Add(this.chkBx_ObjDetect5);
            this.panelControls.Controls.Add(this.chkBx_Onwv5);
            this.panelControls.Controls.Add(this.txBx_Color5);
            this.panelControls.Controls.Add(this.txBx_BrightnessMultiplier5);
            this.panelControls.Controls.Add(this.txBx_Wavelength5);
            this.panelControls.Controls.Add(this.label14);
            this.panelControls.Controls.Add(this.label10);
            this.panelControls.Controls.Add(this.chkBx_ObjDetect4);
            this.panelControls.Controls.Add(this.chkBx_ObjDetect3);
            this.panelControls.Controls.Add(this.chkBx_ObjDetect2);
            this.panelControls.Controls.Add(this.chkBx_ObjDetect1);
            this.panelControls.Controls.Add(this.lbl_xyi);
            this.panelControls.Controls.Add(this.lbl_SegTest);
            this.panelControls.Controls.Add(this.pictureBox_Zoom);
            this.panelControls.Controls.Add(this.btn_ExportCurrentImageView);
            this.panelControls.Controls.Add(this.txBx_FOV_LR);
            this.panelControls.Controls.Add(this.txBx_FOV_UR);
            this.panelControls.Controls.Add(this.txBx_FOV_LL);
            this.panelControls.Controls.Add(this.Leica_Label);
            this.panelControls.Controls.Add(this.label8);
            this.panelControls.Controls.Add(this.chkBx_Onwv4);
            this.panelControls.Controls.Add(this.chkBx_Onwv3);
            this.panelControls.Controls.Add(this.chkBx_Onwv2);
            this.panelControls.Controls.Add(this.txBx_Color4);
            this.panelControls.Controls.Add(this.txBx_Color3);
            this.panelControls.Controls.Add(this.txBx_Color2);
            this.panelControls.Controls.Add(this.txBx_Color1);
            this.panelControls.Controls.Add(this.chkBx_Onwv1);
            this.panelControls.Controls.Add(this.txBx_BrightnessMultiplier4);
            this.panelControls.Controls.Add(this.txBx_Wavelength4);
            this.panelControls.Controls.Add(this.txBx_BrightnessMultiplier3);
            this.panelControls.Controls.Add(this.txBx_Wavelength3);
            this.panelControls.Controls.Add(this.txBx_BrightnessMultiplier2);
            this.panelControls.Controls.Add(this.txBx_Wavelength2);
            this.panelControls.Controls.Add(this.btn_ExportFromList);
            this.panelControls.Controls.Add(this.btnUndoImageCheck);
            this.panelControls.Controls.Add(this.comboBox_Overlay);
            this.panelControls.Controls.Add(this.chkBx_ShowOverlay);
            this.panelControls.Controls.Add(this.btnSize300);
            this.panelControls.Controls.Add(this.btnSize700);
            this.panelControls.Controls.Add(this.label3);
            this.panelControls.Controls.Add(this.btn_Size512);
            this.panelControls.Controls.Add(this.txBx_FolderPath);
            this.panelControls.Controls.Add(this.label13);
            this.panelControls.Controls.Add(this.btn_Update);
            this.panelControls.Controls.Add(this.chkBx_SquareMode);
            this.panelControls.Controls.Add(this.txBx_ImageName);
            this.panelControls.Controls.Add(this.btnResetSaveCal);
            this.panelControls.Controls.Add(this.txBx_FOV);
            this.panelControls.Controls.Add(this.btnResetImageCheck);
            this.panelControls.Controls.Add(this.txBx_BrightnessMultiplier1);
            this.panelControls.Controls.Add(this.lblImageCheck);
            this.panelControls.Controls.Add(this.label1);
            this.panelControls.Controls.Add(this.label12);
            this.panelControls.Controls.Add(this.label2);
            this.panelControls.Controls.Add(this.panel2);
            this.panelControls.Controls.Add(this.txBx_Row);
            this.panelControls.Controls.Add(this.comboBox_Well);
            this.panelControls.Controls.Add(this.txBx_Col);
            this.panelControls.Controls.Add(this.btn_SaveCheck);
            this.panelControls.Controls.Add(this.label4);
            this.panelControls.Controls.Add(this.label11);
            this.panelControls.Controls.Add(this.label5);
            this.panelControls.Controls.Add(this.radioButton_RecordFocus);
            this.panelControls.Controls.Add(this.txBx_Wavelength1);
            this.panelControls.Controls.Add(this.radioButton_TestCal);
            this.panelControls.Controls.Add(this.label6);
            this.panelControls.Controls.Add(this.label_Warning);
            this.panelControls.Controls.Add(this.labl_ImageName);
            this.panelControls.Controls.Add(this.label9);
            this.panelControls.Controls.Add(this.btn_SaveCalibration);
            this.panelControls.Controls.Add(this.txBx_SavedList);
            this.panelControls.Controls.Add(this.panel1);
            this.panelControls.Controls.Add(this.txBx_RaftID);
            this.panelControls.Controls.Add(this.label_Notify);
            this.panelControls.Controls.Add(this.label7);
            this.panelControls.Controls.Add(this.txBx_LocationInfo);
            this.panelControls.Location = new System.Drawing.Point(718, 1);
            this.panelControls.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(454, 771);
            this.panelControls.TabIndex = 50;
            // 
            // txBx_ClipExpand_Neg
            // 
            this.txBx_ClipExpand_Neg.Enabled = false;
            this.txBx_ClipExpand_Neg.Location = new System.Drawing.Point(407, 262);
            this.txBx_ClipExpand_Neg.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ClipExpand_Neg.Name = "txBx_ClipExpand_Neg";
            this.txBx_ClipExpand_Neg.Size = new System.Drawing.Size(28, 23);
            this.txBx_ClipExpand_Neg.TabIndex = 126;
            this.txBx_ClipExpand_Neg.Text = "0";
            this.toolTip1.SetToolTip(this.txBx_ClipExpand_Neg, "When Clip is turned on, this sets how far to expand the thresholded area before a" +
        "pplying the clip");
            // 
            // txBx_ClipExpand_Pos
            // 
            this.txBx_ClipExpand_Pos.Enabled = false;
            this.txBx_ClipExpand_Pos.Location = new System.Drawing.Point(375, 262);
            this.txBx_ClipExpand_Pos.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ClipExpand_Pos.Name = "txBx_ClipExpand_Pos";
            this.txBx_ClipExpand_Pos.Size = new System.Drawing.Size(28, 23);
            this.txBx_ClipExpand_Pos.TabIndex = 125;
            this.txBx_ClipExpand_Pos.Text = "0";
            this.toolTip1.SetToolTip(this.txBx_ClipExpand_Pos, "When Clip is turned on, this sets how far to expand the thresholded area before a" +
        "pplying the clip");
            // 
            // chk_Bx_ObjClipsVis
            // 
            this.chk_Bx_ObjClipsVis.AutoSize = true;
            this.chk_Bx_ObjClipsVis.Location = new System.Drawing.Point(287, 264);
            this.chk_Bx_ObjClipsVis.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chk_Bx_ObjClipsVis.Name = "chk_Bx_ObjClipsVis";
            this.chk_Bx_ObjClipsVis.Size = new System.Drawing.Size(85, 19);
            this.chk_Bx_ObjClipsVis.TabIndex = 124;
            this.chk_Bx_ObjClipsVis.Text = "Obj clip Vis";
            this.toolTip1.SetToolTip(this.chk_Bx_ObjClipsVis, resources.GetString("chk_Bx_ObjClipsVis.ToolTip"));
            this.chk_Bx_ObjClipsVis.UseVisualStyleBackColor = true;
            this.chk_Bx_ObjClipsVis.CheckedChanged += new System.EventHandler(this.chk_Bx_ObjClipsVis_CheckedChanged);
            // 
            // txBx_Color_Overlay
            // 
            this.txBx_Color_Overlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txBx_Color_Overlay.Location = new System.Drawing.Point(238, 236);
            this.txBx_Color_Overlay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Color_Overlay.Name = "txBx_Color_Overlay";
            this.txBx_Color_Overlay.Size = new System.Drawing.Size(50, 17);
            this.txBx_Color_Overlay.TabIndex = 123;
            this.txBx_Color_Overlay.Text = "FFFFFF";
            this.toolTip1.SetToolTip(this.txBx_Color_Overlay, "Use this to define how this channel\'s color looks. ");
            this.txBx_Color_Overlay.TextChanged += new System.EventHandler(this.txBx_Color_TextChanged);
            // 
            // comboBox_ColorMerge
            // 
            this.comboBox_ColorMerge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ColorMerge.Font = new System.Drawing.Font("Segoe UI", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comboBox_ColorMerge.FormattingEnabled = true;
            this.comboBox_ColorMerge.Location = new System.Drawing.Point(295, 235);
            this.comboBox_ColorMerge.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBox_ColorMerge.Name = "comboBox_ColorMerge";
            this.comboBox_ColorMerge.Size = new System.Drawing.Size(50, 19);
            this.comboBox_ColorMerge.TabIndex = 122;
            this.toolTip1.SetToolTip(this.comboBox_ColorMerge, "These options define how the colors above are merged together.  Add is the defaul" +
        "t.  Difference is the older way we did this.");
            // 
            // chkBx_ShowCalGrid
            // 
            this.chkBx_ShowCalGrid.AutoSize = true;
            this.chkBx_ShowCalGrid.Location = new System.Drawing.Point(257, 619);
            this.chkBx_ShowCalGrid.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_ShowCalGrid.Name = "chkBx_ShowCalGrid";
            this.chkBx_ShowCalGrid.Size = new System.Drawing.Size(48, 19);
            this.chkBx_ShowCalGrid.TabIndex = 121;
            this.chkBx_ShowCalGrid.Text = "Grid";
            this.toolTip1.SetToolTip(this.chkBx_ShowCalGrid, "Square Mode - Assumes that the layout of the fields is in a uniform (usually rect" +
        "angular) pattern");
            this.chkBx_ShowCalGrid.UseVisualStyleBackColor = true;
            this.chkBx_ShowCalGrid.CheckedChanged += new System.EventHandler(this.chkBx_ShowCalGrid_CheckedChanged);
            // 
            // btnJumpNextCalPoint
            // 
            this.btnJumpNextCalPoint.Location = new System.Drawing.Point(11, 663);
            this.btnJumpNextCalPoint.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnJumpNextCalPoint.Name = "btnJumpNextCalPoint";
            this.btnJumpNextCalPoint.Size = new System.Drawing.Size(64, 25);
            this.btnJumpNextCalPoint.TabIndex = 120;
            this.btnJumpNextCalPoint.Text = "next Cal";
            this.btnJumpNextCalPoint.UseVisualStyleBackColor = true;
            this.btnJumpNextCalPoint.Click += new System.EventHandler(this.btnJumpNextCalPoint_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(369, 594);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 30);
            this.label19.TabIndex = 119;
            this.label19.Text = "x = field bad\r\nz = reset field";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(385, 60);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(18, 15);
            this.label18.TabIndex = 118;
            this.label18.Text = "DI";
            // 
            // chkBx_DI_5
            // 
            this.chkBx_DI_5.AutoSize = true;
            this.chkBx_DI_5.Location = new System.Drawing.Point(388, 201);
            this.chkBx_DI_5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_DI_5.Name = "chkBx_DI_5";
            this.chkBx_DI_5.Size = new System.Drawing.Size(15, 14);
            this.chkBx_DI_5.TabIndex = 117;
            this.chkBx_DI_5.UseVisualStyleBackColor = true;
            // 
            // chkBx_DI_4
            // 
            this.chkBx_DI_4.AutoSize = true;
            this.chkBx_DI_4.Location = new System.Drawing.Point(388, 171);
            this.chkBx_DI_4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_DI_4.Name = "chkBx_DI_4";
            this.chkBx_DI_4.Size = new System.Drawing.Size(15, 14);
            this.chkBx_DI_4.TabIndex = 116;
            this.chkBx_DI_4.UseVisualStyleBackColor = true;
            // 
            // chkBx_DI_3
            // 
            this.chkBx_DI_3.AutoSize = true;
            this.chkBx_DI_3.Location = new System.Drawing.Point(388, 141);
            this.chkBx_DI_3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_DI_3.Name = "chkBx_DI_3";
            this.chkBx_DI_3.Size = new System.Drawing.Size(15, 14);
            this.chkBx_DI_3.TabIndex = 115;
            this.chkBx_DI_3.UseVisualStyleBackColor = true;
            // 
            // chkBx_DI_2
            // 
            this.chkBx_DI_2.AutoSize = true;
            this.chkBx_DI_2.Location = new System.Drawing.Point(388, 111);
            this.chkBx_DI_2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_DI_2.Name = "chkBx_DI_2";
            this.chkBx_DI_2.Size = new System.Drawing.Size(15, 14);
            this.chkBx_DI_2.TabIndex = 114;
            this.chkBx_DI_2.UseVisualStyleBackColor = true;
            // 
            // chkBx_DI_1
            // 
            this.chkBx_DI_1.AutoSize = true;
            this.chkBx_DI_1.Location = new System.Drawing.Point(388, 82);
            this.chkBx_DI_1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_DI_1.Name = "chkBx_DI_1";
            this.chkBx_DI_1.Size = new System.Drawing.Size(15, 14);
            this.chkBx_DI_1.TabIndex = 113;
            this.chkBx_DI_1.UseVisualStyleBackColor = true;
            // 
            // btnRunSegmentation
            // 
            this.btnRunSegmentation.Location = new System.Drawing.Point(329, 339);
            this.btnRunSegmentation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnRunSegmentation.Name = "btnRunSegmentation";
            this.btnRunSegmentation.Size = new System.Drawing.Size(113, 33);
            this.btnRunSegmentation.TabIndex = 112;
            this.btnRunSegmentation.Text = "Segmentation";
            this.toolTip1.SetToolTip(this.btnRunSegmentation, "Right click for settings, Left click to Run Segmentation and Export images.  Doub" +
        "le click Thresh first to test your object detection and get object area ranges.");
            this.btnRunSegmentation.UseVisualStyleBackColor = true;
            this.btnRunSegmentation.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnRunSegmentation_MouseUp);
            // 
            // txBx_Wv_Thresh5
            // 
            this.txBx_Wv_Thresh5.Location = new System.Drawing.Point(407, 198);
            this.txBx_Wv_Thresh5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Thresh5.Name = "txBx_Wv_Thresh5";
            this.txBx_Wv_Thresh5.Size = new System.Drawing.Size(40, 23);
            this.txBx_Wv_Thresh5.TabIndex = 111;
            this.toolTip1.SetToolTip(this.txBx_Wv_Thresh5, "Double click to preview");
            this.txBx_Wv_Thresh5.DoubleClick += new System.EventHandler(this.txBx_Wv_Thresh_DoubleClick);
            // 
            // txBx_Wv_Thresh4
            // 
            this.txBx_Wv_Thresh4.Location = new System.Drawing.Point(407, 168);
            this.txBx_Wv_Thresh4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Thresh4.Name = "txBx_Wv_Thresh4";
            this.txBx_Wv_Thresh4.Size = new System.Drawing.Size(40, 23);
            this.txBx_Wv_Thresh4.TabIndex = 110;
            this.toolTip1.SetToolTip(this.txBx_Wv_Thresh4, "Double click to preview");
            this.txBx_Wv_Thresh4.DoubleClick += new System.EventHandler(this.txBx_Wv_Thresh_DoubleClick);
            // 
            // txBx_Wv_Thresh3
            // 
            this.txBx_Wv_Thresh3.Location = new System.Drawing.Point(407, 138);
            this.txBx_Wv_Thresh3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Thresh3.Name = "txBx_Wv_Thresh3";
            this.txBx_Wv_Thresh3.Size = new System.Drawing.Size(40, 23);
            this.txBx_Wv_Thresh3.TabIndex = 109;
            this.toolTip1.SetToolTip(this.txBx_Wv_Thresh3, "Double click to preview");
            this.txBx_Wv_Thresh3.DoubleClick += new System.EventHandler(this.txBx_Wv_Thresh_DoubleClick);
            // 
            // txBx_Wv_Thresh2
            // 
            this.txBx_Wv_Thresh2.Location = new System.Drawing.Point(407, 108);
            this.txBx_Wv_Thresh2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Thresh2.Name = "txBx_Wv_Thresh2";
            this.txBx_Wv_Thresh2.Size = new System.Drawing.Size(40, 23);
            this.txBx_Wv_Thresh2.TabIndex = 108;
            this.toolTip1.SetToolTip(this.txBx_Wv_Thresh2, "Double click to preview");
            this.txBx_Wv_Thresh2.DoubleClick += new System.EventHandler(this.txBx_Wv_Thresh_DoubleClick);
            // 
            // txBx_Wv_Thresh1
            // 
            this.txBx_Wv_Thresh1.Location = new System.Drawing.Point(407, 78);
            this.txBx_Wv_Thresh1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Thresh1.Name = "txBx_Wv_Thresh1";
            this.txBx_Wv_Thresh1.Size = new System.Drawing.Size(40, 23);
            this.txBx_Wv_Thresh1.TabIndex = 106;
            this.toolTip1.SetToolTip(this.txBx_Wv_Thresh1, "Double click to preview");
            this.txBx_Wv_Thresh1.DoubleClick += new System.EventHandler(this.txBx_Wv_Thresh_DoubleClick);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(402, 60);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 15);
            this.label17.TabIndex = 107;
            this.label17.Text = "Thresh";
            this.toolTip1.SetToolTip(this.label17, "Only relevant for the specific setting you have on brightness, double click to pr" +
        "eview");
            // 
            // btn_SaveWavelengthInfo
            // 
            this.btn_SaveWavelengthInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_SaveWavelengthInfo.Location = new System.Drawing.Point(4, 198);
            this.btn_SaveWavelengthInfo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_SaveWavelengthInfo.Name = "btn_SaveWavelengthInfo";
            this.btn_SaveWavelengthInfo.Size = new System.Drawing.Size(70, 25);
            this.btn_SaveWavelengthInfo.TabIndex = 105;
            this.btn_SaveWavelengthInfo.Text = "Save";
            this.toolTip1.SetToolTip(this.btn_SaveWavelengthInfo, "Save the newly entered Wavelength Name and Abbreviations");
            this.btn_SaveWavelengthInfo.UseVisualStyleBackColor = true;
            this.btn_SaveWavelengthInfo.Click += new System.EventHandler(this.btn_SaveWavelengthInfo_Click);
            // 
            // txBx_WavelengthExtended
            // 
            this.txBx_WavelengthExtended.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txBx_WavelengthExtended.Location = new System.Drawing.Point(4, 60);
            this.txBx_WavelengthExtended.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_WavelengthExtended.Multiline = true;
            this.txBx_WavelengthExtended.Name = "txBx_WavelengthExtended";
            this.txBx_WavelengthExtended.Size = new System.Drawing.Size(69, 131);
            this.txBx_WavelengthExtended.TabIndex = 104;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(76, 60);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 15);
            this.label16.TabIndex = 103;
            this.label16.Text = "Abbrev";
            this.toolTip1.SetToolTip(this.label16, "This must be set to use Rename Norm");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(131, 60);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 15);
            this.label15.TabIndex = 102;
            this.label15.Text = "Name";
            // 
            // txBx_Wv_Abbrev5
            // 
            this.txBx_Wv_Abbrev5.Location = new System.Drawing.Point(78, 198);
            this.txBx_Wv_Abbrev5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Abbrev5.Name = "txBx_Wv_Abbrev5";
            this.txBx_Wv_Abbrev5.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wv_Abbrev5.TabIndex = 101;
            // 
            // txBx_Wv_Name5
            // 
            this.txBx_Wv_Name5.Location = new System.Drawing.Point(111, 198);
            this.txBx_Wv_Name5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Name5.Name = "txBx_Wv_Name5";
            this.txBx_Wv_Name5.Size = new System.Drawing.Size(92, 23);
            this.txBx_Wv_Name5.TabIndex = 100;
            // 
            // txBx_Wv_Abbrev4
            // 
            this.txBx_Wv_Abbrev4.Location = new System.Drawing.Point(78, 168);
            this.txBx_Wv_Abbrev4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Abbrev4.Name = "txBx_Wv_Abbrev4";
            this.txBx_Wv_Abbrev4.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wv_Abbrev4.TabIndex = 99;
            // 
            // txBx_Wv_Name4
            // 
            this.txBx_Wv_Name4.Location = new System.Drawing.Point(111, 168);
            this.txBx_Wv_Name4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Name4.Name = "txBx_Wv_Name4";
            this.txBx_Wv_Name4.Size = new System.Drawing.Size(92, 23);
            this.txBx_Wv_Name4.TabIndex = 98;
            // 
            // txBx_Wv_Abbrev3
            // 
            this.txBx_Wv_Abbrev3.Location = new System.Drawing.Point(78, 138);
            this.txBx_Wv_Abbrev3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Abbrev3.Name = "txBx_Wv_Abbrev3";
            this.txBx_Wv_Abbrev3.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wv_Abbrev3.TabIndex = 97;
            // 
            // txBx_Wv_Name3
            // 
            this.txBx_Wv_Name3.Location = new System.Drawing.Point(111, 138);
            this.txBx_Wv_Name3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Name3.Name = "txBx_Wv_Name3";
            this.txBx_Wv_Name3.Size = new System.Drawing.Size(92, 23);
            this.txBx_Wv_Name3.TabIndex = 96;
            // 
            // txBx_Wv_Abbrev2
            // 
            this.txBx_Wv_Abbrev2.Location = new System.Drawing.Point(78, 108);
            this.txBx_Wv_Abbrev2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Abbrev2.Name = "txBx_Wv_Abbrev2";
            this.txBx_Wv_Abbrev2.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wv_Abbrev2.TabIndex = 95;
            // 
            // txBx_Wv_Name2
            // 
            this.txBx_Wv_Name2.Location = new System.Drawing.Point(111, 108);
            this.txBx_Wv_Name2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Name2.Name = "txBx_Wv_Name2";
            this.txBx_Wv_Name2.Size = new System.Drawing.Size(92, 23);
            this.txBx_Wv_Name2.TabIndex = 94;
            // 
            // txBx_Wv_Abbrev1
            // 
            this.txBx_Wv_Abbrev1.Location = new System.Drawing.Point(78, 78);
            this.txBx_Wv_Abbrev1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Abbrev1.Name = "txBx_Wv_Abbrev1";
            this.txBx_Wv_Abbrev1.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wv_Abbrev1.TabIndex = 93;
            // 
            // txBx_Wv_Name1
            // 
            this.txBx_Wv_Name1.Location = new System.Drawing.Point(111, 78);
            this.txBx_Wv_Name1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wv_Name1.Name = "txBx_Wv_Name1";
            this.txBx_Wv_Name1.Size = new System.Drawing.Size(92, 23);
            this.txBx_Wv_Name1.TabIndex = 92;
            this.toolTip1.SetToolTip(this.txBx_Wv_Name1, resources.GetString("txBx_Wv_Name1.ToolTip"));
            // 
            // chkBx_ObjDetect5
            // 
            this.chkBx_ObjDetect5.AutoSize = true;
            this.chkBx_ObjDetect5.Location = new System.Drawing.Point(368, 201);
            this.chkBx_ObjDetect5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_ObjDetect5.Name = "chkBx_ObjDetect5";
            this.chkBx_ObjDetect5.Size = new System.Drawing.Size(15, 14);
            this.chkBx_ObjDetect5.TabIndex = 91;
            this.chkBx_ObjDetect5.UseVisualStyleBackColor = true;
            // 
            // chkBx_Onwv5
            // 
            this.chkBx_Onwv5.AutoSize = true;
            this.chkBx_Onwv5.Checked = true;
            this.chkBx_Onwv5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBx_Onwv5.Location = new System.Drawing.Point(346, 201);
            this.chkBx_Onwv5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_Onwv5.Name = "chkBx_Onwv5";
            this.chkBx_Onwv5.Size = new System.Drawing.Size(15, 14);
            this.chkBx_Onwv5.TabIndex = 90;
            this.chkBx_Onwv5.UseVisualStyleBackColor = true;
            this.chkBx_Onwv5.CheckedChanged += new System.EventHandler(this.chkBx_Onwv5_CheckedChanged);
            // 
            // txBx_Color5
            // 
            this.txBx_Color5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txBx_Color5.Location = new System.Drawing.Point(292, 200);
            this.txBx_Color5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Color5.Name = "txBx_Color5";
            this.txBx_Color5.Size = new System.Drawing.Size(50, 17);
            this.txBx_Color5.TabIndex = 89;
            this.txBx_Color5.Text = "FFFFFF";
            this.toolTip1.SetToolTip(this.txBx_Color5, "Use this to define how this channel\'s color looks. ");
            this.txBx_Color5.TextChanged += new System.EventHandler(this.txBx_Color_TextChanged);
            // 
            // txBx_BrightnessMultiplier5
            // 
            this.txBx_BrightnessMultiplier5.Location = new System.Drawing.Point(240, 198);
            this.txBx_BrightnessMultiplier5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_BrightnessMultiplier5.Name = "txBx_BrightnessMultiplier5";
            this.txBx_BrightnessMultiplier5.Size = new System.Drawing.Size(46, 23);
            this.txBx_BrightnessMultiplier5.TabIndex = 87;
            // 
            // txBx_Wavelength5
            // 
            this.txBx_Wavelength5.Location = new System.Drawing.Point(206, 198);
            this.txBx_Wavelength5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wavelength5.Name = "txBx_Wavelength5";
            this.txBx_Wavelength5.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wavelength5.TabIndex = 88;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(363, 60);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 15);
            this.label14.TabIndex = 86;
            this.label14.Text = "Obj";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(340, 60);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 15);
            this.label10.TabIndex = 85;
            this.label10.Text = "Vis";
            // 
            // chkBx_ObjDetect4
            // 
            this.chkBx_ObjDetect4.AutoSize = true;
            this.chkBx_ObjDetect4.Location = new System.Drawing.Point(368, 171);
            this.chkBx_ObjDetect4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_ObjDetect4.Name = "chkBx_ObjDetect4";
            this.chkBx_ObjDetect4.Size = new System.Drawing.Size(15, 14);
            this.chkBx_ObjDetect4.TabIndex = 84;
            this.chkBx_ObjDetect4.UseVisualStyleBackColor = true;
            // 
            // chkBx_ObjDetect3
            // 
            this.chkBx_ObjDetect3.AutoSize = true;
            this.chkBx_ObjDetect3.Location = new System.Drawing.Point(368, 141);
            this.chkBx_ObjDetect3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_ObjDetect3.Name = "chkBx_ObjDetect3";
            this.chkBx_ObjDetect3.Size = new System.Drawing.Size(15, 14);
            this.chkBx_ObjDetect3.TabIndex = 83;
            this.chkBx_ObjDetect3.UseVisualStyleBackColor = true;
            // 
            // chkBx_ObjDetect2
            // 
            this.chkBx_ObjDetect2.AutoSize = true;
            this.chkBx_ObjDetect2.Location = new System.Drawing.Point(368, 111);
            this.chkBx_ObjDetect2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_ObjDetect2.Name = "chkBx_ObjDetect2";
            this.chkBx_ObjDetect2.Size = new System.Drawing.Size(15, 14);
            this.chkBx_ObjDetect2.TabIndex = 82;
            this.chkBx_ObjDetect2.UseVisualStyleBackColor = true;
            // 
            // chkBx_ObjDetect1
            // 
            this.chkBx_ObjDetect1.AutoSize = true;
            this.chkBx_ObjDetect1.Location = new System.Drawing.Point(368, 82);
            this.chkBx_ObjDetect1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_ObjDetect1.Name = "chkBx_ObjDetect1";
            this.chkBx_ObjDetect1.Size = new System.Drawing.Size(15, 14);
            this.chkBx_ObjDetect1.TabIndex = 81;
            this.chkBx_ObjDetect1.UseVisualStyleBackColor = true;
            // 
            // lbl_xyi
            // 
            this.lbl_xyi.AutoSize = true;
            this.lbl_xyi.Location = new System.Drawing.Point(250, 337);
            this.lbl_xyi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_xyi.Name = "lbl_xyi";
            this.lbl_xyi.Size = new System.Drawing.Size(28, 15);
            this.lbl_xyi.TabIndex = 80;
            this.lbl_xyi.Text = "x,y,i";
            // 
            // lbl_SegTest
            // 
            this.lbl_SegTest.AutoSize = true;
            this.lbl_SegTest.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbl_SegTest.Location = new System.Drawing.Point(366, 702);
            this.lbl_SegTest.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_SegTest.Name = "lbl_SegTest";
            this.lbl_SegTest.Size = new System.Drawing.Size(50, 15);
            this.lbl_SegTest.TabIndex = 79;
            this.lbl_SegTest.Text = "Dev Test";
            // 
            // pictureBox_Zoom
            // 
            this.pictureBox_Zoom.Location = new System.Drawing.Point(4, 308);
            this.pictureBox_Zoom.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox_Zoom.Name = "pictureBox_Zoom";
            this.pictureBox_Zoom.Size = new System.Drawing.Size(239, 207);
            this.pictureBox_Zoom.TabIndex = 78;
            this.pictureBox_Zoom.TabStop = false;
            this.pictureBox_Zoom.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Zoom_Paint);
            // 
            // btn_ExportCurrentImageView
            // 
            this.btn_ExportCurrentImageView.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_ExportCurrentImageView.Location = new System.Drawing.Point(285, 5);
            this.btn_ExportCurrentImageView.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_ExportCurrentImageView.Name = "btn_ExportCurrentImageView";
            this.btn_ExportCurrentImageView.Size = new System.Drawing.Size(89, 25);
            this.btn_ExportCurrentImageView.TabIndex = 77;
            this.btn_ExportCurrentImageView.Text = "Save IMGs*";
            this.toolTip1.SetToolTip(this.btn_ExportCurrentImageView, "Export (stitched) the set of images. It will save in the default location and sho" +
        "w that location below after saving. ");
            this.btn_ExportCurrentImageView.UseVisualStyleBackColor = true;
            this.btn_ExportCurrentImageView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_ExportCurrentImageView_MouseUp);
            // 
            // txBx_FOV_LR
            // 
            this.txBx_FOV_LR.Enabled = false;
            this.txBx_FOV_LR.Location = new System.Drawing.Point(48, 740);
            this.txBx_FOV_LR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_FOV_LR.Name = "txBx_FOV_LR";
            this.txBx_FOV_LR.Size = new System.Drawing.Size(33, 23);
            this.txBx_FOV_LR.TabIndex = 76;
            // 
            // txBx_FOV_UR
            // 
            this.txBx_FOV_UR.Enabled = false;
            this.txBx_FOV_UR.Location = new System.Drawing.Point(48, 710);
            this.txBx_FOV_UR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_FOV_UR.Name = "txBx_FOV_UR";
            this.txBx_FOV_UR.Size = new System.Drawing.Size(33, 23);
            this.txBx_FOV_UR.TabIndex = 75;
            // 
            // txBx_FOV_LL
            // 
            this.txBx_FOV_LL.Enabled = false;
            this.txBx_FOV_LL.Location = new System.Drawing.Point(9, 740);
            this.txBx_FOV_LL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_FOV_LL.Name = "txBx_FOV_LL";
            this.txBx_FOV_LL.Size = new System.Drawing.Size(33, 23);
            this.txBx_FOV_LL.TabIndex = 74;
            // 
            // Leica_Label
            // 
            this.Leica_Label.AutoSize = true;
            this.Leica_Label.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Leica_Label.Location = new System.Drawing.Point(258, 702);
            this.Leica_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Leica_Label.Name = "Leica_Label";
            this.Leica_Label.Size = new System.Drawing.Size(55, 15);
            this.Leica_Label.TabIndex = 73;
            this.Leica_Label.Text = "Dev Only";
            this.Leica_Label.Click += new System.EventHandler(this.Leica_Label_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(298, 60);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 15);
            this.label8.TabIndex = 72;
            this.label8.Text = "Color";
            this.toolTip1.SetToolTip(this.label8, "Use this to define how this channel\'s color looks. \r\nThis is a Hex RGB value. \r\nW" +
        "hite = FFFFFF\r\nRed = FF0000\r\nGreen = 00FF00\r\nBlue = 0000FF\r\nYellow = FFFF00\r\nCya" +
        "n =  00FFFF\r\nMagenta = FF00FF");
            this.label8.Click += new System.EventHandler(this.label_Color_Click);
            // 
            // chkBx_Onwv4
            // 
            this.chkBx_Onwv4.AutoSize = true;
            this.chkBx_Onwv4.Checked = true;
            this.chkBx_Onwv4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBx_Onwv4.Location = new System.Drawing.Point(346, 171);
            this.chkBx_Onwv4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_Onwv4.Name = "chkBx_Onwv4";
            this.chkBx_Onwv4.Size = new System.Drawing.Size(15, 14);
            this.chkBx_Onwv4.TabIndex = 71;
            this.chkBx_Onwv4.UseVisualStyleBackColor = true;
            this.chkBx_Onwv4.CheckedChanged += new System.EventHandler(this.chkBx_Onwv4_CheckedChanged);
            // 
            // chkBx_Onwv3
            // 
            this.chkBx_Onwv3.AutoSize = true;
            this.chkBx_Onwv3.Checked = true;
            this.chkBx_Onwv3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBx_Onwv3.Location = new System.Drawing.Point(346, 141);
            this.chkBx_Onwv3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_Onwv3.Name = "chkBx_Onwv3";
            this.chkBx_Onwv3.Size = new System.Drawing.Size(15, 14);
            this.chkBx_Onwv3.TabIndex = 70;
            this.chkBx_Onwv3.UseVisualStyleBackColor = true;
            this.chkBx_Onwv3.CheckedChanged += new System.EventHandler(this.chkBx_Onwv3_CheckedChanged);
            // 
            // chkBx_Onwv2
            // 
            this.chkBx_Onwv2.AutoSize = true;
            this.chkBx_Onwv2.Checked = true;
            this.chkBx_Onwv2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBx_Onwv2.Location = new System.Drawing.Point(346, 111);
            this.chkBx_Onwv2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_Onwv2.Name = "chkBx_Onwv2";
            this.chkBx_Onwv2.Size = new System.Drawing.Size(15, 14);
            this.chkBx_Onwv2.TabIndex = 69;
            this.chkBx_Onwv2.UseVisualStyleBackColor = true;
            this.chkBx_Onwv2.CheckedChanged += new System.EventHandler(this.chkBx_Onwv2_CheckedChanged);
            // 
            // txBx_Color4
            // 
            this.txBx_Color4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txBx_Color4.Location = new System.Drawing.Point(292, 170);
            this.txBx_Color4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Color4.Name = "txBx_Color4";
            this.txBx_Color4.Size = new System.Drawing.Size(50, 17);
            this.txBx_Color4.TabIndex = 68;
            this.txBx_Color4.Text = "FFFFFF";
            this.toolTip1.SetToolTip(this.txBx_Color4, "Use this to define how this channel\'s color looks. ");
            this.txBx_Color4.TextChanged += new System.EventHandler(this.txBx_Color_TextChanged);
            // 
            // txBx_Color3
            // 
            this.txBx_Color3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txBx_Color3.Location = new System.Drawing.Point(292, 140);
            this.txBx_Color3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Color3.Name = "txBx_Color3";
            this.txBx_Color3.Size = new System.Drawing.Size(50, 17);
            this.txBx_Color3.TabIndex = 67;
            this.txBx_Color3.Text = "FFFFFF";
            this.toolTip1.SetToolTip(this.txBx_Color3, "Use this to define how this channel\'s color looks. ");
            this.txBx_Color3.TextChanged += new System.EventHandler(this.txBx_Color_TextChanged);
            // 
            // txBx_Color2
            // 
            this.txBx_Color2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txBx_Color2.Location = new System.Drawing.Point(292, 110);
            this.txBx_Color2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Color2.Name = "txBx_Color2";
            this.txBx_Color2.Size = new System.Drawing.Size(50, 17);
            this.txBx_Color2.TabIndex = 66;
            this.txBx_Color2.Text = "FFFFFF";
            this.toolTip1.SetToolTip(this.txBx_Color2, "Use this to define how this channel\'s color looks. ");
            this.txBx_Color2.TextChanged += new System.EventHandler(this.txBx_Color_TextChanged);
            // 
            // txBx_Color1
            // 
            this.txBx_Color1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txBx_Color1.Location = new System.Drawing.Point(292, 80);
            this.txBx_Color1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Color1.Name = "txBx_Color1";
            this.txBx_Color1.Size = new System.Drawing.Size(50, 17);
            this.txBx_Color1.TabIndex = 65;
            this.txBx_Color1.Text = "FFFFFF";
            this.toolTip1.SetToolTip(this.txBx_Color1, "Use this to define how this channel\'s color looks. ");
            this.txBx_Color1.TextChanged += new System.EventHandler(this.txBx_Color_TextChanged);
            // 
            // chkBx_Onwv1
            // 
            this.chkBx_Onwv1.AutoSize = true;
            this.chkBx_Onwv1.Checked = true;
            this.chkBx_Onwv1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBx_Onwv1.Location = new System.Drawing.Point(346, 82);
            this.chkBx_Onwv1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_Onwv1.Name = "chkBx_Onwv1";
            this.chkBx_Onwv1.Size = new System.Drawing.Size(15, 14);
            this.chkBx_Onwv1.TabIndex = 64;
            this.chkBx_Onwv1.UseVisualStyleBackColor = true;
            this.chkBx_Onwv1.CheckedChanged += new System.EventHandler(this.chkBx_Onwv1_CheckedChanged);
            // 
            // txBx_BrightnessMultiplier4
            // 
            this.txBx_BrightnessMultiplier4.Location = new System.Drawing.Point(240, 168);
            this.txBx_BrightnessMultiplier4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_BrightnessMultiplier4.Name = "txBx_BrightnessMultiplier4";
            this.txBx_BrightnessMultiplier4.Size = new System.Drawing.Size(46, 23);
            this.txBx_BrightnessMultiplier4.TabIndex = 62;
            // 
            // txBx_Wavelength4
            // 
            this.txBx_Wavelength4.Location = new System.Drawing.Point(206, 168);
            this.txBx_Wavelength4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wavelength4.Name = "txBx_Wavelength4";
            this.txBx_Wavelength4.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wavelength4.TabIndex = 63;
            // 
            // txBx_BrightnessMultiplier3
            // 
            this.txBx_BrightnessMultiplier3.Location = new System.Drawing.Point(240, 138);
            this.txBx_BrightnessMultiplier3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_BrightnessMultiplier3.Name = "txBx_BrightnessMultiplier3";
            this.txBx_BrightnessMultiplier3.Size = new System.Drawing.Size(46, 23);
            this.txBx_BrightnessMultiplier3.TabIndex = 60;
            // 
            // txBx_Wavelength3
            // 
            this.txBx_Wavelength3.Location = new System.Drawing.Point(206, 138);
            this.txBx_Wavelength3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wavelength3.Name = "txBx_Wavelength3";
            this.txBx_Wavelength3.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wavelength3.TabIndex = 61;
            // 
            // txBx_BrightnessMultiplier2
            // 
            this.txBx_BrightnessMultiplier2.Location = new System.Drawing.Point(240, 108);
            this.txBx_BrightnessMultiplier2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_BrightnessMultiplier2.Name = "txBx_BrightnessMultiplier2";
            this.txBx_BrightnessMultiplier2.Size = new System.Drawing.Size(46, 23);
            this.txBx_BrightnessMultiplier2.TabIndex = 58;
            // 
            // txBx_Wavelength2
            // 
            this.txBx_Wavelength2.Location = new System.Drawing.Point(206, 108);
            this.txBx_Wavelength2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Wavelength2.Name = "txBx_Wavelength2";
            this.txBx_Wavelength2.Size = new System.Drawing.Size(28, 23);
            this.txBx_Wavelength2.TabIndex = 59;
            // 
            // btn_ExportFromList
            // 
            this.btn_ExportFromList.Location = new System.Drawing.Point(379, 5);
            this.btn_ExportFromList.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_ExportFromList.Name = "btn_ExportFromList";
            this.btn_ExportFromList.Size = new System.Drawing.Size(70, 25);
            this.btn_ExportFromList.TabIndex = 57;
            this.btn_ExportFromList.Text = "Exports..";
            this.toolTip1.SetToolTip(this.btn_ExportFromList, resources.GetString("btn_ExportFromList.ToolTip"));
            this.btn_ExportFromList.UseVisualStyleBackColor = true;
            this.btn_ExportFromList.Click += new System.EventHandler(this.btn_ExportFromList_Click);
            // 
            // btnUndoImageCheck
            // 
            this.btnUndoImageCheck.Location = new System.Drawing.Point(257, 642);
            this.btnUndoImageCheck.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnUndoImageCheck.Name = "btnUndoImageCheck";
            this.btnUndoImageCheck.Size = new System.Drawing.Size(50, 25);
            this.btnUndoImageCheck.TabIndex = 56;
            this.btnUndoImageCheck.Text = "undo";
            this.btnUndoImageCheck.UseVisualStyleBackColor = true;
            this.btnUndoImageCheck.Click += new System.EventHandler(this.btnUndoImageCheck_Click);
            // 
            // comboBox_Overlay
            // 
            this.comboBox_Overlay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Overlay.FormattingEnabled = true;
            this.comboBox_Overlay.Location = new System.Drawing.Point(68, 233);
            this.comboBox_Overlay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBox_Overlay.Name = "comboBox_Overlay";
            this.comboBox_Overlay.Size = new System.Drawing.Size(166, 23);
            this.comboBox_Overlay.TabIndex = 54;
            this.comboBox_Overlay.SelectedIndexChanged += new System.EventHandler(this.comboBox_Overlay_SelectedIndexChanged);
            // 
            // chkBx_ShowOverlay
            // 
            this.chkBx_ShowOverlay.AutoSize = true;
            this.chkBx_ShowOverlay.Enabled = false;
            this.chkBx_ShowOverlay.Location = new System.Drawing.Point(4, 236);
            this.chkBx_ShowOverlay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkBx_ShowOverlay.Name = "chkBx_ShowOverlay";
            this.chkBx_ShowOverlay.Size = new System.Drawing.Size(66, 19);
            this.chkBx_ShowOverlay.TabIndex = 53;
            this.chkBx_ShowOverlay.Text = "Overlay";
            this.chkBx_ShowOverlay.UseVisualStyleBackColor = true;
            this.chkBx_ShowOverlay.CheckedChanged += new System.EventHandler(this.chkBx_ShowOverlay_CheckedChanged);
            // 
            // btnSize300
            // 
            this.btnSize300.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSize300.Location = new System.Drawing.Point(85, 5);
            this.btnSize300.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSize300.Name = "btnSize300";
            this.btnSize300.Size = new System.Drawing.Size(50, 25);
            this.btnSize300.TabIndex = 51;
            this.btnSize300.Text = "300";
            this.toolTip1.SetToolTip(this.btnSize300, "Single Image Size 300x300");
            this.btnSize300.UseVisualStyleBackColor = true;
            this.btnSize300.Click += new System.EventHandler(this.btn_FullSize_Click);
            // 
            // btnSize700
            // 
            this.btnSize700.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSize700.Location = new System.Drawing.Point(190, 5);
            this.btnSize700.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSize700.Name = "btnSize700";
            this.btnSize700.Size = new System.Drawing.Size(50, 25);
            this.btnSize700.TabIndex = 50;
            this.btnSize700.Text = "700";
            this.toolTip1.SetToolTip(this.btnSize700, "Single Image Size 700x700");
            this.btnSize700.UseVisualStyleBackColor = true;
            this.btnSize700.Click += new System.EventHandler(this.btn_FullSize_Click);
            // 
            // btn_Finish
            // 
            this.btn_Finish.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Finish.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_Finish.Location = new System.Drawing.Point(566, 609);
            this.btn_Finish.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Finish.Name = "btn_Finish";
            this.btn_Finish.Size = new System.Drawing.Size(124, 75);
            this.btn_Finish.TabIndex = 77;
            this.btn_Finish.Text = "Finish";
            this.btn_Finish.UseVisualStyleBackColor = false;
            this.btn_Finish.Visible = false;
            this.btn_Finish.Click += new System.EventHandler(this.btn_Finish_Click);
            // 
            // bgWorker_ImageLoad
            // 
            this.bgWorker_ImageLoad.WorkerSupportsCancellation = true;
            this.bgWorker_ImageLoad.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_ImageLoad_DoWork);
            this.bgWorker_ImageLoad.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_ImageLoad_RunWorkerCompleted);
            // 
            // bgWorker_Analysis1
            // 
            this.bgWorker_Analysis1.WorkerReportsProgress = true;
            this.bgWorker_Analysis1.WorkerSupportsCancellation = true;
            this.bgWorker_Analysis1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_Analysis1_DoWork);
            this.bgWorker_Analysis1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorker_Analysis1_ProgressChanged);
            this.bgWorker_Analysis1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_Analysis1_RunWorkerCompleted);
            // 
            // FormRaftCal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 767);
            this.Controls.Add(this.btn_Finish);
            this.Controls.Add(this.panelControls);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "FormRaftCal";
            this.Text = "InCell Image Check (FIV Tools)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRaftCal_FormClosing);
            this.Load += new System.EventHandler(this.FormRaftCal_Load);
            this.Shown += new System.EventHandler(this.FormRaftCal_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormRaftCal_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Zoom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txBx_FolderPath;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.TextBox txBx_FOV;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txBx_Row;
        private System.Windows.Forms.TextBox txBx_Col;
        private System.Windows.Forms.Button btn_Up;
        private System.Windows.Forms.Button btn_Down;
        private System.Windows.Forms.Button btn_Left;
        private System.Windows.Forms.Button btn_Right;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txBx_Wavelength1;
        private System.Windows.Forms.Button btn_U5;
        private System.Windows.Forms.Button btn_D5;
        private System.Windows.Forms.Button btn_L5;
        private System.Windows.Forms.Button btn_R5;
        private System.Windows.Forms.Label labl_ImageName;
        private System.Windows.Forms.TextBox txBx_SavedList;
        private System.Windows.Forms.TextBox txBx_RaftID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txBx_LocationInfo;
        private System.Windows.Forms.Label label_Notify;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_BR;
        private System.Windows.Forms.Button btn_UL;
        private System.Windows.Forms.Button btn_BL;
        private System.Windows.Forms.Button btn_UR;
        private System.Windows.Forms.Button btn_SaveCalibration;
        private System.Windows.Forms.Label label_Warning;
        private System.Windows.Forms.RadioButton radioButton_TestCal;
        private System.Windows.Forms.RadioButton radioButton_RecordFocus;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_SaveCheck;
        private System.Windows.Forms.ComboBox comboBox_Well;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblImageCheck;
        private System.Windows.Forms.Button btnResetImageCheck;
        private System.Windows.Forms.Button btnResetSaveCal;
        private System.Windows.Forms.Button btnFieldBack;
        private System.Windows.Forms.Button btnFieldNext;
        private System.Windows.Forms.Button btn_RaftNext;
        private System.Windows.Forms.Button btn_RaftPrev;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_Size512;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.Button btnSize700;
        private System.Windows.Forms.Button btnSize300;
        private System.Windows.Forms.CheckBox chkBx_ShowOverlay;
        private System.Windows.Forms.ComboBox comboBox_Overlay;
        private System.Windows.Forms.Button btnUndoImageCheck;
        private System.Windows.Forms.Button btn_ExportFromList;
        private System.Windows.Forms.Button btnFieldSnake;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkBx_Onwv4;
        private System.Windows.Forms.CheckBox chkBx_Onwv3;
        private System.Windows.Forms.CheckBox chkBx_Onwv2;
        private System.Windows.Forms.TextBox txBx_Color4;
        private System.Windows.Forms.TextBox txBx_Color3;
        private System.Windows.Forms.TextBox txBx_Color2;
        private System.Windows.Forms.TextBox txBx_Color1;
        private System.Windows.Forms.CheckBox chkBx_Onwv1;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier4;
        private System.Windows.Forms.TextBox txBx_Wavelength4;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier3;
        private System.Windows.Forms.TextBox txBx_Wavelength3;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier2;
        private System.Windows.Forms.TextBox txBx_Wavelength2;
        private System.Windows.Forms.Label Leica_Label;
        private System.Windows.Forms.TextBox txBx_FOV_LR;
        private System.Windows.Forms.TextBox txBx_FOV_UR;
        private System.Windows.Forms.TextBox txBx_FOV_LL;
        private System.Windows.Forms.Button btn_Finish;
        private System.Windows.Forms.Button btn_ExportCurrentImageView;
        private System.Windows.Forms.PictureBox pictureBox_Zoom;
        private System.Windows.Forms.Label lbl_SegTest;
        private System.Windows.Forms.Label lbl_xyi;
        private System.ComponentModel.BackgroundWorker bgWorker_ImageLoad;
        private System.ComponentModel.BackgroundWorker bgWorker_Analysis1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect4;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect3;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect2;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect1;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect5;
        private System.Windows.Forms.CheckBox chkBx_Onwv5;
        private System.Windows.Forms.TextBox txBx_Color5;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier5;
        private System.Windows.Forms.TextBox txBx_Wavelength5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev5;
        private System.Windows.Forms.TextBox txBx_Wv_Name5;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev4;
        private System.Windows.Forms.TextBox txBx_Wv_Name4;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev3;
        private System.Windows.Forms.TextBox txBx_Wv_Name3;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev2;
        private System.Windows.Forms.TextBox txBx_Wv_Name2;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev1;
        private System.Windows.Forms.TextBox txBx_Wv_Name1;
        private System.Windows.Forms.Button btn_SaveWavelengthInfo;
        private System.Windows.Forms.TextBox txBx_WavelengthExtended;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh5;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh4;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh3;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh2;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnRunSegmentation;
        public System.Windows.Forms.CheckBox chkBx_SquareMode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkBx_DI_5;
        private System.Windows.Forms.CheckBox chkBx_DI_4;
        private System.Windows.Forms.CheckBox chkBx_DI_3;
        private System.Windows.Forms.CheckBox chkBx_DI_2;
        private System.Windows.Forms.CheckBox chkBx_DI_1;
        public System.Windows.Forms.TextBox txBx_ImageName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnJumpNextCalPoint;
        public System.Windows.Forms.CheckBox chkBx_ShowCalGrid;
        private System.Windows.Forms.ComboBox comboBox_ColorMerge;
        private System.Windows.Forms.TextBox txBx_Color_Overlay;
        public System.Windows.Forms.CheckBox chk_Bx_ObjClipsVis;
        private System.Windows.Forms.TextBox txBx_ClipExpand_Neg;
        private System.Windows.Forms.TextBox txBx_ClipExpand_Pos;
    }
}

