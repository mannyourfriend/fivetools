﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace FIVE_IMG.Transforms
{

    /// <summary>
    /// Object containing the necessary fields for calculating a raft ID on a particular well / plate.
    /// </summary>
    /// 
    /// <remarks>
    /// ID - Transform ID to distinguish between different plates and/or wells.
    /// PictureDimensions - Length and Width of images taken by the microscope in pixels and microns.
    /// Theta - Angle used for scaling y coordinates.
    /// Phi - Angle used for scaling x coordinates.
    /// XDist - X distance in microns between each raft.
    /// YDist - Y distance in microns between each raft.
    /// XShift - X distance away from the origin.
    /// YShift - Y distance away from the origin.
    /// 
    /// Originally by Vinay Chandrasekaran 2019
    /// </remarks>
    public class Transform
    {
        public CalibrationPoints CalRafts { get; set; }

        /// <summary>
        /// Takes calibration input and returns a Transform for a plate or a well.
        /// </summary>
        /// <param name="c">
        /// Calibration Rafts - 3 KnownRaft objects, an upper left, lower left, and upper right raft.
        /// The upper left and upper right rafts should be colinear on a horizontal line and the upper
        /// left and lowerleft raft should be colinear on a vertical line.
        /// </param>
        public Transform(CalibrationPoints c)
        {
            CalRafts = c;
            SetOrientation(c);

            //Step 3: Calculate Theta and Phi
            this.Theta = Math.Atan((c.UpperRight.Y - c.UpperLeft.Y) / (c.UpperRight.X - c.UpperLeft.X));
            this.Phi = Math.Atan((c.UpperLeft.X - c.LowerLeft.X) / (c.LowerLeft.Y - c.UpperLeft.Y));

            //Step 4: Use Theta, Phi, X, and Y to find XPrime and YPrime (transformed coordinates)
            foreach (KnownPoint raft in c.KnownPoints) raft.Setup_XYPrime(Phi, Theta);

            //Step 5: Find xDist, yDist, xShift, and yShift
            this.XDist = Math.Abs((c.UpperLeft.XPrime - c.UpperRight.XPrime) / (c.UpperLeft.ColGE - c.UpperRight.ColGE));
            this.YDist = Math.Abs((c.LowerLeft.YPrime - c.UpperLeft.YPrime) / (c.UpperLeft.RowGE - c.LowerLeft.RowGE));

            this.XShift = c.UpperLeft.XPrime - this.XDist * (c.UpperLeft.ColGE - 1);
            this.YShift = c.UpperLeft.YPrime - this.YDist * (c.UpperLeft.RowGE - 1);
        }

        public double Theta { get; set; }
        public double Phi { get; set; }
        public double XDist { get; set; }
        public double YDist { get; set; }
        public double XShift { get; set; }
        public double YShift { get; set; }

        public string GetTransform_Metrics()
        {
            StringBuilder sB = new StringBuilder();
            sB.AppendLine("Theta: " + this.Theta);
            sB.AppendLine("Phi: " + this.Phi);
            sB.AppendLine("XDist: " + this.XDist);
            sB.AppendLine("YDist: " + this.YDist);
            sB.AppendLine("XShift: " + this.XShift);
            sB.AppendLine("YShift: " + this.YShift);
            return sB.ToString();
        }

        public string FindRaftID(double X, double Y)
        {
            UnknownPoint u = new UnknownPoint(X, Y);
            return Find_ID(u);
        }

        public string Find_ID(UnknownPoint u)
        {
            ReturnPoint rr = Find_RR(u);
            return rr.RaftID;
        }

        public ReturnPoint FindRaftID_RR(double X, double Y)
        {
            UnknownPoint u = new UnknownPoint(X, Y);
            return Find_RR(u);
        }

        // finds orientation of rafts based on calibration rafts
        public static void RaftOriginIsBottomLeft(CalibrationPoints c, out bool isBottom, out bool isLeft)
        {
            int raftOneCol = ((int)c.LowerLeft.ID[2] - 65) * 10 + ((int)c.LowerLeft.ID[3] - 48) + 1;
            int raftOneRow = ((int)c.LowerLeft.ID[0] - 65) * 10 + (int)c.LowerLeft.ID[1] - 48 + 1;
            int raftThreeCol = ((int)c.UpperRight.ID[2] - 65) * 10 + ((int)c.UpperRight.ID[3] - 48) + 1;
            int raftThreeRow = ((int)c.UpperRight.ID[0] - 65) * 10 + (int)c.UpperRight.ID[1] - 48 + 1;

            isBottom = true;
            isLeft = true;

            //normal orientation
            if (raftThreeRow > raftOneRow && raftThreeCol > raftOneCol)
            {
                isLeft = true;
                isBottom = true;
            }
            //clockwise 90 degree rotation
            if (raftThreeRow < raftOneRow && raftThreeCol > raftOneCol)
            {
                isLeft = true;
                isBottom = false;
            }
            //180 degree rotation
            if (raftThreeRow < raftOneRow && raftThreeCol < raftOneCol)
            {
                isLeft = false;
                isBottom = false;
            }
            //clockwise 270 degree rotation
            if (raftThreeRow > raftOneRow && raftThreeCol < raftOneCol)
            {
                isLeft = false;
                isBottom = true;
            }
        }

        // sets rowGE and colGE of rafts according to their orientation
        public static void SetOrientation(CalibrationPoints c)
        {
            int raftOneCol = ((int)c.LowerLeft.ID[2] - 65) * 10 + ((int)c.LowerLeft.ID[3] - 48) + 1;
            int raftOneRow = ((int)c.LowerLeft.ID[0] - 65) * 10 + (int)c.LowerLeft.ID[1] - 48 + 1;
            int raftTwoCol = ((int)c.UpperLeft.ID[2] - 65) * 10 + ((int)c.UpperLeft.ID[3] - 48) + 1;
            int raftTwoRow = ((int)c.UpperLeft.ID[0] - 65) * 10 + (int)c.UpperLeft.ID[1] - 48 + 1;
            int raftThreeCol = ((int)c.UpperRight.ID[2] - 65) * 10 + ((int)c.UpperRight.ID[3] - 48) + 1;
            int raftThreeRow = ((int)c.UpperRight.ID[0] - 65) * 10 + (int)c.UpperRight.ID[1] - 48 + 1;

            //Step 2: Calculate rowGE and colGE with CalibrationRaft, making sure they are oriented correctly
            //normal orientation
            if (raftThreeRow > raftOneRow && raftThreeCol > raftOneCol)
            {
                c.LowerLeft.RowGE = 101 - raftOneCol;
                c.LowerLeft.ColGE = raftOneRow;

                c.UpperLeft.RowGE = 101 - raftTwoCol; c.UpperLeft.ColGE = raftTwoRow; c.UpperRight.RowGE = 101 - raftThreeCol; c.UpperRight.ColGE = raftThreeRow;
            }
            //clockwise 90 degree rotation
            if (raftThreeRow < raftOneRow && raftThreeCol > raftOneCol)
            {
                c.LowerLeft.RowGE = raftOneRow;
                c.LowerLeft.ColGE = raftOneCol;

                c.UpperLeft.RowGE = raftTwoRow;
                c.UpperLeft.ColGE = raftTwoCol;

                c.UpperRight.RowGE = raftThreeRow;
                c.UpperRight.ColGE = raftThreeCol;
            }
            //180 degree rotation
            if (raftThreeRow < raftOneRow && raftThreeCol < raftOneCol)
            {
                c.LowerLeft.RowGE = raftOneCol;
                c.LowerLeft.ColGE = 101 - raftOneRow;

                c.UpperLeft.RowGE = raftTwoCol;
                c.UpperLeft.ColGE = 101 - raftTwoRow;

                c.UpperRight.RowGE = raftThreeCol;
                c.UpperRight.ColGE = 101 - raftThreeRow;
            }
            //clockwise 270 degree rotation
            if (raftThreeRow > raftOneRow && raftThreeCol < raftOneCol)
            {
                c.LowerLeft.RowGE = 101 - raftOneRow;
                c.LowerLeft.ColGE = 101 - raftOneCol;

                c.UpperLeft.RowGE = 101 - raftTwoRow;
                c.UpperLeft.ColGE = 101 - raftTwoCol;

                c.UpperRight.RowGE = 101 - raftThreeRow;
                c.UpperRight.ColGE = 101 - raftThreeCol;
            }
        }

        /// <summary>
        /// Given a Transform and an UnkownRaft this method returns a 4 character string representing
        /// the raft row and column locations on a plate or well.
        /// </summary>
        /// 
        /// <param name="t">
        /// Transform - A Transform object with the necessary data to figure out a raft's ID
        /// given an FOV, x, and y coordinates.
        /// </param>
        /// 
        /// <param name="u">
        /// UnkownRaft - An object containing fields for calculating its raft ID.
        /// </param>
        /// 
        /// <returns> 
        /// Full ReturnRaft class
        /// </returns>
        public ReturnPoint Find_RR(UnknownPoint u)
        {
            Transform t = this;

            //Step 1: Instantiate Return String
            string rID = ""; ReturnPoint rr = new ReturnPoint(u);

            //Step 2: Find X' and Y' (transformed coordinates)
            u.XPrime = Math.Sqrt(u.X * u.X + u.Y * u.Y) * Math.Cos(Math.Atan(u.Y / u.X) - t.Phi);
            u.YPrime = Math.Sqrt(u.X * u.X + u.Y * u.Y) * Math.Cos(Math.Atan(u.X / u.Y) + t.Theta);

            //Step 3: Find XX and YY (shifted coordinates)
            u.XX = u.XPrime - t.XShift;
            u.YY = u.YPrime - t.YShift;

            //Step 4: Find rowGE and colGE
            u.RowGE = (int)Math.Round(u.YY / t.YDist + 1);
            u.ColGE = (int)Math.Round(u.XX / t.XDist + 1);

            //Step 5: Find rowRaft and colRaft
            {
                //Vinay's update 11/13
                bool A0A0Bottom, A0A0Left;
                RaftOriginIsBottomLeft(t.CalRafts, out A0A0Bottom, out A0A0Left);

                u.RowRaft = u.ColGE;
                u.ColRaft = 101 - u.RowGE;
                if (A0A0Bottom == true && A0A0Left == true)
                {
                    u.RowRaft = u.ColGE;
                    u.ColRaft = 101 - u.RowGE;
                }
                if (A0A0Bottom == false && A0A0Left == true)
                {
                    u.RowRaft = u.ColGE;
                    u.ColRaft = u.RowGE;
                }
                if (A0A0Bottom == false && A0A0Left == false)
                {
                    u.RowRaft = 101 - u.ColGE;
                    u.ColRaft = 101 - u.RowGE;
                }
                if (A0A0Bottom == true && A0A0Left == false)
                {
                    u.RowRaft = 101 - u.ColGE;
                    u.ColRaft = u.RowGE;
                }
            }
            //u.RowRaft = u.ColGE;   //Previous version of the code . . .
            //u.ColRaft = 101 - u.RowGE;

            //Step 6: Translate to Raft Coordinates
            rID += (char)(((u.RowRaft - 1) / 10) + 65);
            rID += (char)(((u.RowRaft - 1) % 10) + 48);
            rID += (char)(((u.ColRaft - 1) / 10) + 65);
            rID += (char)(((u.ColRaft - 1) % 10) + 48);

            //Step 7: Find SOnRaft, DistToEdge
            double XOnRaft = u.XX - (u.ColGE - 1) * t.XDist + (0.5 * t.XDist);
            double YOnRaft = u.YY - (u.RowGE - 1) * t.YDist + (0.5 * t.YDist);
            double[] DistToEdgeArray = { //dist from top, right, bottom, and left edges in array indeces 0,1,2,3
                    Math.Abs(YOnRaft),
                    Math.Abs(t.XDist - XOnRaft),
                    Math.Abs(t.YDist - YOnRaft),
                    Math.Abs(XOnRaft)
                };
            double MinDistToEdge = DistToEdgeArray[0];
            //double MinDistToEdge = XOnRaft;
            for (int i = 1; i <= DistToEdgeArray.Length - 1; i++)
            {
                if (DistToEdgeArray[i] < MinDistToEdge) MinDistToEdge = DistToEdgeArray[i];
            }
            //Step 8: Assign relevant variables to return object and return

            rr.XOnRaft = XOnRaft;
            rr.YOnRaft = YOnRaft;
            rr.MinDistToEdge = MinDistToEdge;
            rr.RaftCorners = FindRaftCornersMicrons(t, rr);
            if (u.RowRaft < 0 || u.ColRaft < 0 || u.RowRaft > 260 || u.ColRaft > 260)
            {
                rr.RaftID = "XXXX";
            }

            rID = rID.Replace(",", ".").Replace("\t", "|");
            rr.RaftID = rID; //Regex r = new Regex("^[a-zA-Z0-9]*$");   // to ensure no funkyness

            return rr;
        }

        /// <summary>
        /// Given a transfrom and ReturnRaft, finds the original microscope x and y coordinates of a raft
        /// in the format
        /// {{top left x, top left y},
        /// {top right x, top right y},
        /// {bottom right x, bottom right y},
        /// {bottom left x, bottom left y},}
        /// </summary>
        /// <param name="t"></param>
        /// <param name="rr"></param>
        /// <returns></returns>
        public static List<Tuple<double, double>> FindRaftCornersMicrons(Transform t, ReturnPoint rr)
        {
            double raftUpperLeftX = (rr.U.ColGE - 1) * t.XDist - 0.5 * t.XDist;
            double raftUpperLeftY = (rr.U.RowGE - 1) * t.YDist - 0.5 * t.YDist;
            List<Tuple<double, double>> raftCornersMicrons = new List<Tuple<double, double>> { };
            List<Tuple<double, double>> raftCornersTransformed = new List<Tuple<double, double>>
                    {
                        new Tuple<double, double>(raftUpperLeftX, raftUpperLeftY),
                        new Tuple<double, double>(raftUpperLeftX + t.XDist, raftUpperLeftY),
                        new Tuple<double, double>(raftUpperLeftX + t.XDist, raftUpperLeftY + t.YDist),
                        new Tuple<double, double>(raftUpperLeftX, raftUpperLeftY + t.YDist)
                    };
            for (int i = 0; i < 4; i++)
            {
                raftCornersMicrons.Add
                (new Tuple<double, double>
                    (ReverseX(raftCornersTransformed[i].Item1, raftCornersTransformed[i].Item2, t),
                        ReverseY(raftCornersTransformed[i].Item1, raftCornersTransformed[i].Item2, t)
                    )
                );
            }
            return raftCornersMicrons;
        }

        /// <summary>
        /// Given transformed coordinates and a transform, returns original, microscope x coordinate in microns.
        /// </summary>
        /// <param name="xx"></param>
        /// <param name="yy"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static double ReverseX(double xx, double yy, Transform t)
        {
            double x = Math.Cos(t.Theta) * (xx + t.XShift) / (Math.Cos(t.Phi) * Math.Cos(t.Theta) + Math.Sin(t.Phi) * Math.Sin(t.Theta)) -
                     Math.Sin(t.Phi) * (yy + t.YShift) / (Math.Cos(t.Phi) * Math.Cos(t.Theta) + Math.Sin(t.Phi) * Math.Sin(t.Theta));
            return x;
        }

        /// <summary>
        /// Given transformed coordinates and a transform, returns original, microscope y coordinate in microns.
        /// </summary>
        /// <param name="xx"></param>
        /// <param name="yy"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static double ReverseY(double xx, double yy, Transform t)
        {
            double y = Math.Sin(t.Theta) * (xx + t.XShift) / (Math.Cos(t.Phi) * Math.Cos(t.Theta) + Math.Sin(t.Phi) * Math.Sin(t.Theta)) +
                    Math.Cos(t.Phi) * (yy + t.YShift) / (Math.Cos(t.Phi) * Math.Cos(t.Theta) + Math.Sin(t.Phi) * Math.Sin(t.Theta));
            return y;
        }
    }


    /// <summary>
    /// Calibration information
    /// </summary>
    public class CalibrationPoints
    {
        public CalibrationPoints()
        {
            //To Serialize we need an empty constructor
            Dirty = false;
        }

        public CalibrationPoints(KnownPoint ll, KnownPoint ul, KnownPoint ur)
        {
            this.LowerLeft = ll;
            this.UpperLeft = ul;
            this.UpperRight = ur;
        }

        public KnownPoint LowerLeft { get; set; }
        public KnownPoint UpperLeft { get; set; }
        public KnownPoint UpperRight { get; set; }
        public DateTime DateCalibrated { get; set; }
        [XmlIgnore]
        public bool Dirty;
        public string Well { get; set; }

        private List<KnownPoint> _KnownPoints = null;
        [XmlIgnore]
        public List<KnownPoint> KnownPoints
        {
            get
            {
                if (_KnownPoints == null) _KnownPoints = new List<KnownPoint>(3) { LowerLeft, UpperLeft, UpperRight };
                return _KnownPoints;
            }
        }

        private Transform MainTransform = null;

        public ReturnPoint Find_RR(double X, double Y)
        {
            UnknownPoint u = new UnknownPoint(X, Y);
            if (MainTransform == null) MainTransform = new Transform(this);
            return MainTransform.Find_RR(u);
        }

        public bool Save(string SavePath)
        {
            DateCalibrated = DateTime.Now;
            XmlSerializer x = new XmlSerializer(typeof(CalibrationPoints));
            using (StreamWriter SW = new StreamWriter(SavePath))
            {
                x.Serialize(SW, this);
                SW.Close();
                Dirty = false;
                return true;
            }
        }

        public bool SaveDefault(string FolderPath)
        {
            string p = Path.Combine(FolderPath, CalibrationName);
            return Save(p);
        }

        public string CalibrationName { get => CalibrationNameConstruct(Well); }

        public static string CalibrationNameConstruct(string WellLabel = "") { return (CalibrationNamePart + " " + WellLabel).Trim() + "." + CalibrationExtension; }
        public static string CalibrationNamePart = "IMGCalibration";
        public static string CalibrationExtension = "XML";

        /// <summary>
        /// Give this the folder that the XDCE file is in, and it will guess on the file name here.  This is fast, but if it returns null, there may be a calibration of a different name or in a different place
        /// </summary>
        public static CalibrationPoints Load_Assumed(string FolderThatXDCEisIn, string WellLabel = "")
        {
            string PathAssumed = Path.Combine(FolderThatXDCEisIn, CalibrationNameConstruct(WellLabel));
            FileInfo FI = new FileInfo(PathAssumed);
            if (FI.Exists) return Load(PathAssumed);
            return null;
        }

        /// <summary>
        /// Must give the fully qualified path, will not look for a calibration file (use Load_Assumed for that)
        /// </summary>
        public static CalibrationPoints Load(string LoadPath)
        {
            XmlSerializer x = new XmlSerializer(typeof(CalibrationPoints));
            using (FileStream F = new FileStream(LoadPath, FileMode.Open))
            {
                CalibrationPoints CR = (CalibrationPoints)x.Deserialize(F);
                F.Close();
                if (CR.DateCalibrated < new DateTime(2019, 8, 15)) CR.DateCalibrated = new DateTime(2019, 8, 15);
                return CR;
            }
        }

        public static bool OnSameVertAxis(string pointOne, string pointTwo)
        {
            return (pointOne[0] == pointTwo[0] && pointOne[1] == pointTwo[1]);
        }

        public static bool OnSameHorAxis(string pointOne, string pointTwo)
        {
            return (pointOne[2] == pointTwo[2] && pointOne[3] == pointTwo[3]);
        }
    }

    /// <summary>
    /// Known Position
    /// </summary>
    public class KnownPoint
    {
        public KnownPoint()
        {

        }

        public KnownPoint(string ID, double x, double y)
        {
            this.ID = ID;
            this.X = x;
            this.Y = y;
        }

        public string ID { get; set; }
        public double X { get; set; }
        public double Y { get; set; }

        [XmlIgnore]
        public double XPrime { get; set; }
        [XmlIgnore]
        public double YPrime { get; set; }

        // these are the row and col of the raft from the microscopes perspective from 1 to 100
        [XmlIgnore]
        public int RowGE
        {
            get
            {
                if (_RowGE == int.MinValue)
                {
                    _RowGE = 100 - (((int)ID[2] - 65) * 10 + ((int)ID[3] - 48));
                }
                return _RowGE;
            }
            set { _RowGE = value; }
        }
        private int _RowGE = int.MinValue;

        [XmlIgnore]
        public int ColGE
        {
            get
            {
                if (_ColGE == int.MinValue)
                {
                    _ColGE = ((int)ID[0] - 65) * 10 + (int)ID[1] - 48 + 1;
                }
                return _ColGE;
            }
            set
            {
                _ColGE = value;
            }
        }
        private int _ColGE = int.MinValue;

        public void Setup_XYPrime(double Phi, double Theta)
        {
            //Phi and Theta are determined from Calibration
            XPrime = Math.Sqrt(X * X + Y * Y) * Math.Cos(Math.Atan(Y / X) - Phi);
            YPrime = Math.Sqrt(X * X + Y * Y) * Math.Cos(Math.Atan(X / Y) + Theta);
        }
    }

    /// <summary>
    /// Unknown Raft Position
    /// </summary>
    public class UnknownPoint
    {
        public UnknownPoint(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public double X { get; set; }
        public double XPrime { get; set; }
        public double XX { get; set; }
        public double Y { get; set; }
        public double YPrime { get; set; }
        public double YY { get; set; }
        public int RowGE { get; set; }
        public int ColGE { get; set; }
        public int RowRaft { get; set; }
        public int ColRaft { get; set; }

        public string Find_ID(Transform t)
        {
            return t.Find_ID(this);
        }
    }

    public class ReturnPoint
    {
        public ReturnPoint(UnknownPoint u)
        {
            this.U = u;
        }
        public UnknownPoint U { get; set; }
        public string RaftID { get; set; }
        public double XOnRaft { get; set; }
        public double YOnRaft { get; set; }
        public double MinDistToEdge { get; set; }

        //In Microns, convert back to pixels
        public List<Tuple<double, double>> RaftCorners { get; set; }
    }
}
