﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Drawing;

namespace FIVE_IMG
{
    namespace Plate_DB_NS
    {

        public class Image_List_Class : IEnumerable<ImgWrap>
        {
            public Dictionary<string, ImgWrap> _Dict;

            public int Count { get => _Dict.Count; }

            public Image_List_Class(DirectoryInfo CreateFromImages)
            {
                Init();
                foreach (FileInfo FI in CreateFromImages.GetFiles())
                {
                    if (ImgWrap.IsImage(FI.FullName))
                    {
                        ImgWrap tI = new ImgWrap(FI.FullName);
                        string Key = First2Coord(FI.Name);
                        if (_Dict.ContainsKey(Key)) Key = Path.GetFileNameWithoutExtension(FI.Name);
                        if (_Dict.ContainsKey(Key)) Key = FI.Name; //Ensure there is no collision
                        _Dict.Add(Key, tI);
                    }
                }
            }

            public static string First2Coord(string name)
            {
                string[] Arr = name.Split('_');
                if (Arr.Length == 1) return name;
                for (int i = 0; i < 2; i++)
                {
                    if (Arr[i].Contains(".")) Arr[i] = Arr[i].Substring(0, Arr[i].IndexOf("."));
                }
                return Arr[0] + '_' + Arr[1];
            }

            public Image_List_Class()
            {
                Init();
            }

            private void Init()
            {
                _Dict = new Dictionary<string, ImgWrap>();
            }

            public IEnumerator<ImgWrap> GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

            internal ImgWrap ClosestImage(ImgWrap Compare)
            {
                SortedList<double, ImgWrap> Sorted = new SortedList<double, ImgWrap>();
                double dist;
                foreach (ImgWrap IMG in this)
                {
                    dist = IMG.DistanceFrom(Compare);
                    if (!Sorted.ContainsKey(dist)) Sorted.Add(dist, IMG);
                }
                return Sorted.Values[0];
            }

            /// <summary>
            /// Looks through the Image List to find the field with these coordinates
            /// </summary>
            /// <param name="Point"></param>
            /// <returns></returns>
            internal ImgWrap GetFromPosition(PointF Point)
            {
                string Key;
                //Remember that the Leica's position gets saved differently than the position requested . . I think that is why It is off by one . . 


                Key = Point.X.ToString("00") + '_' + Point.Y.ToString("00");
                if (_Dict.ContainsKey(Key)) return _Dict[Key];
                Key = (Point.X + 0).ToString("00") + '_' + (Point.Y - 1).ToString("00");
                if (_Dict.ContainsKey(Key)) return _Dict[Key];
                Key = (Point.X - 1).ToString("00") + '_' + (Point.Y + 0).ToString("00");
                if (_Dict.ContainsKey(Key)) return _Dict[Key];
                Key = (Point.X - 1).ToString("00") + '_' + (Point.Y - 1).ToString("00");
                if (_Dict.ContainsKey(Key)) return _Dict[Key];

                //If there is only one thing in the dictionary, try it
                if (_Dict.Count == 1) return _Dict.First().Value;

                //Should look for the closest field, since the direct approach is solved above . . 
                foreach (string Name in _Dict.Keys)
                {
                    if (Name.StartsWith(Point.X.ToString() + "_" + Point.Y.ToString()))
                    {
                        return _Dict[Name];
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Saves each session (i.e. each time the plate is removed then placed back onto the instrument)
        /// Designed to work with sessions from multiple microscopes
        /// </summary>
        public class Plate_Session_Class  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        {
            public Plate_Class Plate;
            public string Session_Name;
            public string Session_Type;
            public DateTime Session_DateTime;

            public Image_List_Class Image_List { get; set; }

            public override string ToString()
            {
                return Session_Name;
            }

            [XmlIgnore]
            public bool Fresh { get; set; } //Was this freshly determined or was it just loaded

            public Plate_Session_Class()
            {
                Init();
            }

            public Plate_Session_Class(DirectoryInfo SessionFolder, Plate_Class plate_Parent)
            {
                Init();
                Plate = plate_Parent;
                Session_Name = SessionFolder.Name;
                ParseName(Session_Name, out Session_DateTime, out Session_Type);
                Image_List = new Image_List_Class(SessionFolder);
                Fresh = true;
            }

            private void Init()
            {
                Fresh = false;
            }

            public static string DateFormat = "yyyyMMdd_HHmmss";
            private static System.Globalization.CultureInfo DFprovider = System.Globalization.CultureInfo.InvariantCulture;

            internal static void ParseName(string Name, out DateTime SDate, out string SType)
            {
                string[] Arr = Name.Split('-'); //20210415_213021-C
                SType = Arr.Length == 1 ? "" : Arr[1];
                try
                {
                    SDate = DateTime.ParseExact(Arr[0], DateFormat, DFprovider);
                }
                catch
                {
                    SDate = DateTime.MinValue;
                }
            }

            internal static bool IsValidName(string name)
            {
                DateTime SD; string ST;
                ParseName(name, out SD, out ST);
                return SD != DateTime.MinValue;
            }

            /// <summary>
            /// Looks through the Image List to find the field with these coordinates
            /// </summary>
            /// <param name="currentFOVPos"></param>
            /// <returns></returns>
            internal ImgWrap GetFromPosition(PointF currentFOVPos)
            {
                return Image_List.GetFromPosition(currentFOVPos);
            }
        }

        /// <summary>
        /// Stores the basic information for a "plate". A plate is a vessel that you want to image, and its iD stays the same for different imaging sessions
        /// </summary>
        public class Plate_Class  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        {
            public Plate_DB_Class DB_Parent;
            public string Path { get; set; }
            public string ID { get; set; }

            public List<Plate_Session_Class> SessionList;

            public Plate_Class()
            {

            }

            public Plate_Class(DirectoryInfo Folder, Plate_DB_Class Parent)
            {
                Init();
                this.Path = Folder.FullName;
                this.ID = Folder.Name.ToUpper().Trim();
                this.DB_Parent = Parent;

                Plate_Session_Class PSC;
                foreach (DirectoryInfo subDIRs in Folder.GetDirectories())
                {
                    if (Plate_Session_Class.IsValidName(subDIRs.Name))
                    {
                        PSC = new Plate_Session_Class(subDIRs, this);
                        SessionList.Add(PSC);
                    }
                }
                if (SessionList.Count == 0)
                {
                    PSC = new Plate_Session_Class(Folder, this);
                    SessionList.Add(PSC);
                }
            }

            private void Init()
            {
                SessionList = new List<Plate_Session_Class>();
            }

            public override string ToString()
            {
                return ID;
            }

            public void AlignSessions(Plate_Session_Class PSC_Origin, Plate_Session_Class PSC_ToAlign)
            {
                List<Register_Return> RRList = new List<Register_Return>(); Register_Return RR;
                List<Tuple<Point, Point>> BeforeAfter = new List<Tuple<Point, Point>>();
                StringBuilder sB = new StringBuilder();
                sB.Append("x1" + "\t" + "x2" + "\t" + "y1" + "\t" + "y2" + "\r\n");
                foreach (ImgWrap IMG in PSC_Origin.Image_List)
                {
                    //First find the closest image to compare with based on the name's coordinates (this won't necessarily work for the Incell <> Leica)
                    ImgWrap Closest = PSC_ToAlign.Image_List.ClosestImage(IMG);

                    //Start out by trying a small area in the middle of the image
                    ImgWrap FindThis = Closest.MiddleCrop(0.25);
                    ImgWrap Among = IMG.MiddleCrop(0.4); //This should be a different method
                    int AdjX = (int)(FindThis.X_um - Among.X_um); int AdjY = (int)(FindThis.Y_um - Among.Y_um);

                    //Now Register those two images
                    RR = CrossCorr.Register(FindThis, Among);
                    RRList.Add(RR);

                    //Record the before and after points
                    Tuple<Point, Point> BA = new Tuple<Point, Point>(new Point((int)Among.X_um, (int)Among.Y_um), new Point(
                        (int)(FindThis.X_um - RR.Location.X),  //This is somewhat wrong, since PIXELS are NOT EQUAL to Microns
                        (int)(FindThis.Y_um - RR.Location.Y)));
                    sB.Append(BA.Item1.X + "\t" + BA.Item2.X + "\t" + BA.Item1.Y + "\t" + BA.Item2.Y + "\r\n");
                    BeforeAfter.Add(BA);

                    //Report this out in time
                    System.Diagnostics.Debug.Print(RRList[RRList.Count - 1].Location.ToString() + " Result" + "\r\n");
                }
                File.WriteAllText(@"c:\temp\deviations.txt", sB.ToString());
            }

        }

        public class Plate_DB_Class : IEnumerable<Plate_Class> // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        {
            private Dictionary<string, Plate_Class> _Dict;
            public int Count { get => _Dict.Count; }

            public string BasePath { get; set; }

            public Plate_DB_Class()
            {
                Init();
            }

            /// <summary>
            /// Makes a new DB from a folder system
            /// </summary>
            /// <param name="Construct_From_Folder"></param>
            public Plate_DB_Class(string Construct_From_Folder)
            {
                Init();
                DirectoryInfo DI = new DirectoryInfo(Construct_From_Folder);
                if (!DI.Exists) return;
                Plate_Class PC;
                foreach (DirectoryInfo dir in DI.GetDirectories())
                {
                    PC = new Plate_Class(dir, this);
                    Add(PC);
                }
                if (Count == 0) //Assume this is just the final folder
                {
                    PC = new Plate_Class(DI, this);
                    Add(PC);
                }
            }

            private void Init()
            {
                _Dict = new Dictionary<string, Plate_Class>();
            }

            public void Add(Plate_Class Plate)
            {
                _Dict.Add(Plate.ID, Plate);
            }

            public static string DefaultSettingsName = "FIVE_IMG_DB.xml";

            public static Plate_DB_Class LoadDB_FromFolder(string FolderPath)
            {
                return LoadDB(Path.Combine(FolderPath, DefaultSettingsName));
            }

            /// <summary>
            /// Loads a Plate Database from a file (not by refreshing)
            /// </summary>
            /// <param name="FullPath"></param>
            /// <returns></returns>
            public static Plate_DB_Class LoadDB(string FullPath)
            {
                XmlSerializer x = new XmlSerializer(typeof(Plate_DB_Class));
                using (FileStream F = new FileStream(FullPath, FileMode.Open))
                {
                    Plate_DB_Class CR = (Plate_DB_Class)x.Deserialize(F);
                    F.Close();
                    return CR;
                }
            }

            public void Save()
            {
                Save(Path.Combine(this.BasePath, DefaultSettingsName));
            }

            public void Save(string Path)
            {
                using (var writer = new StreamWriter(Path))
                {
                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(writer, this);
                    writer.Flush();
                }
            }

            public IEnumerator<Plate_Class> GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

            internal static void AATest()
            {
                Plate_DB_Class PDC = new Plate_DB_Class(@"c:\Temp\PlateDB");
            }

            public Plate_Session_Class FindNewestSession(string PartOfName)
            {
                Plate_Session_Class plate_Session_Class = null;
                foreach (Plate_Class plate_Class in this)
                {
                    if (plate_Class.ID.ToUpper().Contains(PartOfName))
                    {
                        plate_Session_Class = plate_Class.SessionList.Aggregate((x, y) => x.Session_DateTime > y.Session_DateTime ? x : y);
                        return plate_Session_Class;
                    }
                }
                return plate_Session_Class;
            }
        }

    }
}
