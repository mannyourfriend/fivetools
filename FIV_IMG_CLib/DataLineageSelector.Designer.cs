﻿
namespace FIV_IMG_CLib
{
    partial class DataLineageSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txBx_CatalogLocation = new System.Windows.Forms.TextBox();
            this.btn_Load = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.txBx_Search = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox_Main = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // txBx_CatalogLocation
            // 
            this.txBx_CatalogLocation.Enabled = false;
            this.txBx_CatalogLocation.Location = new System.Drawing.Point(12, 372);
            this.txBx_CatalogLocation.Name = "txBx_CatalogLocation";
            this.txBx_CatalogLocation.Size = new System.Drawing.Size(487, 20);
            this.txBx_CatalogLocation.TabIndex = 0;
            // 
            // btn_Load
            // 
            this.btn_Load.Location = new System.Drawing.Point(343, 10);
            this.btn_Load.Name = "btn_Load";
            this.btn_Load.Size = new System.Drawing.Size(75, 23);
            this.btn_Load.TabIndex = 2;
            this.btn_Load.Text = "Load";
            this.btn_Load.UseVisualStyleBackColor = true;
            this.btn_Load.Click += new System.EventHandler(this.btn_Load_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(424, 10);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 3;
            this.btn_Close.Text = "Close";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // txBx_Search
            // 
            this.txBx_Search.Location = new System.Drawing.Point(62, 12);
            this.txBx_Search.Name = "txBx_Search";
            this.txBx_Search.Size = new System.Drawing.Size(275, 20);
            this.txBx_Search.TabIndex = 4;
            this.txBx_Search.TextChanged += new System.EventHandler(this.txBx_Search_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Search";
            // 
            // listBox_Main
            // 
            this.listBox_Main.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox_Main.FormattingEnabled = true;
            this.listBox_Main.ItemHeight = 14;
            this.listBox_Main.Location = new System.Drawing.Point(12, 38);
            this.listBox_Main.Name = "listBox_Main";
            this.listBox_Main.Size = new System.Drawing.Size(487, 326);
            this.listBox_Main.TabIndex = 6;
            this.listBox_Main.SelectedIndexChanged += new System.EventHandler(this.listBox_Main_SelectedIndexChanged);
            this.listBox_Main.DoubleClick += new System.EventHandler(this.listBox_Main_DoubleClick);
            // 
            // DataLineageSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 402);
            this.Controls.Add(this.listBox_Main);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txBx_Search);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Load);
            this.Controls.Add(this.txBx_CatalogLocation);
            this.Name = "DataLineageSelector";
            this.Text = "Select an Experiment Below";
            this.Load += new System.EventHandler(this.DataLineageSelector_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txBx_CatalogLocation;
        private System.Windows.Forms.Button btn_Load;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.TextBox txBx_Search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox_Main;
    }
}