﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Tensorflow;
using Tensorflow.NumPy;
using static Tensorflow.Binding;
using System.Drawing;
using System.Data;
using System.Text.Json.Serialization;
using System.Text.Json;
using ICSharpCode.SharpZipLib.Tar;

namespace FIVE
{
    namespace TF
    {
        public class ModelInferSettings
        {
            public string Model_Classification_Path { get; set; }
            public string Model_Measure_Path { get; set; }
            public float ClassificationKeepThreshold { get; set; }
            public bool SaveImagesWhileInferring { get; set; }
            public byte ExistingAnnotations_1All_2OnlyExisting_3NotExisting_Classification { get; set; }
            public byte ExistingAnnotations_1All_2OnlyExisting_3NotExisting_Measure { get; set; }
            public string Measure_OnlyExistingByName { get; set; }

            public ModelInferSettings()
            {
                Model_Classification_Path = Model_Measure_Path = "";
                ClassificationKeepThreshold = 0.8F;
                SaveImagesWhileInferring = false;
                ExistingAnnotations_1All_2OnlyExisting_3NotExisting_Measure = ExistingAnnotations_1All_2OnlyExisting_3NotExisting_Classification = 1;
                Measure_OnlyExistingByName = "";
            }

            public static ModelInferSettings Default01()
            {
                var MIS = new ModelInferSettings();

                MIS.Model_Classification_Path = "S:\\Raft\\FIV827\\M 20230124 MC\\m acc=0.8177 CNN Cnns=6.5\\frozen_graph.pb";
                MIS.Model_Measure_Path = @"S:\Phys\FIV823 NeuroMito\4a M\ acc_0.0530 CNN CNNs_7 kn_4 flt_22 iRt_1.75 ds__1000_ nE_123 bSz_32 bNm_0 i_12\ acc_0.0530 CNN CNNs_7 kn_4 flt_22 iRt_1.75 ds__1000_ nE_123 bSz_32 bNm_0 i_12 F.pb";
                MIS.ClassificationKeepThreshold = 0.7F;
                MIS.SaveImagesWhileInferring = false;
                MIS.ExistingAnnotations_1All_2OnlyExisting_3NotExisting_Classification = 1;
                MIS.ExistingAnnotations_1All_2OnlyExisting_3NotExisting_Measure = 2;
                MIS.Measure_OnlyExistingByName = "1 Neuron";

                return MIS;
            }
        }

        public class TF_Model_Metadata
        {
            //Remember classes could also be measurement types

            [JsonIgnore]
            public int ClassesCount => Index2ClassInfo.Count;
            public Dictionary<int, TF_Model_ClassInfo> Index2ClassInfo { get; set; }
            public string ModelName { get; set; }
            public string ModelPath { get; set; }
            public string ModelType { get; set; }
            public string ModelDate { get; set; }
            public int TrainingSize { get; set; }

            /// <summary>
            /// Prediction probabilities above this amount will be called positive for this class, but can be adjusted with the per-class settings
            /// </summary>
            public float ThresholdDefault { get; set; }

            public TF_Model_Metadata()
            {
                Index2ClassInfo = new Dictionary<int, TF_Model_ClassInfo>();
                ThresholdDefault = 0.9F;
                ModelName = "";
                ModelType = "";
                ModelPath = "";
                ModelDate = DateTime.Now.ToShortDateString();
                TrainingSize = 0;
            }

            public TF_Model_ClassInfo this[int index]
            {
                get => Index2ClassInfo[index];
            }

            public static TF_Model_Metadata Example_acc81PerCNN()
            {
                var TFMM = new TF_Model_Metadata();

                TFMM.Add(0, "0 Empty", -1, new int[3] { 184, 118, 232 }, true);
                TFMM.Add(1, "0 Neurites No Cell", -1, new int[3] { 193, 73, 75 }, true);
                TFMM.Add(2, "1 Cell Tub No Neurite", -1, new int[3] { 255, 255, 0 }, false);
                TFMM.Add(3, "1 Dead (no tubulin)", -1, new int[3] { 113, 206, 14 }, true);
                TFMM.Add(4, "1 Neuron", -1, new int[3] { 0, 255, 30 }, false);
                TFMM.Add(5, "1 Neuron Poor Qual", -1, new int[3] { 0, 200, 0 }, false);
                TFMM.Add(6, "1 Cells Tub No Neurite", -1, new int[3] { 167, 122, 36 }, false);
                TFMM.Add(7, "2 Nuc 1+ Neuron", -1, new int[3] { 255, 30, 0 }, false);
                TFMM.Add(8, "2 Nuc 1+ Neuron Poor Qual", -1, new int[3] { 87, 138, 227 }, true);
                TFMM.Add(9, "3-4 Nuclei", -1, new int[3] { 250, 245, 255 }, true);
                TFMM.Add(10, "5+ Nuclei", -1, new int[3] { 62, 45, 26 }, true);
                TFMM.Add(11, "Focus (only if you really cant tell)", -1, new int[3] { 253, 246, 41 }, true);

                TFMM.ModelName = "acc=0.8177 CNN Cnns=6.5";
                TFMM.ThresholdDefault = 0.65F;
                TFMM.ModelType = "Multiclass";
                TFMM.ModelDate = DateTime.Now.AddDays(-30).ToLongTimeString();
                TFMM.ModelPath = "S:\\Raft\\FIV827\\M 20230124 MC\\m acc=0.8177 CNN Cnns=6.5\\frozen_graph.pb";
                return TFMM;
            }

            public void Add(int Idx, string Name, int TrainingCount, int[] RGB, bool ignore = false, string comment = "")
            {
                var TFCI = new TF_Model_ClassInfo() { Index = Idx, Name = Name, TrainingCount = TrainingCount, RGB = RGB, Comment = comment, ThresholdMultiplier = 1, Ignore = ignore };
                Index2ClassInfo.Add(Idx, TFCI);
            }

            public void Save(string FilePath, bool UseDefaultName = true)
            {
                string NewPath = UseDefaultName ? Path.Combine(Path.GetDirectoryName(FilePath), DefaultName) : FilePath;
                File.WriteAllText(NewPath, Serialize());
            }

            public static string DefaultName = "FIVE_metadata.json";

            public static TF_Model_Metadata LoadNeighbor(string FilepathOfPB)
            {
                string NewPath = Path.Combine(Path.GetDirectoryName(FilepathOfPB), DefaultName);
                return Load(NewPath);
            }

            public static TF_Model_Metadata Load(string Filepath)
            {
                if (!File.Exists(Filepath)) return null;
                TF_Model_Metadata TMM = null;
                try
                {
                    string json = File.ReadAllText(Filepath);
                    TMM = Deserialize(json);
                }
                catch
                {
                    //problem loading the json
                }
                return TMM;
            }

            public string Serialize()
            {
                var options = new JsonSerializerOptions { WriteIndented = true, Converters = { new JsonStringEnumConverter(JsonNamingPolicy.CamelCase) } };
                return JsonSerializer.Serialize<TF_Model_Metadata>(this, options);

            }

            public static TF_Model_Metadata Deserialize(string json)
            {
                return JsonSerializer.Deserialize<TF_Model_Metadata>(json);
            }

            public IEnumerator<TF_Model_ClassInfo> GetEnumerator()
            {
                return Index2ClassInfo.Values.GetEnumerator();
            }

            public string DeriveFullAnnotation(float[] allScores)
            {
                var sort = new SortedDictionary<float, string>();
                for (int i = 0; i < ClassesCount; i++)
                {
                    float key = -allScores[i];
                    while (sort.ContainsKey(key)) key += float.Epsilon;
                    sort.Add(key, Index2ClassInfo[i].Name);
                }
                //throw new NotImplementedException();
                return string.Join("\r\n", sort.Select(x => x.Value + " = " + (-x.Key).ToString("0.000")));
            }
        }

        public class TF_Model_ClassInfo
        {
            public TF_Model_ClassInfo()
            {
                Index = -1;
                TrainingCount = -1;
                RGB = new int[] { 0, 0, 0 };
                Ignore = false;
                ThresholdMultiplier = 1;
                Name = "";
                Comment = "";
                ScalerMin = 0;
                ScalerMax = 1;
            }

            public int Index { get; set; }
            public string Name { get; set; }
            public long TrainingCount { get; set; }
            public int[] RGB { get; set; }
            public string Comment { get; set; }
            /// <summary>
            /// This will multiply the default threshold being used overall (say 65% is needed for positive), but this multiplier. So for this particular class, if ThresholdMultiplier = 1.2, then the prediction would have to be over 78% to be called positive
            /// </summary>
            public float ThresholdMultiplier { get; set; }
            public float ScalerMin { get; set; }
            public float ScalerMax { get; set; }
            public bool Ignore { get; set; }

            public override string ToString() { return Name; }

            [JsonIgnore]
            public Color Color => Color.FromArgb(RGB[0], RGB[1], RGB[2]);
        }

        public class TF_Image_Model
        {
            protected Tensorflow.Graph graph;
            protected Tensor Input;
            protected Tensor Output;
            public string ModelName;
            public TF_Model_Metadata Metadata;

            public static float[] Slice(float[,] MultiArray, int Idx)
            {
                var T = new float[MultiArray.GetLength(1)];
                for (int i = 0; i < MultiArray.GetLength(1); i++)
                {
                    T[i] = MultiArray[Idx, i];
                }
                return T;
            }

            public static void AAATest()
            {
                var TF = LoadFromFrozen(@"S:\Raft\FIV827\M 20230124 MC\m acc=0.8177 CNN Cnns=6.5\frozen_graph.pb");
                var (Paths, XTest) = LoadImages(@"S:\Raft\FIV827\001 SF Sort\SmallerTrainingSet", "*.bmp", 0.25F);
                var NDres = TF.Predict(XTest);
                var res2 = NDArrayConvert(NDres);
                SaveNDArrayToFile(Paths, NDres, @"c:\temp\pred02.txt");
            }


            /// <summary>
            /// Loads a Frozen TF.Keras Model (not savedmodel or .h5, has the .pb extension, but must be explicetly frozen)
            /// </summary>
            /// <param name="PathToPB">Full filename of frozen model file</param>
            /// <param name="InputByName">Leave this blank to use the first part of the graph</param>
            /// <param name="OutputByName">Leave this blank to use the last part of the graph</param>
            /// <returns></returns>
            public static TF_Image_Model LoadFromFrozen(string PathToPB, string InputByName = "", string OutputByName = "")
            {
                var TFIM = new TF_Image_Model();

                TFIM.graph = tf.Graph().as_default();
                TFIM.graph.Import(PathToPB);
                TFIM.ModelName = Path.GetFileNameWithoutExtension(PathToPB);
                TFIM.Metadata = TF_Model_Metadata.LoadNeighbor(PathToPB);

                //For saving metadata from here..
                //var TFMM = TF_Model_Metadata.Example_acc81PerCNN();
                //TFMM.Save("S:\\Raft\\FIV827\\M 20230124 MC\\m acc=0.8177 CNN Cnns=6.5\\"); //Needs the trailing slash

                //We may be able to just access the first and the last instead of these named ones . .
                if (InputByName == "") TFIM.Input = TFIM.graph.First();
                else TFIM.Input = TFIM.graph.OperationByName(InputByName); //"x"

                if (OutputByName == "") TFIM.Output = TFIM.graph.Last(); //"model_2/dense_7/Softmax"
                else TFIM.Output = TFIM.graph.OperationByName(OutputByName);

                return TFIM;
            }

            public static (string[] Paths, NDArray ImgData) LoadImages(string FolderWithImages, string Extension = "*.bmp", float FractionToInfer = 1)
            {
                int img_h = 128;
                int img_w = 128;
                int n_channels = 3;
                string[] ArrayFileName_Test;
                NDArray x_test;

                var ArrayPre = Directory.GetFiles(FolderWithImages, Extension, SearchOption.AllDirectories);
                ArrayFileName_Test = SubsetFiles(ArrayPre, FractionToInfer);
                x_test = np.zeros((ArrayFileName_Test.Length, img_h, img_w, n_channels), dtype: tf.float32);
                LoadImage_Direct(ArrayFileName_Test, x_test); //1.5 seconds for 170 ish images, TF version was slower

                return (ArrayFileName_Test, x_test);
            }

            public static (string[] Paths, NDArray ImgData) LoadImagesToND(Dictionary<string, Bitmap> bmps)
            {
                int img_w = bmps.Min(x => x.Value.Width);
                int img_h = bmps.Min(x => x.Value.Height);
                int n_channels = 3;
                NDArray x_test;

                x_test = np.zeros((bmps.Count, img_h, img_w, n_channels), dtype: tf.float32);
                var ArrayFileName_Test = new List<string>(bmps.Count);
                foreach (var KVP in bmps)
                {
                    ArrayFileName_Test.Add(KVP.Key);
                    x_test[ArrayFileName_Test.Count - 1] = TensorfromBitmap(KVP.Value, img_w, img_h, n_channels);
                }

                return (ArrayFileName_Test.ToArray(), x_test);
            }

            public static (string[] Paths, float[,,,] ImgData) LoadImagesToArray(Dictionary<string, Bitmap> bmps)
            {
                int img_w = bmps.First().Value.Width;
                int img_h = bmps.First().Value.Height;
                int n_channels = 3;

                float[,,,] Arr = new float[bmps.Count, img_h, img_w, n_channels];
                var ArrayFileName_Test = new List<string>(bmps.Count);
                foreach (var KVP in bmps)
                {
                    ArrayFileName_Test.Add(KVP.Key);
                    Add2ArrayfromBitmap(KVP.Value, img_w, img_h, n_channels, ref Arr, ArrayFileName_Test.Count - 1);
                }

                return (ArrayFileName_Test.ToArray(), Arr);
            }

            public NDArray Predict(NDArray x_test)
            {
                using (var sess = tf.Session(graph))
                {
                    var probility_result = sess.run(Output, (Input, x_test));
                    return probility_result;
                }
            }

            public static (float[,] fullResults, int[] maxIndex, float[] maxVal) NDArrayConvert(NDArray ArrayToConvert)
            {
                float[,] outside = new float[ArrayToConvert.shape[0], ArrayToConvert.shape[1]];
                int[] maxIndex = new int[ArrayToConvert.shape[0]];
                float[] maxVal = new float[ArrayToConvert.shape[0]];
                for (int i = 0; i < ArrayToConvert.shape[0]; i++)
                {
                    float tMax = float.MinValue; int jMax = 0;
                    var res = ArrayToConvert[i].ToArray();
                    for (int j = 0; j < res.Length; j++)
                    {
                        outside[i, j] = res[j];
                        if (res[j] > tMax) { tMax = res[j]; jMax = j; }
                    }
                    maxIndex[i] = jMax;
                    maxVal[i] = tMax;
                }
                return (outside, maxIndex, maxVal);
            }

            public static void SaveNDArrayToFile(string[] RowLabels, NDArray probility_result, string FileSavePath)
            {
                var sb = new StringBuilder();
                for (int i = 0; i < probility_result.shape[0]; i++)
                {
                    var file = RowLabels[i];
                    var res = probility_result[i].ToArray();
                    sb.Append(file + "\t");
                    foreach (var item in res) sb.Append(item + "\t");
                    sb.Append("\r\n");

                }
                File.WriteAllText(FileSavePath, sb.ToString());
            }

            public static string[] SubsetFiles(string[] FileList, float Fraction)
            {
                var _Rand = new Random(119);
                int newLength = (int)(FileList.Length * Fraction);
                var newList = new List<string>(newLength);
                foreach (var file in FileList)
                {
                    if (_Rand.NextDouble() < Fraction)
                    {
                        newList.add(file);
                        if (newList.Count >= newLength) break;
                    }
                }
                return newList.ToArray();
            }

            public static void LoadImage_Direct(string[] imgPaths, NDArray toReturn, bool UseTF = false)
            {
                if (UseTF)
                {
                    using var graph = tf.Graph().as_default();

                    for (int i = 0; i < imgPaths.Length; i++)
                    {
                        toReturn[i] = ReadTensorFromImageFile(imgPaths[i], graph);
                        if (i % 10 == 0) print(i);
                    }
                }
                else
                {

                    for (int i = 0; i < imgPaths.Length - 2; i++) //The -2 is to avoid a weird memory error, but it is incomplete
                    {
                        toReturn[i] = ReadTensorFromImageFile_Direct(imgPaths[i]);
                        if (i % 10 == 0) print(i);
                    }
                }
            }

            public static NDArray ReadTensorFromImageFile_Direct(string file_name)
            {
                using (var bitmap = new System.Drawing.Bitmap(file_name))
                {
                    return TensorfromBitmap(bitmap);
                }
            }

            public static float[,,] ArrayFromBitmap(Bitmap bitmap, int height, int width, int channels)
            {
                float[,,] data = new float[height, width, channels];

                // Lock the bitmap data to prevent the garbage collector from moving it around
                var bmpData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                // Calculate the stride (number of bytes per row, including padding)
                int stride = bmpData.Stride;

                // Access the raw pixel data as a byte array
                byte[] byteArray = new byte[stride * height];
                System.Runtime.InteropServices.Marshal.Copy(bmpData.Scan0, byteArray, 0, byteArray.Length);

                // Loop over the pixels of the image and convert them to floats
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int i = y * stride + x * 4;
                        data[y, x, 0] = (float)byteArray[i + 2];  // blue channel
                        data[y, x, 1] = (float)byteArray[i + 1];  // green channel
                        data[y, x, 2] = (float)byteArray[i];      // red channel
                    }
                }

                // Unlock the bitmap data
                bitmap.UnlockBits(bmpData);
                return data;
            }

            public static NDArray TensorfromBitmap(Bitmap bitmap, int width = -1, int height = -1, int channels = -1)
            {
                var data = ArrayFromBitmap(bitmap, width < 1 ? bitmap.Width : width, height < 1 ? bitmap.Height : height, channels < 1 ? 1 : channels);

                var array = np.array(data);
                return array;
            }

            public static void Add2ArrayfromBitmap(Bitmap bitmap, int width, int height, int channels, ref float[,,,] tensor, int currentIndex)
            {
                var image = ArrayFromBitmap(bitmap, width, height, channels);
                Array.Copy(image, 0, tensor, currentIndex * height * width * channels, height * width * channels);
            }

            private static NDArray ReadTensorFromImageFile(string file_name, Graph graph)
            {
                if (Path.GetExtension(file_name) == ".db") return null;
                var file_reader = tf.io.read_file(file_name, "file_reader");
                var get_img = tf.image.decode_image(file_reader);
                var cast = tf.cast(get_img, tf.float32);
                //var decodeJpeg = tf.image.decode_jpeg(file_reader, channels: n_channels, name: "DecodeJpeg");
                //var cast = tf.cast(decodeJpeg, tf.float32);
                //var dims_expander = tf.expand_dims(cast, 0);
                //var resize = tf.constant(new int[] { img_h, img_w });
                //var bilinear = tf.image.resize_bilinear(dims_expander, resize);
                //var sub = tf.subtract(bilinear, new float[] { img_mean });
                //var normalized = tf.divide(sub, new float[] { img_std });

                using (var sess = tf.Session(graph))
                {
                    return sess.run(cast);
                    //return sess.run(normalized);
                }
            }


        }

        public class TF_Measure_Results
        {
            private TF_Image_Model Model;
            public Dictionary<string, TF_Measure_Result_DataPoint> DataPoints;

            public TF_Measure_Results(TF_Image_Model tFIM)
            {
                DataPoints = new Dictionary<string, TF_Measure_Result_DataPoint>();
                this.Model = tFIM;
            }

            public void AddSet(string[] y, float[,] res2, Dictionary<string, string> annotations)
            {
                //The classes tell us what they are. We can now convert these results into the proper number using the scalers and go from there . . 
                for (int r = 0; r < y.Length; r++)
                {
                    var dataPre = TF_Image_Model.Slice(res2, r);
                    for (int c = 0; c < Model.Metadata.ClassesCount; c++)
                    {
                        float Multiplier = Model.Metadata.Index2ClassInfo[c].TrainingCount; //Should be changed to scaler, but for now . . 
                        if (Multiplier == 0) Multiplier = 1;
                        dataPre[c] = dataPre[c] * Multiplier;
                    }
                    Add(y[r], dataPre, annotations[y[r]]);
                }
            }

            public void Add(string RowDef, float[] Results, string Additional = "")
            {
                var DP = new TF_Measure_Result_DataPoint(this) { Name = RowDef, data = Results, Other = Additional };
                Add(DP);
            }

            public void Add(TF_Measure_Result_DataPoint Datapoint)
            {
                if (DataPoints.ContainsKey(Datapoint.Name))
                {
                    //Debugger.Break(); //Why is there more than 1 of these?
                }
                else
                {
                    DataPoints.Add(Datapoint.Name, Datapoint);
                }
            }

            public static char Delim = '\t';

            public void Export(string PathToExport)
            {
                var sB = new StringBuilder();

                void WriteRow(TF_Measure_Result_DataPoint DP, bool Headers)
                {
                    sB.Append(Headers ? "Name" + Delim + "Other" + Delim : DP.Name + Delim + DP.Other + Delim);
                    for (int c = 0; c < Model.Metadata.ClassesCount; c++)
                    {
                        if (Headers)
                            sB.Append(Model.Metadata.Index2ClassInfo[c].Name + Delim);
                        else
                            sB.Append(DP.data[c].ToString() + Delim);
                    }
                    sB.Append("\r\n");
                }

                //Export headers then results
                WriteRow(null, true);
                foreach (var DP in DataPoints.Values)
                {
                    WriteRow(DP, false);
                }

                File.WriteAllText(Path.Combine(PathToExport, "ModelMeasure01.txt"), sB.ToString());
            }
        }

        public class TF_Measure_Result_DataPoint
        {
            public string Name = "";
            public string Other = "";
            public float[] data;

            TF_Measure_Results Parent;

            public TF_Measure_Result_DataPoint(TF_Measure_Results parent)
            {
                Parent = parent;
            }
        }
    }

    namespace ActivateEnviron
    {
        public class TFSimple
        {
            /// <summary>
            /// This triggers an Anaconda environment and runs a Python Command in it (good for batch, but seems computer specific and therefore not ideal)
            /// </summary>
            public void Go()
            {
                //https://stackoverflow.com/questions/49082312/activating-conda-environment-from-c-sharp-code-or-what-is-the-differences-betwe
                // Set working directory and create process
                var workingDirectory = Path.GetFullPath("Scripts");
                var process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "cmd.exe",
                        RedirectStandardInput = true,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        WorkingDirectory = workingDirectory
                    }
                };
                process.Start();
                // Pass multiple commands to cmd.exe
                using (var sw = process.StandardInput)
                {
                    if (sw.BaseStream.CanWrite)
                    {
                        // Vital to activate Anaconda
                        sw.WriteLine("C:\\PathToAnaconda\\anaconda3\\Scripts\\activate.bat");
                        // Activate your environment
                        sw.WriteLine("activate your-environment");
                        // Any other commands you want to run
                        sw.WriteLine("set KERAS_BACKEND=tensorflow");
                        // run your script. You can also pass in arguments
                        sw.WriteLine("python YourScript.py");
                    }
                }

                // read multiple output lines
                while (!process.StandardOutput.EndOfStream)
                {
                    var line = process.StandardOutput.ReadLine();
                    Console.WriteLine(line);
                }
            }
        }
    }

    namespace MLProjects_3DBlocks
    {
        public class BlockSetCollection : IEnumerable<BlockSet>
        {
            private List<BlockSet> _List;

            public int Count => _List.Count;

            public BlockSetCollection()
            {
                _List = new List<BlockSet>();
            }

            public void Add(BlockSet BSet) { _List.Add(BSet); }

            public BlockSet this[int index]
            {
                get { return _List[index]; }
            }

            /// <summary>
            /// Exports the sparse representation of the blocks
            /// </summary>
            public string Export()
            {
                var sB = new StringBuilder();
                bool first = true;
                foreach (var BlockSet in this)
                {
                    sB.Append(BlockSet.Export(first));
                    first = false;
                }
                return sB.ToString();
            }

            /// <summary>
            /// Exports the fully encoded tensor version
            /// </summary>
            public string Tensor()
            {
                int MinEachDimension = (int)Math.Min(Math.Min(this.Min(x => x.X_Min), this.Min(x => x.Y_Min)), this.Min(x => x.Z_Min));
                int MaxEachDimension = (int)Math.Max(Math.Max(this.Max(x => x.X_Max), this.Max(x => x.Y_Max)), this.Max(x => x.Z_Max));

                //MinEachDimension = -4;
                //MaxEachDimension = 4;

                var sB = new StringBuilder();
                bool first = true;
                foreach (var BlockSet in this)
                {
                    sB.Append(BlockSet.Tensor(first, MinEachDimension, MaxEachDimension));
                    first = false;
                }
                return sB.ToString();
            }

            public static void BlockSetGo(int BlockSets, int BlocksPerSet)
            {
                var BSC = new BlockSetCollection();
                for (int i = 0; i < BlockSets; i++)
                {
                    BSC.Add(BlockSet.GenerateInterconnected(BlocksPerSet));
                    BSC[BSC.Count - 1].Index = i;
                    if (i % 50 == 0) System.Diagnostics.Debug.Print(i.ToString());
                }
                string Fldr = @"R:\FIVE\EXP\FIV688\1 Data\";
                string after = "05";
                System.IO.File.WriteAllText(Path.Combine(Fldr, "Blocks3D_" + after + ".txt"), BSC.Export());
                System.IO.File.WriteAllText(Path.Combine(Fldr, "Tensor3D_" + after + ".txt"), BSC.Tensor());
            }

            public IEnumerator<BlockSet> GetEnumerator()
            {
                return _List.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _List.GetEnumerator();
            }
        }

        public class BlockSet : IEnumerable<Block>
        {
            private Dictionary<XYZ, Block> _Dict;

            public List<Side> Sides_Open;
            public List<Side> Sides_Connected;
            public int Count => _Dict.Count;

            public string Name { get; set; }

            public int Index { get; set; }

            public int Leaves => this.Count(x => x.ConnectedSides == 1);

            public int MaxDistToLeaves => this.Max(x => x.DistToLeaf);

            public double X_Min => _Dict.Keys.Min(k => k.X);
            public double Y_Min => _Dict.Keys.Min(k => k.Y);
            public double Z_Min => _Dict.Keys.Min(k => k.Z);
            public double X_Max => _Dict.Keys.Max(k => k.X);
            public double Y_Max => _Dict.Keys.Max(k => k.Y);
            public double Z_Max => _Dict.Keys.Max(k => k.Z);

            private static Random Rnd = new Random();

            public static BlockSet GenerateInterconnected(int TotalBlocks)
            {
                var BSet = new BlockSet(1);
                for (int i = 0; i < TotalBlocks - 1; i++)
                {
                    var AddFromSide = BSet.Sides_Open[Rnd.Next(0, BSet.Sides_Open.Count)];
                    BSet.AddNeighbor(AddFromSide);
                }

                // - - - - -  Add Distance to Leaf
                Calculate_DistanceToLeaf(BSet);
                return BSet;
            }

            private static void Calculate_DistanceToLeaf(BlockSet BSet)
            {
                int NeedsDistToLeaf = 0;
                //First add the leaves
                foreach (var block in BSet) if (block.ConnectedSides == 1) block.DistToLeaf = 0; else { block.DistToLeaf = int.MaxValue; NeedsDistToLeaf++; }
                //Sometimes, there are NO leaves
                if (NeedsDistToLeaf == BSet.Count)
                {
                    foreach (var block in BSet) block.DistToLeaf = 2;
                    return;
                }

                while (NeedsDistToLeaf > 0)
                {
                    SetLeafDist(BSet, true);
                }
                SetLeafDist(BSet, false); SetLeafDist(BSet, false);
                SetLeafDist(BSet, false); //Do this thrice to clean things up

                void SetLeafDist(BlockSet BSet, bool OnlyUnSet)
                {
                    foreach (var block in OnlyUnSet ? BSet.Where(x => x.DistToLeaf > BSet.Count) : BSet.Where(x => x.DistToLeaf > 0))
                    {
                        int dMin = block.Neighbors.Values.Min(x => x.DistToLeaf);
                        if (dMin == int.MaxValue) continue;
                        block.DistToLeaf = dMin + 1;
                        NeedsDistToLeaf--;
                    }
                }
            }

            public BlockSet(int CubeStartSize = 1)
            {
                _Dict = new Dictionary<XYZ, Block>();
                Sides_Open = new List<Side>();
                Sides_Connected = new List<Side>();

                if (CubeStartSize > 1)
                {
                    System.Diagnostics.Debugger.Break(); //NOT IMPLEMENTED
                }

                int start = (int)Math.Round((double)CubeStartSize / 2, 0);
                for (int i = start; i < start + CubeStartSize; i++)
                    Add(new Block(this, i, i, i));
            }

            public void Add(Block block)
            {
                _Dict.Add(block.XYZ, block);
                foreach (var Side_Connect in block.Sides_Connected)
                {
                    if (Sides_Connected.Contains(Side_Connect.Key)) continue;
                    if (Side_Connect.Value)
                    {
                        Sides_Open.Remove(Side_Connect.Key);
                        Sides_Connected.Add(Side_Connect.Key);
                    }
                    else
                    {
                        Sides_Open.Add(Side_Connect.Key);
                        Sides_Connected.Remove(Side_Connect.Key);
                    }
                }
                Recheck_Sides();
            }

            public Block AddNeighbor(Side AddBlockToThisExistingSide)
            {
                var bl = new Block(AddBlockToThisExistingSide);
                Add(bl);

                return bl;
            }

            public void Recheck_Sides()
            {
                //The issue is that sometimes you add a neighbor and you realize one of the sides, but other sides have to be connected as well
                foreach (var block in this)
                {
                    foreach (var Side in block.Sides.Values)
                    {
                        if (!block.Sides_Connected[Side])
                        {
                            if (_Dict.ContainsKey(Side.NeighborXYZ))
                            {
                                block.AddNeighbor(Side.SType, _Dict[Side.NeighborXYZ]);
                            }
                        }
                    }
                }
            }

            public string Export(bool ExportHeader = false)
            {
                var sB = new StringBuilder();
                bool First = ExportHeader;
                foreach (var block in this)
                {
                    if (First)
                    {
                        sB.Append("BSetIdx" + Block.d + "Name" + Block.d + "Blocks" + Block.d + "Leaves" + Block.d + "MaxDist2Leaves" + Block.d);
                        sB.Append(block.Export(true) + "\r\n");
                        First = false;
                    }
                    sB.Append(Index.ToString() + Block.d + Name + Block.d + Count + Block.d + Leaves + Block.d + MaxDistToLeaves + Block.d);
                    sB.Append(block.Export() + "\r\n");
                }
                return sB.ToString();
            }

            public string Tensor(bool ExportHeader = false, int minEach = int.MaxValue, int maxEach = int.MinValue)
            {
                var sB = new StringBuilder();

                //Now we re-encode this into the full set
                for (int i = ExportHeader ? 0 : 1; i < 2; i++)
                {
                    if (i == 0)
                        sB.Append("BSetIdx" + Block.d + "Name" + Block.d + "Blocks" + Block.d + "Leaves" + Block.d + "MaxDist2Leaves" + Block.d);
                    else
                        sB.Append(Index.ToString() + Block.d + Name + Block.d + Count + Block.d + Leaves + Block.d + MaxDistToLeaves + Block.d);
                    for (int x = (minEach == int.MaxValue ? (int)this.X_Min : minEach); x <= (maxEach == int.MinValue ? (int)this.X_Max : maxEach); x++)
                    {
                        for (int y = (minEach == int.MaxValue ? (int)this.Y_Min : minEach); y <= (maxEach == int.MinValue ? (int)this.Y_Max : maxEach); y++)
                        {
                            for (int z = (minEach == int.MaxValue ? (int)this.Z_Min : minEach); z <= (maxEach == int.MinValue ? (int)this.Z_Max : maxEach); z++)
                            {
                                var Key = new XYZ(x, y, z);
                                int Val = _Dict.ContainsKey(Key) ? 1 : 0;

                                if (i == 0)
                                    sB.Append(Key.ToString() + Block.d); //headers
                                else
                                    sB.Append(Val.ToString() + Block.d); //values
                            }
                        }
                    }
                    sB.Append("\r\n");
                }
                return sB.ToString();
            }

            public IEnumerator<Block> GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

        }

        public class Block
        {
            public override string ToString()
            {
                return XYZ + "  n=" + Neighbors.Count;
            }

            private void Init()
            {
                XYZ = new XYZ(double.NaN, double.NaN, double.NaN);
                Sides = new Dictionary<BlockSideType, Side>();
                Neighbors = new Dictionary<BlockSideType, Block>();
                Sides_Connected = new Dictionary<Side, bool>();
                Parent = null;
                Index = -1;
            }

            private void SetupSides()
            {
                for (int i = 0; i < Side.BlockSideCount; i++)
                {
                    var tS = new Side(this, (BlockSideType)i);
                    Sides.Add(tS.SType, tS);
                    Sides_Connected.Add(tS, false);
                }
            }

            public Block()
            {
                Init();
                SetupSides();
            }

            public Block(BlockSet Parent, double x, double y, double z)
            {
                Init();
                XYZ = new XYZ(x, y, z);
                this.Parent = Parent;
                this.Index = Parent.Count;
                SetupSides();
            }

            public Block(Side Neighborside)
            {
                Init();
                this.Parent = Neighborside.Parent.Parent;
                this.Index = Parent.Count;
                XYZ = Neighborside.NeighborXYZ;
                SetupSides();

                AddNeighbor(Side.Cognate[Neighborside.SType], Neighborside.Parent); //Add neighbor to me
            }

            public void AddNeighbor(BlockSideType SType, Block NewNeighbor)
            {
                //Add it to the list
                Neighbors.Add(SType, NewNeighbor);

                //Signal my side as connected
                var sd = Sides[SType];
                Sides_Connected[sd] = true;
                Parent.Sides_Open.Remove(sd);
                Parent.Sides_Connected.Add(sd);

                if (!NewNeighbor.Neighbors.ContainsValue(this))
                    NewNeighbor.AddNeighbor(Side.Cognate[SType], this);
            }

            public const char d = '\t';

            public string Export(bool Headers = false)
            {
                bool H = Headers;
                return (H ? "Index" : Index.ToString()) + d + (H ? "X" : XYZ.X.ToString()) + d + (H ? "Y" : XYZ.Y.ToString()) + d + (H ? "Z" : XYZ.Z.ToString()) + d + (H ? "Sides" : ConnectedSides) + d + (H ? "DistToLeaf" : DistToLeaf) + d + (H ? "Attrib" : Attrib);
            }

            public XYZ XYZ;

            public int Index;

            public string Attrib = "";

            public int ConnectedSides => Sides_Connected.Values.Where(x => x == true).Count();

            public int DistToLeaf { get; set; }

            public Dictionary<BlockSideType, Side> Sides { get; set; }

            public Dictionary<Side, bool> Sides_Connected { get; set; }

            public Dictionary<BlockSideType, Block> Neighbors { get; set; }

            public BlockSet Parent { get; set; }
        }

        public class Side
        {
            public override string ToString()
            {
                return SType.ToString();
            }

            private XYZ _NeighborXYZ = new XYZ(double.NaN, 0, 0);

            /// <summary>
            /// Gets the coordinates that a neighbor would have on this side
            /// </summary>
            public XYZ NeighborXYZ
            {
                get
                {
                    if (double.IsNaN(_NeighborXYZ.X))
                    {
                        //Take the parents coordinates, and add 1 or subtract based on which side
                        double x = Parent.XYZ.X; double y = Parent.XYZ.Y; double z = Parent.XYZ.Z;
                        switch (SType)
                        {
                            case BlockSideType.u:
                                z--;
                                break;
                            case BlockSideType.d:
                                z++;
                                break;
                            case BlockSideType.l:
                                x--;
                                break;
                            case BlockSideType.r:
                                x++;
                                break;
                            case BlockSideType.f:
                                y++;
                                break;
                            case BlockSideType.b:
                                y--;
                                break;
                            default:
                                break;
                        }
                        _NeighborXYZ = new XYZ(x, y, z);
                    }
                    return _NeighborXYZ;
                }
            }

            public Block Parent;

            public BlockSideType SType;

            public Side(Block ParentBlock, BlockSideType type)
            {
                Parent = ParentBlock;
                SType = type;
            }

            public const int BlockSideCount = 6;

            public static Dictionary<BlockSideType, BlockSideType> Cognate =
                new Dictionary<BlockSideType, BlockSideType>() {
                    { BlockSideType.u, BlockSideType.d }, { BlockSideType.d, BlockSideType.u },
                    { BlockSideType.l, BlockSideType.r }, {BlockSideType.r, BlockSideType.l},
                    { BlockSideType.b, BlockSideType.f }, {BlockSideType.f, BlockSideType.b}
                };
        }

        public struct XYZ
        {
            public double X;
            public double Y;
            public double Z;

            public XYZ(double x, double y, double z)
            {
                X = x; Y = y; Z = z;
            }

            public override string ToString()
            {
                return X + "," + Y + "," + Z;
            }
        }

        public enum BlockSideType
        {
            u = 0,
            d = 1,
            l = 2,
            r = 3,
            f = 4,
            b = 5
        }
    }
}
