﻿using FIVE.InCellLibrary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FIVE_IMG
{



    public class ImageQueue : IEnumerable<Bitmap>
    {
        private Dictionary<Tuple<string, string>, Bitmap> _Dict;

        private Queue<Tuple<string, string>> _Queue;

        private int _Capacity;
        public int Capacity
        {
            get => _Capacity;
            set
            {
                _Capacity = value;
            }
        }

        public ImageQueue(int Capacity)
        {
            Init(Capacity);
        }

        private int Count { get => _Dict.Count; }

        /// <summary>
        /// Name_From_XDCE(Img), wvDisplayParams
        /// </summary>
        public Dictionary<Tuple<string, string>, Bitmap> Dict { get => _Dict; }

        private void Init(int Capacity)
        {
            _Capacity = Capacity;
            _Dict = new Dictionary<Tuple<string, string>, Bitmap>();
            _Queue = new Queue<Tuple<string, string>>();
        }

        /// <summary>
        /// Adds a new entry, and removes and older one
        /// </summary>
        public void Add(string Name, Bitmap Image, wvDisplayParams Params)
        {
            if (_Queue.Count >= Capacity)
            {
                Tuple<string, string> DeQueue = _Queue.Dequeue();
                if (DeQueue == null) return;
                if (!_Dict.ContainsKey(DeQueue)) return;
                Bitmap ToDispose = _Dict[DeQueue];
                _Dict.Remove(DeQueue);
                ToDispose.Dispose();

            }
            Tuple<string, string> Key = new Tuple<string, string>(Name, Params.ToString());
            
            //Because we have some parallelization going on, double check this . . 
            if (_Dict.ContainsKey(Key)) return;
            
            _Queue.Enqueue(Key);
            _Dict.Add(Key, Image);
        }

        public void Add(XDCE_Image Img, System.Drawing.Bitmap BMP, wvDisplayParams Params)
        {
            Add(Name_From_XDCE(Img), BMP, Params);
        }

        public void Remove(XDCE_Image Img, wvDisplayParams Params)
        {
            //See the dequeue above for how to do this . . 
        }

        //public void Add(string Name, System.Drawing.Bitmap BMP, wvDisplayParams Params)
        //{
        //    ImgWrap imWrap = new ImgWrap(Name, BMP);
        //    Add(imWrap, Params);
        //}

        public string Name_From_XDCE(XDCE_Image Img)
        {
            return Img.FileName;
        }

        public bool ContainsKey(XDCE_Image Img, wvDisplayParams Params)
        {
            return ContainsKey(Name_From_XDCE(Img), Params);
        }

        public bool ContainsKey(string Name, wvDisplayParams Params)
        {
            Tuple<string, string> Key = new Tuple<string, string>(Name, Params.ToString());
            return _Dict.ContainsKey(Key);
        }

        //public ImgWrap Get_ImWrap(string Name, wvDisplayParams Params)
        //{
        //    Tuple<string, string> Key = new Tuple<string, string>(Name, Params.ToString());
        //    return _Dict[Key];
        //}

        public System.Drawing.Bitmap Get_Bitmap(string Name, wvDisplayParams Params)
        {
            Tuple<string, string> Key = new Tuple<string, string>(Name, Params.ToString());
            return _Dict[Key];
        }

        public System.Drawing.Bitmap Get_Bitmap(XDCE_Image Img, wvDisplayParams Params)
        {
            return Get_Bitmap(Name_From_XDCE(Img), Params);
        }

        public IEnumerator<Bitmap> GetEnumerator()
        {
            return _Dict.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Dict.Values.GetEnumerator();
        }
    }

}
