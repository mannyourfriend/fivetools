﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using FIVE.InCellLibrary;

namespace FIVE_IMG
{
    public class INCELL_Folder_Reconstruct
    {
        public INCELL_Folder Folder;
        public INCELL_MeasuredOffsets MeasuredOffsets => Folder.XDCE.MeasuredOffsets;
        public float Magnification_New;
        public float Magnification_Original;
        public float FractionFilledToInclude;
        public string AppendToName_PipeIsNewMag;
        //public bool CenterType_TrueQuadFalseSingle; //Probably not needed

        private Font oFont = new Font("Arial", 20);
        private Font tFont = new Font("Arial", 40);
        private Pen oPen = new Pen(Brushes.Magenta, 6);
        private Pen nPen = new Pen(Color.FromArgb(140, Color.Green), 3);
        private Brush nBrush = new SolidBrush(Color.FromArgb(40, Color.Green));

        public static void PerformReconstruction(INCELL_Folder folderToPass,INCELL_MeasuredOffsets measuredOffsets,float newMagnification, string associatedTabularFile, float fractionFilledToInclude, string appendToName_PipeIsNewMag)
        {
            newMagnification = 13; fractionFilledToInclude = 0.9F;
            var Recon = new INCELL_Folder_Reconstruct(folderToPass, measuredOffsets, newMagnification, fractionFilledToInclude, appendToName_PipeIsNewMag);
            Recon.Reconstruct_Scan();
            Recon.Reconstruct_AssociatedTabularFile(associatedTabularFile);
        }

        public INCELL_Folder_Reconstruct(INCELL_Folder folderToPass, INCELL_MeasuredOffsets measuredOffsets, float newMagnification, float fractionFilledToInclude, string appendToName_PipeIsNewMag)
        {
            Folder = folderToPass;
            Folder.XDCE.MeasuredOffsets = measuredOffsets; //Important, this needs to be done BEFORE the plateX and plateY are calculated . . perhaps settings this can cause a re-calulation?

            Magnification_New = newMagnification;
            Magnification_Original = 20; //TODO FIX
            FractionFilledToInclude = fractionFilledToInclude;
            //CenterType_TrueQuadFalseSingle = centerType_TrueQuadFalseSingle;
            AppendToName_PipeIsNewMag = appendToName_PipeIsNewMag;
        }

        public void Reconstruct_Scan()
        {
            foreach (var well in Folder.XDCE.Wells.Values)
            {
                Reconstruct_Well(well);
            }
        }

        public void Reconstruct_Well(XDCE_ImageGroup Well)
        {
            int FinalSizePerTile = 120;
            double Ratio = (double)FinalSizePerTile / Well.Images[0].Height_microns;
            double MinX = Well.X_to_Index.Keys.First(), MaxX = Well.X_to_Index.Keys.Last() + Well.Images[0].Width_microns;
            double MinY = Well.Y_to_Index.Keys.First(), MaxY = Well.Y_to_Index.Keys.Last() + Well.Images[0].Height_microns;

            var bmp = new Bitmap(1 + (int)((MaxX - MinX) * Ratio), 1 + (int)((MaxY - MinY) * Ratio));

            using (var g = Graphics.FromImage(bmp))
            {
                for (int FOV = Well.FOV_Min; FOV < Well.FOV_Max; FOV++)
                {
                    var xISet = Well.GetFields(FOV);
                    int placeX = (int)((xISet[0].PlateX_um - MinX) * Ratio);
                    int placeY = (int)((xISet[0].PlateY_um - MinY) * Ratio);

                    g.DrawRectangle(oPen, placeX, placeY, FinalSizePerTile, FinalSizePerTile);
                    g.DrawString(FOV.ToString(), oFont, Brushes.Magenta, placeX + 4, placeY + 4);
                }
            }

            // New FOVs ---------------------------------
            var newFOVs = GenerateNewFOVPattern(Well, Well.FOV_Max - Well.FOV_Min, Well.Images[0].Width_microns, Well.Images[0].Height_microns, MinX, MaxX, MinY, MaxY);
            int NewSizePerTile = (int)(Magnification_Original * FinalSizePerTile / Magnification_New);

            using (var g = Graphics.FromImage(bmp))
            {
                for (int i = 0; i < newFOVs.Count; i++)
                {
                    var newFOV = newFOVs[i];

                    int placeX = (int)((newFOV.X - MinX) * Ratio);
                    int placeY = (int)((newFOV.Y - MinY) * Ratio);

                    g.FillRectangle(nBrush, placeX, placeY, NewSizePerTile, NewSizePerTile);
                    g.DrawRectangle(nPen, placeX, placeY, NewSizePerTile, NewSizePerTile);
                    g.DrawString(i.ToString(), tFont, Brushes.Green, (float)(placeX + NewSizePerTile / 2.7F), (float)(placeY + NewSizePerTile / 2.7F));
                }
            }

            bmp.Save(@"c:\temp\ReconPlot_" + Magnification_Original.ToString("000x_") + Magnification_New.ToString("000x_F=") + FractionFilledToInclude.ToString("0.00") + ".jpg");
        }

        public List<RectangleF> GenerateNewFOVPattern(XDCE_ImageGroup Well, int originalFOVCount, double width_microns, double height_microns, double minX, double maxX, double minY, double maxY)
        {
            float newWidth_microns = (float)width_microns * Magnification_Original / Magnification_New;
            float newHeight_microns = (float)height_microns * Magnification_Original / Magnification_New;

            int numRows = (int)Math.Ceiling(Magnification_New * Well.Y_to_Index.Count / Magnification_Original);
            int numCols = (int)Math.Ceiling(Magnification_New * Well.X_to_Index.Count / Magnification_Original);

            // Calculate offsets for centering
            float offsetX = (float)((maxX - minX) - numCols * newWidth_microns) / 2;
            float offsetY = (float)((maxY - minY) - numRows * newHeight_microns) / 2;

            var newFOVs = new List<RectangleF>();
            var origRects = Well.Images.Where(i => i.wavelength == 0).Select(f => new RectangleF((float)f.PlateX_um, (float)f.PlateY_um, (float)f.Width_microns, (float)f.Height_microns));

            for (int row = 0; row < numRows; row++)
            {
                bool isEvenRow = row % 2 == 0;
                for (int col = 0; col < numCols; col++)
                {
                    float x = (float)minX + offsetX + (isEvenRow ? col : (numCols - col - 1)) * newWidth_microns;
                    float y = (float)minY + offsetY + row * newHeight_microns;

                    //See whether to include this or not
                    var newRec = new RectangleF(x, y, newWidth_microns, newHeight_microns);
                    double AreaFilled = FOVAreaFilled(newRec, origRects);
                    if (AreaFilled > this.FractionFilledToInclude) newFOVs.Add(newRec);
                }
            }

            return newFOVs;
        }

        public double FOVAreaFilled(RectangleF newFOV, IEnumerable<RectangleF> originalFOVs)
        {
            double filledArea = 0;
            foreach (RectangleF originalFOV in originalFOVs)
            {
                var intersection = RectangleF.Intersect(newFOV, originalFOV);
                if (!intersection.IsEmpty)
                    filledArea += intersection.Width * intersection.Height;
            }
            return filledArea / (newFOV.Width * newFOV.Height);
        }

        public void Reconstruct_AssociatedTabularFile(string FilePath)
        {

        }
    }

    public class INCELL_AlignToLowerMag
    {
        public static void MannyTest01(INCELL_Folder fHigh, INCELL_Folder fLow)
        {
            var xILow = fLow.XDCE.Images.First();
            Tuple<XDCE_Image, List<XDCE_Image>>[] overlapArray = new Tuple<XDCE_Image, List<XDCE_Image>>[fLow.XDCE.Images.Count];
            //var overLapArray = fLow.XDCE.Images;
            //Find 20X fields that would sit under  or intersect with this field
            /* Todo:
            1. Ensure we increment through only 1 img type (?)
            2. account for position correction
            3. account for scale
            */
            // simple counter
            var i =0;
            foreach (XDCE_Image lowRes in fLow.XDCE.Images)
            {
                // Get field of lowRes img
                var loXLeftPoint = 1;
                var loXRightPoint = loXLeftPoint + lowRes.Width_microns;
                var loXTopPoint = 3;
                var loXBotPoint = loXTopPoint + lowRes.Height_microns;
                List<XDCE_Image> subIMGs = new List<XDCE_Image>();


                foreach (XDCE_Image hiRes in fHigh.XDCE.Images)
                {

                    // get field of hiRes img
                    var hiXLeftPoint = 1;
                    var hiXRightPoint = hiXLeftPoint + hiRes.Width_microns;
                    var hiXTopPoint = 3;
                    var hiXBotPoint = hiXTopPoint + hiRes.Height_microns;
                    if (hiXLeftPoint < loXRightPoint ||hiXRightPoint < loXLeftPoint) {
                        // they overlap horizontally
                        // Add small region/point of overlap for cross corellation?
                        subIMGs.Add(hiRes);
                    }
                    else if (hiXTopPoint < loXBotPoint || hiXBotPoint < loXTopPoint) {
                        subIMGs.Add(hiRes);
                    }

                }
                overlapArray[i] = new Tuple<XDCE_Image, List<XDCE_Image>>(lowRes, subIMGs);
                i++;

            }
        }
    }
}
