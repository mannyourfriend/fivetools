﻿using FIV_IMG_CLib;
using FIVE.InCellLibrary;
using FIVE.Masks;
using FIVE.TF;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace FIVE_IMG.Seg
{
    public static class Segmentation
    {

        public static SegResults RunSegmentation(bool UseRafts, DateTime Start, ConcurrentDictionary<string, XDCE_ImageGroup> ImageParts, CropImageSettings CropSettings, INCELL_WV_Notes wvNotes, System.ComponentModel.BackgroundWorker BW = null)
        {
            //We could use the bool return to break out of the loop early if needed
            bool WithinLoop(SegResults Res, KeyValuePair<string, XDCE_ImageGroup> KVP)
            {
                // Use this if you want to skip wells, but I would rather just export all
                //if (CropSettings != null) if (!CropSettings.WellDict.ContainsKey(KVP.Value.Images.First().WellLabel)) continue;
                Segmentation_Parts(KVP, Res, wvNotes, UseRafts, CropSettings);
                Res.ProcessedCountIncrement();

                if (Res.ProcessedCount % (UseRafts ? 35 : 5) == 0)
                    if (BW != null) BW.ReportProgress(0, Res.ProcessedPercent + " " +
                          ((DateTime.Now - Start).TotalMilliseconds / Res.ProcessedCount).ToString("00") + " msec / image. " + KVP.Key);
                //if (Bag.Count > MaxToProcess) break;
                return true;
            }

            var ResA = new FIVE_IMG.Seg.SegResults(ImageParts.Count()); //This will keep the Object-and per-Image results straight
            CropSettings.ParallelizeSegmentation = false;
            //CropSettings.SaveFolder = @"E:\Temp\RaftExport\FIV574C";
            if (CropSettings.ParallelizeSegmentation)
            {
                Parallel.ForEach(ImageParts, (KVP,state) =>  //This is also untested - probably will break with rafts because of not having them in the same image
                {   //7/22 This is breaking since we need to have each thread on different BMPs
                    WithinLoop(ResA, KVP);
                    if (BW != null) if (BW.CancellationPending) state.Break();
                });
            }
            else
            {
                foreach (var KVP in ImageParts)
                {
                    //if (KVP.Key == "A - 1.1") //So you start with the one that you can see
                    WithinLoop(ResA, KVP);
                    if (BW != null) if (BW.CancellationPending) break;
                }
            }
            return ResA;
        }

        private static void Segmentation_Parts(KeyValuePair<string, XDCE_ImageGroup> RaftOrWell, SegResults Results, INCELL_WV_Notes wvNotes, bool Rafts = false, CropImageSettings Settings = null)
        {
            XDCE_ImageGroup Imgs = RaftOrWell.Value; bool First = true;
            foreach (var wvParam in wvNotes.DisplayParams.Tracings)
            {
                INCELL_WV_Note Nt = wvNotes.List[wvParam.wvIdx];
                Segmentation_Part(Results, Imgs.Images[wvParam.wvIdx], wvParam.Brightness, wvNotes,
                    (int)Nt.Threshold_Value, Nt.QuickName, 2, true, (Rafts ? RaftOrWell.Key : ""), First ? Settings : null);
                First = false; //Only need to export the cropped images once
            }
        }

        public static Random Rand = new Random();

        private static void Segmentation_Part(SegResults Results, XDCE_Image xI, float brightness, INCELL_WV_Notes wvNotes, int Threshold, string ChannelName, int ThreshBinning = 2, bool CountObjects = true, string RaftID = "", CropImageSettings Settings = null)
        {
            Bitmap bmap_thresh = Utilities.DrawingUtils.GetAdjustedImageSt(xI.Parent.FolderPath, xI, brightness);
            Bitmap bmap_export = Utilities.DrawingUtils.CombinedBMAPst(xI, wvNotes.DisplayParams, null, Settings.RandomizeImageIntensity_Min);
            //Bitmap bmap2 = wvNotes.DisplayParams.Actives.Count == 1 & wvNotes.DisplayParams.Actives[0].wvIdx == xI.wavelength ?
            //    bmap : Utilities.DrawingUtils.CombinedBMAPst(xI, wvNotes.DisplayParams, null, Settings.RandomizeImageIntensity_Min);
            bool UseRafts = RaftID != "";
            UseRafts = false; //20211122 - need to fix
            if (UseRafts)
            {
                FIVE.FOVtoRaftID.ReturnRaft RR = xI.Parent.Wells[xI.WellLabel].CalibrationRaftSettings.FindRaftID_RR(xI, RaftID);
                RectangleF r = FIVE.FOVtoRaftID.ReturnRaft.RectFromCorners(xI, RR, bmap_thresh.Size);
                if (r.Width < 1 || r.Height < 1 || r.Top < 1 || r.Left < 1) return;
                bmap_thresh = bmap_thresh.Clone(r, bmap_thresh.PixelFormat);
            }

            var Thresh = new ThreshImage(bmap_thresh, Threshold, CountObjects, ThreshBinning);
            if (CountObjects)
            {
                var Objs = SegObjects.ObjectsFromImage(Thresh);
                foreach (var obj in Objs)
                {
                    obj.CropSavePath = Settings.GetSavePath(xI, obj); //Could make this a property, but for now it is saved manually - must be BEFORE AppendObject or won't get saved
                    if (Settings.StoreResults) Results.AppendObject(xI, RaftID, ChannelName, obj);
                    if (obj.Area_Pixels > Settings.MinAreaPixels && obj.Area_Pixels < Settings.MaxAreaPixels)
                    {
                        //Export each of these images around the objects - - account for binning
                        SaveorExecOnSingleCell(Settings, bmap_export, obj, ThreshBinning);
                    }
                }
            }
            else
            {
                //No longer implemented as of 7/26/2021  //Prefix(); //sB.Append(Header ? "ThreshPixels" : Thresh.Pixels.ToString()); sB.Append(delim); //sB.Append("\r\n");
            }
            bmap_thresh.Dispose();
            bmap_export.Dispose();
        }

        private static void SaveorExecOnSingleCell(CropImageSettings Settings, Bitmap WholeField, SegObject obj, float ThreshBinning)
        {
            bool ShowTestPoint = false;
            Bitmap crop1, final; RectangleF cropRect; PointF posInFull;
            Settings.GetRectangle(obj, WholeField.Size, ThreshBinning, out cropRect, out posInFull);
            try
            {
                if (ShowTestPoint)
                {
                    var STP = new Bitmap(WholeField);
                    using (var g = Graphics.FromImage(STP))
                    {
                        g.DrawRectangle(Pens.Yellow, Rectangle.Round(cropRect));
                    }
                    STP.Save(obj.CropSavePath + "T.jpg");
                }
                //Chop out the region the cell is in
                crop1 = WholeField.Clone(cropRect, PixelFormat.Format24bppRgb);
                //Add blank area around the cell to make it the correct final dimensions
                final = new Bitmap(Settings.CropSize.Width, Settings.CropSize.Height);
                using (var g = Graphics.FromImage(final))
                {
                    g.Clear(Color.Black); //Could make this the avg Bacgkround color
                    g.DrawImage(crop1, posInFull);
                }
                if (Settings.RunMLProcessing == "PL")
                {
                    obj.PLResults = PL.PLDeployModule.PredictOnImage(final, Settings.MLSettings).numericRes;
                }
                else
                {
                    final.Save(obj.CropSavePath);
                }
                crop1.Dispose();
                final.Dispose();
            }
            catch { }
        }
    }

    public static class DegenerationIndex
    {
        //This is built into segmentation, but isn't quite the same

        //run("Subtract Background...", "rolling="+ 10 + " light"); rolling = 10
        //		getStatistics(area, mean);
        //		setThreshold(0, threshold* mean); 0.9
        //convert to mask
        //run("Analyze Particles...", "size=0-" + FragmentCutoffA + " circularity=0-1.00 show=Nothing clear");

        //Analyze particles just moves across the image, looking for thresholded pixels
        //then it walks like a turtle around the object until it comes back to the beginning
    }

#if (TRUE) //Classes vs. Structs for Linked Pix
    //As of 11/28, Classes are faster on Willie's computer, but there could be a trick with references that make the other form faster

    public class LinkedPix
    {
        public Point P;

        public override string ToString()
        {
            return P.ToString() + " " + Links.Count;
        }

        public HashSet<LinkedPix> Links;
        public SegObject MemberOfObject { get; set; }
        public bool IsMemberOfObject { get => MemberOfObject != null; }

        private void Init()
        {
            MemberOfObject = null;
            Links = new HashSet<LinkedPix>();
        }

        public LinkedPix(int x, int y)
        {
            Init();
            P = new Point(x, y);
        }

        public LinkedPix(Point p)
        {
            Init();
            P = p;
        }

        public LinkedPix RotateCopy(double theta_radians)
        {
            //Assumes that 0,0 is the origin
            LinkedPix LP2 = new LinkedPix(new Point(
                (int)(P.X * Math.Cos(theta_radians) - P.Y * Math.Sin(theta_radians)),
                (int)(P.Y * Math.Cos(theta_radians) + P.X * Math.Sin(theta_radians))));
            return LP2;
        }

        public void LinksRemove(LinkedPix ToRemove, bool TwoWay = false)
        {
            //Remove from each others
            if (TwoWay) ToRemove.Links.Remove(this);
            Links.Remove(ToRemove);
        }

        /// <summary>
        /// Removes this from each of its links and removes these links
        /// </summary>
        internal void LinksCrossRemove()
        {
            foreach (LinkedPix tPix in Links) tPix.LinksRemove(this, false);
            Links = new HashSet<LinkedPix>();
        }
    }

#else


    public struct LinkedPix
    {
        public Point P;

        public override string ToString()
        {
            return P.ToString() + " " + Links.Count;
        }

        public HashSet<LinkedPix> Links;
        public SegObject MemberOfObject { get; set; }
        public bool IsMemberOfObject { get => MemberOfObject != null; }

        private void Init()
        {
            MemberOfObject = null;
            Links = new HashSet<LinkedPix>();
            P = new Point();
        }

        public LinkedPix(bool T)
        {
            MemberOfObject = null;
            Links = new HashSet<LinkedPix>();
            P = new Point();
        }

        //public LinkedPix(int x, int y)
        //{
        //    Init();
        //    P = new Point(x, y);
        //}

        public LinkedPix(Point p)
        {
            //Init();
            MemberOfObject = null;
            Links = new HashSet<LinkedPix>();
            P = p;
        }

        public LinkedPix RotateCopy(double theta_radians)
        {
            //Assumes that 0,0 is the origin
            var LP2 = new LinkedPix(
            //LP2.P =
                new Point(
                (int)(P.X * Math.Cos(theta_radians) - P.Y * Math.Sin(theta_radians)),
                (int)(P.Y * Math.Cos(theta_radians) + P.X * Math.Sin(theta_radians))));
            return LP2;
        }

        public void LinksRemove(LinkedPix ToRemove, bool TwoWay = false)
        {
            //Remove from each others
            if (TwoWay) ToRemove.Links.Remove(this);
            Links.Remove(ToRemove);
        }

        /// <summary>
        /// Removes this from each of its links and removes these links
        /// </summary>
        internal void LinksCrossRemove()
        {
            foreach (LinkedPix tPix in Links) tPix.LinksRemove(this, false);
            Links = new HashSet<LinkedPix>();
        }
    }


#endif

    public class LinkedPixSet : IEnumerable<LinkedPix>
    {
        private HashSet<LinkedPix> _Set;

        public LinkedPixSet()
        {
            _Set = new HashSet<LinkedPix>();
        }
        public void Add(LinkedPix Pix)
        {
            _Set.Add(Pix);
        }

        public void Remove(LinkedPix Pix)
        {
            _Set.Remove(Pix);
        }

        public IEnumerator<LinkedPix> GetEnumerator()
        {
            return _Set.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Set.GetEnumerator();
        }

        public int Count { get => _Set.Count; }
    }

    public class ThreshImage : LinkedPixSet
    {
        Dictionary<Point, LinkedPix> PointDict;
        public static double DefaultThreshold_AboveMean = 4;
        public static int Slice_ModulusFactor = 12;
        public float BinningUsed = 1;

        public int Pixels { get; set; }

        public Size Size;

        public Bitmap GetPostThreshBitmap(bool Invert = false, int Expand = 0)
        {
            var C_Thresh = Invert ? Color.Black : Color.White;
            var C_Bground = Invert ? Color.White : Color.Black;
            var B = new SolidBrush(C_Thresh);
            var tB = new Bitmap(Size.Width, Size.Height);
            using (Graphics g = Graphics.FromImage(tB))
            {
                g.Clear(C_Bground);
                if (Expand > 0)
                {
                    int ExpandSize = 1 + Expand * 2;
                    foreach (KeyValuePair<Point, LinkedPix> pt in PointDict)
                        g.FillEllipse(B, new Rectangle(pt.Key.X - Expand, pt.Key.Y - Expand, ExpandSize, ExpandSize));
                }
            }
            if (Expand == 0)
            {
                foreach (KeyValuePair<Point, LinkedPix> pt in PointDict)
                    tB.SetPixel(pt.Key.X, pt.Key.Y, C_Thresh);
            }
            return tB;
            //Set points ~70 msec, draw rectangle 86 ms
        }

        private Bitmap _PostThreshBitMap;
        public Bitmap PostThreshBitmap
        {
            get
            {
                if (_PostThreshBitMap == null)
                {
                    _PostThreshBitMap = GetPostThreshBitmap();
                }
                return _PostThreshBitMap;
            }
        }

        //Keeps the LinkedPix that belong to the thresholded part of the image
        private void Init()
        {
            PointDict = new Dictionary<Point, LinkedPix>();
        }

        public ThreshImage()
        {
            Init();
        }

        public ThreshImage(Bitmap bmp, int threshold = -1, bool CreateLinks = true, float BinBitMap = 4, bool Slice = false)
        {
            if (BinBitMap > 1)
            {
                Size tSize = new Size((int)(bmp.Width / BinBitMap), (int)(bmp.Height / BinBitMap));
                bmp = new Bitmap(bmp, tSize);
            }
            BinningUsed = BinBitMap;
            CreateFrom(bmp, threshold, CreateLinks, Slice);
        }

        public ThreshImage(XDCE_Image Img, float brightness, int Threshold, float BinBitMap, bool CreateLinks = true)
        {
            Bitmap bmp = FIVE_IMG.Utilities.DrawingUtils.GetAdjustedImageSt(Img.Parent.FolderPath, Img, brightness); //217 208 225 ms
            Size tSize = new Size((int)(bmp.Width / BinBitMap), (int)(bmp.Height / BinBitMap));
            bmp = new Bitmap(bmp, tSize); //25 27 28 ms
            BinningUsed = BinBitMap;
            CreateFrom(bmp, Threshold, CreateLinks);
        }

        private void CreateFrom(Bitmap BMP, int threshold = -1, bool CreateLinks = true, bool Slice = false)
        {
            Init();
            bool AutoThreshold = (threshold == -1);
            if (BMP == null) return;
            Size = BMP.Size; Pixels = 0;
            ImgWrap imWrap = new ImgWrap(BMP);
            double mean_multiplier = 1;
        TryAgainNewThreshold:
            if (AutoThreshold)
            {
                double mean = imWrap.RedMean; //79 143 69 ms
                mean_multiplier = DefaultThreshold_AboveMean;
                threshold = (int)(mean * mean_multiplier);
            }

            //Just start at 0,0 and make links with pixels that are in 2 dimensions also positive (back link them to the start) and add just those positives to this set
            void EstablishLinks(int xi, int yi, LinkedPix Pixi)
            {
                if (imWrap.Arr[xi, yi] > threshold)
                {
                    Point pi = new Point(xi, yi);
                    LinkedPix Pix2 = GetOrAdd(pi);
                    Pixi.Links.Add(Pix2);
                    Pix2.Links.Add(Pixi);
                }
            }

            LinkedPix Pix; Point p;
            for (int y = 0; y < imWrap.Height - 1; y++)
            {
                if (Slice && y % Slice_ModulusFactor < 1) continue; //This is for Axon Degeneration to break up the axons
                for (int x = 0; x < imWrap.Width - 1; x++)
                {
                    if (Slice && x % Slice_ModulusFactor < 1) continue;
                    if (imWrap.Arr[x, y] > threshold)
                    {
                        Pixels++;
                        if (CreateLinks)
                        {
                            p = new Point(x, y);
                            Pix = GetOrAdd(p);
                            if (y + 1 < imWrap.Height) EstablishLinks(x, y + 1, Pix);
                            if (x + 1 < imWrap.Height) EstablishLinks(x + 1, y, Pix);
                        }
                    }
                }
            }
            if (Pixels < 1 && AutoThreshold)
            {
                mean_multiplier /= 2; threshold = 0;
                goto TryAgainNewThreshold;
            }
        }

        public LinkedPix GetOrAdd(Point p)
        {
            if (PointDict.ContainsKey(p))
            {
                return PointDict[p];
            }
            else
            {
                var LP = new LinkedPix(p);
                PointDict.Add(p, LP);
                Add(LP);
                return LP;
            }
        }
    }

    public class SegObject : LinkedPixSet
    {
        public SegObject()
        {

        }

        public override string ToString()
        {
            return Centroid.ToString() + " " + Area_Pixels.ToString("0.0") + " area";
        }

        private void Regen()
        {
            BoundingRectangle(this, out _Centroid, out _Rect);
            _Size = _Rect.Size;
            _MinorMajorAxisRatio = Calculate_MinorMajorAxisRatio();
        }

        public static void BoundingRectangle(IEnumerable<LinkedPix> Pixels, out PointF Center, out RectangleF Rect)
        {
            float Xmin = float.MaxValue;
            float Xmax = float.MinValue;
            float Ymin = float.MaxValue;
            float Ymax = float.MinValue;
            Center = new PointF(0, 0);

            int counter = 0;
            foreach (LinkedPix pix in Pixels)
            {
                counter++;
                Center.X += pix.P.X; Center.Y += pix.P.Y;
                if (pix.P.X < Xmin) Xmin = pix.P.X;
                if (pix.P.Y < Ymin) Ymin = pix.P.Y;
                if (pix.P.X > Xmax) Xmax = pix.P.X;
                if (pix.P.Y > Ymax) Ymax = pix.P.Y;
            }
            Center.X = Center.X / counter; Center.Y = Center.Y / counter;
            SizeF Size = new SizeF(Xmax - Xmin, Ymax - Ymin);
            Rect = new RectangleF(new PointF(Xmin, Ymin), Size);
        }

        private double Calculate_MinorMajorAxisRatio()
        {
            double Angle_Divisor = 11; //Divide 180 this many times to check the Major and Minor Axes

            List<LinkedPix> LPL; SizeF SZ; PointF PT; RectangleF ReturnRect;
            SortedList<double, double> Ratios = new SortedList<double, double>();
            for (double theta = 0; theta < Math.PI; theta += Math.PI / Angle_Divisor)
            {
                LPL = new List<LinkedPix>();
                foreach (LinkedPix pix in this)
                    LPL.Add(pix.RotateCopy(theta));
                BoundingRectangle(LPL, out PT, out ReturnRect);
                SZ = ReturnRect.Size;
                double Ratio = (SZ.Width < SZ.Height ? SZ.Width / SZ.Height : SZ.Height / SZ.Width);
                if (!Ratios.ContainsKey(Ratio)) Ratios.Add(Ratio, 0);
                Ratios[Ratio]++;
            }
            return Ratios.Keys[0];
        }

        private PointF _Centroid;
        public PointF Centroid
        {
            get
            {
                if (_Centroid == PointF.Empty) Regen();
                return _Centroid;
            }
            //set => _Centroid = value;
        }

        public float Center_X { get => Centroid.X; }
        public float Center_Y { get => Centroid.Y; }

        //public float Size_Dim_Longer { get => Math.Max(Size.Width, Size.Height); }
        public float Size_Dim_Shorter { get => Math.Min(Size.Width, Size.Height); }

        //public float Dim_LongerShorter_Ratio { get => Size_Dim_Longer / Size_Dim_Shorter; }

        //public float Dim_ShorterLonger_Ratio { get => Size_Dim_Shorter / Size_Dim_Longer; }

        public float Area_Bounding { get => (float)(Math.PI * Size.Width * Size.Height); }

        public int Area_Pixels { get => Count; }

        private RectangleF _Rect;
        public RectangleF Rect
        {
            get
            {
                if (_Rect == RectangleF.Empty) Regen();
                return _Rect;
            }
        }

        private double _MinorMajorAxisRatio = -52.45;

        public double MajorMinorAxisRatio
        {
            get
            {
                if (_MinorMajorAxisRatio == -52.45) Regen();
                return _MinorMajorAxisRatio;
            }
        }

        private SizeF _Size;
        public SizeF Size
        {
            get
            {
                if (_Size == SizeF.Empty) Regen();
                return _Size;
            }
        }

        public int ObjID { get; set; }
        public string CropSavePath { get; internal set; }
        public double[] PLResults { get; internal set; }
    }

    public class SegObjects : IEnumerable<SegObject>
    {
        private HashSet<SegObject> _Set;

        private void Init()
        {
            _Set = new HashSet<SegObject>();
        }

        public SegObjects()
        {
            Init();
        }

        public int Count { get => _Set.Count; }
        public void Add(SegObject Obj)
        {
            Obj.ObjID = _Set.Count;
            _Set.Add(Obj);
        }

        public static SegObjects ObjectsFromImage(ThreshImage Image)
        {
            //Take into account the binning
            SegObjects Objs = new SegObjects();
            while (Image.Count > 0)
            {
                SegObject sObj = SearchFromPix(Image.First(), Image);
                if (sObj != null)
                {
                    if (sObj.Size_Dim_Shorter > 1)
                    {
                        Objs.Add(sObj);
                    }
                }
            }
            return Objs;
        }

        public static SegObject SearchFromPix(LinkedPix Start, LinkedPixSet Parent)
        {
            if (Start.IsMemberOfObject) return null;
            SegObject sObj = new SegObject();
            Expand(sObj, Start, Parent);
            return sObj;
        }

        internal static void Expand(SegObject sObj, LinkedPix Pix, LinkedPixSet Parent)
        {
            bool UseIteration = true;
            if (UseIteration)
            {
                //Non recursive way to do it
                HashSet<LinkedPix> NeedsWork = new HashSet<LinkedPix>() { Pix };
                var Full = new List<LinkedPix>();
                LinkedPix aPix;
                while (NeedsWork.Count > 0)
                {
                    aPix = NeedsWork.First();
                    Full.Add(aPix);
                    NeedsWork.Remove(aPix);
                    NeedsWork.UnionWith(aPix.Links);
                    aPix.LinksCrossRemove();
                } //4845
                for (int i = 0; i < Full.Count; i++)
                {
                    var bPix = Full[i];
                    bPix.MemberOfObject = sObj;
                    sObj.Add(bPix);
                    Parent.Remove(bPix);
                }
            }
            else
            {
                //This is the recursive way to do it
                Pix.MemberOfObject = sObj;
                sObj.Add(Pix);
                Parent.Remove(Pix);
                while (Pix.Links.Count > 0)
                {
                    LinkedPix t = Pix.Links.First();
                    Pix.LinksRemove(t, true);
                    Expand(sObj, t, Parent);
                }
            }
        }

        public IEnumerator<SegObject> GetEnumerator()
        {
            return ((IEnumerable<SegObject>)_Set).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_Set).GetEnumerator();
        }

        /// <summary>
        /// Stick nuclei are very elongated objects that are abberant when looking at the nuclear channel
        /// </summary>
        internal void RemoveStickNuclei(float MinRatio = 0.3F)
        {
            _Set.RemoveWhere(x => x.MajorMinorAxisRatio < MinRatio);
        }

        internal RectangleF BoundingRect(float bufferPix = 0, float MaxWidth = float.PositiveInfinity, float MaxHeight = float.PositiveInfinity)
        {
            float MinTop = _Set.Min(x => x.Rect.Top);
            float MinLeft = _Set.Min(x => x.Rect.Left);
            float MaxBottom = _Set.Max(x => x.Rect.Bottom);
            float MaxRight = _Set.Max(x => x.Rect.Right);
            MinTop = Math.Max(0, MinTop - bufferPix);
            MinLeft = Math.Max(0, MinLeft - bufferPix);
            MaxBottom = Math.Min(MaxHeight, MaxBottom + bufferPix);
            MaxRight = Math.Min(MaxWidth, MaxRight + bufferPix);
            RectangleF R = new RectangleF(MinLeft, MinTop, MaxRight - MinLeft, MaxBottom - MinTop);
            return R;
        }

        internal void RemoveNucleiBySize(float MinNuclearArea, float MaxNuclearArea)
        {
            _Set.RemoveWhere(x => x.Area_Pixels < MinNuclearArea || x.Area_Pixels > MaxNuclearArea);
        }
    }

    /// <summary>
    /// Records the results from a single object
    /// </summary>
    public class SegResultsObj
    {
        public SegResults Parent;
        internal Dictionary<int, object> Data;
        internal Dictionary<int, float> Measurements;
        public string AggKey;
        public override string ToString()
        {
            return AggKey;
        }
        //private Dictionary<int, object> MetaData;

        public SegResultsObj(SegResults ParentSegRes, XDCE_Image xI, string RaftID, string Channel, SegObject Obj)
        {
            Parent = ParentSegRes;
            Data = new Dictionary<int, object>();
            Measurements = new Dictionary<int, float>();
            //MetaData = new Dictionary<int, object>();

            AddInternal(nameof(xI.Parent.PlateID), xI.Parent.PlateID, true);
            AddInternal(nameof(xI.WellLabel), xI.WellLabel, true);
            AddInternal(nameof(xI.FOV), xI.FOV, true);
            AddInternal(nameof(RaftID), RaftID, true);
            AddInternal(nameof(Channel), Channel, true);
            AddInternal(nameof(Obj.CropSavePath), Obj.CropSavePath, true);

            AggKey = xI.Parent.PlateID + "." + xI.WellLabel + "." + xI.FOV + "." + RaftID + "." + Channel.Replace(" ", "").Substring(0, 4);

            System.Reflection.PropertyInfo[] Props = Obj.GetType().GetProperties();
            foreach (var Prop in Props)
                if (Prop.PropertyType.IsPrimitive)
                    AddInternal(Prop.Name, Prop.GetValue(Obj));
        }

        private void AddInternal(string Name, object Val, bool IsMetadata = false)
        {
            int Key2 = Parent.MasterHeaderKeyMaker(Name);
            Data.Add(Key2, Val);
            if (!IsMetadata) Measurements.Add(Key2, float.Parse(Val.ToString()));
        }

        public static char delim = '\t';

        internal string Export(bool Headers)
        {
            var sB = new StringBuilder();
            for (int i = 0; i < Parent.MasterHeaders.Count; i++)
            {
                string s = Data[i] == null ? "" : Data[i].ToString();
                sB.Append((Headers ? Parent.MasterHeadersLabel[i] : s) + delim);
            }
            return sB.ToString();
        }
    }

    /// <summary>
    /// Used to aggregate image, raft, or well-level data
    /// </summary>
    public class SegResultsAgg
    {
        public SegResults ResultParent;
        public string Name { get; set; }
        public Dictionary<int, float> Sums;
        public int Count { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public SegResultsAgg(SegResults Parent, string KeyName)
        {
            Name = KeyName;
            ResultParent = Parent;
            Sums = new Dictionary<int, float>();
            Count = 0;
        }

        public void Append(SegResultsObj tSRO)
        {
            Count++;
            //Interlocked.Add(ref Count, Count+1);
            foreach (var KVP in tSRO.Measurements)
            {
                if (!Sums.ContainsKey(KVP.Key)) Sums.Add(KVP.Key, 0);
                Sums[KVP.Key] += KVP.Value;
            }
        }

        public static char delim = SegResultsObj.delim;

        public string Export(bool Header = false)
        {
            var sB = new StringBuilder();
            for (int i = 0; i < ResultParent.MasterHeaders.Count; i++)
                if (Sums.ContainsKey(i))
                    sB.Append((Header ? ResultParent.MasterHeadersLabel[i] : (Sums[i] / Count).ToString()) + delim);
            sB.Append(Header ? "Objects" : Count.ToString() + delim);
            return sB.ToString();
        }
    }

    public class SegResults
    {
        internal ConcurrentBag<SegResultsObj> BagObj;
        internal Dictionary<string, SegResultsAgg> BagAgg;
        internal Dictionary<string, int> MasterHeaders;
        internal Dictionary<int, string> MasterHeadersLabel;

        public SegResults(int TotalCount)
        {
            BagObj = new ConcurrentBag<SegResultsObj>();
            BagAgg = new Dictionary<string, SegResultsAgg>();
            MasterHeaders = new Dictionary<string, int>();
            MasterHeadersLabel = new Dictionary<int, string>();
            ProcessedCounter = new ConcurrentBag<short>();
            TotalCountToProcess = TotalCount;
        }

        public int TotalCountToProcess { get; private set; }

        public int ResultsCount { get => BagObj.Count; }

        public int MasterHeaderKeyMaker(string Name)
        {
            string tName = Name.Trim();
            if (!MasterHeaders.ContainsKey(tName))
            {
                int tKey = MasterHeaders.Count;
                MasterHeaders.Add(tName, tKey);
                MasterHeadersLabel.Add(tKey, tName);
            }
            return MasterHeaders[tName];
        }
        public static string AggKeyMaker(SegResultsObj tSRO)
        {
            return tSRO.AggKey;
        }

        private ConcurrentBag<short> ProcessedCounter;
        public void ProcessedCountIncrement() { ProcessedCounter.Add(0); }
        public int ProcessedCount { get => ProcessedCounter.Count; }
        public string ProcessedPercent { get => ((float)ProcessedCount / TotalCountToProcess).ToString("0.0%"); }

        public void AppendObject(XDCE_Image xI, string raftID, string channelName, SegObject Obj)
        {
            var tSRO = new SegResultsObj(this, xI, raftID, channelName, Obj);
            BagObj.Add(tSRO);
            string AggKey = AggKeyMaker(tSRO);
            if (!BagAgg.ContainsKey(AggKey)) BagAgg.Add(AggKey, new SegResultsAgg(this, AggKey));
            BagAgg[AggKey].Append(tSRO);
        }

        public void ExportObjects(string ExportFilePath)
        {
            if (BagObj.Count == 0)
                return;
            using (var SW = new StreamWriter(ExportFilePath))
            {
                SW.WriteLine(BagObj.First().Export(true));
                foreach (var Line in BagObj) SW.WriteLine(Line.Export(false));
                SW.Close();
            }
        }

        public void ExportAggregations(string ExportFilePath)
        {
            using (var SW = new StreamWriter(ExportFilePath))
            {
                SW.WriteLine(BagAgg.First().Value.Export(true));
                foreach (var Line in BagAgg.Values) SW.WriteLine(Line.Export(false));
                SW.Close();
            }
        }


    }

    public class CropImageSettings
    {
        private Dictionary<string, string> _WellDict;

        [XmlIgnore]
        public Dictionary<string, string> WellDict
        {
            get
            {
                if (_WellDict == null && WellLookUp_WellLabel_Comma_Folder_Semicolon != "")
                {
                    WellDictionary_Refresh_FromString();
                }
                return _WellDict;
            }
            set { _WellDict = value; }
        }

        [XmlIgnore]
        public PL.MLSettings_PL MLSettings { get; set; }

        public string WellLookUp_WellLabel_Comma_Folder_Semicolon { get; set; }

        public bool WellDictionary_Refresh_FromString()
        {
            var TempOld = _WellDict;
            try
            {
                _WellDict = new Dictionary<string, string>();
                string[] Arr = WellLookUp_WellLabel_Comma_Folder_Semicolon.Split(';');
                foreach (string item in Arr)
                {
                    string[] Arr2 = item.Split(',');
                    if (Arr2.Length == 2)
                    {
                        _WellDict.Add(Arr2[0], Arr2[1]);
                    }
                }
                return true;
            }
            catch
            {
                _WellDict = TempOld;
                return false;
            }
        }

        public string WellDictMatch(XDCE_Image xI)
        {
            //First try just the well
            string key;
            key = xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];
            
            //Now try the full plateID and the well
            key = xI.Parent.PlateID + "|" + xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];

            //Now try the plateID no Index and the well
            key = xI.Parent.ParentFolder.PlateID_NoScanIndex + "|" + xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];

            //Now try the plate index and the well
            key = xI.Parent.ParentFolder.PlateIndex + "|" + xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];

            //Now try plate # 1
            key = "1" + "|" + xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];
            return "UNK";
        }

        public void String_FromWellDictionary()
        {
            WellLookUp_WellLabel_Comma_Folder_Semicolon = String.Join(";", WellDict.Select(x => x.Key + "," + x.Value));
        }

        public CropImageSettings()
        {
            ParallelizeSegmentation = false;
            if (CropSize.Height == 0)
            {
            }
        }

        public CropImageSettings(CropImageSettings CopyFrom)
        {
            foreach (var Prop in typeof(CropImageSettings).GetProperties())
            {
                Prop.SetValue(this, Prop.GetValue(CopyFrom));
            }
        }

        public CropImageSettings(bool LoadDefaults)
        {
            if (LoadDefaults)
            {
                //For now, this is the best way to do cell/object based exportes.
                //System.Diagnostics.Debugger.Break();
                WellDict = new Dictionary<string, string>(); WellDict.Add("A - 1", "WT C1"); WellDict.Add("B - 2", "MU C1"); //WellDest.Add("C - 2", "G1"); WellDest.Add("A - 3", "G2"); WellDest.Add("B - 3", "G2");
                String_FromWellDictionary();

                CropSize = new Size(100, 100);
                SaveFolder = @"K:\_Training\Leica MFN2\";
                DirectoryInfo DI = new DirectoryInfo(SaveFolder); if (!DI.Exists) DI.Create();
                MinAreaPixels = 70; MaxAreaPixels = 1000;
                SaveTypeExtension_NoDot = "tiff";
                RandomizeImageIntensity_Min = 1; //This turns it off
                RunMLProcessing = "";
                Version = 3; //This can help us determine if we need to update pieces
                ParallelizeSegmentation = false;
            }
        }

        public int Version { get; set; }

        public Size CropSize { get; set; }
        public string SaveFolder { get; set; }
        public int MinAreaPixels { get; set; }
        public int MaxAreaPixels { get; set; }
        public float RandomizeImageIntensity_Min { get; set; }

        public string SaveTypeExtension_NoDot { get; set; }

        public string RunMLProcessing { get; set; }

        public bool ParallelizeSegmentation { get; set; }

        public bool StoreResults = true;

        public void GetRectangle(SegObject obj, SizeF SourceRect, float ThreshBinning, out RectangleF CropRegion, out PointF StartOfRegion)
        {
            //Since the thresholded image was binned to speed things up (And we don't need the resolution to get the objects), we have to reverse out the binning here
            float x1 = (obj.Center_X * ThreshBinning) - CropSize.Width / 2;
            float y1 = (obj.Center_Y * ThreshBinning) - CropSize.Height / 2;
            float x2 = (obj.Center_X * ThreshBinning) + CropSize.Width / 2;
            float y2 = (obj.Center_Y * ThreshBinning) + CropSize.Height / 2;

            //Make sure it doesn't go off the edges of the canvas
            float x1p = Math.Min(SourceRect.Width, Math.Max(0, x1));
            float y1p = Math.Min(SourceRect.Height, Math.Max(0, y1));
            float x2p = Math.Min(SourceRect.Width, Math.Max(0, x2));
            float y2p = Math.Min(SourceRect.Height, Math.Max(0, y2));

            var P1 = new PointF(x1p, y1p);
            var S = new SizeF(x2p - x1p, y2p - y1p);
            CropRegion = new RectangleF(P1, S);
            StartOfRegion = new PointF(Math.Max(0, -x1), Math.Max(0, -y1));
        }

        public string GetSavePath(XDCE_Image xI, SegObject obj)
        {
            string FileName = xI.Parent.PlateID + "." + xI.WellField + "_" + obj.ObjID + "." + SaveTypeExtension_NoDot;
            string SubFolder = WellDictMatch(xI);
            string Folder = Path.Combine(SaveFolder, SubFolder);
            DirectoryInfo DI = new DirectoryInfo(Folder); if (!DI.Exists) DI.Create();
            return Path.Combine(Folder, FileName);
        }
    }


}

namespace FIVE_IMG
{
    public class Store_Validation_Parameters
    {
        public string SequenceFull { get; set; }
        public string SequenceStart { get; set; }
        public string SequenceEnd { get; set; }
        public string Name { get; set; }
        public string FolderPath { get; set; }
        public List<string> MRU_FolderPaths { get; set; }

        [XmlIgnore]
        protected HashSet<string> MRU_FolderPaths_Hash { get; set; }
        public string SearchPattern { get; set; }
        public bool RenameColumns_DuringInCartaCompile { get; set; }

        public string HCS_Image_SourceFolder { get; set; }
        public string HCS_Image_DestinationFolder { get; set; }
        public string HCS_Image_PlateName { get; set; }
        public bool HCS_Image_CropSquare { get; set; }
        public SVP_Registration_Params RegParams { get; set; }
        public FIVE.TF.ModelInferSettings ModelInferSettings { get; set; }
        public string NormDivisorColumn { get; set; }
        public string fs_Port { get; set; }
        public string fs_dbname { get; set; }
        public string fs_Username { get; set; }
        public string fs_Password { get; set; }
        public string fs_Filetoload { get; set; }
        public int PrePickCheck_Annotated_MaxToLoad { get; set; }
        public string FIVTools_Version { get; set; }
        public DateTime SaveTime { get; set; }

        public PL.MLSettings_PL MLSettings_PL { get; set; }

        public Seg.CropImageSettings CropSettings { get; set; }

        public static string DefaultPath = Path.Combine(Path.GetTempPath(), "geicDesktoptools_settings.xml");

        public bool RegenMRUs(string NewFolderPath = "")
        {
            if (MRU_FolderPaths == null) MRU_FolderPaths = new List<string>();
            if (MRU_FolderPaths_Hash == null) MRU_FolderPaths_Hash = new HashSet<string>(MRU_FolderPaths);
            if (NewFolderPath == "") return false;
            if (!MRU_FolderPaths_Hash.Contains(NewFolderPath))
            {
                MRU_FolderPaths.Add(NewFolderPath); MRU_FolderPaths_Hash.Add(NewFolderPath);
                return true;
            }
            return false;
        }

        public Store_Validation_Parameters()
        {
            RenameColumns_DuringInCartaCompile = true;
            NormDivisorColumn = "mlPlateIndex";
            RegParams = new SVP_Registration_Params() { Mask_PixelsExpand = 0, InitialErrorDivisor = 1.5F, InitialDefaultOffset = new PointF(940, 2203) };

            fs_Port = "5432";
            fs_dbname = "postgres";
            fs_Username = "postgres";
            fs_Password = "ge2";

            CropSettings = new Seg.CropImageSettings();
            MLSettings_PL = new PL.MLSettings_PL();
            ModelInferSettings = FIVE.TF.ModelInferSettings.Default01();

            PrePickCheck_Annotated_MaxToLoad = 192;
        }

        public static Store_Validation_Parameters Load()
        {
            return Load(DefaultPath);
        }

        public static Store_Validation_Parameters Load(string Path)
        {
            if (!File.Exists(Path)) return null;
            try
            {
                using (var stream = File.OpenRead(Path))
                {
                    var serializer = new XmlSerializer(typeof(Store_Validation_Parameters));
                    var SVP = (Store_Validation_Parameters)serializer.Deserialize(stream);
                    if (SVP.NormDivisorColumn == null) SVP.NormDivisorColumn = "mlPlateIndex";
                    SVP.RegParams.Check();
                    if (SVP.MLSettings_PL == null) SVP.MLSettings_PL = new PL.MLSettings_PL();
                    if (SVP.CropSettings == null) SVP.CropSettings = new Seg.CropImageSettings(true);
                    if (SVP.CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon == null) SVP.CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon = "A - 1,WT;B - 1;MU";
                    if (SVP.CropSettings.RunMLProcessing == null) SVP.CropSettings.RunMLProcessing = "";
                    if (SVP.ModelInferSettings.Model_Classification_Path == null) SVP.ModelInferSettings.Model_Classification_Path = ModelInferSettings.Default01().Model_Classification_Path;
                    if (SVP.ModelInferSettings.Model_Measure_Path == null) SVP.ModelInferSettings.Model_Measure_Path = ModelInferSettings.Default01().Model_Measure_Path;
                    return SVP;
                }
            }
            catch
            {
                return null;
            }
        }

        public void Save()
        {
            Save(DefaultPath);
        }

        public void Save(string Path)
        {
            this.SaveTime = DateTime.Now;
            using (var writer = new StreamWriter(Path))
            {
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(writer, this);
                writer.Flush();
            }
        }
    }

    public class SVP_Registration_Params
    {
        public bool Rotate180 { get; set; }
        public float InitialErrorDivisor { get; set; }
        public PointF InitialDefaultOffset { get; set; }
        public int Mask_PixelsExpand { get; set; }
        public bool Mask_20x_to_10x { get; set; }
        public int MskImg_InCell_Width { get; set; }
        public int MskImg_Final_Leica { get; set; }
        public bool CropForLeica { get; set; }

        /// <summary>
        /// Bay put this in to try and match up the masks exactly to the DMD Size
        /// </summary>
        public int MskImg_dither_Width { get; set; } //Bay put this in to try and match up the masks exactly to the DMD size (800x600?)

        public int MskShift_Final_AddPixels_X { get; set; } //Adds this amount of pixels to additionally shift in X (Default 0)
        public int MskShift_Final_AddPixels_Y { get; set; } //Adds this amount of pixels to additional shift in Y (Default 0)
        public int MskShift_Final_ZoomPixels { get; set; } //Removes this many pixels from each side to "zoom" in the mask. Negative adds black pixels to zoom out (Default -1)
        public float MskShift_ShiftMultiplier { get; set; } //This adusts the "strength" of the mask shift by this factor. 2 means it shifts twice as far as it thinks it should be shifted. 0.5 means to shift it half as far. - anything will flop the direction of the shifts (Default 0.5)

        public PointF FOVOffset_MaxDeviationFromWell { get; set; } //If offset for a FOV changes more than this amount from the Well Offset, then this FOV will be considered to FAIL registration and will be skipped (Default 25,25)
        public float LastFOVOffset_over_WellOffset_MixRatio { get; set; } //For an UNREGISTERED FOV, a MixRatio = 1 means to use the last field's Offset, MixRatio = 0 means to use the Well Offset, and 0.5 means to mix them equally (Default 1)

        public SizeF GE_LeicaSizeRatio_ForImage { get; set; } //This is when the GE image is produced for use in registration (Default 1,1)
        public SizeF GE_LeicaSizeRatio_ForMoving { get; set; } //This is when the GE coordinates are adapted to find the Leica coordinates (in combination with the offset) (Default 1,1)



        public SVP_Registration_Params()
        {
            //Below are the defaults

            //Crop for Leica : The GE image is slighly larger, so we will scale it up to 1040 (instead of 1024), then chop off 8 pixels all the way around
            CropForLeica = true;
            Rotate180 = true;
            MskImg_InCell_Width = 2040;
            MskImg_Final_Leica = 1024;
            //MskImg_toChop = 8; //This is necessary for the cells to line up with the masks - removed
            MskImg_dither_Width = 600; //Bay put this in to try and match up the masks exactly to the DMD size (800x600?)
            LastFOVOffset_over_WellOffset_MixRatio = 0.5F;
            FOVOffset_MaxDeviationFromWell = new PointF(100, 100);
            GE_LeicaSizeRatio_ForImage = new SizeF(1.0149F, 1.0149F); //This is when the GE image is produced for used in registration
            GE_LeicaSizeRatio_ForMoving = new SizeF(1, 1);            //This is when the GE coordinates are adapted to find the Leica coordinates (in combination with the offset)
        }

        internal void Check()
        {
            if (InitialErrorDivisor <= 0)
            {
                InitialErrorDivisor = 1.5F; CropForLeica = true; Rotate180 = true;
            }
            if (InitialDefaultOffset.IsEmpty) InitialDefaultOffset = new PointF(1000, 2500);
            if (FOVOffset_MaxDeviationFromWell.IsEmpty) FOVOffset_MaxDeviationFromWell = new PointF(100, 100);
            if (MskImg_InCell_Width <= 0) MskImg_InCell_Width = 2040;
            if (MskImg_Final_Leica <= 0) MskImg_Final_Leica = 1024;
            if (GE_LeicaSizeRatio_ForImage.IsEmpty)
            {
                GE_LeicaSizeRatio_ForImage = new SizeF(1.0149F, 1.0149F);
                GE_LeicaSizeRatio_ForMoving = new SizeF(1, 1);
            }
        }
    }
}

namespace FIVE_IMG
{
    //This works with the graphics for masks
    public static class MasksHelper
    {
        public static string ExportAll(INCELL_Folder folder, SVP_Registration_Params SRVP, System.ComponentModel.BackgroundWorker BW = null)
        {
            var fI = new FileInfo(folder.MaskList_FullPath);
            if (!fI.Exists) return "Couldn't find MaskList.tsv file.  Please save a file with the list of cells you would like to generate masks for. If should be names like this\r\n" + fI.FullName + "\r\nmake sure it has the Well, FOV, Imagename, Left, Top, Width, and Height of the cell in tab-separated format";
            var ML = new MaskList(fI.FullName, folder.PlateID, folder.XDCE.PixelWidth_in_um);

            bool problem = false;
            if (ML == null) problem = true; else if (ML.Count == 0) problem = true;
            if (problem) return INCELL_Folder.ErrorLog;

            if (BW != null) BW.ReportProgress(0, "Loaded Mask List.");
            SRVP.CropForLeica = true;
            MasksHelper.SaveMasks(ML, fI.DirectoryName, SRVP, BW);
            return "";
        }

        public static void ExportOverlay(ImageAlign_Return IAR, string ExportFullFileName, ImgWrap ImgInternal, ImgWrap ImgExternal)
        {
            //First need to "Expand" the Internal Image with Black Border (by pasting it into a black image)
            Bitmap destBitmap = new Bitmap(ImgExternal.Width, ImgExternal.Height);
            using (Graphics g = Graphics.FromImage(destBitmap))
            {
                Rectangle ImageRect_SRC = new Rectangle(0, 0, ImgInternal.Width, ImgInternal.Height);
                Rectangle ImageRect_Dest = new Rectangle(0, 0, ImgExternal.Width, ImgExternal.Height);
                Rectangle destRegion = new Rectangle(Point.Round(IAR.RR.Location), ImgInternal.BMP.Size);
                g.FillRectangle(Brushes.Black, ImageRect_Dest);
                g.DrawImage(ImgInternal.BMP, destRegion, ImageRect_SRC, GraphicsUnit.Pixel);
                g.DrawRectangle(Pens.Yellow, destRegion);
            }
            Bitmap rBMP = Utilities.ColorMatrix.ArithmeticBlend(destBitmap, ImgExternal.BMP, FIVE_IMG.Utilities.ColorMatrix.ColorCalculationType.Difference);
            rBMP.Save(Path.Combine(ExportFullFileName));
        }

        /// <summary>
        /// This is used just to process the mask without having done registration
        /// </summary>
        public static string ExportShiftedMask(XDCE_Image xI, SVP_Registration_Params RegParams)
        {
            //if (!xI.HasMask) xI = xI.Parent.GetField(xI.FOV, 1);
            //Just a cheating way to pass on the RegParams but nothing else
            var IAR = new ImageAlign_Return();
            IAR.Parent = new Associate_InCell_Leica(RegParams);
            IAR.RR = new Register_Return();
            return ExportShiftedMask(xI, IAR, Size.Empty, Size.Empty);
        }

        /// <summary>
        /// This is used to really shift the mask after registration has been performed
        /// </summary>
        public static string ExportShiftedMask(XDCE_Image xI, ImageAlign_Return IAR, Size GEFract_Size, Size Overlap, string MaskSaveTemp = "")
        {
            if (!xI.HasMask) return "no mask";
            var MaskImage = new Bitmap(xI.MaskFullPath);
            var FI = new FileInfo(xI.MaskFullPath);
            long ImgBits = (8 * FI.Length) / (MaskImage.Size.Width * MaskImage.Size.Height);


            bool ShiftMode = !GEFract_Size.IsEmpty;
            float shift_X = 0, shift_Y = 0;

            if (ShiftMode)
            {
                float CenterPixel_LE_small_X = IAR.RR.Location.X + (GEFract_Size.Width / 2); float CenterPixel_LE_small_Y = IAR.RR.Location.Y + (GEFract_Size.Height / 2);
                shift_X = CenterPixel_LE_small_X - ((float)Overlap.Width / 2); shift_Y = CenterPixel_LE_small_Y - ((float)Overlap.Height / 2);
                float Factor = (float)MaskImage.Width / (Overlap.Width);
                Factor *= IAR.Parent.Reg_Params.MskShift_ShiftMultiplier; //This adjusts the "strength" of the mask shift by this factor. 2 means it shifts twice as far as it thinks it should
                shift_X *= Factor; shift_Y *= Factor;
            }
            shift_X += IAR.Parent.Reg_Params.MskShift_Final_AddPixels_X; shift_Y += IAR.Parent.Reg_Params.MskShift_Final_AddPixels_Y; //Adds this amount of pixels to additionally shift in X
            IAR.RR.ShiftMask_Pixels = new PointF(shift_X, shift_Y); //Save this so we can see it elsewhere

            int Zm = IAR.Parent.Reg_Params.MskShift_Final_ZoomPixels; //Removes this many pixels from each side to "zoom" in the mask. Negative adds black pixels to zoom out
            Bitmap destBitmap = new Bitmap(MaskImage.Width, MaskImage.Height);
            using (Graphics g = Graphics.FromImage(destBitmap))
            {
                //Fill the destination image background to black
                Rectangle ImageRect_Dest = new Rectangle(0, 0, destBitmap.Width, destBitmap.Height);
                g.FillRectangle(Brushes.Black, ImageRect_Dest);

                //Now actually plug the mask image into the output
                Rectangle IR_SRC = new Rectangle((int)Math.Max(0, -shift_X), (int)Math.Max(0, -shift_Y), MaskImage.Width - (int)Math.Abs(shift_X), MaskImage.Height - (int)Math.Abs(shift_Y));
                Rectangle IR_sZoom = Zm > 0 ? new Rectangle(IR_SRC.X + Zm, IR_SRC.Y + Zm, IR_SRC.Width - (Zm * 2), IR_SRC.Height - (Zm * 2)) : IR_SRC; //If zoom is positive, then this shrinks the source rectangle, otherwise doesn't change
                Rectangle destRegion = new Rectangle((int)Math.Max(0, shift_X), (int)Math.Max(0, shift_Y), IR_SRC.Width, IR_SRC.Height);
                Rectangle destZoom = Zm > 0 ? destRegion : new Rectangle(destRegion.X - Zm, destRegion.Y - Zm, destRegion.Width + (Zm * 2), destRegion.Height + (Zm * 2)); //If zoom is negative (zoom out), then source rectangle doesnt change, but the destination region is shrunk (since this zoom is negative that is why the signs are swapped)
                g.DrawImage(MaskImage, destZoom, IR_sZoom, GraphicsUnit.Pixel);
            }

            if (ImgBits <= 2)
                MasksHelper.Save1BitTiff(destBitmap, xI.MaskShiftedFullPath);
            else
                MasksHelper.Save8BitTiff(destBitmap, xI.MaskShiftedFullPath);
            return xI.MaskShiftedFullPath;
        }


        public static Bitmap DrawMask(Seg.SegObjects SegObjs, int OrigWidth, int NewWidth, int pixelExpand = 0, bool RandColors = false)
        {

            List<MaskListEntry> Cells = new List<MaskListEntry>();
            foreach (Seg.SegObject Obj in SegObjs)
            {
                Cells.Add(new MaskListEntry() { Left_pix = Obj.Rect.Left, Top_pix = Obj.Rect.Top, Width_pix = Obj.Size.Width, Height_pix = Obj.Size.Height });
            }
            return DrawMask(Cells, OrigWidth, NewWidth, pixelExpand, RandColors);
        }

        public static Bitmap DrawMask(List<MaskListEntry> Cells, int WidthOriginal, int WidthNew, int pixelExpand = 0, bool RandColors = false)
        {
            Bitmap bmap = new Bitmap(WidthNew, WidthNew);
            float Ratio = (float)WidthNew / WidthOriginal;
            using (Graphics g = Graphics.FromImage(bmap))
            {
                g.FillRectangle(solidBrushBlack, new Rectangle(new Point(0, 0), bmap.Size));
                var tBrush = solidBrushBlack;
                for (int i = 0; i < Cells.Count; i++)
                    drawEllipse(g, bmap, Cells[i], Ratio, pixelExpand, RandColors);
            }
            return bmap;
        }

        private static Random _Rand = new Random();
        public static Pen penBlack = new Pen(Color.Black, 1);
        public static Pen penWhite = new Pen(Color.White, 1);
        public static SolidBrush solidBrushBlack = new SolidBrush(Color.Black);
        public static SolidBrush solidBrushWhite = new SolidBrush(Color.White);
        public static SolidBrush solidBrushGray = new SolidBrush(Color.Gray);
        public static SolidBrush solidBrushRed = new SolidBrush(Color.Red);

        public static void drawEllipse(Graphics g, Bitmap bmap, MaskListEntry ellipse, float Resize, int pixelExpand = 0, bool RandColors = false)
        {
            var r = new Random();
            Brush solidBrushUse;
            if (RandColors)
                solidBrushUse = new SolidBrush(Color.FromArgb(_Rand.Next(0, 255), _Rand.Next(0, 255), _Rand.Next(0, 255)));
            else
                solidBrushUse = MaskList.CheckStringAgainst_IntensityType(ellipse.Intensity_Method, MaskIntensityType.Grayscale) ?
                new SolidBrush(Color.FromArgb((int)(ellipse.Intensity * 255), (int)(ellipse.Intensity * 255), (int)(ellipse.Intensity * 255))) :
                solidBrushWhite;

            bool Rotate = false;
            int outer = pixelExpand;
            int leftBound = (int)(ellipse.Left_pix * Resize);
            int topBound = (int)(ellipse.Top_pix * Resize);
            int width = (int)(ellipse.Width_pix * Resize);
            int height = (int)(ellipse.Height_pix * Resize);
            int xa, ya;

            Color pixelcolor;
            if (pixelExpand > 0)
            {
                leftBound = leftBound - outer;
                topBound = topBound - outer;
                width = width + 2 * outer;
                height = height + 2 * outer;
            }

            if (Rotate)
            {
                //Bay/Willie realized 6/2/2021 that this scheme will not work since the cells rotated bounding boxes will "cut" out each other if close together
                Bitmap BMP = new Bitmap((int)ellipse.Width_pix * 2, (int)ellipse.Height_pix * 2);
                using (Graphics g2 = Graphics.FromImage(BMP))
                {
                    g2.TranslateTransform((ellipse.Left_pix + ellipse.minorAxisLength_Pix) * Resize, (ellipse.Top_pix + ellipse.majorAxisLength_Pix) * Resize);
                    g2.RotateTransform(ellipse.axisAngle_deg);
                    g2.TranslateTransform((ellipse.Left_pix + ellipse.minorAxisLength_Pix) * -Resize, (ellipse.Top_pix + ellipse.majorAxisLength_Pix) * -Resize);
                    g2.FillEllipse(solidBrushWhite, ellipse.Left_pix * Resize, ellipse.Top_pix * Resize, ellipse.majorAxisLength_Pix * 2 * Resize, ellipse.minorAxisLength_Pix * 2 * Resize);
                }
                g.DrawImage(BMP, new PointF(1, 1));
            }
            else
            {
                g.FillEllipse(solidBrushUse, leftBound, topBound, width, height);
            }
            //If grayscale is on, now we subtract from the ellipse to get the dithered mask
            if (MaskList.CheckStringAgainst_IntensityType(ellipse.Intensity_Method, MaskIntensityType.Dither))
            {
                SortedList<double, int[]> pixels = new SortedList<double, int[]>();
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        xa = x + leftBound; ya = y + topBound;
                        pixelcolor = bmap.GetPixel(xa, ya);
                        if (pixelcolor.R > 0) //i.e. is white
                        {
                            double rand = r.NextDouble();
                            while (pixels.ContainsKey(rand)) r.NextDouble();
                            pixels.Add(rand, new int[] { xa, ya });
                        }
                    }
                }
                int counter = 0;
                foreach (KeyValuePair<double, int[]> pix in pixels)
                {
                    if (counter++ >= (pixels.Count - (pixels.Count * ellipse.Intensity))) break;
                    g.FillRectangle(solidBrushBlack, pix.Value[0], pix.Value[1], 1, 1);
                }
            }
            if (MaskList.CheckStringAgainst_IntensityType(ellipse.Intensity_Method, MaskIntensityType.Skeleton))
            {
                int ao = 1;
                byte[,] arr = new byte[width + ao * 2, height + ao * 2]; int total = 0;
                //11/9/2021 New method that avoids holes
                //First go through and figure out which points are in the list
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        xa = x + leftBound; ya = y + topBound; pixelcolor = bmap.GetPixel(xa, ya);
                        arr[x + ao, y + ao] = (byte)((pixelcolor.R > 0) ? 1 : 0); //1 if white
                        if (pixelcolor.R > 0) total++;
                    }
                }
                //Now go and turn off blocks of 2 based on the saved list
                int counter = 0; Point tP; int xo, yo, xt, yt; bool exec;
                xo = width / 2; yo = height / 2;
                for (int i = 0; i < MaskSkeletonOrder.Count; i++)
                {
                    exec = false;
                    tP = MaskSkeletonOrder[i]; xt = tP.X * 2 + xo; yt = tP.Y * 2 + yo;
                    if ((xt >= (width + ao)) || (xt < -ao) || (yt >= (height + ao)) || (yt < -ao)) continue;
                    if (arr[xt + ao, yt + ao] == 1) exec = true;
                    if (!exec)
                    {
                        if ((xt + 1 >= (width + ao)) || (yt + 1 >= (height + ao))) continue;
                        if (arr[xt + ao + 1, yt + ao + 1] == 1) exec = true;
                        if (arr[xt + ao + 0, yt + ao + 1] == 1) exec = true;
                        if (arr[xt + ao + 1, yt + ao + 0] == 1) exec = true;
                    }
                    if (exec)
                    {
                        g.FillRectangle(solidBrushBlack, xt + leftBound, yt + topBound, 2, 2);
                        counter += 4;
                        if (counter >= (total - (total * ellipse.Intensity))) break;
                    }
                }
            }
        }

        private static List<Point> _MaskSkeletonOrder;
        private static List<Point> MaskSkeletonOrder
        {
            //11/8/2021 this allows a semi-random pattern that is better for Leica scope region generation
            get
            {
                if (_MaskSkeletonOrder == null)
                {
                    _MaskSkeletonOrder = new List<Point>();
                    var PL = _MaskSkeletonOrder;
                    {
                        PL.Add(new Point(10, 4));
                        PL.Add(new Point(-11, 10));
                        PL.Add(new Point(-11, 0));
                        PL.Add(new Point(-11, -10));
                        PL.Add(new Point(-11, -2));
                        PL.Add(new Point(-11, 12));
                        PL.Add(new Point(-11, 6));
                        PL.Add(new Point(10, -8));
                        PL.Add(new Point(-11, -8));
                        PL.Add(new Point(10, 0));
                        PL.Add(new Point(-11, 8));
                        PL.Add(new Point(10, 2));
                        PL.Add(new Point(10, -10));
                        PL.Add(new Point(-11, 2));
                        PL.Add(new Point(-11, 4));
                        PL.Add(new Point(10, 8));
                        PL.Add(new Point(10, 6));
                        PL.Add(new Point(-11, -4));
                        PL.Add(new Point(10, -6));
                        PL.Add(new Point(10, 10));
                        PL.Add(new Point(-11, -6));
                        PL.Add(new Point(10, 12));
                        PL.Add(new Point(10, -2));
                        PL.Add(new Point(10, -4));
                        PL.Add(new Point(-10, -6));
                        PL.Add(new Point(-10, 6));
                        PL.Add(new Point(-10, -8));
                        PL.Add(new Point(9, 4));
                        PL.Add(new Point(-10, -2));
                        PL.Add(new Point(9, -4));
                        PL.Add(new Point(9, 8));
                        PL.Add(new Point(-10, 4));
                        PL.Add(new Point(-10, 12));
                        PL.Add(new Point(9, 12));
                        PL.Add(new Point(9, -8));
                        PL.Add(new Point(-10, -4));
                        PL.Add(new Point(9, 2));
                        PL.Add(new Point(9, 10));
                        PL.Add(new Point(-10, 8));
                        PL.Add(new Point(9, -2));
                        PL.Add(new Point(-10, 10));
                        PL.Add(new Point(9, -10));
                        PL.Add(new Point(-10, 2));
                        PL.Add(new Point(-10, 0));
                        PL.Add(new Point(9, 6));
                        PL.Add(new Point(9, -6));
                        PL.Add(new Point(9, 0));
                        PL.Add(new Point(-10, -10));
                        PL.Add(new Point(8, -6));
                        PL.Add(new Point(8, 10));
                        PL.Add(new Point(8, 8));
                        PL.Add(new Point(-9, -4));
                        PL.Add(new Point(8, -8));
                        PL.Add(new Point(-9, 10));
                        PL.Add(new Point(8, 4));
                        PL.Add(new Point(-9, -2));
                        PL.Add(new Point(-9, 2));
                        PL.Add(new Point(-9, -8));
                        PL.Add(new Point(8, 6));
                        PL.Add(new Point(8, 2));
                        PL.Add(new Point(-9, 0));
                        PL.Add(new Point(8, -4));
                        PL.Add(new Point(8, 12));
                        PL.Add(new Point(8, 0));
                        PL.Add(new Point(-9, 6));
                        PL.Add(new Point(-9, 4));
                        PL.Add(new Point(-9, -6));
                        PL.Add(new Point(8, -2));
                        PL.Add(new Point(-9, -10));
                        PL.Add(new Point(-9, 12));
                        PL.Add(new Point(8, -10));
                        PL.Add(new Point(-9, 8));
                        PL.Add(new Point(-8, 10));
                        PL.Add(new Point(7, 2));
                        PL.Add(new Point(-8, -4));
                        PL.Add(new Point(7, 4));
                        PL.Add(new Point(-8, -6));
                        PL.Add(new Point(-8, 8));
                        PL.Add(new Point(-8, 4));
                        PL.Add(new Point(7, 0));
                        PL.Add(new Point(-8, 12));
                        PL.Add(new Point(-8, -10));
                        PL.Add(new Point(7, 12));
                        PL.Add(new Point(7, -10));
                        PL.Add(new Point(7, 8));
                        PL.Add(new Point(7, -6));
                        PL.Add(new Point(-8, -8));
                        PL.Add(new Point(7, -2));
                        PL.Add(new Point(7, 10));
                        PL.Add(new Point(-8, -2));
                        PL.Add(new Point(-8, 6));
                        PL.Add(new Point(7, 6));
                        PL.Add(new Point(7, -4));
                        PL.Add(new Point(-8, 0));
                        PL.Add(new Point(-8, 2));
                        PL.Add(new Point(7, -8));
                        PL.Add(new Point(6, -2));
                        PL.Add(new Point(6, -8));
                        PL.Add(new Point(6, 6));
                        PL.Add(new Point(-7, 0));
                        PL.Add(new Point(-7, 6));
                        PL.Add(new Point(-7, 4));
                        PL.Add(new Point(-7, -2));
                        PL.Add(new Point(-7, -10));
                        PL.Add(new Point(-7, 10));
                        PL.Add(new Point(6, -4));
                        PL.Add(new Point(-7, 8));
                        PL.Add(new Point(6, 4));
                        PL.Add(new Point(6, 8));
                        PL.Add(new Point(6, -6));
                        PL.Add(new Point(-7, -6));
                        PL.Add(new Point(6, -10));
                        PL.Add(new Point(-7, 2));
                        PL.Add(new Point(-7, -4));
                        PL.Add(new Point(6, 10));
                        PL.Add(new Point(-7, 12));
                        PL.Add(new Point(6, 0));
                        PL.Add(new Point(6, 12));
                        PL.Add(new Point(-7, -8));
                        PL.Add(new Point(6, 2));
                        PL.Add(new Point(-6, 0));
                        PL.Add(new Point(-6, -2));
                        PL.Add(new Point(5, 6));
                        PL.Add(new Point(5, -6));
                        PL.Add(new Point(-6, 2));
                        PL.Add(new Point(-6, -8));
                        PL.Add(new Point(-6, 12));
                        PL.Add(new Point(-6, 10));
                        PL.Add(new Point(5, 8));
                        PL.Add(new Point(5, -10));
                        PL.Add(new Point(5, -8));
                        PL.Add(new Point(5, -4));
                        PL.Add(new Point(-6, -4));
                        PL.Add(new Point(5, 10));
                        PL.Add(new Point(5, -2));
                        PL.Add(new Point(-6, -6));
                        PL.Add(new Point(-6, 6));
                        PL.Add(new Point(5, 0));
                        PL.Add(new Point(-6, 8));
                        PL.Add(new Point(5, 12));
                        PL.Add(new Point(5, 4));
                        PL.Add(new Point(5, 2));
                        PL.Add(new Point(-6, 4));
                        PL.Add(new Point(-6, -10));
                        PL.Add(new Point(4, -8));
                        PL.Add(new Point(4, -2));
                        PL.Add(new Point(-5, 4));
                        PL.Add(new Point(-5, 6));
                        PL.Add(new Point(-5, -10));
                        PL.Add(new Point(4, 8));
                        PL.Add(new Point(4, -4));
                        PL.Add(new Point(-5, -2));
                        PL.Add(new Point(4, 10));
                        PL.Add(new Point(-5, 10));
                        PL.Add(new Point(4, 0));
                        PL.Add(new Point(4, 2));
                        PL.Add(new Point(4, 6));
                        PL.Add(new Point(-5, 8));
                        PL.Add(new Point(-5, -4));
                        PL.Add(new Point(-5, -8));
                        PL.Add(new Point(-5, -6));
                        PL.Add(new Point(-5, 2));
                        PL.Add(new Point(4, -6));
                        PL.Add(new Point(4, -10));
                        PL.Add(new Point(4, 4));
                        PL.Add(new Point(-5, 12));
                        PL.Add(new Point(4, 12));
                        PL.Add(new Point(-5, 0));
                        PL.Add(new Point(3, -8));
                        PL.Add(new Point(-4, 0));
                        PL.Add(new Point(3, -6));
                        PL.Add(new Point(3, 6));
                        PL.Add(new Point(-4, 8));
                        PL.Add(new Point(3, -2));
                        PL.Add(new Point(3, -10));
                        PL.Add(new Point(3, 8));
                        PL.Add(new Point(3, 12));
                        PL.Add(new Point(-4, 12));
                        PL.Add(new Point(-4, -4));
                        PL.Add(new Point(-4, 2));
                        PL.Add(new Point(-4, -6));
                        PL.Add(new Point(-4, -10));
                        PL.Add(new Point(3, 4));
                        PL.Add(new Point(3, 2));
                        PL.Add(new Point(-4, 10));
                        PL.Add(new Point(3, 10));
                        PL.Add(new Point(3, 0));
                        PL.Add(new Point(3, -4));
                        PL.Add(new Point(-4, -2));
                        PL.Add(new Point(-4, 6));
                        PL.Add(new Point(-4, -8));
                        PL.Add(new Point(-4, 4));
                        PL.Add(new Point(2, 6));
                        PL.Add(new Point(2, 8));
                        PL.Add(new Point(-3, 4));
                        PL.Add(new Point(2, -2));
                        PL.Add(new Point(-3, -10));
                        PL.Add(new Point(-3, -2));
                        PL.Add(new Point(2, 2));
                        PL.Add(new Point(2, 4));
                        PL.Add(new Point(-3, -4));
                        PL.Add(new Point(-3, -8));
                        PL.Add(new Point(2, 12));
                        PL.Add(new Point(-3, 10));
                        PL.Add(new Point(-3, 12));
                        PL.Add(new Point(2, -6));
                        PL.Add(new Point(2, -10));
                        PL.Add(new Point(-3, -6));
                        PL.Add(new Point(-3, 2));
                        PL.Add(new Point(2, 10));
                        PL.Add(new Point(2, -8));
                        PL.Add(new Point(-3, 6));
                        PL.Add(new Point(2, 0));
                        PL.Add(new Point(2, -4));
                        PL.Add(new Point(-3, 0));
                        PL.Add(new Point(-3, 8));
                        PL.Add(new Point(-2, -2));
                        PL.Add(new Point(-2, 2));
                        PL.Add(new Point(1, -2));
                        PL.Add(new Point(-2, 0));
                        PL.Add(new Point(1, -6));
                        PL.Add(new Point(-2, 12));
                        PL.Add(new Point(1, 8));
                        PL.Add(new Point(1, -4));
                        PL.Add(new Point(-2, -10));
                        PL.Add(new Point(-2, -8));
                        PL.Add(new Point(-2, 6));
                        PL.Add(new Point(1, -8));
                        PL.Add(new Point(-2, 4));
                        PL.Add(new Point(1, -10));
                        PL.Add(new Point(1, 4));
                        PL.Add(new Point(-2, -6));
                        PL.Add(new Point(1, 6));
                        PL.Add(new Point(1, 2));
                        PL.Add(new Point(-2, 8));
                        PL.Add(new Point(-2, 10));
                        PL.Add(new Point(1, 0));
                        PL.Add(new Point(1, 12));
                        PL.Add(new Point(1, 10));
                        PL.Add(new Point(-2, -4));
                        PL.Add(new Point(-1, 12));
                        PL.Add(new Point(0, -6));
                        PL.Add(new Point(0, -4));
                        PL.Add(new Point(-1, 6));
                        PL.Add(new Point(-1, -4));
                        PL.Add(new Point(0, 12));
                        PL.Add(new Point(0, 6));
                        PL.Add(new Point(-1, 10));
                        PL.Add(new Point(0, 4));
                        PL.Add(new Point(-1, -8));
                        PL.Add(new Point(-1, -10));
                        PL.Add(new Point(-1, 2));
                        PL.Add(new Point(-1, 8));
                        PL.Add(new Point(0, 8));
                        PL.Add(new Point(-1, -6));
                        PL.Add(new Point(0, -10));
                        PL.Add(new Point(-1, 0));
                        PL.Add(new Point(-1, 4));
                        PL.Add(new Point(0, 2));
                        PL.Add(new Point(-1, -2));
                        PL.Add(new Point(0, -2));
                        PL.Add(new Point(0, 0));
                        PL.Add(new Point(0, -8));
                        PL.Add(new Point(0, 10));
                        PL.Add(new Point(-8, -7));
                        PL.Add(new Point(-7, 1));
                        PL.Add(new Point(6, 7));
                        PL.Add(new Point(-1, -7));
                        PL.Add(new Point(-6, 11));
                        PL.Add(new Point(-4, 1));
                        PL.Add(new Point(-10, 3));
                        PL.Add(new Point(-3, -5));
                        PL.Add(new Point(-11, 9));
                        PL.Add(new Point(0, -11));
                        PL.Add(new Point(-9, 11));
                        PL.Add(new Point(5, 9));
                        PL.Add(new Point(0, 3));
                        PL.Add(new Point(-4, -1));
                        PL.Add(new Point(1, 11));
                        PL.Add(new Point(-3, 9));
                        PL.Add(new Point(10, 9));
                        PL.Add(new Point(-8, 3));
                        PL.Add(new Point(-2, 1));
                        PL.Add(new Point(-2, 11));
                        PL.Add(new Point(-7, 5));
                        PL.Add(new Point(2, 1));
                        PL.Add(new Point(-11, -1));
                        PL.Add(new Point(8, 11));
                        PL.Add(new Point(-2, 3));
                        PL.Add(new Point(-6, -3));
                        PL.Add(new Point(-5, -5));
                        PL.Add(new Point(-5, -11));
                        PL.Add(new Point(8, -3));
                        PL.Add(new Point(3, 11));
                        PL.Add(new Point(-5, -1));
                        PL.Add(new Point(1, -3));
                        PL.Add(new Point(-8, 9));
                        PL.Add(new Point(0, 7));
                        PL.Add(new Point(5, -9));
                        PL.Add(new Point(4, -1));
                        PL.Add(new Point(-1, -1));
                        PL.Add(new Point(-11, -3));
                        PL.Add(new Point(-11, 11));
                        PL.Add(new Point(5, 7));
                        PL.Add(new Point(4, 3));
                        PL.Add(new Point(-4, 7));
                        PL.Add(new Point(7, -1));
                        PL.Add(new Point(4, -11));
                        PL.Add(new Point(9, 11));
                        PL.Add(new Point(-9, -1));
                        PL.Add(new Point(-1, 11));
                        PL.Add(new Point(-2, -7));
                        PL.Add(new Point(1, 3));
                        PL.Add(new Point(-3, -1));
                        PL.Add(new Point(-4, -7));
                        PL.Add(new Point(-10, -7));
                        PL.Add(new Point(-1, -5));
                        PL.Add(new Point(-6, -9));
                        PL.Add(new Point(4, -9));
                        PL.Add(new Point(-6, -1));
                        PL.Add(new Point(-8, -11));
                        PL.Add(new Point(-9, 3));
                        PL.Add(new Point(-7, -5));
                        PL.Add(new Point(0, 1));
                        PL.Add(new Point(-6, -5));
                        PL.Add(new Point(-10, 5));
                        PL.Add(new Point(9, 5));
                        PL.Add(new Point(-5, 11));
                        PL.Add(new Point(-11, -7));
                        PL.Add(new Point(8, -5));
                        PL.Add(new Point(9, -1));
                        PL.Add(new Point(-9, 7));
                        PL.Add(new Point(8, -9));
                        PL.Add(new Point(10, -5));
                        PL.Add(new Point(10, 1));
                        PL.Add(new Point(-9, 9));
                        PL.Add(new Point(-7, -1));
                        PL.Add(new Point(2, -5));
                        PL.Add(new Point(6, 9));
                        PL.Add(new Point(5, -7));
                        PL.Add(new Point(-3, 1));
                        PL.Add(new Point(2, -1));
                        PL.Add(new Point(-5, 5));
                        PL.Add(new Point(-6, -11));
                        PL.Add(new Point(-8, 1));
                        PL.Add(new Point(-9, -5));
                        PL.Add(new Point(0, 5));
                        PL.Add(new Point(0, -9));
                        PL.Add(new Point(5, 11));
                        PL.Add(new Point(4, 1));
                        PL.Add(new Point(-3, -9));
                        PL.Add(new Point(-6, 9));
                        PL.Add(new Point(2, -9));
                        PL.Add(new Point(-4, 3));
                        PL.Add(new Point(-1, 7));
                        PL.Add(new Point(-11, 7));
                        PL.Add(new Point(-2, -9));
                        PL.Add(new Point(-1, -11));
                        PL.Add(new Point(-2, 5));
                        PL.Add(new Point(3, 1));
                        PL.Add(new Point(-9, 1));
                        PL.Add(new Point(-9, -11));
                        PL.Add(new Point(-3, -3));
                        PL.Add(new Point(2, -3));
                        PL.Add(new Point(-1, 5));
                        PL.Add(new Point(3, -5));
                        PL.Add(new Point(0, 9));
                        PL.Add(new Point(0, -7));
                        PL.Add(new Point(6, 11));
                        PL.Add(new Point(2, -7));
                        PL.Add(new Point(-6, 7));
                        PL.Add(new Point(7, 9));
                        PL.Add(new Point(-7, -3));
                        PL.Add(new Point(6, -5));
                        PL.Add(new Point(10, 11));
                        PL.Add(new Point(-6, 3));
                        PL.Add(new Point(1, -5));
                        PL.Add(new Point(-11, -9));
                        PL.Add(new Point(10, 3));
                        PL.Add(new Point(5, 5));
                        PL.Add(new Point(7, 11));
                        PL.Add(new Point(-10, -3));
                        PL.Add(new Point(7, 5));
                        PL.Add(new Point(-5, 7));
                        PL.Add(new Point(1, 1));
                        PL.Add(new Point(7, 3));
                        PL.Add(new Point(9, 7));
                        PL.Add(new Point(-5, -3));
                        PL.Add(new Point(-8, 11));
                        PL.Add(new Point(-5, -9));
                        PL.Add(new Point(1, -11));
                        PL.Add(new Point(-2, -11));
                        PL.Add(new Point(9, 3));
                        PL.Add(new Point(3, 7));
                        PL.Add(new Point(3, -1));
                        PL.Add(new Point(-7, -11));
                        PL.Add(new Point(-5, 9));
                        PL.Add(new Point(-4, 9));
                        PL.Add(new Point(-3, 7));
                        PL.Add(new Point(10, -9));
                        PL.Add(new Point(2, 5));
                        PL.Add(new Point(-4, 11));
                        PL.Add(new Point(-10, -5));
                        PL.Add(new Point(-7, -9));
                        PL.Add(new Point(5, 1));
                        PL.Add(new Point(-3, 11));
                        PL.Add(new Point(-4, -5));
                        PL.Add(new Point(0, -3));
                        PL.Add(new Point(-1, -9));
                        PL.Add(new Point(9, -7));
                        PL.Add(new Point(5, -3));
                        PL.Add(new Point(4, -5));
                        PL.Add(new Point(8, 7));
                        PL.Add(new Point(-2, -5));
                        PL.Add(new Point(-6, -7));
                        PL.Add(new Point(0, 11));
                        PL.Add(new Point(-4, -11));
                        PL.Add(new Point(-5, 1));
                        PL.Add(new Point(5, 3));
                        PL.Add(new Point(0, -5));
                        PL.Add(new Point(9, 1));
                        PL.Add(new Point(8, 3));
                        PL.Add(new Point(-3, 5));
                        PL.Add(new Point(-10, 7));
                        PL.Add(new Point(6, -3));
                        PL.Add(new Point(-2, 7));
                        PL.Add(new Point(-9, 5));
                        PL.Add(new Point(1, -7));
                        PL.Add(new Point(-1, 9));
                        PL.Add(new Point(7, 7));
                        PL.Add(new Point(-5, 3));
                        PL.Add(new Point(9, 9));
                        PL.Add(new Point(4, 11));
                        PL.Add(new Point(3, 3));
                        PL.Add(new Point(-8, -3));
                        PL.Add(new Point(6, -7));
                        PL.Add(new Point(-11, 5));
                        PL.Add(new Point(-7, 9));
                        PL.Add(new Point(2, 3));
                        PL.Add(new Point(-3, -7));
                        PL.Add(new Point(8, 1));
                        PL.Add(new Point(-7, -7));
                        PL.Add(new Point(-1, -3));
                        PL.Add(new Point(-5, -7));
                        PL.Add(new Point(10, -7));
                        PL.Add(new Point(8, 5));
                        PL.Add(new Point(-10, -9));
                        PL.Add(new Point(4, -7));
                        PL.Add(new Point(6, -1));
                        PL.Add(new Point(0, -1));
                        PL.Add(new Point(4, 9));
                        PL.Add(new Point(6, 5));
                        PL.Add(new Point(-1, 3));
                        PL.Add(new Point(6, -11));
                        PL.Add(new Point(-10, -1));
                        PL.Add(new Point(1, 9));
                        PL.Add(new Point(1, -9));
                        PL.Add(new Point(8, 9));
                        PL.Add(new Point(-11, 3));
                        PL.Add(new Point(9, -11));
                        PL.Add(new Point(6, 1));
                        PL.Add(new Point(3, -3));
                        PL.Add(new Point(-8, -1));
                        PL.Add(new Point(-11, -11));
                        PL.Add(new Point(10, 7));
                        PL.Add(new Point(-11, -5));
                        PL.Add(new Point(3, 5));
                        PL.Add(new Point(-9, -3));
                        PL.Add(new Point(3, 9));
                        PL.Add(new Point(5, -5));
                        PL.Add(new Point(5, -11));
                        PL.Add(new Point(-4, -3));
                        PL.Add(new Point(-3, 3));
                        PL.Add(new Point(8, -1));
                        PL.Add(new Point(10, 5));
                        PL.Add(new Point(2, 7));
                        PL.Add(new Point(7, -11));
                        PL.Add(new Point(-11, 1));
                        PL.Add(new Point(-6, 5));
                        PL.Add(new Point(3, -11));
                        PL.Add(new Point(-3, -11));
                        PL.Add(new Point(1, 7));
                        PL.Add(new Point(5, -1));
                        PL.Add(new Point(-4, 5));
                        PL.Add(new Point(9, -5));
                        PL.Add(new Point(-7, 11));
                        PL.Add(new Point(-9, -7));
                        PL.Add(new Point(-1, 1));
                        PL.Add(new Point(6, -9));
                        PL.Add(new Point(10, -11));
                        PL.Add(new Point(-10, 1));
                        PL.Add(new Point(7, -5));
                        PL.Add(new Point(8, -11));
                        PL.Add(new Point(4, 7));
                        PL.Add(new Point(6, 3));
                        PL.Add(new Point(2, 9));
                        PL.Add(new Point(-10, 11));
                        PL.Add(new Point(-2, -1));
                        PL.Add(new Point(-10, -11));
                        PL.Add(new Point(-6, 1));
                        PL.Add(new Point(10, -3));
                        PL.Add(new Point(1, -1));
                        PL.Add(new Point(4, 5));
                        PL.Add(new Point(9, -9));
                        PL.Add(new Point(-2, 9));
                        PL.Add(new Point(3, -9));
                        PL.Add(new Point(7, -9));
                        PL.Add(new Point(7, -3));
                        PL.Add(new Point(-8, -5));
                        PL.Add(new Point(-8, 7));
                        PL.Add(new Point(-8, -9));
                        PL.Add(new Point(-7, 3));
                        PL.Add(new Point(10, -1));
                        PL.Add(new Point(2, 11));
                        PL.Add(new Point(-4, -9));
                        PL.Add(new Point(7, 1));
                        PL.Add(new Point(8, -7));
                        PL.Add(new Point(-7, 7));
                        PL.Add(new Point(-8, 5));
                        PL.Add(new Point(-10, 9));
                        PL.Add(new Point(3, -7));
                        PL.Add(new Point(9, -3));
                        PL.Add(new Point(1, 5));
                        PL.Add(new Point(-9, -9));
                        PL.Add(new Point(7, -7));
                        PL.Add(new Point(-2, -3));
                        PL.Add(new Point(2, -11));
                        PL.Add(new Point(4, -3));
                    }
                }
                return _MaskSkeletonOrder;
            }
        }

        public static void SaveMask(List<MaskListEntry> Cells, string FullPath, SVP_Registration_Params SVRP)
        {
            int MskImg_toChop = 0; //This used to be a setting, but now that we shift the masks, this has been removed 6/28/2022
            if (SVRP.MskImg_Final_Leica < 100) SVRP.MskImg_Final_Leica = 1024; //Just in case the settings got messed up

            Bitmap BMP = DrawMask(Cells, SVRP.MskImg_InCell_Width, SVRP.CropForLeica ? (SVRP.MskImg_Final_Leica + MskImg_toChop * 2) : SVRP.MskImg_Final_Leica, SVRP.Mask_PixelsExpand);
            if (SVRP.CropForLeica) BMP = BMP.Clone(new Rectangle(MskImg_toChop, MskImg_toChop, SVRP.MskImg_Final_Leica, SVRP.MskImg_Final_Leica), BMP.PixelFormat);
            if (SVRP.Rotate180) BMP.RotateFlip(RotateFlipType.Rotate180FlipNone);

            //Figure out how to save the mask
            int nGray = Cells.Count(x => MaskList.CheckStringAgainst_IntensityType(x.Intensity_Method, MaskIntensityType.Grayscale));
            if (nGray > 0)
                Save8BitTiff(BMP, FullPath);
            else
                Save1BitTiff(BMP, FullPath);
        }

        public static XmlDocument ExtractXmp()
        {
            //This may or may not work, untested as below . . probably don't need it now
            byte[] jpegBytes = null;
            var asString = System.Text.Encoding.UTF8.GetString(jpegBytes);
            var start = asString.IndexOf("<x:xmpmeta");
            var end = asString.IndexOf("</x:xmpmeta>") + 12;
            if (start == -1 || end == -1)
                return null;
            var justTheMeta = asString.Substring(start, end - start);
            var returnVal = new XmlDocument();
            returnVal.LoadXml(justTheMeta);
            return returnVal;
        }

        public static void Metadata()
        {


            //https://stackoverflow.com/questions/2280948/reading-data-metadata-from-jpeg-xmp-or-exif-in-c-sharp
            //string FullName = "";
            //using (FileStream fs = new FileStream(FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
            //{
            //    BitmapSource img = BitmapFrame.Create(fs);
            //    BitmapMetadata md = (BitmapMetadata)img.Metadata;
            //    string date = md.DateTaken;
            //    Console.WriteLine(date);
            //    return date;
            //}
        }

        public static void Save1BitTiff(Bitmap img, string SavePath)
        {
            bool Make1Bit = true;
            if (Make1Bit)
            {
                ImageCodecInfo TiffCodec = null;
                foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageEncoders())
                    if (codec.MimeType == "image/tiff")
                    {
                        TiffCodec = codec;
                        break;
                    }
                EncoderParameters parameters = new EncoderParameters(2);
                parameters.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, (long)1);
                parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionNone);
                img.Save(SavePath, TiffCodec, parameters);
            }
            else
            {
                img.Save(SavePath);
            }
            img.Dispose();
        }

        public static void Save8BitTiff(Bitmap img, string SavePath)
        {
            ImageCodecInfo TiffCodec = null;
            foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageEncoders())
                if (codec.MimeType == "image/tiff")
                {
                    TiffCodec = codec;
                    break;
                }
            EncoderParameters parameters = new EncoderParameters(2);
            parameters.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, (long)8);
            parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionNone);
            img.Save(SavePath, TiffCodec, parameters);
            img.Dispose();
        }

        public static void SaveMasks(MaskList Masks, string directoryName, SVP_Registration_Params SRVP, System.ComponentModel.BackgroundWorker BW = null)
        {
            int Counter = 0; int Count = Masks.WellFieldDict.Count;
            foreach (KeyValuePair<string, List<MaskListEntry>> WellField in Masks.WellFieldDict)
            {
                SaveMask(WellField.Value, Path.Combine(directoryName, Path.GetFileNameWithoutExtension(WellField.Value[0].FileName) + ".TIF"), SRVP);
                if (BW != null)
                {
                    string t = Counter.ToString() + "/" + Count.ToString() + " = " + (100 * Counter / Count).ToString("0.0");
                    if (Counter++ % 10 == 0) BW.ReportProgress(0, t);
                }
            }
        }
    }

    public static class CellZoom
    {
        public static void ZoomTest()
        {
            Debugger.Break();
            string Folder = @"K:\RaftExport\FIV471A3_1\A\";
            DirectoryInfo DI = new DirectoryInfo(Folder);
            SizeF SizeSave = new SizeF(120, 120);
            Parallel.ForEach(DI.GetFiles("*.bmp"), file => { Zoom_Resave(file.FullName, "", 0, SizeSave); });
        }

        public static void Zoom_Resave(string SourceImage, string DestFullName = "", float BufferPix = 20, SizeF SaveSize = default(SizeF))
        {
            Bitmap BMP, BMP2;
            bool ExportSegImage = false;

            //Get the bitmap, Threshold, and segment the image to find the cells
            BMP = new Bitmap(SourceImage);
            Seg.SegObjects SOs = Zoom_Internal_SO(BMP);
            if (SOs.Count < 1 || SOs.Count > 2) return;

            //Make the destination if it doesn't exist
            if (DestFullName == "") DestFullName = DeriveNewDestImageName(SourceImage/*,  SOs.First().Area_Approx.ToString("00000") + "_"*/);

            if (ExportSegImage) { BMP2 = MasksHelper.DrawMask(SOs, BMP.Width, BMP.Width); BMP2.Save(SourceImage.Replace(".", ".Seg.")); }

            //Crop out the interesting area
            if (SaveSize.IsEmpty)
            {
                //Do any size based on the bounding box
                BMP2 = BMP.Clone(SOs.BoundingRect(BufferPix, BMP.Width, BMP.Height), BMP.PixelFormat);
            }
            else
            {
                //Do the central method and restrict the size
                RectangleF R = SOs.BoundingRect(BufferPix, BMP.Width, BMP.Height);
                float XDelta = -(R.Width - SaveSize.Width) / 2;
                float YDelta = -(R.Height - SaveSize.Height) / 2;
                float NewX = Math.Max(0, R.Left - XDelta);
                float NewY = Math.Max(0, R.Top - YDelta);
                if ((NewX + SaveSize.Width) > BMP.Width) NewX = BMP.Width - SaveSize.Width;
                if ((NewY + SaveSize.Height) > BMP.Height) NewY = BMP.Height - SaveSize.Height;
                RectangleF R2 = new RectangleF(new PointF(NewX, NewY), SaveSize);
                try
                {
                    BMP2 = BMP.Clone(R2, BMP.PixelFormat);
                }
                catch
                {
                    BMP2 = null;
                }
            }
            if (BMP2 != null) BMP2.Save(DestFullName);

            //Let go of the Bitmaps
            BMP.Dispose();
            if (BMP2 != null) BMP2.Dispose();
        }

        private static string DeriveNewDestImageName(string SourceImage, string Prefix = "")
        {
            string DestFullName;
            FileInfo FI = new FileInfo(SourceImage);
            DirectoryInfo DINew = new DirectoryInfo(Path.Combine(FI.Directory.Parent.FullName, "Zoomed"));
            if (!DINew.Exists) DINew.Create();
            DestFullName = Path.Combine(DINew.FullName, Prefix + FI.Name);
            return DestFullName;
        }

        private static Seg.SegObjects Zoom_Internal_SO(Bitmap BMP, float MinRatio = 0.3F, float MinNuclearArea = 400, float MaxNuclearArea = 20000)
        {
            Seg.ThreshImage TI = new Seg.ThreshImage(BMP);
            Seg.SegObjects SOs = Seg.SegObjects.ObjectsFromImage(TI);
            SOs.RemoveStickNuclei(MinRatio); //Get rid of some artifacts
            int sizeBefore = SOs.Count;
            SOs.RemoveNucleiBySize(MinNuclearArea, MaxNuclearArea); //Get rid of really small or really large nuclei
            if (SOs.Count < sizeBefore)
            {

            }
            return SOs;
        }
    }
}
