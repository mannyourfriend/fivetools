﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Net;
using FIVE.InCellLibrary;
using System.Xml.Serialization;
using System.Diagnostics;

namespace FIVE_IMG.PL
{
    public static class PLDeployModule
    {
        public static void AA_TestRun()
        {
            //string Path = @"R:\FIVE\EXP\FIV565\Training Images"; //Older from 9/2021 ish

            // - - This works 1/5/2022
            //string ImagesPath = @"K:\_Training\Leica MFN2\MU 1\";
            //MLSettings_PL Settings = MLSettings_PL.SetupStartPort(@"e:\PLModel\LeicaMFN2_LargeSet", "8038");

            // - - Purva's test model "Y"
            string ImagesPath = @"R:\People\Purva_Patel\PLTest\Images\TestY\";
            MLSettings_PL Settings = MLSettings_PL.SetupStartPort(@"E:\PLModel\TestY_TF", "8038", true);
            Settings.SearchString = "*.jpg";
            PredictOnFolder(ImagesPath, Settings);
        }

        public static short[][][][] Convert2Array(string ImagePath)
        {
            ImgWrap IW = new ImgWrap(new Bitmap(ImagePath));
            return IW.ArrayForPL();
        }

        public static short[][][][] Convert2Array(Bitmap Image)
        {
            ImgWrap IW = new ImgWrap(Image);
            return IW.ArrayForPL();
        }


        public static void Create_CSV(string csvFile, MLSettings_PL Settings)
        {
            char delim = ',';
            using (StreamWriter sr = new StreamWriter(csvFile))
            {
                sr.WriteLine("File" + delim + "ImageDirectory" + delim + "PredictedClass" + delim + Settings.mapping[0] + " Score", +delim + Settings.mapping[1] + " Score");
                //sr.WriteLine(myFile + delim + imgDir + delim + predStr + delim + scoresSplit[0] + delim + scoresSplit[1]);
            }
        }

        public static TF_Results PredictOnImage(string ImagePath, MLSettings_PL Settings)
        {
            short[][][][] imageSeq = Convert2Array(ImagePath);
            return PredictOnImage_Internal(imageSeq, Settings);
        }

        public static TF_Results PredictOnImage(Bitmap Image, MLSettings_PL Settings)
        {
            short[][][][] imageSeq = Convert2Array(Image);
            return PredictOnImage_Internal(imageSeq, Settings);
        }

        public static TF_Results PredictOnImage_Internal(short[][][][] imageSeq, MLSettings_PL Settings)
        {
            var TFR = new TF_Results();
            string data = JsonSerializer.Serialize(new Dictionary<string, object> { { "signature_name", "serving_default" }, { "instances", imageSeq } });
            //File.WriteAllText(@"e:\temp\json.txt", data);
            string json_response = PostRequest(Settings.URL, data);

            //string data2 = File.ReadAllText(@"R:\FIVE\EXP\FIV567\PerceptiLabs Deployment\json_data.txt"); //string json_Resp2 = PostRequest(postURL, data2, headers);
            string e1 = "PREDICTION";
            if (json_response.ToUpper().Contains(e1))
            {
                try
                {
                    var ret = (Dictionary<string, double[][]>)JsonSerializer.Deserialize(json_response, typeof(Dictionary<string, double[][]>));
                    TFR.textRes = "";
                    TFR.numericRes = ret.Values.First()[0];
                }
                catch
                {
                    TFR.numericRes = new double[0];
                    string tZ = json_response.Substring(json_response.ToUpper().IndexOf(e1) + e1.Length);
                    tZ = tZ.Substring(tZ.IndexOf(":") + 1).Trim();
                    tZ = tZ.Substring(tZ.IndexOf("\"") + 1).Trim();
                    TFR.textRes = tZ.Substring(0, tZ.IndexOf("\""));
                }
            }
            return TFR;
        }

        internal static void Predict_OnTheFly(INCELL_Folder iC_Folder)
        {
            //Basically trigger segmentation on these

        }

        public static Dictionary<string, TF_Results> PredictOnFolder(string FolderPath, MLSettings_PL Settings, System.ComponentModel.BackgroundWorker BW = null)
        {
            var DI = new DirectoryInfo(FolderPath);
            var Result = new Dictionary<string, TF_Results>();
            int counter = 0;
            DateTime Start = DateTime.Now;
            //StringBuilder sB = new StringBuilder();
            FileInfo[] Files = DI.GetFiles(Settings.SearchString);
            if (Files.Length == 0)
            {
                if (BW != null) BW.ReportProgress(0, "!! No files found, try a different extension.");
                return Result;
            }
            foreach (FileInfo file in Files)
            {
                if (counter % 10 == 0) if (BW != null) { BW.ReportProgress(0, file.Name + " " + counter); if (BW.CancellationPending) break; }
                Result.Add(file.Name, PredictOnImage(file.FullName, Settings));
                counter++;
            }
            double millis = (DateTime.Now - Start).TotalMilliseconds;
            string T = (millis / counter).ToString("0.0");
            if (BW != null) BW.ReportProgress(0, T + " milli seconds per image");
            return Result;
        }

        public static string PostRequest(string URL, string data, string headers = "application/json", string Method = "POST")
        {
            //https://stackoverflow.com/questions/9145667/how-to-post-json-to-a-server-using-c
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
            httpWebRequest.ContentType = headers; httpWebRequest.Method = Method;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream())) { streamWriter.Write(data); }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string result = streamReader.ReadToEnd();
                    return result;
                }
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
    }

    public class TF_Results
    {
        public double[] numericRes;
        public string textRes;
    }

    public class DockerContainer
    {
        public string JSON;
        public string Port;
        public string Error;
        public string Image;
        public string Name;
        public string ContainerID;
        public string Hostname;
        public string ModelPath;
        public string Status;

        public DockerContainer()
        {
            Error = "Not Setup";
        }

        public DockerContainer(string JSON_from_DockerInspect, string ContainerID_FromPS)
        {
            JSON = JSON_from_DockerInspect;
            Port = FromJSON("HostPort");
            Name = FromJSON("MODEL_NAME");
            Image = FromJSON("Image");
            Status = FromJSON("Status");
            ContainerID = ContainerID_FromPS;
            Hostname = FromJSON("Hostname");
            ModelPath = FromJSON("Source").Replace("\\\\", "\\");

            Error = "";
        }

        private string FromJSON(string Tag)
        {
            int l1 = JSON.IndexOf(Tag);
            if (l1 < 2) return "";
            int start = l1 + Tag.Length;
            int l2 = JSON.IndexOf(":", start);
            int l3 = JSON.IndexOf("=", start);
            int valstart = Math.Min(l2, l3) + 1;
            int valend = JSON.IndexOf("\"", valstart + 2);
            string t = JSON.Substring(valstart, valend - valstart);
            t = t.Trim().Replace("\"", "");
            return t;
        }

        public string Delete()
        {
            //https://docs.docker.com/engine/reference/commandline/rm/
            string output;
            using (Process p = new Process())
            {
                p.StartInfo.FileName = "docker"; p.StartInfo.Arguments = "rm -f " + ContainerID;
                p.StartInfo.UseShellExecute = false; p.StartInfo.RedirectStandardOutput = true; p.StartInfo.CreateNoWindow = true;
                p.Start();
                output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            }
            return output;
        }

        public static Dictionary<string, DockerContainer> GetAllContainers()
        {
            Dictionary<string, DockerContainer> Ports_Containers = new Dictionary<string, DockerContainer>();
            string[] pIDs;
            pIDs = GetDockerContainerIDs();

            foreach (var pID in pIDs)
            {
                if (pID == "") continue;
                string tJson = InspectDockerContainer(pID);
                DockerContainer DC = new DockerContainer(tJson, pID);
                if (DC.Error == "") Ports_Containers.Add(DC.Port, DC);
            }

            return Ports_Containers;
        }

        public static string[] GetDockerContainerIDs()
        {
            //
            string[] pIDs;
            using (Process p = new Process())
            {
                p.StartInfo.FileName = "docker"; p.StartInfo.Arguments = "ps -q";
                p.StartInfo.UseShellExecute = false; p.StartInfo.RedirectStandardOutput = true; p.StartInfo.CreateNoWindow = true;
                p.Start();
                string output = p.StandardOutput.ReadToEnd();
                pIDs = output.Split('\n');
                p.WaitForExit();
            }
            return pIDs;
        }

        public static string InspectDockerContainer(string ContainerID)
        {
            //https://docs.docker.com/engine/reference/commandline/inspect/
            string output;
            using (Process p = new Process())
            {
                p.StartInfo.FileName = "docker"; p.StartInfo.Arguments = "inspect " + ContainerID;
                p.StartInfo.UseShellExecute = false; p.StartInfo.RedirectStandardOutput = true; p.StartInfo.CreateNoWindow = true;
                p.Start();
                output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            }
            return output;
        }

       
    }

    public class MLSettings_PL
    {
        public List<string> DockerStartCommands;
        public string URL;
        public int BurstSize;
        public string SearchString;
        public bool MoveImageToSubfolder;
        public string ImageFolder;
        public string ResultFolder;

        public string ModelName;

        [XmlIgnore]
        public Dictionary<int, string> mapping;

        public MLSettings_PL()
        {
            mapping = new Dictionary<int, string>();
            DockerStartCommands = new List<string>();
            ModelName = "_UNK";
        }

        public MLSettings_PL(bool LoadDefaults)
        {
            mapping = new Dictionary<int, string>();
            DockerStartCommands = new List<string>();
            SearchString = "*.tiff";
            if (!LoadDefaults) return;
            ModelName = "LeicaMFN2_LargeSet";
            DockerStartCommands.Add(@"docker pull tensorflow/serving");
            DockerStartCommands.Add(@"docker run -p 8038:8501 --mount type=bind,source=e:/PLModel/LeicaMFN2_LargeSet,target=/models/LeicaMFN2_LargeSet -e MODEL_NAME=LeicaMFN2_LargeSet -t tensorflow/serving");
            URL = "http://localhost:8038/v1/models/LeicaMFN2_LargeSet:predict";
            mapping = new Dictionary<int, string> { { 0, "WT" }, { 1, "MU" } };
        }

        public string DockerResult;

        public static MLSettings_PL SetupStartPort(string ModelExportPath, string Port, bool Start, string modelName = "", string version = "1")
        {
            var NewSettings = new MLSettings_PL();
            NewSettings.DockerResult = "Error setting up paths (check model path).";

            if (modelName == "") modelName = new DirectoryInfo(ModelExportPath).Name;
            NewSettings.ModelName = modelName;
            NewSettings.DockerStartCommands.Add(@"pull tensorflow/serving"); //This is installed just once, so we can skip running it here
            NewSettings.DockerStartCommands.Add(@"run -p " + Port + ":8501 --mount type=bind,source=" +
                ModelExportPath + ",target=/models/" + modelName + " -e MODEL_NAME=" +
                modelName + " -t tensorflow/serving");

            if (Start)
            {
                NewSettings.DockerResult = "Error running docker commands.";
                ProcessStartInfo startInfo = new ProcessStartInfo("docker", NewSettings.DockerStartCommands[1]);
                startInfo.CreateNoWindow = true;
                startInfo.UseShellExecute = false;
                Process.Start(startInfo);
            }

            NewSettings.URL = "http://localhost:" + Port + "/v" + version + "/models/" + modelName + ":predict";
            NewSettings.DockerResult = "Docker container should be running.";
            return NewSettings;
        }
    }

}
