﻿using FIVE.InCellLibrary;
using FIVE_IMG.PL;
using FIVE_IMG.Plate_DB_NS;
using ImageMagick;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FIVE_IMG
{
    //https://stackoverflow.com/questions/49904033/crosscorrelation-of-images

    public class CrossCorr
    {
        public static void BBB_Test()
        {
            string Pth1 = @"c:\temp\test3.tif";
            string Pth2 = @"c:\temp\test3 N1R1.tif";
            Rectangle R = new Rectangle(90, 280, 110, 134);
            Bitmap MainImage = new Bitmap(Pth1);
            Bitmap DegImage = new Bitmap(Pth2);
            Bitmap SubImage = DegImage.Clone(R, MainImage.PixelFormat);
            SubImage.Save(Pth2 + " sm.tif");

            //Run the registration
            Register_Return RR = Register(new ImgWrap(SubImage), new ImgWrap(MainImage));
        }

        public static Register_Return Register(ImgWrap FindThis, ImgWrap AmongThat)
        {
            var c = new CrossCorr();
            var RR = new Register_Return(AmongThat, FindThis); //Starts the clock
            int initialStride1 = 9, initialStride2 = 3; double score;

            //This is happening in three stages currently.
            //First, we look using a large stride (how many pixels to skip) on both the source and the destination.
            //This should be at maximum ~half the size of your objects, then we go to a small number of the best alignments and refine them further

            score = c.Search(FindThis, AmongThat, initialStride1, (int)Math.Ceiling((double)1 + initialStride1 / 2));
            RR.Notes += "Round1=" + score + "\r\n"; RR.Score_Worst = score;

            score = c.SearchNear(FindThis, AmongThat, 0.005, 4, initialStride1 + 1, initialStride2, initialStride2); RR.Notes += "Round2=" + score + "\r\n";
            score = c.SearchNear(FindThis, AmongThat, 0.0005, 2, initialStride2 + 1, 1, 1); RR.Notes += "Round3=" + score + "\r\n";

            RR.FinalR2 = -score / FindThis.TotalPixels; RR.Score_Final = score;
            RR.Location = c.Location();
            RR.Finish(); //Stops the clock
            return RR;
        }

        public PointF Location()
        {
            return new PointF(SortedCorrs.Values[0][0].X, SortedCorrs.Values[0][0].Y);
        }

        public double SearchNear(ImgWrap findThis, ImgWrap amongThat, double topPercentile, int MaxSub, int searchSize, int stride, int internalStride)
        {
            //Go through the best one(s), this part of the code just picks the best ones so far
            int total = (int)Math.Max(1, Math.Min(topPercentile * SortedCorrs.Count, MaxSub));
            int x, y; double Last = 0; List<Point> ToSearch = new List<Point>(total);
            for (int i = 0; i < total; i++)
            {
                for (int j = 0; j < SortedCorrs.Values[i].Count; j++)
                {
                    ToSearch.Add(SortedCorrs.Values[i][j]);
                }
            }
            //Now see how well they match
            for (int i = 0; i < ToSearch.Count; i++)
            {
                x = ToSearch[i].X; y = ToSearch[i].Y;
                Last = Search(findThis, amongThat, stride, internalStride, x - searchSize, y - searchSize, x + searchSize, y + searchSize);
            }
            return Last;
        }

        public double Search(ImgWrap FindThis, ImgWrap AmongThat, int stride, int internalstride)
        {
            return Search(FindThis, AmongThat, stride, internalstride, 0, 0, AmongThat.BMP.Width - FindThis.BMP.Width, AmongThat.BMP.Height - FindThis.BMP.Height);
        }

        public double Search(ImgWrap FindThis, ImgWrap AmongThat, int stride, int internalstride, int startX, int startY, int lastX, int lastY)
        {
            //TODO: Parallelize this or figure out the conditions that make it worth parallelizing
            double corr; Point p;
            for (int x = Math.Max(0, startX); x < Math.Min(AmongThat.Width - FindThis.Width, lastX); x += stride)
            {
                for (int y = Math.Max(0, startY); y < Math.Min(AmongThat.Height - FindThis.Height, lastY); y += stride)
                {
                    p = new Point(x, y); if (TriedList.Contains(p)) continue; //Don't get this point twice
                    corr = XCOR(FindThis, AmongThat, x, y, internalstride);
                    Add(p, corr);
                }
            }
            return SortedCorrs.Keys[0];
        }


        /// <summary>
        /// Cross Correlation of two Images
        /// </summary>
        /// <param name="stride">How many pixels to advance on each try (1) is minimum</param>
        /// <returns></returns>
        private static double XCOR(ImgWrap findThis, ImgWrap amongThat, int x, int y, int stride)
        {
            double Sum = 0;
            double val1, val2;

            for (int xi = 0; xi < findThis.BMP.Width; xi += stride)
            {
                for (int yi = 0; yi < findThis.BMP.Height; yi += stride)
                {
                    //To get the actual r2 we wouldn't just subtract the mean, but divide by the SD and then mean-center the data, but this should work fine for now
                    val1 = findThis.NormColorAtPoint(xi, yi);
                    val2 = amongThat.NormColorAtPoint(x + xi, y + yi);
                    Sum += val1 * val2;
                }
            }
            return Sum;
        }

        //// -------------------------------

        public SortedList<double, List<Point>> SortedCorrs;
        public HashSet<Point> TriedList;

        public CrossCorr()
        {
            SortedCorrs = new SortedList<double, List<Point>>();
            TriedList = new HashSet<Point>();
        }

        public void Add(Point p, double corr)
        {
            double key = -corr;
            if (!SortedCorrs.ContainsKey(key)) SortedCorrs.Add(key, new List<Point>());
            SortedCorrs[key].Add(p);
            TriedList.Add(p);
        }
    }

    public class Register_Return
    {
        public double PixelSizeSearch { get; set; }
        public double PixelSizeMatch { get; set; }
        public PointF Location { get; set; }

        public PointF ShiftMask_Pixels { get; set; }

        public string Notes { get; set; }
        public double MSEC_Total => (Finished - Start).TotalMilliseconds;

        public double FinalR2 { get; set; }
        public bool Include = true;

        public double MSEC_Pixels_1; //This is how long it takes to load the IMG Wraps (sometimes already loaded)
        public double MSEC_Pixels_2;

        public double Score_Final { get; set; }
        public double Score_AvgInitial { get; set; }
        public double Score_Worst { get; set; }

        public DateTime Start;
        public DateTime Finished;
        public double MSECTotal; //Just so this gets saved out

        public void Finish()
        {
            Finished = DateTime.Now;
            MSECTotal = MSEC_Total; //Just so we don't have to calculate it later
        }

        public Register_Return()
        {
            //For serialization
        }

        public Register_Return(ImgWrap Search, ImgWrap Match)
        {
            Start = DateTime.Now;
            PixelSizeSearch = Search.Width * Search.Height;
            PixelSizeMatch = Match.Width * Match.Height;

            Byte Fake; //Triggers the images to be deconstructed, which takes a little bit of time
            Fake = Search.Arr[0, 0]; MSEC_Pixels_1 = (DateTime.Now - Start).TotalMilliseconds;
            Fake = Match.Arr[0, 0]; MSEC_Pixels_2 = (DateTime.Now - Start).TotalMilliseconds;
        }
    }

    public class ImgWrap
    {
        private Bitmap _BMP;
        public Bitmap BMP
        {
            get
            {
                if (_BMP == null)
                {
                    _BMP = new Bitmap(PathToImage);
                }
                return _BMP;
            }
            set { _BMP = value; }
        }

        public string PathToImage { get; internal set; }

        public int Width { get => BMP.Width; }
        public int Height { get => BMP.Height; }


        private int _RedMin; private int _RedMax; private double _RedMean; private double _RedStDev;
        public int RedMin { get { if (_arr == null) TakeApart(); return _RedMin; } }
        public int RedMax { get { if (_arr == null) TakeApart(); return _RedMax; } }
        public double RedMean { get { if (_arr == null) TakeApart(); return _RedMean; } }
        public double RedStDev { get { if (_arr == null) TakeApart(); return _RedStDev; } }

        public float X_um = float.NaN;
        public float Y_um = float.NaN;
        public PointF Location_um { get => new PointF(X_um, Y_um); }
        public float Z = float.NaN;
        public float umPerpixel = float.NaN;
        public string channel = "";

        private byte[,] _arr;
        public byte[,] Arr
        {
            get
            {
                if (_arr == null)
                {
                    TakeApart();
                    //TakeApart_Fast();
                }
                return _arr;
            }
        }

        public double TotalPixels { get => Width * Height; }
        public float X_umCenter { get => X_um - (Width_um / 2); }
        public float Y_umCenter { get => Y_um - (Height_um / 2); }
        public PointF Center_um { get => new PointF(X_umCenter, Y_umCenter); }
        public float Width_um { get => umPerpixel * Width; }
        public float Height_um { get => umPerpixel * Height; }

        public double NormColorAtPoint(int x, int y)
        {
            return ((Arr[x, y] - RedMean) / RedStDev);
        }

        private string _Name = string.Empty;
        public string Name
        {
            get
            {
                if (_Name == string.Empty) _Name = PathToImage;
                return _Name;
            }
            set { _Name = value; }
        }

        public ImgWrap()
        {
            _BMP = null;
            _arr = null;
        }

        public ImgWrap(Bitmap bitMAP)
        {
            BMP = bitMAP;
        }

        public ImgWrap(string FullFileName, Bitmap bitMAP)
        {
            BMP = bitMAP;
            PathToImage = FullFileName;
        }

        /// <summary>
        /// When using this constructor we try to figure out X Y Z from the filename
        /// </summary>
        /// <param name="PathToImage"></param>
        public ImgWrap(string PathToImage)
        {
            this.PathToImage = PathToImage;
            //_BMP = new Bitmap(PathToImage);
            string extra;
            string tName;
            GetXYZFromName(PathToImage, out tName, out X_um, out Y_um, out Z, out umPerpixel, out channel, out extra);
            Name = tName;
        }

        public static void GetXYZFromName(string PathToImage, out string Name, out float x, out float y, out float z, out float umPerpix, out string chx, out string extra)
        {
            x = y = z = umPerpix = float.NaN; extra = chx = "";
            Name = Path.GetFileNameWithoutExtension(PathToImage);
            string[] Arr = Name.Split('_');
            try
            {
                x = float.Parse(Arr[0]);
                y = float.Parse(Arr[1]);
                z = float.Parse(Arr[2]);
                umPerpix = float.Parse(Arr[3]);
                chx = Arr[4];
                extra = Arr[5];
            }
            catch { }
        }

        public static bool IsImage(string FullPath)
        {
            string Ext = Path.GetExtension(FullPath).ToUpper().Trim();
            if (Ext == ".BMP" || Ext == ".TIF" || Ext == ".TIFF" || Ext == ".GIF" || Ext == ".JPG") return true;
            return false;
        }

        public static string GetMimeType(Image i)
        {
            foreach (ImageCodecInfo codec in System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders())
            {
                if (codec.FormatID == i.RawFormat.Guid)
                    return codec.MimeType;
            }

            return "image/unknown";
        }

        private void TakeApart()
        {
            //On a 2040x2040 image, the Fast Method takes 575 ms (avg of 3), and the slow method takes 2460 ms (avg 2)
            //TakeApart_Fast();
            TakeApart_Much_Faster(BMP, 90);
        }

        private void TakeApart_Slow()
        {
            Bitmap bmap = BMP;
            Color col;
            _RedMin = byte.MaxValue; _RedMax = byte.MinValue;
            _arr = new byte[bmap.Width, bmap.Height];
            double Sum = 0; double SumSq = 0;
            for (int i = 0; i < bmap.Width; i++)
            {
                for (int j = 0; j < bmap.Height; j++)
                {
                    col = bmap.GetPixel(i, j);
                    _arr[i, j] = col.R;
                    Sum += col.R; SumSq += col.R * col.R;
                    if (col.R < _RedMin) _RedMin = col.R;
                    if (col.R > _RedMax) _RedMax = col.R;
                }
            }
            _RedMean = Sum / TotalPixels;
            _RedStDev = (SumSq / TotalPixels) - (_RedMean * _RedMean);
        }

        private void TakeApart_Fast()
        {
            //This doesn't probably work so well with 16-bit TIFFs, but if we have already made them 8 bit it is probably OK
            Bitmap sourceBitmap = BMP;
            BitmapData sourceData = sourceBitmap.LockBits(new Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height),
                                    ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb  /*This will change the result*/);

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];
            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            sourceBitmap.UnlockBits(sourceData);

            //If we just want the initial pixelBuffer, it is very fast, and can be indexed quickly, but getting the Mean, Avg, Min, Max values requires looking through it which is slow

            _RedMin = byte.MaxValue;
            _RedMax = byte.MinValue;
            _arr = new byte[BMP.Width, BMP.Height];

            byte Pix; int x, y; int Offset;
            double Sum = 0, SumSq = 0; byte chs = 3;
            for (y = 0; y < BMP.Height; y++)
            {
                Offset = (y * sourceData.Stride);
                for (x = 0; x < BMP.Width; x++)
                {
                    Pix = pixelBuffer[(x * chs) + Offset];
                    _arr[x, y] = Pix;
                    Sum += Pix; SumSq += Pix * Pix;
                    if (Pix < _RedMin) _RedMin = Pix;
                    if (Pix > _RedMax) _RedMax = Pix;
                }
            }
            _RedMean = Sum / TotalPixels;
            _RedStDev = (SumSq / TotalPixels) - (_RedMean * _RedMean);
        } //15 25 34 14 32 25 - - large : 456 420

        //Parallel Unsafe, Corrected Channel, Corrected Standard div 5x faster (Josh M 11/6/2022)
        private void TakeApart_Much_Faster(Bitmap processedBitmap, float threshold)
        {
            _RedMin = byte.MaxValue;
            _RedMax = byte.MinValue;
            _arr = new byte[BMP.Width, BMP.Height];
            long Sum = 0,
            SumSq = 0;
            BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            //this is a much better datatype than the
            var points = new ConcurrentDictionary<Point, byte>();
            unsafe
            {
                int bytesPerPixel = Image.GetPixelFormatSize(bitmapData.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                _RedMin = byte.MaxValue;
                _RedMax = byte.MinValue;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;
                Parallel.For(0, heightInPixels, y =>
                {
                    //pointer to the first pixel so we don't lose track of where we are
                    byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        //0+2 is red channel
                        byte redPixel = currentLine[x + 2];
                        Interlocked.Add(ref Sum, redPixel);
                        Interlocked.Add(ref SumSq, redPixel * redPixel);
                        //divide by three since we are skipping ahead 3 at a time.
                        _arr[x / 3, y] = redPixel;
                        _RedMin = redPixel < _RedMin ? _RedMin : redPixel;
                        _RedMax = redPixel > RedMax ? RedMax : redPixel;
                    }
                });
                _RedMean = Sum / TotalPixels;
                _RedStDev = Math.Sqrt((SumSq / TotalPixels) - (_RedMean * _RedMean));
                processedBitmap.UnlockBits(bitmapData);
            }
        } // 9 17 20 22 - - -large 275 232 258


        /// <summary>
        /// Returns the middle part of the image where the size of the image is base on the fraction of the original's width
        /// </summary>
        internal ImgWrap MiddleCrop(double FractionOfWidth)
        {
            int Size = (int)(Width * FractionOfWidth);
            int Top = (Height / 2) - (Size / 2);
            int Left = (Width / 2) - (Size / 2);

            Rectangle R = new Rectangle(Top, Left, Size, Size);
            //System.Diagnostics.Debug.Print(R.Location.ToString());

            Bitmap BMPCrop = this.BMP.Clone(R, BMP.PixelFormat);

            ImgWrap IMCrop = new ImgWrap(BMPCrop);
            IMCrop.PathToImage = this.PathToImage;
            IMCrop.X_um = X_um + Left;
            IMCrop.Y_um = Y_um + Top;
            IMCrop.Z = Z;

            return IMCrop;
        }

        public double DistanceFrom(ImgWrap ImageCompare, bool IgnoreZ = true)
        {
            //Can only calculate the distance if we know the distances
            try
            {
                if (IgnoreZ)
                    return Math.Sqrt(Math.Pow(X_um - ImageCompare.X_um, 2) + Math.Pow(Y_um - ImageCompare.Y_um, 2));
                else
                    return Math.Sqrt(Math.Pow(X_um - ImageCompare.X_um, 2) + Math.Pow(Y_um - ImageCompare.Y_um, 2) + Math.Pow(Z - ImageCompare.Z, 2));
            }
            catch
            {
                return double.NaN;
            }
        }

        internal void Delete()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The outer most array is empty, but inside of that is the x,y image and the 3 color channels
        /// </summary>
        /// <returns></returns>
        public short[][][][] ArrayForPL()
        {
            int x = Arr.GetLength(0);
            int y = Arr.GetLength(1);
            short[][][][] tempArray = new short[1][][][];
            tempArray[0] = new short[x][][];
            for (int i = 0; i < x; i++)
            {
                tempArray[0][i] = new short[y][];
                for (int j = 0; j < y; j++)
                {
                    tempArray[0][i][j] = new short[3];
                    tempArray[0][i][j][0] = Arr[j, i];
                    tempArray[0][i][j][1] = Arr[j, i];
                    tempArray[0][i][j][2] = Arr[j, i];
                }
            }
            return tempArray;
        }
    }

    public class MetaMorph_StagePoint
    {
        public PointF XY;
        public double Z;
        public string Name;

        public MetaMorph_StagePoint()
        {

        }

        public MetaMorph_StagePoint(XDCE_Image FOV, float LeicaImageWidth_um, Associate_InCell_Leica Transform)
        {
            this.Name = FOV.FileName;
            this.XY = Transform.LeicaPositionEstimate_FromGEXDCE(FOV, LeicaImageWidth_um, PointF.Empty, SizeF.Empty);
        }
    }

    public class MetaMorph_MemoryList : IEnumerable<MetaMorph_StagePoint>
    {
        private List<MetaMorph_StagePoint> _List;
        public Associate_InCell_Leica Transform;

        public static MetaMorph_MemoryList FromInCellWell(XDCE_ImageGroup WellIG, float StartingZPoint, Associate_InCell_Leica Transform, List<int> OnlyTheseFields = null)
        {
            MetaMorph_MemoryList ML = new MetaMorph_MemoryList(Transform);
            if (OnlyTheseFields == null) OnlyTheseFields = Enumerable.Range(WellIG.FOV_Min, WellIG.FOV_Max).ToList();
            for (int i = 0; i < OnlyTheseFields.Count; i++)
            {
                int f = OnlyTheseFields[i];
                XDCE_Image FOV = WellIG.GetField(f, 0);
                ML.AddFromFOV(FOV, StartingZPoint);
            }
            return ML;
        }

        public MetaMorph_MemoryList(Associate_InCell_Leica TransformToUse)
        {
            _List = new List<MetaMorph_StagePoint>();
            Transform = TransformToUse;
        }

        public int Count { get => _List.Count; }

        public IEnumerator<MetaMorph_StagePoint> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public MetaMorph_StagePoint this[int index]
        {
            get { return _List[index]; }
        }

        public void Add(MetaMorph_StagePoint SPoint)
        {
            _List.Add(SPoint);
        }
        internal void AddFromFOV(XDCE_Image FOV, float LeicaImageWidth_um, double startingZPoint = double.NaN)
        {
            MetaMorph_StagePoint SP = new MetaMorph_StagePoint(FOV, LeicaImageWidth_um, Transform);
            if (!double.IsNaN(startingZPoint))
            {
                SP.Z = startingZPoint;
            }
            Add(SP);
        }

        public static StringBuilder HeaderExport(int Count)
        {
            StringBuilder SB = new StringBuilder();
            SB.Append("\"Stage Memory List\", Version 6.0\r\n");
            SB.Append("\"Stage Memory List\", Version 6.00, 0, 0, 0, 0, 0, 0, \"microns\", \"microns\"\r\n");
            SB.Append("0\r\n");
            SB.Append(Count + "\r\n");//11 is for the number of points
            return SB;
        }

        public static string LineExport(double X_um, double Y_um, double Z_um, string PointName = "")
        {
            //SB.Append("\"next", 54417.8, 30311, -530.503, 0, 0, FALSE, -9999, TRUE, TRUE, 1, -1, ""
            if (PointName == "") PointName = Math.Round(X_um, 0) + "_" + Math.Round(Y_um, 0);
            return "\"" + PointName + "\", " + X_um + ", " + Y_um + ", " + Z_um + ", 0, 0, FALSE, -9999, TRUE, TRUE, 1, -1, \"\"\r\n";
        }

        public static string LineExport(MetaMorph_StagePoint SPoint)
        {
            return LineExport(SPoint.XY.X, SPoint.XY.Y, SPoint.Z, SPoint.Name);
        }

        public string Export_MemoryListString()
        {
            StringBuilder Sb = HeaderExport(Count);
            foreach (MetaMorph_StagePoint stagePoint in _List) Sb.Append(LineExport(stagePoint));
            return Sb.ToString();
        }

        public bool Export_MemoryListFile(string Path)
        {
            string FullFile = Export_MemoryListString();
            File.WriteAllText(Path, FullFile);
            return true;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }
    }

    public static class INI_Files
    {
        public static string Get_Well_ini(INCELL_Folder IC_Folder, string WellLabel, string Folder, string Objective, short Binning, int FOV_Count, bool IsThereNextWell, Associate_InCell_Leica Transform)
        {
            if (!IC_Folder.XDCE.Wells.ContainsKey(WellLabel)) return "";

            XDCE_ImageGroup Fields = IC_Folder.XDCE.Wells[WellLabel];
            List<int> FOVs_Picked = Fields.RandomFields(FOV_Count, true);
            MetaMorph_MemoryList stagePoints = MetaMorph_MemoryList.FromInCellWell(Fields, 1500, Transform, FOVs_Picked);
            return Get_Well_ini(IC_Folder.PlateID, WellLabel, Folder, Objective, Binning, IsThereNextWell, stagePoints);
        }

        public static string Get_Well_ini(string PlateID, string Well, string Folder, string Objective, short Binning, bool ContinueWells, MetaMorph_MemoryList ML)
        {
            //  - Well Init ini - defines the first 4 calibration wells(also reset the field ini file to not confuse the program)
            StringBuilder sB = new StringBuilder();
            sB.Append("[Header]\r\n");
            sB.Append("PlateID=\"" + PlateID + "\"\r\n");
            sB.Append("Well=\"" + Well + "\"\r\n");
            sB.Append("WellFolder=\"" + Folder + "\"\r\n");
            sB.Append("Objective=\"" + Objective + "\"\r\n");
            sB.Append("Binning=" + Binning + "\r\n");
            sB.Append("ContinueWells=" + (ContinueWells ? 1 : 0) + "\r\n");
            sB.Append("FOVCount=" + ML.Count + "\r\n");
            for (int i = 0; i < ML.Count; i++) sB.Append("[" + i + "]\r\nX=" + ML[i].XY.X + "\r\nY=" + ML[i].XY.Y + "\r\nName=\"" + ML[i].Name + "\"\r\n");
            return sB.ToString();
        }

        public static string Get_FieldStep_ini(string PlateID, string Well, string FOV, string FOVFolder, short Binning, bool ContinueFields, string MaskFile, string ShiftMaskFile, string ShiftAmount, bool Skip, bool ForceMove, bool Cancel, PointF PointWell)
        {
            //- generate ini file with field coordinates
            StringBuilder sB = new StringBuilder();
            sB.Append("[Header]\r\n");
            sB.Append("PlateID=\"" + PlateID + "\"\r\n");
            sB.Append("Well=\"" + Well + "\"\r\n");
            sB.Append("FOV=\"" + FOV + "\"\r\n");
            sB.Append("FOVFolder=\"" + FOVFolder + "\"\r\n");
            sB.Append("Binning=" + Binning + "\r\n");
            sB.Append("ContinueFields=" + (ContinueFields ? 1 : 0) + "\r\n");
            sB.Append("Skip=" + (Skip ? 1 : 0) + "\r\n");
            sB.Append("ForceMove=" + (ForceMove ? 1 : 0) + "\r\n");
            sB.Append("Cancel=" + (Cancel ? 1 : 0) + "\r\n");
            sB.Append("MaskFile=\"" + MaskFile + "\"\r\n");
            sB.Append("ShiftMaskFile=\"" + ShiftMaskFile + "\"\r\n");
            sB.Append("ShiftAmount=\"" + ShiftAmount + "\"\r\n");
            sB.Append("X=" + PointWell.X + "\r\nY=" + PointWell.Y + "\r\n");
            //sB.Append("Xa=" + PointAssumed.X + "\r\nYa=" + PointAssumed.Y + "\r\n");
            return sB.ToString();
        }

        public static string Get_MaskStep_ini(string PlateID, string Well, string FOV, string MaskFile, PointF P)
        {
            //- generate ini file with field coordinates
            StringBuilder sB = new StringBuilder();
            sB.Append("[Header]\r\n");
            sB.Append("PlateID=\"" + PlateID + "\"\r\n");
            sB.Append("Well=\"" + Well + "\"\r\n");
            sB.Append("FOV=\"" + FOV + "\"\r\n");
            sB.Append("MaskFile=\"" + MaskFile + "\"\r\n");
            sB.Append("X=" + P.X + "\r\nY=" + P.Y + "\r\n");

            return sB.ToString();
        }
    }

    public static class InCell_To_Leica
    {
        private static Random _Rand = new Random();
        public static SharedResource SR;
        public static int SRQCapacity = 12;
        public static Queue<SharedResource> SRQ = new Queue<SharedResource>(SRQCapacity);

        public static void AddSR()
        {
            AddSR(InCell_To_Leica.SR);
        }

        public static void AddSR(SharedResource tSR)
        {
            //Worked on this 6/17/2021, but have to come back to it . .
            if (SRQ.Count >= SRQCapacity) SRQ.Dequeue();
            SRQ.Enqueue(tSR.Copy());
        }

        public static void Leica001_WellStart(INCELL_Folder IC_Folder, string WellList, string FieldStart, string WellFieldChecks, short wavelength, float brightness, SVP_Registration_Params RegParams)
        {
            //Setup and save for future use the AIL object (right now static)
            int FieldStartInt;
            if (!int.TryParse(FieldStart, out FieldStartInt)) FieldStartInt = 1;
            int WellFieldChecksInt;
            if (!int.TryParse(WellFieldChecks, out WellFieldChecksInt)) WellFieldChecksInt = 5;
            if (WellList == null) WellList = "A - 1";

            SR = new SharedResource(IC_Folder, WellList, FieldStartInt, WellFieldChecksInt, wavelength, brightness, @"C:\temp\AILBase\", RegParams, "20210505_100224-C");

            //Leica000_CheckAgainstAll(@"C:\Temp\AILBase\FIV538_2\20210505_100224-C");
        }

        private static void Leica000_CheckAgainstAll(string FolderName)
        {
            //This is a way to check 
            DirectoryInfo DI = new DirectoryInfo(FolderName);
            foreach (FileInfo file in DI.GetFiles("*.tif"))
                CheckLeicaImage_AgainstAllFields(file.FullName);
        }

        private static void CheckLeicaImage_AgainstAllFields(string FileName)
        {
            ImgWrap imgWrap = new ImgWrap(FileName);
            ImageAlign_Return IAR;
            SortedList<double, ImageAlign_Return> Results = new SortedList<double, ImageAlign_Return>();
            float size = 0.45f;
            foreach (XDCE_Image xI in SR.SavedICFolder.XDCE.Wells["A - 3"].Images.Where(x => x.wavelength == SR.Wavelength))
            {
                for (float x = 0; x < 1 - size; x += size)
                {
                    for (float y = 0; y < 1 - size; y += size)
                    {
                        IAR = SR.AIL.ImageAlign_Distant(imgWrap, xI, SR.Brightness, new PointF(x, y), new SizeF(size, size));
                        Results.Add(IAR.RR.Score_Final, IAR);
                        if (Results.Count > 20)
                        {
                            File.Delete(Results.Last().Value.OverlayName); Results.RemoveAt(Results.Count - 1);
                        }
                    }
                }
                Debug.WriteLine(xI.FileName + " " + Results.First().Key);
                //SR.Brightness = SR.Brightness * 0.8f;
            }
            File.WriteAllLines(@"c:\temp\trymatch2.txt", Results.Select(x => (x.Key + "\t" + x.Value.OverlayName)).ToList());
        }

        public static string Leica002_NextWell()
        {
            SR.IncrementWell();
            if (SR.CurrentWellXDCE.HasMasks)
            {
                string ini = INI_Files.Get_Well_ini(SR.SavedICFolder, SR.CurrentWell, SR.Path_WellImages, SR.Objective, SR.BinningWell, SR.WellFieldChecks, SR.IsThereNextWell, SR.AIL);
                File.WriteAllText(SR.FullPath_Wellini, ini);
                return "Created ini file for " + SR.CurrentWell;
            }
            return "Well didn't have masks (something went wrong)";
        }

        public static string Leica003b_RegisterWell()
        {
            //We know that we got the number of images asked for, so we have to use them to refine the transform of interest
            File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Start Register Well .." + "\r\n");
            Plate_Session_Class PSC = new Plate_Session_Class(new DirectoryInfo(SR.Path_WellImages), null);
            XDCE_ImageGroup Well = SR.CurrentWellXDCE;
            //SR.TestingMode = true;
            if (SR.TestingMode)
            {
                //PSC = new Plate_Session_Class(new DirectoryInfo(@"R:\FIVE\EXP\FIV538\6 Images\20210619A\Well"), null);
                SR.Offset_WellRefined = new PointF() { X = 895.4479F, Y = 2318.917F };
            }
            else
            {
                SR.AIL.FindOffset_CrossPlatform(PSC, Well, SR.Wavelength, SR.Brightness, out SR.Offset_WellRefined, out SR.WellBased_MaxRange_LastSet, true);
                File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + SR.Offset_WellRefined + "\r\n");
            }
            return "Registered well and found the following offsets " + SR.Offset_WellRefined.ToString();
        }

        public static DateTime FOVTracker = DateTime.MinValue;

        public static string Leica004_NextFOV()
        {
            File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Get Next FOV.." + "\r\n");
            if (SR.CancelRequested) { WriteOutLatestField(null, false, true); return "Cancel Requested"; }

            SR.IncrementFOV(); XDCE_Image FOV = SR.CurrentFOV;

            SR.AIL.Initial_Leica_Offset_um = SR.GetNextOffset_Mix(); //Use the previous learned offset here . .

            WriteOutLatestField(FOV); //Write out the ini information
            File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Next FOV Done" + "\r\n");
            if (true) MasksHelper.ExportShiftedMask(FOV, SR.Reg_Params); //This exports a shifted version of the mask to use

            string TimeEstimate = "";
            if (FOVTracker == DateTime.MinValue) FOVTracker = DateTime.Now; //This means we are at the first one
            if (SR.FOVIdx >= 4)
            {
                double Minutes = SR.FOVCount * (DateTime.Now - FOVTracker).TotalMinutes / (SR.FOVIdx - 1);
                TimeEstimate = Minutes.ToString("0.0") + " Min remaining.";
            }
            return "Updated INI for " + SR.CurrentWell + "." + FOV.FOV.ToString() + ", which is " + SR.FOVIdx + "/" + SR.FOVCount + (TimeEstimate == "" ? "" : " " + TimeEstimate);
        }

        private static void WriteOutLatestField(XDCE_Image FOV, bool Skip = false, bool Cancel = false)
        {
            ////First delete the field that is slated for deletion
            //if (SR.FOVImageToDelete != null) { FileInfo FI = new FileInfo(SR.FOVImageToDelete); FI.Delete(); }

            //Now make a new ini for the field we are working on
            if (!Cancel) SR.SetCurrentPos(FOV);
            bool ForceMove = SR.DifferentThanLast_Y; //TODO: This should look at the delta from the current position to the refined position and decide
            string FOVStr = FOV == null ? "" : FOV.FOV.ToString();
            string ini = INI_Files.Get_FieldStep_ini(SR.PlateID, SR.CurrentWell, FOVStr, SR.Path_FieldImage, SR.BinningField, SR.IsThereNextField, SR.CurrentMaskFullPath, SR.CurrentShiftMaskFullPath, SR.Shift, Skip, ForceMove, Cancel, SR.CurrentFOVPos);
            File.WriteAllText(SR.FullPath_FOVini, ini);
        }

        /// <summary>
        /// This is designed to run on a smaller search area to quickly refine the field's position based on the Well's offset
        /// </summary>
        /// <param name="Opt">Usually we are in Opt=1, but if we have already moved to the optimal position then use Opt=2</param>
        /// <returns>A message about the results</returns>
        public static string Leica005_RefineFOV(float InitialErrorDivisor, int Opt = 1)
        {
            File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Refine FOV.." + "\r\n");
            var PSC = new Plate_Session_Class(new DirectoryInfo(SR.Path_FieldImage), null);
            var imgLE = PSC.GetFromPosition(SR.CurrentFOVPos);
            if (imgLE == null)
            {
                Debugger.Break(); //Something is wrong, couldn't find the matching field
                WriteOutLatestField(SR.CurrentFOV, true);
                return "Error refining FOV, Couldnt find matching field. " + SR.CurrentFOVPos;
            }
            string Note = "";
            if (Opt == 2) SR.AIL.Initial_Leica_Offset_um = SR.Offset_FieldRefined; //Normall in Option 1, continue to use the Mixed Field/Well starting point (changed 5/25/2022)
            SR.AIL.Error_Divisor = InitialErrorDivisor;
            var IAR = SR.AIL.ImageAlign(imgLE, SR.CurrentFOV, SR.Brightness, true, false, false); //Do the registration
            if (IAR.RR == null)
            {
                IAR = SR.AIL.ImageAlign(imgLE, SR.CurrentFOV, SR.Brightness * 4, true); //Try registration again but brighten the image
                if (IAR.RR == null) { WriteOutLatestField(SR.CurrentFOV, true); return "Failed to get good refinement, skipping this well"; }
            }
            //PointF CurrentFOVPos = SR.AIL.LeicaPositionEstimate_FromGEXDCE(SR.CurrentFOV, SR.LeicaImageWidth_um); ////Just for debugging . . 

            var TempOffset = new PointF(IAR.Offset.X, IAR.Offset.Y);
            bool WithinMaxBounds = SR.IsOffsetWithinMaxBounds(TempOffset);
            if (WithinMaxBounds)
            {
                //Adjust the position
                Note = "GoodAlign";
                SR.Offset_FieldRefined = TempOffset;
                SR.Shift = "(" + IAR.RR.ShiftMask_Pixels.X.ToString("0.0") + "," + IAR.RR.ShiftMask_Pixels.Y.ToString("0.0") + ")";
                //The IAR is already saved insde of the ImageAlign bit
                SR.AIL.Initial_Leica_Offset_um = SR.Offset_FieldRefined; //Use this as the new initial offset
                WriteOutLatestField(SR.CurrentFOV); //re-make the FOV ini file to go to the refined position
            }
            else
            {
                Note = "OutsideOfMaxBounds";
                //Says to skip this field since we think registration may have failed
                WriteOutLatestField(SR.CurrentFOV, true);
            }
            if (Opt == 2) //This is when we go back to the optimized position a second time . . 
            {
                Note = "2ndLevelRegistration";
                //Make a file that has the Mask overlaid with the real file
                Bitmap Mask = new Bitmap(new Bitmap(SR.CurrentMaskFullPath), imgLE.BMP.Size);
                Bitmap rBMP = Utilities.ColorMatrix.ArithmeticBlend(Mask, imgLE.BMP, Utilities.ColorMatrix.ColorCalculationType.Difference);
                DirectoryInfo DI = new DirectoryInfo(SR.Path_Overlays + " wMask");
                if (!DI.Exists) DI.Create();
                rBMP.Save(Path.Combine(DI.FullName, imgLE.Name + ".bmp"));
            }
            //Save the name so we can easily delete this image file on the next cycle
            SR.FOVImageToDelete = imgLE.PathToImage;

            AddSR(SR); //Adds to the SR queue. Saving the IAR happens inside the ImageAlign above
            File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "FOV Aligned " + Note + "\r\n");
            return "Refined FOV " + SR.Offset_FieldRefined;
        }

        public static string Leica007_SaveCropped()
        {
            //TODO IMPLEMENT this for testing (maybe)
            string newMaskName = ""; //= SR.IAR.ExportShiftedMask(SR.CurrentFOV); //Resave a mask image with the correct stuff
            return newMaskName;
        }

        //Find currently-running instance
        //https://stackoverflow.com/questions/26938984/open-file-in-the-currently-running-instance-of-my-program

        //Send receive message between programs
        //https://stackoverflow.com/questions/11358841/send-receive-message-to-from-two-running-application

        //Pipes
        //https://docs.microsoft.com/en-us/dotnet/standard/io/how-to-use-anonymous-pipes-for-local-interprocess-communication

        //  - Scan on Geic
        //  - InCarta
        //  - Select cells and intensities
        //  - Generate mask images (specific folder that can be built into IC_Folder)
        //- Leica Run Program
        //- Set Obective, Channel, and approximate focus
        //- FIVTools.exe LeicaStart_001
        //  - Loads usual form, but in "Leica Connect mode", add an extra button to the right once things have been tested(Raft Cal)
        //  - Somewhere to enter the wells to scan(like a list A1, A2, A3, etc)
        //  - WellStart ini - defines the first 4 calibration wells(also reset the field ini file to not confuse the program)
        //- Goto Well
        //  - Load INI File(PlateDB/Auto01/WellStart.ini)
        //  - Get 4 registration images(Saves to PlateDB/Auto01/WellTemp
        //  - Calls FIV tools (wait till finished) - remebers previous settings(maybe just loads the previous ini file to get where it left off)
        //    - FIVTools.exe WellRegister_A5
        //	- Generate offsets(wider error in FIV Tools)
        //	- check that things make sense
        //	- generate ini file with field coordinates
        //  - Step every GE FOV
        //    - Leica looks for a file that instructs starting field(PlateDB/Auto01/FieldStep.ini)
        //    - Get small reg image
        //	- Call FIV Tools
        //	  - Get offset from GE Field(smaller error range in FIV Tools
        //	  - FIV tools returns with another file that tells it where to go
        //	  - Preload next image into memory so it is ready (can't keep them all in memory)
        //	- Move Leica stage to exact position
        //	- Photoactivate
        //	- Call FIV Tools that finished photo-activation
        //- Find out from Josh how he does a multi-channel scan (preferred way), so we can build this for both GE and Leica versions
        //- Leica use 4x4 binning for registration images
        //- Where does the instruction file get saved
        //- Folders for my Leica journals
        //- Instead of the main FIV-tools (which remains open and can keep some of the images in-memory), we can have a utility that just talks to FIV tools, but can open and close.Alternatively we can have a setting where FIV tools doesn't load its GUI, and just runs a function then closes
    }

    public class Associate_InCell_Leica
    {
        internal SVP_Registration_Params Reg_Params;
        public PointF Default_InitialOffset_um; //This one doesn't change
        public PointF Initial_Leica_Offset_um;  //This one changes
        public float X_Error_Pixels = 510;      //This is like the search space
        public float Y_Error_Pixels = 460;
        public float Error_Divisor = 1;
        public float resize_divisor_forRegistration = 2;
        public double MinStDev = 30;
        public SizeF GE_LeicaSizeRatio_ForImage;   //This is when the GE image is produced for used in registration
        public SizeF GE_LeicaSizeRatio_ForMoving;  //This is when the GE coordinates are adapted to find the Leica coordinates (in combination with the offset)
        public string ExportLoc = @"c:\temp\export\";

        public Associate_InCell_Leica(SVP_Registration_Params Reg_Params)
        {
            this.Reg_Params = Reg_Params;
            Default_InitialOffset_um = Reg_Params.InitialDefaultOffset;
            Initial_Leica_Offset_um = Reg_Params.InitialDefaultOffset;
            GE_LeicaSizeRatio_ForImage = Reg_Params.GE_LeicaSizeRatio_ForImage;
            GE_LeicaSizeRatio_ForMoving = Reg_Params.GE_LeicaSizeRatio_ForMoving;
        }

        public static void GenerateMemoryList(INCELL_Folder icFolder, string Well, string SaveLocationOfMemoryList, float StartingZPoint, SVP_Registration_Params Reg_Params)
        {

            if (!icFolder.XDCE.Wells.ContainsKey(Well)) return;
            XDCE_ImageGroup WellIG = icFolder.XDCE.Wells[Well];
            MetaMorph_MemoryList ML = MetaMorph_MemoryList.FromInCellWell(WellIG, StartingZPoint, new Associate_InCell_Leica(Reg_Params));
            ML.Export_MemoryListFile(SaveLocationOfMemoryList);
        }

        public static void CompareAllLeicaSessionImages(INCELL_Folder icFolder, int icWvIdx, float icBrightness, string PlateDB_Location, SVP_Registration_Params Reg_Params, bool ExportOverlayImages = false, System.ComponentModel.BackgroundWorker BW = null)
        {
            //I think this is just used in testing
            var AC = new Associate_InCell_Leica(Reg_Params); Plate_Session_Class PSC;

            Debug.Print("Loaded DB and finding matches . .");
            bool TestMode = true;
            if (TestMode)
            {
                PSC = new Plate_Session_Class(new DirectoryInfo(@"E:\Temp\AILBase\FIV497P6\20210505_100224-C\FOV"), null);
                //AC.Error_Divisor = 1; //This makes the error smaller
            }
            else
            {
                Plate_DB_Class PDB = new Plate_DB_Class(PlateDB_Location);
                PSC = PDB.FindNewestSession(icFolder.PlateID_NoScanIndex);
            }
            //AC.FindOffset_CrossPlatform(PSC, icFolder.XDCE, icWvIdx, icBrightness, , , ExportOverlayImages);
        }

        public void FindOffset_CrossPlatform(Plate_Session_Class PSC, XDCE_ImageGroup InCellPlate_or_Well, int icWvIdx, float icBrightness, out PointF NewOffsets, out PointF Ranges, bool ExportOverlayImages = false)
        {
            Dictionary<string, List<ImageAlign_Return>> IAR_Dict = new Dictionary<string, List<ImageAlign_Return>>();
            foreach (ImgWrap imgLE in PSC.Image_List)
            {
                //if (imgLE.Name.StartsWith("525"))
                ImageAlign_Return IAR = ImageAlign(imgLE, InCellPlate_or_Well, icWvIdx, icBrightness, ExportOverlayImages);
                if (IAR.RR != null)
                {
                    if (!IAR_Dict.ContainsKey(IAR.WellLabel)) IAR_Dict.Add(IAR.WellLabel, new List<ImageAlign_Return>());
                    IAR_Dict[IAR.WellLabel].Add(IAR);
                } //The note may indicate why
            }
            if (IAR_Dict.Count == 0) //None matched
            {
                Ranges = new PointF(0, 0); NewOffsets = new PointF(0, 0); return;
            }
            //Construct the look-up table
            List<ImageAlign_Return> IAR_List = IAR_Dict.First().Value;
            string S = string.Join("\r\n", IAR_List.Select(x => x.ExportName + "\t" + x.FractCenter_GE_um.X + "\t" + x.FractCenter_GE_um.Y + "\t" + x.FractCenter_LE_um.X + "\t" + x.FractCenter_LE_um.Y + "\t" + x.RR.FinalR2 + "\t" + x.ImgInternal_RedVariation + "\t" + x.Note));

            //Calculate regression 
            double X_slope, X_intr, X_r2, Y_slope, Y_intr, Y_r2;
            Utilities.MathUtils.LinearRegression(IAR_List.Select(x => x.FractCenter_LE_um.X).ToArray(), IAR_List.Select(x => x.FractCenter_GE_um.X).ToArray(), out X_r2, out X_intr, out X_slope);
            Utilities.MathUtils.LinearRegression(IAR_List.Select(x => x.FractCenter_LE_um.Y).ToArray(), IAR_List.Select(x => x.FractCenter_GE_um.Y).ToArray(), out Y_r2, out Y_intr, out Y_slope);

            //Get the Ranges of offsets from the positions 5/2022
            var Offsets = IAR_List.Select(x => new PointF(x.FractCenter_GE_um.X - x.FractCenter_LE_um.X, x.FractCenter_GE_um.Y - x.FractCenter_LE_um.Y));
            Ranges = new PointF(Offsets.Max(x => x.X) - Offsets.Min(x => x.X), Offsets.Max(x => x.Y) - Offsets.Min(x => x.Y));

            //Here is the final best range
            //TODO: This could just use the pre-existing Offsets made above instead of re-making it 5/2022
            NewOffsets = new PointF(
                IAR_List.Select(x => (x.FractCenter_GE_um.X - x.FractCenter_LE_um.X)).Average(),
                IAR_List.Select(x => (x.FractCenter_GE_um.Y - x.FractCenter_LE_um.Y)).Average());
        }

        public ImageAlign_Return ImageAlign(ImgWrap imgLE, XDCE_ImageGroup InCellPlate_or_Well, int icWvIdx, float icBrightness, bool ExportOverlay = false, bool AddCenterTick = false, bool ExportMatch = false)
        {
            //First find the closest image in the InCell scan to the image we are comparing
            PointF LeicaInitialCenter_um = LeicaImageCenter_Estimate(imgLE);
            List<XDCE_Image> rIMGs = InCellPlate_or_Well.GetClosestFields(LeicaInitialCenter_um.X, LeicaInitialCenter_um.Y);
            if (rIMGs == null) return new ImageAlign_Return("", "Couldn't find a match in this InCell Scan");
            XDCE_Image xI = rIMGs.Where(x => x.wavelength == icWvIdx).First();

            bool DoMatchingPoints = false;
            if (DoMatchingPoints)
            {
                //try and check various points
                string S = MatchingPointsText(imgLE, xI);
            }

            //For testing, do 2 rounds of stuff
            ImageAlign_Return IAR;
            IAR = ImageAlign(imgLE, xI, icBrightness, ExportOverlay, AddCenterTick, ExportMatch);
            bool SecondCheck = false;
            if (!SecondCheck)
            {   //This was added to skip the second check . . not sure the impact
                this.Initial_Leica_Offset_um = IAR.Offset;
            }
            else
            {
                if (IAR.RR != null)
                {
                    //It should be very fast since it doesn't have much area to explore
                    RefineBaseOnAlignment(this, IAR, 1000); //Fixes up the intial offset and turns the error spread way down 
                    ImageAlign_Return IAR2 = ImageAlign(imgLE, xI, icBrightness, ExportOverlay, AddCenterTick, ExportMatch);
                    this.Error_Divisor = 1;
                    float dist = ImageAlign_Return.Distance(IAR, IAR2);
                    IAR.Note += "/r/n Distance between two versions (5 is cut-off) = " + dist;
                    if (dist > 5)
                    {
                        //Questionable, so lets revert back to the starting
                        this.Initial_Leica_Offset_um = Default_InitialOffset_um;
                    }
                    else
                    {
                        //Did good, but lets be safe and go halfway there
                        this.Initial_Leica_Offset_um = Utilities.MathUtils.GetAverage(Default_InitialOffset_um, IAR.Offset);
                    }
                }
            }
            return IAR;
        }

        private string MatchingPointsText(ImgWrap imgLE, XDCE_Image imgIC)
        {
            PointF LeicaInitialCenter_um = LeicaImageCenter_Estimate(imgLE);
            MetaMorph_StagePoint SP = new MetaMorph_StagePoint(imgIC, imgLE.Width_um, this);
            StringBuilder sB = new StringBuilder();
            sB.Append("InCel Center GE_Corr\t" + imgIC.PlateXY_Center_um.X + "\t" + imgIC.PlateXY_Center_um.Y + "\r\n"); //These two should match pretty closely
            sB.Append("Leica Center GE_Pred\t" + LeicaInitialCenter_um.X + "\t" + LeicaInitialCenter_um.Y + "\r\n");

            sB.Append("Leica Center LE_Corr\t" + imgLE.Center_um.X + "\t" + imgLE.Center_um.Y + "\r\n"); //This should just be the initial offset away from above

            sB.Append("Leica GoTo   LE_Corr\t" + imgLE.X_um + "\t" + imgLE.Y_um + "\r\n"); //These two should match pretty closely
            sB.Append("InCel StgPos LE_Pred\t" + SP.XY.X + "\t" + SP.XY.Y + "\r\n");

            return sB.ToString();
        }

        private Dictionary<string, ImgWrap> CacheImWrap = new Dictionary<string, ImgWrap>();

        public ImageAlign_Return ImageAlign_Distant(ImgWrap imgLE, XDCE_Image imgINCELL, float icBrightness, PointF FractionStart, SizeF FractionalSize)
        {
            bool ExportOverlay = true;
            //float resize_divisor_forRegistration = 1; //override the main one

            var IAR = new ImageAlign_Return(imgINCELL.WellLabel, this);
            RectangleF R_GEFract_Pix; Bitmap GEFract_BMP; string ExportFront;
            R_GEFract_Pix = new RectangleF(FractionStart.X * imgINCELL.Width_Pixels, FractionStart.Y * imgINCELL.Height_Pixels, FractionalSize.Width * imgINCELL.Width_Pixels, FractionalSize.Height * imgINCELL.Height_Pixels);
            PointF CenterPixel_LE = new PointF(300, 300); //Not actually used except to plot center pixel
            IAR.ExportName = imgINCELL.WellField + "=" + imgLE.Name;
            ExportFront = Path.Combine(ExportLoc, IAR.ExportName);
            GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE, "", false);
            if (GEFract_BMP == null) { IAR.Note = "Problem getting the InCell Image"; return IAR; }

            ImgWrap ImWrapGE = new ImgWrap(GEFract_BMP);
            IAR.ImgInternal_RedVariation = ImWrapGE.RedStDev;

            //Resize the Leica Image and package into ImgWraps
            ImgWrap imgLE_RS;
            if (!CacheImWrap.ContainsKey(imgLE.Name))
            {
                Bitmap LE_BMP = new Bitmap(imgLE.BMP, new Size((int)(imgLE.Width / resize_divisor_forRegistration), (int)(imgLE.Width / resize_divisor_forRegistration)));
                imgLE_RS = new ImgWrap(LE_BMP);
                CacheImWrap.Add(imgLE.Name, imgLE_RS);
            }
            else { imgLE_RS = CacheImWrap[imgLE.Name]; }

            //Use cross-correlation to register the images together
            IAR.RR = CrossCorr.Register(ImWrapGE, imgLE_RS);

            IAR.FractCenter_GE_um = imgINCELL.PlateCoordinates_um_InCenterOfRectangle(R_GEFract_Pix); //This is on the pre-shrunk coordinates
            IAR.FractCenter_LE_um = new PointF( //these are shrunk so have to expand, Leica Pixel binning already accounted for, don't do here
                    (float)(imgLE.X_um - (IAR.RR.Location.X + (ImWrapGE.Width / 2)) * imgLE.umPerpixel * resize_divisor_forRegistration),
                    (float)(imgLE.Y_um - (IAR.RR.Location.Y + (ImWrapGE.Height / 2)) * imgLE.umPerpixel * resize_divisor_forRegistration));

            //Export Overlay
            IAR.OverlayName = ExportFront + "_E" + this.Error_Divisor + "_OV_" + IAR.Offsets + ".jpg";
            InCell_To_Leica.SR.OverlayPath = IAR.OverlayName;
            if (ExportOverlay) MasksHelper.ExportOverlay(IAR, IAR.OverlayName, ImWrapGE, imgLE_RS);
            GEFract_BMP.Dispose();

            return IAR;
        }

        public ImageAlign_Return ImageAlign(ImgWrap imgLE, XDCE_Image imgINCELL, float icBrightness, bool ExportOverlay = false, bool AddCenterTick = false, bool ExportMatch = false)
        {
            //Creat IAR then Add some more info for the saved XML file
            var IAR = new ImageAlign_Return(imgINCELL.WellLabel, this);
            //IAR.WellOffset = Default_InitialOffset_um; //Be nice to include this as well, the Well offset that is
            IAR.Offset_Before = Initial_Leica_Offset_um;
            IAR.Leica_StartingPosition = imgLE.Center_um;
            IAR.InCell_StartingPosition = imgINCELL.CenterPixel;

            //icBrightness = 4;
            AddCenterTick = false; //For testing
            resize_divisor_forRegistration = 2;

            //There is a slight bit of scale coming from somewhere, the GE Image is slightly smaller
            RectangleF R_GEFract_Pix; PointF CenterPixel_LE; Bitmap GEFract_BMP; string ExportFront;

            //Where is the center of the Leica Image, when corrected to ge coordinates (in Plate um)
            PointF LeicaInitialCenter_um = LeicaImageCenter_Estimate(imgLE);

            //Use the GE Field's Plate coordinates to find which pixel that center point is at
            CenterPixel_LE = imgINCELL.Pixel_FromPlateCoordinates(LeicaInitialCenter_um);

            //We need to chop out a safe area from the GE image to do CrossCorr with the other Leica Image - This is the "GE Fract" Image
            R_GEFract_Pix = GetInCellFractionalRectangle(imgINCELL, imgLE, CenterPixel_LE);

            //Now get the GE image, add any annotation, and prep it for comparison
            IAR.ExportName = imgINCELL.WellField + "=" + imgLE.Name;
            ExportFront = Path.Combine(ExportLoc, IAR.ExportName);
            //ExportMatch = true;
            GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE, ExportMatch ? ExportFront : "", AddCenterTick);
            if (GEFract_BMP == null) { IAR.Note = "Problem getting the InCell Image"; return IAR; }

            //Check to make sure that region has enough variation to do XCorr
            ImgWrap ImWrapGE = new ImgWrap(GEFract_BMP);
            IAR.ImgInternal_RedVariation = ImWrapGE.RedStDev;

            //Resize the Leica Image and package into ImgWraps
            Bitmap LE_BMP = new Bitmap(imgLE.BMP, new Size((int)(imgLE.Width / resize_divisor_forRegistration), (int)(imgLE.Width / resize_divisor_forRegistration)));
            ImgWrap imgLE_RS = new ImgWrap(LE_BMP);
            IAR.ImgExternal_RedVariation = imgLE_RS.RedStDev;

            //Check quality of starting image
            if (ImWrapGE.RedStDev < MinStDev || imgLE_RS.RedStDev < MinStDev) { IAR.Note = "Not enough image variation for XCorr"; return IAR; }

            //Use cross-correlation to register the images together
            IAR.RR = CrossCorr.Register(ImWrapGE, imgLE_RS);

            //Record the before and after points
            //Need the cetner of the GE Fract in GE Coordinates, then the one in LE coordinates
            //TODO SOMETHING HERE IS WRONG . . This may be explained by the new factor of size difference found . . 
            IAR.FractCenter_GE_um = imgINCELL.PlateCoordinates_um_InCenterOfRectangle(R_GEFract_Pix); //This is on the pre-shrunk coordinates
            IAR.FractCenter_LE_um = new PointF( //these are shrunk so have to expand, Leica Pixel binning already accounted for, don't do here
                    (float)(imgLE.X_um - (IAR.RR.Location.X + (ImWrapGE.Width / 2)) * imgLE.umPerpixel * resize_divisor_forRegistration),
                    (float)(imgLE.Y_um - (IAR.RR.Location.Y + (ImWrapGE.Height / 2)) * imgLE.umPerpixel * resize_divisor_forRegistration));

            //Export Overlay
            IAR.OverlayName = ExportFront + "_E" + this.Error_Divisor + "_OV_" + IAR.Offsets + ".jpg";
            InCell_To_Leica.SR.OverlayPath = IAR.OverlayName;
            if (ExportOverlay) MasksHelper.ExportOverlay(IAR, IAR.OverlayName, ImWrapGE, imgLE_RS);
            if (true) MasksHelper.ExportShiftedMask(imgINCELL, IAR, ImWrapGE.BMP.Size, imgLE_RS.BMP.Size /*, ExportFront + "_E" + this.Error_Divisor + "_OV_" + IAR.Offsets*/);

            //Clean Up
            GEFract_BMP.Dispose();
            imgLE_RS.BMP.Dispose();

            //Save this in the SR Queue
            InCell_To_Leica.SR.IAR = IAR;
            InCell_To_Leica.AddSR();

            //Add some more info for the saved XML file
            IAR.Offset_After = IAR.Offset;

            //Export QC
            IAR.ExportQC(ExportFront + ".xml");
            return IAR;
        }

        public static void RefineBaseOnAlignment(Associate_InCell_Leica AIL, ImageAlign_Return IAR, float ErrorDivisor = -1)
        {
            AIL.Initial_Leica_Offset_um.X = IAR.FractCenter_GE_um.X - IAR.FractCenter_LE_um.X;
            AIL.Initial_Leica_Offset_um.Y = IAR.FractCenter_GE_um.Y - IAR.FractCenter_LE_um.Y;
            if (ErrorDivisor > 0) AIL.Error_Divisor = ErrorDivisor;
        }

        public int FractRect_XErr => Math.Max(1, (int)(X_Error_Pixels / Error_Divisor));
        public int FractRect_YErr => Math.Max(1, (int)(Y_Error_Pixels / Error_Divisor));

        private RectangleF GetInCellFractionalRectangle(XDCE_Image xI, ImgWrap imgLE, PointF CenterPixel_LE)
        {
            //We need to chop out a safe area from the GE image to do CrossCorr with the other Leica Image - This is the "GE Fract" Image
            //The larger the area, the smaller this area can be since we don't want to go outside of either image
            float LE_Rel = (imgLE.umPerpixel / (float)xI.Parent.PixelWidth_in_um);
            RectangleF R_GEFract_Pix = new RectangleF(CenterPixel_LE.X - (LE_Rel * imgLE.Width / 2) + FractRect_XErr,
                                                      CenterPixel_LE.Y - (LE_Rel * imgLE.Height / 2) + FractRect_YErr,
                                                      CenterPixel_LE.X + (LE_Rel * imgLE.Width / 2) - FractRect_XErr,
                                                      CenterPixel_LE.Y + (LE_Rel * imgLE.Height / 2) - FractRect_YErr);
            R_GEFract_Pix.X = Math.Max(0, R_GEFract_Pix.X);
            R_GEFract_Pix.Y = Math.Max(0, R_GEFract_Pix.Y);
            R_GEFract_Pix.Width = Math.Min(xI.Width_Pixels - 0.1f, R_GEFract_Pix.Width);
            R_GEFract_Pix.Height = Math.Min(xI.Height_Pixels - 0.1f, R_GEFract_Pix.Height);

            //I am making the rect as x1,y1,x2,y2, which is wrong, so I have to fix the width and height
            R_GEFract_Pix.Width -= R_GEFract_Pix.Left; R_GEFract_Pix.Height -= R_GEFract_Pix.Top;
            return R_GEFract_Pix;
        }

        public Bitmap GetInCellBMP02(XDCE_Image xI, float LE_umPerPixel, ushort black, ushort white, RectangleF R_GEFract_Pix, PointF CenterPixel_LE, string ExportFront = "")
        {
            //Now get the GE image, add any annotation, and prep it for comparison
            Bitmap GEFract_BMP;

            //We don't know ahead of time the region of this to grab, but we do know the brightness and the binning, so those to things should be done first and stored in memory
            //Bitmap BMP = Utilities.DrawingUtils.GetImageSt(xI);
            var BMP = Utilities.DrawingUtils.GetAdjustedImageSt(xI, black, white);
            GEFract_BMP = new Bitmap(BMP, new Size((int)(BMP.Width / resize_divisor_forRegistration), (int)(BMP.Height / resize_divisor_forRegistration)));
            GEFract_BMP.RotateFlip(RotateFlipType.Rotate180FlipNone);

            //Last thing to do is just crop out the right area
            float Leica_Pixel_Binning = LE_umPerPixel / (float)xI.Parent.PixelWidth_in_um;
            GEFract_BMP = BMP.Clone(R_GEFract_Pix, BMP.PixelFormat);
            if (ExportFront != "") GEFract_BMP.Save(ExportFront + "_GF.BMP");

            return GEFract_BMP;
        }

        public Bitmap GetInCellBMP(XDCE_Image xI, float LE_umPerPixel, float Brightness, RectangleF R_GEFract_Pix, PointF CenterPixel_LE, string ExportFront = "", bool AddCenterTick = false)
        {
            //Now get the GE image, add any annotation, and prep it for comparison
            Bitmap GEFract_BMP = null;
            Bitmap BMP = Utilities.DrawingUtils.GetAdjustedImageSt(xI, Brightness);
            if (ExportFront != "") BMP.Save(ExportFront + "_GO.BMP"); //Original
            if (AddCenterTick) AddCenterTickToBMP(CenterPixel_LE, BMP);
            float Leica_Pixel_Binning = LE_umPerPixel / (float)xI.Parent.PixelWidth_in_um;
            try
            {
                GEFract_BMP = BMP.Clone(R_GEFract_Pix, BMP.PixelFormat);
            }
            catch { return null; }
            GEFract_BMP = new Bitmap(GEFract_BMP, new Size(
                (int)(GE_LeicaSizeRatio_ForImage.Width * GEFract_BMP.Width / (resize_divisor_forRegistration * Leica_Pixel_Binning)),
                (int)(GE_LeicaSizeRatio_ForImage.Height * GEFract_BMP.Height / (resize_divisor_forRegistration * Leica_Pixel_Binning))));
            //GEFract_BMP = new Bitmap(GEFract_BMP, new Size(  //This is what we had before 5/12
            //    (int)(GEFract_BMP.Width / (resize_divisor_forRegistration * Leica_Pixel_Binning)),
            //    (int)(GEFract_BMP.Height / (resize_divisor_forRegistration * Leica_Pixel_Binning))));
            GEFract_BMP.RotateFlip(RotateFlipType.Rotate180FlipNone);
            if (ExportFront != "") GEFract_BMP.Save(ExportFront + "_GF.BMP"); //Fractional
            BMP.Dispose();
            return GEFract_BMP;
        }

        private static void AddCenterTickToBMP(PointF CenterPixel_LE, Bitmap BMP)
        {
            using (Graphics g = Graphics.FromImage(BMP))
            {
                g.FillRectangle(Utilities.DrawingUtils.RedBrush, CenterPixel_LE.X, CenterPixel_LE.Y, 22, 6);
                g.FillRectangle(Utilities.DrawingUtils.RedBrush, CenterPixel_LE.X, CenterPixel_LE.Y, 6, 22);
            }
        }

        /// <summary>
        /// Tried to estimate the correct position to take a picture that would match a FOV in the GE
        /// </summary>
        /// <param name="FOV"></param>
        /// <returns></returns>
        public PointF LeicaPositionEstimate_FromGEXDCE(XDCE_Image FOV, float LeicaImageWidth_um, PointF SpecifyOffset, SizeF SpecifyStretch)
        {
            float HalfWidth = LeicaImageWidth_um / 2;
            float X_Offset = (SpecifyOffset.IsEmpty) ? Initial_Leica_Offset_um.X : SpecifyOffset.X;
            float Y_Offset = (SpecifyOffset.IsEmpty) ? Initial_Leica_Offset_um.Y : SpecifyOffset.Y;
            float X_Stretch = SpecifyStretch.IsEmpty ? GE_LeicaSizeRatio_ForMoving.Width : SpecifyStretch.Width;
            float Y_Stretch = SpecifyStretch.IsEmpty ? GE_LeicaSizeRatio_ForMoving.Height : SpecifyStretch.Height;
            PointF NewCenter = new PointF((FOV.PlateXY_Center_um.X - X_Offset) * X_Stretch, (FOV.PlateXY_Center_um.Y - Y_Offset) * Y_Stretch);
            PointF NewStart = new PointF(NewCenter.X + HalfWidth, NewCenter.Y + HalfWidth);
            return NewStart;
        }

        /// <summary>
        /// Returns the estimated GE coordinates for the center of the given Leica Image
        /// </summary>
        public PointF LeicaImageCenter_Estimate(ImgWrap imgLE)
        {
            //New as of 4/26, realized that the center based on using the microscope really is minus minus
            return new PointF((imgLE.X_umCenter + Initial_Leica_Offset_um.X),
                              (imgLE.Y_umCenter + Initial_Leica_Offset_um.Y));
        }

        internal Associate_InCell_Leica Copy()
        {
            var Dest = new Associate_InCell_Leica(this.Reg_Params);
            Dest.Error_Divisor = this.Error_Divisor;
            Dest.GE_LeicaSizeRatio_ForImage = this.GE_LeicaSizeRatio_ForImage;
            Dest.MinStDev = this.MinStDev;
            Dest.resize_divisor_forRegistration = this.resize_divisor_forRegistration;
            Dest.X_Error_Pixels = this.X_Error_Pixels;
            Dest.Y_Error_Pixels = this.Y_Error_Pixels;
            return Dest;
        }
    }

    public class SharedResource
    {
        public SharedResource(INCELL_Folder IC_Folder, string WellList, int firstFieldStart, int wellFieldChecks, short waveLength, float brightness, string BasePath, SVP_Registration_Params RegParams, string SessionID = "", bool ClearFolder = true)
        {
            CancelRequested = false;
            IsThereNextWell = true;
            Reg_Params = RegParams;
            DefaultInitOffset = Reg_Params.InitialDefaultOffset;

            Wavelength = waveLength;
            Brightness = brightness;
            PlateID = IC_Folder.PlateID_NoScanIndex;
            FirstFieldIdx_Start = firstFieldStart;
            WellFieldChecks = wellFieldChecks;
            this.SessionID = SessionID == "" ? DateTime.Now.ToString(Plate_Session_Class.DateFormat) + "-C" : SessionID;

            Path_Root = BasePath; //This has functionality to create folders

            _SavedWellList = WellList.Split(',').ToList();
            for (int i = 0; i < _SavedWellList.Count; i++) _SavedWellList[i] = _SavedWellList[i].ToUpper().Trim();
            this.WellList = WellList;
            _SavedICFolder = IC_Folder;
            _NextSavedWellIdx = 0;
            _SavedAIL = new Associate_InCell_Leica(Reg_Params) { ExportLoc = Path_Overlays };

            if (ClearFolder)
            {
                //Clear out the Well and Overlay folders
                DirectoryInfo DI;
                DI = new DirectoryInfo(this.Path_Overlays); foreach (FileInfo FI in DI.GetFiles()) FI.Delete();
                DI = new DirectoryInfo(this.Path_WellImages); foreach (FileInfo FI in DI.GetFiles()) FI.Delete();
            }
        }


        //Not used, this was a possible way to talk
        public void AO()
        {
            //Memory Mapped Files
            //https://docs.microsoft.com/en-us/dotnet/standard/io/memory-mapped-files
            using (MemoryMappedFile mmf = MemoryMappedFile.CreateNew("testmap", 10000))
            {
                bool mutexCreated;
                Mutex mutex = new Mutex(true, "testmapmutex", out mutexCreated);
                using (MemoryMappedViewStream stream = mmf.CreateViewStream())
                {
                    BinaryWriter writer = new BinaryWriter(stream);
                    writer.Write(1);
                }
                mutex.ReleaseMutex();

                Console.WriteLine("Start Process B and press ENTER to continue.");
                Console.ReadLine();

                Console.WriteLine("Start Process C and press ENTER to continue.");
                Console.ReadLine();

                mutex.WaitOne();
                using (MemoryMappedViewStream stream = mmf.CreateViewStream())
                {
                    BinaryReader reader = new BinaryReader(stream);
                    Console.WriteLine("Process A says: {0}", reader.ReadBoolean());
                    Console.WriteLine("Process B says: {0}", reader.ReadBoolean());
                    Console.WriteLine("Process C says: {0}", reader.ReadBoolean());
                }
                mutex.ReleaseMutex();
            }
        }

        public SharedResource Copy()
        {
            SharedResource CopyFrom = this;
            SharedResource Dest = new SharedResource(CopyFrom.SavedICFolder, CopyFrom.WellList, CopyFrom.FirstFieldIdx_Start, CopyFrom.WellFieldChecks, CopyFrom.Wavelength, CopyFrom.Brightness, CopyFrom.Path_Root, CopyFrom.Reg_Params, CopyFrom.SessionID, false);
            Dest.AIL = CopyFrom.AIL.Copy();
            Dest.IAR = CopyFrom.IAR;
            Dest.CurrentFOV = CopyFrom.CurrentFOV;
            Dest.CurrentWell = CopyFrom.CurrentWell;
            Dest.OverlayPath = CopyFrom.OverlayPath;
            return Dest;
        }

        public SVP_Registration_Params Reg_Params;
        public short Wavelength { get; set; }
        public float Brightness { get; set; }

        public string WellList { get; set; }

        public string OverlayPath { get; set; }

        private INCELL_Folder _SavedICFolder;
        public INCELL_Folder SavedICFolder { get => _SavedICFolder; }

        private List<string> _SavedWellList;
        public List<string> SavedWellList { get => _SavedWellList; }
        public string CurrentWell { get; internal set; }
        public XDCE_ImageGroup CurrentWellXDCE { get => SavedICFolder.XDCE.Wells[CurrentWell]; }
        public PointF DefaultInitOffset { get; set; }

        private int _NextSavedWellIdx;
        public void IncrementWell()
        {
            do
            {
                CurrentWell = SavedWellList[_NextSavedWellIdx++];
                if (_NextSavedWellIdx >= SavedWellList.Count)
                {
                    IsThereNextWell = false;
                    _NextSavedWellIdx--; //Just to not break anything
                    break;
                }
            } while (!CurrentWellXDCE.HasMasks);
            if (CurrentWell == SavedWellList[0]) /*I.E. the first well of the set*/
                _FOVIdx = Math.Max(-1, FirstFieldIdx_Start - 2); //-1 since it is 1 to 0 indexed , -1 again since we will increment it to start
            else
                _FOVIdx = -1;
        }

        private int _FOVIdx = 0;
        public int FOVIdx { get => _FOVIdx; }

        public int FOVCount { get => CurrentWellXDCE.MaskFields.Count; }
        public void IncrementFOV()
        {
            PreviousFOV = CurrentFOV;
            _FOVIdx++;
            if (_SavedNextFOV != null)
            {
                _SavedThisFOV = _SavedNextFOV;
            }
            else
            {
                XDCE_Image tXI = CurrentWellXDCE.MaskFields[FOVIdx];
                CurrentMaskFullPath = tXI.MaskFullPath;
                CurrentShiftMaskFullPath = tXI.MaskShiftedFullPath;
                _SavedThisFOV = CurrentWellXDCE.GetField(tXI.FOV, this.Wavelength);
            }
            _SavedNextFOV = null;
            IsThereNextField = (_FOVIdx + 1) < CurrentWellXDCE.MaskFields.Count;
        }

        internal void SetTesting(string Well, int FOV_Idx)
        {
            CurrentWell = Well;
            _FOVIdx = FOV_Idx;
        }

        internal void SetCurrentPos(XDCE_Image FOV)
        {
            CurrentFOVPos = AIL.LeicaPositionEstimate_FromGEXDCE(FOV, LeicaImageWidth_um, PointF.Empty, SizeF.Empty);
        }

        /// <summary>
        /// Mixes the Well Based Offset with the Field Refined Offset. If the Mix == 0, then everything is Well based, if the Mix == 1 then everything is the last Field
        /// </summary>
        /// <returns></returns>
        internal PointF GetNextOffset_Mix()
        {
            float x1 = Offset_FieldRefined.X;
            float y1 = Offset_FieldRefined.Y;

            if (x1 == 0 && y1 == 0) return Offset_WellRefined; //first time and there is no Field Refined

            float x2 = Offset_WellRefined.X;
            float y2 = Offset_WellRefined.Y;

            float Mix = Reg_Params.LastFOVOffset_over_WellOffset_MixRatio;

            return new PointF(x1 * Mix + x2 * (1 - Mix), y1 * Mix + y2 * (1 - Mix));
        }

        internal bool IsOffsetWithinMaxBounds(PointF tempOffset)
        {
            //Find the difference from Well
            float dX = Math.Abs(tempOffset.X - Offset_WellRefined.X);
            float dY = Math.Abs(tempOffset.Y - Offset_WellRefined.Y);

            return (dX < Reg_Params.FOVOffset_MaxDeviationFromWell.X) && (dY < Reg_Params.FOVOffset_MaxDeviationFromWell.Y);
        }

        public XDCE_Image PreviousFOV { get; internal set; }
        private XDCE_Image _SavedNextFOV;
        private XDCE_Image _SavedThisFOV;

        public XDCE_Image CurrentFOV
        {
            get
            {
                //XDCE_ImageGroup Well = CurrentWellXDCE;
                //FOV = Well.GetField(FOVIdx + Well.FOV_Min, 0); //Usually the first field is the one the mask is made from . . but this could be an issue
                //return CurrentWellXDCE.MaskFields[FOVIdx]; //Changed 5/17/2021
                return _SavedThisFOV;
            }
            set { _SavedThisFOV = value; }
        }

        public bool FOVsDone { get => false; }

        public string PlateID { get; set; }
        public string SessionID { get; set; }

        private Associate_InCell_Leica _SavedAIL;
        public Associate_InCell_Leica AIL
        {
            get
            {
                return _SavedAIL;
            }
            internal set { _SavedAIL = value; }
        }

        private string _Path_Root;
        public string Path_Root
        {
            get => _Path_Root; set
            {
                DirectoryInfo DI = new DirectoryInfo(value); if (!DI.Exists) DI.Create();
                _Path_Root = value;
                //Now we can update the rest of the folders
                DI = new DirectoryInfo(Path_WellImages); if (!DI.Exists) DI.Create();
                DI = new DirectoryInfo(Path_FieldImage); if (!DI.Exists) DI.Create();
                DI = new DirectoryInfo(Path_Overlays); if (!DI.Exists) DI.Create();
                DI = new DirectoryInfo(Path_INI_Folder); if (!DI.Exists) DI.Create();
            }
        }
        public string Path_WellImages { get => Path.Combine(Path_Root, PlateID, SessionID, "Well"); }
        public string Path_FieldImage { get => Path.Combine(Path_Root, PlateID, SessionID, "FOV"); }
        public string Path_INI_Folder { get => Path.Combine(Path_Root, "INI"); }
        public string Path_Overlays { get => Path.Combine(Path_Root, PlateID, SessionID, "Overlays"); }
        public string FullPath_Wellini { get => Path.Combine(Path_INI_Folder, "Wells.ini"); }
        public string FullPath_FOVini { get => Path.Combine(Path_INI_Folder, "Fields.ini"); }
        public string FullPath_Maskini { get => Path.Combine(Path_INI_Folder, "Masks.ini"); }
        public string FullPath_Log { get => Path.Combine(Path_INI_Folder, "Log.txt"); }
        public string Objective { get => "20x"; }
        public PointF CurrentFOVPos { get; internal set; }
        public string Shift { get; set; }
        public short BinningField { get => 4; }
        public short BinningWell { get => 4; }
        public float LeicaImageWidth_um { get => 2048 * 0.325f; }
        public bool IsThereNextWell { get; set; }
        public bool IsThereNextField { get; set; }
        public bool TestingMode = false;
        public ImageAlign_Return IAR;

        public PointF WellBased_MaxRange_LastSet;
        public PointF Offset_WellRefined;
        public PointF Offset_FieldRefined { get; internal set; }
        public string FOVImageToDelete { get; internal set; }
        public string CurrentMaskFullPath { get; set; }
        public string CurrentShiftMaskFullPath { get; set; }
        public bool CancelRequested { get; set; }
        public bool DifferentThanLast_Y { get { try { return Math.Abs(PreviousFOV.PlateY_um - CurrentFOV.PlateY_um) > 1.5; } catch { return true; } } }
        public bool DifferentThanLast_X { get { try { return Math.Abs(PreviousFOV.PlateX_um - CurrentFOV.PlateX_um) > 1.5; } catch { return true; } } }
        public int FirstFieldIdx_Start { get; set; }
        public int WellFieldChecks { get; set; }
    }

    public class ImageAlign_Return
    {
        public string WellLabel;
        public string Note;
        public string ExportName { get; set; }
        public string OverlayName;
        public PointF FractCenter_GE_um;
        public PointF FractCenter_LE_um;
        public Register_Return RR;
        public double ImgInternal_RedVariation;
        public double ImgExternal_RedVariation;
        public DateTime StartTime;
        public DateTime EndTime;

        [XmlIgnore]
        public Associate_InCell_Leica Parent;


        public PointF Offset_Before { get; set; } //Just informative in the saved .xml file, not used in the calculation
        public PointF Offset_After { get; set; } //Just informative in the saved .xml file, not used in the calculation
        public PointF Leica_StartingPosition { get; set; } //Just informative in the saved .xml file, not used in the calculation
        public PointF InCell_StartingPosition { get; set; } //Just informative in the saved .xml file, not used in the calculation

        public ImageAlign_Return()
        {
            //Need this to serialize
        }

        public ImageAlign_Return(string well, Associate_InCell_Leica AIL_PArent)
        {
            StartTime = DateTime.Now;
            WellLabel = well;
            Note = "";
            Parent = AIL_PArent;
        }

        public ImageAlign_Return(string well, string OnlyNote)
        {
            WellLabel = well;
            Note = OnlyNote;
        }



        public static float Distance(ImageAlign_Return Offset1, ImageAlign_Return Offset2)
        {
            return Distance(Offset1.Offset, Offset2.Offset);
        }

        public static float Distance(PointF Offset1, PointF Offset2)
        {
            return (float)Math.Sqrt((Offset1.X - Offset2.X) * (Offset1.X - Offset2.X) + (Offset1.Y - Offset2.Y) * (Offset1.Y - Offset2.Y));
        }

        internal void ExportQC(string FullFileName)
        {
            this.EndTime = DateTime.Now;
            //Serialize this and save it as an XML file
            using (var writer = new StreamWriter(FullFileName))
            {
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(writer, this);
                writer.Flush();
            }
        }

        public static ImageAlign_Return Load(string fullName)
        {
            try
            {
                using (var stream = File.OpenRead(fullName))
                {
                    var serializer = new XmlSerializer(typeof(ImageAlign_Return));
                    return (ImageAlign_Return)serializer.Deserialize(stream);
                }
            }
            catch
            {

            }
            return null;
        }

        public static void ReadFromFolder(string FolderPath)
        {
            var R = new Random();
            DirectoryInfo DI = new DirectoryInfo(FolderPath);
            ImageAlign_Return IAR;
            StringBuilder sB = new StringBuilder();
            bool First = true;
            foreach (FileInfo FI in DI.GetFiles("*.xml"))
            {
                IAR = ImageAlign_Return.Load(FI.FullName);
                if (First)
                {
                    sB.Append("Name" + "\t" + IAR.FullResultsString(true) + "\r\n");
                    First = false;
                }
                sB.Append(FI.Name + "\t" + IAR.FullResultsString() + "\r\n");
                if (R.Next(0, 200) < 2) Debug.Print(FI.Name);
            }
            File.WriteAllText(Path.Combine(FolderPath, "_XMLCompiled.txt"), sB.ToString());
        }

        public string FullResultsString(bool H = false)
        {
            char d = '\t';

            string ProcessVal(bool Header, string Name, string Type, object Ret)
            {
                if (Type == "PointF")
                {
                    if (Header) return Name + ".X" + d + Name + ".Y";
                    var PF = (PointF)Ret;
                    return "" + PF.X + d + PF.Y;
                }
                if (Header) return Name;
                return "" + Ret;
            }

            StringBuilder sB = new StringBuilder();
            foreach (System.Reflection.PropertyInfo prop in this.GetType().GetProperties()) sB.Append(ProcessVal(H, prop.Name, prop.PropertyType.Name, prop.GetValue(this)) + d);
            foreach (System.Reflection.FieldInfo prop in this.GetType().GetFields()) sB.Append(ProcessVal(H, prop.Name, prop.FieldType.Name, prop.GetValue(this)) + d);
            if (RR != null)
            {
                foreach (System.Reflection.PropertyInfo prop in RR.GetType().GetProperties()) sB.Append(ProcessVal(H, prop.Name, prop.PropertyType.Name, prop.GetValue(RR)) + d);
                foreach (System.Reflection.FieldInfo prop in RR.GetType().GetFields()) sB.Append(ProcessVal(H, prop.Name, prop.FieldType.Name, prop.GetValue(RR)) + d);
            }
            //foreach (System.Reflection.PropertyInfo prop in this.GetType().GetProperties()) sB.Append(prop.Name + ":" + prop.GetValue(this) + "\t");
            //foreach (System.Reflection.FieldInfo field in this.GetType().GetFields()) sB.Append(field.Name + ":" + field.GetValue(this) + "\t");
            //if (RR != null)
            //{
            //    foreach (System.Reflection.PropertyInfo prop in RR.GetType().GetProperties()) sB.Append(prop.Name + ":" + prop.GetValue(RR) + "\t");
            //    foreach (System.Reflection.FieldInfo field in RR.GetType().GetFields()) sB.Append(field.Name + ":" + field.GetValue(RR) + "\t");
            //}
            return sB.ToString().Replace("\n", " ");
        }

        public PointF Offset
        {
            get
            {
                return new PointF((FractCenter_GE_um.X - FractCenter_LE_um.X), (FractCenter_GE_um.Y - FractCenter_LE_um.Y));
            }
        }

        public string Offsets
        {
            get
            {
                return "x" + (FractCenter_GE_um.X - FractCenter_LE_um.X).ToString("0") + " y" + (FractCenter_GE_um.Y - FractCenter_LE_um.Y).ToString("0");
            }
        }
    }

    public static class LeicaOnTheFly
    {
        static bool Testing = true;

        public static SharedRes_OnTheFly SRO;
        public static void InitStuff(INCELL_Folder iC_Folder, Seg.CropImageSettings CropSettings, MLSettings_PL mLSettings_PL, string ExpName)
        {
            ImageExtensions.Add(".TIFF"); ImageExtensions.Add(".TIF"); ImageExtensions.Add(".BMP");

            SRO = new SharedRes_OnTheFly(iC_Folder, CropSettings, mLSettings_PL, ExpName);
            //Now move those inital images to a new folder

            if (!Testing) MoveImages(iC_Folder, "Initial");
            if (Testing) NextField();
        }

        public static void NextField()
        {
            if (!Testing) SRO.IC_Folder.RefreshImages();
            var ImageParts = SRO.IC_Folder.XDCE.WellFOV_Dict;
            var Res = Seg.Segmentation.RunSegmentation(false, DateTime.Now, ImageParts, SRO.CropSettings, SRO.IC_Folder.InCell_Wavelength_Notes); //Actually Segmentation Part of it
            if (!Testing) MoveImages(SRO.IC_Folder, "Regular");
        }

        internal static HashSet<string> ImageExtensions = new HashSet<string>();

        public static void MoveImages(INCELL_Folder iC_Folder, string NewLocationName)
        {
            DirectoryInfo DI = new DirectoryInfo(Path.Combine(iC_Folder.FullPath, SRO.ExpName, NewLocationName));
            if (!DI.Exists) DI.Create(); string ext;
            foreach (string file in Directory.GetFiles(iC_Folder.FullPath))
            {
                ext = Path.GetExtension(file).ToUpper();
                if (ImageExtensions.Contains(ext))
                    File.Move(file, Path.Combine(DI.FullName, Path.GetFileName(file)));
            }
        }
    }

    public class SharedRes_OnTheFly
    {
        public INCELL_Folder IC_Folder;
        public Seg.CropImageSettings CropSettings;
        public MLSettings_PL MLSettings { get => CropSettings.MLSettings; }
        public string ExpName;

        public SharedRes_OnTheFly(INCELL_Folder iC_Folder, Seg.CropImageSettings CropSettings, MLSettings_PL mLSettings_PL, string ExpName)
        {
            this.IC_Folder = iC_Folder;
            this.ExpName = ExpName.Trim();
            this.CropSettings = new Seg.CropImageSettings(CropSettings); //Important so we can change these independent from the main ones

            this.CropSettings.RunMLProcessing = "PL";
            this.CropSettings.RandomizeImageIntensity_Min = 1;
            this.CropSettings.MLSettings = mLSettings_PL;
        }
    }

    namespace Utilities
    {
        public class MathUtils
        {
            public static PointF GetAverage(PointF p1, PointF p2)
            {
                return new PointF((p1.X + p2.X) / 2, (p1.Y + p2.Y) / 2);
            }

            public static void LinearRegression(float[] xVals, float[] yVals, out double rSquared, out double yIntercept, out double slope)
            {
                //https://gist.github.com/NikolayIT/d86118a3a0cb3f5ed63d674a350d75f2
                if (xVals.Length != yVals.Length)
                {
                    throw new Exception("Input values should be with the same length.");
                }

                double sumOfX = 0;
                double sumOfY = 0;
                double sumOfXSq = 0;
                double sumOfYSq = 0;
                double sumCodeviates = 0;

                for (var i = 0; i < xVals.Length; i++)
                {
                    var x = xVals[i]; var y = yVals[i];
                    sumCodeviates += x * y;
                    sumOfX += x; sumOfY += y;
                    sumOfXSq += x * x; sumOfYSq += y * y;
                }

                var count = xVals.Length;
                var ssX = sumOfXSq - ((sumOfX * sumOfX) / count);
                var ssY = sumOfYSq - ((sumOfY * sumOfY) / count);

                var rNumerator = (count * sumCodeviates) - (sumOfX * sumOfY);
                var rDenom = (count * sumOfXSq - (sumOfX * sumOfX)) * (count * sumOfYSq - (sumOfY * sumOfY));
                var sCo = sumCodeviates - ((sumOfX * sumOfY) / count);

                var meanX = sumOfX / count;
                var meanY = sumOfY / count;
                var dblR = rNumerator / Math.Sqrt(rDenom);

                rSquared = dblR * dblR;
                yIntercept = meanY - ((sCo / ssX) * meanX);
                slope = sCo / ssX;
            }
        }
        public class DrawingUtils
        {
            public static Brush RedBrush = new SolidBrush(Color.Red);

            public static Bitmap GetImageSt(XDCE_Image xI)
            {
                return new Bitmap(Path.Combine(xI.Parent.FolderPath, xI.FileName));
            }

            public static Bitmap GetAdjustedImageSt_Old(string ImageFolder, XDCE_Image xI, float Brightness)
            {
                Bitmap bmap = new Bitmap(10, 10);
                if (xI == null) return bmap;
                try
                {
                    string Filename = Path.Combine(ImageFolder, xI.FileName);
                    bmap = Utilities.ColorMatrix.AdjustBrightness_Old(new Bitmap(Filename), Brightness);
                }
                catch
                {

                }
                return bmap;
            }

            public static Bitmap GetAdjustedImageSt(XDCE_Image xI, float Brightness)
            {
                if (xI == null) return null;
                ushort blackPoint = 10; ushort whitePoint = (ushort)(65000F / Brightness);
                return GetAdjustedImageSt(xI.Parent.FolderPath, xI, blackPoint, whitePoint);
            }

            public static Bitmap GetAdjustedImageSt(XDCE_Image xI, ushort blackPoint, ushort whitePoint)
            {
                if (xI == null) return null;
                return GetAdjustedImageSt(xI.Parent.FolderPath, xI, blackPoint, whitePoint);
            }

            public static Bitmap GetAdjustedImageSt(string ImageFolder, XDCE_Image xI, float Brightness)
            {
                ushort blackPoint = 10; ushort whitePoint = (ushort)(65000F / Brightness);
                return GetAdjustedImageSt(ImageFolder, xI, blackPoint, whitePoint);
            }

            public static Bitmap GetAdjustedImageSt(string ImageFolder, XDCE_Image xI, ushort blackPoint, ushort whitePoint)
            {
                Bitmap bmap = new Bitmap(10, 10);
                if (xI == null) return bmap;
                try
                {
                    string Filename = Path.Combine(ImageFolder, xI.FileName);
                    //whitePoint = 2000;
                    bmap = Utilities.ColorMatrix.AdjustBrightness(Filename, blackPoint, whitePoint);
                    //bmap.Save(@"S:\Cells\FIV617\UNK\trying.jpg");
                }
                catch
                {

                }
                return bmap;
            }

            public static MagickImage GetAdjustedMImageSt( XDCE_Image xI, float Brightness)
            {
                if (xI == null) return null;
                MagickImage MagI = null;
                try
                {   
                    string Filename = Path.Combine(xI.Parent.FolderPath, xI.FileName);
                    ushort blackPoint = 10; ushort whitePoint = (ushort)(65000F / Brightness);
                    MagI = Utilities.ColorMatrix.AdjustBrightnessMI(Filename, blackPoint, whitePoint);
                }
                catch
                {

                }
                return MagI;
            }

            public static Bitmap CombinedBMAPst(XDCE_Image xI, wvDisplayParams CurrentWVParams, XDCE_ImageGroup Welli = null, float RandomizeStrechMin = 1, string ColorCalcTypeSz = "Add")
            {   //Randomize stretch of 1 turns off RandomStretch
                try
                {    
                    wvDisplayParams wvPs = CurrentWVParams;
                    Bitmap bmap, BMTemp;
                    var ColorCalcType = ColorMatrix.GetColorCalcFromString(ColorCalcTypeSz);
                    var MF = new MagickFactory();
                    if (Welli == null) Welli = xI.Parent.Wells[xI.WellLabel];

                    //First get the parameters that the user wants and load all the bitmaps
                    List<Bitmap> bmaps = new List<Bitmap>();
                    foreach (wvDisplayParam wvP in wvPs.Actives)
                    {
                        BMTemp = GetAdjustedImageSt(Welli.GetField(xI.FOV, wvP.wvIdx), RandomizeStrechMin >= 1 ? wvP.Brightness : RandomStretch(wvP.Brightness, RandomizeStrechMin));
                        if (wvP.Color == "FFFFFF")
                        {
                            //No change needed
                        }
                        else if (ColorCalcType != ColorMatrix.ColorCalculationType.Difference)
                        {
                            BMTemp = ColorMatrix.AdjustColors(BMTemp, (float)wvP.c_R, (float)wvP.c_G, (float)wvP.c_B);
                            //ImageMagick Was actually alittle slower, so fixed this 10/19/2022
                        }
                        bmaps.Add(BMTemp);
                    }

                    //Multichannel stuff
                    if (bmaps.Count == 0) return new Bitmap(10, 10);
                    bmap = bmaps[0];
                    for (int i = 1; i < bmaps.Count; i++) //Note we start at 1 not 0
                    {
                        if (bmaps[i] == null) return new Bitmap(10, 10);
                        bmap = ColorMatrix.ArithmeticBlend(bmaps[i], bmap, ColorCalcType); //SubtractRight was previous
                    }

                    //Clip based on Threshold - - - - - -- - - - 
                    bmap = ClipChannels(xI, bmap, CurrentWVParams, Welli);
                    return bmap;
                }
                catch
                {
                    return new Bitmap(10, 10);
                }
            } 


            public static Bitmap ClipChannels(XDCE_Image xI, Bitmap bmap, wvDisplayParams CurrentWVParams, XDCE_ImageGroup CurrentWell )
            {
                if (CurrentWVParams.UseClipping)
                {
                    //First do the positive direction
                    //Only show VIS part of it where the OBJ part of it is checked
                    wvDisplayParam wvP = null;
                    foreach (var wvPt in CurrentWVParams)
                        if (wvPt.ObjectAnalysis) { wvP = wvPt; break; }
                    if (wvP != null)
                    {
                        var BMTemp = DrawingUtils.GetAdjustedImageSt(CurrentWell.GetField(xI.FOV, wvP.wvIdx), wvP.Brightness);
                        var Thresh = new Seg.ThreshImage(BMTemp, (int)wvP.Threshold, true, 1);
                        BMTemp = Thresh.GetPostThreshBitmap(false, CurrentWVParams.ClippingExpand_positive);
                        bmap = ColorMatrix.ArithmeticBlend(bmap, BMTemp, ColorMatrix.ColorCalculationType.Multiply); 
                    }

                    //Now do the negative direction (multiply by the inverse mask)
                    wvP = null;
                    foreach (var wvPt in CurrentWVParams)
                        if (wvPt.DI_Analysis) { wvP = wvPt; break; }
                    if (wvP != null)
                    {
                        var BMTemp = DrawingUtils.GetAdjustedImageSt(CurrentWell.GetField(xI.FOV, wvP.wvIdx), wvP.Brightness);
                        var Thresh = new Seg.ThreshImage(BMTemp, (int)wvP.Threshold, true, 1);
                        BMTemp = Thresh.GetPostThreshBitmap(true, CurrentWVParams.ClippingExpand_negative);
                        bmap = ColorMatrix.ArithmeticBlend(bmap, BMTemp, ColorMatrix.ColorCalculationType.Multiply); 
                    }
                }

                return bmap;
            }

            private static Random _Rand = new Random();

            private static float RandomStretch(float brightness, float randomizeStrechMin)
            {
                float min = randomizeStrechMin;
                return (float)(brightness * (min + _Rand.NextDouble() * (1 - min)));
            }
        }

        public class ColorMatrix
        {
            private static Dictionary<string, ColorCalculationType> _ColorCalcLoopUp;
            private static string[] _ColorCalcTypes;
            public static string[] ColorCalcTypes
            {
                get
                {
                    if (_ColorCalcTypes == null)
                    {
                        _ColorCalcTypes = Enum.GetNames(typeof(ColorCalculationType));
                        _ColorCalcLoopUp = Enum.GetValues(typeof(ColorCalculationType)).Cast<ColorCalculationType>().ToDictionary(t => t.ToString(), t => t);
                    }
                    return _ColorCalcTypes;
                }
            }

            public static ColorCalculationType GetColorCalcFromString(string CCType) { return _ColorCalcLoopUp[CCType]; }

            public static Bitmap AdjustBrighnessSlow(string FileName, double brightness)
            {
                //https://docs.microsoft.com/en-us/windows/win32/wic/-wic-codec-native-pixel-formats#bit-depth
                Bitmap temp = new Bitmap(FileName);
                Bitmap bmap = (Bitmap)temp.Clone();
                if (brightness < -255) brightness = -255;
                if (brightness > 255) brightness = 255;
                Color col;
                for (int i = 0; i < bmap.Width; i++)
                {
                    for (int j = 0; j < bmap.Height; j++)
                    {
                        col = bmap.GetPixel(i, j);
                        int colRed = (int)(col.R * brightness);
                        int colGreen = (int)(col.G * brightness);
                        int colBlue = (int)(col.B * brightness);
                        if (col.Name != "ff000000") { }
                        if (colRed < 0) colRed = 1;
                        if (colRed > 255) colRed = 255;

                        if (colGreen < 0) colGreen = 1;
                        if (colGreen > 255) colGreen = 255;

                        if (colBlue < 0) colBlue = 1;
                        if (colBlue > 255) colBlue = 255;

                        bmap.SetPixel(i, j, Color.FromArgb((byte)colRed, (byte)colGreen, (byte)colBlue));
                    }
                }

                return bmap;
            }

            /// <summary>
            /// New Adjust brightness from Josh 9/2022
            /// </summary>
            /// <param name="filename"></param>
            /// <param name="blackPoint"></param>
            /// <param name="whitePoint"></param>
            public static Bitmap AdjustBrightness(string filename, ushort blackPoint, ushort whitePoint)
            {
                var mI = AdjustBrightnessMI(filename, blackPoint, whitePoint);
                var bmp = mI.ToBitmap(ImageFormat.MemoryBmp);
                bmp.SetResolution(96, 96);
                return bmp;
            }

            /// <summary>
            /// New Adjust brightness from Josh 9/2022
            /// </summary>
            /// <param name="filename"></param>
            /// <param name="blackPoint"></param>
            /// <param name="whitePoint"></param>
            public static MagickImage AdjustBrightnessMI(string filename, ushort blackPoint, ushort whitePoint)
            {
                var mI = new MagickImage(filename);
                mI.Level(blackPoint, whitePoint);
                return mI;
            }

            /// <summary>
            /// Adjusts the brightness
            /// </summary>
            /// <param name="Image">Image to change</param>
            /// <param name="Value">-255 to 255</param>
            /// <returns>A bitmap object</returns>
            public static Bitmap AdjustBrightness_Old(Bitmap Image, float Brightness)
            {
                //http://csharphelper.com/blog/2014/10/use-an-imageattributes-object-to-adjust-an-images-brightness-in-c/
                float b = Brightness;
                ColorMatrix TempMatrix = new ColorMatrix();
                TempMatrix.Matrix = new float[][]{
                       new float[] {b, 0, 0, 0, 0},
                       new float[] {0, b, 0, 0, 0},
                       new float[] {0, 0, b, 0, 0},
                       new float[] {0, 0, 0, 1, 0},
                       new float[] {0, 0, 0, 0, 1}
                   };
                return TempMatrix.Apply(Image);
            }

            public static Bitmap AdjustColors(Bitmap Image, float R, float G, float B)
            {
                //http://csharphelper.com/blog/2014/10/use-an-imageattributes-object-to-adjust-an-images-brightness-in-c/
                ColorMatrix TempMatrix = new ColorMatrix();
                TempMatrix.Matrix = new float[][]{
                       new float[] {R, 0, 0, 0, 0},
                       new float[] {0, G, 0, 0, 0},
                       new float[] {0, 0, B, 0, 0},
                       new float[] {0, 0, 0, 1, 0},
                       new float[] {0, 0, 0, 0, 1}
                   };
                return TempMatrix.Apply(Image);
            }

            public ColorMatrix()
            {
            }

            public float[][] Matrix { get; set; }

            /// <summary>
            /// Applies the color matrix
            /// </summary>
            /// <param name="OriginalImage">Image sent in</param>
            /// <returns>An image with the color matrix applied</returns>
            public Bitmap Apply(Bitmap OriginalImage)
            {
                Bitmap NewBitmap = new Bitmap(OriginalImage.Width, OriginalImage.Height);
                using (var NewGraphics = Graphics.FromImage(NewBitmap))
                {
                    var NewColorMatrix = new System.Drawing.Imaging.ColorMatrix(Matrix);
                    using (ImageAttributes Attributes = new ImageAttributes())
                    {
                        Attributes.SetColorMatrix(NewColorMatrix);
                        NewGraphics.DrawImage(OriginalImage,
                            new Rectangle(0, 0, OriginalImage.Width, OriginalImage.Height),
                            0, 0, OriginalImage.Width, OriginalImage.Height,
                            GraphicsUnit.Pixel, Attributes);
                    }
                }
                return NewBitmap;
            }

            // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
            // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

            //https://softwarebydefault.com/2013/04/27/image-arithmetic/

            public enum ColorCalculationType
            {
                Average,
                Add,
                SubtractLeft,
                SubtractRight,
                Difference,
                Multiply,
                Min,
                Max,
                Amplitude
            }

#if (TRUE)  //USE THIS TO SWAP OUT THE OLD AND NEW Arithmetic Blends
            //Josh's faster version 11/2022
            //3Ch Old 731, 816, 678, 746, 653 (combineBMAPst on Willie's GTAC machine)
            //3Ch New 497, 482, 520, 495, 497
            public static Bitmap ArithmeticBlend(Bitmap sourceBitmap, Bitmap blendBitmap, ColorCalculationType calculationType)
            {
                BitmapData sourceData = sourceBitmap.LockBits(new Rectangle(0, 0,
                                        sourceBitmap.Width, sourceBitmap.Height),
                                        ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                BitmapData blendData = blendBitmap.LockBits(new Rectangle(0, 0,
                                       blendBitmap.Width, blendBitmap.Height),
                                       ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                unsafe
                {
                    bool subtractionInvolved = calculationType == ColorCalculationType.Difference || calculationType == ColorCalculationType.SubtractRight || calculationType == ColorCalculationType.SubtractLeft;
                    int bytesPerPixel = Image.GetPixelFormatSize(sourceData.PixelFormat) / 8;
                    int heightInPixels = sourceData.Height;
                    int widthInBytes = sourceData.Width * bytesPerPixel;
                    byte* PtrFirstPixelsource = (byte*)sourceData.Scan0;
                    byte* PtrFirstPixelblend = (byte*)blendData.Scan0;
                    //int mathHolder = 0;
                    Parallel.For(0, heightInPixels, y =>
                    {
                        byte* sourceLine = PtrFirstPixelsource + (y * sourceData.Stride);
                        byte* blendLine = PtrFirstPixelblend + (y * blendData.Stride);
                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            switch (calculationType)
                            {
                                case ColorCalculationType.Add:
                                    sourceLine[x] = Calculate(sourceLine[x], blendLine[x], ColorCalculationType.Add);
                                    sourceLine[x + 1] = Calculate(sourceLine[x + 1], blendLine[x + 1], ColorCalculationType.Add);
                                    sourceLine[x + 2] = Calculate(sourceLine[x + 2], blendLine[x + 2], ColorCalculationType.Add);
                                    break;
                                case ColorCalculationType.Average:
                                    sourceLine[x] = Calculate(sourceLine[x], blendLine[x], ColorCalculationType.Average);
                                    sourceLine[x + 1] = Calculate(sourceLine[x + 1], blendLine[x + 1], ColorCalculationType.Average);
                                    sourceLine[x + 2] = Calculate(sourceLine[x + 2], blendLine[x + 2], ColorCalculationType.Average);
                                    break;
                                case ColorCalculationType.SubtractLeft:
                                    sourceLine[x + 2] = Calculate(sourceLine[x + 2], blendLine[x + 2], ColorCalculationType.SubtractLeft);
                                    break;
                                case ColorCalculationType.SubtractRight:
                                    sourceLine[x + 2] = Calculate(sourceLine[x + 2], blendLine[x + 2], ColorCalculationType.SubtractRight);
                                    break;
                                case ColorCalculationType.Difference:
                                    sourceLine[x + 2] = Calculate(sourceLine[x + 2], blendLine[x + 2], ColorCalculationType.Difference);
                                    break;
                                case ColorCalculationType.Multiply:
                                    sourceLine[x] = Calculate(sourceLine[x], blendLine[x], ColorCalculationType.Multiply);
                                    sourceLine[x + 1] = Calculate(sourceLine[x + 1], blendLine[x + 1], ColorCalculationType.Multiply);
                                    sourceLine[x + 2] = Calculate(sourceLine[x + 2], blendLine[x + 2], ColorCalculationType.Multiply);
                                    break;
                                case ColorCalculationType.Min:
                                    sourceLine[x] = Calculate(sourceLine[x], blendLine[x], ColorCalculationType.Min);
                                    sourceLine[x + 1] = Calculate(sourceLine[x + 1], blendLine[x + 1], ColorCalculationType.Min);
                                    sourceLine[x + 2] = Calculate(sourceLine[x + 2], blendLine[x + 2], ColorCalculationType.Min);
                                    break;
                                case ColorCalculationType.Max:
                                    sourceLine[x] = Calculate(sourceLine[x], blendLine[x], ColorCalculationType.Max);
                                    sourceLine[x + 1] = Calculate(sourceLine[x + 1], blendLine[x + 1], ColorCalculationType.Max);
                                    sourceLine[x + 2] = Calculate(sourceLine[x + 2], blendLine[x + 2], ColorCalculationType.Max);
                                    break;
                                case ColorCalculationType.Amplitude:
                                    sourceLine[x] = Calculate(sourceLine[x], blendLine[x], ColorCalculationType.Amplitude);
                                    sourceLine[x + 1] = Calculate(sourceLine[x + 1], blendLine[x + 1], ColorCalculationType.Amplitude);
                                    sourceLine[x + 2] = Calculate(sourceLine[x + 2], blendLine[x + 2], ColorCalculationType.Amplitude);
                                    break;
                            }
                        }
                    });
                    sourceBitmap.UnlockBits(sourceData);
                }
                return sourceBitmap;
            }
            public static byte Calculate(byte color1, byte color2, ColorCalculationType calculationType)
            {
                int intResult;
                switch (calculationType)
                {
                    case ColorCalculationType.Add:
                        intResult = color1 + color2;
                        break;
                    case ColorCalculationType.Average:
                        intResult = (color1 + color2) / 2;
                        break;
                    case ColorCalculationType.SubtractLeft:
                        intResult = color1 - color2;
                        break;
                    case ColorCalculationType.SubtractRight:
                        intResult = (color1 + color2) / 2;
                        break;
                    case ColorCalculationType.Difference:
                        intResult = color1 - color2;
                        if (color1 - color2 < 0)
                            intResult = -intResult;
                        break;
                    case ColorCalculationType.Multiply:
                        intResult = (int)((color1 / 255.0) * (color2 / 255.0) * 255.0);
                        break;
                    case ColorCalculationType.Min:
                        intResult = (color1 < color2 ? color1 : color2);
                        break;
                    case ColorCalculationType.Max:
                        intResult = (color1 > color2 ? color1 : color2);
                        break;
                    case ColorCalculationType.Amplitude:
                        intResult = (int)(Math.Sqrt(color1 * color1 + color2 * color2) / Math.Sqrt(2.0));
                        break;
                    default:
                        intResult = -1;
                        break;
                }
                if (intResult < 0)
                {
                    return byte.MinValue;
                }
                else if (intResult > 255)
                {
                    return byte.MaxValue;
                }
                else
                {
                    return (byte)intResult;
                }
            }
#else
            public static Bitmap ArithmeticBlend(Bitmap sourceBitmap, Bitmap blendBitmap, ColorCalculationType calculationType)
            {
                BitmapData sourceData = sourceBitmap.LockBits(new Rectangle(0, 0,
                                        sourceBitmap.Width, sourceBitmap.Height),
                                        ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];
                Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
                sourceBitmap.UnlockBits(sourceData);

                BitmapData blendData = blendBitmap.LockBits(new Rectangle(0, 0,
                                       blendBitmap.Width, blendBitmap.Height),
                                       ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                byte[] blendBuffer = new byte[blendData.Stride * blendData.Height];
                Marshal.Copy(blendData.Scan0, blendBuffer, 0, blendBuffer.Length);
                blendBitmap.UnlockBits(blendData);
                if (calculationType == ColorCalculationType.Difference || calculationType == ColorCalculationType.SubtractRight || calculationType == ColorCalculationType.SubtractLeft)
                {
                    for (int k = 0; (k + 4 < pixelBuffer.Length) && (k + 4 < blendBuffer.Length); k += 4)
                    {
                        pixelBuffer[k + 2] = Calculate(pixelBuffer[k + 2], blendBuffer[k + 2], calculationType);
                    }
                }
                else
                {
                    for (int k = 0; (k + 4 < pixelBuffer.Length) && (k + 4 < blendBuffer.Length); k += 4)
                    {
                        pixelBuffer[k + 0] = Calculate(pixelBuffer[k + 0], blendBuffer[k + 0], calculationType);
                        pixelBuffer[k + 1] = Calculate(pixelBuffer[k + 1], blendBuffer[k + 1], calculationType);
                        pixelBuffer[k + 2] = Calculate(pixelBuffer[k + 2], blendBuffer[k + 2], calculationType);
                    }
                }

                Bitmap resultBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);

                BitmapData resultData = resultBitmap.LockBits(new Rectangle(0, 0,
                                        resultBitmap.Width, resultBitmap.Height),
                                        ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

                Marshal.Copy(pixelBuffer, 0, resultData.Scan0, pixelBuffer.Length);
                resultBitmap.UnlockBits(resultData);

                return resultBitmap;
            }

            public static byte Calculate(byte color1, byte color2, ColorCalculationType calculationType)
            {
                byte resultValue = 0;
                int intResult = 0;

                switch (calculationType)
                {
                    case ColorCalculationType.Add:
                        intResult = color1 + color2;
                        break;
                    case ColorCalculationType.Average:
                        intResult = (color1 + color2) / 2;
                        break;
                    case ColorCalculationType.SubtractLeft:
                        intResult = color1 - color2;
                        break;
                    case ColorCalculationType.SubtractRight:
                        intResult = (color1 + color2) / 2;
                        break;
                    case ColorCalculationType.Difference:
                        intResult = Math.Abs(color1 - color2);
                        break;
                    case ColorCalculationType.Multiply:
                        intResult = (int)((color1 / 255.0) * (color2 / 255.0) * 255.0);
                        break;
                    case ColorCalculationType.Min:
                        intResult = (color1 < color2 ? color1 : color2);
                        break;
                    case ColorCalculationType.Max:
                        intResult = (color1 > color2 ? color1 : color2);
                        break;
                    case ColorCalculationType.Amplitude:
                        intResult = (int)(Math.Sqrt(color1 * color1 + color2 * color2) / Math.Sqrt(2.0));
                        break;
                }

                if (intResult < 0)
                {
                    resultValue = byte.MinValue;
                }
                else if (intResult > 255)
                {
                    resultValue = byte.MaxValue;
                }
                else
                {
                    resultValue = (byte)intResult;
                }

                return resultValue;
            }
#endif
        }
    }
}